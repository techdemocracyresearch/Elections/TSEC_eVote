package com.sun.mail.imap;

import com.sun.mail.iap.ByteArray;
import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.protocol.BODY;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.util.FolderClosedIOException;
import com.sun.mail.util.MessageRemovedIOException;
import java.io.IOException;
import java.io.InputStream;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.MessagingException;
import kotlin.UByte;

public class IMAPInputStream extends InputStream {
    private static final int slop = 64;
    private int blksize;
    private byte[] buf;
    private int bufcount;
    private int bufpos;
    private int max;
    private IMAPMessage msg;
    private boolean peek;
    private int pos = 0;
    private ByteArray readbuf;
    private String section;

    public IMAPInputStream(IMAPMessage iMAPMessage, String str, int i, boolean z) {
        this.msg = iMAPMessage;
        this.section = str;
        this.max = i;
        this.peek = z;
        this.blksize = iMAPMessage.getFetchBlockSize();
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0013 */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001d  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001c A[RETURN] */
    private void forceCheckExpunged() throws MessageRemovedIOException, FolderClosedIOException {
        synchronized (this.msg.getMessageCacheLock()) {
            try {
                this.msg.getProtocol().noop();
            } catch (ConnectionException e) {
                throw new FolderClosedIOException(this.msg.getFolder(), e.getMessage());
            } catch (FolderClosedException e2) {
                throw new FolderClosedIOException(e2.getFolder(), e2.getMessage());
            } catch (ProtocolException unknown) {
                if (!this.msg.isExpunged()) {
                }
            }
        }
        if (!this.msg.isExpunged()) {
            throw new MessageRemovedIOException();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050 A[Catch:{ ProtocolException -> 0x00b4, FolderClosedException -> 0x00a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005b A[Catch:{ ProtocolException -> 0x00b4, FolderClosedException -> 0x00a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0067 A[SYNTHETIC] */
    private void fill() throws IOException {
        int i;
        BODY body;
        ByteArray byteArray;
        int i2;
        int i3 = this.max;
        if (i3 == -1 || (i2 = this.pos) < i3) {
            if (this.readbuf == null) {
                this.readbuf = new ByteArray(this.blksize + 64);
            }
            synchronized (this.msg.getMessageCacheLock()) {
                try {
                    IMAPProtocol protocol = this.msg.getProtocol();
                    if (!this.msg.isExpunged()) {
                        int sequenceNumber = this.msg.getSequenceNumber();
                        int i4 = this.blksize;
                        int i5 = this.max;
                        if (i5 != -1) {
                            int i6 = this.pos;
                            if (i6 + i4 > i5) {
                                i = i5 - i6;
                                if (!this.peek) {
                                    body = protocol.peekBody(sequenceNumber, this.section, this.pos, i, this.readbuf);
                                } else {
                                    body = protocol.fetchBody(sequenceNumber, this.section, this.pos, i, this.readbuf);
                                }
                                if (body != null) {
                                    byteArray = body.getByteArray();
                                    if (byteArray != null) {
                                    }
                                }
                                forceCheckExpunged();
                                throw new IOException("No content");
                            }
                        }
                        i = i4;
                        if (!this.peek) {
                        }
                        if (body != null) {
                        }
                        forceCheckExpunged();
                        throw new IOException("No content");
                    }
                    throw new MessageRemovedIOException("No content for expunged message");
                } catch (ProtocolException e) {
                    forceCheckExpunged();
                    throw new IOException(e.getMessage());
                } catch (FolderClosedException e2) {
                    throw new FolderClosedIOException(e2.getFolder(), e2.getMessage());
                }
            }
            if (this.pos == 0) {
                checkSeen();
            }
            this.buf = byteArray.getBytes();
            this.bufpos = byteArray.getStart();
            int count = byteArray.getCount();
            this.bufcount = this.bufpos + count;
            this.pos += count;
            return;
        }
        if (i2 == 0) {
            checkSeen();
        }
        this.readbuf = null;
    }

    @Override // java.io.InputStream
    public synchronized int read() throws IOException {
        if (this.bufpos >= this.bufcount) {
            fill();
            if (this.bufpos >= this.bufcount) {
                return -1;
            }
        }
        byte[] bArr = this.buf;
        int i = this.bufpos;
        this.bufpos = i + 1;
        return bArr[i] & UByte.MAX_VALUE;
    }

    @Override // java.io.InputStream
    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        int i3 = this.bufcount - this.bufpos;
        if (i3 <= 0) {
            fill();
            i3 = this.bufcount - this.bufpos;
            if (i3 <= 0) {
                return -1;
            }
        }
        if (i3 < i2) {
            i2 = i3;
        }
        System.arraycopy(this.buf, this.bufpos, bArr, i, i2);
        this.bufpos += i2;
        return i2;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public synchronized int available() throws IOException {
        return this.bufcount - this.bufpos;
    }

    private void checkSeen() {
        if (!this.peek) {
            try {
                Folder folder = this.msg.getFolder();
                if (folder != null && folder.getMode() != 1 && !this.msg.isSet(Flags.Flag.SEEN)) {
                    this.msg.setFlag(Flags.Flag.SEEN, true);
                }
            } catch (MessagingException unused) {
            }
        }
    }
}
