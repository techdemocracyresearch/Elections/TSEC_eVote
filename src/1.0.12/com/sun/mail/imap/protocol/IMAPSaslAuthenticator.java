package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;
import java.util.Vector;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.sasl.RealmCallback;
import javax.security.sasl.RealmChoiceCallback;
import javax.security.sasl.Sasl;
import javax.security.sasl.SaslClient;
import javax.security.sasl.SaslException;

public class IMAPSaslAuthenticator implements SaslAuthenticator {
    private boolean debug;
    private String host;
    private String name;
    private PrintStream out;
    private IMAPProtocol pr;
    private Properties props;

    public IMAPSaslAuthenticator(IMAPProtocol iMAPProtocol, String str, Properties properties, boolean z, PrintStream printStream, String str2) {
        this.pr = iMAPProtocol;
        this.name = str;
        this.props = properties;
        this.debug = z;
        this.out = printStream;
        this.host = str2;
    }

    @Override // com.sun.mail.imap.protocol.SaslAuthenticator
    public boolean authenticate(String[] strArr, final String str, String str2, final String str3, final String str4) throws ProtocolException {
        String str5;
        synchronized (this.pr) {
            Vector vector = new Vector();
            if (this.debug) {
                this.out.print("IMAP SASL DEBUG: Mechanisms:");
                for (int i = 0; i < strArr.length; i++) {
                    this.out.print(" " + strArr[i]);
                }
                this.out.println();
            }
            try {
                SaslClient createSaslClient = Sasl.createSaslClient(strArr, str2, this.name, this.host, this.props, new CallbackHandler() {
                    /* class com.sun.mail.imap.protocol.IMAPSaslAuthenticator.AnonymousClass1 */

                    @Override // javax.security.auth.callback.CallbackHandler
                    public void handle(Callback[] callbackArr) {
                        if (IMAPSaslAuthenticator.this.debug) {
                            PrintStream printStream = IMAPSaslAuthenticator.this.out;
                            printStream.println("IMAP SASL DEBUG: callback length: " + callbackArr.length);
                        }
                        for (int i = 0; i < callbackArr.length; i++) {
                            if (IMAPSaslAuthenticator.this.debug) {
                                PrintStream printStream2 = IMAPSaslAuthenticator.this.out;
                                printStream2.println("IMAP SASL DEBUG: callback " + i + ": " + callbackArr[i]);
                            }
                            if (callbackArr[i] instanceof NameCallback) {
                                ((NameCallback) callbackArr[i]).setName(str3);
                            } else if (callbackArr[i] instanceof PasswordCallback) {
                                ((PasswordCallback) callbackArr[i]).setPassword(str4.toCharArray());
                            } else if (callbackArr[i] instanceof RealmCallback) {
                                RealmCallback realmCallback = (RealmCallback) callbackArr[i];
                                String str = str;
                                if (str == null) {
                                    str = realmCallback.getDefaultText();
                                }
                                realmCallback.setText(str);
                            } else if (callbackArr[i] instanceof RealmChoiceCallback) {
                                RealmChoiceCallback realmChoiceCallback = (RealmChoiceCallback) callbackArr[i];
                                if (str != null) {
                                    String[] choices = realmChoiceCallback.getChoices();
                                    int i2 = 0;
                                    while (true) {
                                        if (i2 >= choices.length) {
                                            break;
                                        } else if (choices[i2].equals(str)) {
                                            realmChoiceCallback.setSelectedIndex(i2);
                                            break;
                                        } else {
                                            i2++;
                                        }
                                    }
                                } else {
                                    realmChoiceCallback.setSelectedIndex(realmChoiceCallback.getDefaultChoice());
                                }
                            }
                        }
                    }
                });
                if (createSaslClient == null) {
                    if (this.debug) {
                        this.out.println("IMAP SASL DEBUG: No SASL support");
                    }
                    return false;
                }
                if (this.debug) {
                    this.out.println("IMAP SASL DEBUG: SASL client " + createSaslClient.getMechanismName());
                }
                try {
                    byte[] bArr = null;
                    String writeCommand = this.pr.writeCommand("AUTHENTICATE " + createSaslClient.getMechanismName(), null);
                    OutputStream iMAPOutputStream = this.pr.getIMAPOutputStream();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    boolean z = true;
                    byte[] bArr2 = {13, 10};
                    boolean equals = createSaslClient.getMechanismName().equals("XGWTRUSTEDAPP");
                    Response response = null;
                    boolean z2 = false;
                    while (!z2) {
                        try {
                            response = this.pr.readResponse();
                            if (response.isContinuation()) {
                                byte[] bArr3 = bArr;
                                if (!createSaslClient.isComplete()) {
                                    byte[] newBytes = response.readByteArray().getNewBytes();
                                    if (newBytes.length > 0) {
                                        newBytes = BASE64DecoderStream.decode(newBytes);
                                    }
                                    if (this.debug) {
                                        this.out.println("IMAP SASL DEBUG: challenge: " + ASCIIUtility.toString(newBytes, 0, newBytes.length) + " :");
                                    }
                                    bArr3 = createSaslClient.evaluateChallenge(newBytes);
                                }
                                if (bArr3 == null) {
                                    if (this.debug) {
                                        this.out.println("IMAP SASL DEBUG: no response");
                                    }
                                    iMAPOutputStream.write(bArr2);
                                    iMAPOutputStream.flush();
                                    byteArrayOutputStream.reset();
                                } else {
                                    if (this.debug) {
                                        this.out.println("IMAP SASL DEBUG: response: " + ASCIIUtility.toString(bArr3, 0, bArr3.length) + " :");
                                    }
                                    byte[] encode = BASE64EncoderStream.encode(bArr3);
                                    if (equals) {
                                        byteArrayOutputStream.write("XGWTRUSTEDAPP ".getBytes());
                                    }
                                    byteArrayOutputStream.write(encode);
                                    byteArrayOutputStream.write(bArr2);
                                    iMAPOutputStream.write(byteArrayOutputStream.toByteArray());
                                    iMAPOutputStream.flush();
                                    byteArrayOutputStream.reset();
                                }
                            } else if (response.isTagged() && response.getTag().equals(writeCommand)) {
                                z2 = true;
                            } else if (response.isBYE()) {
                                z2 = true;
                            } else {
                                vector.addElement(response);
                            }
                        } catch (Exception e) {
                            if (this.debug) {
                                e.printStackTrace();
                            }
                            response = Response.byeResponse(e);
                        }
                        bArr = null;
                        z = true;
                    }
                    if (!createSaslClient.isComplete() || (str5 = (String) createSaslClient.getNegotiatedProperty("javax.security.sasl.qop")) == null || (!str5.equalsIgnoreCase("auth-int") && !str5.equalsIgnoreCase("auth-conf"))) {
                        Response[] responseArr = new Response[vector.size()];
                        vector.copyInto(responseArr);
                        this.pr.notifyResponseHandlers(responseArr);
                        this.pr.handleResult(response);
                        this.pr.setCapabilities(response);
                        return z;
                    }
                    if (this.debug) {
                        this.out.println("IMAP SASL DEBUG: Mechanism requires integrity or confidentiality");
                    }
                    return false;
                } catch (Exception e2) {
                    if (this.debug) {
                        this.out.println("IMAP SASL DEBUG: AUTHENTICATE Exception: " + e2);
                    }
                    return false;
                }
            } catch (SaslException e3) {
                if (this.debug) {
                    this.out.println("IMAP SASL DEBUG: Failed to create SASL client: " + e3);
                }
                return false;
            }
        }
    }
}
