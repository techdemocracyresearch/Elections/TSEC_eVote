package com.sun.mail.imap;

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.protocol.BODYSTRUCTURE;
import com.sun.mail.imap.protocol.ENVELOPE;
import com.sun.mail.imap.protocol.IMAPProtocol;
import javax.mail.Flags;
import javax.mail.FolderClosedException;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.MethodNotSupportedException;

public class IMAPNestedMessage extends IMAPMessage {
    private IMAPMessage msg;

    IMAPNestedMessage(IMAPMessage iMAPMessage, BODYSTRUCTURE bodystructure, ENVELOPE envelope, String str) {
        super(iMAPMessage._getSession());
        this.msg = iMAPMessage;
        this.bs = bodystructure;
        this.envelope = envelope;
        this.sectionId = str;
    }

    /* access modifiers changed from: protected */
    @Override // com.sun.mail.imap.IMAPMessage
    public IMAPProtocol getProtocol() throws ProtocolException, FolderClosedException {
        return this.msg.getProtocol();
    }

    /* access modifiers changed from: protected */
    @Override // com.sun.mail.imap.IMAPMessage
    public boolean isREV1() throws FolderClosedException {
        return this.msg.isREV1();
    }

    /* access modifiers changed from: protected */
    @Override // com.sun.mail.imap.IMAPMessage
    public Object getMessageCacheLock() {
        return this.msg.getMessageCacheLock();
    }

    /* access modifiers changed from: protected */
    @Override // com.sun.mail.imap.IMAPMessage
    public int getSequenceNumber() {
        return this.msg.getSequenceNumber();
    }

    /* access modifiers changed from: protected */
    @Override // com.sun.mail.imap.IMAPMessage
    public void checkExpunged() throws MessageRemovedException {
        this.msg.checkExpunged();
    }

    @Override // javax.mail.Message
    public boolean isExpunged() {
        return this.msg.isExpunged();
    }

    /* access modifiers changed from: protected */
    @Override // com.sun.mail.imap.IMAPMessage
    public int getFetchBlockSize() {
        return this.msg.getFetchBlockSize();
    }

    @Override // com.sun.mail.imap.IMAPMessage, javax.mail.Part, javax.mail.internet.MimeMessage
    public int getSize() throws MessagingException {
        return this.bs.size;
    }

    @Override // javax.mail.Message, com.sun.mail.imap.IMAPMessage, javax.mail.internet.MimeMessage
    public synchronized void setFlags(Flags flags, boolean z) throws MessagingException {
        throw new MethodNotSupportedException("Cannot set flags on this nested message");
    }
}
