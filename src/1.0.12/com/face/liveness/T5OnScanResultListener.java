package com.face.liveness;

import com.face.liveness.utils.T5FaceAngle;

public interface T5OnScanResultListener {
    void onScanError(int i, String str);

    void onScanResult(T5FaceAngle t5FaceAngle);
}
