package com.face.liveness;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import com.face.liveness.activity.TechCameraActivity;
import com.face.liveness.constants.Constants;
import com.face.liveness.exception.ExceptionUtility;
import com.face.liveness.model.Tech5LivenessRequestModel;
import com.face.liveness.utils.AES256Cipher;
import com.face.liveness.utils.ErrorUtils;
import com.face.liveness.utils.T5LivenessException;

public class LivenessController {
    private static LivenessController livenessController = null;
    private static Context mContext = null;
    public static String t5LanguageCode = "en";
    private static T5LivenessListener t5LivenessListener;
    private String TAG = "LivenessController";

    static {
        System.loadLibrary("t5liveness");
    }

    public static LivenessController getInstance(Context context, String str) {
        mContext = context;
        if (str == null || str.trim().length() < 5) {
            throw new T5LivenessException(ErrorUtils.invalidKeyMessage);
        }
        String isValidKey = AES256Cipher.isValidKey(str);
        if (isValidKey.equals("Success")) {
            if (livenessController == null) {
                livenessController = new LivenessController();
            }
            return livenessController;
        }
        throw new T5LivenessException(isValidKey);
    }

    public void detectLivenessFace(Tech5LivenessRequestModel tech5LivenessRequestModel, T5LivenessListener t5LivenessListener2) {
        t5LanguageCode = tech5LivenessRequestModel.getLanguageCode();
        t5LivenessListener = t5LivenessListener2;
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(mContext, "android.permission.CAMERA") != 0) {
            t5LivenessListener2.onError(ErrorUtils.camErrorCode, ErrorUtils.camErrorMessage);
        } else if (tech5LivenessRequestModel.getChallengesCount() > 5) {
            t5LivenessListener2.onError(ErrorUtils.invalidRandomCode, ErrorUtils.invalidRandomMessage);
        } else if (tech5LivenessRequestModel.getChallengeTimeoutInSec() < 3 || tech5LivenessRequestModel.getChallengeTimeoutInSec() > 10) {
            t5LivenessListener2.onError(ErrorUtils.timeoutRangeCode, ErrorUtils.timeOutRangeMessage);
        } else if (tech5LivenessRequestModel.getChallengeStartCounterInSec() < 1 || tech5LivenessRequestModel.getChallengeStartCounterInSec() > 5) {
            t5LivenessListener2.onError(ErrorUtils.startCountdownTimeRangeCode, ErrorUtils.startCountdownTimeMessage);
        } else if (tech5LivenessRequestModel.isShowTextInstructions()) {
            showDialog(tech5LivenessRequestModel);
        } else if (tech5LivenessRequestModel.isShowImageInstructions()) {
            showImageWarningPopUp(tech5LivenessRequestModel);
        } else {
            startCaptureActivity(tech5LivenessRequestModel);
        }
    }

    private void showDialog(Tech5LivenessRequestModel tech5LivenessRequestModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(false);
        builder.setMessage(R.string.instruction_message);
        builder.setPositiveButton(R.string.ok_message, new DialogInterface.OnClickListener(tech5LivenessRequestModel) {
            /* class com.face.liveness.$$Lambda$LivenessController$oFNrMY7_ywt82Tw9uNeOaxGWWQ */
            public final /* synthetic */ Tech5LivenessRequestModel f$1;

            {
                this.f$1 = r2;
            }

            public final void onClick(DialogInterface dialogInterface, int i) {
                LivenessController.this.lambda$showDialog$0$LivenessController(this.f$1, dialogInterface, i);
            }
        });
        builder.create().show();
    }

    public /* synthetic */ void lambda$showDialog$0$LivenessController(Tech5LivenessRequestModel tech5LivenessRequestModel, DialogInterface dialogInterface, int i) {
        if (tech5LivenessRequestModel.isShowImageInstructions()) {
            showImageWarningPopUp(tech5LivenessRequestModel);
        } else {
            startCaptureActivity(tech5LivenessRequestModel);
        }
    }

    private void startCaptureActivity(Tech5LivenessRequestModel tech5LivenessRequestModel) {
        Intent intent = new Intent(mContext, TechCameraActivity.class);
        intent.putExtra(Constants.RANDOM_CAPTURES_COUNT_, tech5LivenessRequestModel.getChallengesCount());
        intent.putExtra(Constants.TIME_OUT_IN_SEC, tech5LivenessRequestModel.getChallengeTimeoutInSec());
        intent.putExtra(Constants.CAPTURE_TIME_OUT_IN_SEC, 3);
        intent.putExtra(Constants.USE_BACK_CAMERA, tech5LivenessRequestModel.isUseBackCamera());
        intent.putExtra(Constants.IS_IMAGE_COMPRESSION, tech5LivenessRequestModel.isCompression());
        intent.putExtra(Constants.CHALLENGE_START_COUNT_IN_SEC, tech5LivenessRequestModel.getChallengeStartCounterInSec());
        intent.putExtra(Constants.IS_CAPTURE_BUTTON_ENABLE, tech5LivenessRequestModel.isShowSelfieCaptureButton());
        intent.addFlags(268435456);
        mContext.startActivity(intent);
    }

    public String getSDKVersion() {
        Log.i("code ", "version code " + BuildConfig.VERSION_NAME);
        return BuildConfig.VERSION_NAME;
    }

    public static T5LivenessListener getT5LivenessListener() {
        return t5LivenessListener;
    }

    public int sizeOf(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT < 12) {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
        return bitmap.getByteCount();
    }

    public void showImageWarningPopUp(Tech5LivenessRequestModel tech5LivenessRequestModel) {
        try {
            Dialog dialog = new Dialog(mContext, 16973834);
            dialog.setContentView(R.layout.user_glasses_headcover_layout);
            dialog.setCancelable(false);
            ((Button) dialog.findViewById(R.id.user_glass_headcover_Id)).setOnClickListener(new View.OnClickListener(tech5LivenessRequestModel, dialog) {
                /* class com.face.liveness.$$Lambda$LivenessController$YoFRrtcV9vAtptrhPDRSJzrG2EU */
                public final /* synthetic */ Tech5LivenessRequestModel f$1;
                public final /* synthetic */ Dialog f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void onClick(View view) {
                    LivenessController.this.lambda$showImageWarningPopUp$1$LivenessController(this.f$1, this.f$2, view);
                }
            });
            dialog.show();
        } catch (Exception e) {
            ExceptionUtility.logError(this.TAG, "setWarningPopUp", e);
            Log.i("Exception ", "Exception is " + e.getMessage());
        }
    }

    public /* synthetic */ void lambda$showImageWarningPopUp$1$LivenessController(Tech5LivenessRequestModel tech5LivenessRequestModel, Dialog dialog, View view) {
        startCaptureActivity(tech5LivenessRequestModel);
        dialog.dismiss();
    }
}
