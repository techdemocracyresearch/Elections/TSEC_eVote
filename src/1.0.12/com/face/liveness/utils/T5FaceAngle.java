package com.face.liveness.utils;

public enum T5FaceAngle {
    FACE_FOUND,
    LEFT,
    RIGHT,
    CLOCKWISE,
    ANTI_CLOCKWISE,
    STRAIGHT,
    NO_FACE_FOUND
}
