package com.face.liveness.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class BrightnessScreenHelper {
    private int brightness;
    private ContentResolver cResolver;
    Context context;
    Window window;

    public BrightnessScreenHelper(Context context2, Window window2) {
        this.context = context2;
        this.window = window2;
        initScreeBrightness();
    }

    public int initScreeBrightness() {
        this.cResolver = this.context.getContentResolver();
        try {
            if (Build.VERSION.SDK_INT < 23 || Settings.System.canWrite(this.context)) {
                Settings.System.putInt(this.cResolver, "screen_brightness_mode", 0);
                this.brightness = Settings.System.getInt(this.cResolver, "screen_brightness");
            } else {
                Intent intent = new Intent("android.settings.action.MANAGE_WRITE_SETTINGS");
                intent.setData(Uri.parse("package:" + this.context.getPackageName()));
                this.context.startActivity(intent);
            }
        } catch (Settings.SettingNotFoundException e) {
            Log.e("Error", "Cannot access system brightness");
            e.printStackTrace();
        }
        return this.brightness;
    }

    public void setScreenBrightness(int i) {
        Settings.System.putInt(this.cResolver, "screen_brightness", i);
        WindowManager.LayoutParams attributes = this.window.getAttributes();
        attributes.screenBrightness = (float) i;
        this.window.setAttributes(attributes);
    }
}
