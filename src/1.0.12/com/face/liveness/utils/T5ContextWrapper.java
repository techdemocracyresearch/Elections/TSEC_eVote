package com.face.liveness.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.os.Build;
import java.util.Locale;

public class T5ContextWrapper extends ContextWrapper {
    public T5ContextWrapper(Context context) {
        super(context);
    }

    public static ContextWrapper wrap(Context context, String str) {
        Locale locale;
        Configuration configuration = context.getResources().getConfiguration();
        if (!str.equals("")) {
            String replace = str.replace("_", "-");
            if (!replace.contains("-")) {
                locale = new Locale(replace);
            } else if (replace.split("-").length > 1) {
                locale = new Locale(replace.split("-")[0], replace.split("-")[1]);
            } else {
                locale = new Locale(replace.split("-")[0]);
            }
            Locale.setDefault(locale);
            if (Build.VERSION.SDK_INT >= 24) {
                setSystemLocale(configuration, locale);
            } else {
                setSystemLocaleLegacy(configuration, locale);
            }
            if (Build.VERSION.SDK_INT >= 17) {
                context = context.createConfigurationContext(configuration);
            } else {
                context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
            }
        }
        return new T5ContextWrapper(context);
    }

    public static Locale getSystemLocaleLegacy(Configuration configuration) {
        return configuration.locale;
    }

    public static Locale getSystemLocale(Configuration configuration) {
        return configuration.getLocales().get(0);
    }

    public static void setSystemLocaleLegacy(Configuration configuration, Locale locale) {
        configuration.locale = locale;
    }

    public static void setSystemLocale(Configuration configuration, Locale locale) {
        configuration.setLocale(locale);
    }
}
