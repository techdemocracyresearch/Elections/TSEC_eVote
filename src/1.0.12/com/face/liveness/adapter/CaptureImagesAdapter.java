package com.face.liveness.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.face.liveness.R;
import java.util.ArrayList;

public class CaptureImagesAdapter extends ArrayAdapter<Bitmap> {
    private final ArrayList<Bitmap> capturedBitmaps;
    private final Context context;
    LayoutInflater layoutInflater;

    public CaptureImagesAdapter(Context context2, ArrayList<Bitmap> arrayList) {
        super(context2, R.layout.pager_image, arrayList);
        this.context = context2;
        this.capturedBitmaps = arrayList;
        this.layoutInflater = LayoutInflater.from(context2);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = this.layoutInflater.inflate(R.layout.pager_image, viewGroup, false);
        ((ImageView) inflate.findViewById(R.id.image)).setImageBitmap(this.capturedBitmaps.get(i));
        return inflate;
    }
}
