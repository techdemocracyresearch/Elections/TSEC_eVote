package com.face.liveness.view.gif;

import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import androidx.fragment.app.FragmentTransaction;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;
import kotlin.UByte;

/* access modifiers changed from: package-private */
public class GifDecoder {
    private static final int BYTES_PER_INTEGER = 4;
    private static final int DISPOSAL_BACKGROUND = 2;
    private static final int DISPOSAL_NONE = 1;
    private static final int DISPOSAL_PREVIOUS = 3;
    private static final int DISPOSAL_UNSPECIFIED = 0;
    private static final int INITIAL_FRAME_POINTER = -1;
    static final int LOOP_FOREVER = -1;
    private static final int MAX_STACK_SIZE = 4096;
    private static final int NULL_CODE = -1;
    static final int STATUS_FORMAT_ERROR = 1;
    static final int STATUS_OK = 0;
    static final int STATUS_OPEN_ERROR = 2;
    static final int STATUS_PARTIAL_DECODE = 3;
    private static final String TAG = "GifDecoder";
    private static final int WORK_BUFFER_SIZE = 16384;
    private int[] act;
    private BitmapProvider bitmapProvider;
    private byte[] block;
    private int downsampledHeight;
    private int downsampledWidth;
    private int framePointer;
    private GifHeader header;
    private boolean isFirstFrameTransparent;
    private int loopIndex;
    private byte[] mainPixels;
    private int[] mainScratch;
    private GifHeaderParser parser;
    private final int[] pct;
    private byte[] pixelStack;
    private short[] prefix;
    private Bitmap previousImage;
    private ByteBuffer rawData;
    private int sampleSize;
    private boolean savePrevious;
    private int status;
    private byte[] suffix;
    private byte[] workBuffer;
    private int workBufferPosition;
    private int workBufferSize;

    /* access modifiers changed from: package-private */
    public interface BitmapProvider {
        Bitmap obtain(int i, int i2, Bitmap.Config config);

        byte[] obtainByteArray(int i);

        int[] obtainIntArray(int i);

        void release(Bitmap bitmap);

        void release(byte[] bArr);

        void release(int[] iArr);
    }

    GifDecoder(BitmapProvider bitmapProvider2, GifHeader gifHeader, ByteBuffer byteBuffer) {
        this(bitmapProvider2, gifHeader, byteBuffer, 1);
    }

    GifDecoder(BitmapProvider bitmapProvider2, GifHeader gifHeader, ByteBuffer byteBuffer, int i) {
        this(bitmapProvider2);
        setData(gifHeader, byteBuffer, i);
    }

    GifDecoder(BitmapProvider bitmapProvider2) {
        this.pct = new int[256];
        this.workBufferSize = 0;
        this.workBufferPosition = 0;
        this.bitmapProvider = bitmapProvider2;
        this.header = new GifHeader();
    }

    GifDecoder() {
        this(new SimpleBitmapProvider());
    }

    /* access modifiers changed from: package-private */
    public int getWidth() {
        return this.header.width;
    }

    /* access modifiers changed from: package-private */
    public int getHeight() {
        return this.header.height;
    }

    /* access modifiers changed from: package-private */
    public ByteBuffer getData() {
        return this.rawData;
    }

    /* access modifiers changed from: package-private */
    public int getStatus() {
        return this.status;
    }

    /* access modifiers changed from: package-private */
    public boolean advance() {
        if (this.header.frameCount <= 0) {
            return false;
        }
        if (this.framePointer == getFrameCount() - 1) {
            this.loopIndex++;
        }
        if (this.header.loopCount != -1 && this.loopIndex > this.header.loopCount) {
            return false;
        }
        this.framePointer = (this.framePointer + 1) % this.header.frameCount;
        return true;
    }

    /* access modifiers changed from: package-private */
    public int getDelay(int i) {
        if (i < 0 || i >= this.header.frameCount) {
            return -1;
        }
        return this.header.frames.get(i).delay;
    }

    /* access modifiers changed from: package-private */
    public int getNextDelay() {
        int i;
        if (this.header.frameCount <= 0 || (i = this.framePointer) < 0) {
            return 0;
        }
        return getDelay(i);
    }

    /* access modifiers changed from: package-private */
    public int getFrameCount() {
        return this.header.frameCount;
    }

    /* access modifiers changed from: package-private */
    public int getCurrentFrameIndex() {
        return this.framePointer;
    }

    /* access modifiers changed from: package-private */
    public boolean setFrameIndex(int i) {
        if (i < -1 || i >= getFrameCount()) {
            return false;
        }
        this.framePointer = i;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void resetFrameIndex() {
        this.framePointer = -1;
    }

    /* access modifiers changed from: package-private */
    public void resetLoopIndex() {
        this.loopIndex = 0;
    }

    /* access modifiers changed from: package-private */
    public int getLoopCount() {
        return this.header.loopCount;
    }

    /* access modifiers changed from: package-private */
    public int getLoopIndex() {
        return this.loopIndex;
    }

    /* access modifiers changed from: package-private */
    public int getByteSize() {
        return this.rawData.limit() + this.mainPixels.length + (this.mainScratch.length * 4);
    }

    /* access modifiers changed from: package-private */
    public synchronized Bitmap getNextFrame() {
        if (this.header.frameCount <= 0 || this.framePointer < 0) {
            String str = TAG;
            if (Log.isLoggable(str, 3)) {
                Log.d(str, "unable to decode frame, frameCount=" + this.header.frameCount + " framePointer=" + this.framePointer);
            }
            this.status = 1;
        }
        int i = this.status;
        if (i != 1) {
            if (i != 2) {
                this.status = 0;
                GifFrame gifFrame = this.header.frames.get(this.framePointer);
                int i2 = this.framePointer - 1;
                GifFrame gifFrame2 = i2 >= 0 ? this.header.frames.get(i2) : null;
                int[] iArr = gifFrame.lct != null ? gifFrame.lct : this.header.gct;
                this.act = iArr;
                if (iArr == null) {
                    String str2 = TAG;
                    if (Log.isLoggable(str2, 3)) {
                        Log.d(str2, "No Valid Color Table for frame #" + this.framePointer);
                    }
                    this.status = 1;
                    return null;
                }
                if (gifFrame.transparency) {
                    int[] iArr2 = this.act;
                    System.arraycopy(iArr2, 0, this.pct, 0, iArr2.length);
                    int[] iArr3 = this.pct;
                    this.act = iArr3;
                    iArr3[gifFrame.transIndex] = 0;
                }
                return setPixels(gifFrame, gifFrame2);
            }
        }
        String str3 = TAG;
        if (Log.isLoggable(str3, 3)) {
            Log.d(str3, "Unable to decode frame, status=" + this.status);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public int read(InputStream inputStream, int i) {
        if (inputStream != null) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(i > 0 ? i + 4096 : 16384);
                byte[] bArr = new byte[16384];
                while (true) {
                    int read = inputStream.read(bArr, 0, 16384);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                byteArrayOutputStream.flush();
                read(byteArrayOutputStream.toByteArray());
            } catch (IOException e) {
                Log.w(TAG, "Error reading data from stream", e);
            }
        } else {
            this.status = 2;
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
                Log.w(TAG, "Error closing stream", e2);
            }
        }
        return this.status;
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.header = null;
        byte[] bArr = this.mainPixels;
        if (bArr != null) {
            this.bitmapProvider.release(bArr);
        }
        int[] iArr = this.mainScratch;
        if (iArr != null) {
            this.bitmapProvider.release(iArr);
        }
        Bitmap bitmap = this.previousImage;
        if (bitmap != null) {
            this.bitmapProvider.release(bitmap);
        }
        this.previousImage = null;
        this.rawData = null;
        this.isFirstFrameTransparent = false;
        byte[] bArr2 = this.block;
        if (bArr2 != null) {
            this.bitmapProvider.release(bArr2);
        }
        byte[] bArr3 = this.workBuffer;
        if (bArr3 != null) {
            this.bitmapProvider.release(bArr3);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void setData(GifHeader gifHeader, byte[] bArr) {
        setData(gifHeader, ByteBuffer.wrap(bArr));
    }

    /* access modifiers changed from: package-private */
    public synchronized void setData(GifHeader gifHeader, ByteBuffer byteBuffer) {
        setData(gifHeader, byteBuffer, 1);
    }

    /* access modifiers changed from: package-private */
    public synchronized void setData(GifHeader gifHeader, ByteBuffer byteBuffer, int i) {
        if (i > 0) {
            int highestOneBit = Integer.highestOneBit(i);
            this.status = 0;
            this.header = gifHeader;
            this.isFirstFrameTransparent = false;
            this.framePointer = -1;
            resetLoopIndex();
            ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
            this.rawData = asReadOnlyBuffer;
            asReadOnlyBuffer.position(0);
            this.rawData.order(ByteOrder.LITTLE_ENDIAN);
            this.savePrevious = false;
            Iterator<GifFrame> it = gifHeader.frames.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().dispose == 3) {
                        this.savePrevious = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            this.sampleSize = highestOneBit;
            this.downsampledWidth = gifHeader.width / highestOneBit;
            this.downsampledHeight = gifHeader.height / highestOneBit;
            this.mainPixels = this.bitmapProvider.obtainByteArray(gifHeader.width * gifHeader.height);
            this.mainScratch = this.bitmapProvider.obtainIntArray(this.downsampledWidth * this.downsampledHeight);
        } else {
            throw new IllegalArgumentException("Sample size must be >=0, not: " + i);
        }
    }

    private GifHeaderParser getHeaderParser() {
        if (this.parser == null) {
            this.parser = new GifHeaderParser();
        }
        return this.parser;
    }

    /* access modifiers changed from: package-private */
    public synchronized int read(byte[] bArr) {
        GifHeader parseHeader = getHeaderParser().setData(bArr).parseHeader();
        this.header = parseHeader;
        if (bArr != null) {
            setData(parseHeader, bArr);
        }
        return this.status;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        if (r17.header.bgIndex == r18.transIndex) goto L_0x0036;
     */
    private Bitmap setPixels(GifFrame gifFrame, GifFrame gifFrame2) {
        int i;
        int i2;
        int i3;
        int[] iArr = this.mainScratch;
        int i4 = 0;
        if (gifFrame2 == null) {
            Arrays.fill(iArr, 0);
        }
        int i5 = 3;
        int i6 = 2;
        if (gifFrame2 != null && gifFrame2.dispose > 0) {
            if (gifFrame2.dispose == 2) {
                if (!gifFrame.transparency) {
                    i3 = this.header.bgColor;
                    if (gifFrame.lct != null) {
                    }
                    fillRect(iArr, gifFrame2, i3);
                } else if (this.framePointer == 0) {
                    this.isFirstFrameTransparent = true;
                }
                i3 = 0;
                fillRect(iArr, gifFrame2, i3);
            } else if (gifFrame2.dispose == 3) {
                if (this.previousImage == null) {
                    fillRect(iArr, gifFrame2, 0);
                } else {
                    int i7 = gifFrame2.ih / this.sampleSize;
                    int i8 = gifFrame2.iy / this.sampleSize;
                    int i9 = gifFrame2.iw / this.sampleSize;
                    int i10 = gifFrame2.ix / this.sampleSize;
                    int i11 = this.downsampledWidth;
                    this.previousImage.getPixels(iArr, (i8 * i11) + i10, i11, i10, i8, i9, i7);
                }
            }
        }
        decodeBitmapData(gifFrame);
        int i12 = gifFrame.ih / this.sampleSize;
        int i13 = gifFrame.iy / this.sampleSize;
        int i14 = gifFrame.iw / this.sampleSize;
        int i15 = gifFrame.ix / this.sampleSize;
        int i16 = 8;
        boolean z = this.framePointer == 0;
        int i17 = 0;
        int i18 = 1;
        while (i4 < i12) {
            if (gifFrame.interlace) {
                if (i17 >= i12) {
                    i18++;
                    if (i18 == i6) {
                        i17 = 4;
                    } else if (i18 == i5) {
                        i16 = 4;
                        i17 = 2;
                    } else if (i18 == 4) {
                        i16 = 2;
                        i17 = 1;
                    }
                }
                i = i17 + i16;
            } else {
                i = i17;
                i17 = i4;
            }
            int i19 = i17 + i13;
            if (i19 < this.downsampledHeight) {
                int i20 = this.downsampledWidth;
                int i21 = i19 * i20;
                int i22 = i21 + i15;
                int i23 = i22 + i14;
                if (i21 + i20 < i23) {
                    i23 = i21 + i20;
                }
                int i24 = this.sampleSize * i4 * gifFrame.iw;
                int i25 = ((i23 - i22) * this.sampleSize) + i24;
                int i26 = i22;
                while (i26 < i23) {
                    if (this.sampleSize == 1) {
                        i2 = this.act[this.mainPixels[i24] & UByte.MAX_VALUE];
                    } else {
                        i2 = averageColorsNear(i24, i25, gifFrame.iw);
                    }
                    if (i2 != 0) {
                        iArr[i26] = i2;
                    } else if (!this.isFirstFrameTransparent && z) {
                        this.isFirstFrameTransparent = true;
                    }
                    i24 += this.sampleSize;
                    i26++;
                    i12 = i12;
                    i13 = i13;
                }
            }
            i4++;
            i12 = i12;
            i17 = i;
            i13 = i13;
            i5 = 3;
            i6 = 2;
        }
        if (this.savePrevious && (gifFrame.dispose == 0 || gifFrame.dispose == 1)) {
            if (this.previousImage == null) {
                this.previousImage = getNextBitmap();
            }
            Bitmap bitmap = this.previousImage;
            int i27 = this.downsampledWidth;
            bitmap.setPixels(iArr, 0, i27, 0, 0, i27, this.downsampledHeight);
        }
        Bitmap nextBitmap = getNextBitmap();
        int i28 = this.downsampledWidth;
        nextBitmap.setPixels(iArr, 0, i28, 0, 0, i28, this.downsampledHeight);
        return nextBitmap;
    }

    private void fillRect(int[] iArr, GifFrame gifFrame, int i) {
        int i2 = gifFrame.ih / this.sampleSize;
        int i3 = gifFrame.iy / this.sampleSize;
        int i4 = gifFrame.iw / this.sampleSize;
        int i5 = gifFrame.ix / this.sampleSize;
        int i6 = this.downsampledWidth;
        int i7 = (i3 * i6) + i5;
        int i8 = (i2 * i6) + i7;
        while (i7 < i8) {
            int i9 = i7 + i4;
            for (int i10 = i7; i10 < i9; i10++) {
                iArr[i10] = i;
            }
            i7 += this.downsampledWidth;
        }
    }

    private int averageColorsNear(int i, int i2, int i3) {
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        for (int i9 = i; i9 < this.sampleSize + i; i9++) {
            byte[] bArr = this.mainPixels;
            if (i9 >= bArr.length || i9 >= i2) {
                break;
            }
            int i10 = this.act[bArr[i9] & UByte.MAX_VALUE];
            if (i10 != 0) {
                i4 += (i10 >> 24) & 255;
                i5 += (i10 >> 16) & 255;
                i6 += (i10 >> 8) & 255;
                i7 += i10 & 255;
                i8++;
            }
        }
        int i11 = i + i3;
        for (int i12 = i11; i12 < this.sampleSize + i11; i12++) {
            byte[] bArr2 = this.mainPixels;
            if (i12 >= bArr2.length || i12 >= i2) {
                break;
            }
            int i13 = this.act[bArr2[i12] & UByte.MAX_VALUE];
            if (i13 != 0) {
                i4 += (i13 >> 24) & 255;
                i5 += (i13 >> 16) & 255;
                i6 += (i13 >> 8) & 255;
                i7 += i13 & 255;
                i8++;
            }
        }
        if (i8 == 0) {
            return 0;
        }
        return ((i4 / i8) << 24) | ((i5 / i8) << 16) | ((i6 / i8) << 8) | (i7 / i8);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:80:0x00f7 */
    /* JADX DEBUG: Multi-variable search result rejected for r3v21, resolved type: short[] */
    /* JADX DEBUG: Multi-variable search result rejected for r2v12, resolved type: short */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x013f A[LOOP:4: B:59:0x013d->B:60:0x013f, LOOP_END] */
    private void decodeBitmapData(GifFrame gifFrame) {
        int i;
        int i2;
        this.workBufferSize = 0;
        this.workBufferPosition = 0;
        if (gifFrame != null) {
            this.rawData.position(gifFrame.bufferFrameStart);
        }
        int i3 = gifFrame == null ? this.header.width * this.header.height : gifFrame.ih * gifFrame.iw;
        byte[] bArr = this.mainPixels;
        if (bArr == null || bArr.length < i3) {
            this.mainPixels = this.bitmapProvider.obtainByteArray(i3);
        }
        if (this.prefix == null) {
            this.prefix = new short[4096];
        }
        if (this.suffix == null) {
            this.suffix = new byte[4096];
        }
        if (this.pixelStack == null) {
            this.pixelStack = new byte[FragmentTransaction.TRANSIT_FRAGMENT_OPEN];
        }
        int readByte = readByte();
        int i4 = 1;
        int i5 = 1 << readByte;
        int i6 = i5 + 1;
        int i7 = i5 + 2;
        int i8 = readByte + 1;
        int i9 = (1 << i8) - 1;
        for (int i10 = 0; i10 < i5; i10++) {
            this.prefix[i10] = 0;
            this.suffix[i10] = (byte) i10;
        }
        int i11 = -1;
        int i12 = i8;
        int i13 = i7;
        int i14 = i9;
        int i15 = 0;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        int i20 = 0;
        int i21 = -1;
        int i22 = 0;
        int i23 = 0;
        while (true) {
            if (i15 >= i3) {
                break;
            }
            if (i16 == 0) {
                i16 = readBlock();
                if (i16 <= 0) {
                    this.status = 3;
                    break;
                }
                i17 = 0;
            }
            i19 += (this.block[i17] & UByte.MAX_VALUE) << i18;
            i18 += 8;
            i17 += i4;
            i16 += i11;
            int i24 = i13;
            int i25 = i12;
            int i26 = i21;
            int i27 = i22;
            while (true) {
                if (i18 < i25) {
                    i22 = i27;
                    i13 = i24;
                    i12 = i25;
                    i21 = i26;
                    break;
                }
                short s = i19 & i14;
                i19 >>= i25;
                i18 -= i25;
                if (s == i5) {
                    i25 = i8;
                    i24 = i7;
                    i14 = i9;
                    i26 = -1;
                } else if (s > i24) {
                    i = i8;
                    this.status = 3;
                    break;
                } else {
                    i = i8;
                    if (s == i6) {
                        break;
                    } else if (i26 == -1) {
                        this.pixelStack[i23] = this.suffix[s];
                        i26 = s;
                        i27 = i26;
                        i23++;
                        i8 = i;
                    } else {
                        if (s >= i24) {
                            i2 = s;
                            this.pixelStack[i23] = (byte) i27;
                            s = i26;
                            i23++;
                        } else {
                            i2 = s;
                        }
                        while (s >= i5) {
                            this.pixelStack[i23] = this.suffix[s];
                            s = this.prefix[s];
                            i23++;
                            i5 = i5;
                        }
                        byte[] bArr2 = this.suffix;
                        int i28 = bArr2[s] & UByte.MAX_VALUE;
                        int i29 = i23 + 1;
                        byte b = (byte) i28;
                        this.pixelStack[i23] = b;
                        if (i24 < 4096) {
                            this.prefix[i24] = (short) i26;
                            bArr2[i24] = b;
                            i24++;
                            if ((i24 & i14) == 0) {
                                if (i24 < 4096) {
                                    i25++;
                                    i14 += i24;
                                }
                                i23 = i29;
                                while (i23 > 0) {
                                    i23--;
                                    this.mainPixels[i20] = this.pixelStack[i23];
                                    i15++;
                                    i20++;
                                }
                                i8 = i;
                                i5 = i5;
                                i26 = i2;
                                i6 = i6;
                                i27 = i28;
                            }
                        }
                        i23 = i29;
                        while (i23 > 0) {
                        }
                        i8 = i;
                        i5 = i5;
                        i26 = i2;
                        i6 = i6;
                        i27 = i28;
                    }
                }
            }
            i13 = i24;
            i12 = i25;
            i21 = i26;
            i8 = i;
            i22 = i27;
            i4 = 1;
            i11 = -1;
        }
        for (int i30 = i20; i30 < i3; i30++) {
            this.mainPixels[i30] = 0;
        }
    }

    private void readChunkIfNeeded() {
        if (this.workBufferSize <= this.workBufferPosition) {
            if (this.workBuffer == null) {
                this.workBuffer = this.bitmapProvider.obtainByteArray(16384);
            }
            this.workBufferPosition = 0;
            int min = Math.min(this.rawData.remaining(), 16384);
            this.workBufferSize = min;
            this.rawData.get(this.workBuffer, 0, min);
        }
    }

    private int readByte() {
        try {
            readChunkIfNeeded();
            byte[] bArr = this.workBuffer;
            int i = this.workBufferPosition;
            this.workBufferPosition = i + 1;
            return bArr[i] & UByte.MAX_VALUE;
        } catch (Exception unused) {
            this.status = 1;
            return 0;
        }
    }

    private int readBlock() {
        int readByte = readByte();
        if (readByte > 0) {
            try {
                if (this.block == null) {
                    this.block = this.bitmapProvider.obtainByteArray(255);
                }
                int i = this.workBufferSize;
                int i2 = this.workBufferPosition;
                int i3 = i - i2;
                if (i3 >= readByte) {
                    System.arraycopy(this.workBuffer, i2, this.block, 0, readByte);
                    this.workBufferPosition += readByte;
                } else if (this.rawData.remaining() + i3 >= readByte) {
                    System.arraycopy(this.workBuffer, this.workBufferPosition, this.block, 0, i3);
                    this.workBufferPosition = this.workBufferSize;
                    readChunkIfNeeded();
                    int i4 = readByte - i3;
                    System.arraycopy(this.workBuffer, 0, this.block, i3, i4);
                    this.workBufferPosition += i4;
                } else {
                    this.status = 1;
                }
            } catch (Exception e) {
                Log.w(TAG, "Error Reading Block", e);
                this.status = 1;
            }
        }
        return readByte;
    }

    private Bitmap getNextBitmap() {
        Bitmap obtain = this.bitmapProvider.obtain(this.downsampledWidth, this.downsampledHeight, this.isFirstFrameTransparent ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        setAlpha(obtain);
        return obtain;
    }

    private static void setAlpha(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 12) {
            bitmap.setHasAlpha(true);
        }
    }
}
