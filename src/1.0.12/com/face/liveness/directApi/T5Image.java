package com.face.liveness.directApi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import com.facebook.imagepipeline.common.RotationOptions;

public class T5Image {
    private Bitmap bitmap;
    private int height;
    private byte[] imageData;
    private T5ImageOrientation orientation;
    private int width;

    public T5Image(byte[] bArr, int i, int i2, T5ImageOrientation t5ImageOrientation) {
        this.imageData = bArr;
        this.width = i;
        this.height = i2;
        this.orientation = t5ImageOrientation;
    }

    public T5Image(Bitmap bitmap2) {
        this.bitmap = bitmap2;
    }

    public byte[] getImageData() {
        return this.imageData;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public T5ImageOrientation getOrientation() {
        return this.orientation;
    }

    public Bitmap getBitmap() {
        if (this.bitmap == null) {
            byte[] bArr = this.imageData;
            int i = 0;
            this.bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            Matrix matrix = new Matrix();
            int i2 = AnonymousClass1.$SwitchMap$com$face$liveness$directApi$T5ImageOrientation[this.orientation.ordinal()];
            if (i2 != 1) {
                if (i2 == 2) {
                    i = 90;
                } else if (i2 == 3) {
                    i = RotationOptions.ROTATE_180;
                } else if (i2 == 4) {
                    i = -90;
                }
            }
            matrix.postRotate((float) i);
            Bitmap bitmap2 = this.bitmap;
            this.bitmap = Bitmap.createBitmap(bitmap2, 0, 0, bitmap2.getWidth(), this.bitmap.getHeight(), matrix, true);
        }
        return this.bitmap;
    }

    /* renamed from: com.face.liveness.directApi.T5Image$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$face$liveness$directApi$T5ImageOrientation;

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        static {
            int[] iArr = new int[T5ImageOrientation.values().length];
            $SwitchMap$com$face$liveness$directApi$T5ImageOrientation = iArr;
            iArr[T5ImageOrientation.ORIENTATION_0.ordinal()] = 1;
            $SwitchMap$com$face$liveness$directApi$T5ImageOrientation[T5ImageOrientation.ORIENTATION_90.ordinal()] = 2;
            $SwitchMap$com$face$liveness$directApi$T5ImageOrientation[T5ImageOrientation.ORIENTATION_180.ordinal()] = 3;
            try {
                $SwitchMap$com$face$liveness$directApi$T5ImageOrientation[T5ImageOrientation.ORIENTATION_270.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
    }
}
