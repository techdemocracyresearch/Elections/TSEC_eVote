package com.face.liveness.constants;

public class Constants {
    public static int BACK_CAMERA_EXTRA_THRESHOLD = 3;
    public static String CALL_BACK_LISTENER_IS_NEEDED = "Call back listener need to be initialize";
    public static String CAPTURE_TIME_OUT_IN_SEC = "captureTimeOutInSec";
    public static String CHALLENGE_START_COUNT_IN_SEC = "CHALLENGE_START_COUNT_IN_SEC";
    public static int COMPRESSION_RATE = 80;
    public static int EYES_X_MAX_THRESHOLD = 25;
    public static int EYES_X_MIN_THRESHOLD = 18;
    public static int EYES_Y_MAX_THRESHOLD = 50;
    public static int EYES_Y_MIN_THRESHOLD = 40;
    public static String IS_CAPTURE_BUTTON_ENABLE = "IS_CAPTURE_BUTTON_ENABLE";
    public static boolean IS_EXCEPTION_MAIL_SUPPORT_ENABLED = false;
    public static String IS_IMAGE_COMPRESSION = "IS_IMAGE_COMPRESSION";
    public static int NO_COMPRESSION_RATE = 100;
    public static String RANDOM_CAPTURES_COUNT_ = "randomCapturesCount";
    public static int REQUIRED_HEIGHT = 1080;
    public static String TIME_OUT_IN_SEC = "timeOutInSec";
    public static String USE_BACK_CAMERA = "useBackCamera";

    private Constants() {
    }
}
