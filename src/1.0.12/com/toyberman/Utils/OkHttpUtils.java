package com.toyberman.Utils;

import android.content.Context;
import android.net.Uri;
import com.brentvatne.react.ReactVideoViewManager;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableType;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.CertificatePinner;
import okhttp3.CookieJar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONException;

public class OkHttpUtils {
    private static final String BODY_KEY = "body";
    private static final String FILE = "file";
    private static final String HEADERS_KEY = "headers";
    private static final String METHOD_KEY = "method";
    private static final HashMap<String, OkHttpClient> clientsByDomain = new HashMap<>();
    private static String content_type = "application/json; charset=utf-8";
    private static OkHttpClient defaultClient = null;
    public static MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
    private static SSLContext sslContext;

    public static OkHttpClient buildOkHttpClient(CookieJar cookieJar, String str, ReadableArray readableArray, ReadableMap readableMap) {
        HashMap<String, OkHttpClient> hashMap = clientsByDomain;
        if (!hashMap.containsKey(str)) {
            new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.cookieJar(cookieJar);
            if (!readableMap.hasKey("pkPinning") || !readableMap.getBoolean("pkPinning")) {
                builder.sslSocketFactory(sslContext.getSocketFactory(), initSSLPinning(readableArray));
            } else {
                builder.certificatePinner(initPublicKeyPinning(readableArray, str));
            }
            OkHttpClient build = builder.build();
            hashMap.put(str, build);
            return build;
        }
        OkHttpClient okHttpClient = hashMap.get(str);
        if (!readableMap.hasKey("timeoutInterval")) {
            return okHttpClient;
        }
        long j = (long) readableMap.getInt("timeoutInterval");
        return okHttpClient.newBuilder().readTimeout(j, TimeUnit.MILLISECONDS).writeTimeout(j, TimeUnit.MILLISECONDS).connectTimeout(j, TimeUnit.MILLISECONDS).build();
    }

    public static OkHttpClient buildDefaultOHttpClient(CookieJar cookieJar, String str, ReadableMap readableMap) {
        if (defaultClient == null) {
            new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.cookieJar(cookieJar);
            defaultClient = builder.build();
        }
        if (readableMap.hasKey("timeoutInterval")) {
            long j = (long) readableMap.getInt("timeoutInterval");
            defaultClient = defaultClient.newBuilder().readTimeout(j, TimeUnit.MILLISECONDS).writeTimeout(j, TimeUnit.MILLISECONDS).connectTimeout(j, TimeUnit.MILLISECONDS).build();
        }
        return defaultClient;
    }

    private static CertificatePinner initPublicKeyPinning(ReadableArray readableArray, String str) {
        CertificatePinner.Builder builder = new CertificatePinner.Builder();
        for (int i = 0; i < readableArray.size(); i++) {
            builder.add(str, readableArray.getString(i));
        }
        return builder.build();
    }

    /* JADX INFO: finally extract failed */
    private static X509TrustManager initSSLPinning(ReadableArray readableArray) {
        Exception e;
        X509TrustManager x509TrustManager = null;
        try {
            sslContext = SSLContext.getInstance("TLS");
            CertificateFactory instance = CertificateFactory.getInstance("X.509");
            KeyStore instance2 = KeyStore.getInstance(KeyStore.getDefaultType());
            instance2.load(null, null);
            for (int i = 0; i < readableArray.size(); i++) {
                String string = readableArray.getString(i);
                ClassLoader classLoader = OkHttpUtils.class.getClassLoader();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(classLoader.getResourceAsStream("assets/" + string + ".cer"));
                try {
                    Certificate generateCertificate = instance.generateCertificate(bufferedInputStream);
                    bufferedInputStream.close();
                    instance2.setCertificateEntry(string, generateCertificate);
                } catch (Throwable th) {
                    bufferedInputStream.close();
                    throw th;
                }
            }
            TrustManagerFactory instance3 = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance3.init(instance2);
            TrustManager[] trustManagers = instance3.getTrustManagers();
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
            }
            X509TrustManager x509TrustManager2 = (X509TrustManager) trustManagers[0];
            try {
                sslContext.init(null, new TrustManager[]{x509TrustManager2}, null);
                return x509TrustManager2;
            } catch (Exception e2) {
                x509TrustManager = x509TrustManager2;
                e = e2;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return x509TrustManager;
        }
    }

    private static boolean isFilePart(ReadableArray readableArray) {
        if (readableArray.getType(1) != ReadableType.Map) {
            return false;
        }
        ReadableMap map = readableArray.getMap(1);
        if (!map.hasKey(ReactVideoViewManager.PROP_SRC_TYPE) || (!map.hasKey(ReactVideoViewManager.PROP_SRC_URI) && !map.hasKey("path"))) {
            return false;
        }
        return true;
    }

    private static void addFormDataPart(Context context, MultipartBody.Builder builder, ReadableMap readableMap, String str) {
        String str2 = "";
        Uri parse = Uri.parse(str2);
        if (readableMap.hasKey(ReactVideoViewManager.PROP_SRC_URI)) {
            parse = Uri.parse(readableMap.getString(ReactVideoViewManager.PROP_SRC_URI));
        } else if (readableMap.hasKey("path")) {
            parse = Uri.parse(readableMap.getString("path"));
        }
        String string = readableMap.getString(ReactVideoViewManager.PROP_SRC_TYPE);
        if (readableMap.hasKey("fileName")) {
            str2 = readableMap.getString("fileName");
        } else if (readableMap.hasKey("name")) {
            str2 = readableMap.getString("name");
        }
        try {
            builder.addFormDataPart(str, str2, RequestBody.create(MediaType.parse(string), getTempFile(context, parse)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static RequestBody buildFormDataRequestBody(Context context, ReadableMap readableMap) {
        String str;
        MultipartBody.Builder type = new MultipartBody.Builder().setType(MultipartBody.FORM);
        type.setType(MediaType.parse("multipart/form-data"));
        if (readableMap.hasKey("_parts")) {
            ReadableArray array = readableMap.getArray("_parts");
            for (int i = 0; i < array.size(); i++) {
                ReadableArray array2 = array.getArray(i);
                if (array2.getType(0) == ReadableType.String) {
                    str = array2.getString(0);
                } else {
                    str = array2.getType(0) == ReadableType.Number ? String.valueOf(array2.getInt(0)) : "";
                }
                if (isFilePart(array2)) {
                    addFormDataPart(context, type, array2.getMap(1), str);
                } else {
                    type.addFormDataPart(str, array2.getString(1));
                }
            }
        }
        return type.build();
    }

    public static Request buildRequest(Context context, ReadableMap readableMap, String str) throws JSONException {
        RequestBody requestBody;
        Request.Builder builder = new Request.Builder();
        if (readableMap.hasKey(HEADERS_KEY)) {
            setRequestHeaders(readableMap, builder);
        }
        String string = readableMap.hasKey(METHOD_KEY) ? readableMap.getString(METHOD_KEY) : "GET";
        if (readableMap.hasKey(BODY_KEY)) {
            int i = AnonymousClass1.$SwitchMap$com$facebook$react$bridge$ReadableType[readableMap.getType(BODY_KEY).ordinal()];
            if (i == 1) {
                requestBody = RequestBody.create(mediaType, readableMap.getString(BODY_KEY));
            } else if (i == 2) {
                ReadableMap map = readableMap.getMap(BODY_KEY);
                if (map.hasKey("formData")) {
                    requestBody = buildFormDataRequestBody(context, map.getMap("formData"));
                } else if (map.hasKey("_parts")) {
                    requestBody = buildFormDataRequestBody(context, map);
                }
            }
            return builder.url(str).method(string, requestBody).build();
        }
        requestBody = null;
        return builder.url(str).method(string, requestBody).build();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.toyberman.Utils.OkHttpUtils$1  reason: invalid class name */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$facebook$react$bridge$ReadableType;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        static {
            int[] iArr = new int[ReadableType.values().length];
            $SwitchMap$com$facebook$react$bridge$ReadableType = iArr;
            iArr[ReadableType.String.ordinal()] = 1;
            $SwitchMap$com$facebook$react$bridge$ReadableType[ReadableType.Map.ordinal()] = 2;
        }
    }

    public static File getTempFile(Context context, Uri uri) throws IOException {
        File createTempFile = File.createTempFile("media", null);
        InputStream openInputStream = context.getContentResolver().openInputStream(uri);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(createTempFile));
        byte[] bArr = new byte[1024];
        while (true) {
            int read = openInputStream.read(bArr);
            if (read != -1) {
                bufferedOutputStream.write(bArr, 0, read);
            } else {
                openInputStream.close();
                bufferedOutputStream.close();
                return createTempFile;
            }
        }
    }

    private static void setRequestHeaders(ReadableMap readableMap, Request.Builder builder) {
        ReadableMap map = readableMap.getMap(HEADERS_KEY);
        Utilities.addHeadersFromMap(map, builder);
        if (map.hasKey("content-type")) {
            String string = map.getString("content-type");
            content_type = string;
            mediaType = MediaType.parse(string);
        }
    }
}
