package com.gantix.JailMonkey.ExternalStorage;

import android.content.Context;
import android.os.Build;

public class ExternalStorageCheck {
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:3:0x000b */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0, types: [android.content.Context] */
    /* JADX WARN: Type inference failed for: r4v1, types: [android.content.Context] */
    /* JADX WARN: Type inference failed for: r4v5, types: [int] */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:0|(5:2|3|4|5|(1:19)(1:7))|8|9|(1:11)(2:12|(1:16)(1:17))) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x001f */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f A[Catch:{ all -> 0x0041 }, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0030 A[Catch:{ all -> 0x0041 }] */
    public static boolean isOnExternalStorage(Context context) {
        if (Build.VERSION.SDK_INT > 7) {
            context = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.flags;
            if ((context & 262144) == 262144) {
                return true;
            }
            return false;
        }
        String absolutePath = context.getFilesDir().getAbsolutePath();
        if (!absolutePath.startsWith("/data/")) {
            return false;
        }
        return absolutePath.contains("/mnt/") || absolutePath.contains("/sdcard/");
    }
}
