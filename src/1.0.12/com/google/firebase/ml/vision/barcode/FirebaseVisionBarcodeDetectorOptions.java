package com.google.firebase.ml.vision.barcode;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.internal.firebase_ml.zztg;
import com.google.android.gms.internal.firebase_ml.zzvh;
import com.google.android.gms.internal.firebase_ml.zzwz;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionBarcodeDetectorOptions {
    private static final Map<Integer, zzvh> zzbqm;
    private final int zzbql;

    private FirebaseVisionBarcodeDetectorOptions(int i) {
        this.zzbql = i;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private int zzbqn = 0;

        public Builder setBarcodeFormats(int i, int... iArr) {
            this.zzbqn = i;
            if (iArr != null) {
                for (int i2 : iArr) {
                    this.zzbqn = i2 | this.zzbqn;
                }
            }
            return this;
        }

        public FirebaseVisionBarcodeDetectorOptions build() {
            return new FirebaseVisionBarcodeDetectorOptions(this.zzbqn);
        }
    }

    public final int zzqh() {
        return this.zzbql;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof FirebaseVisionBarcodeDetectorOptions) && this.zzbql == ((FirebaseVisionBarcodeDetectorOptions) obj).zzbql;
    }

    public int hashCode() {
        return Objects.hashCode(Integer.valueOf(this.zzbql));
    }

    public final zztg.zza zzqi() {
        ArrayList arrayList = new ArrayList();
        if (this.zzbql == 0) {
            arrayList.addAll(zzbqm.values());
        } else {
            for (Map.Entry<Integer, zzvh> entry : zzbqm.entrySet()) {
                if ((this.zzbql & entry.getKey().intValue()) != 0) {
                    arrayList.add(entry.getValue());
                }
            }
        }
        return (zztg.zza) ((zzwz) zztg.zza.zzrq().zzz(arrayList).zzvb());
    }

    static {
        HashMap hashMap = new HashMap();
        zzbqm = hashMap;
        hashMap.put(1, zzvh.CODE_128);
        hashMap.put(2, zzvh.CODE_39);
        hashMap.put(4, zzvh.CODE_93);
        hashMap.put(8, zzvh.CODABAR);
        hashMap.put(16, zzvh.DATA_MATRIX);
        hashMap.put(32, zzvh.EAN_13);
        hashMap.put(64, zzvh.EAN_8);
        hashMap.put(128, zzvh.ITF);
        hashMap.put(256, zzvh.QR_CODE);
        hashMap.put(512, zzvh.UPC_A);
        hashMap.put(1024, zzvh.UPC_E);
        hashMap.put(2048, zzvh.PDF417);
        hashMap.put(4096, zzvh.AZTEC);
    }
}
