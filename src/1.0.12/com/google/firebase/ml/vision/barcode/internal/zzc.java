package com.google.firebase.ml.vision.barcode.internal;

import android.content.Context;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.internal.firebase_ml.zzod;
import com.google.android.gms.internal.firebase_ml.zzpu;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.android.gms.internal.firebase_ml.zzqp;
import com.google.android.gms.internal.firebase_ml.zzry;
import com.google.android.gms.internal.firebase_ml.zzsa;
import com.google.android.gms.internal.firebase_ml.zzsb;
import com.google.android.gms.internal.firebase_ml.zzsf;
import com.google.android.gms.internal.firebase_ml.zzwz;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzc implements zzpu<List<FirebaseVisionBarcode>, zzsf>, zzqp {
    private static boolean zzbqs = true;
    private final Context zzbkt;
    private final zzqg zzbmd;
    private final FirebaseVisionBarcodeDetectorOptions zzbqt;
    private final zzry zzbqu = new zzry();
    private IBarcodeDetector zzbqv;
    private BarcodeDetector zzbqw;

    public zzc(zzqf zzqf, FirebaseVisionBarcodeDetectorOptions firebaseVisionBarcodeDetectorOptions) {
        Preconditions.checkNotNull(zzqf, "MlKitContext can not be null");
        Preconditions.checkNotNull(firebaseVisionBarcodeDetectorOptions, "FirebaseVisionBarcodeDetectorOptions can not be null");
        this.zzbkt = zzqf.getApplicationContext();
        this.zzbqt = firebaseVisionBarcodeDetectorOptions;
        this.zzbmd = zzqg.zza(zzqf, 1);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzpu
    public final zzqp zzoc() {
        return this;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void zzol() throws FirebaseMLException {
        if (this.zzbqv == null) {
            this.zzbqv = zzqj();
        }
        IBarcodeDetector iBarcodeDetector = this.zzbqv;
        if (iBarcodeDetector != null) {
            try {
                iBarcodeDetector.start();
            } catch (RemoteException e) {
                throw new FirebaseMLException("Failed to start barcode detector pipeline.", 14, e);
            }
        } else {
            if (this.zzbqw == null) {
                this.zzbqw = new BarcodeDetector.Builder(this.zzbkt).setBarcodeFormats(this.zzbqt.zzqh()).build();
            }
        }
    }

    private final IBarcodeDetector zzqj() throws FirebaseMLException {
        if (DynamiteModule.getLocalVersion(this.zzbkt, "com.google.firebase.ml.vision.dynamite.barcode") <= 0) {
            return null;
        }
        try {
            return zzh.asInterface(DynamiteModule.load(this.zzbkt, DynamiteModule.PREFER_LOCAL, "com.google.firebase.ml.vision.dynamite.barcode").instantiate("com.google.firebase.ml.vision.barcode.BarcodeDetectorCreator")).newBarcodeDetector(new BarcodeDetectorOptionsParcel(this.zzbqt.zzqh()));
        } catch (RemoteException | DynamiteModule.LoadingException e) {
            throw new FirebaseMLException("Failed to load barcode detector module.", 14, e);
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void release() {
        IBarcodeDetector iBarcodeDetector = this.zzbqv;
        if (iBarcodeDetector != null) {
            try {
                iBarcodeDetector.stop();
            } catch (RemoteException e) {
                Log.e("BarcodeDetectorTask", "Failed to stop barcode detector pipeline.", e);
            }
            this.zzbqv = null;
        }
        BarcodeDetector barcodeDetector = this.zzbqw;
        if (barcodeDetector != null) {
            barcodeDetector.release();
            this.zzbqw = null;
        }
        zzbqs = true;
    }

    /* access modifiers changed from: private */
    public final synchronized List<FirebaseVisionBarcode> zza(zzsf zzsf) throws FirebaseMLException {
        ArrayList arrayList;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        this.zzbqu.zzb(zzsf);
        arrayList = new ArrayList();
        if (this.zzbqv != null) {
            try {
                IObjectWrapper wrap = ObjectWrapper.wrap(zzsf.zzbrv);
                Frame.Metadata metadata = zzsf.zzbrv.getMetadata();
                for (zzd zzd : (List) ObjectWrapper.unwrap(this.zzbqv.zzb(wrap, new zzsb(metadata.getWidth(), metadata.getHeight(), metadata.getId(), metadata.getTimestampMillis(), metadata.getRotation())))) {
                    arrayList.add(new FirebaseVisionBarcode(zzd));
                }
            } catch (RemoteException e) {
                throw new FirebaseMLException("Failed to run barcode detector.", 14, e);
            }
        } else {
            BarcodeDetector barcodeDetector = this.zzbqw;
            if (barcodeDetector == null) {
                zza(zzoc.UNKNOWN_ERROR, elapsedRealtime, zzsf, null);
                throw new FirebaseMLException("Model source is unavailable. Please load the model resource first.", 14);
            } else if (barcodeDetector.isOperational()) {
                SparseArray<Barcode> detect = this.zzbqw.detect(zzsf.zzbrv);
                for (int i = 0; i < detect.size(); i++) {
                    arrayList.add(new FirebaseVisionBarcode(new zzf(detect.get(detect.keyAt(i)))));
                }
            } else {
                zza(zzoc.MODEL_NOT_DOWNLOADED, elapsedRealtime, zzsf, null);
                throw new FirebaseMLException("Waiting for the barcode detection model to be downloaded. Please wait.", 14);
            }
        }
        zza(zzoc.NO_ERROR, elapsedRealtime, zzsf, arrayList);
        zzbqs = false;
        return arrayList;
    }

    private final void zza(zzoc zzoc, long j, zzsf zzsf, List<FirebaseVisionBarcode> list) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (list != null) {
            for (FirebaseVisionBarcode firebaseVisionBarcode : list) {
                arrayList.add(firebaseVisionBarcode.zzqf());
                arrayList2.add(firebaseVisionBarcode.zzqg());
            }
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        this.zzbmd.zza(new zzb(this, elapsedRealtime, zzoc, arrayList, arrayList2, zzsf), zzod.ON_DEVICE_BARCODE_DETECT);
        zze zze = new zze(this);
        this.zzbmd.zza((zzns.zzc.zzb) ((zzwz) zzns.zzc.zzb.zzkd().zzd(zzoc).zzp(zzbqs).zzc(zzsa.zzc(zzsf)).zzb(this.zzbqt.zzqi()).zzj(arrayList).zzk(arrayList2).zzvb()), elapsedRealtime, zzod.AGGREGATED_ON_DEVICE_BARCODE_DETECTION, zze);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzns.zzad.zza zza(long j, zzoc zzoc, List list, List list2, zzsf zzsf) {
        boolean z = true;
        zzns.zzam.zzc zzi = zzns.zzam.zzmt().zzc(zzns.zzaf.zzme().zzk(j).zzk(zzoc).zzah(zzbqs).zzai(true).zzaj(true)).zzc(this.zzbqt.zzqi()).zzs(list).zzt(list2).zzi(zzsa.zzc(zzsf));
        zzns.zzad.zza zzma = zzns.zzad.zzma();
        if (this.zzbqv == null) {
            z = false;
        }
        return zzma.zzac(z).zza(zzi);
    }
}
