package com.google.firebase.ml.vision.barcode.internal;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public interface zzi extends IInterface {
    IBarcodeDetector newBarcodeDetector(BarcodeDetectorOptionsParcel barcodeDetectorOptionsParcel) throws RemoteException;
}
