package com.google.firebase.ml.vision.face;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.internal.firebase_ml.zzmb;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzwz;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionFaceDetectorOptions {
    public static final int ACCURATE = 2;
    public static final int ALL_CLASSIFICATIONS = 2;
    public static final int ALL_CONTOURS = 2;
    public static final int ALL_LANDMARKS = 2;
    public static final int FAST = 1;
    public static final int NO_CLASSIFICATIONS = 1;
    public static final int NO_CONTOURS = 1;
    public static final int NO_LANDMARKS = 1;
    private final boolean trackingEnabled;
    private final int zzbsy;
    private final int zzbsz;
    private final int zzbta;
    private final int zzbtb;
    private final float zzbtc;

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface ClassificationMode {
    }

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface ContourMode {
    }

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface LandmarkMode {
    }

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface PerformanceMode {
    }

    public int getLandmarkMode() {
        return this.zzbsy;
    }

    public int getContourMode() {
        return this.zzbsz;
    }

    public int getClassificationMode() {
        return this.zzbta;
    }

    public int getPerformanceMode() {
        return this.zzbtb;
    }

    public boolean isTrackingEnabled() {
        return this.trackingEnabled;
    }

    public float getMinFaceSize() {
        return this.zzbtc;
    }

    private FirebaseVisionFaceDetectorOptions(int i, int i2, int i3, int i4, boolean z, float f) {
        this.zzbsy = i;
        this.zzbsz = i2;
        this.zzbta = i3;
        this.zzbtb = i4;
        this.trackingEnabled = z;
        this.zzbtc = f;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private boolean trackingEnabled = false;
        private int zzbsy = 1;
        private int zzbsz = 1;
        private int zzbta = 1;
        private int zzbtb = 1;
        private float zzbtc = 0.1f;

        public Builder setLandmarkMode(int i) {
            this.zzbsy = i;
            return this;
        }

        public Builder setContourMode(int i) {
            this.zzbsz = i;
            return this;
        }

        public Builder setClassificationMode(int i) {
            this.zzbta = i;
            return this;
        }

        public Builder enableTracking() {
            this.trackingEnabled = true;
            return this;
        }

        public Builder setPerformanceMode(int i) {
            this.zzbtb = i;
            return this;
        }

        public Builder setMinFaceSize(float f) {
            this.zzbtc = f;
            return this;
        }

        public FirebaseVisionFaceDetectorOptions build() {
            return new FirebaseVisionFaceDetectorOptions(this.zzbsy, this.zzbsz, this.zzbta, this.zzbtb, this.trackingEnabled, this.zzbtc);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseVisionFaceDetectorOptions)) {
            return false;
        }
        FirebaseVisionFaceDetectorOptions firebaseVisionFaceDetectorOptions = (FirebaseVisionFaceDetectorOptions) obj;
        return Float.floatToIntBits(this.zzbtc) == Float.floatToIntBits(firebaseVisionFaceDetectorOptions.zzbtc) && this.zzbsy == firebaseVisionFaceDetectorOptions.zzbsy && this.zzbsz == firebaseVisionFaceDetectorOptions.zzbsz && this.zzbtb == firebaseVisionFaceDetectorOptions.zzbtb && this.trackingEnabled == firebaseVisionFaceDetectorOptions.trackingEnabled && this.zzbta == firebaseVisionFaceDetectorOptions.zzbta;
    }

    public int hashCode() {
        return Objects.hashCode(Integer.valueOf(Float.floatToIntBits(this.zzbtc)), Integer.valueOf(this.zzbsy), Integer.valueOf(this.zzbsz), Integer.valueOf(this.zzbtb), Boolean.valueOf(this.trackingEnabled), Integer.valueOf(this.zzbta));
    }

    public String toString() {
        return zzmb.zzaz("FaceDetectorOptions").zzb("landmarkMode", this.zzbsy).zzb("contourMode", this.zzbsz).zzb("classificationMode", this.zzbta).zzb("performanceMode", this.zzbtb).zza("trackingEnabled", this.trackingEnabled).zza("minFaceSize", this.zzbtc).toString();
    }

    public final zzns.zzac zzqs() {
        zzns.zzac.zzd zzd;
        zzns.zzac.zzb zzb;
        zzns.zzac.zze zze;
        zzns.zzac.zzc zzc;
        zzns.zzac.zza zzlx = zzns.zzac.zzlx();
        int i = this.zzbsy;
        if (i == 1) {
            zzd = zzns.zzac.zzd.NO_LANDMARKS;
        } else if (i != 2) {
            zzd = zzns.zzac.zzd.UNKNOWN_LANDMARKS;
        } else {
            zzd = zzns.zzac.zzd.ALL_LANDMARKS;
        }
        zzns.zzac.zza zza = zzlx.zza(zzd);
        int i2 = this.zzbta;
        if (i2 == 1) {
            zzb = zzns.zzac.zzb.NO_CLASSIFICATIONS;
        } else if (i2 != 2) {
            zzb = zzns.zzac.zzb.UNKNOWN_CLASSIFICATIONS;
        } else {
            zzb = zzns.zzac.zzb.ALL_CLASSIFICATIONS;
        }
        zzns.zzac.zza zza2 = zza.zza(zzb);
        int i3 = this.zzbtb;
        if (i3 == 1) {
            zze = zzns.zzac.zze.FAST;
        } else if (i3 != 2) {
            zze = zzns.zzac.zze.UNKNOWN_PERFORMANCE;
        } else {
            zze = zzns.zzac.zze.ACCURATE;
        }
        zzns.zzac.zza zza3 = zza2.zza(zze);
        int i4 = this.zzbsz;
        if (i4 == 1) {
            zzc = zzns.zzac.zzc.NO_CONTOURS;
        } else if (i4 != 2) {
            zzc = zzns.zzac.zzc.UNKNOWN_CONTOURS;
        } else {
            zzc = zzns.zzac.zzc.ALL_CONTOURS;
        }
        return (zzns.zzac) ((zzwz) zza3.zza(zzc).zzaa(isTrackingEnabled()).zzk(this.zzbtc).zzvb());
    }
}
