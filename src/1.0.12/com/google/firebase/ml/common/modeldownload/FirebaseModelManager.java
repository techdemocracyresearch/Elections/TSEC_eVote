package com.google.firebase.ml.common.modeldownload;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.FirebaseApp;
import com.google.firebase.inject.Provider;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.internal.modeldownload.RemoteModelManagerInterface;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class FirebaseModelManager {
    private final Map<Class<? extends FirebaseRemoteModel>, Provider<? extends RemoteModelManagerInterface<? extends FirebaseRemoteModel>>> zzbna = new HashMap();

    public static synchronized FirebaseModelManager getInstance() {
        FirebaseModelManager instance;
        synchronized (FirebaseModelManager.class) {
            instance = getInstance(FirebaseApp.getInstance());
        }
        return instance;
    }

    public static synchronized FirebaseModelManager getInstance(FirebaseApp firebaseApp) {
        FirebaseModelManager firebaseModelManager;
        synchronized (FirebaseModelManager.class) {
            Preconditions.checkNotNull(firebaseApp, "Please provide a valid FirebaseApp");
            firebaseModelManager = (FirebaseModelManager) firebaseApp.get(FirebaseModelManager.class);
        }
        return firebaseModelManager;
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static class RemoteModelManagerRegistration {
        private final Class<? extends FirebaseRemoteModel> zzbne;
        private final Provider<? extends RemoteModelManagerInterface<? extends FirebaseRemoteModel>> zzbnf;

        public <TRemote extends FirebaseRemoteModel> RemoteModelManagerRegistration(Class<TRemote> cls, Provider<? extends RemoteModelManagerInterface<TRemote>> provider) {
            this.zzbne = cls;
            this.zzbnf = provider;
        }

        /* access modifiers changed from: package-private */
        public final Class<? extends FirebaseRemoteModel> zzpp() {
            return this.zzbne;
        }

        /* access modifiers changed from: package-private */
        public final Provider<? extends RemoteModelManagerInterface<? extends FirebaseRemoteModel>> zzpq() {
            return this.zzbnf;
        }
    }

    public FirebaseModelManager(Set<RemoteModelManagerRegistration> set) {
        for (RemoteModelManagerRegistration remoteModelManagerRegistration : set) {
            this.zzbna.put(remoteModelManagerRegistration.zzpp(), remoteModelManagerRegistration.zzpq());
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.google.firebase.ml.common.modeldownload.FirebaseModelManager */
    /* JADX WARN: Multi-variable type inference failed */
    public Task<Void> download(FirebaseRemoteModel firebaseRemoteModel, FirebaseModelDownloadConditions firebaseModelDownloadConditions) {
        Preconditions.checkNotNull(firebaseRemoteModel, "FirebaseRemoteModel cannot be null");
        Preconditions.checkNotNull(firebaseModelDownloadConditions, "FirebaseModelDownloadConditions cannot be null");
        if (this.zzbna.containsKey(firebaseRemoteModel.getClass())) {
            return zzg(firebaseRemoteModel.getClass()).download(firebaseRemoteModel, firebaseModelDownloadConditions);
        }
        return Tasks.forException(new FirebaseMLException("Feature model doesn't have a corresponding modelmanager registered.", 13));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.google.firebase.ml.common.modeldownload.FirebaseModelManager */
    /* JADX WARN: Multi-variable type inference failed */
    public Task<Void> deleteDownloadedModel(FirebaseRemoteModel firebaseRemoteModel) {
        Preconditions.checkNotNull(firebaseRemoteModel, "FirebaseRemoteModel cannot be null");
        return zzg(firebaseRemoteModel.getClass()).deleteDownloadedModel(firebaseRemoteModel);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.google.firebase.ml.common.modeldownload.FirebaseModelManager */
    /* JADX WARN: Multi-variable type inference failed */
    public Task<Boolean> isModelDownloaded(FirebaseRemoteModel firebaseRemoteModel) {
        Preconditions.checkNotNull(firebaseRemoteModel, "FirebaseRemoteModel cannot be null");
        return zzg(firebaseRemoteModel.getClass()).isModelDownloaded(firebaseRemoteModel);
    }

    public <T extends FirebaseRemoteModel> Task<Set<T>> getDownloadedModels(Class<T> cls) {
        return ((RemoteModelManagerInterface) this.zzbna.get(cls).get()).getDownloadedModels();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.google.firebase.ml.common.modeldownload.FirebaseModelManager */
    /* JADX WARN: Multi-variable type inference failed */
    public Task<File> getLatestModelFile(FirebaseRemoteModel firebaseRemoteModel) {
        Preconditions.checkNotNull(firebaseRemoteModel, "FirebaseRemoteModel cannot be null");
        if (this.zzbna.containsKey(firebaseRemoteModel.getClass())) {
            return zzg(firebaseRemoteModel.getClass()).getLatestModelFile(firebaseRemoteModel);
        }
        return Tasks.forException(new FirebaseMLException("Feature model doesn't have a corresponding modelmanager registered.", 13));
    }

    private final RemoteModelManagerInterface<FirebaseRemoteModel> zzg(Class<? extends FirebaseRemoteModel> cls) {
        return (RemoteModelManagerInterface) this.zzbna.get(cls).get();
    }
}
