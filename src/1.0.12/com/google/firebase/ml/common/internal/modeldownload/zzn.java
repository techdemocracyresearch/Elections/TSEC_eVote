package com.google.firebase.ml.common.internal.modeldownload;

import com.google.android.gms.internal.firebase_ml.zzns;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public enum zzn {
    UNKNOWN,
    BASE,
    AUTOML,
    CUSTOM,
    TRANSLATE;

    public final zzns.zzaj.zzb zzow() {
        int i = zzq.zzbkv[ordinal()];
        if (i == 1) {
            return zzns.zzaj.zzb.BASE_TRANSLATE;
        }
        if (i == 2) {
            return zzns.zzaj.zzb.CUSTOM;
        }
        if (i != 3) {
            return zzns.zzaj.zzb.TYPE_UNKNOWN;
        }
        return zzns.zzaj.zzb.AUTOML_IMAGE_LABELING;
    }
}
