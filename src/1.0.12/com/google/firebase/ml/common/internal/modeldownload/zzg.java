package com.google.firebase.ml.common.internal.modeldownload;

import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.firebase.ml.common.FirebaseMLException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzg {
    private final zzi zzbko;

    public zzg(zzqf zzqf) {
        this.zzbko = new zzi(zzqf);
    }

    public final void zza(String str, zzn zzn) throws FirebaseMLException {
        this.zzbko.zza(str, zzn);
    }

    public final boolean zza(zzaa zzaa) throws FirebaseMLException {
        return this.zzbko.zzb(zzaa.zzpl(), zzaa.zzpm());
    }

    public final boolean zzb(String str, zzn zzn) throws FirebaseMLException {
        return this.zzbko.zzb(str, zzn);
    }
}
