package com.google.android.gms.base;

public final class R {

    public static final class attr {
        public static final int buttonSize = 2130903146;
        public static final int circleCrop = 2130903187;
        public static final int colorScheme = 2130903218;
        public static final int imageAspectRatio = 2130903370;
        public static final int imageAspectRatioAdjust = 2130903371;
        public static final int scopeUris = 2130903527;

        private attr() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131034167;
        public static final int common_google_signin_btn_text_dark_default = 2131034168;
        public static final int common_google_signin_btn_text_dark_disabled = 2131034169;
        public static final int common_google_signin_btn_text_dark_focused = 2131034170;
        public static final int common_google_signin_btn_text_dark_pressed = 2131034171;
        public static final int common_google_signin_btn_text_light = 2131034172;
        public static final int common_google_signin_btn_text_light_default = 2131034173;
        public static final int common_google_signin_btn_text_light_disabled = 2131034174;
        public static final int common_google_signin_btn_text_light_focused = 2131034175;
        public static final int common_google_signin_btn_text_light_pressed = 2131034176;
        public static final int common_google_signin_btn_tint = 2131034177;

        private color() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131165288;
        public static final int common_google_signin_btn_icon_dark = 2131165289;
        public static final int common_google_signin_btn_icon_dark_focused = 2131165290;
        public static final int common_google_signin_btn_icon_dark_normal = 2131165291;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131165292;
        public static final int common_google_signin_btn_icon_disabled = 2131165293;
        public static final int common_google_signin_btn_icon_light = 2131165294;
        public static final int common_google_signin_btn_icon_light_focused = 2131165295;
        public static final int common_google_signin_btn_icon_light_normal = 2131165296;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131165297;
        public static final int common_google_signin_btn_text_dark = 2131165298;
        public static final int common_google_signin_btn_text_dark_focused = 2131165299;
        public static final int common_google_signin_btn_text_dark_normal = 2131165300;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131165301;
        public static final int common_google_signin_btn_text_disabled = 2131165302;
        public static final int common_google_signin_btn_text_light = 2131165303;
        public static final int common_google_signin_btn_text_light_focused = 2131165304;
        public static final int common_google_signin_btn_text_light_normal = 2131165305;
        public static final int common_google_signin_btn_text_light_normal_background = 2131165306;
        public static final int googleg_disabled_color_18 = 2131165314;
        public static final int googleg_standard_color_18 = 2131165315;

        private drawable() {
        }
    }

    public static final class id {
        public static final int adjust_height = 2131230789;
        public static final int adjust_width = 2131230790;
        public static final int auto = 2131230795;
        public static final int dark = 2131230834;
        public static final int icon_only = 2131230883;
        public static final int light = 2131230909;
        public static final int none = 2131230952;
        public static final int standard = 2131231036;
        public static final int wide = 2131231096;

        private id() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131689543;
        public static final int common_google_play_services_enable_text = 2131689544;
        public static final int common_google_play_services_enable_title = 2131689545;
        public static final int common_google_play_services_install_button = 2131689546;
        public static final int common_google_play_services_install_text = 2131689547;
        public static final int common_google_play_services_install_title = 2131689548;
        public static final int common_google_play_services_notification_channel_name = 2131689549;
        public static final int common_google_play_services_notification_ticker = 2131689550;
        public static final int common_google_play_services_unsupported_text = 2131689552;
        public static final int common_google_play_services_update_button = 2131689553;
        public static final int common_google_play_services_update_text = 2131689554;
        public static final int common_google_play_services_update_title = 2131689555;
        public static final int common_google_play_services_updating_text = 2131689556;
        public static final int common_google_play_services_wear_update_text = 2131689557;
        public static final int common_open_on_phone = 2131689558;
        public static final int common_signin_button_text = 2131689559;
        public static final int common_signin_button_text_long = 2131689560;

        private string() {
        }
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {com.naavote.R.attr.circleCrop, com.naavote.R.attr.imageAspectRatio, com.naavote.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.naavote.R.attr.buttonSize, com.naavote.R.attr.colorScheme, com.naavote.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }

    private R() {
    }
}
