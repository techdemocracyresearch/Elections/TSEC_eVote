package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzhf {
    String content;
    String message;
    int statusCode;
    zzgx zzack;
    String zznl;

    public zzhf(int i, String str, zzgx zzgx) {
        zzml.checkArgument(i >= 0);
        this.statusCode = i;
        this.zznl = str;
        this.zzack = (zzgx) zzml.checkNotNull(zzgx);
    }

    public zzhf(zzhd zzhd) {
        this(zzhd.getStatusCode(), zzhd.getStatusMessage(), zzhd.zzga());
        try {
            String zzgh = zzhd.zzgh();
            this.content = zzgh;
            if (zzgh.length() == 0) {
                this.content = null;
            }
        } catch (IOException e) {
            zzne.zzb(e);
        }
        StringBuilder zzc = zzhg.zzc(zzhd);
        if (this.content != null) {
            zzc.append(zzjt.zzaig);
            zzc.append(this.content);
        }
        this.message = zzc.toString();
    }

    public final zzhf zzah(String str) {
        this.message = str;
        return this;
    }

    public final zzhf zzai(String str) {
        this.content = str;
        return this;
    }
}
