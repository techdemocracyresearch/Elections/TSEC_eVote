package com.google.android.gms.internal.firebase_ml;

import java.util.concurrent.Executor;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public enum zzpz implements Executor {
    INSTANCE;

    public final void execute(Runnable runnable) {
        zzpx.zzof().handler.post(runnable);
    }
}
