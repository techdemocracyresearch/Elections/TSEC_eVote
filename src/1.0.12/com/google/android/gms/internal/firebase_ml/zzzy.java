package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzzy extends zzzw<zzzz, zzzz> {
    zzzy() {
    }

    private static void zza(Object obj, zzzz zzzz) {
        ((zzwz) obj).zzclj = zzzz;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final void zzq(Object obj) {
        ((zzwz) obj).zzclj.zztm();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ int zzaa(zzzz zzzz) {
        return zzzz.zzuo();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ int zzaf(zzzz zzzz) {
        return zzzz.zzxb();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ zzzz zzi(zzzz zzzz, zzzz zzzz2) {
        zzzz zzzz3 = zzzz;
        zzzz zzzz4 = zzzz2;
        if (zzzz4.equals(zzzz.zzwz())) {
            return zzzz3;
        }
        return zzzz.zza(zzzz3, zzzz4);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.google.android.gms.internal.firebase_ml.zzaat] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ void zzc(zzzz zzzz, zzaat zzaat) throws IOException {
        zzzz.zza(zzaat);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.google.android.gms.internal.firebase_ml.zzaat] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ void zza(zzzz zzzz, zzaat zzaat) throws IOException {
        zzzz.zzb(zzaat);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ void zzh(Object obj, zzzz zzzz) {
        zza(obj, zzzz);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ zzzz zzae(Object obj) {
        return ((zzwz) obj).zzclj;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ void zzg(Object obj, zzzz zzzz) {
        zza(obj, zzzz);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ zzzz zzwy() {
        return zzzz.zzxa();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, com.google.android.gms.internal.firebase_ml.zzvv] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ void zza(zzzz zzzz, int i, zzvv zzvv) {
        zzzz.zzb((i << 3) | 2, zzvv);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, long] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzzw
    public final /* synthetic */ void zza(zzzz zzzz, int i, long j) {
        zzzz.zzb(i << 3, Long.valueOf(j));
    }
}
