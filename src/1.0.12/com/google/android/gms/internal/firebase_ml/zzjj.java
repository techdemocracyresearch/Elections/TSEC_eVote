package com.google.android.gms.internal.firebase_ml;

import com.bumptech.glide.load.Key;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzjj extends ByteArrayOutputStream {
    private boolean closed;
    private final Logger logger;
    private int zzaic;
    private final int zzaid;
    private final Level zzaie;

    public zzjj(Logger logger2, Level level, int i) {
        this.logger = (Logger) zzml.checkNotNull(logger2);
        this.zzaie = (Level) zzml.checkNotNull(level);
        zzml.checkArgument(i >= 0);
        this.zzaid = i;
    }

    @Override // java.io.OutputStream, java.io.ByteArrayOutputStream
    public final synchronized void write(int i) {
        zzml.checkArgument(!this.closed);
        this.zzaic++;
        if (this.count < this.zzaid) {
            super.write(i);
        }
    }

    @Override // java.io.OutputStream
    public final synchronized void write(byte[] bArr, int i, int i2) {
        zzml.checkArgument(!this.closed);
        this.zzaic += i2;
        if (this.count < this.zzaid) {
            int i3 = this.count + i2;
            int i4 = this.zzaid;
            if (i3 > i4) {
                i2 += i4 - i3;
            }
            super.write(bArr, i, i2);
        }
    }

    @Override // java.io.OutputStream, java.io.ByteArrayOutputStream, java.io.Closeable, java.lang.AutoCloseable
    public final synchronized void close() throws IOException {
        if (!this.closed) {
            if (this.zzaic != 0) {
                StringBuilder sb = new StringBuilder("Total: ");
                zza(sb, this.zzaic);
                if (this.count != 0 && this.count < this.zzaic) {
                    sb.append(" (logging first ");
                    zza(sb, this.count);
                    sb.append(")");
                }
                this.logger.logp(Level.CONFIG, "com.google.api.client.util.LoggingByteArrayOutputStream", "close", sb.toString());
                if (this.count != 0) {
                    this.logger.logp(this.zzaie, "com.google.api.client.util.LoggingByteArrayOutputStream", "close", toString(Key.STRING_CHARSET_NAME).replaceAll("[\\x00-\\x09\\x0B\\x0C\\x0E-\\x1F\\x7F]", " "));
                }
            }
            this.closed = true;
        }
    }

    private static void zza(StringBuilder sb, int i) {
        if (i == 1) {
            sb.append("1 byte");
            return;
        }
        sb.append(NumberFormat.getInstance().format((long) i));
        sb.append(" bytes");
    }
}
