package com.google.android.gms.internal.firebase_ml;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzix {
    private static final Boolean zzagr;
    private static final String zzags;
    private static final Character zzagt;
    private static final Byte zzagu;
    private static final Short zzagv;
    private static final Integer zzagw;
    private static final Float zzagx;
    private static final Long zzagy;
    private static final Double zzagz;
    private static final BigInteger zzaha;
    private static final BigDecimal zzahb;
    private static final zzje zzahc;
    private static final ConcurrentHashMap<Class<?>, Object> zzahd;

    public static <T> T zzd(Class<?> cls) {
        T t;
        T t2 = (T) zzahd.get(cls);
        if (t2 != null) {
            return t2;
        }
        int i = 0;
        if (cls.isArray()) {
            Class<?> cls2 = cls;
            do {
                cls2 = cls2.getComponentType();
                i++;
            } while (cls2.isArray());
            t = (T) Array.newInstance(cls2, new int[i]);
        } else if (cls.isEnum()) {
            zzjd zzao = zziv.zzc(cls).zzao(null);
            Object[] objArr = {cls};
            if (zzao != null) {
                t = (T) zzao.zzic();
            } else {
                throw new NullPointerException(zzms.zzb("enum missing constant with @NullValue annotation: %s", objArr));
            }
        } else {
            t = (T) zzjs.zzf((Class) cls);
        }
        T t3 = (T) zzahd.putIfAbsent(cls, t);
        return t3 == null ? t : t3;
    }

    public static boolean isNull(Object obj) {
        return obj != null && obj == zzahd.get(obj.getClass());
    }

    public static Map<String, Object> zzf(Object obj) {
        if (obj == null || isNull(obj)) {
            return Collections.emptyMap();
        }
        if (obj instanceof Map) {
            return (Map) obj;
        }
        return new zzja(obj, false);
    }

    public static <T> T clone(T t) {
        T t2;
        if (t == null || zza(t.getClass())) {
            return t;
        }
        if (t instanceof zzjf) {
            return (T) ((zzjf) t.clone());
        }
        Class<?> cls = t.getClass();
        if (cls.isArray()) {
            t2 = (T) Array.newInstance(cls.getComponentType(), Array.getLength(t));
        } else if (t instanceof zziq) {
            t2 = (T) ((zziq) t.clone());
        } else if ("java.util.Arrays$ArrayList".equals(cls.getName())) {
            Object[] array = t.toArray();
            zza(array, array);
            return (T) Arrays.asList(array);
        } else {
            t2 = (T) zzjs.zzf((Class) cls);
        }
        zza((Object) t, (Object) t2);
        return t2;
    }

    public static void zza(Object obj, Object obj2) {
        Object zzh;
        Class<?> cls = obj.getClass();
        boolean z = true;
        int i = 0;
        zzml.checkArgument(cls == obj2.getClass());
        if (cls.isArray()) {
            if (Array.getLength(obj) != Array.getLength(obj2)) {
                z = false;
            }
            zzml.checkArgument(z);
            for (Object obj3 : zzjs.zzi(obj)) {
                Array.set(obj2, i, clone(obj3));
                i++;
            }
        } else if (Collection.class.isAssignableFrom(cls)) {
            Collection<Object> collection = (Collection) obj;
            if (ArrayList.class.isAssignableFrom(cls)) {
                ((ArrayList) obj2).ensureCapacity(collection.size());
            }
            Collection collection2 = (Collection) obj2;
            for (Object obj4 : collection) {
                collection2.add(clone(obj4));
            }
        } else {
            boolean isAssignableFrom = zzjf.class.isAssignableFrom(cls);
            if (isAssignableFrom || !Map.class.isAssignableFrom(cls)) {
                zziv zzc = isAssignableFrom ? ((zzjf) obj).zzacg : zziv.zzc(cls);
                for (String str : zzc.zzagq) {
                    zzjd zzao = zzc.zzao(str);
                    if (!zzao.zzib() && ((!isAssignableFrom || !zzao.isPrimitive()) && (zzh = zzao.zzh(obj)) != null)) {
                        zzao.zzb(obj2, clone(zzh));
                    }
                }
            } else if (zziq.class.isAssignableFrom(cls)) {
                zziq zziq = (zziq) obj2;
                zziq zziq2 = (zziq) obj;
                int size = zziq2.size();
                while (i < size) {
                    zziq.set(i, clone(zziq2.zzaj(i)));
                    i++;
                }
            } else {
                Map map = (Map) obj2;
                for (Map.Entry entry : ((Map) obj).entrySet()) {
                    map.put((String) entry.getKey(), clone(entry.getValue()));
                }
            }
        }
    }

    public static boolean zza(Type type) {
        if (type instanceof WildcardType) {
            type = zzjs.zza((WildcardType) type);
        }
        if (!(type instanceof Class)) {
            return false;
        }
        Class cls = (Class) type;
        if (cls.isPrimitive() || cls == Character.class || cls == String.class || cls == Integer.class || cls == Long.class || cls == Short.class || cls == Byte.class || cls == Float.class || cls == Double.class || cls == BigInteger.class || cls == BigDecimal.class || cls == zzje.class || cls == Boolean.class) {
            return true;
        }
        return false;
    }

    public static boolean zzg(Object obj) {
        return obj == null || zza(obj.getClass());
    }

    public static Object zza(Type type, String str) {
        Class cls = type instanceof Class ? (Class) type : null;
        if (type == null || cls != null) {
            if (cls == Void.class) {
                return null;
            }
            if (str == null || cls == null || cls.isAssignableFrom(String.class)) {
                return str;
            }
            if (cls == Character.class || cls == Character.TYPE) {
                if (str.length() == 1) {
                    return Character.valueOf(str.charAt(0));
                }
                String valueOf = String.valueOf(cls);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 37);
                sb.append("expected type Character/char but got ");
                sb.append(valueOf);
                throw new IllegalArgumentException(sb.toString());
            } else if (cls == Boolean.class || cls == Boolean.TYPE) {
                return Boolean.valueOf(str);
            } else {
                if (cls == Byte.class || cls == Byte.TYPE) {
                    return Byte.valueOf(str);
                }
                if (cls == Short.class || cls == Short.TYPE) {
                    return Short.valueOf(str);
                }
                if (cls == Integer.class || cls == Integer.TYPE) {
                    return Integer.valueOf(str);
                }
                if (cls == Long.class || cls == Long.TYPE) {
                    return Long.valueOf(str);
                }
                if (cls == Float.class || cls == Float.TYPE) {
                    return Float.valueOf(str);
                }
                if (cls == Double.class || cls == Double.TYPE) {
                    return Double.valueOf(str);
                }
                if (cls == zzje.class) {
                    return zzje.zzap(str);
                }
                if (cls == BigInteger.class) {
                    return new BigInteger(str);
                }
                if (cls == BigDecimal.class) {
                    return new BigDecimal(str);
                }
                if (cls.isEnum()) {
                    if (zziv.zzc(cls).zzagq.contains(str)) {
                        return zziv.zzc(cls).zzao(str).zzic();
                    }
                    throw new IllegalArgumentException(String.format("given enum name %s not part of enumeration", str));
                }
            }
        }
        String valueOf2 = String.valueOf(type);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 35);
        sb2.append("expected primitive class, but got: ");
        sb2.append(valueOf2);
        throw new IllegalArgumentException(sb2.toString());
    }

    public static Collection<Object> zzb(Type type) {
        if (type instanceof WildcardType) {
            type = zzjs.zza((WildcardType) type);
        }
        if (type instanceof ParameterizedType) {
            type = ((ParameterizedType) type).getRawType();
        }
        Class cls = type instanceof Class ? (Class) type : null;
        if (type == null || (type instanceof GenericArrayType) || (cls != null && (cls.isArray() || cls.isAssignableFrom(ArrayList.class)))) {
            return new ArrayList();
        }
        if (cls == null) {
            String valueOf = String.valueOf(type);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39);
            sb.append("unable to create new instance of type: ");
            sb.append(valueOf);
            throw new IllegalArgumentException(sb.toString());
        } else if (cls.isAssignableFrom(HashSet.class)) {
            return new HashSet();
        } else {
            if (cls.isAssignableFrom(TreeSet.class)) {
                return new TreeSet();
            }
            return (Collection) zzjs.zzf(cls);
        }
    }

    public static Map<String, Object> zze(Class<?> cls) {
        if (cls == null || cls.isAssignableFrom(zziq.class)) {
            return new zziq();
        }
        if (cls.isAssignableFrom(TreeMap.class)) {
            return new TreeMap();
        }
        return (Map) zzjs.zzf((Class) cls);
    }

    public static Type zza(List<Type> list, Type type) {
        if (type instanceof WildcardType) {
            type = zzjs.zza((WildcardType) type);
        }
        while (type instanceof TypeVariable) {
            Type zza = zzjs.zza(list, (TypeVariable) type);
            if (zza != null) {
                type = zza;
            }
            if (type instanceof TypeVariable) {
                type = ((TypeVariable) type).getBounds()[0];
            }
        }
        return type;
    }

    static {
        Boolean bool = new Boolean(true);
        zzagr = bool;
        String str = new String();
        zzags = str;
        Character ch = new Character(0);
        zzagt = ch;
        Byte b = new Byte((byte) 0);
        zzagu = b;
        Short sh = new Short((short) 0);
        zzagv = sh;
        Integer num = new Integer(0);
        zzagw = num;
        Float f = new Float(0.0f);
        zzagx = f;
        Long l = new Long(0);
        zzagy = l;
        Double d = new Double(0.0d);
        zzagz = d;
        BigInteger bigInteger = new BigInteger("0");
        zzaha = bigInteger;
        BigDecimal bigDecimal = new BigDecimal("0");
        zzahb = bigDecimal;
        zzje zzje = new zzje(0);
        zzahc = zzje;
        ConcurrentHashMap<Class<?>, Object> concurrentHashMap = new ConcurrentHashMap<>();
        zzahd = concurrentHashMap;
        concurrentHashMap.put(Boolean.class, bool);
        concurrentHashMap.put(String.class, str);
        concurrentHashMap.put(Character.class, ch);
        concurrentHashMap.put(Byte.class, b);
        concurrentHashMap.put(Short.class, sh);
        concurrentHashMap.put(Integer.class, num);
        concurrentHashMap.put(Float.class, f);
        concurrentHashMap.put(Long.class, l);
        concurrentHashMap.put(Double.class, d);
        concurrentHashMap.put(BigInteger.class, bigInteger);
        concurrentHashMap.put(BigDecimal.class, bigDecimal);
        concurrentHashMap.put(zzje.class, zzje);
    }
}
