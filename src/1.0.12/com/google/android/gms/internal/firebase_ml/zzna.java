package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzna<E> extends zzmw<E> {
    static final zzmw<Object> zzalt = new zzna(new Object[0], 0);
    private final transient int size;
    private final transient Object[] zzalu;

    zzna(Object[] objArr, int i) {
        this.zzalu = objArr;
        this.size = i;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmx
    public final int zzjm() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmx
    public final boolean zzjo() {
        return false;
    }

    public final int size() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmx
    public final Object[] zzjl() {
        return this.zzalu;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmx
    public final int zzjn() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmw, com.google.android.gms.internal.firebase_ml.zzmx
    public final int zza(Object[] objArr, int i) {
        System.arraycopy(this.zzalu, 0, objArr, 0, this.size);
        return this.size + 0;
    }

    @Override // java.util.List
    public final E get(int i) {
        zzml.zzb(i, this.size);
        return (E) this.zzalu[i];
    }
}
