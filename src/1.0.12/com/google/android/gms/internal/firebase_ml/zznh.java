package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zznh {
    private static final Throwable[] zzalz = new Throwable[0];

    zznh() {
    }

    public abstract void zza(Throwable th, Throwable th2);

    public abstract void zzb(Throwable th);
}
