package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzzn {
    private static final Iterator<Object> zzcpv = new zzzm();
    private static final Iterable<Object> zzcpw = new zzzp();

    static <T> Iterable<T> zzww() {
        return (Iterable<T>) zzcpw;
    }
}
