package com.google.android.gms.internal.firebase_ml;

import com.facebook.react.views.textinput.ReactEditTextInputConnectionWrapper;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;
import java.util.Objects;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzte implements Closeable, Flushable {
    private static final String[] zzbwe = new String[128];
    private static final String[] zzbwf;
    private final Writer out;
    private String separator;
    private boolean zzbvg;
    private int[] zzbvo = new int[32];
    private int zzbvp = 0;
    private String zzbwg;
    private String zzbwh;
    private boolean zzbwi;

    public zzte(Writer writer) {
        zzcb(6);
        this.separator = ":";
        this.zzbwi = true;
        this.out = writer;
    }

    public final void setIndent(String str) {
        if (str.length() == 0) {
            this.zzbwg = null;
            this.separator = ":";
            return;
        }
        this.zzbwg = str;
        this.separator = ": ";
    }

    public final void setLenient(boolean z) {
        this.zzbvg = true;
    }

    public final zzte zzri() throws IOException {
        zzrm();
        return zza(1, "[");
    }

    public final zzte zzrj() throws IOException {
        return zzc(1, 2, "]");
    }

    public final zzte zzrk() throws IOException {
        zzrm();
        return zza(3, "{");
    }

    public final zzte zzrl() throws IOException {
        return zzc(3, 5, "}");
    }

    private final zzte zza(int i, String str) throws IOException {
        zzrp();
        zzcb(i);
        this.out.write(str);
        return this;
    }

    private final zzte zzc(int i, int i2, String str) throws IOException {
        int peek = peek();
        if (peek != i2 && peek != i) {
            throw new IllegalStateException("Nesting problem.");
        } else if (this.zzbwh == null) {
            this.zzbvp--;
            if (peek == i2) {
                zzro();
            }
            this.out.write(str);
            return this;
        } else {
            throw new IllegalStateException("Dangling name: " + this.zzbwh);
        }
    }

    private final void zzcb(int i) {
        int i2 = this.zzbvp;
        int[] iArr = this.zzbvo;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[(i2 << 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.zzbvo = iArr2;
        }
        int[] iArr3 = this.zzbvo;
        int i3 = this.zzbvp;
        this.zzbvp = i3 + 1;
        iArr3[i3] = i;
    }

    private final int peek() {
        int i = this.zzbvp;
        if (i != 0) {
            return this.zzbvo[i - 1];
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    private final void zzcd(int i) {
        this.zzbvo[this.zzbvp - 1] = i;
    }

    public final zzte zzcf(String str) throws IOException {
        Objects.requireNonNull(str, "name == null");
        if (this.zzbwh != null) {
            throw new IllegalStateException();
        } else if (this.zzbvp != 0) {
            this.zzbwh = str;
            return this;
        } else {
            throw new IllegalStateException("JsonWriter is closed.");
        }
    }

    private final void zzrm() throws IOException {
        if (this.zzbwh != null) {
            int peek = peek();
            if (peek == 5) {
                this.out.write(44);
            } else if (peek != 3) {
                throw new IllegalStateException("Nesting problem.");
            }
            zzro();
            zzcd(4);
            zzch(this.zzbwh);
            this.zzbwh = null;
        }
    }

    public final zzte zzcg(String str) throws IOException {
        if (str == null) {
            return zzrn();
        }
        zzrm();
        zzrp();
        zzch(str);
        return this;
    }

    public final zzte zzrn() throws IOException {
        if (this.zzbwh != null) {
            if (this.zzbwi) {
                zzrm();
            } else {
                this.zzbwh = null;
                return this;
            }
        }
        zzrp();
        this.out.write("null");
        return this;
    }

    public final zzte zzaw(boolean z) throws IOException {
        zzrm();
        zzrp();
        this.out.write(z ? "true" : "false");
        return this;
    }

    public final zzte zzb(double d) throws IOException {
        zzrm();
        if (this.zzbvg || (!Double.isNaN(d) && !Double.isInfinite(d))) {
            zzrp();
            this.out.append((CharSequence) Double.toString(d));
            return this;
        }
        throw new IllegalArgumentException("Numeric values must be finite, but was " + d);
    }

    public final zzte zzu(long j) throws IOException {
        zzrm();
        zzrp();
        this.out.write(Long.toString(j));
        return this;
    }

    public final zzte zza(Number number) throws IOException {
        if (number == null) {
            return zzrn();
        }
        zzrm();
        String obj = number.toString();
        if (this.zzbvg || (!obj.equals("-Infinity") && !obj.equals("Infinity") && !obj.equals("NaN"))) {
            zzrp();
            this.out.append((CharSequence) obj);
            return this;
        }
        throw new IllegalArgumentException("Numeric values must be finite, but was " + number);
    }

    @Override // java.io.Flushable
    public final void flush() throws IOException {
        if (this.zzbvp != 0) {
            this.out.flush();
            return;
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() throws IOException {
        this.out.close();
        int i = this.zzbvp;
        if (i > 1 || (i == 1 && this.zzbvo[i - 1] != 7)) {
            throw new IOException("Incomplete document");
        }
        this.zzbvp = 0;
    }

    private final void zzch(String str) throws IOException {
        String str2;
        String[] strArr = zzbwe;
        this.out.write("\"");
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt < 128) {
                str2 = strArr[charAt];
                if (str2 == null) {
                }
            } else if (charAt == 8232) {
                str2 = "\\u2028";
            } else if (charAt == 8233) {
                str2 = "\\u2029";
            }
            if (i < i2) {
                this.out.write(str, i, i2 - i);
            }
            this.out.write(str2);
            i = i2 + 1;
        }
        if (i < length) {
            this.out.write(str, i, length - i);
        }
        this.out.write("\"");
    }

    private final void zzro() throws IOException {
        if (this.zzbwg != null) {
            this.out.write(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            int i = this.zzbvp;
            for (int i2 = 1; i2 < i; i2++) {
                this.out.write(this.zzbwg);
            }
        }
    }

    private final void zzrp() throws IOException {
        int peek = peek();
        if (peek == 1) {
            zzcd(2);
            zzro();
        } else if (peek == 2) {
            this.out.append(',');
            zzro();
        } else if (peek != 4) {
            if (peek != 6) {
                if (peek != 7) {
                    throw new IllegalStateException("Nesting problem.");
                } else if (!this.zzbvg) {
                    throw new IllegalStateException("JSON must have only one top-level value.");
                }
            }
            zzcd(7);
        } else {
            this.out.append((CharSequence) this.separator);
            zzcd(5);
        }
    }

    static {
        for (int i = 0; i <= 31; i++) {
            zzbwe[i] = String.format("\\u%04x", Integer.valueOf(i));
        }
        String[] strArr = zzbwe;
        strArr[34] = "\\\"";
        strArr[92] = "\\\\";
        strArr[9] = "\\t";
        strArr[8] = "\\b";
        strArr[10] = "\\n";
        strArr[13] = "\\r";
        strArr[12] = "\\f";
        String[] strArr2 = (String[]) strArr.clone();
        zzbwf = strArr2;
        strArr2[60] = "\\u003c";
        strArr2[62] = "\\u003e";
        strArr2[38] = "\\u0026";
        strArr2[61] = "\\u003d";
        strArr2[39] = "\\u0027";
    }
}
