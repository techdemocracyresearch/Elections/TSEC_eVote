package com.google.android.gms.internal.firebase_ml;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.Arrays;
import java.util.Collection;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzmx<E> extends AbstractCollection<E> implements Serializable {
    private static final Object[] zzalq = new Object[0];

    zzmx() {
    }

    /* renamed from: zzjj */
    public abstract zznd<E> iterator();

    /* access modifiers changed from: package-private */
    @NullableDecl
    public Object[] zzjl() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public abstract boolean zzjo();

    public final Object[] toArray() {
        return toArray(zzalq);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public final <T> T[] toArray(T[] tArr) {
        zzml.checkNotNull(tArr);
        int size = size();
        if (tArr.length < size) {
            Object[] zzjl = zzjl();
            if (zzjl != null) {
                return (T[]) Arrays.copyOfRange(zzjl, zzjm(), zzjn(), tArr.getClass());
            }
            tArr = (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), size));
        } else if (tArr.length > size) {
            tArr[size] = null;
        }
        zza(tArr, 0);
        return tArr;
    }

    /* access modifiers changed from: package-private */
    public int zzjm() {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    public int zzjn() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public zzmw<E> zzjk() {
        return isEmpty() ? zzmw.zzji() : zzmw.zza(toArray());
    }

    /* access modifiers changed from: package-private */
    public int zza(Object[] objArr, int i) {
        zznd zznd = (zznd) iterator();
        while (zznd.hasNext()) {
            objArr[i] = zznd.next();
            i++;
        }
        return i;
    }
}
