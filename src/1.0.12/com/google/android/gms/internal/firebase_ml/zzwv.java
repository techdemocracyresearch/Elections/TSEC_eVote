package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final /* synthetic */ class zzwv {
    static final /* synthetic */ int[] zzcit;
    static final /* synthetic */ int[] zzciu;

    /* JADX WARNING: Can't wrap try/catch for region: R(15:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|16|17|18|20) */
    /* JADX WARNING: Can't wrap try/catch for region: R(17:0|1|2|3|5|6|7|9|10|11|13|14|15|16|17|18|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0039 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0043 */
    static {
        int[] iArr = new int[zzxm.values().length];
        zzciu = iArr;
        try {
            iArr[zzxm.BYTE_STRING.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            zzciu[zzxm.MESSAGE.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            zzciu[zzxm.STRING.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        int[] iArr2 = new int[zzwy.values().length];
        zzcit = iArr2;
        iArr2[zzwy.MAP.ordinal()] = 1;
        zzcit[zzwy.VECTOR.ordinal()] = 2;
        zzcit[zzwy.SCALAR.ordinal()] = 3;
    }
}
