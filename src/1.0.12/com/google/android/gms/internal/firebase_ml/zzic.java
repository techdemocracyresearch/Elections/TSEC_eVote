package com.google.android.gms.internal.firebase_ml;

import java.util.Collection;
import java.util.HashSet;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzic {
    final zzhx zzaeg;
    Collection<String> zzael = new HashSet();

    public zzic(zzhx zzhx) {
        this.zzaeg = (zzhx) zzml.checkNotNull(zzhx);
    }

    public final zzhz zzhp() {
        return new zzhz(this);
    }

    public final zzic zza(Collection<String> collection) {
        this.zzael = collection;
        return this;
    }
}
