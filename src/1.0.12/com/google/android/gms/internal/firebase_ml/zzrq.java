package com.google.android.gms.internal.firebase_ml;

import android.graphics.Rect;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzrq {
    public static String zzbv(int i) {
        if (i == 1) {
            return "builtin/stable";
        }
        if (i != 2) {
            return null;
        }
        return "builtin/latest";
    }

    public static String zzcd(String str) {
        return str == null ? "" : str;
    }

    public static float zza(Float f) {
        if (f == null) {
            return 0.0f;
        }
        return f.floatValue();
    }

    private static int zzc(Integer num) {
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    public static Rect zza(zzkp zzkp, float f) {
        if (zzkp == null || zzkp.zzio() == null || zzkp.zzio().size() != 4) {
            return null;
        }
        int i = 0;
        int i2 = 0;
        int i3 = Integer.MAX_VALUE;
        int i4 = Integer.MAX_VALUE;
        for (zzlm zzlm : zzkp.zzio()) {
            i3 = Math.min(zzc(zzlm.zziv()), i3);
            i4 = Math.min(zzc(zzlm.zziw()), i4);
            i = Math.max(zzc(zzlm.zziv()), i);
            i2 = Math.max(zzc(zzlm.zziw()), i2);
        }
        return new Rect(Math.round(((float) i3) * f), Math.round(((float) i4) * f), Math.round(((float) i) * f), Math.round(((float) i2) * f));
    }
}
