package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzaad implements Iterator<String> {
    private final /* synthetic */ zzaab zzcqf;
    private Iterator<String> zzcrb;

    zzaad(zzaab zzaab) {
        this.zzcqf = zzaab;
        this.zzcrb = zzaab.zzcqg.iterator();
    }

    public final boolean hasNext() {
        return this.zzcrb.hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public final /* synthetic */ String next() {
        return this.zzcrb.next();
    }
}
