package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;
import com.google.android.gms.internal.firebase_ml.zzwz.zzb;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzwz<MessageType extends zzwz<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzvl<MessageType, BuilderType> {
    private static Map<Object, zzwz<?, ?>> zzcll = new ConcurrentHashMap();
    protected zzzz zzclj = zzzz.zzwz();
    private int zzclk = -1;

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static class zza<T extends zzwz<T, ?>> extends zzvm<T> {
        private final T zzcln;

        public zza(T t) {
            this.zzcln = t;
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static class zze<ContainingType extends zzyk, Type> extends zzwm<ContainingType, Type> {
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    static final class zzf implements zzwt<zzf> {
        @Override // com.google.android.gms.internal.firebase_ml.zzwt
        public final int zzd() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwt
        public final zzaan zzui() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwt
        public final zzaaq zzuj() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwt
        public final boolean zzuk() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwt
        public final boolean zzul() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwt
        public final zzyn zza(zzyn zzyn, zzyk zzyk) {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwt
        public final zzyt zza(zzyt zzyt, zzyt zzyt2) {
            throw new NoSuchMethodError();
        }

        @Override // java.lang.Comparable
        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzg {
        public static final int zzclt = 1;
        public static final int zzclu = 2;
        public static final int zzclv = 3;
        public static final int zzclw = 4;
        public static final int zzclx = 5;
        public static final int zzcly = 6;
        public static final int zzclz = 7;
        private static final /* synthetic */ int[] zzcma = {1, 2, 3, 4, 5, 6, 7};
        public static final int zzcmb = 1;
        public static final int zzcmc = 2;
        private static final /* synthetic */ int[] zzcmd = {1, 2};
        public static final int zzcme = 1;
        public static final int zzcmf = 2;
        private static final /* synthetic */ int[] zzcmg = {1, 2};

        public static int[] values$50KLMJ33DTMIUPRFDTJMOP9FE1P6UT3FC9QMCBQ7CLN6ASJ1EHIM8JB5EDPM2PR59HKN8P949LIN8Q3FCHA6UIBEEPNMMP9R0() {
            return (int[]) zzcma.clone();
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static abstract class zzc<MessageType extends zzc<MessageType, BuilderType>, BuilderType extends zzd<MessageType, BuilderType>> extends zzwz<MessageType, BuilderType> implements zzym {
        protected zzwr<zzf> zzclq = zzwr.zzud();

        /* access modifiers changed from: package-private */
        public final zzwr<zzf> zzvc() {
            if (this.zzclq.isImmutable()) {
                this.zzclq = (zzwr) this.zzclq.clone();
            }
            return this.zzclq;
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static abstract class zzd<MessageType extends zzc<MessageType, BuilderType>, BuilderType extends zzd<MessageType, BuilderType>> extends zzb<MessageType, BuilderType> implements zzym {
        protected zzd(MessageType messagetype) {
            super(messagetype);
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz.zzb
        public void zzux() {
            super.zzux();
            ((zzc) this.zzclo).zzclq = (zzwr) ((zzc) this.zzclo).zzclq.clone();
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwz.zzb
        public /* synthetic */ zzwz zzuy() {
            return (zzc) zzva();
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwz.zzb, com.google.android.gms.internal.firebase_ml.zzyn
        public /* synthetic */ zzyk zzva() {
            if (this.zzclp) {
                return (zzc) this.zzclo;
            }
            ((zzc) this.zzclo).zzclq.zztm();
            return (zzc) super.zzva();
        }
    }

    public String toString() {
        return zzyp.zza(this, super.toString());
    }

    public int hashCode() {
        if (this.zzchd != 0) {
            return this.zzchd;
        }
        this.zzchd = zzyz.zzwh().zzad(this).hashCode(this);
        return this.zzchd;
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static abstract class zzb<MessageType extends zzwz<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzvk<MessageType, BuilderType> {
        private final MessageType zzcln;
        protected MessageType zzclo;
        protected boolean zzclp = false;

        protected zzb(MessageType messagetype) {
            this.zzcln = messagetype;
            this.zzclo = (MessageType) ((zzwz) messagetype.zza(zzg.zzclw, null, null));
        }

        /* access modifiers changed from: protected */
        public void zzux() {
            MessageType messagetype = (MessageType) ((zzwz) this.zzclo.zza(zzg.zzclw, null, null));
            zza(messagetype, this.zzclo);
            this.zzclo = messagetype;
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzym
        public final boolean isInitialized() {
            return zzwz.zza((zzwz) this.zzclo, false);
        }

        /* renamed from: zzuy */
        public MessageType zzva() {
            if (this.zzclp) {
                return this.zzclo;
            }
            MessageType messagetype = this.zzclo;
            zzyz.zzwh().zzad(messagetype).zzq(messagetype);
            this.zzclp = true;
            return this.zzclo;
        }

        /* renamed from: zzuz */
        public final MessageType zzvb() {
            MessageType messagetype = (MessageType) ((zzwz) zzva());
            if (messagetype.isInitialized()) {
                return messagetype;
            }
            throw new zzzx(messagetype);
        }

        public final BuilderType zza(MessageType messagetype) {
            if (this.zzclp) {
                zzux();
                this.zzclp = false;
            }
            zza(this.zzclo, messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzyz.zzwh().zzad(messagetype).zze(messagetype, messagetype2);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzvk
        public final /* synthetic */ zzvk zztf() {
            return (zzb) clone();
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzym
        public final /* synthetic */ zzyk zzuv() {
            return this.zzcln;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.google.android.gms.internal.firebase_ml.zzwz$zzb */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.android.gms.internal.firebase_ml.zzvk, java.lang.Object
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zzb zzb = (zzb) this.zzcln.zza(zzg.zzclx, null, null);
            zzb.zza((zzwz) zzva());
            return zzb;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return zzyz.zzwh().zzad(this).equals(this, (zzwz) obj);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzwz<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> BuilderType zzun() {
        return (BuilderType) ((zzb) zza(zzg.zzclx, (Object) null, (Object) null));
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzym
    public final boolean isInitialized() {
        Boolean bool = Boolean.TRUE;
        return zza((zzwz) this, true);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzvl
    public final int zzth() {
        return this.zzclk;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzvl
    public final void zzcs(int i) {
        this.zzclk = i;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyk
    public final void zzb(zzwi zzwi) throws IOException {
        zzyz.zzwh().zzad(this).zza(this, zzwl.zza(zzwi));
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyk
    public final int zzuo() {
        if (this.zzclk == -1) {
            this.zzclk = zzyz.zzwh().zzad(this).zzaa(this);
        }
        return this.zzclk;
    }

    static <T extends zzwz<?, ?>> T zzh(Class<T> cls) {
        zzwz<?, ?> zzwz = zzcll.get(cls);
        if (zzwz == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                zzwz = (T) zzcll.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (zzwz == null) {
            zzwz = (T) ((zzwz) ((zzwz) zzaac.zzn(cls)).zza(zzg.zzcly, (Object) null, (Object) null));
            if (zzwz != null) {
                zzcll.put(cls, zzwz);
            } else {
                throw new IllegalStateException();
            }
        }
        return (T) zzwz;
    }

    protected static <T extends zzwz<?, ?>> void zza(Class<T> cls, T t) {
        zzcll.put(cls, t);
    }

    protected static Object zza(zzyk zzyk, String str, Object[] objArr) {
        return new zzzb(zzyk, str, objArr);
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    protected static final <T extends zzwz<T, ?>> boolean zza(T t, boolean z) {
        byte byteValue = ((Byte) t.zza(zzg.zzclt, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzac = zzyz.zzwh().zzad(t).zzac(t);
        if (z) {
            t.zza(zzg.zzclu, zzac ? t : null, null);
        }
        return zzac;
    }

    protected static zzxg zzup() {
        return zzxb.zzvd();
    }

    protected static zzxg zza(zzxg zzxg) {
        int size = zzxg.size();
        return zzxg.zzdr(size == 0 ? 10 : size << 1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.gms.internal.firebase_ml.zzxy, com.google.android.gms.internal.firebase_ml.zzxi] */
    /* JADX WARNING: Unknown variable types count: 1 */
    protected static zzxi zzuq() {
        return zzxy.zzvr();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.gms.internal.firebase_ml.zzxh, com.google.android.gms.internal.firebase_ml.zzwx] */
    /* JADX WARNING: Unknown variable types count: 1 */
    protected static zzxh zzur() {
        return zzwx.zzum();
    }

    protected static <E> zzxl<E> zzus() {
        return zzyy.zzwg();
    }

    protected static <E> zzxl<E> zza(zzxl<E> zzxl) {
        int size = zzxl.size();
        return zzxl.zzcv(size == 0 ? 10 : size << 1);
    }

    private static <T extends zzwz<T, ?>> T zza(T t, byte[] bArr, int i, int i2, zzwo zzwo) throws zzxk {
        T t2 = (T) ((zzwz) t.zza(zzg.zzclw, null, null));
        try {
            zzze zzad = zzyz.zzwh().zzad(t2);
            zzad.zza(t2, bArr, 0, i2, new zzvq(zzwo));
            zzad.zzq(t2);
            if (((zzwz) t2).zzchd == 0) {
                return t2;
            }
            throw new RuntimeException();
        } catch (IOException e) {
            if (e.getCause() instanceof zzxk) {
                throw ((zzxk) e.getCause());
            }
            throw new zzxk(e.getMessage()).zzg(t2);
        } catch (IndexOutOfBoundsException unused) {
            throw zzxk.zzve().zzg(t2);
        }
    }

    protected static <T extends zzwz<T, ?>> T zza(T t, byte[] bArr, zzwo zzwo) throws zzxk {
        T t2 = (T) zza(t, bArr, 0, bArr.length, zzwo);
        if (t2 == null || t2.isInitialized()) {
            return t2;
        }
        throw new zzxk(new zzzx(t2).getMessage()).zzg(t2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyk
    public final /* synthetic */ zzyn zzut() {
        zzb zzb2 = (zzb) zza(zzg.zzclx, (Object) null, (Object) null);
        zzb2.zza((zzwz) this);
        return zzb2;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyk
    public final /* synthetic */ zzyn zzuu() {
        return (zzb) zza(zzg.zzclx, (Object) null, (Object) null);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzym
    public final /* synthetic */ zzyk zzuv() {
        return (zzwz) zza(zzg.zzcly, (Object) null, (Object) null);
    }
}
