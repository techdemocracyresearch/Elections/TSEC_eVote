package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public enum zzaaq {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(zzvv.zzchp),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzcmx;

    private zzaaq(Object obj) {
        this.zzcmx = obj;
    }
}
