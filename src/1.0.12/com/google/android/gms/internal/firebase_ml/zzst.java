package com.google.android.gms.internal.firebase_ml;

import android.content.Context;
import android.os.SystemClock;
import android.util.SparseArray;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.text.FirebaseVisionText;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzst implements zzpu<FirebaseVisionText, zzsf>, zzqp {
    static boolean zzbqs = true;
    private final Context zzbkt;
    private final zzqg zzbmd;
    private final zzry zzbqu = new zzry();
    private TextRecognizer zzbvb;

    zzst(zzqf zzqf) {
        Preconditions.checkNotNull(zzqf, "MlKitContext can not be null");
        this.zzbkt = zzqf.getApplicationContext();
        this.zzbmd = zzqg.zza(zzqf, 1);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzpu
    public final zzqp zzoc() {
        return this;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void zzol() {
        if (this.zzbvb == null) {
            this.zzbvb = new TextRecognizer.Builder(this.zzbkt).build();
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void release() {
        TextRecognizer textRecognizer = this.zzbvb;
        if (textRecognizer != null) {
            textRecognizer.release();
            this.zzbvb = null;
        }
        zzbqs = true;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzd */
    public final synchronized FirebaseVisionText zza(zzsf zzsf) throws FirebaseMLException {
        SparseArray<TextBlock> detect;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        TextRecognizer textRecognizer = this.zzbvb;
        if (textRecognizer == null) {
            zza(zzoc.UNKNOWN_ERROR, elapsedRealtime, zzsf);
            throw new FirebaseMLException("Model source is unavailable. Please load the model resource first.", 13);
        } else if (textRecognizer.isOperational()) {
            this.zzbqu.zzb(zzsf);
            detect = this.zzbvb.detect(zzsf.zzbrv);
            zza(zzoc.NO_ERROR, elapsedRealtime, zzsf);
            zzbqs = false;
        } else {
            zza(zzoc.MODEL_NOT_DOWNLOADED, elapsedRealtime, zzsf);
            throw new FirebaseMLException("Waiting for the text recognition model to be downloaded. Please wait.", 14);
        }
        return new FirebaseVisionText(detect);
    }

    private final void zza(zzoc zzoc, long j, zzsf zzsf) {
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        this.zzbmd.zza(new zzsw(elapsedRealtime, zzoc, zzsf), zzod.ON_DEVICE_TEXT_DETECT);
        zzqm zzqm = zzsv.zzboj;
        this.zzbmd.zza((zzns.zzi.zzb) ((zzwz) zzns.zzi.zzb.zzkt().zzh(zzoc).zzv(zzbqs).zzg(zzsa.zzc(zzsf)).zzvb()), elapsedRealtime, zzod.AGGREGATED_ON_DEVICE_TEXT_DETECTION, zzqm);
    }
}
