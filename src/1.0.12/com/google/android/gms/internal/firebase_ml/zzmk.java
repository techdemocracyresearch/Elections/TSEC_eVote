package com.google.android.gms.internal.firebase_ml;

import java.util.logging.Logger;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzmk {
    private static final Logger logger = Logger.getLogger(zzmk.class.getName());
    private static final zzmh zzakb = new zza();

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    static final class zza {
        private zza() {
        }
    }

    private zzmk() {
    }

    static String zzbb(@NullableDecl String str) {
        return str == null ? "" : str;
    }

    static boolean zzba(@NullableDecl String str) {
        return str == null || str.isEmpty();
    }
}
