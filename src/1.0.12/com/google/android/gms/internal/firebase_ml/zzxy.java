package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzxy extends zzvp<Long> implements zzxl<Long>, zzyw, RandomAccess {
    private static final zzxy zzcnk;
    private int size;
    private long[] zzcnl;

    public static zzxy zzvr() {
        return zzcnk;
    }

    zzxy() {
        this(new long[10], 0);
    }

    private zzxy(long[] jArr, int i) {
        this.zzcnl = jArr;
        this.size = i;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zztn();
        if (i2 >= i) {
            long[] jArr = this.zzcnl;
            System.arraycopy(jArr, i2, jArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzxy)) {
            return super.equals(obj);
        }
        zzxy zzxy = (zzxy) obj;
        if (this.size != zzxy.size) {
            return false;
        }
        long[] jArr = zzxy.zzcnl;
        for (int i = 0; i < this.size; i++) {
            if (this.zzcnl[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + zzxd.zzaf(this.zzcnl[i2]);
        }
        return i;
    }

    public final long getLong(int i) {
        zzct(i);
        return this.zzcnl[i];
    }

    public final int indexOf(Object obj) {
        if (!(obj instanceof Long)) {
            return -1;
        }
        long longValue = ((Long) obj).longValue();
        int size2 = size();
        for (int i = 0; i < size2; i++) {
            if (this.zzcnl[i] == longValue) {
                return i;
            }
        }
        return -1;
    }

    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    public final int size() {
        return this.size;
    }

    public final void zzag(long j) {
        zztn();
        int i = this.size;
        long[] jArr = this.zzcnl;
        if (i == jArr.length) {
            long[] jArr2 = new long[(((i * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            this.zzcnl = jArr2;
        }
        long[] jArr3 = this.zzcnl;
        int i2 = this.size;
        this.size = i2 + 1;
        jArr3[i2] = j;
    }

    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection
    public final boolean addAll(Collection<? extends Long> collection) {
        zztn();
        zzxd.checkNotNull(collection);
        if (!(collection instanceof zzxy)) {
            return super.addAll(collection);
        }
        zzxy zzxy = (zzxy) collection;
        int i = zzxy.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.zzcnl;
            if (i3 > jArr.length) {
                this.zzcnl = Arrays.copyOf(jArr, i3);
            }
            System.arraycopy(zzxy.zzcnl, 0, this.zzcnl, this.size, zzxy.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean remove(Object obj) {
        zztn();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Long.valueOf(this.zzcnl[i]))) {
                long[] jArr = this.zzcnl;
                System.arraycopy(jArr, i + 1, jArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    private final void zzct(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
    }

    private final String zzcu(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Long set(int i, Long l) {
        long longValue = l.longValue();
        zztn();
        zzct(i);
        long[] jArr = this.zzcnl;
        long j = jArr[i];
        jArr[i] = longValue;
        return Long.valueOf(j);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Long remove(int i) {
        zztn();
        zzct(i);
        long[] jArr = this.zzcnl;
        long j = jArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Long.valueOf(j);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ void add(int i, Long l) {
        int i2;
        long longValue = l.longValue();
        zztn();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
        long[] jArr = this.zzcnl;
        if (i2 < jArr.length) {
            System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
        } else {
            long[] jArr2 = new long[(((i2 * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            System.arraycopy(this.zzcnl, i, jArr2, i + 1, this.size - i);
            this.zzcnl = jArr2;
        }
        this.zzcnl[i] = longValue;
        this.size++;
        this.modCount++;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection, java.util.AbstractList
    public final /* synthetic */ boolean add(Long l) {
        zzag(l.longValue());
        return true;
    }

    /* Return type fixed from 'com.google.android.gms.internal.firebase_ml.zzxl' to match base method */
    @Override // com.google.android.gms.internal.firebase_ml.zzxl
    public final /* synthetic */ zzxl<Long> zzcv(int i) {
        if (i >= this.size) {
            return new zzxy(Arrays.copyOf(this.zzcnl, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        return Long.valueOf(getLong(i));
    }

    static {
        zzxy zzxy = new zzxy(new long[0], 0);
        zzcnk = zzxy;
        zzxy.zztm();
    }
}
