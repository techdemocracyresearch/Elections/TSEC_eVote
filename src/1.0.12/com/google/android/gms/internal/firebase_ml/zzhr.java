package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzhr extends zzhj {
    private final int responseCode;
    private final String responseMessage;
    private final HttpURLConnection zzadz;
    private final ArrayList<String> zzaea;
    private final ArrayList<String> zzaeb;

    zzhr(HttpURLConnection httpURLConnection) throws IOException {
        ArrayList<String> arrayList = new ArrayList<>();
        this.zzaea = arrayList;
        ArrayList<String> arrayList2 = new ArrayList<>();
        this.zzaeb = arrayList2;
        this.zzadz = httpURLConnection;
        int responseCode2 = httpURLConnection.getResponseCode();
        this.responseCode = responseCode2 == -1 ? 0 : responseCode2;
        this.responseMessage = httpURLConnection.getResponseMessage();
        for (Map.Entry<String, List<String>> entry : httpURLConnection.getHeaderFields().entrySet()) {
            String key = entry.getKey();
            if (key != null) {
                for (String str : entry.getValue()) {
                    if (str != null) {
                        arrayList.add(key);
                        arrayList2.add(str);
                    }
                }
            }
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final int getStatusCode() {
        return this.responseCode;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final InputStream getContent() throws IOException {
        InputStream inputStream;
        try {
            inputStream = this.zzadz.getInputStream();
        } catch (IOException unused) {
            inputStream = this.zzadz.getErrorStream();
        }
        if (inputStream == null) {
            return null;
        }
        return new zzhu(this, inputStream);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final String getContentEncoding() {
        return this.zzadz.getContentEncoding();
    }

    public final long getContentLength() {
        String headerField = this.zzadz.getHeaderField("Content-Length");
        if (headerField == null) {
            return -1;
        }
        return Long.parseLong(headerField);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final String getContentType() {
        return this.zzadz.getHeaderField("Content-Type");
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final String getReasonPhrase() {
        return this.responseMessage;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final String zzgj() {
        String headerField = this.zzadz.getHeaderField(0);
        if (headerField == null || !headerField.startsWith("HTTP/1.")) {
            return null;
        }
        return headerField;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final int zzgk() {
        return this.zzaea.size();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final String zzaf(int i) {
        return this.zzaea.get(i);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final String zzag(int i) {
        return this.zzaeb.get(i);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhj
    public final void disconnect() {
        this.zzadz.disconnect();
    }
}
