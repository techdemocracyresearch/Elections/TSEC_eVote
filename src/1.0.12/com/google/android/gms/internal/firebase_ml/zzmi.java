package com.google.android.gms.internal.firebase_ml;

import java.io.Serializable;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzmi<T> implements Serializable {
    public static <T> zzmi<T> zzjf() {
        return zzls.zzajo;
    }

    public abstract T get();

    public abstract boolean isPresent();

    public static <T> zzmi<T> zzj(T t) {
        return new zzmn(zzml.checkNotNull(t));
    }

    zzmi() {
    }
}
