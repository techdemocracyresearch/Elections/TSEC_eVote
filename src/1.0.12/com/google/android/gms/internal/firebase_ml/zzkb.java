package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzkb {
    private static final ThreadLocal<char[]> zzais = new zzka();

    static char[] zzig() {
        return zzais.get();
    }
}
