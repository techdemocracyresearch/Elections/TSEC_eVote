package com.google.android.gms.internal.firebase_ml;

import java.util.List;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzmy extends zzmw<E> {
    private final transient int length;
    private final transient int offset;
    private final /* synthetic */ zzmw zzalr;

    zzmy(zzmw zzmw, int i, int i2) {
        this.zzalr = zzmw;
        this.offset = i;
        this.length = i2;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmx
    public final boolean zzjo() {
        return true;
    }

    public final int size() {
        return this.length;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmx
    public final Object[] zzjl() {
        return this.zzalr.zzjl();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmx
    public final int zzjm() {
        return this.zzalr.zzjm() + this.offset;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmx
    public final int zzjn() {
        return this.zzalr.zzjm() + this.offset + this.length;
    }

    @Override // java.util.List
    public final E get(int i) {
        zzml.zzb(i, this.length);
        return (E) this.zzalr.get(i + this.offset);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzmw
    public final zzmw<E> zzd(int i, int i2) {
        zzml.zza(i, i2, this.length);
        zzmw zzmw = this.zzalr;
        int i3 = this.offset;
        return (zzmw) zzmw.subList(i + i3, i2 + i3);
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzmw
    public final /* synthetic */ List subList(int i, int i2) {
        return subList(i, i2);
    }
}
