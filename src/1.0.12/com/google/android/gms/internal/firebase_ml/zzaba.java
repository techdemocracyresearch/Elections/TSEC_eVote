package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzaba implements zzxe {
    static final zzxe zzan = new zzaba();

    private zzaba() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxe
    public final boolean zzb(int i) {
        return zzaaz.zzeh(i) != null;
    }
}
