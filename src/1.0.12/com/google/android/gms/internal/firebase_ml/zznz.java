package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzns;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zznz implements zzxe {
    static final zzxe zzan = new zznz();

    private zznz() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxe
    public final boolean zzb(int i) {
        return zzns.zzy.zzb.EnumC0015zzb.zzau(i) != null;
    }
}
