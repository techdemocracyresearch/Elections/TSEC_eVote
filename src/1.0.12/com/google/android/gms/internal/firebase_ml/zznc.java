package com.google.android.gms.internal.firebase_ml;

import java.util.ListIterator;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zznc<E> extends zznd<E> implements ListIterator<E> {
    protected zznc() {
    }

    @Override // java.util.ListIterator
    @Deprecated
    public final void add(E e) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.ListIterator
    @Deprecated
    public final void set(E e) {
        throw new UnsupportedOperationException();
    }
}
