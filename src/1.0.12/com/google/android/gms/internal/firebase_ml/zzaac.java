package com.google.android.gms.internal.firebase_ml;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Unsafe;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzaac {
    private static final Logger logger = Logger.getLogger(zzaac.class.getName());
    private static final Class<?> zzchh = zzvo.zztk();
    private static final boolean zzchy = zzxf();
    private static final Unsafe zzcnz;
    private static final boolean zzcqh;
    private static final boolean zzcqi;
    private static final zzd zzcqj;
    private static final boolean zzcqk = zzxg();
    private static final long zzcql;
    private static final long zzcqm = ((long) zzo(boolean[].class));
    private static final long zzcqn = ((long) zzp(boolean[].class));
    private static final long zzcqo = ((long) zzo(int[].class));
    private static final long zzcqp = ((long) zzp(int[].class));
    private static final long zzcqq = ((long) zzo(long[].class));
    private static final long zzcqr = ((long) zzp(long[].class));
    private static final long zzcqs = ((long) zzo(float[].class));
    private static final long zzcqt = ((long) zzp(float[].class));
    private static final long zzcqu = ((long) zzo(double[].class));
    private static final long zzcqv = ((long) zzp(double[].class));
    private static final long zzcqw = ((long) zzo(Object[].class));
    private static final long zzcqx = ((long) zzp(Object[].class));
    private static final long zzcqy;
    private static final int zzcqz;
    static final boolean zzcra = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);

    private zzaac() {
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    static final class zza extends zzd {
        zza(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final byte zzy(Object obj, long j) {
            if (zzaac.zzcra) {
                return zzaac.zzq(obj, j);
            }
            return zzaac.zzr(obj, j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zze(Object obj, long j, byte b) {
            if (zzaac.zzcra) {
                zzaac.zza(obj, j, b);
            } else {
                zzaac.zzb(obj, j, b);
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final boolean zzm(Object obj, long j) {
            if (zzaac.zzcra) {
                return zzaac.zzs(obj, j);
            }
            return zzaac.zzt(obj, j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zza(Object obj, long j, boolean z) {
            if (zzaac.zzcra) {
                zzaac.zzb(obj, j, z);
            } else {
                zzaac.zzc(obj, j, z);
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final float zzn(Object obj, long j) {
            return Float.intBitsToFloat(zzk(obj, j));
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zza(Object obj, long j, float f) {
            zza(obj, j, Float.floatToIntBits(f));
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final double zzo(Object obj, long j) {
            return Double.longBitsToDouble(zzl(obj, j));
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    static final class zzb extends zzd {
        zzb(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final byte zzy(Object obj, long j) {
            return this.zzcrd.getByte(obj, j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zze(Object obj, long j, byte b) {
            this.zzcrd.putByte(obj, j, b);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final boolean zzm(Object obj, long j) {
            return this.zzcrd.getBoolean(obj, j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zza(Object obj, long j, boolean z) {
            this.zzcrd.putBoolean(obj, j, z);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final float zzn(Object obj, long j) {
            return this.zzcrd.getFloat(obj, j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zza(Object obj, long j, float f) {
            this.zzcrd.putFloat(obj, j, f);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final double zzo(Object obj, long j) {
            return this.zzcrd.getDouble(obj, j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zza(Object obj, long j, double d) {
            this.zzcrd.putDouble(obj, j, d);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    static final class zzc extends zzd {
        zzc(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final byte zzy(Object obj, long j) {
            if (zzaac.zzcra) {
                return zzaac.zzq(obj, j);
            }
            return zzaac.zzr(obj, j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zze(Object obj, long j, byte b) {
            if (zzaac.zzcra) {
                zzaac.zza(obj, j, b);
            } else {
                zzaac.zzb(obj, j, b);
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final boolean zzm(Object obj, long j) {
            if (zzaac.zzcra) {
                return zzaac.zzs(obj, j);
            }
            return zzaac.zzt(obj, j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zza(Object obj, long j, boolean z) {
            if (zzaac.zzcra) {
                zzaac.zzb(obj, j, z);
            } else {
                zzaac.zzc(obj, j, z);
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final float zzn(Object obj, long j) {
            return Float.intBitsToFloat(zzk(obj, j));
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zza(Object obj, long j, float f) {
            zza(obj, j, Float.floatToIntBits(f));
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final double zzo(Object obj, long j) {
            return Double.longBitsToDouble(zzl(obj, j));
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzaac.zzd
        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }
    }

    static boolean zzxc() {
        return zzchy;
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static abstract class zzd {
        Unsafe zzcrd;

        zzd(Unsafe unsafe) {
            this.zzcrd = unsafe;
        }

        public abstract void zza(Object obj, long j, double d);

        public abstract void zza(Object obj, long j, float f);

        public abstract void zza(Object obj, long j, boolean z);

        public abstract void zze(Object obj, long j, byte b);

        public abstract boolean zzm(Object obj, long j);

        public abstract float zzn(Object obj, long j);

        public abstract double zzo(Object obj, long j);

        public abstract byte zzy(Object obj, long j);

        public final int zzk(Object obj, long j) {
            return this.zzcrd.getInt(obj, j);
        }

        public final void zza(Object obj, long j, int i) {
            this.zzcrd.putInt(obj, j, i);
        }

        public final long zzl(Object obj, long j) {
            return this.zzcrd.getLong(obj, j);
        }

        public final void zza(Object obj, long j, long j2) {
            this.zzcrd.putLong(obj, j, j2);
        }
    }

    static boolean zzxd() {
        return zzcqk;
    }

    static <T> T zzn(Class<T> cls) {
        try {
            return (T) zzcnz.allocateInstance(cls);
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        }
    }

    private static int zzo(Class<?> cls) {
        if (zzchy) {
            return zzcqj.zzcrd.arrayBaseOffset(cls);
        }
        return -1;
    }

    private static int zzp(Class<?> cls) {
        if (zzchy) {
            return zzcqj.zzcrd.arrayIndexScale(cls);
        }
        return -1;
    }

    static int zzk(Object obj, long j) {
        return zzcqj.zzk(obj, j);
    }

    static void zza(Object obj, long j, int i) {
        zzcqj.zza(obj, j, i);
    }

    static long zzl(Object obj, long j) {
        return zzcqj.zzl(obj, j);
    }

    static void zza(Object obj, long j, long j2) {
        zzcqj.zza(obj, j, j2);
    }

    static boolean zzm(Object obj, long j) {
        return zzcqj.zzm(obj, j);
    }

    static void zza(Object obj, long j, boolean z) {
        zzcqj.zza(obj, j, z);
    }

    static float zzn(Object obj, long j) {
        return zzcqj.zzn(obj, j);
    }

    static void zza(Object obj, long j, float f) {
        zzcqj.zza(obj, j, f);
    }

    static double zzo(Object obj, long j) {
        return zzcqj.zzo(obj, j);
    }

    static void zza(Object obj, long j, double d) {
        zzcqj.zza(obj, j, d);
    }

    static Object zzp(Object obj, long j) {
        return zzcqj.zzcrd.getObject(obj, j);
    }

    static void zza(Object obj, long j, Object obj2) {
        zzcqj.zzcrd.putObject(obj, j, obj2);
    }

    static byte zza(byte[] bArr, long j) {
        return zzcqj.zzy(bArr, zzcql + j);
    }

    static void zza(byte[] bArr, long j, byte b) {
        zzcqj.zze(bArr, zzcql + j, b);
    }

    static Unsafe zzxe() {
        try {
            return (Unsafe) AccessController.doPrivileged(new zzaae());
        } catch (Throwable unused) {
            return null;
        }
    }

    private static boolean zzxf() {
        Unsafe unsafe = zzcnz;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("arrayBaseOffset", Class.class);
            cls.getMethod("arrayIndexScale", Class.class);
            cls.getMethod("getInt", Object.class, Long.TYPE);
            cls.getMethod("putInt", Object.class, Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            cls.getMethod("putLong", Object.class, Long.TYPE, Long.TYPE);
            cls.getMethod("getObject", Object.class, Long.TYPE);
            cls.getMethod("putObject", Object.class, Long.TYPE, Object.class);
            if (zzvo.zztj()) {
                return true;
            }
            cls.getMethod("getByte", Object.class, Long.TYPE);
            cls.getMethod("putByte", Object.class, Long.TYPE, Byte.TYPE);
            cls.getMethod("getBoolean", Object.class, Long.TYPE);
            cls.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE);
            cls.getMethod("getFloat", Object.class, Long.TYPE);
            cls.getMethod("putFloat", Object.class, Long.TYPE, Float.TYPE);
            cls.getMethod("getDouble", Object.class, Long.TYPE);
            cls.getMethod("putDouble", Object.class, Long.TYPE, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    private static boolean zzxg() {
        Unsafe unsafe = zzcnz;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            if (zzxh() == null) {
                return false;
            }
            if (zzvo.zztj()) {
                return true;
            }
            cls.getMethod("getByte", Long.TYPE);
            cls.getMethod("putByte", Long.TYPE, Byte.TYPE);
            cls.getMethod("getInt", Long.TYPE);
            cls.getMethod("putInt", Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Long.TYPE);
            cls.getMethod("putLong", Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Long.TYPE, Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    private static boolean zzq(Class<?> cls) {
        if (!zzvo.zztj()) {
            return false;
        }
        try {
            Class<?> cls2 = zzchh;
            cls2.getMethod("peekLong", cls, Boolean.TYPE);
            cls2.getMethod("pokeLong", cls, Long.TYPE, Boolean.TYPE);
            cls2.getMethod("pokeInt", cls, Integer.TYPE, Boolean.TYPE);
            cls2.getMethod("peekInt", cls, Boolean.TYPE);
            cls2.getMethod("pokeByte", cls, Byte.TYPE);
            cls2.getMethod("peekByte", cls);
            cls2.getMethod("pokeByteArray", cls, byte[].class, Integer.TYPE, Integer.TYPE);
            cls2.getMethod("peekByteArray", cls, byte[].class, Integer.TYPE, Integer.TYPE);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    private static Field zzxh() {
        Field zzb2;
        if (zzvo.zztj() && (zzb2 = zzb(Buffer.class, "effectiveDirectAddress")) != null) {
            return zzb2;
        }
        Field zzb3 = zzb(Buffer.class, "address");
        if (zzb3 == null || zzb3.getType() != Long.TYPE) {
            return null;
        }
        return zzb3;
    }

    private static Field zzb(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static byte zzq(Object obj, long j) {
        return (byte) (zzk(obj, -4 & j) >>> ((int) (((~j) & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static byte zzr(Object obj, long j) {
        return (byte) (zzk(obj, -4 & j) >>> ((int) ((j & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static void zza(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int zzk = zzk(obj, j2);
        int i = ((~((int) j)) & 3) << 3;
        zza(obj, j2, ((255 & b) << i) | (zzk & (~(255 << i))));
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int i = (((int) j) & 3) << 3;
        zza(obj, j2, ((255 & b) << i) | (zzk(obj, j2) & (~(255 << i))));
    }

    /* access modifiers changed from: private */
    public static boolean zzs(Object obj, long j) {
        return zzq(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    public static boolean zzt(Object obj, long j) {
        return zzr(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, boolean z) {
        zza(obj, j, z ? (byte) 1 : 0);
    }

    /* access modifiers changed from: private */
    public static void zzc(Object obj, long j, boolean z) {
        zzb(obj, j, z ? (byte) 1 : 0);
    }

    static {
        Unsafe zzxe = zzxe();
        zzcnz = zzxe;
        boolean zzq = zzq(Long.TYPE);
        zzcqh = zzq;
        boolean zzq2 = zzq(Integer.TYPE);
        zzcqi = zzq2;
        zzd zzd2 = null;
        if (zzxe != null) {
            if (!zzvo.zztj()) {
                zzd2 = new zzb(zzxe);
            } else if (zzq) {
                zzd2 = new zzc(zzxe);
            } else if (zzq2) {
                zzd2 = new zza(zzxe);
            }
        }
        zzcqj = zzd2;
        long zzo = (long) zzo(byte[].class);
        zzcql = zzo;
        Field zzxh = zzxh();
        zzcqy = (zzxh == null || zzd2 == null) ? -1 : zzd2.zzcrd.objectFieldOffset(zzxh);
        zzcqz = (int) (7 & zzo);
    }
}
