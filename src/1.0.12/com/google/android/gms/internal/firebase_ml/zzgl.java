package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzge;
import java.util.Collections;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzgl extends zzge.zza {
    protected zzgl(zzhh zzhh, zzhx zzhx, String str, String str2, zzhe zzhe, boolean z) {
        super(zzhh, str, str2, new zzic(zzhx).zza(Collections.emptySet()).zzhp(), zzhe);
    }

    /* renamed from: zzq */
    public zzgl zzj(String str) {
        return (zzgl) super.zzj(str);
    }

    /* renamed from: zzr */
    public zzgl zzk(String str) {
        return (zzgl) super.zzk(str);
    }

    /* renamed from: zzb */
    public zzgl zza(zzgj zzgj) {
        return (zzgl) super.zza(zzgj);
    }

    /* renamed from: zzs */
    public zzgl zzm(String str) {
        return (zzgl) super.zzm(str);
    }
}
