package com.google.android.gms.internal.firebase_ml;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzzq extends AbstractSet<Map.Entry<K, V>> {
    private final /* synthetic */ zzzj zzcpt;

    private zzzq(zzzj zzzj) {
        this.zzcpt = zzzj;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator<Map.Entry<K, V>> iterator() {
        return new zzzr(this.zzcpt, null);
    }

    public int size() {
        return this.zzcpt.size();
    }

    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.zzcpt.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.zzcpt.remove(entry.getKey());
        return true;
    }

    public void clear() {
        this.zzcpt.clear();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.zzcpt.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    /* synthetic */ zzzq(zzzj zzzj, zzzi zzzi) {
        this(zzzj);
    }
}
