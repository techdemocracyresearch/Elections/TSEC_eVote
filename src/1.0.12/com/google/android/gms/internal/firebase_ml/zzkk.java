package com.google.android.gms.internal.firebase_ml;

import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzkk extends zzhy {
    @zzjg
    private String parent;
    @zzjg
    private List<zzki> requests;

    public final zzkk zzc(List<zzki> list) {
        this.requests = list;
        return this;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ zzhy zza(String str, Object obj) {
        return (zzkk) zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ zzhy zzfc() {
        return (zzkk) clone();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf, com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ zzjf zzfd() {
        return (zzkk) clone();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf, com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ zzjf zzb(String str, Object obj) {
        return (zzkk) super.zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf, java.util.AbstractMap, java.lang.Object, com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        return (zzkk) super.clone();
    }
}
