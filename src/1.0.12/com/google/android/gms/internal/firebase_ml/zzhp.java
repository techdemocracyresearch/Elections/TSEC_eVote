package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzhp implements zzhq {
    private final Proxy zzady;

    public zzhp() {
        this(null);
    }

    public zzhp(Proxy proxy) {
        this.zzady = proxy;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhq
    public final HttpURLConnection zza(URL url) throws IOException {
        Proxy proxy = this.zzady;
        return (HttpURLConnection) (proxy == null ? url.openConnection() : url.openConnection(proxy));
    }
}
