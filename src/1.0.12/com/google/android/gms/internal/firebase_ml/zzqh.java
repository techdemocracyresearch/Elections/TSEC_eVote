package com.google.android.gms.internal.firebase_ml;

import java.util.Objects;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzqh<T> {
    private final String zzbju;
    private final T zzbjv;

    public static <T> zzqh<T> zzj(String str, T t) {
        return new zzqh<>(str, t);
    }

    private zzqh(String str, T t) {
        Objects.requireNonNull(str, "Null firebasePersistentKey");
        this.zzbju = str;
        Objects.requireNonNull(t, "Null options");
        this.zzbjv = t;
    }

    public final String toString() {
        String str = this.zzbju;
        String valueOf = String.valueOf(this.zzbjv);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 58 + String.valueOf(valueOf).length());
        sb.append("MlModelDriverInstanceKey{firebasePersistentKey=");
        sb.append(str);
        sb.append(", options=");
        sb.append(valueOf);
        sb.append("}");
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof zzqh) {
            zzqh zzqh = (zzqh) obj;
            return this.zzbju.equals(zzqh.zzbju) && this.zzbjv.equals(zzqh.zzbjv);
        }
    }

    public final int hashCode() {
        return com.google.android.gms.common.internal.Objects.hashCode(this.zzbju, this.zzbjv);
    }
}
