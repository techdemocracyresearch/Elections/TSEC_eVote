package com.google.android.gms.internal.firebase_ml;

import kotlin.text.Typography;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public enum zzaav implements zzxc {
    DELEGATE_NONE(0),
    NNAPI(1),
    GPU(2),
    HEXAGON(3);
    
    private static final zzxf<zzaav> zzac = new zzaax();
    private final int value;

    @Override // com.google.android.gms.internal.firebase_ml.zzxc
    public final int zzd() {
        return this.value;
    }

    public static zzaav zzeg(int i) {
        if (i == 0) {
            return DELEGATE_NONE;
        }
        if (i == 1) {
            return NNAPI;
        }
        if (i == 2) {
            return GPU;
        }
        if (i != 3) {
            return null;
        }
        return HEXAGON;
    }

    public static zzxe zzf() {
        return zzaaw.zzan;
    }

    public final String toString() {
        return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
    }

    private zzaav(int i) {
        this.value = i;
    }
}
