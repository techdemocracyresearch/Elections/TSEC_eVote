package com.google.android.gms.internal.firebase_ml;

import android.content.Context;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.SparseArray;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.vision.dynamite.face.ModuleDescriptor;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsh implements zzpu<List<FirebaseVisionFace>, zzsf>, zzqp {
    private static AtomicBoolean zzbob = new AtomicBoolean(true);
    private static volatile Boolean zzbth;
    private final Context zzbkt;
    private final zzqg zzbmd;
    private final zzry zzbqu = new zzry();
    private final FirebaseVisionFaceDetectorOptions zzbti;
    private FaceDetector zzbtj;
    private FaceDetector zzbtk;

    public zzsh(zzqf zzqf, FirebaseVisionFaceDetectorOptions firebaseVisionFaceDetectorOptions) {
        Preconditions.checkNotNull(zzqf, "MlKitContext can not be null");
        Preconditions.checkNotNull(firebaseVisionFaceDetectorOptions, "FirebaseVisionFaceDetectorOptions can not be null");
        this.zzbkt = zzqf.getApplicationContext();
        this.zzbti = firebaseVisionFaceDetectorOptions;
        this.zzbmd = zzqg.zza(zzqf, 1);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzpu
    public final zzqp zzoc() {
        return this;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void zzol() {
        if (this.zzbti.getContourMode() == 2) {
            if (this.zzbtk == null) {
                this.zzbtk = new FaceDetector.Builder(this.zzbkt).setLandmarkType(2).setMode(2).setTrackingEnabled(false).setProminentFaceOnly(true).build();
            }
            if ((this.zzbti.getLandmarkMode() == 2 || this.zzbti.getClassificationMode() == 2 || this.zzbti.getPerformanceMode() == 2) && this.zzbtj == null) {
                this.zzbtj = new FaceDetector.Builder(this.zzbkt).setLandmarkType(zzsa.zzbw(this.zzbti.getLandmarkMode())).setClassificationType(zzsa.zzby(this.zzbti.getClassificationMode())).setMode(zzsa.zzbx(this.zzbti.getPerformanceMode())).setMinFaceSize(this.zzbti.getMinFaceSize()).setTrackingEnabled(this.zzbti.isTrackingEnabled()).build();
            }
        } else if (this.zzbtj == null) {
            this.zzbtj = new FaceDetector.Builder(this.zzbkt).setLandmarkType(zzsa.zzbw(this.zzbti.getLandmarkMode())).setClassificationType(zzsa.zzby(this.zzbti.getClassificationMode())).setMode(zzsa.zzbx(this.zzbti.getPerformanceMode())).setMinFaceSize(this.zzbti.getMinFaceSize()).setTrackingEnabled(this.zzbti.isTrackingEnabled()).build();
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void release() {
        FaceDetector faceDetector = this.zzbtj;
        if (faceDetector != null) {
            faceDetector.release();
            this.zzbtj = null;
        }
        FaceDetector faceDetector2 = this.zzbtk;
        if (faceDetector2 != null) {
            faceDetector2.release();
            this.zzbtk = null;
        }
        zzbob.set(true);
    }

    private final synchronized List<FirebaseVisionFace> zza(FaceDetector faceDetector, zzsf zzsf, long j) throws FirebaseMLException {
        ArrayList arrayList;
        if (this.zzbtk != null) {
            if (zzbth == null) {
                zzbth = Boolean.valueOf(DynamiteModule.getLocalVersion(this.zzbkt, ModuleDescriptor.MODULE_ID) > 0);
            }
            if (!zzbth.booleanValue()) {
                throw new FirebaseMLException("No Face Contour model is bundled. Please check your app setup to include firebase-ml-vision-face-model dependency.", 14);
            }
        }
        if (faceDetector.isOperational()) {
            SparseArray<Face> detect = faceDetector.detect(zzsf.zzbrv);
            arrayList = new ArrayList();
            for (int i = 0; i < detect.size(); i++) {
                arrayList.add(new FirebaseVisionFace(detect.get(detect.keyAt(i))));
            }
        } else {
            zza(zzoc.MODEL_NOT_DOWNLOADED, j, zzsf, 0, 0);
            throw new FirebaseMLException("Waiting for the face detection model to be downloaded. Please wait.", 14);
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x013d  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x010b A[SYNTHETIC] */
    public final synchronized List<FirebaseVisionFace> zza(zzsf zzsf) throws FirebaseMLException {
        List<FirebaseVisionFace> list;
        long j;
        List<FirebaseVisionFace> list2;
        int i;
        int i2;
        long j2;
        Iterator<FirebaseVisionFace> it;
        Iterator<FirebaseVisionFace> it2;
        boolean z;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.zzbtj == null) {
            if (this.zzbtk == null) {
                zza(zzoc.UNKNOWN_ERROR, elapsedRealtime, zzsf, 0, 0);
                throw new FirebaseMLException("Face detector wasn't initialized correctly. This is most likely caused by invalid face detector options.", 13);
            }
        }
        this.zzbqu.zzb(zzsf);
        FaceDetector faceDetector = this.zzbtj;
        List<FirebaseVisionFace> list3 = null;
        if (faceDetector != null) {
            list = zza(faceDetector, zzsf, elapsedRealtime);
            if (!this.zzbti.isTrackingEnabled()) {
                zzh(list);
            }
        } else {
            list = null;
        }
        FaceDetector faceDetector2 = this.zzbtk;
        if (faceDetector2 != null) {
            list3 = zza(faceDetector2, zzsf, elapsedRealtime);
            zzh(list3);
        }
        if (list == null) {
            if (list3 == null) {
                throw new FirebaseMLException("No detector is enabled", 13);
            }
        }
        if (list == null) {
            list2 = list3;
        } else if (list3 == null) {
            list2 = list;
        } else {
            HashSet hashSet = new HashSet();
            Iterator<FirebaseVisionFace> it3 = list3.iterator();
            while (it3.hasNext()) {
                FirebaseVisionFace next = it3.next();
                Iterator<FirebaseVisionFace> it4 = list.iterator();
                boolean z2 = false;
                while (it4.hasNext()) {
                    FirebaseVisionFace next2 = it4.next();
                    if (next.getBoundingBox() != null) {
                        if (next2.getBoundingBox() != null) {
                            Rect boundingBox = next.getBoundingBox();
                            Rect boundingBox2 = next2.getBoundingBox();
                            if (boundingBox.intersect(boundingBox2)) {
                                it2 = it3;
                                double min = (double) ((Math.min(boundingBox.right, boundingBox2.right) - Math.max(boundingBox.left, boundingBox2.left)) * (Math.min(boundingBox.bottom, boundingBox2.bottom) - Math.max(boundingBox.top, boundingBox2.top)));
                                it = it4;
                                j2 = elapsedRealtime;
                                if (min / ((((double) ((boundingBox.right - boundingBox.left) * (boundingBox.bottom - boundingBox.top))) + ((double) ((boundingBox2.right - boundingBox2.left) * (boundingBox2.bottom - boundingBox2.top)))) - min) > 0.6d) {
                                    z = true;
                                    if (z) {
                                        next2.zza(next.zzqr());
                                        z2 = true;
                                    }
                                    hashSet.add(next2);
                                    it3 = it2;
                                    it4 = it;
                                    elapsedRealtime = j2;
                                }
                                z = false;
                                if (z) {
                                }
                                hashSet.add(next2);
                                it3 = it2;
                                it4 = it;
                                elapsedRealtime = j2;
                            }
                        }
                    }
                    j2 = elapsedRealtime;
                    it2 = it3;
                    it = it4;
                    z = false;
                    if (z) {
                    }
                    hashSet.add(next2);
                    it3 = it2;
                    it4 = it;
                    elapsedRealtime = j2;
                }
                if (!z2) {
                    hashSet.add(next);
                }
                it3 = it3;
                elapsedRealtime = elapsedRealtime;
            }
            j = elapsedRealtime;
            list2 = new ArrayList<>(hashSet);
            zzoc zzoc = zzoc.NO_ERROR;
            if (list3 != null) {
                i = 0;
            } else {
                i = list3.size();
            }
            if (list != null) {
                i2 = 0;
            } else {
                i2 = list.size();
            }
            zza(zzoc, j, zzsf, i, i2);
            zzbob.set(false);
        }
        j = elapsedRealtime;
        zzoc zzoc2 = zzoc.NO_ERROR;
        if (list3 != null) {
        }
        if (list != null) {
        }
        zza(zzoc2, j, zzsf, i, i2);
        zzbob.set(false);
        return list2;
    }

    private static void zzh(List<FirebaseVisionFace> list) {
        for (FirebaseVisionFace firebaseVisionFace : list) {
            firebaseVisionFace.zzbi(-1);
        }
    }

    private final synchronized void zza(zzoc zzoc, long j, zzsf zzsf, int i, int i2) {
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        this.zzbmd.zza(new zzsg(this, elapsedRealtime, zzoc, i, i2, zzsf), zzod.ON_DEVICE_FACE_DETECT);
        zzqm zzqm = zzsi.zzboj;
        this.zzbmd.zza((zzns.zzd.zzb) ((zzwz) zzns.zzd.zzb.zzkg().zze(zzoc).zzq(zzbob.get()).zzd(zzsa.zzc(zzsf)).zzap(i).zzaq(i2).zza(this.zzbti.zzqs()).zzvb()), elapsedRealtime, zzod.AGGREGATED_ON_DEVICE_FACE_DETECTION, zzqm);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzns.zzad.zza zza(long j, zzoc zzoc, int i, int i2, zzsf zzsf) {
        return zzns.zzad.zzma().zza((zzns.zzan) ((zzwz) zzns.zzan.zzmv().zzd(zzns.zzaf.zzme().zzk(j).zzk(zzoc).zzah(zzbob.get()).zzai(true).zzaj(true)).zzc(this.zzbti.zzqs()).zzbm(i).zzbn(i2).zzj(zzsa.zzc(zzsf)).zzvb()));
    }
}
