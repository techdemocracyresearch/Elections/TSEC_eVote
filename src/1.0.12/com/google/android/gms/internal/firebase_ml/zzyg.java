package com.google.android.gms.internal.firebase_ml;

import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzyg implements zzyh {
    zzyg() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyh
    public final Map<?, ?> zzu(Object obj) {
        return (zzye) obj;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyh
    public final zzyf<?, ?> zzv(Object obj) {
        return ((zzyc) obj).zzvt();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyh
    public final Map<?, ?> zzw(Object obj) {
        return (zzye) obj;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyh
    public final boolean zzx(Object obj) {
        return !((zzye) obj).isMutable();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyh
    public final Object zzy(Object obj) {
        ((zzye) obj).zztm();
        return obj;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyh
    public final Object zzz(Object obj) {
        return zzye.zzvu().zzvv();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyh
    public final Object zzd(Object obj, Object obj2) {
        zzye zzye = (zzye) obj;
        zzye zzye2 = (zzye) obj2;
        if (!zzye2.isEmpty()) {
            if (!zzye.isMutable()) {
                zzye = zzye.zzvv();
            }
            zzye.zza(zzye2);
        }
        return zzye;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyh
    public final int zzd(int i, Object obj, Object obj2) {
        zzye zzye = (zzye) obj;
        zzyc zzyc = (zzyc) obj2;
        int i2 = 0;
        if (zzye.isEmpty()) {
            return 0;
        }
        for (Map.Entry entry : zzye.entrySet()) {
            i2 += zzyc.zzc(i, entry.getKey(), entry.getValue());
        }
        return i2;
    }
}
