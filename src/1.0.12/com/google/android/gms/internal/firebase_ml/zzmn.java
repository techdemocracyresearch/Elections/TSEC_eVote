package com.google.android.gms.internal.firebase_ml;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzmn<T> extends zzmi<T> {
    private final T zzakf;

    zzmn(T t) {
        this.zzakf = t;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzmi
    public final boolean isPresent() {
        return true;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzmi
    public final T get() {
        return this.zzakf;
    }

    public final boolean equals(@NullableDecl Object obj) {
        if (obj instanceof zzmn) {
            return this.zzakf.equals(((zzmn) obj).zzakf);
        }
        return false;
    }

    public final int hashCode() {
        return this.zzakf.hashCode() + 1502476572;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.zzakf);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
        sb.append("Optional.of(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }
}
