package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.OutputStream;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzhw extends zzgq {
    private final Object data;
    private final zzhx zzaeg;
    private String zzaeh;

    public zzhw(zzhx zzhx, Object obj) {
        super("application/json; charset=UTF-8");
        this.zzaeg = (zzhx) zzml.checkNotNull(zzhx);
        this.data = zzml.checkNotNull(obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjq
    public final void writeTo(OutputStream outputStream) throws IOException {
        zzia zza = this.zzaeg.zza(outputStream, zzfs());
        if (this.zzaeh != null) {
            zza.zzgx();
            zza.zzan(this.zzaeh);
        }
        zza.zzd(this.data);
        if (this.zzaeh != null) {
            zza.zzgy();
        }
        zza.flush();
    }

    public final zzhw zzal(String str) {
        this.zzaeh = str;
        return this;
    }
}
