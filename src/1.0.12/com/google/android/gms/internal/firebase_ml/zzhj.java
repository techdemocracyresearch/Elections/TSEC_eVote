package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.InputStream;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzhj {
    public void disconnect() throws IOException {
    }

    public abstract InputStream getContent() throws IOException;

    public abstract String getContentEncoding() throws IOException;

    public abstract String getContentType() throws IOException;

    public abstract String getReasonPhrase() throws IOException;

    public abstract int getStatusCode() throws IOException;

    public abstract String zzaf(int i) throws IOException;

    public abstract String zzag(int i) throws IOException;

    public abstract String zzgj() throws IOException;

    public abstract int zzgk() throws IOException;
}
