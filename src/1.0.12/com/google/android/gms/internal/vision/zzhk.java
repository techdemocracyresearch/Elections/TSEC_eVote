package com.google.android.gms.internal.vision;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzhk extends zzex<String> implements zzhj, RandomAccess {
    private static final zzhk zzyh;
    private static final zzhj zzyi;
    private final List<Object> zzyj;

    public zzhk() {
        this(10);
    }

    public zzhk(int i) {
        this(new ArrayList(i));
    }

    private zzhk(ArrayList<Object> arrayList) {
        this.zzyj = arrayList;
    }

    public final int size() {
        return this.zzyj.size();
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.google.android.gms.internal.vision.zzex
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @Override // java.util.List, java.util.AbstractList, com.google.android.gms.internal.vision.zzex
    public final boolean addAll(int i, Collection<? extends String> collection) {
        zzdq();
        if (collection instanceof zzhj) {
            collection = ((zzhj) collection).zzgx();
        }
        boolean addAll = this.zzyj.addAll(i, collection);
        this.modCount++;
        return addAll;
    }

    @Override // com.google.android.gms.internal.vision.zzex
    public final void clear() {
        zzdq();
        this.zzyj.clear();
        this.modCount++;
    }

    @Override // com.google.android.gms.internal.vision.zzhj
    public final void zzc(zzfh zzfh) {
        zzdq();
        this.zzyj.add(zzfh);
        this.modCount++;
    }

    @Override // com.google.android.gms.internal.vision.zzhj
    public final Object getRaw(int i) {
        return this.zzyj.get(i);
    }

    private static String zzk(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzfh) {
            return ((zzfh) obj).zzer();
        }
        return zzgt.zzh((byte[]) obj);
    }

    @Override // com.google.android.gms.internal.vision.zzhj
    public final List<?> zzgx() {
        return Collections.unmodifiableList(this.zzyj);
    }

    @Override // com.google.android.gms.internal.vision.zzhj
    public final zzhj zzgy() {
        return zzdo() ? new zzjo(this) : this;
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, java.util.AbstractList, com.google.android.gms.internal.vision.zzex
    public final /* synthetic */ String set(int i, String str) {
        zzdq();
        return zzk(this.zzyj.set(i, str));
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.google.android.gms.internal.vision.zzex
    public final /* bridge */ /* synthetic */ boolean retainAll(Collection collection) {
        return super.retainAll(collection);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.google.android.gms.internal.vision.zzex
    public final /* bridge */ /* synthetic */ boolean removeAll(Collection collection) {
        return super.removeAll(collection);
    }

    @Override // java.util.List, com.google.android.gms.internal.vision.zzex
    public final /* bridge */ /* synthetic */ boolean remove(Object obj) {
        return super.remove(obj);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, java.util.AbstractList, com.google.android.gms.internal.vision.zzex
    public final /* synthetic */ String remove(int i) {
        zzdq();
        Object remove = this.zzyj.remove(i);
        this.modCount++;
        return zzk(remove);
    }

    @Override // com.google.android.gms.internal.vision.zzex, com.google.android.gms.internal.vision.zzgz
    public final /* bridge */ /* synthetic */ boolean zzdo() {
        return super.zzdo();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, java.util.AbstractList, com.google.android.gms.internal.vision.zzex
    public final /* synthetic */ void add(int i, String str) {
        zzdq();
        this.zzyj.add(i, str);
        this.modCount++;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, com.google.android.gms.internal.vision.zzex
    public final /* bridge */ /* synthetic */ boolean add(String str) {
        return super.add(str);
    }

    @Override // com.google.android.gms.internal.vision.zzex
    public final /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @Override // com.google.android.gms.internal.vision.zzex
    public final /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override // com.google.android.gms.internal.vision.zzgz
    public final /* synthetic */ zzgz zzah(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.zzyj);
            return new zzhk(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        Object obj = this.zzyj.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzfh) {
            zzfh zzfh = (zzfh) obj;
            String zzer = zzfh.zzer();
            if (zzfh.zzes()) {
                this.zzyj.set(i, zzer);
            }
            return zzer;
        }
        byte[] bArr = (byte[]) obj;
        String zzh = zzgt.zzh(bArr);
        if (zzgt.zzg(bArr)) {
            this.zzyj.set(i, zzh);
        }
        return zzh;
    }

    static {
        zzhk zzhk = new zzhk();
        zzyh = zzhk;
        zzhk.zzdp();
        zzyi = zzhk;
    }
}
