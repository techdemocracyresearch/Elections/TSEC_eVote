package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzji implements zzjh {
    private final /* synthetic */ zzfh zzaan;

    zzji(zzfh zzfh) {
        this.zzaan = zzfh;
    }

    @Override // com.google.android.gms.internal.vision.zzjh
    public final int size() {
        return this.zzaan.size();
    }

    @Override // com.google.android.gms.internal.vision.zzjh
    public final byte zzao(int i) {
        return this.zzaan.zzao(i);
    }
}
