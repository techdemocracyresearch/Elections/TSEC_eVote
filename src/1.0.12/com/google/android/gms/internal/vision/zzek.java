package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzea;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzek implements zzgy {
    static final zzgy zzhf = new zzek();

    private zzek() {
    }

    @Override // com.google.android.gms.internal.vision.zzgy
    public final boolean zzg(int i) {
        return zzea.zzg.zzd.zzx(i) != null;
    }
}
