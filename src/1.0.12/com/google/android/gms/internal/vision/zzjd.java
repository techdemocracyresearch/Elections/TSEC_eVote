package com.google.android.gms.internal.vision;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public class zzjd extends AbstractSet<Map.Entry<K, V>> {
    private final /* synthetic */ zziw zzaah;

    private zzjd(zziw zziw) {
        this.zzaah = zziw;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator<Map.Entry<K, V>> iterator() {
        return new zzje(this.zzaah, null);
    }

    public int size() {
        return this.zzaah.size();
    }

    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.zzaah.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.zzaah.remove(entry.getKey());
        return true;
    }

    public void clear() {
        this.zzaah.clear();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.zzaah.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    /* synthetic */ zzjd(zziw zziw, zziv zziv) {
        this(zziw);
    }
}
