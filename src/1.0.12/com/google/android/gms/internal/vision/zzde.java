package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzde<E> extends zzdb<E> {
    private final zzdf<E> zzlq;

    zzde(zzdf<E> zzdf, int i) {
        super(zzdf.size(), i);
        this.zzlq = zzdf;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.vision.zzdb
    public final E get(int i) {
        return this.zzlq.get(i);
    }
}
