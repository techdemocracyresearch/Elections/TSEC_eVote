package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzgs;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
public final class zzkn extends zzgs<zzkn, zza> implements zzie {
    private static final zzkn zzagr;
    private static volatile zzil<zzkn> zzbd;
    private zzgz<zzkf> zzagq = zzgh();
    private byte zzjy = 2;

    private zzkn() {
    }

    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static final class zza extends zzgs.zza<zzkn, zza> implements zzie {
        private zza() {
            super(zzkn.zzagr);
        }

        /* synthetic */ zza(zzkp zzkp) {
            this();
        }
    }

    public final int zzjl() {
        return this.zzagq.size();
    }

    public final zzkf zzcc(int i) {
        return this.zzagq.get(i);
    }

    /* JADX WARN: Type inference failed for: r3v14, types: [com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzkn>, com.google.android.gms.internal.vision.zzgs$zzc] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzgs
    public final Object zza(int i, Object obj, Object obj2) {
        zzil<zzkn> zzil;
        int i2 = 1;
        switch (zzkp.zzbe[i - 1]) {
            case 1:
                return new zzkn();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzagr, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0001\u0001Л", new Object[]{"zzagq", zzkf.class});
            case 4:
                return zzagr;
            case 5:
                zzil<zzkn> zzil2 = zzbd;
                zzil<zzkn> zzil3 = zzil2;
                if (zzil2 == null) {
                    synchronized (zzkn.class) {
                        zzil<zzkn> zzil4 = zzbd;
                        zzil = zzil4;
                        if (zzil4 == null) {
                            ?? zzc = new zzgs.zzc(zzagr);
                            zzbd = zzc;
                            zzil = zzc;
                        }
                    }
                    zzil3 = zzil;
                }
                return zzil3;
            case 6:
                return Byte.valueOf(this.zzjy);
            case 7:
                if (obj == null) {
                    i2 = 0;
                }
                this.zzjy = (byte) i2;
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzkn zzjm() {
        return zzagr;
    }

    static {
        zzkn zzkn = new zzkn();
        zzagr = zzkn;
        zzgs.zza(zzkn.class, zzkn);
    }
}
