package com.google.android.gms.internal.firebase_messaging;

import java.util.Objects;

/* compiled from: com.google.firebase:firebase-iid@@20.1.5 */
final class zzq extends zzp {
    private final zzo zza = new zzo();

    zzq() {
    }

    @Override // com.google.android.gms.internal.firebase_messaging.zzp
    public final void zza(Throwable th, Throwable th2) {
        if (th2 != th) {
            Objects.requireNonNull(th2, "The suppressed exception cannot be null.");
            this.zza.zza(th, true).add(th2);
            return;
        }
        throw new IllegalArgumentException("Self suppression is not allowed.", th2);
    }
}
