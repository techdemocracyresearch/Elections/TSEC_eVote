package com.google.android.gms.internal.base;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

public final class zag extends Drawable {
    private static final zag zaa = new zag();
    private static final zaj zab = new zaj();

    private zag() {
    }

    public final void draw(Canvas canvas) {
    }

    public final int getOpacity() {
        return -2;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }

    public final Drawable.ConstantState getConstantState() {
        return zab;
    }
}
