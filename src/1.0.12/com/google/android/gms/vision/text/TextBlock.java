package com.google.android.gms.vision.text;

import android.graphics.Point;
import android.graphics.Rect;
import android.util.SparseArray;
import com.facebook.react.views.textinput.ReactEditTextInputConnectionWrapper;
import com.google.android.gms.internal.vision.zzac;
import com.google.android.gms.internal.vision.zzw;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision@@20.0.0 */
public class TextBlock implements Text {
    private Point[] cornerPoints;
    private zzac[] zzeb;
    private List<Line> zzec;
    private String zzed;
    private Rect zzee;

    TextBlock(SparseArray<zzac> sparseArray) {
        this.zzeb = new zzac[sparseArray.size()];
        int i = 0;
        while (true) {
            zzac[] zzacArr = this.zzeb;
            if (i < zzacArr.length) {
                zzacArr[i] = sparseArray.valueAt(i);
                i++;
            } else {
                return;
            }
        }
    }

    @Override // com.google.android.gms.vision.text.Text
    public String getLanguage() {
        String str = this.zzed;
        if (str != null) {
            return str;
        }
        HashMap hashMap = new HashMap();
        zzac[] zzacArr = this.zzeb;
        for (zzac zzac : zzacArr) {
            hashMap.put(zzac.zzed, Integer.valueOf((hashMap.containsKey(zzac.zzed) ? ((Integer) hashMap.get(zzac.zzed)).intValue() : 0) + 1));
        }
        String str2 = (String) ((Map.Entry) Collections.max(hashMap.entrySet(), new zza(this))).getKey();
        this.zzed = str2;
        if (str2 == null || str2.isEmpty()) {
            this.zzed = "und";
        }
        return this.zzed;
    }

    @Override // com.google.android.gms.vision.text.Text
    public String getValue() {
        zzac[] zzacArr = this.zzeb;
        if (zzacArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(zzacArr[0].zzem);
        for (int i = 1; i < this.zzeb.length; i++) {
            sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            sb.append(this.zzeb[i].zzem);
        }
        return sb.toString();
    }

    @Override // com.google.android.gms.vision.text.Text
    public Point[] getCornerPoints() {
        TextBlock textBlock;
        zzac[] zzacArr;
        TextBlock textBlock2 = this;
        if (textBlock2.cornerPoints == null) {
            char c = 0;
            if (textBlock2.zzeb.length == 0) {
                textBlock2.cornerPoints = new Point[0];
            } else {
                int i = Integer.MIN_VALUE;
                int i2 = Integer.MIN_VALUE;
                int i3 = Integer.MAX_VALUE;
                int i4 = Integer.MAX_VALUE;
                int i5 = 0;
                while (true) {
                    zzacArr = textBlock2.zzeb;
                    if (i5 >= zzacArr.length) {
                        break;
                    }
                    zzw zzw = zzacArr[i5].zzej;
                    zzw zzw2 = textBlock2.zzeb[c].zzej;
                    double sin = Math.sin(Math.toRadians((double) zzw2.zzeh));
                    double cos = Math.cos(Math.toRadians((double) zzw2.zzeh));
                    Point[] pointArr = new Point[4];
                    pointArr[c] = new Point(zzw.left, zzw.top);
                    pointArr[c].offset(-zzw2.left, -zzw2.top);
                    int i6 = (int) ((((double) pointArr[c].x) * cos) + (((double) pointArr[c].y) * sin));
                    int i7 = (int) ((((double) (-pointArr[0].x)) * sin) + (((double) pointArr[0].y) * cos));
                    pointArr[0].x = i6;
                    pointArr[0].y = i7;
                    pointArr[1] = new Point(zzw.width + i6, i7);
                    pointArr[2] = new Point(zzw.width + i6, zzw.height + i7);
                    pointArr[3] = new Point(i6, i7 + zzw.height);
                    i2 = i2;
                    for (int i8 = 0; i8 < 4; i8++) {
                        Point point = pointArr[i8];
                        i3 = Math.min(i3, point.x);
                        i = Math.max(i, point.x);
                        i4 = Math.min(i4, point.y);
                        i2 = Math.max(i2, point.y);
                    }
                    i5++;
                    c = 0;
                    textBlock2 = this;
                }
                zzw zzw3 = zzacArr[0].zzej;
                int i9 = zzw3.left;
                int i10 = zzw3.top;
                double sin2 = Math.sin(Math.toRadians((double) zzw3.zzeh));
                double cos2 = Math.cos(Math.toRadians((double) zzw3.zzeh));
                Point[] pointArr2 = {new Point(i3, i4), new Point(i, i4), new Point(i, i2), new Point(i3, i2)};
                for (int i11 = 0; i11 < 4; i11++) {
                    pointArr2[i11].x = (int) ((((double) pointArr2[i11].x) * cos2) - (((double) pointArr2[i11].y) * sin2));
                    pointArr2[i11].y = (int) ((((double) pointArr2[i11].x) * sin2) + (((double) pointArr2[i11].y) * cos2));
                    pointArr2[i11].offset(i9, i10);
                }
                textBlock = this;
                textBlock.cornerPoints = pointArr2;
                return textBlock.cornerPoints;
            }
        }
        textBlock = textBlock2;
        return textBlock.cornerPoints;
    }

    @Override // com.google.android.gms.vision.text.Text
    public List<? extends Text> getComponents() {
        if (this.zzeb.length == 0) {
            return new ArrayList(0);
        }
        if (this.zzec == null) {
            this.zzec = new ArrayList(this.zzeb.length);
            for (zzac zzac : this.zzeb) {
                this.zzec.add(new Line(zzac));
            }
        }
        return this.zzec;
    }

    @Override // com.google.android.gms.vision.text.Text
    public Rect getBoundingBox() {
        if (this.zzee == null) {
            this.zzee = zzc.zza(this);
        }
        return this.zzee;
    }
}
