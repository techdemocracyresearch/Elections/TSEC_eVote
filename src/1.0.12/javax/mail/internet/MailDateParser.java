package javax.mail.internet;

import java.text.ParseException;

/* access modifiers changed from: package-private */
/* compiled from: MailDateFormat */
public class MailDateParser {
    int index = 0;
    char[] orig = null;

    public MailDateParser(char[] cArr) {
        this.orig = cArr;
    }

    public void skipUntilNumber() throws ParseException {
        while (true) {
            try {
                char[] cArr = this.orig;
                int i = this.index;
                switch (cArr[i]) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        return;
                    default:
                        this.index = i + 1;
                }
            } catch (ArrayIndexOutOfBoundsException unused) {
                throw new ParseException("No Number Found", this.index);
            }
        }
    }

    public void skipWhiteSpace() {
        int length = this.orig.length;
        while (true) {
            int i = this.index;
            if (i < length) {
                char c = this.orig[i];
                if (c == '\t' || c == '\n' || c == '\r' || c == ' ') {
                    this.index = i + 1;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public int peekChar() throws ParseException {
        int i = this.index;
        char[] cArr = this.orig;
        if (i < cArr.length) {
            return cArr[i];
        }
        throw new ParseException("No more characters", this.index);
    }

    public void skipChar(char c) throws ParseException {
        int i = this.index;
        char[] cArr = this.orig;
        if (i >= cArr.length) {
            throw new ParseException("No more characters", this.index);
        } else if (cArr[i] == c) {
            this.index = i + 1;
        } else {
            throw new ParseException("Wrong char", this.index);
        }
    }

    public boolean skipIfChar(char c) throws ParseException {
        int i = this.index;
        char[] cArr = this.orig;
        if (i >= cArr.length) {
            throw new ParseException("No more characters", this.index);
        } else if (cArr[i] != c) {
            return false;
        } else {
            this.index = i + 1;
            return true;
        }
    }

    public int parseNumber() throws ParseException {
        int length = this.orig.length;
        boolean z = false;
        int i = 0;
        while (true) {
            int i2 = this.index;
            if (i2 < length) {
                switch (this.orig[i2]) {
                    case '0':
                        i *= 10;
                        break;
                    case '1':
                        i = (i * 10) + 1;
                        break;
                    case '2':
                        i = (i * 10) + 2;
                        break;
                    case '3':
                        i = (i * 10) + 3;
                        break;
                    case '4':
                        i = (i * 10) + 4;
                        break;
                    case '5':
                        i = (i * 10) + 5;
                        break;
                    case '6':
                        i = (i * 10) + 6;
                        break;
                    case '7':
                        i = (i * 10) + 7;
                        break;
                    case '8':
                        i = (i * 10) + 8;
                        break;
                    case '9':
                        i = (i * 10) + 9;
                        break;
                    default:
                        if (z) {
                            return i;
                        }
                        throw new ParseException("No Number found", this.index);
                }
                this.index = i2 + 1;
                z = true;
            } else if (z) {
                return i;
            } else {
                throw new ParseException("No Number found", this.index);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00d2, code lost:
        if (r3 == 'u') goto L_0x00d4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e A[Catch:{ ArrayIndexOutOfBoundsException -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0069 A[Catch:{ ArrayIndexOutOfBoundsException -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0088 A[Catch:{ ArrayIndexOutOfBoundsException -> 0x0155 }] */
    public int parseMonth() throws ParseException {
        try {
            char[] cArr = this.orig;
            int i = this.index;
            int i2 = i + 1;
            this.index = i2;
            char c = cArr[i];
            if (c != 'A') {
                if (c != 'D') {
                    if (c != 'F') {
                        if (c != 'J') {
                            if (c != 'S') {
                                if (c != 'a') {
                                    if (c != 'd') {
                                        if (c != 'f') {
                                            if (c != 'j') {
                                                if (c != 's') {
                                                    switch (c) {
                                                        case 'M':
                                                            int i3 = i2 + 1;
                                                            this.index = i3;
                                                            char c2 = cArr[i2];
                                                            if (c2 == 'A' || c2 == 'a') {
                                                                this.index = i3 + 1;
                                                                char c3 = cArr[i3];
                                                                if (c3 == 'R') {
                                                                    return 2;
                                                                }
                                                                if (c3 == 'r') {
                                                                    return 2;
                                                                }
                                                                if (c3 == 'Y' || c3 == 'y') {
                                                                    return 4;
                                                                }
                                                            }
                                                            throw new ParseException("Bad Month", this.index);
                                                        case 'N':
                                                            int i4 = i2 + 1;
                                                            this.index = i4;
                                                            char c4 = cArr[i2];
                                                            if (c4 == 'O' || c4 == 'o') {
                                                                this.index = i4 + 1;
                                                                char c5 = cArr[i4];
                                                                if (c5 == 'V' || c5 == 'v') {
                                                                    return 10;
                                                                }
                                                            }
                                                            throw new ParseException("Bad Month", this.index);
                                                        case 'O':
                                                            int i5 = i2 + 1;
                                                            this.index = i5;
                                                            char c6 = cArr[i2];
                                                            if (c6 == 'C' || c6 == 'c') {
                                                                this.index = i5 + 1;
                                                                char c7 = cArr[i5];
                                                                if (c7 == 'T' || c7 == 't') {
                                                                    return 9;
                                                                }
                                                            }
                                                            throw new ParseException("Bad Month", this.index);
                                                        default:
                                                            switch (c) {
                                                                case 'm':
                                                                    break;
                                                                case 'n':
                                                                    break;
                                                                case 'o':
                                                                    break;
                                                                default:
                                                                    throw new ParseException("Bad Month", this.index);
                                                            }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            int i6 = i2 + 1;
                            this.index = i6;
                            char c8 = cArr[i2];
                            if (c8 == 'E' || c8 == 'e') {
                                this.index = i6 + 1;
                                char c9 = cArr[i6];
                                if (c9 == 'P' || c9 == 'p') {
                                    return 8;
                                }
                            }
                            throw new ParseException("Bad Month", this.index);
                        }
                        int i7 = i2 + 1;
                        this.index = i7;
                        char c10 = cArr[i2];
                        if (c10 != 'A') {
                            if (c10 != 'U') {
                                if (c10 != 'a') {
                                }
                            }
                            this.index = i7 + 1;
                            char c11 = cArr[i7];
                            if (c11 == 'N') {
                                return 5;
                            }
                            if (c11 == 'n') {
                                return 5;
                            }
                            if (c11 == 'L' || c11 == 'l') {
                                return 6;
                            }
                            throw new ParseException("Bad Month", this.index);
                        }
                        this.index = i7 + 1;
                        char c12 = cArr[i7];
                        if (c12 == 'N' || c12 == 'n') {
                            return 0;
                        }
                        throw new ParseException("Bad Month", this.index);
                    }
                    int i8 = i2 + 1;
                    this.index = i8;
                    char c13 = cArr[i2];
                    if (c13 == 'E' || c13 == 'e') {
                        this.index = i8 + 1;
                        char c14 = cArr[i8];
                        if (c14 == 'B' || c14 == 'b') {
                            return 1;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                }
                int i9 = i2 + 1;
                this.index = i9;
                char c15 = cArr[i2];
                if (c15 == 'E' || c15 == 'e') {
                    this.index = i9 + 1;
                    char c16 = cArr[i9];
                    if (c16 == 'C' || c16 == 'c') {
                        return 11;
                    }
                }
                throw new ParseException("Bad Month", this.index);
            }
            int i10 = i2 + 1;
            this.index = i10;
            char c17 = cArr[i2];
            if (c17 != 'P') {
                if (c17 != 'p') {
                    if (c17 == 'U' || c17 == 'u') {
                        this.index = i10 + 1;
                        char c18 = cArr[i10];
                        if (c18 == 'G' || c18 == 'g') {
                            return 7;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                }
            }
            this.index = i10 + 1;
            char c19 = cArr[i10];
            if (c19 == 'R' || c19 == 'r') {
                return 3;
            }
        } catch (ArrayIndexOutOfBoundsException unused) {
        }
        throw new ParseException("Bad Month", this.index);
    }

    public int parseTimeZone() throws ParseException {
        int i = this.index;
        char[] cArr = this.orig;
        if (i < cArr.length) {
            char c = cArr[i];
            if (c == '+' || c == '-') {
                return parseNumericTimeZone();
            }
            return parseAlphaTimeZone();
        }
        throw new ParseException("No more characters", this.index);
    }

    public int parseNumericTimeZone() throws ParseException {
        boolean z;
        char[] cArr = this.orig;
        int i = this.index;
        this.index = i + 1;
        char c = cArr[i];
        if (c == '+') {
            z = true;
        } else if (c == '-') {
            z = false;
        } else {
            throw new ParseException("Bad Numeric TimeZone", this.index);
        }
        int parseNumber = parseNumber();
        int i2 = ((parseNumber / 100) * 60) + (parseNumber % 100);
        return z ? -i2 : i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public int parseAlphaTimeZone() throws ParseException {
        int i;
        try {
            char[] cArr = this.orig;
            int i2 = this.index;
            int i3 = i2 + 1;
            this.index = i3;
            boolean z = false;
            switch (cArr[i2]) {
                case 'C':
                case 'c':
                    i = 360;
                    z = true;
                    if (z) {
                        return i;
                    }
                    int i4 = this.index;
                    int i5 = i4 + 1;
                    this.index = i5;
                    char c = cArr[i4];
                    if (c == 'S' || c == 's') {
                        this.index = i5 + 1;
                        char c2 = cArr[i5];
                        if (c2 == 'T' || c2 == 't') {
                            return i;
                        }
                        throw new ParseException("Bad Alpha TimeZone", this.index);
                    } else if (c != 'D' && c != 'd') {
                        return i;
                    } else {
                        this.index = i5 + 1;
                        char c3 = cArr[i5];
                        if (c3 == 'T' || c3 != 't') {
                            return i - 60;
                        }
                        throw new ParseException("Bad Alpha TimeZone", this.index);
                    }
                case 'E':
                case 'e':
                    i = 300;
                    z = true;
                    if (z) {
                    }
                    break;
                case 'G':
                case 'g':
                    int i6 = i3 + 1;
                    this.index = i6;
                    char c4 = cArr[i3];
                    if (c4 == 'M' || c4 == 'm') {
                        this.index = i6 + 1;
                        char c5 = cArr[i6];
                        if (c5 != 'T') {
                            if (c5 == 't') {
                            }
                        }
                        i = 0;
                        if (z) {
                        }
                    }
                    throw new ParseException("Bad Alpha TimeZone", this.index);
                case 'M':
                case 'm':
                    i = 420;
                    z = true;
                    if (z) {
                    }
                    break;
                case 'P':
                case 'p':
                    i = 480;
                    z = true;
                    if (z) {
                    }
                    break;
                case 'U':
                case 'u':
                    this.index = i3 + 1;
                    char c6 = cArr[i3];
                    if (c6 != 'T') {
                        if (c6 != 't') {
                            throw new ParseException("Bad Alpha TimeZone", this.index);
                        }
                    }
                    i = 0;
                    if (z) {
                    }
                    break;
                default:
                    throw new ParseException("Bad Alpha TimeZone", this.index);
            }
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ParseException("Bad Alpha TimeZone", this.index);
        }
    }

    /* access modifiers changed from: package-private */
    public int getIndex() {
        return this.index;
    }
}
