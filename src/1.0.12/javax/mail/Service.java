package javax.mail;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Vector;
import javax.mail.event.ConnectionEvent;
import javax.mail.event.ConnectionListener;
import javax.mail.event.MailEvent;

public abstract class Service {
    private boolean connected = false;
    private Vector connectionListeners = null;
    protected boolean debug = false;
    private EventQueue q;
    private Object qLock = new Object();
    protected Session session;
    protected URLName url = null;

    /* access modifiers changed from: protected */
    public boolean protocolConnect(String str, int i, String str2, String str3) throws MessagingException {
        return false;
    }

    protected Service(Session session2, URLName uRLName) {
        this.session = session2;
        this.url = uRLName;
        this.debug = session2.getDebug();
    }

    public void connect() throws MessagingException {
        connect(null, null, null);
    }

    public void connect(String str, String str2, String str3) throws MessagingException {
        connect(str, -1, str2, str3);
    }

    public void connect(String str, String str2) throws MessagingException {
        connect(null, str, str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:64:0x0122 A[SYNTHETIC, Splitter:B:64:0x0122] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0143  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x014c  */
    public synchronized void connect(String str, int i, String str2, String str3) throws MessagingException {
        String str4;
        String str5;
        String str6;
        String str7;
        int i2;
        boolean z;
        boolean z2;
        String str8;
        String str9;
        AuthenticationFailedException authenticationFailedException;
        boolean z3;
        InetAddress inetAddress;
        String str10;
        String str11 = str2;
        synchronized (this) {
            if (!isConnected()) {
                URLName uRLName = this.url;
                if (uRLName != null) {
                    String protocol = uRLName.getProtocol();
                    str7 = str == null ? this.url.getHost() : str;
                    int port = i == -1 ? this.url.getPort() : i;
                    if (str11 == null) {
                        str11 = this.url.getUsername();
                        if (str3 == null) {
                            str10 = this.url.getPassword();
                            str5 = protocol;
                            i2 = port;
                            str6 = str10;
                            str4 = this.url.getFile();
                        }
                    } else if (str3 == null && str11.equals(this.url.getUsername())) {
                        str10 = this.url.getPassword();
                        str5 = protocol;
                        i2 = port;
                        str6 = str10;
                        str4 = this.url.getFile();
                    }
                    str10 = str3;
                    str5 = protocol;
                    i2 = port;
                    str6 = str10;
                    str4 = this.url.getFile();
                } else {
                    str7 = str;
                    str6 = str3;
                    i2 = i;
                    str5 = null;
                    str4 = null;
                }
                if (str5 != null) {
                    if (str7 == null) {
                        Session session2 = this.session;
                        str7 = session2.getProperty("mail." + str5 + ".host");
                    }
                    if (str11 == null) {
                        Session session3 = this.session;
                        str11 = session3.getProperty("mail." + str5 + ".user");
                    }
                }
                if (str7 == null) {
                    str7 = this.session.getProperty("mail.host");
                }
                if (str11 == null) {
                    str11 = this.session.getProperty("mail.user");
                }
                String str12 = str11;
                if (str12 == null) {
                    try {
                        str12 = System.getProperty("user.name");
                    } catch (SecurityException e) {
                        if (this.debug) {
                            e.printStackTrace(this.session.getDebugOut());
                        }
                    }
                }
                String str13 = str12;
                if (str6 != null || this.url == null) {
                    z = true;
                } else {
                    z = true;
                    z = true;
                    z = true;
                    z = true;
                    setURLName(new URLName(str5, str7, i2, str4, str13, null));
                    PasswordAuthentication passwordAuthentication = this.session.getPasswordAuthentication(getURLName());
                    if (passwordAuthentication == null) {
                        str9 = str13;
                        str8 = str6;
                        z2 = true;
                        z3 = protocolConnect(str7, i2, str9, str8);
                        authenticationFailedException = null;
                        if (!z3) {
                            try {
                                inetAddress = InetAddress.getByName(str7);
                            } catch (UnknownHostException unused) {
                                inetAddress = null;
                            }
                            PasswordAuthentication requestPasswordAuthentication = this.session.requestPasswordAuthentication(inetAddress, i2, str5, null, str9);
                            if (requestPasswordAuthentication != null) {
                                str9 = requestPasswordAuthentication.getUserName();
                                str8 = requestPasswordAuthentication.getPassword();
                                z3 = protocolConnect(str7, i2, str9, str8);
                            }
                        }
                        if (!z3) {
                            setURLName(new URLName(str5, str7, i2, str4, str9, str8));
                            if (z2) {
                                this.session.setPasswordAuthentication(getURLName(), new PasswordAuthentication(str9, str8));
                            }
                            setConnected(z);
                            int i3 = z ? 1 : 0;
                            int i4 = z ? 1 : 0;
                            int i5 = z ? 1 : 0;
                            int i6 = z ? 1 : 0;
                            int i7 = z ? 1 : 0;
                            int i8 = z ? 1 : 0;
                            int i9 = z ? 1 : 0;
                            notifyConnectionListeners(i3);
                        } else if (authenticationFailedException != null) {
                            throw authenticationFailedException;
                        } else {
                            throw new AuthenticationFailedException();
                        }
                    } else if (str13 == null) {
                        str13 = passwordAuthentication.getUserName();
                        str6 = passwordAuthentication.getPassword();
                    } else if (str13.equals(passwordAuthentication.getUserName())) {
                        str6 = passwordAuthentication.getPassword();
                    }
                }
                str9 = str13;
                str8 = str6;
                z2 = false;
                try {
                    z3 = protocolConnect(str7, i2, str9, str8);
                    authenticationFailedException = null;
                } catch (AuthenticationFailedException e2) {
                    authenticationFailedException = e2;
                    z3 = false;
                }
                if (!z3) {
                }
                if (!z3) {
                }
            } else {
                throw new IllegalStateException("already connected");
            }
        }
    }

    public synchronized boolean isConnected() {
        return this.connected;
    }

    /* access modifiers changed from: protected */
    public synchronized void setConnected(boolean z) {
        this.connected = z;
    }

    public synchronized void close() throws MessagingException {
        setConnected(false);
        notifyConnectionListeners(3);
    }

    public synchronized URLName getURLName() {
        URLName uRLName = this.url;
        if (uRLName == null || (uRLName.getPassword() == null && this.url.getFile() == null)) {
            return this.url;
        }
        return new URLName(this.url.getProtocol(), this.url.getHost(), this.url.getPort(), null, this.url.getUsername(), null);
    }

    /* access modifiers changed from: protected */
    public synchronized void setURLName(URLName uRLName) {
        this.url = uRLName;
    }

    public synchronized void addConnectionListener(ConnectionListener connectionListener) {
        if (this.connectionListeners == null) {
            this.connectionListeners = new Vector();
        }
        this.connectionListeners.addElement(connectionListener);
    }

    public synchronized void removeConnectionListener(ConnectionListener connectionListener) {
        Vector vector = this.connectionListeners;
        if (vector != null) {
            vector.removeElement(connectionListener);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void notifyConnectionListeners(int i) {
        if (this.connectionListeners != null) {
            queueEvent(new ConnectionEvent(this, i), this.connectionListeners);
        }
        if (i == 3) {
            terminateQueue();
        }
    }

    public String toString() {
        URLName uRLName = getURLName();
        if (uRLName != null) {
            return uRLName.toString();
        }
        return super.toString();
    }

    /* access modifiers changed from: protected */
    public void queueEvent(MailEvent mailEvent, Vector vector) {
        synchronized (this.qLock) {
            if (this.q == null) {
                this.q = new EventQueue();
            }
        }
        this.q.enqueue(mailEvent, (Vector) vector.clone());
    }

    /* access modifiers changed from: package-private */
    public static class TerminatorEvent extends MailEvent {
        private static final long serialVersionUID = 5542172141759168416L;

        TerminatorEvent() {
            super(new Object());
        }

        @Override // javax.mail.event.MailEvent
        public void dispatch(Object obj) {
            Thread.currentThread().interrupt();
        }
    }

    private void terminateQueue() {
        synchronized (this.qLock) {
            if (this.q != null) {
                Vector vector = new Vector();
                vector.setSize(1);
                this.q.enqueue(new TerminatorEvent(), vector);
                this.q = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        terminateQueue();
    }
}
