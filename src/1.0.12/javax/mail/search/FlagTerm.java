package javax.mail.search;

import javax.mail.Flags;
import javax.mail.Message;

public final class FlagTerm extends SearchTerm {
    private static final long serialVersionUID = -142991500302030647L;
    protected Flags flags;
    protected boolean set;

    public FlagTerm(Flags flags2, boolean z) {
        this.flags = flags2;
        this.set = z;
    }

    public Flags getFlags() {
        return (Flags) this.flags.clone();
    }

    public boolean getTestSet() {
        return this.set;
    }

    @Override // javax.mail.search.SearchTerm
    public boolean match(Message message) {
        Flags.Flag[] systemFlags;
        String[] userFlags;
        try {
            Flags flags2 = message.getFlags();
            if (!this.set) {
                for (Flags.Flag flag : this.flags.getSystemFlags()) {
                    if (flags2.contains(flag)) {
                        return false;
                    }
                }
                for (String str : this.flags.getUserFlags()) {
                    if (flags2.contains(str)) {
                        return false;
                    }
                }
                return true;
            } else if (flags2.contains(this.flags)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception unused) {
            return false;
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FlagTerm)) {
            return false;
        }
        FlagTerm flagTerm = (FlagTerm) obj;
        if (flagTerm.set != this.set || !flagTerm.flags.equals(this.flags)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.set ? this.flags.hashCode() : ~this.flags.hashCode();
    }
}
