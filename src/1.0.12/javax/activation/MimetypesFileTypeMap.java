package javax.activation;

import com.sun.activation.registries.LogSupport;
import com.sun.activation.registries.MimeTypeFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Vector;

public class MimetypesFileTypeMap extends FileTypeMap {
    private static final int PROG = 0;
    private static MimeTypeFile defDB = null;
    private static String defaultType = "application/octet-stream";
    private MimeTypeFile[] DB;

    public MimetypesFileTypeMap() {
        Vector vector = new Vector(5);
        vector.addElement(null);
        LogSupport.log("MimetypesFileTypeMap: load HOME");
        try {
            String property = System.getProperty("user.home");
            if (property != null) {
                MimeTypeFile loadFile = loadFile(String.valueOf(property) + File.separator + ".mime.types");
                if (loadFile != null) {
                    vector.addElement(loadFile);
                }
            }
        } catch (SecurityException unused) {
        }
        LogSupport.log("MimetypesFileTypeMap: load SYS");
        try {
            MimeTypeFile loadFile2 = loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "mime.types");
            if (loadFile2 != null) {
                vector.addElement(loadFile2);
            }
        } catch (SecurityException unused2) {
        }
        LogSupport.log("MimetypesFileTypeMap: load JAR");
        loadAllResources(vector, "mime.types");
        LogSupport.log("MimetypesFileTypeMap: load DEF");
        synchronized (MimetypesFileTypeMap.class) {
            if (defDB == null) {
                defDB = loadResource("/mimetypes.default");
            }
        }
        MimeTypeFile mimeTypeFile = defDB;
        if (mimeTypeFile != null) {
            vector.addElement(mimeTypeFile);
        }
        MimeTypeFile[] mimeTypeFileArr = new MimeTypeFile[vector.size()];
        this.DB = mimeTypeFileArr;
        vector.copyInto(mimeTypeFileArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0046, code lost:
        if (r2 != null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0083, code lost:
        if (r2 == null) goto L_0x0086;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a A[Catch:{ IOException -> 0x006c, SecurityException -> 0x0052, all -> 0x0050, all -> 0x0087 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006b A[Catch:{ IOException -> 0x006c, SecurityException -> 0x0052, all -> 0x0050, all -> 0x0087 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0074 A[Catch:{ IOException -> 0x006c, SecurityException -> 0x0052, all -> 0x0050, all -> 0x0087 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008b A[SYNTHETIC, Splitter:B:41:0x008b] */
    private MimeTypeFile loadResource(String str) {
        Throwable th;
        IOException e;
        InputStream inputStream;
        SecurityException e2;
        InputStream inputStream2 = null;
        try {
            inputStream = SecuritySupport.getResourceAsStream(getClass(), str);
            if (inputStream != null) {
                try {
                    MimeTypeFile mimeTypeFile = new MimeTypeFile(inputStream);
                    if (LogSupport.isLoggable()) {
                        LogSupport.log("MimetypesFileTypeMap: successfully loaded mime types file: " + str);
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused) {
                        }
                    }
                    return mimeTypeFile;
                } catch (IOException e3) {
                    e = e3;
                    if (LogSupport.isLoggable()) {
                        LogSupport.log("MimetypesFileTypeMap: can't load " + str, e);
                    }
                } catch (SecurityException e4) {
                    e2 = e4;
                    if (LogSupport.isLoggable()) {
                        LogSupport.log("MimetypesFileTypeMap: can't load " + str, e2);
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused2) {
                        }
                    }
                    return null;
                }
            } else if (LogSupport.isLoggable()) {
                LogSupport.log("MimetypesFileTypeMap: not loading mime types file: " + str);
            }
        } catch (IOException e5) {
            e = e5;
            inputStream = null;
            if (LogSupport.isLoggable()) {
            }
        } catch (SecurityException e6) {
            e2 = e6;
            inputStream = null;
            if (LogSupport.isLoggable()) {
            }
            if (inputStream != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            inputStream2 = inputStream;
            if (inputStream2 != null) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ce, code lost:
        if (r5 == null) goto L_0x00d7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a3 A[Catch:{ IOException -> 0x00b5, SecurityException -> 0x0099, all -> 0x0097, all -> 0x00dc }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b4 A[Catch:{ IOException -> 0x00b5, SecurityException -> 0x0099, all -> 0x0097, all -> 0x00dc }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00bf A[Catch:{ IOException -> 0x00b5, SecurityException -> 0x0099, all -> 0x0097, all -> 0x00dc }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00e0 A[SYNTHETIC, Splitter:B:66:0x00e0] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:85:? A[RETURN, SYNTHETIC] */
    private void loadAllResources(Vector vector, String str) {
        Exception e;
        URL[] urlArr;
        Throwable th;
        IOException e2;
        SecurityException e3;
        int i = 0;
        try {
            ClassLoader contextClassLoader = SecuritySupport.getContextClassLoader();
            if (contextClassLoader == null) {
                contextClassLoader = getClass().getClassLoader();
            }
            if (contextClassLoader != null) {
                urlArr = SecuritySupport.getResources(contextClassLoader, str);
            } else {
                urlArr = SecuritySupport.getSystemResources(str);
            }
            if (urlArr != null) {
                if (LogSupport.isLoggable()) {
                    LogSupport.log("MimetypesFileTypeMap: getResources");
                }
                int i2 = 0;
                while (i < urlArr.length) {
                    try {
                        URL url = urlArr[i];
                        InputStream inputStream = null;
                        if (LogSupport.isLoggable()) {
                            LogSupport.log("MimetypesFileTypeMap: URL " + url);
                        }
                        int i3 = 1;
                        try {
                            inputStream = SecuritySupport.openStream(url);
                            if (inputStream != null) {
                                vector.addElement(new MimeTypeFile(inputStream));
                                try {
                                    if (LogSupport.isLoggable()) {
                                        LogSupport.log("MimetypesFileTypeMap: successfully loaded mime types from URL: " + url);
                                    }
                                    i2 = 1;
                                } catch (IOException e4) {
                                    e2 = e4;
                                    if (LogSupport.isLoggable()) {
                                    }
                                } catch (SecurityException e5) {
                                    e3 = e5;
                                    if (LogSupport.isLoggable()) {
                                    }
                                    if (inputStream != null) {
                                    }
                                    i2 = i3;
                                    i++;
                                } catch (Throwable th2) {
                                    th = th2;
                                    if (inputStream != null) {
                                    }
                                    throw th;
                                }
                            } else if (LogSupport.isLoggable()) {
                                LogSupport.log("MimetypesFileTypeMap: not loading mime types from URL: " + url);
                            }
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException unused) {
                                }
                            }
                        } catch (IOException e6) {
                            i3 = i2;
                            e2 = e6;
                            if (LogSupport.isLoggable()) {
                                LogSupport.log("MimetypesFileTypeMap: can't load " + url, e2);
                            }
                        } catch (SecurityException e7) {
                            i3 = i2;
                            e3 = e7;
                            if (LogSupport.isLoggable()) {
                                LogSupport.log("MimetypesFileTypeMap: can't load " + url, e3);
                            }
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException unused2) {
                                } catch (Exception e8) {
                                    e = e8;
                                    i = i3;
                                    if (LogSupport.isLoggable()) {
                                    }
                                    if (i != 0) {
                                    }
                                }
                            }
                            i2 = i3;
                            i++;
                        } catch (Throwable th3) {
                            th = th3;
                            if (inputStream != null) {
                            }
                            throw th;
                        }
                        i++;
                    } catch (Exception e9) {
                        e = e9;
                        i = i2;
                        if (LogSupport.isLoggable()) {
                        }
                        if (i != 0) {
                        }
                    }
                }
                i = i2;
            }
        } catch (Exception e10) {
            e = e10;
            if (LogSupport.isLoggable()) {
                LogSupport.log("MimetypesFileTypeMap: can't load " + str, e);
            }
            if (i != 0) {
            }
        }
        if (i != 0) {
            LogSupport.log("MimetypesFileTypeMap: !anyLoaded");
            MimeTypeFile loadResource = loadResource("/" + str);
            if (loadResource != null) {
                vector.addElement(loadResource);
            }
        }
    }

    private MimeTypeFile loadFile(String str) {
        try {
            return new MimeTypeFile(str);
        } catch (IOException unused) {
            return null;
        }
    }

    public MimetypesFileTypeMap(String str) throws IOException {
        this();
        this.DB[0] = new MimeTypeFile(str);
    }

    public MimetypesFileTypeMap(InputStream inputStream) {
        this();
        try {
            this.DB[0] = new MimeTypeFile(inputStream);
        } catch (IOException unused) {
        }
    }

    public synchronized void addMimeTypes(String str) {
        MimeTypeFile[] mimeTypeFileArr = this.DB;
        if (mimeTypeFileArr[0] == null) {
            mimeTypeFileArr[0] = new MimeTypeFile();
        }
        this.DB[0].appendToRegistry(str);
    }

    @Override // javax.activation.FileTypeMap
    public String getContentType(File file) {
        return getContentType(file.getName());
    }

    @Override // javax.activation.FileTypeMap
    public synchronized String getContentType(String str) {
        String mIMETypeString;
        int lastIndexOf = str.lastIndexOf(".");
        if (lastIndexOf < 0) {
            return defaultType;
        }
        String substring = str.substring(lastIndexOf + 1);
        if (substring.length() == 0) {
            return defaultType;
        }
        int i = 0;
        while (true) {
            MimeTypeFile[] mimeTypeFileArr = this.DB;
            if (i >= mimeTypeFileArr.length) {
                return defaultType;
            }
            if (mimeTypeFileArr[i] != null && (mIMETypeString = mimeTypeFileArr[i].getMIMETypeString(substring)) != null) {
                return mIMETypeString;
            }
            i++;
        }
    }
}
