package me.furtado.smsretriever;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

/* access modifiers changed from: package-private */
public final class SmsHelper {
    private static final String TASK_FAILURE_ERROR_MESSAGE = "Task failed.";
    private static final String TASK_FAILURE_ERROR_TYPE = "TASK_FAILURE_ERROR_TYPE";
    private final ReactApplicationContext mContext;
    private final OnFailureListener mOnFailureListener = new OnFailureListener() {
        /* class me.furtado.smsretriever.SmsHelper.AnonymousClass2 */

        @Override // com.google.android.gms.tasks.OnFailureListener
        public void onFailure(Exception exc) {
            SmsHelper.this.unregisterReceiverIfNeeded();
            SmsHelper.this.promiseReject(SmsHelper.TASK_FAILURE_ERROR_TYPE, SmsHelper.TASK_FAILURE_ERROR_MESSAGE);
        }
    };
    private final OnSuccessListener<Void> mOnSuccessListener = new OnSuccessListener<Void>() {
        /* class me.furtado.smsretriever.SmsHelper.AnonymousClass1 */

        public void onSuccess(Void r2) {
            SmsHelper.this.promiseResolve(Boolean.valueOf(SmsHelper.this.tryToRegisterReceiver()));
        }
    };
    private Promise mPromise;
    private BroadcastReceiver mReceiver;

    SmsHelper(ReactApplicationContext reactApplicationContext) {
        this.mContext = reactApplicationContext;
    }

    /* access modifiers changed from: package-private */
    public void startRetriever(Promise promise) {
        this.mPromise = promise;
        if (!GooglePlayServicesHelper.isAvailable(this.mContext)) {
            promiseReject("UNAVAILABLE_ERROR_TYPE", "Google Play Services is not available.");
        } else if (!GooglePlayServicesHelper.hasSupportedVersion(this.mContext)) {
            promiseReject("UNSUPORTED_VERSION_ERROR_TYPE", "The device version of Google Play Services is not supported.");
        } else {
            Task<Void> startSmsRetriever = SmsRetriever.getClient(this.mContext).startSmsRetriever();
            startSmsRetriever.addOnSuccessListener(this.mOnSuccessListener);
            startSmsRetriever.addOnFailureListener(this.mOnFailureListener);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean tryToRegisterReceiver() {
        this.mReceiver = new SmsBroadcastReceiver(this.mContext);
        try {
            this.mContext.registerReceiver(this.mReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void unregisterReceiverIfNeeded() {
        BroadcastReceiver broadcastReceiver = this.mReceiver;
        if (broadcastReceiver != null) {
            try {
                this.mContext.unregisterReceiver(broadcastReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void promiseResolve(Object obj) {
        Promise promise = this.mPromise;
        if (promise != null) {
            promise.resolve(obj);
            this.mPromise = null;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void promiseReject(String str, String str2) {
        Promise promise = this.mPromise;
        if (promise != null) {
            promise.reject(str, str2);
            this.mPromise = null;
        }
    }
}
