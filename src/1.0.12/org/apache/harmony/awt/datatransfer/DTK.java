package org.apache.harmony.awt.datatransfer;

import com.bumptech.glide.load.Key;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.SystemFlavorMap;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.peer.DragSourceContextPeer;
import java.awt.dnd.peer.DropTargetContextPeer;
import java.nio.charset.Charset;
import org.apache.harmony.awt.ContextStorage;
import org.apache.harmony.awt.internal.nls.Messages;
import org.apache.harmony.misc.SystemUtils;

public abstract class DTK {
    protected final DataTransferThread dataTransferThread;
    private NativeClipboard nativeClipboard = null;
    private NativeClipboard nativeSelection = null;
    protected SystemFlavorMap systemFlavorMap;

    public abstract DragSourceContextPeer createDragSourceContextPeer(DragGestureEvent dragGestureEvent);

    public abstract DropTargetContextPeer createDropTargetContextPeer(DropTargetContext dropTargetContext);

    public String getDefaultCharset() {
        return "unicode";
    }

    public abstract void initDragAndDrop();

    /* access modifiers changed from: protected */
    public abstract NativeClipboard newNativeClipboard();

    /* access modifiers changed from: protected */
    public abstract NativeClipboard newNativeSelection();

    public abstract void runEventLoop();

    protected DTK() {
        DataTransferThread dataTransferThread2 = new DataTransferThread(this);
        this.dataTransferThread = dataTransferThread2;
        dataTransferThread2.start();
    }

    public static DTK getDTK() {
        synchronized (ContextStorage.getContextLock()) {
            if (ContextStorage.shutdownPending()) {
                return null;
            }
            DTK dtk = ContextStorage.getDTK();
            if (dtk == null) {
                dtk = createDTK();
                ContextStorage.setDTK(dtk);
            }
            return dtk;
        }
    }

    public synchronized SystemFlavorMap getSystemFlavorMap() {
        return this.systemFlavorMap;
    }

    public synchronized void setSystemFlavorMap(SystemFlavorMap systemFlavorMap2) {
        this.systemFlavorMap = systemFlavorMap2;
    }

    public NativeClipboard getNativeClipboard() {
        if (this.nativeClipboard == null) {
            this.nativeClipboard = newNativeClipboard();
        }
        return this.nativeClipboard;
    }

    public NativeClipboard getNativeSelection() {
        if (this.nativeSelection == null) {
            this.nativeSelection = newNativeSelection();
        }
        return this.nativeSelection;
    }

    private static DTK createDTK() {
        String str;
        int os = SystemUtils.getOS();
        if (os == 1) {
            str = "org.apache.harmony.awt.datatransfer.windows.WinDTK";
        } else if (os == 2) {
            str = "org.apache.harmony.awt.datatransfer.linux.LinuxDTK";
        } else {
            throw new RuntimeException(Messages.getString("awt.4E"));
        }
        try {
            return (DTK) Class.forName(str).newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: protected */
    public String[] getCharsets() {
        return new String[]{"UTF-16", Key.STRING_CHARSET_NAME, "unicode", "ISO-8859-1", "US-ASCII"};
    }

    public void initSystemFlavorMap(SystemFlavorMap systemFlavorMap2) {
        String[] charsets = getCharsets();
        appendSystemFlavorMap(systemFlavorMap2, DataFlavor.stringFlavor, "text/plain");
        appendSystemFlavorMap(systemFlavorMap2, charsets, "plain", "text/plain");
        appendSystemFlavorMap(systemFlavorMap2, charsets, "html", "text/html");
        appendSystemFlavorMap(systemFlavorMap2, DataProvider.urlFlavor, "application/x-java-url");
        appendSystemFlavorMap(systemFlavorMap2, charsets, "uri-list", "application/x-java-url");
        appendSystemFlavorMap(systemFlavorMap2, DataFlavor.javaFileListFlavor, "application/x-java-file-list");
        appendSystemFlavorMap(systemFlavorMap2, DataFlavor.imageFlavor, "image/x-java-image");
    }

    /* access modifiers changed from: protected */
    public void appendSystemFlavorMap(SystemFlavorMap systemFlavorMap2, DataFlavor dataFlavor, String str) {
        systemFlavorMap2.addFlavorForUnencodedNative(str, dataFlavor);
        systemFlavorMap2.addUnencodedNativeForFlavor(dataFlavor, str);
    }

    /* access modifiers changed from: protected */
    public void appendSystemFlavorMap(SystemFlavorMap systemFlavorMap2, String[] strArr, String str, String str2) {
        TextFlavor.addUnicodeClasses(systemFlavorMap2, str2, str);
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] != null && Charset.isSupported(strArr[i])) {
                TextFlavor.addCharsetClasses(systemFlavorMap2, str2, str, strArr[i]);
            }
        }
    }
}
