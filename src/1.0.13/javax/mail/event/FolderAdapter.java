package javax.mail.event;

public abstract class FolderAdapter implements FolderListener {
    @Override // javax.mail.event.FolderListener
    public void folderCreated(FolderEvent folderEvent) {
    }

    @Override // javax.mail.event.FolderListener
    public void folderDeleted(FolderEvent folderEvent) {
    }

    @Override // javax.mail.event.FolderListener
    public void folderRenamed(FolderEvent folderEvent) {
    }
}
