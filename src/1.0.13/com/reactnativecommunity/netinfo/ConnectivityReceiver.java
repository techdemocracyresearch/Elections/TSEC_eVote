package com.reactnativecommunity.netinfo;

import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import androidx.core.net.ConnectivityManagerCompat;
import com.brentvatne.react.ReactVideoViewManager;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.reactnativecommunity.netinfo.types.CellularGeneration;
import com.reactnativecommunity.netinfo.types.ConnectionType;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Locale;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/* access modifiers changed from: package-private */
public abstract class ConnectivityReceiver {
    @Nullable
    private CellularGeneration mCellularGeneration = null;
    @Nonnull
    private ConnectionType mConnectionType = ConnectionType.UNKNOWN;
    private final ConnectivityManager mConnectivityManager;
    private boolean mIsInternetReachable = false;
    private Boolean mIsInternetReachableOverride;
    private final ReactApplicationContext mReactContext;
    private final TelephonyManager mTelephonyManager;
    private final WifiManager mWifiManager;

    /* access modifiers changed from: package-private */
    public abstract void register();

    /* access modifiers changed from: package-private */
    public abstract void unregister();

    ConnectivityReceiver(ReactApplicationContext reactApplicationContext) {
        this.mReactContext = reactApplicationContext;
        this.mConnectivityManager = (ConnectivityManager) reactApplicationContext.getSystemService("connectivity");
        this.mWifiManager = (WifiManager) reactApplicationContext.getApplicationContext().getSystemService("wifi");
        this.mTelephonyManager = (TelephonyManager) reactApplicationContext.getSystemService("phone");
    }

    public void getCurrentState(@Nullable String str, Promise promise) {
        promise.resolve(createConnectivityEventMap(str));
    }

    public void setIsInternetReachableOverride(boolean z) {
        this.mIsInternetReachableOverride = Boolean.valueOf(z);
        updateConnectivity(this.mConnectionType, this.mCellularGeneration, this.mIsInternetReachable);
    }

    public void clearIsInternetReachableOverride() {
        this.mIsInternetReachableOverride = null;
    }

    /* access modifiers changed from: package-private */
    public ReactApplicationContext getReactContext() {
        return this.mReactContext;
    }

    /* access modifiers changed from: package-private */
    public ConnectivityManager getConnectivityManager() {
        return this.mConnectivityManager;
    }

    /* access modifiers changed from: package-private */
    public void updateConnectivity(@Nonnull ConnectionType connectionType, @Nullable CellularGeneration cellularGeneration, boolean z) {
        Boolean bool = this.mIsInternetReachableOverride;
        if (bool != null) {
            z = bool.booleanValue();
        }
        boolean z2 = true;
        boolean z3 = connectionType != this.mConnectionType;
        boolean z4 = cellularGeneration != this.mCellularGeneration;
        if (z == this.mIsInternetReachable) {
            z2 = false;
        }
        if (z3 || z4 || z2) {
            this.mConnectionType = connectionType;
            this.mCellularGeneration = cellularGeneration;
            this.mIsInternetReachable = z;
            sendConnectivityChangedEvent();
        }
    }

    private void sendConnectivityChangedEvent() {
        ((DeviceEventManagerModule.RCTDeviceEventEmitter) getReactContext().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)).emit("netInfo.networkStatusDidChange", createConnectivityEventMap(null));
    }

    private WritableMap createConnectivityEventMap(@Nullable String str) {
        String str2;
        WritableMap createMap = Arguments.createMap();
        if (NetInfoUtils.isAccessWifiStatePermissionGranted(getReactContext())) {
            createMap.putBoolean("isWifiEnabled", this.mWifiManager.isWifiEnabled());
        }
        if (str != null) {
            str2 = str;
        } else {
            str2 = this.mConnectionType.label;
        }
        createMap.putString(ReactVideoViewManager.PROP_SRC_TYPE, str2);
        boolean z = true;
        boolean z2 = !this.mConnectionType.equals(ConnectionType.NONE) && !this.mConnectionType.equals(ConnectionType.UNKNOWN);
        createMap.putBoolean("isConnected", z2);
        if (!this.mIsInternetReachable || (str != null && !str.equals(this.mConnectionType.label))) {
            z = false;
        }
        createMap.putBoolean("isInternetReachable", z);
        if (str == null) {
            str = this.mConnectionType.label;
        }
        WritableMap createDetailsMap = createDetailsMap(str);
        if (z2) {
            createDetailsMap.putBoolean("isConnectionExpensive", ConnectivityManagerCompat.isActiveNetworkMetered(getConnectivityManager()));
        }
        createMap.putMap("details", createDetailsMap);
        return createMap;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(14:8|9|(1:13)|14|15|(1:17)|18|19|20|21|(1:23)|24|25|(2:26|27)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(15:8|9|(1:13)|14|15|(1:17)|18|19|20|21|(1:23)|24|25|26|27) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0046 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0051 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0060 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x006f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x008c */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004c A[Catch:{ Exception -> 0x0051 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0066 A[Catch:{ Exception -> 0x006f }] */
    private WritableMap createDetailsMap(@Nonnull String str) {
        WifiInfo connectionInfo;
        WritableMap createMap = Arguments.createMap();
        str.hashCode();
        if (str.equals("cellular")) {
            CellularGeneration cellularGeneration = this.mCellularGeneration;
            if (cellularGeneration != null) {
                createMap.putString("cellularGeneration", cellularGeneration.label);
            }
            String networkOperatorName = this.mTelephonyManager.getNetworkOperatorName();
            if (networkOperatorName != null) {
                createMap.putString("carrier", networkOperatorName);
            }
        } else if (str.equals("wifi") && NetInfoUtils.isAccessWifiStatePermissionGranted(getReactContext()) && (connectionInfo = this.mWifiManager.getConnectionInfo()) != null) {
            String ssid = connectionInfo.getSSID();
            if (ssid != null && !ssid.contains("<unknown ssid>")) {
                createMap.putString("ssid", ssid.replace("\"", ""));
            }
            String bssid = connectionInfo.getBSSID();
            if (bssid != null) {
                createMap.putString("bssid", bssid);
            }
            createMap.putInt("strength", WifiManager.calculateSignalLevel(connectionInfo.getRssi(), 100));
            if (Build.VERSION.SDK_INT >= 21) {
                createMap.putInt("frequency", connectionInfo.getFrequency());
            }
            byte[] byteArray = BigInteger.valueOf((long) connectionInfo.getIpAddress()).toByteArray();
            NetInfoUtils.reverseByteArray(byteArray);
            createMap.putString("ipAddress", InetAddress.getByAddress(byteArray).getHostAddress());
            try {
                byte[] byteArray2 = BigInteger.valueOf((long) connectionInfo.getIpAddress()).toByteArray();
                NetInfoUtils.reverseByteArray(byteArray2);
                int networkPrefixLength = -1 << (32 - NetworkInterface.getByInetAddress(InetAddress.getByAddress(byteArray2)).getInterfaceAddresses().get(1).getNetworkPrefixLength());
                createMap.putString("subnet", String.format(Locale.US, "%d.%d.%d.%d", Integer.valueOf((networkPrefixLength >> 24) & 255), Integer.valueOf((networkPrefixLength >> 16) & 255), Integer.valueOf((networkPrefixLength >> 8) & 255), Integer.valueOf(networkPrefixLength & 255)));
            } catch (Exception unused) {
            }
        }
        return createMap;
    }
}
