package com.learnium.RNDeviceInfo.resolver;

import android.content.Context;
import androidx.core.os.EnvironmentCompat;
import java.lang.reflect.InvocationTargetException;

public class DeviceIdResolver {
    private final Context context;

    public DeviceIdResolver(Context context2) {
        this.context = context2;
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x000c */
    public String getInstanceIdSync() {
        try {
            return getFirebaseInstanceId();
        } catch (ClassNotFoundException unknown) {
            try {
            } catch (ClassNotFoundException unused) {
                System.err.println("Can't generate id. Please add com.google.firebase:firebase-iid to your project.");
                return EnvironmentCompat.MEDIA_UNKNOWN;
            } catch (IllegalAccessException | NoSuchMethodException | SecurityException | InvocationTargetException unused2) {
                System.err.println("N/A: Unsupported version of com.google.android.gms.iid in your project.");
                System.err.println("Can't generate id. Please add com.google.firebase:firebase-iid to your project.");
                return EnvironmentCompat.MEDIA_UNKNOWN;
            }
        } catch (IllegalAccessException | NoSuchMethodException | SecurityException | InvocationTargetException unused3) {
            System.err.println("N/A: Unsupported version of com.google.firebase:firebase-iid in your project.");
        }
        return getGmsInstanceId();
    }

    /* access modifiers changed from: package-private */
    public String getGmsInstanceId() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Object invoke = Class.forName("com.google.android.gms.iid.InstanceID").getDeclaredMethod("getInstance", Context.class).invoke(null, this.context);
        return (String) invoke.getClass().getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
    }

    /* access modifiers changed from: package-private */
    public String getFirebaseInstanceId() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Object invoke = Class.forName("com.google.firebase.iid.FirebaseInstanceId").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        return (String) invoke.getClass().getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
    }
}
