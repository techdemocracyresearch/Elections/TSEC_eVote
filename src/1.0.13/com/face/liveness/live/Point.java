package com.face.liveness.live;

public class Point {
    private final double y;
    private final double z;

    public Point(double d, double d2) {
        this.z = d2;
        this.y = d;
    }

    public double getZ() {
        return this.z;
    }

    public double getY() {
        return this.y;
    }

    public double distanceL1(Point point) {
        return Math.abs(this.z - point.getZ()) + Math.abs(this.y - point.getY());
    }

    public double distanceL2(Point point) {
        return Math.pow(this.z - point.getZ(), 2.0d) + Math.pow(this.y - point.getY(), 2.0d);
    }

    public double distanceLM(Point point) {
        return Math.max(Math.abs(this.z - point.getZ()), Math.abs(this.y - point.getY()));
    }

    public String toString() {
        return "(" + this.y + "," + this.z + ")";
    }
}
