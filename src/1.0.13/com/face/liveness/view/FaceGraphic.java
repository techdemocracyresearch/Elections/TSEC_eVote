package com.face.liveness.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import androidx.core.internal.view.SupportMenu;
import androidx.core.view.InputDeviceCompat;
import com.face.liveness.view.GraphicOverlay;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;

public class FaceGraphic extends GraphicOverlay.Graphic {
    private static final float BOX_STROKE_WIDTH = 5.0f;
    private static final int[] COLOR_CHOICES = {-16776961, -16711681, -16711936, -65281, SupportMenu.CATEGORY_MASK, -1, InputDeviceCompat.SOURCE_ANY};
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static final float ID_TEXT_SIZE = 40.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static int currentColorIndex = 0;
    private final Paint boxPaint;
    private final Paint facePositionPaint;
    private int facing;
    private volatile FirebaseVisionFace firebaseVisionFace;
    private final Paint idPaint;

    public FaceGraphic(GraphicOverlay graphicOverlay) {
        super(graphicOverlay);
        int[] iArr = COLOR_CHOICES;
        int length = (currentColorIndex + 1) % iArr.length;
        currentColorIndex = length;
        int i = iArr[length];
        Paint paint = new Paint();
        this.facePositionPaint = paint;
        paint.setColor(i);
        Paint paint2 = new Paint();
        this.idPaint = paint2;
        paint2.setColor(SupportMenu.CATEGORY_MASK);
        Paint paint3 = new Paint();
        this.boxPaint = paint3;
        paint3.setColor(-16711936);
        paint3.setStyle(Paint.Style.FILL);
        paint3.setAlpha(50);
    }

    public void updateFace(FirebaseVisionFace firebaseVisionFace2, int i) {
        this.firebaseVisionFace = firebaseVisionFace2;
        this.facing = i;
        postInvalidate();
    }

    @Override // com.face.liveness.view.GraphicOverlay.Graphic
    public void draw(Canvas canvas) {
        FirebaseVisionFace firebaseVisionFace2 = this.firebaseVisionFace;
        if (firebaseVisionFace2 != null) {
            float translateX = translateX((float) firebaseVisionFace2.getBoundingBox().centerX());
            float translateY = translateY((float) firebaseVisionFace2.getBoundingBox().centerY());
            float scaleX = scaleX(((float) firebaseVisionFace2.getBoundingBox().width()) / 2.0f);
            float scaleY = scaleY(((float) firebaseVisionFace2.getBoundingBox().height()) / 2.0f);
            canvas.drawRect(translateX - scaleX, translateY - scaleY, translateX + scaleX, translateY + scaleY, this.boxPaint);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 0);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 1);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 3);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 5);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 4);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 6);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 7);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 9);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 10);
            drawLandmarkPosition(canvas, firebaseVisionFace2, 11);
            for (FirebaseVisionPoint firebaseVisionPoint : firebaseVisionFace2.getContour(1).getPoints()) {
                canvas.drawCircle(translateX(firebaseVisionPoint.getX().floatValue()), translateY(firebaseVisionPoint.getY().floatValue()), BOX_STROKE_WIDTH, this.idPaint);
            }
        }
    }

    private void drawLandmarkPosition(Canvas canvas, FirebaseVisionFace firebaseVisionFace2, int i) {
        FirebaseVisionFaceLandmark landmark = firebaseVisionFace2.getLandmark(i);
        if (landmark != null) {
            FirebaseVisionPoint position = landmark.getPosition();
            canvas.drawCircle(translateX(position.getX().floatValue()), translateY(position.getY().floatValue()), FACE_POSITION_RADIUS, this.idPaint);
        }
    }
}
