package com.face.liveness.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;
import com.facebook.imagepipeline.common.RotationOptions;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.Landmark;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Utils {
    private static String TAG = "Utils";
    private static boolean isResized = false;
    private static double leftEyeOpenProbability = -1.0d;
    private static double rightEyeOpenProbability = -1.0d;

    public static void showLogs(String str, String str2) {
        Log.v(str, str2);
    }

    public static Camera.Size getResolution(List<Camera.Size> list, float f) {
        ArrayList arrayList = new ArrayList();
        for (Camera.Size size : list) {
            if (size.width != size.height && ((float) size.width) / ((float) size.height) == f) {
                arrayList.add(size);
            }
        }
        if (arrayList.isEmpty()) {
            for (Camera.Size size2 : list) {
                if (size2.width != size2.height) {
                    arrayList.add(size2);
                }
            }
        }
        Collections.sort(arrayList, new Comparator<Camera.Size>() {
            /* class com.face.liveness.utils.Utils.AnonymousClass1 */

            public int compare(Camera.Size size, Camera.Size size2) {
                return (size.width * size.height) - (size2.width * size2.height);
            }
        });
        Collections.reverse(arrayList);
        if (arrayList.size() > 3) {
            return (Camera.Size) arrayList.get(3);
        }
        if (arrayList.size() > 2) {
            return (Camera.Size) arrayList.get(2);
        }
        return (Camera.Size) arrayList.get(1);
    }

    public static boolean isEyeBlinked(FirebaseVisionFace firebaseVisionFace) {
        float leftEyeOpenProbability2 = firebaseVisionFace.getLeftEyeOpenProbability();
        float rightEyeOpenProbability2 = firebaseVisionFace.getRightEyeOpenProbability();
        double d = (double) leftEyeOpenProbability2;
        boolean z = false;
        if (d != -1.0d) {
            double d2 = (double) rightEyeOpenProbability2;
            if (d2 != -1.0d) {
                if (leftEyeOpenProbability > 0.9d || rightEyeOpenProbability > 0.9d) {
                    if (d < 0.6d || rightEyeOpenProbability < 0.6d) {
                        z = true;
                    }
                    leftEyeOpenProbability = d;
                    rightEyeOpenProbability = d2;
                } else {
                    leftEyeOpenProbability = d;
                    rightEyeOpenProbability = d2;
                    return false;
                }
            }
        }
        return z;
    }

    public static void saveInExternalStorageData(byte[] bArr, String str) {
        String file = Environment.getExternalStorageDirectory().toString();
        showLogs("SDCard", file);
        File file2 = new File(file + "/T5Live");
        if (!file2.exists()) {
            file2.mkdirs();
        }
        File file3 = new File(file2, str);
        if (file3.exists()) {
            file3.delete();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file3);
            fileOutputStream.write(bArr);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isResource(Context context, int i) {
        if (context == null) {
            return false;
        }
        try {
            return context.getResources().getResourceName(i) != null;
        } catch (Resources.NotFoundException unused) {
            return false;
        }
    }

    public static int resizeBasedOnEyeDistance(Context context, byte[] bArr) {
        float f = 0.0f;
        try {
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            SparseArray<Face> detect = new FaceDetector.Builder(context).setTrackingEnabled(false).setLandmarkType(1).build().detect(new Frame.Builder().setImageData(ByteBuffer.wrap(bArr), 320, 480, 0).build());
            if (detect.size() == 0) {
                return 0;
            }
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < detect.size(); i3++) {
                for (Landmark landmark : detect.valueAt(i3).getLandmarks()) {
                    int type = landmark.getType();
                    if (type == 4) {
                        i2 = (int) landmark.getPosition().x;
                        float f2 = landmark.getPosition().y;
                    } else if (type == 10) {
                        i = (int) landmark.getPosition().x;
                        float f3 = landmark.getPosition().y;
                    }
                }
            }
            Log.v("eyes distance", "== " + i2 + "," + i);
            f = ((float) (i - i2)) / 15.0f;
            StringBuilder sb = new StringBuilder();
            sb.append("==  requiredPercent");
            sb.append(f);
            Log.i("eyes distance", sb.toString());
            return (int) f;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("eyes distance", "exception " + e + " = distanceBtwEyesX " + 0);
        }
    }

    public static byte[] bitmapToByetArray(Bitmap bitmap) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int[] getOptimalDimensions(float f, float f2, int i, int i2) {
        float f3 = (float) i;
        float f4 = (float) i2;
        float f5 = f / f2;
        if (f3 / f > f4 / f2) {
            i = (int) (f4 * f5);
        } else {
            i2 = (int) (f3 / f5);
        }
        String str = TAG;
        Log.i(str, "layoutWidth: " + i);
        String str2 = TAG;
        Log.i(str2, "layoutHeight: " + i2);
        String str3 = TAG;
        Log.i(str3, "aspectRatio: " + f5);
        return new int[]{i, i2};
    }

    public static byte[] rotateImageData(Activity activity, byte[] bArr, int i) throws Exception {
        Bitmap bitmap;
        if (bArr != null) {
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr != null ? bArr.length : 0);
            if (activity.getResources().getConfiguration().orientation == 1) {
                Matrix matrix = new Matrix();
                int photoOrientation = setPhotoOrientation(activity, i);
                if (i != 1) {
                    matrix.postRotate((float) photoOrientation);
                } else if (photoOrientation == 270) {
                    matrix.postRotate(90.0f);
                } else if (photoOrientation == 90) {
                    matrix.postRotate(270.0f);
                }
                bitmap = Bitmap.createBitmap(decodeByteArray, 0, 0, decodeByteArray.getWidth(), decodeByteArray.getHeight(), matrix, true);
            } else {
                bitmap = Bitmap.createScaledBitmap(decodeByteArray, decodeByteArray.getWidth(), decodeByteArray.getHeight(), true);
            }
        } else {
            bitmap = null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private static int setPhotoOrientation(Activity activity, int i) {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(i, cameraInfo);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int i2 = 0;
        if (rotation != 0) {
            if (rotation == 1) {
                i2 = 90;
            } else if (rotation == 2) {
                i2 = RotationOptions.ROTATE_180;
            } else if (rotation == 3) {
                i2 = RotationOptions.ROTATE_270;
            }
        }
        if (cameraInfo.facing == 1) {
            return (360 - ((cameraInfo.orientation + i2) % 360)) % 360;
        }
        return ((cameraInfo.orientation - i2) + 360) % 360;
    }

    public static String getDateTime() {
        return new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
    }

    public static File createExternalDirectory(String str) {
        File file = new File(getAppDirectory() + "/" + str);
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    public static String getAppDirectory() {
        File file = new File(Environment.getExternalStorageDirectory() + "/T5AL");
        if (!file.exists()) {
            file.mkdir();
        }
        return file.toString();
    }

    public static int incrementUserEnrollCount(Context context) {
        int userEnrollCount = getUserEnrollCount(context) + 1;
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("enroll_c", userEnrollCount).apply();
        return userEnrollCount;
    }

    public static int getUserEnrollCount(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("enroll_c", -1);
    }

    public static byte[] compressFaceImage(byte[] bArr, int i, int i2) {
        Bitmap resizeBitmap = resizeBitmap(bArr, i);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        resizeBitmap.compress(Bitmap.CompressFormat.JPEG, i2, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private static Bitmap resizeBitmap(byte[] bArr, int i) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        int i2 = options.outWidth * i;
        return Bitmap.createScaledBitmap(convertByteArrayToBitmap(bArr), i2 / options.outHeight, i, true);
    }

    public static Bitmap convertByteArrayToBitmap(byte[] bArr) {
        return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
    }
}
