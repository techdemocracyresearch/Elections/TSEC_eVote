package com.face.liveness.utils;

import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.util.logging.FileHandler;

public class Logger {
    public static final String LOG_FILE_NAME = "T5LivenessLog.txt";
    private static final String TAG = "Logger";
    static boolean isExternalStorageAvailable = false;
    static boolean isExternalStorageWriteable = false;
    public static FileHandler logger;
    static String state = Environment.getExternalStorageState();

    public static void logException(String str, Exception exc) {
        exc.printStackTrace();
        addToLog(str, Log.getStackTraceString(exc));
    }

    public static void addToLog(String str, String str2) {
        Log.i("Record" + str, str2);
    }

    public static File createExternalDirectory(String str) {
        File file = new File(getAppDirectory() + "/" + str);
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    public static String getAppDirectory() {
        File file = new File(Environment.getExternalStorageDirectory() + "");
        if (!file.exists()) {
            file.mkdir();
        }
        return file.toString();
    }
}
