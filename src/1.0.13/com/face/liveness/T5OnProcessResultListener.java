package com.face.liveness;

import android.graphics.Bitmap;
import com.face.liveness.utils.T5FaceAngle;

public interface T5OnProcessResultListener {
    void onProcessError(int i, String str);

    void onProcessResult(Bitmap bitmap, T5FaceAngle t5FaceAngle);
}
