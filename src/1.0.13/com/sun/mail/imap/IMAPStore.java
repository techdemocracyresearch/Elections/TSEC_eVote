package com.sun.mail.imap;

import com.sun.mail.iap.BadCommandException;
import com.sun.mail.iap.CommandFailedException;
import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.iap.ResponseHandler;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.Namespaces;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.mail.AuthenticationFailedException;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Quota;
import javax.mail.QuotaAwareStore;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.StoreClosedException;
import javax.mail.URLName;
import kotlin.jvm.internal.CharCompanionObject;

public class IMAPStore extends Store implements QuotaAwareStore, ResponseHandler {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    public static final int RESPONSE = 1000;
    private int appendBufferSize;
    private String authorizationID;
    private int blksize;
    private volatile boolean connected;
    private int defaultPort;
    private boolean disableAuthLogin;
    private boolean disableAuthPlain;
    private boolean enableImapEvents;
    private boolean enableSASL;
    private boolean enableStartTLS;
    private boolean forcePasswordRefresh;
    private String host;
    private boolean isSSL;
    private int minIdleTime;
    private String name;
    private Namespaces namespaces;
    private PrintStream out;
    private String password;
    private ConnectionPool pool;
    private int port;
    private String proxyAuthUser;
    private String[] saslMechanisms;
    private String saslRealm;
    private int statusCacheTimeout;
    private String user;

    /* access modifiers changed from: package-private */
    public static class ConnectionPool {
        private static final int ABORTING = 2;
        private static final int IDLE = 1;
        private static final int RUNNING = 0;
        private Vector authenticatedConnections = new Vector();
        private long clientTimeoutInterval = 45000;
        private boolean debug = false;
        private Vector folders;
        private IMAPProtocol idleProtocol;
        private int idleState = 0;
        private long lastTimePruned;
        private int poolSize = 1;
        private long pruningInterval = 60000;
        private boolean separateStoreConnection = false;
        private long serverTimeoutInterval = 1800000;
        private boolean storeConnectionInUse = false;

        ConnectionPool() {
        }
    }

    public IMAPStore(Session session, URLName uRLName) {
        this(session, uRLName, "imap", 143, false);
    }

    protected IMAPStore(Session session, URLName uRLName, String str, int i, boolean z) {
        super(session, uRLName);
        this.name = "imap";
        this.defaultPort = 143;
        this.isSSL = false;
        this.port = -1;
        this.blksize = 16384;
        this.statusCacheTimeout = 1000;
        this.appendBufferSize = -1;
        this.minIdleTime = 10;
        this.disableAuthLogin = false;
        this.disableAuthPlain = false;
        this.enableStartTLS = false;
        this.enableSASL = false;
        this.forcePasswordRefresh = false;
        this.enableImapEvents = false;
        this.connected = false;
        this.pool = new ConnectionPool();
        str = uRLName != null ? uRLName.getProtocol() : str;
        this.name = str;
        this.defaultPort = i;
        this.isSSL = z;
        this.pool.lastTimePruned = System.currentTimeMillis();
        this.debug = session.getDebug();
        PrintStream debugOut = session.getDebugOut();
        this.out = debugOut;
        if (debugOut == null) {
            this.out = System.out;
        }
        String property = session.getProperty("mail." + str + ".connectionpool.debug");
        if (property != null && property.equalsIgnoreCase("true")) {
            this.pool.debug = true;
        }
        String property2 = session.getProperty("mail." + str + ".partialfetch");
        if (property2 == null || !property2.equalsIgnoreCase("false")) {
            String property3 = session.getProperty("mail." + str + ".fetchsize");
            if (property3 != null) {
                this.blksize = Integer.parseInt(property3);
            }
            if (this.debug) {
                PrintStream printStream = this.out;
                printStream.println("DEBUG: mail.imap.fetchsize: " + this.blksize);
            }
        } else {
            this.blksize = -1;
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.partialfetch: false");
            }
        }
        String property4 = session.getProperty("mail." + str + ".statuscachetimeout");
        if (property4 != null) {
            this.statusCacheTimeout = Integer.parseInt(property4);
            if (this.debug) {
                PrintStream printStream2 = this.out;
                printStream2.println("DEBUG: mail.imap.statuscachetimeout: " + this.statusCacheTimeout);
            }
        }
        String property5 = session.getProperty("mail." + str + ".appendbuffersize");
        if (property5 != null) {
            this.appendBufferSize = Integer.parseInt(property5);
            if (this.debug) {
                PrintStream printStream3 = this.out;
                printStream3.println("DEBUG: mail.imap.appendbuffersize: " + this.appendBufferSize);
            }
        }
        String property6 = session.getProperty("mail." + str + ".minidletime");
        if (property6 != null) {
            this.minIdleTime = Integer.parseInt(property6);
            if (this.debug) {
                PrintStream printStream4 = this.out;
                printStream4.println("DEBUG: mail.imap.minidletime: " + this.minIdleTime);
            }
        }
        String property7 = session.getProperty("mail." + str + ".connectionpoolsize");
        if (property7 != null) {
            try {
                int parseInt = Integer.parseInt(property7);
                if (parseInt > 0) {
                    this.pool.poolSize = parseInt;
                }
            } catch (NumberFormatException unused) {
            }
            if (this.pool.debug) {
                PrintStream printStream5 = this.out;
                printStream5.println("DEBUG: mail.imap.connectionpoolsize: " + this.pool.poolSize);
            }
        }
        String property8 = session.getProperty("mail." + str + ".connectionpooltimeout");
        if (property8 != null) {
            try {
                int parseInt2 = Integer.parseInt(property8);
                if (parseInt2 > 0) {
                    this.pool.clientTimeoutInterval = (long) parseInt2;
                }
            } catch (NumberFormatException unused2) {
            }
            if (this.pool.debug) {
                PrintStream printStream6 = this.out;
                printStream6.println("DEBUG: mail.imap.connectionpooltimeout: " + this.pool.clientTimeoutInterval);
            }
        }
        String property9 = session.getProperty("mail." + str + ".servertimeout");
        if (property9 != null) {
            try {
                int parseInt3 = Integer.parseInt(property9);
                if (parseInt3 > 0) {
                    this.pool.serverTimeoutInterval = (long) parseInt3;
                }
            } catch (NumberFormatException unused3) {
            }
            if (this.pool.debug) {
                PrintStream printStream7 = this.out;
                printStream7.println("DEBUG: mail.imap.servertimeout: " + this.pool.serverTimeoutInterval);
            }
        }
        String property10 = session.getProperty("mail." + str + ".separatestoreconnection");
        if (property10 != null && property10.equalsIgnoreCase("true")) {
            if (this.pool.debug) {
                this.out.println("DEBUG: dedicate a store connection");
            }
            this.pool.separateStoreConnection = true;
        }
        String property11 = session.getProperty("mail." + str + ".proxyauth.user");
        if (property11 != null) {
            this.proxyAuthUser = property11;
            if (this.debug) {
                PrintStream printStream8 = this.out;
                printStream8.println("DEBUG: mail.imap.proxyauth.user: " + this.proxyAuthUser);
            }
        }
        String property12 = session.getProperty("mail." + str + ".auth.login.disable");
        if (property12 != null && property12.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: disable AUTH=LOGIN");
            }
            this.disableAuthLogin = true;
        }
        String property13 = session.getProperty("mail." + str + ".auth.plain.disable");
        if (property13 != null && property13.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: disable AUTH=PLAIN");
            }
            this.disableAuthPlain = true;
        }
        String property14 = session.getProperty("mail." + str + ".starttls.enable");
        if (property14 != null && property14.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: enable STARTTLS");
            }
            this.enableStartTLS = true;
        }
        String property15 = session.getProperty("mail." + str + ".sasl.enable");
        if (property15 != null && property15.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: enable SASL");
            }
            this.enableSASL = true;
        }
        if (this.enableSASL) {
            String property16 = session.getProperty("mail." + str + ".sasl.mechanisms");
            if (property16 != null && property16.length() > 0) {
                if (this.debug) {
                    PrintStream printStream9 = this.out;
                    printStream9.println("DEBUG: SASL mechanisms allowed: " + property16);
                }
                Vector vector = new Vector(5);
                StringTokenizer stringTokenizer = new StringTokenizer(property16, " ,");
                while (stringTokenizer.hasMoreTokens()) {
                    String nextToken = stringTokenizer.nextToken();
                    if (nextToken.length() > 0) {
                        vector.addElement(nextToken);
                    }
                }
                String[] strArr = new String[vector.size()];
                this.saslMechanisms = strArr;
                vector.copyInto(strArr);
            }
        }
        String property17 = session.getProperty("mail." + str + ".sasl.authorizationid");
        if (property17 != null) {
            this.authorizationID = property17;
            if (this.debug) {
                PrintStream printStream10 = this.out;
                printStream10.println("DEBUG: mail.imap.sasl.authorizationid: " + this.authorizationID);
            }
        }
        String property18 = session.getProperty("mail." + str + ".sasl.realm");
        if (property18 != null) {
            this.saslRealm = property18;
            if (this.debug) {
                PrintStream printStream11 = this.out;
                printStream11.println("DEBUG: mail.imap.sasl.realm: " + this.saslRealm);
            }
        }
        String property19 = session.getProperty("mail." + str + ".forcepasswordrefresh");
        if (property19 != null && property19.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: enable forcePasswordRefresh");
            }
            this.forcePasswordRefresh = true;
        }
        String property20 = session.getProperty("mail." + str + ".enableimapevents");
        if (property20 != null && property20.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: enable IMAP events");
            }
            this.enableImapEvents = true;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ab, code lost:
        r11 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ac, code lost:
        r0 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b6, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c0, code lost:
        throw new javax.mail.MessagingException(r11.getMessage(), r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c1, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00cb, code lost:
        throw new javax.mail.MessagingException(r11.getMessage(), r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00cf, code lost:
        r0.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b6 A[ExcHandler: IOException (r11v13 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:15:0x003b] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00c1 A[ExcHandler: ProtocolException (r11v12 'e' com.sun.mail.iap.ProtocolException A[CUSTOM_DECLARE]), Splitter:B:15:0x003b] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00cf  */
    @Override // javax.mail.Service
    public synchronized boolean protocolConnect(String str, int i, String str2, String str3) throws MessagingException {
        boolean isEmpty;
        IMAPProtocol iMAPProtocol = null;
        if (str == null || str3 == null || str2 == null) {
            if (this.debug) {
                PrintStream printStream = this.out;
                StringBuilder sb = new StringBuilder("DEBUG: protocolConnect returning false, host=");
                sb.append(str);
                sb.append(", user=");
                sb.append(str2);
                sb.append(", password=");
                sb.append(str3 != null ? "<non-null>" : "<null>");
                printStream.println(sb.toString());
            }
            return false;
        }
        if (i != -1) {
            this.port = i;
        } else {
            Session session = this.session;
            String property = session.getProperty("mail." + this.name + ".port");
            if (property != null) {
                this.port = Integer.parseInt(property);
            }
        }
        if (this.port == -1) {
            this.port = this.defaultPort;
        }
        try {
            synchronized (this.pool) {
                isEmpty = this.pool.authenticatedConnections.isEmpty();
            }
            if (isEmpty) {
                IMAPProtocol iMAPProtocol2 = new IMAPProtocol(this.name, str, this.port, this.session.getDebug(), this.session.getDebugOut(), this.session.getProperties(), this.isSSL);
                if (this.debug) {
                    PrintStream printStream2 = this.out;
                    printStream2.println("DEBUG: protocolConnect login, host=" + str + ", user=" + str2 + ", password=<non-null>");
                }
                login(iMAPProtocol2, str2, str3);
                iMAPProtocol2.addResponseHandler(this);
                this.host = str;
                this.user = str2;
                this.password = str3;
                synchronized (this.pool) {
                    this.pool.authenticatedConnections.addElement(iMAPProtocol2);
                }
            }
            this.connected = true;
            return true;
        } catch (CommandFailedException e) {
            CommandFailedException e2 = e;
            if (iMAPProtocol != null) {
            }
            throw new AuthenticationFailedException(e2.getResponse().getRest());
        } catch (ProtocolException e3) {
        } catch (IOException e4) {
        }
    }

    private void login(IMAPProtocol iMAPProtocol, String str, String str2) throws ProtocolException {
        if (this.enableStartTLS && iMAPProtocol.hasCapability("STARTTLS")) {
            iMAPProtocol.startTLS();
            iMAPProtocol.capability();
        }
        if (!iMAPProtocol.isAuthenticated()) {
            iMAPProtocol.getCapabilities().put("__PRELOGIN__", "");
            String str3 = this.authorizationID;
            if (str3 == null && (str3 = this.proxyAuthUser) == null) {
                str3 = str;
            }
            if (this.enableSASL) {
                iMAPProtocol.sasllogin(this.saslMechanisms, this.saslRealm, str3, str, str2);
            }
            if (!iMAPProtocol.isAuthenticated()) {
                if (iMAPProtocol.hasCapability("AUTH=PLAIN") && !this.disableAuthPlain) {
                    iMAPProtocol.authplain(str3, str, str2);
                } else if ((iMAPProtocol.hasCapability("AUTH-LOGIN") || iMAPProtocol.hasCapability("AUTH=LOGIN")) && !this.disableAuthLogin) {
                    iMAPProtocol.authlogin(str, str2);
                } else if (!iMAPProtocol.hasCapability("LOGINDISABLED")) {
                    iMAPProtocol.login(str, str2);
                } else {
                    throw new ProtocolException("No login methods supported!");
                }
            }
            String str4 = this.proxyAuthUser;
            if (str4 != null) {
                iMAPProtocol.proxyauth(str4);
            }
            if (iMAPProtocol.hasCapability("__PRELOGIN__")) {
                try {
                    iMAPProtocol.capability();
                } catch (ConnectionException e) {
                    throw e;
                } catch (ProtocolException unused) {
                }
            }
        }
    }

    public synchronized void setUsername(String str) {
        this.user = str;
    }

    public synchronized void setPassword(String str) {
        this.password = str;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:22|23|24|25|70) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x007f */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0117 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0114 A[SYNTHETIC] */
    public IMAPProtocol getProtocol(IMAPFolder iMAPFolder) throws MessagingException {
        IMAPProtocol iMAPProtocol;
        InetAddress inetAddress;
        while (true) {
            IMAPProtocol iMAPProtocol2 = null;
            while (iMAPProtocol2 == null) {
                synchronized (this.pool) {
                    if (!this.pool.authenticatedConnections.isEmpty()) {
                        if (this.pool.authenticatedConnections.size() == 1) {
                            if (!this.pool.separateStoreConnection) {
                                if (this.pool.storeConnectionInUse) {
                                }
                            }
                        }
                        if (this.debug) {
                            this.out.println("DEBUG: connection available -- size: " + this.pool.authenticatedConnections.size());
                        }
                        iMAPProtocol2 = (IMAPProtocol) this.pool.authenticatedConnections.lastElement();
                        this.pool.authenticatedConnections.removeElement(iMAPProtocol2);
                        if (System.currentTimeMillis() - iMAPProtocol2.getTimestamp() > this.pool.serverTimeoutInterval) {
                            iMAPProtocol2.noop();
                            iMAPProtocol2.removeResponseHandler(this);
                            iMAPProtocol2.disconnect();
                        }
                        iMAPProtocol2.removeResponseHandler(this);
                        timeoutConnections();
                        if (iMAPFolder != null) {
                            if (this.pool.folders == null) {
                                this.pool.folders = new Vector();
                            }
                            this.pool.folders.addElement(iMAPFolder);
                        }
                    }
                    if (this.debug) {
                        this.out.println("DEBUG: no connections in the pool, creating a new one");
                    }
                    try {
                        if (this.forcePasswordRefresh) {
                            try {
                                inetAddress = InetAddress.getByName(this.host);
                            } catch (UnknownHostException unused) {
                                inetAddress = null;
                            }
                            PasswordAuthentication requestPasswordAuthentication = this.session.requestPasswordAuthentication(inetAddress, this.port, this.name, null, this.user);
                            if (requestPasswordAuthentication != null) {
                                this.user = requestPasswordAuthentication.getUserName();
                                this.password = requestPasswordAuthentication.getPassword();
                            }
                        }
                        iMAPProtocol = new IMAPProtocol(this.name, this.host, this.port, this.session.getDebug(), this.session.getDebugOut(), this.session.getProperties(), this.isSSL);
                        try {
                            login(iMAPProtocol, this.user, this.password);
                        } catch (Exception unused2) {
                            iMAPProtocol2 = iMAPProtocol;
                        }
                    } catch (Exception unused3) {
                        if (iMAPProtocol2 != null) {
                            try {
                                iMAPProtocol2.disconnect();
                            } catch (Exception unused4) {
                            }
                        }
                        iMAPProtocol = null;
                        if (iMAPProtocol == null) {
                        }
                    }
                    if (iMAPProtocol == null) {
                        iMAPProtocol2 = iMAPProtocol;
                        timeoutConnections();
                        if (iMAPFolder != null) {
                        }
                    } else {
                        throw new MessagingException("connection failure");
                    }
                }
            }
            return iMAPProtocol2;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0066 A[SYNTHETIC] */
    public IMAPProtocol getStoreProtocol() throws ProtocolException {
        IMAPProtocol iMAPProtocol;
        IMAPProtocol iMAPProtocol2 = null;
        while (iMAPProtocol2 == null) {
            synchronized (this.pool) {
                waitIfIdle();
                if (this.pool.authenticatedConnections.isEmpty()) {
                    if (this.pool.debug) {
                        this.out.println("DEBUG: getStoreProtocol() - no connections in the pool, creating a new one");
                    }
                    try {
                        iMAPProtocol = new IMAPProtocol(this.name, this.host, this.port, this.session.getDebug(), this.session.getDebugOut(), this.session.getProperties(), this.isSSL);
                        try {
                            login(iMAPProtocol, this.user, this.password);
                        } catch (Exception unused) {
                            iMAPProtocol2 = iMAPProtocol;
                        }
                    } catch (Exception unused2) {
                        if (iMAPProtocol2 != null) {
                            try {
                                iMAPProtocol2.logout();
                            } catch (Exception unused3) {
                            }
                        }
                        iMAPProtocol = null;
                        if (iMAPProtocol == null) {
                        }
                    }
                    if (iMAPProtocol == null) {
                        iMAPProtocol.addResponseHandler(this);
                        this.pool.authenticatedConnections.addElement(iMAPProtocol);
                    } else {
                        throw new ConnectionException("failed to create new store connection");
                    }
                } else {
                    if (this.pool.debug) {
                        this.out.println("DEBUG: getStoreProtocol() - connection available -- size: " + this.pool.authenticatedConnections.size());
                    }
                    iMAPProtocol = (IMAPProtocol) this.pool.authenticatedConnections.firstElement();
                }
                if (this.pool.storeConnectionInUse) {
                    try {
                        this.pool.wait();
                    } catch (InterruptedException unused4) {
                    }
                    iMAPProtocol2 = null;
                } else {
                    this.pool.storeConnectionInUse = true;
                    if (this.pool.debug) {
                        this.out.println("DEBUG: getStoreProtocol() -- storeConnectionInUse");
                    }
                    iMAPProtocol2 = iMAPProtocol;
                }
                timeoutConnections();
            }
        }
        return iMAPProtocol2;
    }

    /* access modifiers changed from: package-private */
    public boolean allowReadOnlySelect() {
        Session session = this.session;
        String property = session.getProperty("mail." + this.name + ".allowreadonlyselect");
        return property != null && property.equalsIgnoreCase("true");
    }

    /* access modifiers changed from: package-private */
    public boolean hasSeparateStoreConnection() {
        return this.pool.separateStoreConnection;
    }

    /* access modifiers changed from: package-private */
    public boolean getConnectionPoolDebug() {
        return this.pool.debug;
    }

    /* access modifiers changed from: package-private */
    public boolean isConnectionPoolFull() {
        boolean z;
        synchronized (this.pool) {
            if (this.pool.debug) {
                PrintStream printStream = this.out;
                printStream.println("DEBUG: current size: " + this.pool.authenticatedConnections.size() + "   pool size: " + this.pool.poolSize);
            }
            z = this.pool.authenticatedConnections.size() >= this.pool.poolSize;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void releaseProtocol(IMAPFolder iMAPFolder, IMAPProtocol iMAPProtocol) {
        synchronized (this.pool) {
            if (iMAPProtocol != null) {
                if (!isConnectionPoolFull()) {
                    iMAPProtocol.addResponseHandler(this);
                    this.pool.authenticatedConnections.addElement(iMAPProtocol);
                    if (this.debug) {
                        PrintStream printStream = this.out;
                        printStream.println("DEBUG: added an Authenticated connection -- size: " + this.pool.authenticatedConnections.size());
                    }
                } else {
                    if (this.debug) {
                        this.out.println("DEBUG: pool is full, not adding an Authenticated connection");
                    }
                    try {
                        iMAPProtocol.logout();
                    } catch (ProtocolException unused) {
                    }
                }
            }
            if (this.pool.folders != null) {
                this.pool.folders.removeElement(iMAPFolder);
            }
            timeoutConnections();
        }
    }

    /* access modifiers changed from: package-private */
    public void releaseStoreProtocol(IMAPProtocol iMAPProtocol) {
        if (iMAPProtocol != null) {
            synchronized (this.pool) {
                this.pool.storeConnectionInUse = false;
                this.pool.notifyAll();
                if (this.pool.debug) {
                    this.out.println("DEBUG: releaseStoreProtocol()");
                }
                timeoutConnections();
            }
        }
    }

    private void emptyConnectionPool(boolean z) {
        synchronized (this.pool) {
            for (int size = this.pool.authenticatedConnections.size() - 1; size >= 0; size--) {
                try {
                    IMAPProtocol iMAPProtocol = (IMAPProtocol) this.pool.authenticatedConnections.elementAt(size);
                    iMAPProtocol.removeResponseHandler(this);
                    if (z) {
                        iMAPProtocol.disconnect();
                    } else {
                        iMAPProtocol.logout();
                    }
                } catch (ProtocolException unused) {
                }
            }
            this.pool.authenticatedConnections.removeAllElements();
        }
        if (this.pool.debug) {
            this.out.println("DEBUG: removed all authenticated connections");
        }
    }

    private void timeoutConnections() {
        synchronized (this.pool) {
            if (System.currentTimeMillis() - this.pool.lastTimePruned > this.pool.pruningInterval && this.pool.authenticatedConnections.size() > 1) {
                if (this.pool.debug) {
                    this.out.println("DEBUG: checking for connections to prune: " + (System.currentTimeMillis() - this.pool.lastTimePruned));
                    this.out.println("DEBUG: clientTimeoutInterval: " + this.pool.clientTimeoutInterval);
                }
                for (int size = this.pool.authenticatedConnections.size() - 1; size > 0; size--) {
                    IMAPProtocol iMAPProtocol = (IMAPProtocol) this.pool.authenticatedConnections.elementAt(size);
                    if (this.pool.debug) {
                        this.out.println("DEBUG: protocol last used: " + (System.currentTimeMillis() - iMAPProtocol.getTimestamp()));
                    }
                    if (System.currentTimeMillis() - iMAPProtocol.getTimestamp() > this.pool.clientTimeoutInterval) {
                        if (this.pool.debug) {
                            this.out.println("DEBUG: authenticated connection timed out");
                            this.out.println("DEBUG: logging out the connection");
                        }
                        iMAPProtocol.removeResponseHandler(this);
                        this.pool.authenticatedConnections.removeElementAt(size);
                        try {
                            iMAPProtocol.logout();
                        } catch (ProtocolException unused) {
                        }
                    }
                }
                this.pool.lastTimePruned = System.currentTimeMillis();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int getFetchBlockSize() {
        return this.blksize;
    }

    /* access modifiers changed from: package-private */
    public Session getSession() {
        return this.session;
    }

    /* access modifiers changed from: package-private */
    public int getStatusCacheTimeout() {
        return this.statusCacheTimeout;
    }

    /* access modifiers changed from: package-private */
    public int getAppendBufferSize() {
        return this.appendBufferSize;
    }

    /* access modifiers changed from: package-private */
    public int getMinIdleTime() {
        return this.minIdleTime;
    }

    public synchronized boolean hasCapability(String str) throws MessagingException {
        boolean hasCapability;
        IMAPProtocol iMAPProtocol = null;
        try {
            iMAPProtocol = getStoreProtocol();
            hasCapability = iMAPProtocol.hasCapability(str);
            releaseStoreProtocol(iMAPProtocol);
        } catch (ProtocolException e) {
            if (iMAPProtocol == null) {
                cleanup();
            }
            throw new MessagingException(e.getMessage(), e);
        } catch (Throwable th) {
            releaseStoreProtocol(iMAPProtocol);
            throw th;
        }
        return hasCapability;
    }

    @Override // javax.mail.Service
    public synchronized boolean isConnected() {
        if (!this.connected) {
            super.setConnected(false);
            return false;
        }
        IMAPProtocol iMAPProtocol = null;
        try {
            iMAPProtocol = getStoreProtocol();
            iMAPProtocol.noop();
        } catch (ProtocolException unused) {
            if (iMAPProtocol == null) {
                cleanup();
            }
        } catch (Throwable th) {
            releaseStoreProtocol(iMAPProtocol);
            throw th;
        }
        releaseStoreProtocol(iMAPProtocol);
        return super.isConnected();
    }

    @Override // javax.mail.Service
    public synchronized void close() throws MessagingException {
        boolean isEmpty;
        if (super.isConnected()) {
            try {
                synchronized (this.pool) {
                    isEmpty = this.pool.authenticatedConnections.isEmpty();
                }
                if (isEmpty) {
                    if (this.pool.debug) {
                        this.out.println("DEBUG: close() - no connections ");
                    }
                    cleanup();
                    releaseStoreProtocol(null);
                    return;
                }
                IMAPProtocol storeProtocol = getStoreProtocol();
                synchronized (this.pool) {
                    this.pool.authenticatedConnections.removeElement(storeProtocol);
                }
                storeProtocol.logout();
                releaseStoreProtocol(storeProtocol);
            } catch (ProtocolException e) {
                cleanup();
                throw new MessagingException(e.getMessage(), e);
            } catch (Throwable th) {
                releaseStoreProtocol(null);
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // javax.mail.Service
    public void finalize() throws Throwable {
        super.finalize();
        close();
    }

    private void cleanup() {
        cleanup(false);
    }

    private void cleanup(boolean z) {
        boolean z2;
        if (this.debug) {
            this.out.println("DEBUG: IMAPStore cleanup, force " + z);
        }
        Vector vector = null;
        while (true) {
            synchronized (this.pool) {
                if (this.pool.folders != null) {
                    vector = this.pool.folders;
                    this.pool.folders = null;
                    z2 = false;
                } else {
                    z2 = true;
                }
            }
            if (z2) {
                synchronized (this.pool) {
                    emptyConnectionPool(z);
                }
                this.connected = false;
                notifyConnectionListeners(3);
                if (this.debug) {
                    this.out.println("DEBUG: IMAPStore cleanup done");
                    return;
                }
                return;
            }
            int size = vector.size();
            for (int i = 0; i < size; i++) {
                IMAPFolder iMAPFolder = (IMAPFolder) vector.elementAt(i);
                if (z) {
                    try {
                        if (this.debug) {
                            this.out.println("DEBUG: force folder to close");
                        }
                        iMAPFolder.forceClose();
                    } catch (IllegalStateException | MessagingException unused) {
                    }
                } else {
                    if (this.debug) {
                        this.out.println("DEBUG: close folder");
                    }
                    iMAPFolder.close(false);
                }
            }
        }
    }

    @Override // javax.mail.Store
    public synchronized Folder getDefaultFolder() throws MessagingException {
        checkConnected();
        return new DefaultFolder(this);
    }

    @Override // javax.mail.Store
    public synchronized Folder getFolder(String str) throws MessagingException {
        checkConnected();
        return new IMAPFolder(str, CharCompanionObject.MAX_VALUE, this);
    }

    @Override // javax.mail.Store
    public synchronized Folder getFolder(URLName uRLName) throws MessagingException {
        checkConnected();
        return new IMAPFolder(uRLName.getFile(), CharCompanionObject.MAX_VALUE, this);
    }

    @Override // javax.mail.Store
    public Folder[] getPersonalNamespaces() throws MessagingException {
        Namespaces namespaces2 = getNamespaces();
        if (namespaces2 == null || namespaces2.personal == null) {
            return super.getPersonalNamespaces();
        }
        return namespaceToFolders(namespaces2.personal, null);
    }

    @Override // javax.mail.Store
    public Folder[] getUserNamespaces(String str) throws MessagingException {
        Namespaces namespaces2 = getNamespaces();
        if (namespaces2 == null || namespaces2.otherUsers == null) {
            return super.getUserNamespaces(str);
        }
        return namespaceToFolders(namespaces2.otherUsers, str);
    }

    @Override // javax.mail.Store
    public Folder[] getSharedNamespaces() throws MessagingException {
        Namespaces namespaces2 = getNamespaces();
        if (namespaces2 == null || namespaces2.shared == null) {
            return super.getSharedNamespaces();
        }
        return namespaceToFolders(namespaces2.shared, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0040, code lost:
        if (r0 != null) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0016, code lost:
        if (r0 == null) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        cleanup();
     */
    private synchronized Namespaces getNamespaces() throws MessagingException {
        checkConnected();
        IMAPProtocol iMAPProtocol = null;
        if (this.namespaces == null) {
            try {
                iMAPProtocol = getStoreProtocol();
                this.namespaces = iMAPProtocol.namespace();
                releaseStoreProtocol(iMAPProtocol);
            } catch (BadCommandException unused) {
                releaseStoreProtocol(iMAPProtocol);
            } catch (ConnectionException e) {
                throw new StoreClosedException(this, e.getMessage());
            } catch (ProtocolException e2) {
                throw new MessagingException(e2.getMessage(), e2);
            } catch (Throwable th) {
                releaseStoreProtocol(iMAPProtocol);
                if (iMAPProtocol == null) {
                    cleanup();
                }
                throw th;
            }
        }
        return this.namespaces;
    }

    private Folder[] namespaceToFolders(Namespaces.Namespace[] namespaceArr, String str) {
        int length = namespaceArr.length;
        Folder[] folderArr = new Folder[length];
        for (int i = 0; i < length; i++) {
            String str2 = namespaceArr[i].prefix;
            if (str == null) {
                int length2 = str2.length();
                if (length2 > 0) {
                    int i2 = length2 - 1;
                    if (str2.charAt(i2) == namespaceArr[i].delimiter) {
                        str2 = str2.substring(0, i2);
                    }
                }
            } else {
                str2 = String.valueOf(str2) + str;
            }
            folderArr[i] = new IMAPFolder(str2, namespaceArr[i].delimiter, this, str == null);
        }
        return folderArr;
    }

    @Override // javax.mail.QuotaAwareStore
    public synchronized Quota[] getQuota(String str) throws MessagingException {
        Quota[] quotaRoot;
        checkConnected();
        IMAPProtocol iMAPProtocol = null;
        Quota[] quotaArr = null;
        try {
            iMAPProtocol = getStoreProtocol();
            quotaRoot = iMAPProtocol.getQuotaRoot(str);
            releaseStoreProtocol(iMAPProtocol);
            if (iMAPProtocol == null) {
                cleanup();
            }
        } catch (BadCommandException e) {
            throw new MessagingException("QUOTA not supported", e);
        } catch (ConnectionException e2) {
            throw new StoreClosedException(this, e2.getMessage());
        } catch (ProtocolException e3) {
            throw new MessagingException(e3.getMessage(), e3);
        } catch (Throwable th) {
            releaseStoreProtocol(iMAPProtocol);
            if (iMAPProtocol == null) {
                cleanup();
            }
            throw th;
        }
        return quotaRoot;
    }

    @Override // javax.mail.QuotaAwareStore
    public synchronized void setQuota(Quota quota) throws MessagingException {
        checkConnected();
        IMAPProtocol iMAPProtocol = null;
        try {
            iMAPProtocol = getStoreProtocol();
            iMAPProtocol.setQuota(quota);
            releaseStoreProtocol(iMAPProtocol);
            if (iMAPProtocol == null) {
                cleanup();
            }
        } catch (BadCommandException e) {
            throw new MessagingException("QUOTA not supported", e);
        } catch (ConnectionException e2) {
            throw new StoreClosedException(this, e2.getMessage());
        } catch (ProtocolException e3) {
            throw new MessagingException(e3.getMessage(), e3);
        } catch (Throwable th) {
            releaseStoreProtocol(iMAPProtocol);
            if (iMAPProtocol == null) {
                cleanup();
            }
            throw th;
        }
    }

    private void checkConnected() {
        if (!this.connected) {
            super.setConnected(false);
            throw new IllegalStateException("Not connected");
        }
    }

    @Override // com.sun.mail.iap.ResponseHandler
    public void handleResponse(Response response) {
        if (response.isOK() || response.isNO() || response.isBAD() || response.isBYE()) {
            handleResponseCode(response);
        }
        if (response.isBYE()) {
            if (this.debug) {
                this.out.println("DEBUG: IMAPStore connection dead");
            }
            if (this.connected) {
                cleanup(response.isSynthetic());
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r1 = r2.readIdleResponse();
        r3 = r5.pool;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002a, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002b, code lost:
        if (r1 == null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0031, code lost:
        if (r2.processIdleResponse(r1) != false) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0034, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0037, code lost:
        if (r5.enableImapEvents == false) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003d, code lost:
        if (r1.isUnTagged() == false) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003f, code lost:
        notifyStoreListeners(1000, r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0049, code lost:
        r5.pool.idleState = 0;
        r5.pool.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0054, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0055, code lost:
        r1 = getMinIdleTime();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0059, code lost:
        if (r1 <= 0) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        java.lang.Thread.sleep((long) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0098, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x009a, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x009c, code lost:
        r1 = e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x00c7 A[SYNTHETIC] */
    public void idle() throws MessagingException {
        IMAPProtocol iMAPProtocol;
        Throwable th;
        Throwable th2;
        synchronized (this) {
            checkConnected();
        }
        try {
            synchronized (this.pool) {
                try {
                    iMAPProtocol = getStoreProtocol();
                    try {
                        if (this.pool.idleState == 0) {
                            iMAPProtocol.idleStart();
                            this.pool.idleState = 1;
                            this.pool.idleProtocol = iMAPProtocol;
                        } else {
                            try {
                                this.pool.wait();
                            } catch (InterruptedException unused) {
                            }
                        }
                    } catch (Throwable th3) {
                        th2 = th3;
                        throw th2;
                    }
                } catch (Throwable th4) {
                    th2 = th4;
                    throw th2;
                }
            }
            synchronized (this.pool) {
                this.pool.idleProtocol = null;
            }
            releaseStoreProtocol(iMAPProtocol);
            if (iMAPProtocol == null) {
                cleanup();
                return;
            }
            return;
            synchronized (this.pool) {
                this.pool.idleProtocol = null;
            }
            releaseStoreProtocol(iMAPProtocol);
            if (iMAPProtocol == null) {
                cleanup();
            }
        } catch (BadCommandException e) {
            BadCommandException e2 = e;
            throw new MessagingException("IDLE not supported", e2);
        } catch (ConnectionException e3) {
            ConnectionException e4 = e3;
            throw new StoreClosedException(this, e4.getMessage());
        } catch (ProtocolException e5) {
            ProtocolException e6 = e5;
            iMAPProtocol = null;
            throw new MessagingException(e6.getMessage(), e6);
        } catch (Throwable th5) {
            th = th5;
            synchronized (this.pool) {
            }
        }
    }

    private void waitIfIdle() throws ProtocolException {
        while (this.pool.idleState != 0) {
            if (this.pool.idleState == 1) {
                this.pool.idleProtocol.idleAbort();
                this.pool.idleState = 2;
            }
            try {
                this.pool.wait();
            } catch (InterruptedException unused) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void handleResponseCode(Response response) {
        String rest = response.getRest();
        boolean z = false;
        if (rest.startsWith("[")) {
            int indexOf = rest.indexOf(93);
            if (indexOf > 0 && rest.substring(0, indexOf + 1).equalsIgnoreCase("[ALERT]")) {
                z = true;
            }
            rest = rest.substring(indexOf + 1).trim();
        }
        if (z) {
            notifyStoreListeners(1, rest);
        } else if (response.isUnTagged() && rest.length() > 0) {
            notifyStoreListeners(2, rest);
        }
    }
}
