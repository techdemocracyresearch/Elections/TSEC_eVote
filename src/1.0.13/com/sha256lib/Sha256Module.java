package com.sha256lib;

import com.bumptech.glide.load.Key;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Sha256Module extends ReactContextBaseJavaModule {
    private final ReactApplicationContext reactContext;

    @Override // com.facebook.react.bridge.NativeModule
    public String getName() {
        return "sha256Lib";
    }

    public Sha256Module(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
        this.reactContext = reactApplicationContext;
    }

    /* access modifiers changed from: package-private */
    public String buildHash(String str, String str2, Integer num) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest instance = MessageDigest.getInstance(str2);
        instance.update(str.getBytes(Key.STRING_CHARSET_NAME));
        byte[] digest = instance.digest();
        return String.format("%0" + num.toString() + "x", new BigInteger(1, digest));
    }

    @ReactMethod
    public void sha256(String str, Promise promise) {
        try {
            promise.resolve(buildHash(str, "SHA-256", 64));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            promise.reject("sha256", e.getMessage());
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            promise.reject("sha256", e2.getMessage());
        }
    }

    @ReactMethod
    public void sha1(String str, Promise promise) {
        try {
            promise.resolve(buildHash(str, "SHA-1", 40));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            promise.reject("sha1", e.getMessage());
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            promise.reject("sha1", e2.getMessage());
        }
    }
}
