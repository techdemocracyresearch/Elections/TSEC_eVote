package com.google.android.gms.internal.vision;

import java.util.Arrays;
import java.util.Map;
import kotlin.UByte;
import kotlin.UShort;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzdl<K, V> extends zzdg<K, V> {
    private static final zzdg<Object, Object> zzmd = new zzdl(null, new Object[0], 0);
    private final transient int size;
    private final transient Object[] zzmb;
    private final transient Object zzme;

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:55:0x00e7 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: short[] */
    /* JADX DEBUG: Multi-variable search result rejected for r7v5, resolved type: short */
    /* JADX DEBUG: Multi-variable search result rejected for r0v6, resolved type: byte[] */
    /* JADX DEBUG: Multi-variable search result rejected for r7v7, resolved type: byte */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v4, types: [int[]] */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005f, code lost:
        r0[r6] = (byte) r2;
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009d, code lost:
        r0[r6] = (short) r2;
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d2, code lost:
        r0[r7] = r2;
        r3 = r3 + 1;
     */
    static <K, V> zzdl<K, V> zza(int i, Object[] objArr) {
        byte[] bArr;
        boolean z = true;
        zzct.zzd(4, objArr.length >> 1);
        int max = Math.max(4, 2);
        int i2 = 1073741824;
        int i3 = 0;
        if (max < 751619276) {
            i2 = Integer.highestOneBit(max - 1) << 1;
            while (((double) i2) * 0.7d < ((double) max)) {
                i2 <<= 1;
            }
        } else {
            if (max >= 1073741824) {
                z = false;
            }
            zzct.checkArgument(z, "collection too large");
        }
        int i4 = i2 - 1;
        if (i2 <= 128) {
            bArr = new byte[i2];
            Arrays.fill(bArr, (byte) -1);
            while (i3 < 4) {
                int i5 = i3 * 2;
                Object obj = objArr[i5];
                Object obj2 = objArr[i5 ^ 1];
                zzda.zza(obj, obj2);
                int zzs = zzdd.zzs(obj.hashCode());
                while (true) {
                    int i6 = zzs & i4;
                    int i7 = bArr[i6] & 255;
                    if (i7 == 255) {
                        break;
                    } else if (!objArr[i7].equals(obj)) {
                        zzs = i6 + 1;
                    } else {
                        throw zza(obj, obj2, objArr, i7);
                    }
                }
            }
        } else if (i2 <= 32768) {
            bArr = new short[i2];
            Arrays.fill(bArr, (short) -1);
            while (i3 < 4) {
                int i8 = i3 * 2;
                Object obj3 = objArr[i8];
                Object obj4 = objArr[i8 ^ 1];
                zzda.zza(obj3, obj4);
                int zzs2 = zzdd.zzs(obj3.hashCode());
                while (true) {
                    int i9 = zzs2 & i4;
                    int i10 = bArr[i9] & 65535;
                    if (i10 == 65535) {
                        break;
                    } else if (!objArr[i10].equals(obj3)) {
                        zzs2 = i9 + 1;
                    } else {
                        throw zza(obj3, obj4, objArr, i10);
                    }
                }
            }
        } else {
            bArr = new int[i2];
            Arrays.fill((int[]) bArr, -1);
            while (i3 < 4) {
                int i11 = i3 * 2;
                Object obj5 = objArr[i11];
                Object obj6 = objArr[i11 ^ 1];
                zzda.zza(obj5, obj6);
                int zzs3 = zzdd.zzs(obj5.hashCode());
                while (true) {
                    int i12 = zzs3 & i4;
                    int i13 = bArr[i12];
                    if (i13 == -1) {
                        break;
                    } else if (!objArr[i13].equals(obj5)) {
                        zzs3 = i12 + 1;
                    } else {
                        throw zza(obj5, obj6, objArr, i13);
                    }
                }
            }
        }
        return new zzdl<>(bArr, objArr, 4);
    }

    private static IllegalArgumentException zza(Object obj, Object obj2, Object[] objArr, int i) {
        String valueOf = String.valueOf(obj);
        String valueOf2 = String.valueOf(obj2);
        String valueOf3 = String.valueOf(objArr[i]);
        String valueOf4 = String.valueOf(objArr[i ^ 1]);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length());
        sb.append("Multiple entries with same key: ");
        sb.append(valueOf);
        sb.append("=");
        sb.append(valueOf2);
        sb.append(" and ");
        sb.append(valueOf3);
        sb.append("=");
        sb.append(valueOf4);
        return new IllegalArgumentException(sb.toString());
    }

    private zzdl(Object obj, Object[] objArr, int i) {
        this.zzme = obj;
        this.zzmb = objArr;
        this.size = i;
    }

    public final int size() {
        return this.size;
    }

    @Override // java.util.Map, com.google.android.gms.internal.vision.zzdg
    @NullableDecl
    public final V get(@NullableDecl Object obj) {
        Object obj2 = this.zzme;
        Object[] objArr = this.zzmb;
        int i = this.size;
        if (obj == null) {
            return null;
        }
        if (i == 1) {
            if (objArr[0].equals(obj)) {
                return (V) objArr[1];
            }
            return null;
        } else if (obj2 == null) {
            return null;
        } else {
            if (obj2 instanceof byte[]) {
                byte[] bArr = (byte[]) obj2;
                int length = bArr.length - 1;
                int zzs = zzdd.zzs(obj.hashCode());
                while (true) {
                    int i2 = zzs & length;
                    int i3 = bArr[i2] & UByte.MAX_VALUE;
                    if (i3 == 255) {
                        return null;
                    }
                    if (objArr[i3].equals(obj)) {
                        return (V) objArr[i3 ^ 1];
                    }
                    zzs = i2 + 1;
                }
            } else if (obj2 instanceof short[]) {
                short[] sArr = (short[]) obj2;
                int length2 = sArr.length - 1;
                int zzs2 = zzdd.zzs(obj.hashCode());
                while (true) {
                    int i4 = zzs2 & length2;
                    int i5 = sArr[i4] & UShort.MAX_VALUE;
                    if (i5 == 65535) {
                        return null;
                    }
                    if (objArr[i5].equals(obj)) {
                        return (V) objArr[i5 ^ 1];
                    }
                    zzs2 = i4 + 1;
                }
            } else {
                int[] iArr = (int[]) obj2;
                int length3 = iArr.length - 1;
                int zzs3 = zzdd.zzs(obj.hashCode());
                while (true) {
                    int i6 = zzs3 & length3;
                    int i7 = iArr[i6];
                    if (i7 == -1) {
                        return null;
                    }
                    if (objArr[i7].equals(obj)) {
                        return (V) objArr[i7 ^ 1];
                    }
                    zzs3 = i6 + 1;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdg
    public final zzdj<Map.Entry<K, V>> zzce() {
        return new zzdk(this, this.zzmb, 0, this.size);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdg
    public final zzdj<K> zzcf() {
        return new zzdm(this, new zzdp(this.zzmb, 0, this.size));
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdg
    public final zzdc<V> zzcg() {
        return new zzdp(this.zzmb, 1, this.size);
    }
}
