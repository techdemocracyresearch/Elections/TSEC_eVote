package com.google.android.gms.internal.vision;

import java.util.Iterator;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzdk<K, V> extends zzdj<Map.Entry<K, V>> {
    private final transient int size;
    private final transient zzdg<K, V> zzma;
    private final transient Object[] zzmb;
    private final transient int zzmc = 0;

    zzdk(zzdg<K, V> zzdg, Object[] objArr, int i, int i2) {
        this.zzma = zzdg;
        this.zzmb = objArr;
        this.size = i2;
    }

    @Override // com.google.android.gms.internal.vision.zzdc
    public final zzdr<Map.Entry<K, V>> zzby() {
        return (zzdr) zzcc().iterator();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdc
    public final int zza(Object[] objArr, int i) {
        return zzcc().zza(objArr, i);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdj
    public final zzdf<Map.Entry<K, V>> zzch() {
        return new zzdn(this);
    }

    @Override // com.google.android.gms.internal.vision.zzdc
    public final boolean contains(Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            Object value = entry.getValue();
            if (value == null || !value.equals(this.zzma.get(key))) {
                return false;
            }
            return true;
        }
        return false;
    }

    public final int size() {
        return this.size;
    }

    @Override // com.google.android.gms.internal.vision.zzdc, java.util.AbstractCollection, java.util.Collection, java.util.Set, com.google.android.gms.internal.vision.zzdj, java.lang.Iterable
    public final /* synthetic */ Iterator iterator() {
        return iterator();
    }
}
