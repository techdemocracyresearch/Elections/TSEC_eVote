package com.google.android.gms.internal.vision;

import java.util.Iterator;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzja {
    private static final Iterator<Object> zzaaj = new zziz();
    private static final Iterable<Object> zzaak = new zzjc();

    static <T> Iterable<T> zzid() {
        return (Iterable<T>) zzaak;
    }
}
