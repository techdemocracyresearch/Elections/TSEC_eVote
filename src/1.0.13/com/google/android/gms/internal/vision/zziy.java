package com.google.android.gms.internal.vision;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zziy implements Iterator<Map.Entry<K, V>> {
    private int pos;
    private final /* synthetic */ zziw zzaah;
    private Iterator<Map.Entry<K, V>> zzaai;

    private zziy(zziw zziw) {
        this.zzaah = zziw;
        this.pos = zziw.zzaac.size();
    }

    public final boolean hasNext() {
        int i = this.pos;
        return (i > 0 && i <= this.zzaah.zzaac.size()) || zzic().hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    private final Iterator<Map.Entry<K, V>> zzic() {
        if (this.zzaai == null) {
            this.zzaai = this.zzaah.zzaaf.entrySet().iterator();
        }
        return this.zzaai;
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        if (zzic().hasNext()) {
            return (Map.Entry) zzic().next();
        }
        List list = this.zzaah.zzaac;
        int i = this.pos - 1;
        this.pos = i;
        return (Map.Entry) list.get(i);
    }

    /* synthetic */ zziy(zziw zziw, zziv zziv) {
        this(zziw);
    }
}
