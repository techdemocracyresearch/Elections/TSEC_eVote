package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzgs;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
final /* synthetic */ class zzby {
    static final /* synthetic */ int[] zzbe;

    /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0037 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x000f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0017 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0027 */
    static {
        int[] iArr = new int[zzgs.zzf.values$50KLMJ33DTMIUPRFDTJMOP9FE1P6UT3FC9QMCBQ7CLN6ASJ1EHIM8JB5EDPM2PR59HKN8P949LIN8Q3FCHA6UIBEEPNMMP9R0().length];
        zzbe = iArr;
        iArr[zzgs.zzf.zzwu - 1] = 1;
        zzbe[zzgs.zzf.zzwv - 1] = 2;
        zzbe[zzgs.zzf.zzwt - 1] = 3;
        zzbe[zzgs.zzf.zzww - 1] = 4;
        zzbe[zzgs.zzf.zzwx - 1] = 5;
        zzbe[zzgs.zzf.zzwr - 1] = 6;
        zzbe[zzgs.zzf.zzws - 1] = 7;
    }
}
