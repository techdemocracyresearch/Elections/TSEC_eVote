package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzgs;
import com.google.android.gms.internal.vision.zzgs.zza;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public abstract class zzgs<MessageType extends zzgs<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzet<MessageType, BuilderType> {
    private static Map<Object, zzgs<?, ?>> zzwl = new ConcurrentHashMap();
    protected zzjm zzwj = zzjm.zzig();
    private int zzwk = -1;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static class zzc<T extends zzgs<T, ?>> extends zzey<T> {
        private final T zzwg;

        public zzc(T t) {
            this.zzwg = t;
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzf {
        public static final int zzwr = 1;
        public static final int zzws = 2;
        public static final int zzwt = 3;
        public static final int zzwu = 4;
        public static final int zzwv = 5;
        public static final int zzww = 6;
        public static final int zzwx = 7;
        private static final /* synthetic */ int[] zzwy = {1, 2, 3, 4, 5, 6, 7};
        public static final int zzwz = 1;
        public static final int zzxa = 2;
        private static final /* synthetic */ int[] zzxb = {1, 2};
        public static final int zzxc = 1;
        public static final int zzxd = 2;
        private static final /* synthetic */ int[] zzxe = {1, 2};

        public static int[] values$50KLMJ33DTMIUPRFDTJMOP9FE1P6UT3FC9QMCBQ7CLN6ASJ1EHIM8JB5EDPM2PR59HKN8P949LIN8Q3FCHA6UIBEEPNMMP9R0() {
            return (int[]) zzwy.clone();
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static abstract class zzb<MessageType extends zze<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zza<MessageType, BuilderType> implements zzie {
        protected zzb(MessageType messagetype) {
            super(messagetype);
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.vision.zzgs.zza
        public void zzfy() {
            super.zzfy();
            ((zze) this.zzwh).zzwq = (zzgi) ((zze) this.zzwh).zzwq.clone();
        }

        @Override // com.google.android.gms.internal.vision.zzgs.zza
        public /* synthetic */ zzgs zzfz() {
            return (zze) zzgb();
        }

        @Override // com.google.android.gms.internal.vision.zzib, com.google.android.gms.internal.vision.zzgs.zza
        public /* synthetic */ zzic zzgb() {
            if (this.zzwi) {
                return (zze) this.zzwh;
            }
            ((zze) this.zzwh).zzwq.zzdp();
            return (zze) super.zzgb();
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static abstract class zze<MessageType extends zze<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzgs<MessageType, BuilderType> implements zzie {
        protected zzgi<zzd> zzwq = zzgi.zzfn();

        /* access modifiers changed from: package-private */
        public final zzgi<zzd> zzgk() {
            if (this.zzwq.isImmutable()) {
                this.zzwq = (zzgi) this.zzwq.clone();
            }
            return this.zzwq;
        }

        public final <Type> Type zzc(zzge<MessageType, Type> zzge) {
            zzg zza = zzgs.zza(zzge);
            if (zza.zzxf == ((zzgs) zzgd())) {
                Type<Object> type = (Type) this.zzwq.zza(zza.zzxh);
                if (type == null) {
                    return zza.zzgd;
                }
                if (!zza.zzxh.zzwo) {
                    return (Type) zza.zzj(type);
                }
                if (zza.zzxh.zzwn.zzip() != zzkd.ENUM) {
                    return type;
                }
                Type type2 = (Type) new ArrayList();
                for (Object obj : type) {
                    type2.add(zza.zzj(obj));
                }
                return type2;
            }
            throw new IllegalArgumentException("This extension is for a different message type.  Please make sure that you are not suppressing any generics type warnings.");
        }
    }

    public String toString() {
        return zzid.zza(this, super.toString());
    }

    public int hashCode() {
        if (this.zzro != 0) {
            return this.zzro;
        }
        this.zzro = zzin.zzho().zzv(this).hashCode(this);
        return this.zzro;
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzd implements zzgk<zzd> {
        final int number = 202056002;
        final zzgv<?> zzwm = null;
        final zzka zzwn;
        final boolean zzwo;
        final boolean zzwp;

        zzd(zzgv<?> zzgv, int i, zzka zzka, boolean z, boolean z2) {
            this.zzwn = zzka;
            this.zzwo = true;
            this.zzwp = false;
        }

        @Override // com.google.android.gms.internal.vision.zzgk
        public final int zzag() {
            return this.number;
        }

        @Override // com.google.android.gms.internal.vision.zzgk
        public final zzka zzfs() {
            return this.zzwn;
        }

        @Override // com.google.android.gms.internal.vision.zzgk
        public final zzkd zzft() {
            return this.zzwn.zzip();
        }

        @Override // com.google.android.gms.internal.vision.zzgk
        public final boolean zzfu() {
            return this.zzwo;
        }

        @Override // com.google.android.gms.internal.vision.zzgk
        public final boolean zzfv() {
            return this.zzwp;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.google.android.gms.internal.vision.zzgs$zza */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.android.gms.internal.vision.zzgk
        public final zzib zza(zzib zzib, zzic zzic) {
            return ((zza) zzib).zza((zzgs) zzic);
        }

        @Override // com.google.android.gms.internal.vision.zzgk
        public final zzih zza(zzih zzih, zzih zzih2) {
            throw new UnsupportedOperationException();
        }

        @Override // java.lang.Comparable
        public final /* synthetic */ int compareTo(Object obj) {
            return this.number - ((zzd) obj).number;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static abstract class zza<MessageType extends zzgs<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzew<MessageType, BuilderType> {
        private final MessageType zzwg;
        protected MessageType zzwh;
        protected boolean zzwi = false;

        protected zza(MessageType messagetype) {
            this.zzwg = messagetype;
            this.zzwh = (MessageType) ((zzgs) messagetype.zza(zzf.zzwu, null, null));
        }

        /* access modifiers changed from: protected */
        public void zzfy() {
            MessageType messagetype = (MessageType) ((zzgs) this.zzwh.zza(zzf.zzwu, null, null));
            zza(messagetype, this.zzwh);
            this.zzwh = messagetype;
        }

        @Override // com.google.android.gms.internal.vision.zzie
        public final boolean isInitialized() {
            return zzgs.zza((zzgs) this.zzwh, false);
        }

        /* renamed from: zzfz */
        public MessageType zzgb() {
            if (this.zzwi) {
                return this.zzwh;
            }
            MessageType messagetype = this.zzwh;
            zzin.zzho().zzv(messagetype).zzh(messagetype);
            this.zzwi = true;
            return this.zzwh;
        }

        /* renamed from: zzga */
        public final MessageType zzgc() {
            MessageType messagetype = (MessageType) ((zzgs) zzgb());
            if (messagetype.isInitialized()) {
                return messagetype;
            }
            throw new zzjk(messagetype);
        }

        public final BuilderType zza(MessageType messagetype) {
            if (this.zzwi) {
                zzfy();
                this.zzwi = false;
            }
            zza(this.zzwh, messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzin.zzho().zzv(messagetype).zzd(messagetype, messagetype2);
        }

        private final BuilderType zzb(byte[] bArr, int i, int i2, zzgd zzgd) throws zzhc {
            if (this.zzwi) {
                zzfy();
                this.zzwi = false;
            }
            try {
                zzin.zzho().zzv(this.zzwh).zza(this.zzwh, bArr, 0, i2 + 0, new zzfb(zzgd));
                return this;
            } catch (zzhc e) {
                throw e;
            } catch (IndexOutOfBoundsException unused) {
                throw zzhc.zzgm();
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: zzb */
        public final BuilderType zza(zzft zzft, zzgd zzgd) throws IOException {
            if (this.zzwi) {
                zzfy();
                this.zzwi = false;
            }
            try {
                zzin.zzho().zzv(this.zzwh).zza(this.zzwh, zzfy.zza(zzft), zzgd);
                return this;
            } catch (RuntimeException e) {
                if (e.getCause() instanceof IOException) {
                    throw ((IOException) e.getCause());
                }
                throw e;
            }
        }

        @Override // com.google.android.gms.internal.vision.zzew
        public final /* synthetic */ zzew zza(byte[] bArr, int i, int i2, zzgd zzgd) throws zzhc {
            return zzb(bArr, 0, i2, zzgd);
        }

        @Override // com.google.android.gms.internal.vision.zzew
        public final /* synthetic */ zzew zzdn() {
            return (zza) clone();
        }

        @Override // com.google.android.gms.internal.vision.zzie
        public final /* synthetic */ zzic zzgd() {
            return this.zzwg;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.google.android.gms.internal.vision.zzgs$zza */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.android.gms.internal.vision.zzew, java.lang.Object
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zza zza = (zza) this.zzwg.zza(zzf.zzwv, null, null);
            zza.zza((zzgs) zzgb());
            return zza;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static class zzg<ContainingType extends zzic, Type> extends zzge<ContainingType, Type> {
        final Type zzgd;
        final ContainingType zzxf;
        final zzic zzxg;
        final zzd zzxh;

        zzg(ContainingType containingtype, Type type, zzic zzic, zzd zzd, Class cls) {
            if (containingtype == null) {
                throw new IllegalArgumentException("Null containingTypeDefaultInstance");
            } else if (zzd.zzwn == zzka.MESSAGE && zzic == null) {
                throw new IllegalArgumentException("Null messageDefaultInstance");
            } else {
                this.zzxf = containingtype;
                this.zzgd = type;
                this.zzxg = zzic;
                this.zzxh = zzd;
            }
        }

        /* access modifiers changed from: package-private */
        public final Object zzj(Object obj) {
            return this.zzxh.zzwn.zzip() == zzkd.ENUM ? this.zzxh.zzwm.zzh(((Integer) obj).intValue()) : obj;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return zzin.zzho().zzv(this).equals(this, (zzgs) obj);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzgs<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> BuilderType zzge() {
        return (BuilderType) ((zza) zza(zzf.zzwv, (Object) null, (Object) null));
    }

    @Override // com.google.android.gms.internal.vision.zzie
    public final boolean isInitialized() {
        Boolean bool = Boolean.TRUE;
        return zza((zzgs) this, true);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzet
    public final int zzdl() {
        return this.zzwk;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzet
    public final void zzae(int i) {
        this.zzwk = i;
    }

    @Override // com.google.android.gms.internal.vision.zzic
    public final void zzb(zzga zzga) throws IOException {
        zzin.zzho().zzv(this).zza(this, zzgc.zza(zzga));
    }

    @Override // com.google.android.gms.internal.vision.zzic
    public final int zzgf() {
        if (this.zzwk == -1) {
            this.zzwk = zzin.zzho().zzv(this).zzs(this);
        }
        return this.zzwk;
    }

    static <T extends zzgs<?, ?>> T zzd(Class<T> cls) {
        zzgs<?, ?> zzgs = zzwl.get(cls);
        if (zzgs == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                zzgs = (T) zzwl.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (zzgs == null) {
            zzgs = (T) ((zzgs) ((zzgs) zzjp.zzh(cls)).zza(zzf.zzww, (Object) null, (Object) null));
            if (zzgs != null) {
                zzwl.put(cls, zzgs);
            } else {
                throw new IllegalStateException();
            }
        }
        return (T) zzgs;
    }

    protected static <T extends zzgs<?, ?>> void zza(Class<T> cls, T t) {
        zzwl.put(cls, t);
    }

    protected static Object zza(zzic zzic, String str, Object[] objArr) {
        return new zzip(zzic, str, objArr);
    }

    public static <ContainingType extends zzic, Type> zzg<ContainingType, Type> zza(ContainingType containingtype, zzic zzic, zzgv<?> zzgv, int i, zzka zzka, boolean z, Class cls) {
        return new zzg<>(containingtype, Collections.emptyList(), zzic, new zzd(null, 202056002, zzka, true, false), cls);
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    /* access modifiers changed from: private */
    public static <MessageType extends zze<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>, T> zzg<MessageType, T> zza(zzge<MessageType, T> zzge) {
        return (zzg) zzge;
    }

    protected static final <T extends zzgs<T, ?>> boolean zza(T t, boolean z) {
        byte byteValue = ((Byte) t.zza(zzf.zzwr, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzu = zzin.zzho().zzv(t).zzu(t);
        if (z) {
            t.zza(zzf.zzws, zzu ? t : null, null);
        }
        return zzu;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.gms.internal.vision.zzgx, com.google.android.gms.internal.vision.zzgu] */
    /* JADX WARNING: Unknown variable types count: 1 */
    protected static zzgx zzgg() {
        return zzgu.zzgl();
    }

    protected static <E> zzgz<E> zzgh() {
        return zziq.zzhr();
    }

    protected static <E> zzgz<E> zza(zzgz<E> zzgz) {
        int size = zzgz.size();
        return zzgz.zzah(size == 0 ? 10 : size << 1);
    }

    private static <T extends zzgs<T, ?>> T zza(T t, byte[] bArr, int i, int i2, zzgd zzgd) throws zzhc {
        T t2 = (T) ((zzgs) t.zza(zzf.zzwu, null, null));
        try {
            zzir zzv = zzin.zzho().zzv(t2);
            zzv.zza(t2, bArr, 0, i2, new zzfb(zzgd));
            zzv.zzh(t2);
            if (((zzgs) t2).zzro == 0) {
                return t2;
            }
            throw new RuntimeException();
        } catch (IOException e) {
            if (e.getCause() instanceof zzhc) {
                throw ((zzhc) e.getCause());
            }
            throw new zzhc(e.getMessage()).zzg(t2);
        } catch (IndexOutOfBoundsException unused) {
            throw zzhc.zzgm().zzg(t2);
        }
    }

    private static <T extends zzgs<T, ?>> T zzb(T t) throws zzhc {
        if (t == null || t.isInitialized()) {
            return t;
        }
        throw new zzhc(new zzjk(t).getMessage()).zzg(t);
    }

    protected static <T extends zzgs<T, ?>> T zza(T t, byte[] bArr) throws zzhc {
        return (T) zzb(zza(t, bArr, 0, bArr.length, zzgd.zzfl()));
    }

    protected static <T extends zzgs<T, ?>> T zza(T t, byte[] bArr, zzgd zzgd) throws zzhc {
        return (T) zzb(zza(t, bArr, 0, bArr.length, zzgd));
    }

    @Override // com.google.android.gms.internal.vision.zzic
    public final /* synthetic */ zzib zzgi() {
        zza zza2 = (zza) zza(zzf.zzwv, (Object) null, (Object) null);
        zza2.zza((zzgs) this);
        return zza2;
    }

    @Override // com.google.android.gms.internal.vision.zzic
    public final /* synthetic */ zzib zzgj() {
        return (zza) zza(zzf.zzwv, (Object) null, (Object) null);
    }

    @Override // com.google.android.gms.internal.vision.zzie
    public final /* synthetic */ zzic zzgd() {
        return (zzgs) zza(zzf.zzww, (Object) null, (Object) null);
    }
}
