package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzgs;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzgc implements zzkg {
    private final zzga zzsj;

    public static zzgc zza(zzga zzga) {
        if (zzga.zzsy != null) {
            return zzga.zzsy;
        }
        return new zzgc(zzga);
    }

    private zzgc(zzga zzga) {
        zzga zzga2 = (zzga) zzgt.zza(zzga, "output");
        this.zzsj = zzga2;
        zzga2.zzsy = this;
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final int zzfj() {
        return zzgs.zzf.zzxc;
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzq(int i, int i2) throws IOException {
        this.zzsj.zzj(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzi(int i, long j) throws IOException {
        this.zzsj.zza(i, j);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzj(int i, long j) throws IOException {
        this.zzsj.zzc(i, j);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, float f) throws IOException {
        this.zzsj.zza(i, f);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, double d) throws IOException {
        this.zzsj.zza(i, d);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzr(int i, int i2) throws IOException {
        this.zzsj.zzg(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, long j) throws IOException {
        this.zzsj.zza(i, j);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzg(int i, int i2) throws IOException {
        this.zzsj.zzg(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzc(int i, long j) throws IOException {
        this.zzsj.zzc(i, j);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzj(int i, int i2) throws IOException {
        this.zzsj.zzj(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, boolean z) throws IOException {
        this.zzsj.zza(i, z);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, String str) throws IOException {
        this.zzsj.zza(i, str);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, zzfh zzfh) throws IOException {
        this.zzsj.zza(i, zzfh);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzh(int i, int i2) throws IOException {
        this.zzsj.zzh(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzi(int i, int i2) throws IOException {
        this.zzsj.zzi(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzb(int i, long j) throws IOException {
        this.zzsj.zzb(i, j);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, Object obj, zzir zzir) throws IOException {
        this.zzsj.zza(i, (zzic) obj, zzir);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzb(int i, Object obj, zzir zzir) throws IOException {
        zzga zzga = this.zzsj;
        zzga.writeTag(i, 3);
        zzir.zza((zzic) obj, zzga.zzsy);
        zzga.writeTag(i, 4);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzbk(int i) throws IOException {
        this.zzsj.writeTag(i, 3);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzbl(int i) throws IOException {
        this.zzsj.writeTag(i, 4);
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, Object obj) throws IOException {
        if (obj instanceof zzfh) {
            this.zzsj.zzb(i, (zzfh) obj);
        } else {
            this.zzsj.zza(i, (zzic) obj);
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzbc(list.get(i4).intValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzax(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zzg(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzbf(list.get(i4).intValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzba(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zzj(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzv(list.get(i4).longValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzs(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zza(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzw(list.get(i4).longValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzs(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zza(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzy(list.get(i4).longValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzu(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zzc(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzt(list.get(i4).floatValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzs(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zza(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzb(list.get(i4).doubleValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zza(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zza(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzbh(list.get(i4).intValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzax(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zzg(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzl(list.get(i4).booleanValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzk(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zza(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof zzhj) {
            zzhj zzhj = (zzhj) list;
            while (i2 < list.size()) {
                Object raw = zzhj.getRaw(i2);
                if (raw instanceof String) {
                    this.zzsj.zza(i, (String) raw);
                } else {
                    this.zzsj.zza(i, (zzfh) raw);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zza(i, list.get(i2));
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzb(int i, List<zzfh> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.zzsj.zza(i, list.get(i2));
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzbd(list.get(i4).intValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzay(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zzh(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzbg(list.get(i4).intValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzba(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zzj(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzz(list.get(i4).longValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzu(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zzc(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzbe(list.get(i4).intValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzaz(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zzi(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzsj.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzga.zzx(list.get(i4).longValue());
            }
            this.zzsj.zzay(i3);
            while (i2 < list.size()) {
                this.zzsj.zzt(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzsj.zzb(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zza(int i, List<?> list, zzir zzir) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            zza(i, list.get(i2), zzir);
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final void zzb(int i, List<?> list, zzir zzir) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            zzb(i, list.get(i2), zzir);
        }
    }

    @Override // com.google.android.gms.internal.vision.zzkg
    public final <K, V> void zza(int i, zzht<K, V> zzht, Map<K, V> map) throws IOException {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            this.zzsj.writeTag(i, 2);
            this.zzsj.zzay(zzhu.zza(zzht, entry.getKey(), entry.getValue()));
            zzhu.zza(this.zzsj, zzht, entry.getKey(), entry.getValue());
        }
    }
}
