package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public abstract class zzfs extends zzfh {
    zzfs() {
    }

    /* access modifiers changed from: package-private */
    public abstract boolean zza(zzfh zzfh, int i, int i2);
}
