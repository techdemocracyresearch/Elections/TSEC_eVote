package com.google.android.gms.internal.vision;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzhn extends zzhm {
    private zzhn() {
        super();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzhm
    public final <L> List<L> zza(Object obj, long j) {
        zzgz zzc = zzc(obj, j);
        if (zzc.zzdo()) {
            return zzc;
        }
        int size = zzc.size();
        zzgz zzah = zzc.zzah(size == 0 ? 10 : size << 1);
        zzjp.zza(obj, j, zzah);
        return zzah;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzhm
    public final void zzb(Object obj, long j) {
        zzc(obj, j).zzdp();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [com.google.android.gms.internal.vision.zzgz] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzhm
    public final <E> void zza(Object obj, Object obj2, long j) {
        zzgz<E> zzc = zzc(obj, j);
        zzgz<E> zzc2 = zzc(obj2, j);
        int size = zzc.size();
        int size2 = zzc2.size();
        zzgz<E> zzgz = zzc;
        zzgz = zzc;
        if (size > 0 && size2 > 0) {
            boolean zzdo = zzc.zzdo();
            zzgz<E> zzgz2 = zzc;
            if (!zzdo) {
                zzgz2 = zzc.zzah(size2 + size);
            }
            zzgz2.addAll(zzc2);
            zzgz = zzgz2;
        }
        if (size > 0) {
            zzc2 = zzgz;
        }
        zzjp.zza(obj, j, zzc2);
    }

    private static <E> zzgz<E> zzc(Object obj, long j) {
        return (zzgz) zzjp.zzp(obj, j);
    }
}
