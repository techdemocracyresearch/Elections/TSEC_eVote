package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final /* synthetic */ class zzgm {
    static final /* synthetic */ int[] zzto;
    static final /* synthetic */ int[] zztp;

    /* JADX WARNING: Can't wrap try/catch for region: R(15:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|16|17|18|20) */
    /* JADX WARNING: Can't wrap try/catch for region: R(17:0|1|2|3|5|6|7|9|10|11|13|14|15|16|17|18|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0039 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0043 */
    static {
        int[] iArr = new int[zzhe.values().length];
        zztp = iArr;
        try {
            iArr[zzhe.BYTE_STRING.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            zztp[zzhe.MESSAGE.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            zztp[zzhe.STRING.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        int[] iArr2 = new int[zzgp.values().length];
        zzto = iArr2;
        iArr2[zzgp.MAP.ordinal()] = 1;
        zzto[zzgp.VECTOR.ordinal()] = 2;
        zzto[zzgp.SCALAR.ordinal()] = 3;
    }
}
