package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzmo extends zzmr {
    private final /* synthetic */ zzmp zzakg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzmo(zzmp zzmp, zzmm zzmm, CharSequence charSequence) {
        super(zzmm, charSequence);
        this.zzakg = zzmp;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmr
    public final int zzao(int i) {
        return i + 1;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzmr
    public final int zzan(int i) {
        return this.zzakg.zzakh.zza(this.zzaki, i);
    }
}
