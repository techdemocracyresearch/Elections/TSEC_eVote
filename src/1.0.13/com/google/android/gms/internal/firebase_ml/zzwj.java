package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzwj extends zzwh {
    private final byte[] buffer;
    private int limit;
    private int pos;
    private final boolean zzcia;
    private int zzcib;
    private int zzcic;
    private int zzcid;

    private zzwj(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.zzcid = Integer.MAX_VALUE;
        this.buffer = bArr;
        this.limit = i2 + i;
        this.pos = i;
        this.zzcic = i;
        this.zzcia = z;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzwh
    public final int zzcz(int i) throws zzxk {
        if (i >= 0) {
            int zztx = i + zztx();
            int i2 = this.zzcid;
            if (zztx <= i2) {
                this.zzcid = zztx;
                int i3 = this.limit + this.zzcib;
                this.limit = i3;
                int i4 = i3 - this.zzcic;
                if (i4 > zztx) {
                    int i5 = i4 - zztx;
                    this.zzcib = i5;
                    this.limit = i3 - i5;
                } else {
                    this.zzcib = 0;
                }
                return i2;
            }
            throw zzxk.zzve();
        }
        throw zzxk.zzvf();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzwh
    public final int zztx() {
        return this.pos - this.zzcic;
    }
}
