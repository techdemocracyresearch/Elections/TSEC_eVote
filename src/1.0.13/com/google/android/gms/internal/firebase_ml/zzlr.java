package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;
import java.util.NoSuchElementException;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
abstract class zzlr<T> implements Iterator<T> {
    private int zzajm = zzlt.zzajq;
    @NullableDecl
    private T zzajn;

    protected zzlr() {
    }

    /* access modifiers changed from: protected */
    public abstract T zzjc();

    /* access modifiers changed from: protected */
    @NullableDecl
    public final T zzjd() {
        this.zzajm = zzlt.zzajr;
        return null;
    }

    public final boolean hasNext() {
        zzml.checkState(this.zzajm != zzlt.zzajs);
        int i = zzlu.zzaju[this.zzajm - 1];
        if (i == 1) {
            return false;
        }
        if (i == 2) {
            return true;
        }
        this.zzajm = zzlt.zzajs;
        this.zzajn = zzjc();
        if (this.zzajm == zzlt.zzajr) {
            return false;
        }
        this.zzajm = zzlt.zzajp;
        return true;
    }

    @Override // java.util.Iterator
    public final T next() {
        if (hasNext()) {
            this.zzajm = zzlt.zzajq;
            T t = this.zzajn;
            this.zzajn = null;
            return t;
        }
        throw new NoSuchElementException();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
