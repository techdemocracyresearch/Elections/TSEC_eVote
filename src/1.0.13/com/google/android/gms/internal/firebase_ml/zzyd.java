package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzyd implements zzyl {
    private zzyl[] zzcnq;

    zzyd(zzyl... zzylArr) {
        this.zzcnq = zzylArr;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyl
    public final boolean zzi(Class<?> cls) {
        for (zzyl zzyl : this.zzcnq) {
            if (zzyl.zzi(cls)) {
                return true;
            }
        }
        return false;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyl
    public final zzyi zzj(Class<?> cls) {
        zzyl[] zzylArr = this.zzcnq;
        for (zzyl zzyl : zzylArr) {
            if (zzyl.zzi(cls)) {
                return zzyl.zzj(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
