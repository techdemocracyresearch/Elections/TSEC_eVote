package com.google.android.gms.internal.firebase_ml;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class zzgh {
    private static final zzgh zzabh = new zzgh();
    private final String zzabi;

    /* JADX WARNING: Illegal instructions before constructor call */
    zzgh() {
        this(r0, zzmt.OS_NAME.value(), zzmt.OS_VERSION.value(), zzgc.VERSION);
        String str;
        String property = System.getProperty("java.version");
        if (property.startsWith("9")) {
            str = "9.0.0";
        } else {
            str = zzp(property);
        }
    }

    private zzgh(String str, String str2, String str3, String str4) {
        StringBuilder sb = new StringBuilder("java/");
        sb.append(zzp(str));
        sb.append(" http-google-%s/");
        sb.append(zzp(str4));
        if (!(str2 == null || str3 == null)) {
            sb.append(" ");
            sb.append(zzo(str2));
            sb.append("/");
            sb.append(zzp(str3));
        }
        this.zzabi = sb.toString();
    }

    public final String zzn(String str) {
        return String.format(this.zzabi, zzo(str));
    }

    public static zzgh zzfn() {
        return zzabh;
    }

    private static String zzo(String str) {
        return str.toLowerCase().replaceAll("[^\\w\\d\\-]", "-");
    }

    private static String zzp(String str) {
        if (str == null) {
            return null;
        }
        Matcher matcher = Pattern.compile("(\\d+\\.\\d+\\.\\d+).*").matcher(str);
        return matcher.find() ? matcher.group(1) : str;
    }
}
