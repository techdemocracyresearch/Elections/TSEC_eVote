package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzge {
    private static final Logger logger = Logger.getLogger(zzge.class.getName());
    private final zzhb zzaam;
    private final zzgj zzaan;
    private final String zzaao;
    private final String zzaap;
    private final String zzaaq;
    private final String zzaar;
    private final zzjm zzaas;
    private final boolean zzaat;
    private final boolean zzaau;

    protected zzge(zza zza2) {
        zzhb zzhb;
        this.zzaan = zza2.zzaan;
        this.zzaao = zzh(zza2.zzaao);
        this.zzaap = zzi(zza2.zzaap);
        this.zzaaq = zza2.zzaaq;
        if (zzms.zzbc(zza2.zzaar)) {
            logger.logp(Level.WARNING, "com.google.api.client.googleapis.services.AbstractGoogleClient", "<init>", "Application name is not set. Call Builder#setApplicationName.");
        }
        this.zzaar = zza2.zzaar;
        if (zza2.zzabg == null) {
            zzhb = zza2.zzabf.zza(null);
        } else {
            zzhb = zza2.zzabf.zza(zza2.zzabg);
        }
        this.zzaam = zzhb;
        this.zzaas = zza2.zzaas;
        this.zzaat = false;
        this.zzaau = false;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static abstract class zza {
        zzgj zzaan;
        String zzaao;
        String zzaap;
        String zzaaq;
        String zzaar;
        final zzjm zzaas;
        final zzhh zzabf;
        zzhe zzabg;

        protected zza(zzhh zzhh, String str, String str2, zzjm zzjm, zzhe zzhe) {
            this.zzabf = (zzhh) zzml.checkNotNull(zzhh);
            this.zzaas = zzjm;
            zzj(str);
            zzk(str2);
            this.zzabg = zzhe;
        }

        public zza zzj(String str) {
            this.zzaao = zzge.zzh(str);
            return this;
        }

        public zza zzk(String str) {
            this.zzaap = zzge.zzi(str);
            return this;
        }

        public zza zzl(String str) {
            this.zzaaq = str;
            return this;
        }

        public zza zza(zzgj zzgj) {
            this.zzaan = zzgj;
            return this;
        }

        public zza zzm(String str) {
            this.zzaar = str;
            return this;
        }
    }

    public final String zzff() {
        String valueOf = String.valueOf(this.zzaao);
        String valueOf2 = String.valueOf(this.zzaap);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    public final String zzfg() {
        return this.zzaar;
    }

    public final zzhb zzfh() {
        return this.zzaam;
    }

    public zzjm zzfi() {
        return this.zzaas;
    }

    /* access modifiers changed from: protected */
    public void zza(zzgg<?> zzgg) throws IOException {
        zzgj zzgj = this.zzaan;
        if (zzgj != null) {
            zzgj.zza(zzgg);
        }
    }

    static String zzh(String str) {
        zzml.checkNotNull(str, "root URL cannot be null.");
        return !str.endsWith("/") ? String.valueOf(str).concat("/") : str;
    }

    static String zzi(String str) {
        zzml.checkNotNull(str, "service path cannot be null");
        if (str.length() == 1) {
            zzml.checkArgument("/".equals(str), "service path must equal \"/\" if it is of length 1.");
            return "";
        } else if (str.length() <= 0) {
            return str;
        } else {
            if (!str.endsWith("/")) {
                str = String.valueOf(str).concat("/");
            }
            return str.startsWith("/") ? str.substring(1) : str;
        }
    }
}
