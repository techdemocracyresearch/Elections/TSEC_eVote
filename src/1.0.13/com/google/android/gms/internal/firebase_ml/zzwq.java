package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwt;
import java.io.IOException;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
abstract class zzwq<T extends zzwt<T>> {
    zzwq() {
    }

    /* access modifiers changed from: package-private */
    public abstract int zza(Map.Entry<?, ?> entry);

    /* access modifiers changed from: package-private */
    public abstract Object zza(zzwo zzwo, zzyk zzyk, int i);

    /* access modifiers changed from: package-private */
    public abstract void zza(zzaat zzaat, Map.Entry<?, ?> entry) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract boolean zze(zzyk zzyk);

    /* access modifiers changed from: package-private */
    public abstract zzwr<T> zzo(Object obj);

    /* access modifiers changed from: package-private */
    public abstract zzwr<T> zzp(Object obj);

    /* access modifiers changed from: package-private */
    public abstract void zzq(Object obj);
}
