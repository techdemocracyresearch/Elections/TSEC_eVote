package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzmz<E> extends zzmu<E> {
    private final zzmw<E> zzals;

    zzmz(zzmw<E> zzmw, int i) {
        super(zzmw.size(), i);
        this.zzals = zzmw;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzmu
    public final E get(int i) {
        return this.zzals.get(i);
    }
}
