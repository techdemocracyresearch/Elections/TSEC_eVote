package com.google.android.gms.internal.firebase_ml;

import android.os.SystemClock;
import com.google.android.gms.common.internal.GmsLogger;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzry {
    private static final GmsLogger zzbin = new GmsLogger("StreamingFormatChecker", "");
    private final LinkedList<Long> zzbsa = new LinkedList<>();
    private long zzbsb = -1;

    public final void zzb(zzsf zzsf) {
        if (zzsf.zzbrv.getBitmap() != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.zzbsa.add(Long.valueOf(elapsedRealtime));
            if (this.zzbsa.size() > 5) {
                this.zzbsa.removeFirst();
            }
            if (this.zzbsa.size() == 5 && elapsedRealtime - this.zzbsa.peekFirst().longValue() < 5000) {
                long j = this.zzbsb;
                if (j == -1 || elapsedRealtime - j >= TimeUnit.SECONDS.toMillis(5)) {
                    this.zzbsb = elapsedRealtime;
                    zzbin.w("StreamingFormatChecker", "ML Kit has detected that you seem to pass camera frames to the detector as a Bitmap object. This is inefficient. Please use YUV_420_888 format for camera2 API or NV21 format for (legacy) camera API and directly pass down the byte array to ML Kit.");
                }
            }
        }
    }
}
