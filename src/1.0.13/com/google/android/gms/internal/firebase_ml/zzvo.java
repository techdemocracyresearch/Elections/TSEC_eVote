package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzvo {
    private static final Class<?> zzchh = zzci("libcore.io.Memory");
    private static final boolean zzchi = (zzci("org.robolectric.Robolectric") != null);

    static boolean zztj() {
        return zzchh != null && !zzchi;
    }

    static Class<?> zztk() {
        return zzchh;
    }

    private static <T> Class<T> zzci(String str) {
        try {
            return (Class<T>) Class.forName(str);
        } catch (Throwable unused) {
            return null;
        }
    }
}
