package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzxu {
    private static final zzxu zzcnh = new zzxw();
    private static final zzxu zzcni = new zzxz();

    private zzxu() {
    }

    /* access modifiers changed from: package-private */
    public abstract <L> void zza(Object obj, Object obj2, long j);

    /* access modifiers changed from: package-private */
    public abstract void zzb(Object obj, long j);

    static zzxu zzvp() {
        return zzcnh;
    }

    static zzxu zzvq() {
        return zzcni;
    }
}
