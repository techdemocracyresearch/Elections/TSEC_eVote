package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzwd {
    private final byte[] buffer;
    private final zzwi zzchu;

    private zzwd(int i) {
        byte[] bArr = new byte[i];
        this.buffer = bArr;
        this.zzchu = zzwi.zzg(bArr);
    }

    public final zzvv zztv() {
        this.zzchu.zztz();
        return new zzwf(this.buffer);
    }

    public final zzwi zztw() {
        return this.zzchu;
    }

    /* synthetic */ zzwd(int i, zzvu zzvu) {
        this(i);
    }
}
