package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzgt extends zzjq {
    long getLength() throws IOException;

    String getType();

    boolean zzfr();
}
