package com.google.android.gms.internal.firebase_ml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzxs extends zzvp<String> implements zzxv, RandomAccess {
    private static final zzxs zzcnb;
    private static final zzxv zzcnc;
    private final List<Object> zzcnd;

    public zzxs() {
        this(10);
    }

    public zzxs(int i) {
        this(new ArrayList(i));
    }

    private zzxs(ArrayList<Object> arrayList) {
        this.zzcnd = arrayList;
    }

    public final int size() {
        return this.zzcnd.size();
    }

    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final boolean addAll(int i, Collection<? extends String> collection) {
        zztn();
        if (collection instanceof zzxv) {
            collection = ((zzxv) collection).zzvn();
        }
        boolean addAll = this.zzcnd.addAll(i, collection);
        this.modCount++;
        return addAll;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final void clear() {
        zztn();
        this.zzcnd.clear();
        this.modCount++;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxv
    public final void zze(zzvv zzvv) {
        zztn();
        this.zzcnd.add(zzvv);
        this.modCount++;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxv
    public final Object getRaw(int i) {
        return this.zzcnd.get(i);
    }

    private static String zzs(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzvv) {
            return ((zzvv) obj).zztp();
        }
        return zzxd.zzj((byte[]) obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxv
    public final List<?> zzvn() {
        return Collections.unmodifiableList(this.zzcnd);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxv
    public final zzxv zzvo() {
        return zztl() ? new zzaab(this) : this;
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ String set(int i, String str) {
        zztn();
        return zzs(this.zzcnd.set(i, str));
    }

    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection
    public final /* bridge */ /* synthetic */ boolean retainAll(Collection collection) {
        return super.retainAll(collection);
    }

    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection
    public final /* bridge */ /* synthetic */ boolean removeAll(Collection collection) {
        return super.removeAll(collection);
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp
    public final /* bridge */ /* synthetic */ boolean remove(Object obj) {
        return super.remove(obj);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ String remove(int i) {
        zztn();
        Object remove = this.zzcnd.remove(i);
        this.modCount++;
        return zzs(remove);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp, com.google.android.gms.internal.firebase_ml.zzxl
    public final /* bridge */ /* synthetic */ boolean zztl() {
        return super.zztl();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ void add(int i, String str) {
        zztn();
        this.zzcnd.add(i, str);
        this.modCount++;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection, java.util.AbstractList
    public final /* bridge */ /* synthetic */ boolean add(String str) {
        return super.add(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxl
    public final /* synthetic */ zzxl zzcv(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.zzcnd);
            return new zzxs(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        Object obj = this.zzcnd.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzvv) {
            zzvv zzvv = (zzvv) obj;
            String zztp = zzvv.zztp();
            if (zzvv.zztq()) {
                this.zzcnd.set(i, zztp);
            }
            return zztp;
        }
        byte[] bArr = (byte[]) obj;
        String zzj = zzxd.zzj(bArr);
        if (zzxd.zzi(bArr)) {
            this.zzcnd.set(i, zzj);
        }
        return zzj;
    }

    static {
        zzxs zzxs = new zzxs();
        zzcnb = zzxs;
        zzxs.zztm();
        zzcnc = zzxs;
    }
}
