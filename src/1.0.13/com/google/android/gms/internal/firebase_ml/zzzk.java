package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzzk extends zzzq {
    private final /* synthetic */ zzzj zzcpt;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private zzzk(zzzj zzzj) {
        super(zzzj, null);
        this.zzcpt = zzzj;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.google.android.gms.internal.firebase_ml.zzzq, java.lang.Iterable
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new zzzl(this.zzcpt, null);
    }

    /* synthetic */ zzzk(zzzj zzzj, zzzi zzzi) {
        this(zzzj);
    }
}
