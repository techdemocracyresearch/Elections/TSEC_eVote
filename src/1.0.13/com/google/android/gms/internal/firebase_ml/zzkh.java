package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzkh extends zzkg<zzkn> {
    protected zzkh(zzke zzke, zzkk zzkk) {
        super(zzke.zzait, "POST", "v1/images:annotate", zzkk, zzkn.class);
    }

    /* Return type fixed from 'com.google.android.gms.internal.firebase_ml.zzkg' to match base method */
    @Override // com.google.android.gms.internal.firebase_ml.zzkg
    public final /* synthetic */ zzkg<zzkn> zzg(String str, Object obj) {
        return (zzkh) zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzkg, com.google.android.gms.internal.firebase_ml.zzgo
    public final /* synthetic */ zzgo zzd(String str, Object obj) {
        return (zzkh) zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzkg, com.google.android.gms.internal.firebase_ml.zzgo, com.google.android.gms.internal.firebase_ml.zzgg
    public final /* synthetic */ zzgg zzc(String str, Object obj) {
        return (zzkh) zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzkg, com.google.android.gms.internal.firebase_ml.zzjf, com.google.android.gms.internal.firebase_ml.zzgo, com.google.android.gms.internal.firebase_ml.zzgg
    public final /* synthetic */ zzjf zzb(String str, Object obj) {
        return (zzkh) super.zzb(str, obj);
    }
}
