package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzgk implements zzgj {
    private final String key;
    private final String zzabm;

    public zzgk() {
        this(null);
    }

    public zzgk(String str) {
        this(str, null);
    }

    private zzgk(String str, String str2) {
        this.key = str;
        this.zzabm = null;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgj
    public void zza(zzgg<?> zzgg) throws IOException {
        String str = this.key;
        if (str != null) {
            zzgg.put("key", str);
        }
    }
}
