package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.Callable;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final /* synthetic */ class zzpw implements Runnable {
    private final Callable zzbiq;
    private final TaskCompletionSource zzbir;

    zzpw(Callable callable, TaskCompletionSource taskCompletionSource) {
        this.zzbiq = callable;
        this.zzbir = taskCompletionSource;
    }

    public final void run() {
        zzpx.zza(this.zzbiq, this.zzbir);
    }
}
