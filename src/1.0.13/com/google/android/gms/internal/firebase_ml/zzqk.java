package com.google.android.gms.internal.firebase_ml;

import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.ComponentFactory;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final /* synthetic */ class zzqk implements ComponentFactory {
    static final ComponentFactory zzbil = new zzqk();

    private zzqk() {
    }

    @Override // com.google.firebase.components.ComponentFactory
    public final Object create(ComponentContainer componentContainer) {
        return zzqg.zzc(componentContainer);
    }
}
