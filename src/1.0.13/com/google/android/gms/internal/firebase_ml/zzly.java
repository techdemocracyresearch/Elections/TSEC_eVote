package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzly extends zzlv {
    private final char zzajv;

    zzly(char c) {
        this.zzajv = c;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzlw
    public final boolean zzb(char c) {
        return c == this.zzajv;
    }

    public final String toString() {
        String str = zzlw.zzc(this.zzajv);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 18);
        sb.append("CharMatcher.is('");
        sb.append(str);
        sb.append("')");
        return sb.toString();
    }
}
