package com.google.android.gms.internal.firebase_ml;

import kotlin.text.Typography;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public enum zzhl {
    PLUS('+', "", ",", false, true),
    HASH('#', "#", ",", false, true),
    DOT('.', ".", ".", false, false),
    FORWARD_SLASH('/', "/", "/", false, false),
    SEMI_COLON(';', ";", ";", true, false),
    QUERY('?', "?", "&", true, false),
    AMP(Character.valueOf(Typography.amp), "&", "&", true, false),
    SIMPLE(null, "", ",", false, false);
    
    private final Character zzadr;
    private final String zzads;
    private final String zzadt;
    private final boolean zzadu;
    private final boolean zzadv;

    private zzhl(Character ch, String str, String str2, boolean z, boolean z2) {
        this.zzadr = ch;
        this.zzads = (String) zzml.checkNotNull(str);
        this.zzadt = (String) zzml.checkNotNull(str2);
        this.zzadu = z;
        this.zzadv = z2;
        if (ch != null) {
            zzhm.zzadx.put(ch, this);
        }
    }

    /* access modifiers changed from: package-private */
    public final String zzgn() {
        return this.zzads;
    }

    /* access modifiers changed from: package-private */
    public final String zzgo() {
        return this.zzadt;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzgp() {
        return this.zzadu;
    }

    /* access modifiers changed from: package-private */
    public final int zzgq() {
        return this.zzadr == null ? 0 : 1;
    }

    /* access modifiers changed from: package-private */
    public final String zzak(String str) {
        if (this.zzadv) {
            return zzjw.zzas(str);
        }
        return zzjw.zzaq(str);
    }

    /* access modifiers changed from: package-private */
    public final boolean zzgr() {
        return this.zzadv;
    }
}
