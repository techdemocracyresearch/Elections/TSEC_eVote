package com.google.android.gms.internal.firebase_ml;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzju implements Iterator<T> {
    private int index = 0;
    private final int length;
    private final /* synthetic */ zzjv zzaih;

    zzju(zzjv zzjv) {
        this.zzaih = zzjv;
        this.length = Array.getLength(zzjv.zzaii);
    }

    public final boolean hasNext() {
        return this.index < this.length;
    }

    @Override // java.util.Iterator
    public final T next() {
        if (hasNext()) {
            Object obj = this.zzaih.zzaii;
            int i = this.index;
            this.index = i + 1;
            return (T) Array.get(obj, i);
        }
        throw new NoSuchElementException();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
