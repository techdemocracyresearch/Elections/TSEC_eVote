package com.google.android.gms.internal.firebase_ml;

import com.brentvatne.react.ReactVideoView;
import java.io.IOException;
import java.util.Collections;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzgf extends zzhg {
    private final transient zzgd zzaav;

    private zzgf(zzhf zzhf, zzgd zzgd) {
        super(zzhf);
        this.zzaav = zzgd;
    }

    public final zzgd zzfj() {
        return this.zzaav;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v1, types: [com.google.android.gms.internal.firebase_ml.zzgd] */
    /* JADX WARN: Type inference failed for: r3v2 */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r2v28, types: [com.google.android.gms.internal.firebase_ml.zzgd, com.google.android.gms.internal.firebase_ml.zzhy] */
    /* JADX WARN: Type inference failed for: r3v18 */
    /* JADX WARN: Type inference failed for: r3v19 */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007c A[SYNTHETIC, Splitter:B:30:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0082 A[Catch:{ IOException -> 0x0080 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x009a A[SYNTHETIC, Splitter:B:47:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x009e A[Catch:{ IOException -> 0x00b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00aa A[Catch:{ IOException -> 0x00b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b0 A[Catch:{ IOException -> 0x00b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00d0  */
    /* JADX WARNING: Unknown variable types count: 2 */
    public static zzgf zza(zzhx zzhx, zzhd zzhd) {
        ?? r3;
        String str;
        IOException e;
        zzib zzib;
        String str2;
        Throwable th;
        String str3;
        IOException e2;
        zzhf zzhf = new zzhf(zzhd.getStatusCode(), zzhd.getStatusMessage(), zzhd.zzga());
        zzml.checkNotNull(zzhx);
        String str4 = null;
        try {
            if (zzhd.zzgg() || !zzgz.zzb("application/json; charset=UTF-8", zzhd.getContentType()) || zzhd.getContent() == null) {
                r3 = 0;
                str4 = zzhd.zzgh();
                StringBuilder zzc = zzhg.zzc(zzhd);
                if (!zzms.zzbc(str4)) {
                    zzc.append(zzjt.zzaig);
                    zzc.append(str4);
                    zzhf.zzai(str4);
                }
                zzhf.zzah(zzc.toString());
                return new zzgf(zzhf, r3);
            }
            try {
                zzib = zzhx.zza(zzhd.getContent());
                try {
                    zzih zzhd2 = zzib.zzhd();
                    if (zzhd2 == null) {
                        zzhd2 = zzib.zzhc();
                    }
                    if (zzhd2 != null) {
                        zzib.zza(Collections.singleton(ReactVideoView.EVENT_PROP_ERROR));
                        if (zzib.zzhd() == zzih.VALUE_STRING) {
                            str = zzib.getText();
                        } else if (zzib.zzhd() == zzih.START_OBJECT) {
                            ?? r2 = (zzgd) zzib.zza(zzgd.class, null);
                            try {
                                str = r2.zzgt();
                                str4 = r2;
                            } catch (IOException e3) {
                                str3 = r2;
                                e2 = e3;
                                try {
                                    zzne.zzb(e2);
                                    if (zzib == null) {
                                        try {
                                            zzhd.ignore();
                                            r3 = str3;
                                        } catch (IOException e4) {
                                            e = e4;
                                            str = null;
                                            str4 = str3;
                                            zzne.zzb(e);
                                            r3 = str4;
                                            str4 = str;
                                            StringBuilder zzc2 = zzhg.zzc(zzhd);
                                            if (!zzms.zzbc(str4)) {
                                            }
                                            zzhf.zzah(zzc2.toString());
                                            return new zzgf(zzhf, r3);
                                        }
                                    } else if (str3 == null) {
                                        zzib.close();
                                        r3 = str3;
                                    } else {
                                        str = null;
                                        str4 = str3;
                                        r3 = str4;
                                        str4 = str;
                                    }
                                    StringBuilder zzc22 = zzhg.zzc(zzhd);
                                    if (!zzms.zzbc(str4)) {
                                    }
                                    zzhf.zzah(zzc22.toString());
                                    return new zzgf(zzhf, r3);
                                } catch (Throwable th2) {
                                    th = th2;
                                    str2 = str3;
                                    if (zzib != null) {
                                        zzhd.ignore();
                                    } else if (str2 == null) {
                                        zzib.close();
                                    }
                                    throw th;
                                }
                            } catch (Throwable th3) {
                                str2 = r2;
                                th = th3;
                                if (zzib != null) {
                                }
                                throw th;
                            }
                        }
                        if (zzib != null) {
                            try {
                                zzhd.ignore();
                            } catch (IOException e5) {
                                e = e5;
                                zzne.zzb(e);
                                r3 = str4;
                                str4 = str;
                                StringBuilder zzc222 = zzhg.zzc(zzhd);
                                if (!zzms.zzbc(str4)) {
                                }
                                zzhf.zzah(zzc222.toString());
                                return new zzgf(zzhf, r3);
                            }
                        } else if (str4 == null) {
                            zzib.close();
                        }
                        r3 = str4;
                        str4 = str;
                        StringBuilder zzc2222 = zzhg.zzc(zzhd);
                        if (!zzms.zzbc(str4)) {
                        }
                        zzhf.zzah(zzc2222.toString());
                        return new zzgf(zzhf, r3);
                    }
                    str = null;
                    if (zzib != null) {
                    }
                } catch (IOException e6) {
                    e2 = e6;
                    str3 = null;
                    zzne.zzb(e2);
                    if (zzib == null) {
                    }
                    StringBuilder zzc22222 = zzhg.zzc(zzhd);
                    if (!zzms.zzbc(str4)) {
                    }
                    zzhf.zzah(zzc22222.toString());
                    return new zzgf(zzhf, r3);
                } catch (Throwable th4) {
                    th = th4;
                    str2 = null;
                    if (zzib != null) {
                    }
                    throw th;
                }
            } catch (IOException e7) {
                e2 = e7;
                zzib = null;
                str3 = null;
                zzne.zzb(e2);
                if (zzib == null) {
                }
                StringBuilder zzc222222 = zzhg.zzc(zzhd);
                if (!zzms.zzbc(str4)) {
                }
                zzhf.zzah(zzc222222.toString());
                return new zzgf(zzhf, r3);
            } catch (Throwable th5) {
                th = th5;
                zzib = null;
                str2 = null;
                if (zzib != null) {
                }
                throw th;
            }
            r3 = str4;
            str4 = str;
            StringBuilder zzc2222222 = zzhg.zzc(zzhd);
            if (!zzms.zzbc(str4)) {
            }
            zzhf.zzah(zzc2222222.toString());
            return new zzgf(zzhf, r3);
        } catch (IOException e8) {
            e = e8;
            str = null;
            zzne.zzb(e);
            r3 = str4;
            str4 = str;
            StringBuilder zzc22222222 = zzhg.zzc(zzhd);
            if (!zzms.zzbc(str4)) {
            }
            zzhf.zzah(zzc22222222.toString());
            return new zzgf(zzhf, r3);
        }
    }
}
