package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzkg<T> extends zzgo<T> {
    @zzjg("$.xgafv")
    private String $Xgafv;
    @zzjg("access_token")
    private String accessToken;
    @zzjg
    private String alt;
    @zzjg
    private String callback;
    @zzjg
    private String fields;
    @zzjg
    private String key;
    @zzjg("oauth_token")
    private String oauthToken;
    @zzjg
    private Boolean prettyPrint;
    @zzjg
    private String quotaUser;
    @zzjg("upload_protocol")
    private String uploadProtocol;
    @zzjg
    private String uploadType;

    public zzkg(zzkc zzkc, String str, String str2, Object obj, Class<T> cls) {
        super(zzkc, str, str2, obj, cls);
    }

    /* renamed from: zzg */
    public zzkg<T> zzb(String str, Object obj) {
        return (zzkg) super.zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgo
    public /* synthetic */ zzgo zzd(String str, Object obj) {
        return (zzkg) zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgo
    public final /* synthetic */ zzgm zzfq() {
        return (zzkc) zzfk();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgo, com.google.android.gms.internal.firebase_ml.zzgg
    public /* synthetic */ zzgg zzc(String str, Object obj) {
        return (zzkg) zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgo, com.google.android.gms.internal.firebase_ml.zzgg
    public final /* synthetic */ zzge zzfk() {
        return (zzkc) super.zzfk();
    }
}
