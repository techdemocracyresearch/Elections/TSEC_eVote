package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzye<K, V> extends LinkedHashMap<K, V> {
    private static final zzye zzcnr;
    private boolean zzchf = true;

    private zzye() {
    }

    private zzye(Map<K, V> map) {
        super(map);
    }

    public static <K, V> zzye<K, V> zzvu() {
        return zzcnr;
    }

    public final void zza(zzye<K, V> zzye) {
        zzvw();
        if (!zzye.isEmpty()) {
            putAll(zzye);
        }
    }

    @Override // java.util.LinkedHashMap, java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Set<Map.Entry<K, V>> entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    public final void clear() {
        zzvw();
        super.clear();
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final V put(K k, V v) {
        zzvw();
        zzxd.checkNotNull(k);
        zzxd.checkNotNull(v);
        return (V) super.put(k, v);
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final void putAll(Map<? extends K, ? extends V> map) {
        zzvw();
        for (Object obj : map.keySet()) {
            zzxd.checkNotNull(obj);
            zzxd.checkNotNull(map.get(obj));
        }
        super.putAll(map);
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final V remove(Object obj) {
        zzvw();
        return (V) super.remove(obj);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c A[RETURN] */
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (this != map) {
                if (size() == map.size()) {
                    Iterator<Map.Entry<K, V>> it = entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Map.Entry<K, V> next = it.next();
                        if (map.containsKey(next.getKey())) {
                            V value = next.getValue();
                            Object obj2 = map.get(next.getKey());
                            if (!(value instanceof byte[]) || !(obj2 instanceof byte[])) {
                                z2 = value.equals(obj2);
                                continue;
                            } else {
                                z2 = Arrays.equals((byte[]) value, (byte[]) obj2);
                                continue;
                            }
                            if (!z2) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                z = false;
                if (!z) {
                    return true;
                }
            }
            z = true;
            if (!z) {
                return false;
            }
        }
        return false;
    }

    private static int zzt(Object obj) {
        if (obj instanceof byte[]) {
            return zzxd.hashCode((byte[]) obj);
        }
        if (!(obj instanceof zzxc)) {
            return obj.hashCode();
        }
        throw new UnsupportedOperationException();
    }

    public final int hashCode() {
        int i = 0;
        for (Map.Entry<K, V> entry : entrySet()) {
            i += zzt(entry.getValue()) ^ zzt(entry.getKey());
        }
        return i;
    }

    public final zzye<K, V> zzvv() {
        return isEmpty() ? new zzye<>() : new zzye<>(this);
    }

    public final void zztm() {
        this.zzchf = false;
    }

    public final boolean isMutable() {
        return this.zzchf;
    }

    private final void zzvw() {
        if (!this.zzchf) {
            throw new UnsupportedOperationException();
        }
    }

    static {
        zzye zzye = new zzye();
        zzcnr = zzye;
        zzye.zzchf = false;
    }
}
