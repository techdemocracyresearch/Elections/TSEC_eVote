package com.google.android.gms.internal.firebase_ml;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.Objects;
import kotlin.text.Typography;
import okio.internal.BufferKt;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsz implements Closeable {
    private static final char[] zzbvf = ")]}'\n".toCharArray();
    private final Reader in;
    private int limit = 0;
    private int pos = 0;
    private boolean zzbvg = false;
    private final char[] zzbvh = new char[1024];
    private int zzbvi = 0;
    private int zzbvj = 0;
    private int zzbvk = 0;
    private long zzbvl;
    private int zzbvm;
    private String zzbvn;
    private int[] zzbvo;
    private int zzbvp;
    private String[] zzbvq;
    private int[] zzbvr;

    public zzsz(Reader reader) {
        int[] iArr = new int[32];
        this.zzbvo = iArr;
        this.zzbvp = 0;
        this.zzbvp = 0 + 1;
        iArr[0] = 6;
        this.zzbvq = new String[32];
        this.zzbvr = new int[32];
        Objects.requireNonNull(reader, "in == null");
        this.in = reader;
    }

    public final void setLenient(boolean z) {
        this.zzbvg = true;
    }

    public final void beginArray() throws IOException {
        int i = this.zzbvk;
        if (i == 0) {
            i = zzrc();
        }
        if (i == 3) {
            zzcb(1);
            this.zzbvr[this.zzbvp - 1] = 0;
            this.zzbvk = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_ARRAY but was " + zzrb() + zzrg());
    }

    public final void endArray() throws IOException {
        int i = this.zzbvk;
        if (i == 0) {
            i = zzrc();
        }
        if (i == 4) {
            int i2 = this.zzbvp - 1;
            this.zzbvp = i2;
            int[] iArr = this.zzbvr;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.zzbvk = 0;
            return;
        }
        throw new IllegalStateException("Expected END_ARRAY but was " + zzrb() + zzrg());
    }

    public final void beginObject() throws IOException {
        int i = this.zzbvk;
        if (i == 0) {
            i = zzrc();
        }
        if (i == 1) {
            zzcb(3);
            this.zzbvk = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_OBJECT but was " + zzrb() + zzrg());
    }

    public final void endObject() throws IOException {
        int i = this.zzbvk;
        if (i == 0) {
            i = zzrc();
        }
        if (i == 2) {
            int i2 = this.zzbvp - 1;
            this.zzbvp = i2;
            this.zzbvq[i2] = null;
            int[] iArr = this.zzbvr;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.zzbvk = 0;
            return;
        }
        throw new IllegalStateException("Expected END_OBJECT but was " + zzrb() + zzrg());
    }

    public final zztb zzrb() throws IOException {
        int i = this.zzbvk;
        if (i == 0) {
            i = zzrc();
        }
        switch (i) {
            case 1:
                return zztb.BEGIN_OBJECT;
            case 2:
                return zztb.END_OBJECT;
            case 3:
                return zztb.BEGIN_ARRAY;
            case 4:
                return zztb.END_ARRAY;
            case 5:
            case 6:
                return zztb.BOOLEAN;
            case 7:
                return zztb.NULL;
            case 8:
            case 9:
            case 10:
            case 11:
                return zztb.STRING;
            case 12:
            case 13:
            case 14:
                return zztb.NAME;
            case 15:
            case 16:
                return zztb.NUMBER;
            case 17:
                return zztb.END_DOCUMENT;
            default:
                throw new AssertionError();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:142:0x01fb, code lost:
        if (zze(r5) == false) goto L_0x01fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x01fe, code lost:
        if (r4 != 2) goto L_0x0222;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0200, code lost:
        if (r11 == false) goto L_0x0222;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0206, code lost:
        if (r12 != Long.MIN_VALUE) goto L_0x020a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0208, code lost:
        if (r16 == false) goto L_0x0222;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x020e, code lost:
        if (r12 != 0) goto L_0x0212;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0210, code lost:
        if (r16 != false) goto L_0x0222;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0212, code lost:
        if (r16 == false) goto L_0x0215;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0215, code lost:
        r12 = -r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0216, code lost:
        r19.zzbvl = r12;
        r19.pos += r9;
        r5 = 15;
        r19.zzbvk = 15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0222, code lost:
        if (r4 == 2) goto L_0x022d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0225, code lost:
        if (r4 == 4) goto L_0x022d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0228, code lost:
        if (r4 != 7) goto L_0x022b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x022d, code lost:
        r19.zzbvm = r9;
        r5 = 16;
        r19.zzbvk = 16;
     */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0173 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0174  */
    private final int zzrc() throws IOException {
        int i;
        int i2;
        String str;
        String str2;
        char c;
        int zzav;
        int[] iArr = this.zzbvo;
        int i3 = this.zzbvp;
        int i4 = iArr[i3 - 1];
        char c2 = 1;
        if (i4 == 1) {
            iArr[i3 - 1] = 2;
        } else if (i4 == 2) {
            int zzav2 = zzav(true);
            if (zzav2 != 44) {
                if (zzav2 == 59) {
                    zzre();
                } else if (zzav2 == 93) {
                    this.zzbvk = 4;
                    return 4;
                } else {
                    throw zzce("Unterminated array");
                }
            }
        } else if (i4 == 3 || i4 == 5) {
            iArr[i3 - 1] = 4;
            if (i4 == 5 && (zzav = zzav(true)) != 44) {
                if (zzav == 59) {
                    zzre();
                } else if (zzav == 125) {
                    this.zzbvk = 2;
                    return 2;
                } else {
                    throw zzce("Unterminated object");
                }
            }
            int zzav3 = zzav(true);
            if (zzav3 == 34) {
                this.zzbvk = 13;
                return 13;
            } else if (zzav3 == 39) {
                zzre();
                this.zzbvk = 12;
                return 12;
            } else if (zzav3 != 125) {
                zzre();
                this.pos--;
                if (zze((char) zzav3)) {
                    this.zzbvk = 14;
                    return 14;
                }
                throw zzce("Expected name");
            } else if (i4 != 5) {
                this.zzbvk = 2;
                return 2;
            } else {
                throw zzce("Expected name");
            }
        } else if (i4 == 4) {
            iArr[i3 - 1] = 5;
            int zzav4 = zzav(true);
            if (zzav4 != 58) {
                if (zzav4 == 61) {
                    zzre();
                    if (this.pos < this.limit || zzcc(1)) {
                        char[] cArr = this.zzbvh;
                        int i5 = this.pos;
                        if (cArr[i5] == '>') {
                            this.pos = i5 + 1;
                        }
                    }
                } else {
                    throw zzce("Expected ':'");
                }
            }
        } else if (i4 == 6) {
            if (this.zzbvg) {
                zzav(true);
                int i6 = this.pos - 1;
                this.pos = i6;
                char[] cArr2 = zzbvf;
                if (i6 + cArr2.length <= this.limit || zzcc(cArr2.length)) {
                    int i7 = 0;
                    while (true) {
                        char[] cArr3 = zzbvf;
                        if (i7 < cArr3.length) {
                            if (this.zzbvh[this.pos + i7] != cArr3[i7]) {
                                break;
                            }
                            i7++;
                        } else {
                            this.pos += cArr3.length;
                            break;
                        }
                    }
                }
            }
            this.zzbvo[this.zzbvp - 1] = 7;
        } else if (i4 == 7) {
            if (zzav(false) == -1) {
                this.zzbvk = 17;
                return 17;
            }
            zzre();
            this.pos--;
        } else if (i4 == 8) {
            throw new IllegalStateException("JsonReader is closed");
        }
        int zzav5 = zzav(true);
        if (zzav5 == 34) {
            this.zzbvk = 9;
            return 9;
        } else if (zzav5 != 39) {
            if (zzav5 == 44 || zzav5 == 59) {
                i = 1;
            } else if (zzav5 == 91) {
                this.zzbvk = 3;
                return 3;
            } else if (zzav5 == 93) {
                i = 1;
                if (i4 == 1) {
                    this.zzbvk = 4;
                    return 4;
                }
            } else if (zzav5 != 123) {
                int i8 = this.pos - 1;
                this.pos = i8;
                char c3 = this.zzbvh[i8];
                if (c3 == 't' || c3 == 'T') {
                    str2 = "true";
                    str = "TRUE";
                    i2 = 5;
                } else if (c3 == 'f' || c3 == 'F') {
                    str2 = "false";
                    str = "FALSE";
                    i2 = 6;
                } else {
                    if (c3 == 'n' || c3 == 'N') {
                        str2 = "null";
                        str = "NULL";
                        i2 = 7;
                    }
                    i2 = 0;
                    if (i2 == 0) {
                        return i2;
                    }
                    char[] cArr4 = this.zzbvh;
                    int i9 = this.pos;
                    int i10 = this.limit;
                    long j = 0;
                    long j2 = 0;
                    char c4 = 0;
                    int i11 = 0;
                    boolean z = true;
                    boolean z2 = false;
                    while (true) {
                        if (i9 + i11 == i10) {
                            if (i11 != cArr4.length) {
                                if (!zzcc(i11 + 1)) {
                                    break;
                                }
                                i9 = this.pos;
                                i10 = this.limit;
                            } else {
                                break;
                            }
                        }
                        char c5 = cArr4[i9 + i11];
                        if (c5 == '+') {
                            if (c4 != 5) {
                                break;
                            }
                        } else if (c5 != 'E' && c5 != 'e') {
                            if (c5 == '-') {
                                if (c4 != 0) {
                                    if (c4 != 5) {
                                        break;
                                    }
                                } else {
                                    c4 = 1;
                                    z2 = true;
                                    i11++;
                                    c2 = 1;
                                }
                            } else if (c5 == '.') {
                                if (c4 != 2) {
                                    break;
                                }
                                c4 = 3;
                                i11++;
                                c2 = 1;
                            } else if (c5 >= '0' && c5 <= '9') {
                                if (c4 == c2 || c4 == 0) {
                                    j2 = (long) (-(c5 - '0'));
                                    c4 = 2;
                                } else if (c4 == 2) {
                                    if (j2 == j) {
                                        break;
                                    }
                                    long j3 = (10 * j2) - ((long) (c5 - '0'));
                                    int i12 = (j2 > BufferKt.OVERFLOW_ZONE ? 1 : (j2 == BufferKt.OVERFLOW_ZONE ? 0 : -1));
                                    z &= i12 > 0 || (i12 == 0 && j3 < j2);
                                    j2 = j3;
                                } else if (c4 == 3) {
                                    c4 = 4;
                                } else if (c4 == 5 || c4 == 6) {
                                    c4 = 7;
                                }
                                j = 0;
                                i11++;
                                c2 = 1;
                            }
                        } else if (c4 != 2 && c4 != 4) {
                            break;
                        } else {
                            c4 = 5;
                            i11++;
                            c2 = 1;
                        }
                        c4 = 6;
                        i11++;
                        c2 = 1;
                    }
                    int i13 = 0;
                    if (i13 != 0) {
                        return i13;
                    }
                    if (zze(this.zzbvh[this.pos])) {
                        zzre();
                        this.zzbvk = 10;
                        return 10;
                    }
                    throw zzce("Expected value");
                }
                int length = str2.length();
                int i14 = 1;
                while (true) {
                    if (i14 < length) {
                        if ((this.pos + i14 >= this.limit && !zzcc(i14 + 1)) || ((c = this.zzbvh[this.pos + i14]) != str2.charAt(i14) && c != str.charAt(i14))) {
                            break;
                        }
                        i14++;
                    } else if ((this.pos + length >= this.limit && !zzcc(length + 1)) || !zze(this.zzbvh[this.pos + length])) {
                        this.pos += length;
                        this.zzbvk = i2;
                    }
                }
                i2 = 0;
                if (i2 == 0) {
                }
            } else {
                this.zzbvk = 1;
                return 1;
            }
            if (i4 == i || i4 == 2) {
                zzre();
                this.pos -= i;
                this.zzbvk = 7;
                return 7;
            }
            throw zzce("Unexpected value");
        } else {
            zzre();
            this.zzbvk = 8;
            return 8;
        }
    }

    private final boolean zze(char c) throws IOException {
        if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
            return false;
        }
        if (c != '#') {
            if (c == ',') {
                return false;
            }
            if (!(c == '/' || c == '=')) {
                if (c == '{' || c == '}' || c == ':') {
                    return false;
                }
                if (c != ';') {
                    switch (c) {
                        case '[':
                        case ']':
                            return false;
                        case '\\':
                            break;
                        default:
                            return true;
                    }
                }
            }
        }
        zzre();
        return false;
    }

    public final String nextName() throws IOException {
        String str;
        int i = this.zzbvk;
        if (i == 0) {
            i = zzrc();
        }
        if (i == 14) {
            str = zzrd();
        } else if (i == 12) {
            str = zzf('\'');
        } else if (i == 13) {
            str = zzf(Typography.quote);
        } else {
            throw new IllegalStateException("Expected a name but was " + zzrb() + zzrg());
        }
        this.zzbvk = 0;
        this.zzbvq[this.zzbvp - 1] = str;
        return str;
    }

    public final String nextString() throws IOException {
        int i = this.zzbvk;
        if (i == 0) {
            i = zzrc();
        }
        String str = null;
        if (i == 10) {
            str = zzrd();
        } else if (i == 8) {
            str = zzf('\'');
        } else if (i == 9) {
            str = zzf(Typography.quote);
        } else if (i == 11) {
            this.zzbvn = null;
        } else if (i == 15) {
            str = Long.toString(this.zzbvl);
        } else if (i == 16) {
            str = new String(this.zzbvh, this.pos, this.zzbvm);
            this.pos += this.zzbvm;
        } else {
            throw new IllegalStateException("Expected a string but was " + zzrb() + zzrg());
        }
        this.zzbvk = 0;
        int[] iArr = this.zzbvr;
        int i2 = this.zzbvp - 1;
        iArr[i2] = iArr[i2] + 1;
        return str;
    }

    public final boolean nextBoolean() throws IOException {
        int i = this.zzbvk;
        if (i == 0) {
            i = zzrc();
        }
        if (i == 5) {
            this.zzbvk = 0;
            int[] iArr = this.zzbvr;
            int i2 = this.zzbvp - 1;
            iArr[i2] = iArr[i2] + 1;
            return true;
        } else if (i == 6) {
            this.zzbvk = 0;
            int[] iArr2 = this.zzbvr;
            int i3 = this.zzbvp - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return false;
        } else {
            throw new IllegalStateException("Expected a boolean but was " + zzrb() + zzrg());
        }
    }

    public final void nextNull() throws IOException {
        int i = this.zzbvk;
        if (i == 0) {
            i = zzrc();
        }
        if (i == 7) {
            this.zzbvk = 0;
            int[] iArr = this.zzbvr;
            int i2 = this.zzbvp - 1;
            iArr[i2] = iArr[i2] + 1;
            return;
        }
        throw new IllegalStateException("Expected null but was " + zzrb() + zzrg());
    }

    private final String zzf(char c) throws IOException {
        char[] cArr = this.zzbvh;
        StringBuilder sb = null;
        while (true) {
            int i = this.pos;
            int i2 = this.limit;
            int i3 = i;
            while (true) {
                if (i3 < i2) {
                    int i4 = i3 + 1;
                    char c2 = cArr[i3];
                    if (c2 == c) {
                        this.pos = i4;
                        int i5 = (i4 - i) - 1;
                        if (sb == null) {
                            return new String(cArr, i, i5);
                        }
                        sb.append(cArr, i, i5);
                        return sb.toString();
                    } else if (c2 == '\\') {
                        this.pos = i4;
                        int i6 = (i4 - i) - 1;
                        if (sb == null) {
                            sb = new StringBuilder(Math.max((i6 + 1) << 1, 16));
                        }
                        sb.append(cArr, i, i6);
                        sb.append(zzrh());
                    } else {
                        if (c2 == '\n') {
                            this.zzbvi++;
                            this.zzbvj = i4;
                        }
                        i3 = i4;
                    }
                } else {
                    if (sb == null) {
                        sb = new StringBuilder(Math.max((i3 - i) << 1, 16));
                    }
                    sb.append(cArr, i, i3 - i);
                    this.pos = i3;
                    if (!zzcc(1)) {
                        throw zzce("Unterminated string");
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004a, code lost:
        zzre();
     */
    private final String zzrd() throws IOException {
        int i;
        String str;
        int i2 = 0;
        StringBuilder sb = null;
        while (true) {
            i = 0;
            while (true) {
                int i3 = this.pos;
                if (i3 + i < this.limit) {
                    char c = this.zzbvh[i3 + i];
                    if (!(c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ')) {
                        if (c != '#') {
                            if (c != ',') {
                                if (!(c == '/' || c == '=')) {
                                    if (!(c == '{' || c == '}' || c == ':')) {
                                        if (c != ';') {
                                            switch (c) {
                                                case '[':
                                                case ']':
                                                    break;
                                                case '\\':
                                                    break;
                                                default:
                                                    i++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (i >= this.zzbvh.length) {
                    if (sb == null) {
                        sb = new StringBuilder(Math.max(i, 16));
                    }
                    sb.append(this.zzbvh, this.pos, i);
                    this.pos += i;
                    if (!zzcc(1)) {
                    }
                } else if (zzcc(i + 1)) {
                }
            }
        }
        i2 = i;
        if (sb == null) {
            str = new String(this.zzbvh, this.pos, i2);
        } else {
            sb.append(this.zzbvh, this.pos, i2);
            str = sb.toString();
        }
        this.pos += i2;
        return str;
    }

    private final void zzg(char c) throws IOException {
        char[] cArr = this.zzbvh;
        while (true) {
            int i = this.pos;
            int i2 = this.limit;
            while (true) {
                if (i < i2) {
                    int i3 = i + 1;
                    char c2 = cArr[i];
                    if (c2 == c) {
                        this.pos = i3;
                        return;
                    } else if (c2 == '\\') {
                        this.pos = i3;
                        zzrh();
                        break;
                    } else {
                        if (c2 == '\n') {
                            this.zzbvi++;
                            this.zzbvj = i3;
                        }
                        i = i3;
                    }
                } else {
                    this.pos = i;
                    if (!zzcc(1)) {
                        throw zzce("Unterminated string");
                    }
                }
            }
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() throws IOException {
        this.zzbvk = 0;
        this.zzbvo[0] = 8;
        this.zzbvp = 1;
        this.in.close();
    }

    public final void skipValue() throws IOException {
        int i;
        int i2 = 0;
        do {
            int i3 = this.zzbvk;
            if (i3 == 0) {
                i3 = zzrc();
            }
            if (i3 == 3) {
                zzcb(1);
            } else if (i3 == 1) {
                zzcb(3);
            } else {
                if (i3 == 4) {
                    this.zzbvp--;
                } else if (i3 == 2) {
                    this.zzbvp--;
                } else if (i3 == 14 || i3 == 10) {
                    while (true) {
                        i = 0;
                        while (true) {
                            int i4 = this.pos;
                            if (i4 + i < this.limit) {
                                char c = this.zzbvh[i4 + i];
                                if (!(c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ')) {
                                    if (c != '#') {
                                        if (c != ',') {
                                            if (!(c == '/' || c == '=')) {
                                                if (!(c == '{' || c == '}' || c == ':')) {
                                                    if (c != ';') {
                                                        switch (c) {
                                                            case '[':
                                                            case ']':
                                                                break;
                                                            case '\\':
                                                                break;
                                                            default:
                                                                i++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                this.pos = i4 + i;
                                if (!zzcc(1)) {
                                }
                            }
                        }
                    }
                    zzre();
                    this.pos += i;
                    this.zzbvk = 0;
                } else if (i3 == 8 || i3 == 12) {
                    zzg('\'');
                    this.zzbvk = 0;
                } else if (i3 == 9 || i3 == 13) {
                    zzg(Typography.quote);
                    this.zzbvk = 0;
                } else {
                    if (i3 == 16) {
                        this.pos += this.zzbvm;
                    }
                    this.zzbvk = 0;
                }
                i2--;
                this.zzbvk = 0;
            }
            i2++;
            this.zzbvk = 0;
        } while (i2 != 0);
        int[] iArr = this.zzbvr;
        int i5 = this.zzbvp;
        int i6 = i5 - 1;
        iArr[i6] = iArr[i6] + 1;
        this.zzbvq[i5 - 1] = "null";
    }

    private final void zzcb(int i) {
        int i2 = this.zzbvp;
        int[] iArr = this.zzbvo;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[(i2 << 1)];
            int[] iArr3 = new int[(i2 << 1)];
            String[] strArr = new String[(i2 << 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            System.arraycopy(this.zzbvr, 0, iArr3, 0, this.zzbvp);
            System.arraycopy(this.zzbvq, 0, strArr, 0, this.zzbvp);
            this.zzbvo = iArr2;
            this.zzbvr = iArr3;
            this.zzbvq = strArr;
        }
        int[] iArr4 = this.zzbvo;
        int i3 = this.zzbvp;
        this.zzbvp = i3 + 1;
        iArr4[i3] = i;
    }

    private final boolean zzcc(int i) throws IOException {
        int i2;
        int i3;
        char[] cArr = this.zzbvh;
        int i4 = this.zzbvj;
        int i5 = this.pos;
        this.zzbvj = i4 - i5;
        int i6 = this.limit;
        if (i6 != i5) {
            int i7 = i6 - i5;
            this.limit = i7;
            System.arraycopy(cArr, i5, cArr, 0, i7);
        } else {
            this.limit = 0;
        }
        this.pos = 0;
        do {
            Reader reader = this.in;
            int i8 = this.limit;
            int read = reader.read(cArr, i8, cArr.length - i8);
            if (read == -1) {
                return false;
            }
            i2 = this.limit + read;
            this.limit = i2;
            if (this.zzbvi == 0 && (i3 = this.zzbvj) == 0 && i2 > 0 && cArr[0] == 65279) {
                this.pos++;
                this.zzbvj = i3 + 1;
                i++;
                continue;
            }
        } while (i2 < i);
        return true;
    }

    private final int zzav(boolean z) throws IOException {
        char[] cArr = this.zzbvh;
        int i = this.pos;
        int i2 = this.limit;
        while (true) {
            boolean z2 = true;
            if (i == i2) {
                this.pos = i;
                if (zzcc(1)) {
                    i = this.pos;
                    i2 = this.limit;
                } else if (!z) {
                    return -1;
                } else {
                    throw new EOFException("End of input" + zzrg());
                }
            }
            int i3 = i + 1;
            char c = cArr[i];
            if (c == '\n') {
                this.zzbvi++;
                this.zzbvj = i3;
            } else if (!(c == ' ' || c == '\r' || c == '\t')) {
                if (c == '/') {
                    this.pos = i3;
                    if (i3 == i2) {
                        this.pos = i3 - 1;
                        boolean zzcc = zzcc(2);
                        this.pos++;
                        if (!zzcc) {
                            return c;
                        }
                    }
                    zzre();
                    int i4 = this.pos;
                    char c2 = cArr[i4];
                    if (c2 == '*') {
                        this.pos = i4 + 1;
                        while (true) {
                            if (this.pos + 2 > this.limit && !zzcc(2)) {
                                z2 = false;
                                break;
                            }
                            char[] cArr2 = this.zzbvh;
                            int i5 = this.pos;
                            if (cArr2[i5] != '\n') {
                                for (int i6 = 0; i6 < 2; i6++) {
                                    if (this.zzbvh[this.pos + i6] == "*/".charAt(i6)) {
                                    }
                                }
                                break;
                            }
                            this.zzbvi++;
                            this.zzbvj = i5 + 1;
                            this.pos++;
                        }
                        if (z2) {
                            i = this.pos + 2;
                            i2 = this.limit;
                        } else {
                            throw zzce("Unterminated comment");
                        }
                    } else if (c2 != '/') {
                        return c;
                    } else {
                        this.pos = i4 + 1;
                        zzrf();
                        i = this.pos;
                        i2 = this.limit;
                    }
                } else if (c == '#') {
                    this.pos = i3;
                    zzre();
                    zzrf();
                    i = this.pos;
                    i2 = this.limit;
                } else {
                    this.pos = i3;
                    return c;
                }
            }
            i = i3;
        }
    }

    private final void zzre() throws IOException {
        if (!this.zzbvg) {
            throw zzce("Use JsonReader.setLenient(true) to accept malformed JSON");
        }
    }

    private final void zzrf() throws IOException {
        char c;
        do {
            if (this.pos < this.limit || zzcc(1)) {
                char[] cArr = this.zzbvh;
                int i = this.pos;
                int i2 = i + 1;
                this.pos = i2;
                c = cArr[i];
                if (c == '\n') {
                    this.zzbvi++;
                    this.zzbvj = i2;
                    return;
                }
            } else {
                return;
            }
        } while (c != '\r');
    }

    public final String toString() {
        return getClass().getSimpleName() + zzrg();
    }

    private final String zzrg() {
        StringBuilder sb = new StringBuilder(" at line ");
        sb.append(this.zzbvi + 1);
        sb.append(" column ");
        sb.append((this.pos - this.zzbvj) + 1);
        sb.append(" path ");
        StringBuilder sb2 = new StringBuilder("$");
        int i = this.zzbvp;
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = this.zzbvo[i2];
            if (i3 == 1 || i3 == 2) {
                sb2.append('[');
                sb2.append(this.zzbvr[i2]);
                sb2.append(']');
            } else if (i3 == 3 || i3 == 4 || i3 == 5) {
                sb2.append('.');
                String[] strArr = this.zzbvq;
                if (strArr[i2] != null) {
                    sb2.append(strArr[i2]);
                }
            }
        }
        sb.append(sb2.toString());
        return sb.toString();
    }

    private final char zzrh() throws IOException {
        int i;
        int i2;
        if (this.pos != this.limit || zzcc(1)) {
            char[] cArr = this.zzbvh;
            int i3 = this.pos;
            int i4 = i3 + 1;
            this.pos = i4;
            char c = cArr[i3];
            if (c == '\n') {
                this.zzbvi++;
                this.zzbvj = i4;
            } else if (!(c == '\"' || c == '\'' || c == '/' || c == '\\')) {
                if (c == 'b') {
                    return '\b';
                }
                if (c == 'f') {
                    return '\f';
                }
                if (c == 'n') {
                    return '\n';
                }
                if (c == 'r') {
                    return '\r';
                }
                if (c == 't') {
                    return '\t';
                }
                if (c != 'u') {
                    throw zzce("Invalid escape sequence");
                } else if (i4 + 4 <= this.limit || zzcc(4)) {
                    char c2 = 0;
                    int i5 = this.pos;
                    int i6 = i5 + 4;
                    while (i5 < i6) {
                        char c3 = this.zzbvh[i5];
                        char c4 = (char) (c2 << 4);
                        if (c3 < '0' || c3 > '9') {
                            if (c3 >= 'a' && c3 <= 'f') {
                                i = c3 - 'a';
                            } else if (c3 < 'A' || c3 > 'F') {
                                throw new NumberFormatException("\\u" + new String(this.zzbvh, this.pos, 4));
                            } else {
                                i = c3 - 'A';
                            }
                            i2 = i + 10;
                        } else {
                            i2 = c3 - '0';
                        }
                        c2 = (char) (c4 + i2);
                        i5++;
                    }
                    this.pos += 4;
                    return c2;
                } else {
                    throw zzce("Unterminated escape sequence");
                }
            }
            return c;
        }
        throw zzce("Unterminated escape sequence");
    }

    private final IOException zzce(String str) throws IOException {
        throw new zztd(str + zzrg());
    }

    static {
        zzta.zzbvs = new zztc();
    }
}
