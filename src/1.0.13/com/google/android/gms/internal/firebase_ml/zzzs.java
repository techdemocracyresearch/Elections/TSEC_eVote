package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzzs {
    static String zzh(zzvv zzvv) {
        zzzv zzzv = new zzzv(zzvv);
        StringBuilder sb = new StringBuilder(zzzv.size());
        for (int i = 0; i < zzzv.size(); i++) {
            byte zzcw = zzzv.zzcw(i);
            if (zzcw == 34) {
                sb.append("\\\"");
            } else if (zzcw == 39) {
                sb.append("\\'");
            } else if (zzcw != 92) {
                switch (zzcw) {
                    case 7:
                        sb.append("\\a");
                        continue;
                    case 8:
                        sb.append("\\b");
                        continue;
                    case 9:
                        sb.append("\\t");
                        continue;
                    case 10:
                        sb.append("\\n");
                        continue;
                    case 11:
                        sb.append("\\v");
                        continue;
                    case 12:
                        sb.append("\\f");
                        continue;
                    case 13:
                        sb.append("\\r");
                        continue;
                    default:
                        if (zzcw < 32 || zzcw > 126) {
                            sb.append('\\');
                            sb.append((char) (((zzcw >>> 6) & 3) + 48));
                            sb.append((char) (((zzcw >>> 3) & 7) + 48));
                            sb.append((char) ((zzcw & 7) + 48));
                            break;
                        } else {
                            sb.append((char) zzcw);
                            continue;
                        }
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }
}
