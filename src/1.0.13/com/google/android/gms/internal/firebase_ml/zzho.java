package com.google.android.gms.internal.firebase_ml;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzho extends zzgq {
    private Object data;

    public zzho(Object obj) {
        super(zzhn.MEDIA_TYPE);
        this.data = zzml.checkNotNull(obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjq
    public final void writeTo(OutputStream outputStream) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, zzfs()));
        boolean z = true;
        for (Map.Entry<String, Object> entry : zzix.zzf(this.data).entrySet()) {
            Object value = entry.getValue();
            if (value != null) {
                String zzaq = zzjw.zzaq(entry.getKey());
                Class<?> cls = value.getClass();
                if ((value instanceof Iterable) || cls.isArray()) {
                    for (Object obj : zzjs.zzi(value)) {
                        z = zza(z, bufferedWriter, zzaq, obj);
                    }
                } else {
                    z = zza(z, bufferedWriter, zzaq, value);
                }
            }
        }
        bufferedWriter.flush();
    }

    private static boolean zza(boolean z, Writer writer, String str, Object obj) throws IOException {
        if (obj != null && !zzix.isNull(obj)) {
            if (z) {
                z = false;
            } else {
                writer.write("&");
            }
            writer.write(str);
            String zzaq = zzjw.zzaq(obj instanceof Enum ? zzjd.zza((Enum) obj).getName() : obj.toString());
            if (zzaq.length() != 0) {
                writer.write("=");
                writer.write(zzaq);
            }
        }
        return z;
    }
}
