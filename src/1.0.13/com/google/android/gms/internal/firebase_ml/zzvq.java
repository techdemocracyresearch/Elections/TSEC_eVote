package com.google.android.gms.internal.firebase_ml;

import java.util.Objects;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzvq {
    public int zzchj;
    public long zzchk;
    public Object zzchl;
    public final zzwo zzit;

    zzvq() {
        this.zzit = zzwo.zzuc();
    }

    zzvq(zzwo zzwo) {
        Objects.requireNonNull(zzwo);
        this.zzit = zzwo;
    }
}
