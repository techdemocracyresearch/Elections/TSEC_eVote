package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzkj extends zzgn {
    public zzkj() {
    }

    /* access modifiers changed from: protected */
    public void zza(zzkg<?> zzkg) throws IOException {
    }

    public zzkj(String str) {
        super(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgn
    public final void zza(zzgo<?> zzgo) throws IOException {
        super.zza(zzgo);
        zza((zzkg) zzgo);
    }
}
