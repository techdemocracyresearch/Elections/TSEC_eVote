package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzmp implements zzmq {
    final /* synthetic */ zzlw zzakh;

    zzmp(zzlw zzlw) {
        this.zzakh = zzlw;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzmq
    public final /* synthetic */ Iterator zza(zzmm zzmm, CharSequence charSequence) {
        return new zzmo(this, zzmm, charSequence);
    }
}
