package com.google.android.gms.internal.firebase_ml;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzhd {
    private final int statusCode;
    private final zzgz zzabo;
    private int zzacn;
    private boolean zzaco;
    private InputStream zzacz;
    private final String zzada;
    private final String zzadb;
    private zzhj zzadc;
    private final zzhc zzadd;
    private boolean zzade;
    private final String zznl;

    zzhd(zzhc zzhc, zzhj zzhj) throws IOException {
        StringBuilder sb;
        this.zzadd = zzhc;
        this.zzacn = zzhc.zzfy();
        this.zzaco = zzhc.zzfz();
        this.zzadc = zzhj;
        this.zzada = zzhj.getContentEncoding();
        int statusCode2 = zzhj.getStatusCode();
        boolean z = false;
        statusCode2 = statusCode2 < 0 ? 0 : statusCode2;
        this.statusCode = statusCode2;
        String reasonPhrase = zzhj.getReasonPhrase();
        this.zznl = reasonPhrase;
        Logger logger = zzhh.zzadf;
        if (this.zzaco && logger.isLoggable(Level.CONFIG)) {
            z = true;
        }
        StringBuilder sb2 = null;
        if (z) {
            sb = new StringBuilder();
            sb.append("-------------- RESPONSE --------------");
            sb.append(zzjt.zzaig);
            String zzgj = zzhj.zzgj();
            if (zzgj != null) {
                sb.append(zzgj);
            } else {
                sb.append(statusCode2);
                if (reasonPhrase != null) {
                    sb.append(' ');
                    sb.append(reasonPhrase);
                }
            }
            sb.append(zzjt.zzaig);
        } else {
            sb = null;
        }
        zzhc.zzgb().zza(zzhj, z ? sb : sb2);
        String contentType = zzhj.getContentType();
        contentType = contentType == null ? zzhc.zzgb().getContentType() : contentType;
        this.zzadb = contentType;
        this.zzabo = zzag(contentType);
        if (z) {
            logger.logp(Level.CONFIG, "com.google.api.client.http.HttpResponse", "<init>", sb.toString());
        }
    }

    private static zzgz zzag(String str) {
        if (str == null) {
            return null;
        }
        try {
            return new zzgz(str);
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }

    public final String getContentType() {
        return this.zzadb;
    }

    public final zzgx zzga() {
        return this.zzadd.zzgb();
    }

    public final boolean zzgg() {
        int i = this.statusCode;
        return i >= 200 && i < 300;
    }

    public final int getStatusCode() {
        return this.statusCode;
    }

    public final String getStatusMessage() {
        return this.zznl;
    }

    public final InputStream getContent() throws IOException {
        if (!this.zzade) {
            zzji content = this.zzadc.getContent();
            if (content != null) {
                try {
                    String str = this.zzada;
                    if (str != null && str.contains("gzip")) {
                        content = new GZIPInputStream(content);
                    }
                    Logger logger = zzhh.zzadf;
                    if (this.zzaco && logger.isLoggable(Level.CONFIG)) {
                        content = new zzji(content, logger, Level.CONFIG, this.zzacn);
                    }
                    this.zzacz = content;
                } catch (EOFException unused) {
                    content.close();
                } catch (Throwable th) {
                    content.close();
                    throw th;
                }
            }
            this.zzade = true;
        }
        return this.zzacz;
    }

    public final void ignore() throws IOException {
        InputStream content = getContent();
        if (content != null) {
            content.close();
        }
    }

    public final void disconnect() throws IOException {
        ignore();
        this.zzadc.disconnect();
    }

    public final <T> T zza(Class<T> cls) throws IOException {
        int i = this.statusCode;
        boolean z = true;
        if (this.zzadd.getRequestMethod().equals("HEAD") || i / 100 == 1 || i == 204 || i == 304) {
            ignore();
            z = false;
        }
        if (!z) {
            return null;
        }
        return (T) this.zzadd.zzgd().zza(getContent(), zzgi(), cls);
    }

    /* JADX INFO: finally extract failed */
    public final String zzgh() throws IOException {
        InputStream content = getContent();
        if (content == null) {
            return "";
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            zzml.checkNotNull(content);
            zzml.checkNotNull(byteArrayOutputStream);
            byte[] bArr = new byte[4096];
            while (true) {
                int read = content.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    content.close();
                    return byteArrayOutputStream.toString(zzgi().name());
                }
            }
        } catch (Throwable th) {
            content.close();
            throw th;
        }
    }

    private final Charset zzgi() {
        zzgz zzgz = this.zzabo;
        if (zzgz == null || zzgz.zzfu() == null) {
            return zziw.ISO_8859_1;
        }
        return this.zzabo.zzfu();
    }
}
