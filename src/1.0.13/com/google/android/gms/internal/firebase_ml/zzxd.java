package com.google.android.gms.internal.firebase_ml;

import com.bumptech.glide.load.Key;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Objects;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzxd {
    private static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    static final Charset UTF_8 = Charset.forName(Key.STRING_CHARSET_NAME);
    public static final byte[] zzcmh;
    private static final ByteBuffer zzcmi;
    private static final zzwh zzcmj;

    public static int zzaf(long j) {
        return (int) (j ^ (j >>> 32));
    }

    public static int zzaz(boolean z) {
        return z ? 1231 : 1237;
    }

    static <T> T checkNotNull(T t) {
        Objects.requireNonNull(t);
        return t;
    }

    static <T> T checkNotNull(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }

    public static boolean zzi(byte[] bArr) {
        return zzaaf.zzi(bArr);
    }

    public static String zzj(byte[] bArr) {
        return new String(bArr, UTF_8);
    }

    public static int hashCode(byte[] bArr) {
        int length = bArr.length;
        int zza = zza(length, bArr, 0, length);
        if (zza == 0) {
            return 1;
        }
        return zza;
    }

    static int zza(int i, byte[] bArr, int i2, int i3) {
        for (int i4 = i2; i4 < i2 + i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    static boolean zzf(zzyk zzyk) {
        if (!(zzyk instanceof zzvn)) {
            return false;
        }
        zzvn zzvn = (zzvn) zzyk;
        return false;
    }

    static Object zzc(Object obj, Object obj2) {
        return ((zzyk) obj).zzut().zza((zzyk) obj2).zzva();
    }

    static {
        byte[] bArr = new byte[0];
        zzcmh = bArr;
        zzcmi = ByteBuffer.wrap(bArr);
        zzcmj = zzwh.zza(bArr, 0, bArr.length, false);
    }
}
