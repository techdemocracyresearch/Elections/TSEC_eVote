package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzzr implements Iterator<Map.Entry<K, V>> {
    private int pos;
    private final /* synthetic */ zzzj zzcpt;
    private Iterator<Map.Entry<K, V>> zzcpu;
    private boolean zzcpy;

    private zzzr(zzzj zzzj) {
        this.zzcpt = zzzj;
        this.pos = -1;
    }

    public final boolean hasNext() {
        if (this.pos + 1 < this.zzcpt.zzcpo.size() || (!this.zzcpt.zzcpp.isEmpty() && zzwv().hasNext())) {
            return true;
        }
        return false;
    }

    public final void remove() {
        if (this.zzcpy) {
            this.zzcpy = false;
            this.zzcpt.zzwt();
            if (this.pos < this.zzcpt.zzcpo.size()) {
                zzzj zzzj = this.zzcpt;
                int i = this.pos;
                this.pos = i - 1;
                Object unused = zzzj.zzed(i);
                return;
            }
            zzwv().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }

    private final Iterator<Map.Entry<K, V>> zzwv() {
        if (this.zzcpu == null) {
            this.zzcpu = this.zzcpt.zzcpp.entrySet().iterator();
        }
        return this.zzcpu;
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        this.zzcpy = true;
        int i = this.pos + 1;
        this.pos = i;
        if (i < this.zzcpt.zzcpo.size()) {
            return (Map.Entry) this.zzcpt.zzcpo.get(this.pos);
        }
        return (Map.Entry) zzwv().next();
    }

    /* synthetic */ zzzr(zzzj zzzj, zzzi zzzi) {
        this(zzzj);
    }
}
