package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzmd {
    private final String className;
    private final zzmg zzajx;
    private zzmg zzajy;
    private boolean zzajz;

    private zzmd(String str) {
        zzmg zzmg = new zzmg();
        this.zzajx = zzmg;
        this.zzajy = zzmg;
        this.zzajz = false;
        this.className = (String) zzml.checkNotNull(str);
    }

    public final zzmd zzh(String str, @NullableDecl Object obj) {
        return zzi(str, obj);
    }

    public final zzmd zza(String str, boolean z) {
        return zzi(str, String.valueOf(z));
    }

    public final zzmd zza(String str, float f) {
        return zzi(str, String.valueOf(f));
    }

    public final zzmd zzb(String str, int i) {
        return zzi(str, String.valueOf(i));
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.className);
        sb.append('{');
        zzmg zzmg = this.zzajx.zzaka;
        String str = "";
        while (zzmg != null) {
            Object obj = zzmg.value;
            sb.append(str);
            if (zzmg.name != null) {
                sb.append(zzmg.name);
                sb.append('=');
            }
            if (obj == null || !obj.getClass().isArray()) {
                sb.append(obj);
            } else {
                String deepToString = Arrays.deepToString(new Object[]{obj});
                sb.append((CharSequence) deepToString, 1, deepToString.length() - 1);
            }
            zzmg = zzmg.zzaka;
            str = ", ";
        }
        sb.append('}');
        return sb.toString();
    }

    private final zzmd zzi(String str, @NullableDecl Object obj) {
        zzmg zzmg = new zzmg();
        this.zzajy.zzaka = zzmg;
        this.zzajy = zzmg;
        zzmg.value = obj;
        zzmg.name = (String) zzml.checkNotNull(str);
        return this;
    }
}
