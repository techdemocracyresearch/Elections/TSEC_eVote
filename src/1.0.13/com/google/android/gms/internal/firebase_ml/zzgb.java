package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzgb implements zzgy, zzhe {
    private final boolean zzaai;

    public zzgb() {
        this(false);
    }

    private zzgb(boolean z) {
        this.zzaai = false;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhe
    public final void zza(zzhc zzhc) {
        zzhc.zza(this);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgy
    public final void zzb(zzhc zzhc) throws IOException {
        String requestMethod = zzhc.getRequestMethod();
        boolean z = true;
        if (requestMethod.equals("POST") || ((!requestMethod.equals("GET") || zzhc.zzfw().zzft().length() <= 2048) && zzhc.zzfv().zzaj(requestMethod))) {
            z = false;
        }
        if (z) {
            String requestMethod2 = zzhc.getRequestMethod();
            zzhc.zzaf("POST");
            zzgx zzgx = (zzgx) zzhc.zzga().zzb("X-HTTP-Method-Override", requestMethod2);
            if (requestMethod2.equals("GET")) {
                zzhc.zza(new zzho((zzgu) zzhc.zzfw().clone()));
                zzhc.zzfw().clear();
            } else if (zzhc.zzfx() == null) {
                zzhc.zza(new zzgp());
            }
        }
    }
}
