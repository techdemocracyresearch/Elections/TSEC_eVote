package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzjc implements Iterator<Map.Entry<String, Object>> {
    private final /* synthetic */ zzja zzahg;
    private int zzahh = -1;
    private zzjd zzahi;
    private Object zzahj;
    private boolean zzahk;
    private boolean zzahl;
    private zzjd zzahm;

    zzjc(zzja zzja) {
        this.zzahg = zzja;
    }

    public final boolean hasNext() {
        if (!this.zzahl) {
            this.zzahl = true;
            this.zzahj = null;
            while (this.zzahj == null) {
                int i = this.zzahh + 1;
                this.zzahh = i;
                if (i >= this.zzahg.zzacg.zzagq.size()) {
                    break;
                }
                zzjd zzao = this.zzahg.zzacg.zzao(this.zzahg.zzacg.zzagq.get(this.zzahh));
                this.zzahi = zzao;
                this.zzahj = zzao.zzh(this.zzahg.object);
            }
        }
        if (this.zzahj != null) {
            return true;
        }
        return false;
    }

    public final void remove() {
        zzml.checkState(this.zzahm != null && !this.zzahk);
        this.zzahk = true;
        this.zzahm.zzb(this.zzahg.object, null);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public final /* synthetic */ Map.Entry<String, Object> next() {
        if (hasNext()) {
            zzjd zzjd = this.zzahi;
            this.zzahm = zzjd;
            Object obj = this.zzahj;
            this.zzahl = false;
            this.zzahk = false;
            this.zzahi = null;
            this.zzahj = null;
            return new zziz(this.zzahg, zzjd, obj);
        }
        throw new NoSuchElementException();
    }
}
