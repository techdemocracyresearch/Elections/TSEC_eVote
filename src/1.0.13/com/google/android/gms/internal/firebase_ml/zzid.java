package com.google.android.gms.internal.firebase_ml;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public @interface zzid {

    @Target({ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface zza {
        String zzhr();

        Class<?> zzhs();
    }

    zza[] zzhq();
}
