package com.google.android.gms.internal.firebase_ml;

import kotlin.text.Typography;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public enum zzabf implements zzxc {
    NNAPI_EXECUTION_PREFERENCE_UNDEFINED(0),
    NNAPI_EXECUTION_PREFERENCE_LOW_POWER(1),
    NNAPI_EXECUTION_PREFERENCE_FAST_SINGLE_ANSWER(2),
    NNAPI_EXECUTION_PREFERENCE_SUSTAINED_SPEED(3);
    
    private static final zzxf<zzabf> zzac = new zzabe();
    private final int value;

    @Override // com.google.android.gms.internal.firebase_ml.zzxc
    public final int zzd() {
        return this.value;
    }

    public static zzabf zzej(int i) {
        if (i == 0) {
            return NNAPI_EXECUTION_PREFERENCE_UNDEFINED;
        }
        if (i == 1) {
            return NNAPI_EXECUTION_PREFERENCE_LOW_POWER;
        }
        if (i == 2) {
            return NNAPI_EXECUTION_PREFERENCE_FAST_SINGLE_ANSWER;
        }
        if (i != 3) {
            return null;
        }
        return NNAPI_EXECUTION_PREFERENCE_SUSTAINED_SPEED;
    }

    public static zzxe zzf() {
        return zzabg.zzan;
    }

    public final String toString() {
        return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
    }

    private zzabf(int i) {
        this.value = i;
    }
}
