package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzyc<K, V> {
    private final V value;
    private final zzyf<K, V> zzcno;
    private final K zzcnp;

    private zzyc(zzaan zzaan, K k, zzaan zzaan2, V v) {
        this.zzcno = new zzyf<>(zzaan, k, zzaan2, v);
        this.zzcnp = k;
        this.value = v;
    }

    public static <K, V> zzyc<K, V> zza(zzaan zzaan, K k, zzaan zzaan2, V v) {
        return new zzyc<>(zzaan, k, zzaan2, v);
    }

    static <K, V> void zza(zzwi zzwi, zzyf<K, V> zzyf, K k, V v) throws IOException {
        zzwr.zza(zzwi, zzyf.zzcns, 1, k);
        zzwr.zza(zzwi, zzyf.zzcnu, 2, v);
    }

    static <K, V> int zza(zzyf<K, V> zzyf, K k, V v) {
        return zzwr.zza(zzyf.zzcns, 1, k) + zzwr.zza(zzyf.zzcnu, 2, v);
    }

    public final int zzc(int i, K k, V v) {
        return zzwi.zzdf(i) + zzwi.zzdm(zza(this.zzcno, k, v));
    }

    /* access modifiers changed from: package-private */
    public final zzyf<K, V> zzvt() {
        return this.zzcno;
    }
}
