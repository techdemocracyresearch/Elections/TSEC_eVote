package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final class zzrw extends zzkj {
    private final /* synthetic */ zzrt zzbro;

    zzrw(zzrt zzrt) {
        this.zzbro = zzrt;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzkj
    public final void zza(zzkg<?> zzkg) throws IOException {
        super.zza(zzkg);
        zzkg.zzfl().put("X-Goog-Spatula", zzrt.zza(this.zzbro));
    }
}
