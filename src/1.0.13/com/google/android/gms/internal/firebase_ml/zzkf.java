package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzge;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzkf extends zzgl {
    public zzkf(zzhh zzhh, zzhx zzhx, zzhe zzhe) {
        super(zzhh, zzhx, "https://vision.googleapis.com/", "", zzhe, false);
        zzkf zzkf = (zzkf) zzl("batch");
    }

    public final zzkc zzih() {
        return new zzkc(this);
    }

    public final zzkf zza(zzkj zzkj) {
        return (zzkf) super.zza(zzkj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgl
    public final /* synthetic */ zzgl zzs(String str) {
        return (zzkf) zzm(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgl
    public final /* synthetic */ zzgl zzb(zzgj zzgj) {
        return (zzkf) zza(zzgj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgl
    public final /* synthetic */ zzgl zzr(String str) {
        return (zzkf) zzk(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgl
    public final /* synthetic */ zzgl zzq(String str) {
        return (zzkf) zzj(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzge.zza, com.google.android.gms.internal.firebase_ml.zzgl
    public final /* synthetic */ zzge.zza zzm(String str) {
        return (zzkf) super.zzm(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzge.zza, com.google.android.gms.internal.firebase_ml.zzgl
    public final /* synthetic */ zzge.zza zza(zzgj zzgj) {
        return (zzkf) super.zza(zzgj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzge.zza
    public final /* synthetic */ zzge.zza zzl(String str) {
        return (zzkf) super.zzl(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzge.zza, com.google.android.gms.internal.firebase_ml.zzgl
    public final /* synthetic */ zzge.zza zzk(String str) {
        return (zzkf) super.zzk(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzge.zza, com.google.android.gms.internal.firebase_ml.zzgl
    public final /* synthetic */ zzge.zza zzj(String str) {
        return (zzkf) super.zzj(str);
    }
}
