package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Objects;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzwf extends zzwc {
    protected final byte[] bytes;

    zzwf(byte[] bArr) {
        Objects.requireNonNull(bArr);
        this.bytes = bArr;
    }

    /* access modifiers changed from: protected */
    public int zztu() {
        return 0;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public byte zzcw(int i) {
        return this.bytes[i];
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public byte zzcx(int i) {
        return this.bytes[i];
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public int size() {
        return this.bytes.length;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final zzvv zzf(int i, int i2) {
        int zzd = zzd(i, i2, size());
        if (zzd == 0) {
            return zzvv.zzchp;
        }
        return new zzvy(this.bytes, zztu() + i, zzd);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public void zzb(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.bytes, i, bArr, i2, i3);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final void zza(zzvs zzvs) throws IOException {
        zzvs.zzb(this.bytes, zztu(), size());
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final String zzb(Charset charset) {
        return new String(this.bytes, zztu(), size(), charset);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final boolean zztq() {
        int zztu = zztu();
        return zzaaf.zzf(this.bytes, zztu, size() + zztu);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final int zzb(int i, int i2, int i3) {
        int zztu = zztu() + i2;
        return zzaaf.zzb(i, this.bytes, zztu, i3 + zztu);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzvv) || size() != ((zzvv) obj).size()) {
            return false;
        }
        if (size() == 0) {
            return true;
        }
        if (!(obj instanceof zzwf)) {
            return obj.equals(this);
        }
        zzwf zzwf = (zzwf) obj;
        int zztt = zztt();
        int zztt2 = zzwf.zztt();
        if (zztt == 0 || zztt2 == 0 || zztt == zztt2) {
            return zza(zzwf, 0, size());
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzwc
    public final boolean zza(zzvv zzvv, int i, int i2) {
        if (i2 <= zzvv.size()) {
            int i3 = i + i2;
            if (i3 > zzvv.size()) {
                int size = zzvv.size();
                StringBuilder sb = new StringBuilder(59);
                sb.append("Ran off end of other: ");
                sb.append(i);
                sb.append(", ");
                sb.append(i2);
                sb.append(", ");
                sb.append(size);
                throw new IllegalArgumentException(sb.toString());
            } else if (!(zzvv instanceof zzwf)) {
                return zzvv.zzf(i, i3).equals(zzf(0, i2));
            } else {
                zzwf zzwf = (zzwf) zzvv;
                byte[] bArr = this.bytes;
                byte[] bArr2 = zzwf.bytes;
                int zztu = zztu() + i2;
                int zztu2 = zztu();
                int zztu3 = zzwf.zztu() + i;
                while (zztu2 < zztu) {
                    if (bArr[zztu2] != bArr2[zztu3]) {
                        return false;
                    }
                    zztu2++;
                    zztu3++;
                }
                return true;
            }
        } else {
            int size2 = size();
            StringBuilder sb2 = new StringBuilder(40);
            sb2.append("Length too large: ");
            sb2.append(i2);
            sb2.append(size2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final int zzc(int i, int i2, int i3) {
        return zzxd.zza(i, this.bytes, zztu() + i2, i3);
    }
}
