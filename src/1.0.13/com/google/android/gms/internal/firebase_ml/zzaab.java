package com.google.android.gms.internal.firebase_ml;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzaab extends AbstractList<String> implements zzxv, RandomAccess {
    private final zzxv zzcqg;

    public zzaab(zzxv zzxv) {
        this.zzcqg = zzxv;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxv
    public final zzxv zzvo() {
        return this;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxv
    public final Object getRaw(int i) {
        return this.zzcqg.getRaw(i);
    }

    public final int size() {
        return this.zzcqg.size();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxv
    public final void zze(zzvv zzvv) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.List, java.util.AbstractList
    public final ListIterator<String> listIterator(int i) {
        return new zzaaa(this, i);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, java.lang.Iterable
    public final Iterator<String> iterator() {
        return new zzaad(this);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxv
    public final List<?> zzvn() {
        return this.zzcqg.zzvn();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ String get(int i) {
        return (String) this.zzcqg.get(i);
    }
}
