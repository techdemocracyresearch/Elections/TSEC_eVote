package com.google.android.gms.internal.firebase_ml;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzja extends AbstractMap<String, Object> {
    final Object object;
    final zziv zzacg;

    zzja(Object obj, boolean z) {
        this.object = obj;
        zziv zza = zziv.zza(obj.getClass(), z);
        this.zzacg = zza;
        zzml.checkArgument(!zza.isEnum());
    }

    public final boolean containsKey(Object obj) {
        return get(obj) != null;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final Object get(Object obj) {
        zzjd zzao;
        if ((obj instanceof String) && (zzao = this.zzacg.zzao((String) obj)) != null) {
            return zzao.zzh(this.object);
        }
        return null;
    }

    /* Return type fixed from 'java.util.Set' to match base method */
    @Override // java.util.AbstractMap, java.util.Map
    public final /* synthetic */ Set<Map.Entry<String, Object>> entrySet() {
        return new zzjb(this);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // java.util.AbstractMap, java.util.Map
    public final /* synthetic */ Object put(String str, Object obj) {
        String str2 = str;
        zzjd zzao = this.zzacg.zzao(str2);
        String valueOf = String.valueOf(str2);
        zzml.checkNotNull(zzao, valueOf.length() != 0 ? "no field of key ".concat(valueOf) : new String("no field of key "));
        Object zzh = zzao.zzh(this.object);
        zzao.zzb(this.object, zzml.checkNotNull(obj));
        return zzh;
    }
}
