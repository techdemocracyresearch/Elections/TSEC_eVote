package com.google.firebase.ml.vision.objects.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.firebase_ml.zzb;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzd extends zzb implements zzc {
    zzd(IBinder iBinder) {
        super(iBinder, "com.google.firebase.ml.vision.objects.internal.IObjectDetectorCreator");
    }

    @Override // com.google.firebase.ml.vision.objects.internal.zzc
    public final IObjectDetector newObjectDetector(IObjectWrapper iObjectWrapper, ObjectDetectorOptionsParcel objectDetectorOptionsParcel) throws RemoteException {
        IObjectDetector iObjectDetector;
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        com.google.android.gms.internal.firebase_ml.zzd.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        com.google.android.gms.internal.firebase_ml.zzd.zza(obtainAndWriteInterfaceToken, objectDetectorOptionsParcel);
        Parcel zza = zza(1, obtainAndWriteInterfaceToken);
        IBinder readStrongBinder = zza.readStrongBinder();
        if (readStrongBinder == null) {
            iObjectDetector = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.firebase.ml.vision.objects.internal.IObjectDetector");
            if (queryLocalInterface instanceof IObjectDetector) {
                iObjectDetector = (IObjectDetector) queryLocalInterface;
            } else {
                iObjectDetector = new zza(readStrongBinder);
            }
        }
        zza.recycle();
        return iObjectDetector;
    }
}
