package com.google.firebase.ml.vision.objects;

import com.google.android.gms.common.internal.Objects;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionObjectDetectorOptions {
    public static final int SINGLE_IMAGE_MODE = 2;
    public static final int STREAM_MODE = 1;
    private final int zzbue;
    private final boolean zzbuf;
    private final boolean zzbug;

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface DetectorMode {
    }

    private FirebaseVisionObjectDetectorOptions(int i, boolean z, boolean z2) {
        this.zzbue = i;
        this.zzbuf = z;
        this.zzbug = z2;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private int zzbue = 1;
        private boolean zzbuf = false;
        private boolean zzbug = false;

        public Builder setDetectorMode(int i) {
            this.zzbue = i;
            return this;
        }

        public Builder enableMultipleObjects() {
            this.zzbuf = true;
            return this;
        }

        public Builder enableClassification() {
            this.zzbug = true;
            return this;
        }

        public FirebaseVisionObjectDetectorOptions build() {
            return new FirebaseVisionObjectDetectorOptions(this.zzbue, this.zzbuf, this.zzbug);
        }
    }

    public final int zzqw() {
        return this.zzbue;
    }

    public final boolean zzqx() {
        return this.zzbug;
    }

    public final boolean zzqy() {
        return this.zzbuf;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseVisionObjectDetectorOptions)) {
            return false;
        }
        FirebaseVisionObjectDetectorOptions firebaseVisionObjectDetectorOptions = (FirebaseVisionObjectDetectorOptions) obj;
        return firebaseVisionObjectDetectorOptions.zzbue == this.zzbue && firebaseVisionObjectDetectorOptions.zzbug == this.zzbug && firebaseVisionObjectDetectorOptions.zzbuf == this.zzbuf;
    }

    public int hashCode() {
        return Objects.hashCode(Integer.valueOf(this.zzbue), Boolean.valueOf(this.zzbug), Boolean.valueOf(this.zzbuf));
    }
}
