package com.google.firebase.ml.vision.label;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionCloudImageLabelerOptions {
    private final float confidenceThreshold;
    private final boolean zzbra;

    private FirebaseVisionCloudImageLabelerOptions(float f, boolean z) {
        this.confidenceThreshold = f;
        this.zzbra = z;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private float confidenceThreshold = 0.5f;
        private boolean zzbra = false;

        public Builder setConfidenceThreshold(float f) {
            Preconditions.checkArgument(Float.compare(f, 0.0f) >= 0 && Float.compare(f, 1.0f) <= 0, "Confidence Threshold should be in range [0.0f, 1.0f].");
            this.confidenceThreshold = f;
            return this;
        }

        public Builder enforceCertFingerprintMatch() {
            this.zzbra = true;
            return this;
        }

        public FirebaseVisionCloudImageLabelerOptions build() {
            return new FirebaseVisionCloudImageLabelerOptions(this.confidenceThreshold, this.zzbra);
        }
    }

    public float getConfidenceThreshold() {
        return this.confidenceThreshold;
    }

    public boolean isEnforceCertFingerprintMatch() {
        return this.zzbra;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseVisionCloudImageLabelerOptions)) {
            return false;
        }
        FirebaseVisionCloudImageLabelerOptions firebaseVisionCloudImageLabelerOptions = (FirebaseVisionCloudImageLabelerOptions) obj;
        return Float.compare(this.confidenceThreshold, firebaseVisionCloudImageLabelerOptions.confidenceThreshold) == 0 && this.zzbra == firebaseVisionCloudImageLabelerOptions.zzbra;
    }

    public int hashCode() {
        return Objects.hashCode(Float.valueOf(this.confidenceThreshold), Boolean.valueOf(this.zzbra));
    }
}
