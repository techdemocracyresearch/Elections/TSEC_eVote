package com.google.firebase.ml.vision.label;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLLocalModel;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLRemoteModel;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionOnDeviceAutoMLImageLabelerOptions {
    private final float confidenceThreshold;
    @Nullable
    private final FirebaseAutoMLLocalModel zzboy;
    @Nullable
    private final FirebaseAutoMLRemoteModel zzbpo;

    public final float getConfidenceThreshold() {
        return this.confidenceThreshold;
    }

    public final FirebaseAutoMLLocalModel zzqt() {
        return this.zzboy;
    }

    public final FirebaseAutoMLRemoteModel zzqu() {
        return this.zzbpo;
    }

    private FirebaseVisionOnDeviceAutoMLImageLabelerOptions(float f, @Nullable FirebaseAutoMLLocalModel firebaseAutoMLLocalModel, @Nullable FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel) {
        this.confidenceThreshold = f;
        this.zzboy = firebaseAutoMLLocalModel;
        this.zzbpo = firebaseAutoMLRemoteModel;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private float confidenceThreshold = 0.5f;
        @Nullable
        private FirebaseAutoMLLocalModel zzboy;
        @Nullable
        private FirebaseAutoMLRemoteModel zzbpo;

        public Builder(FirebaseAutoMLLocalModel firebaseAutoMLLocalModel) {
            Preconditions.checkNotNull(firebaseAutoMLLocalModel);
            this.zzboy = firebaseAutoMLLocalModel;
        }

        public Builder(FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel) {
            Preconditions.checkNotNull(firebaseAutoMLRemoteModel);
            this.zzbpo = firebaseAutoMLRemoteModel;
        }

        @Nonnull
        public Builder setConfidenceThreshold(float f) {
            Preconditions.checkArgument(Float.compare(f, 0.0f) >= 0 && Float.compare(f, 1.0f) <= 0, "Confidence Threshold should be in range [0.0f, 1.0f].");
            this.confidenceThreshold = f;
            return this;
        }

        @Nonnull
        public FirebaseVisionOnDeviceAutoMLImageLabelerOptions build() {
            Preconditions.checkArgument((this.zzboy == null && this.zzbpo == null) ? false : true, "Either a local model or remote model must be set.");
            return new FirebaseVisionOnDeviceAutoMLImageLabelerOptions(this.confidenceThreshold, this.zzboy, this.zzbpo);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseVisionOnDeviceAutoMLImageLabelerOptions)) {
            return false;
        }
        FirebaseVisionOnDeviceAutoMLImageLabelerOptions firebaseVisionOnDeviceAutoMLImageLabelerOptions = (FirebaseVisionOnDeviceAutoMLImageLabelerOptions) obj;
        return Float.compare(this.confidenceThreshold, firebaseVisionOnDeviceAutoMLImageLabelerOptions.confidenceThreshold) == 0 && Objects.equal(this.zzboy, firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzboy) && Objects.equal(this.zzbpo, firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzbpo);
    }

    public int hashCode() {
        return Objects.hashCode(Float.valueOf(this.confidenceThreshold), this.zzboy, this.zzbpo);
    }
}
