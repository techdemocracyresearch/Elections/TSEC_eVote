package com.google.firebase.ml.vision.face;

import android.graphics.PointF;
import android.graphics.Rect;
import android.util.SparseArray;
import com.google.android.gms.internal.firebase_ml.zzmb;
import com.google.android.gms.internal.firebase_ml.zzmd;
import com.google.android.gms.vision.face.Contour;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionFace {
    public static final int INVALID_ID = -1;
    public static final float UNCOMPUTED_PROBABILITY = -1.0f;
    private final Rect zzbrq;
    private int zzbsp;
    private float zzbsq;
    private float zzbsr;
    private float zzbss;
    private final float zzbst;
    private final float zzbsu;
    private final SparseArray<FirebaseVisionFaceLandmark> zzbsv = new SparseArray<>();
    private final SparseArray<FirebaseVisionFaceContour> zzbsw = new SparseArray<>();

    private static boolean zzbz(int i) {
        return i == 0 || i == 1 || i == 7 || i == 3 || i == 9 || i == 4 || i == 10 || i == 5 || i == 11 || i == 6;
    }

    public Rect getBoundingBox() {
        return this.zzbrq;
    }

    public int getTrackingId() {
        return this.zzbsp;
    }

    public FirebaseVisionFace(Face face) {
        int i;
        PointF position = face.getPosition();
        this.zzbrq = new Rect((int) position.x, (int) position.y, (int) (position.x + face.getWidth()), (int) (position.y + face.getHeight()));
        this.zzbsp = face.getId();
        for (Landmark landmark : face.getLandmarks()) {
            if (zzbz(landmark.getType()) && landmark.getPosition() != null) {
                this.zzbsv.put(landmark.getType(), new FirebaseVisionFaceLandmark(landmark.getType(), new FirebaseVisionPoint(Float.valueOf(landmark.getPosition().x), Float.valueOf(landmark.getPosition().y), null)));
            }
        }
        ArrayList arrayList = new ArrayList();
        Iterator<Contour> it = face.getContours().iterator();
        while (true) {
            boolean z = true;
            if (it.hasNext()) {
                Contour next = it.next();
                switch (next.getType()) {
                    case 1:
                        i = 2;
                        break;
                    case 2:
                        i = 3;
                        break;
                    case 3:
                        i = 4;
                        break;
                    case 4:
                        i = 5;
                        break;
                    case 5:
                        i = 6;
                        break;
                    case 6:
                        i = 7;
                        break;
                    case 7:
                        i = 8;
                        break;
                    case 8:
                        i = 9;
                        break;
                    case 9:
                        i = 10;
                        break;
                    case 10:
                        i = 11;
                        break;
                    case 11:
                        i = 12;
                        break;
                    case 12:
                        i = 13;
                        break;
                    case 13:
                        i = 14;
                        break;
                    default:
                        i = -1;
                        break;
                }
                if ((i > 14 || i <= 0) ? false : z) {
                    PointF[] positions = next.getPositions();
                    ArrayList arrayList2 = new ArrayList();
                    if (positions != null) {
                        for (PointF pointF : positions) {
                            arrayList2.add(new FirebaseVisionPoint(Float.valueOf(pointF.x), Float.valueOf(pointF.y), null));
                        }
                        this.zzbsw.put(i, new FirebaseVisionFaceContour(i, arrayList2));
                        arrayList.addAll(arrayList2);
                    }
                }
            } else {
                this.zzbsw.put(1, new FirebaseVisionFaceContour(1, arrayList));
                this.zzbst = face.getEulerY();
                this.zzbsu = face.getEulerZ();
                this.zzbss = face.getIsSmilingProbability();
                this.zzbsr = face.getIsLeftEyeOpenProbability();
                this.zzbsq = face.getIsRightEyeOpenProbability();
                return;
            }
        }
    }

    public float getHeadEulerAngleY() {
        return this.zzbst;
    }

    public float getHeadEulerAngleZ() {
        return this.zzbsu;
    }

    public FirebaseVisionFaceLandmark getLandmark(int i) {
        return this.zzbsv.get(i);
    }

    public FirebaseVisionFaceContour getContour(int i) {
        FirebaseVisionFaceContour firebaseVisionFaceContour = this.zzbsw.get(i);
        if (firebaseVisionFaceContour != null) {
            return firebaseVisionFaceContour;
        }
        return new FirebaseVisionFaceContour(i, new ArrayList());
    }

    public final SparseArray<FirebaseVisionFaceContour> zzqr() {
        return this.zzbsw;
    }

    public final void zza(SparseArray<FirebaseVisionFaceContour> sparseArray) {
        this.zzbsw.clear();
        for (int i = 0; i < sparseArray.size(); i++) {
            this.zzbsw.put(sparseArray.keyAt(i), sparseArray.valueAt(i));
        }
    }

    public final void zzbi(int i) {
        this.zzbsp = -1;
    }

    public float getSmilingProbability() {
        return this.zzbss;
    }

    public float getLeftEyeOpenProbability() {
        return this.zzbsr;
    }

    public float getRightEyeOpenProbability() {
        return this.zzbsq;
    }

    public String toString() {
        zzmd zza = zzmb.zzaz("FirebaseVisionFace").zzh("boundingBox", this.zzbrq).zzb("trackingId", this.zzbsp).zza("rightEyeOpenProbability", this.zzbsq).zza("leftEyeOpenProbability", this.zzbsr).zza("smileProbability", this.zzbss).zza("eulerY", this.zzbst).zza("eulerZ", this.zzbsu);
        zzmd zzaz = zzmb.zzaz("Landmarks");
        for (int i = 0; i <= 11; i++) {
            if (zzbz(i)) {
                StringBuilder sb = new StringBuilder(20);
                sb.append("landmark_");
                sb.append(i);
                zzaz.zzh(sb.toString(), getLandmark(i));
            }
        }
        zza.zzh("landmarks", zzaz.toString());
        zzmd zzaz2 = zzmb.zzaz("Contours");
        for (int i2 = 1; i2 <= 14; i2++) {
            StringBuilder sb2 = new StringBuilder(19);
            sb2.append("Contour_");
            sb2.append(i2);
            zzaz2.zzh(sb2.toString(), getContour(i2));
        }
        zza.zzh("contours", zzaz2.toString());
        return zza.toString();
    }
}
