package com.google.firebase.ml.vision;

import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.ComponentFactory;
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLRemoteModel;
import com.google.firebase.ml.vision.automl.internal.zzb;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zzc implements ComponentFactory {
    static final ComponentFactory zzbil = new zzc();

    private zzc() {
    }

    @Override // com.google.firebase.components.ComponentFactory
    public final Object create(ComponentContainer componentContainer) {
        return new FirebaseModelManager.RemoteModelManagerRegistration(FirebaseAutoMLRemoteModel.class, componentContainer.getProvider(zzb.class));
    }
}
