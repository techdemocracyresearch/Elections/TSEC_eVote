package com.google.firebase.ml.vision.cloud.landmark;

import android.graphics.Rect;
import com.google.android.gms.internal.firebase_ml.zzkv;
import com.google.android.gms.internal.firebase_ml.zzlc;
import com.google.android.gms.internal.firebase_ml.zzms;
import com.google.android.gms.internal.firebase_ml.zzrq;
import com.google.firebase.ml.vision.common.FirebaseVisionLatLng;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionCloudLandmark {
    private final List<FirebaseVisionLatLng> locations;
    private final String mid;
    private final float zzbpv;
    private final String zzbrp;
    private final Rect zzbrq;

    public String getEntityId() {
        return this.mid;
    }

    public String getLandmark() {
        return this.zzbrp;
    }

    public Rect getBoundingBox() {
        return this.zzbrq;
    }

    public float getConfidence() {
        return this.zzbpv;
    }

    public List<FirebaseVisionLatLng> getLocations() {
        return this.locations;
    }

    private FirebaseVisionCloudLandmark(String str, float f, Rect rect, String str2, List<FirebaseVisionLatLng> list) {
        this.zzbrq = rect;
        this.zzbrp = zzms.zzbb(str);
        this.mid = zzms.zzbb(str2);
        this.locations = list;
        if (Float.compare(f, 0.0f) < 0) {
            f = 0.0f;
        } else if (Float.compare(f, 1.0f) > 0) {
            f = 1.0f;
        }
        this.zzbpv = f;
    }

    static FirebaseVisionCloudLandmark zza(zzkv zzkv, float f) {
        ArrayList arrayList;
        if (zzkv == null) {
            return null;
        }
        float zza = zzrq.zza(zzkv.zzir());
        Rect zza2 = zzrq.zza(zzkv.zziq(), f);
        String description = zzkv.getDescription();
        String mid2 = zzkv.getMid();
        List<zzlc> locations2 = zzkv.getLocations();
        if (locations2 == null) {
            arrayList = new ArrayList();
        } else {
            ArrayList arrayList2 = new ArrayList();
            for (zzlc zzlc : locations2) {
                if (!(zzlc.zziu() == null || zzlc.zziu().zzis() == null || zzlc.zziu().zzit() == null)) {
                    arrayList2.add(new FirebaseVisionLatLng(zzlc.zziu().zzis().doubleValue(), zzlc.zziu().zzit().doubleValue()));
                }
            }
            arrayList = arrayList2;
        }
        return new FirebaseVisionCloudLandmark(description, zza, zza2, mid2, arrayList);
    }
}
