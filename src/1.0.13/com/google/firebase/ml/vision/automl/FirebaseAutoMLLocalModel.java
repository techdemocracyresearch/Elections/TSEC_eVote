package com.google.firebase.ml.vision.automl;

import com.google.android.gms.common.internal.Preconditions;
import com.google.firebase.ml.common.modeldownload.FirebaseLocalModel;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseAutoMLLocalModel extends FirebaseLocalModel {
    private FirebaseAutoMLLocalModel(String str, String str2) {
        super(null, str, str2);
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private String zzbmv = null;
        private String zzbmw = null;

        public Builder setFilePath(String str) {
            Preconditions.checkNotEmpty(str, "Model Source file path can not be empty");
            Preconditions.checkArgument(this.zzbmw == null, "A local model source is either from local file or for asset, you can not set both.");
            this.zzbmv = str;
            return this;
        }

        public Builder setAssetFilePath(String str) {
            Preconditions.checkNotEmpty(str, "Model Source file path can not be empty");
            Preconditions.checkArgument(this.zzbmv == null, "A local model source is either from local file or for asset, you can not set both.");
            this.zzbmw = str;
            return this;
        }

        public FirebaseAutoMLLocalModel build() {
            String str = this.zzbmv;
            Preconditions.checkArgument((str != null && this.zzbmw == null) || (str == null && this.zzbmw != null), "Set either filePath or assetFilePath.");
            return new FirebaseAutoMLLocalModel(this.zzbmv, this.zzbmw);
        }
    }
}
