package com.google.firebase.ml.vision.automl.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class OnDeviceAutoMLImageLabelerOptionsParcel extends AbstractSafeParcelable {
    public static final Parcelable.Creator<OnDeviceAutoMLImageLabelerOptionsParcel> CREATOR = new zzm();
    public final float confidenceThreshold;
    public final String zzbpw;
    public final String zzbpx;
    public final String zzbpy;

    public OnDeviceAutoMLImageLabelerOptionsParcel(float f, String str, String str2, String str3) {
        this.confidenceThreshold = f;
        this.zzbpw = str;
        this.zzbpx = str2;
        this.zzbpy = str3;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeFloat(parcel, 1, this.confidenceThreshold);
        SafeParcelWriter.writeString(parcel, 2, this.zzbpw, false);
        SafeParcelWriter.writeString(parcel, 3, this.zzbpx, false);
        SafeParcelWriter.writeString(parcel, 4, this.zzbpy, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
