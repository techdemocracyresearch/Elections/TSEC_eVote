package com.google.firebase.ml.common.internal.modeldownload;

import android.os.SystemClock;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.internal.firebase_ml.zzod;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.android.gms.internal.firebase_ml.zzqu;
import com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzw {
    private static final GmsLogger zzbin = new GmsLogger("ModelDownloadLogger", "");
    private final zzqu zzbjl;
    private final FirebaseRemoteModel zzblz;
    private final zzqg zzbmd;

    public zzw(zzqf zzqf, FirebaseRemoteModel firebaseRemoteModel) {
        this.zzbmd = zzqg.zza(zzqf, 4);
        this.zzblz = firebaseRemoteModel;
        this.zzbjl = zzqu.zzb(zzqf);
    }

    public final void zza(zzoc zzoc, String str, boolean z, zzn zzn) {
        zza(zzoc, str, false, false, zzn, zzns.zzai.zza.UNKNOWN_STATUS, 0);
    }

    private final void zza(zzoc zzoc, String str, boolean z, boolean z2, zzn zzn, zzns.zzai.zza zza, int i) {
        zzns.zzai.zzb zzk = zzns.zzai.zzmj().zzl(zzoc).zzb(zza).zzq((long) i).zzk(zzt.zza(this.zzblz, zzn));
        if (z) {
            long zzf = this.zzbjl.zzf(this.zzblz);
            if (zzf == 0) {
                zzbin.w("ModelDownloadLogger", "Model downloaded without its beginning time recorded.");
            } else {
                long zzg = this.zzbjl.zzg(this.zzblz);
                if (zzg == 0) {
                    zzg = SystemClock.elapsedRealtime();
                    this.zzbjl.zza(this.zzblz, zzg);
                }
                zzk.zzo(zzg - zzf);
            }
        }
        if (z2) {
            long zzf2 = this.zzbjl.zzf(this.zzblz);
            if (zzf2 == 0) {
                zzbin.w("ModelDownloadLogger", "Model downloaded without its beginning time recorded.");
            } else {
                zzk.zzp(SystemClock.elapsedRealtime() - zzf2);
            }
        }
        this.zzbmd.zza(zzns.zzad.zzma().zza(zzns.zzbc.zzny().zzbt(str)).zza(zzk), zzod.MODEL_DOWNLOAD);
    }

    public final void zza(zzoc zzoc, boolean z, zzn zzn, zzns.zzai.zza zza) {
        zza(zzoc, "NA", z, false, zzn, zza, 0);
    }

    public final void zza(zzoc zzoc, zzn zzn, zzns.zzai.zza zza) {
        zza(zzoc, "NA", false, true, zzn, zza, 0);
    }

    public final void zza(boolean z, zzn zzn, int i) {
        zza(zzoc.DOWNLOAD_FAILED, "NA", false, false, zzn, zzns.zzai.zza.FAILED, i);
    }

    /* access modifiers changed from: package-private */
    public final void zzo(zzoc zzoc) {
        zza(zzoc, 0);
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzoc zzoc, int i) {
        zza(zzoc, "NA", false, false, zzn.UNKNOWN, zzns.zzai.zza.MODEL_INFO_RETRIEVAL_FAILED, i);
    }
}
