package com.google.firebase.ml.common.internal.modeldownload;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final /* synthetic */ class zzq {
    static final /* synthetic */ int[] zzbkv;

    /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
    static {
        int[] iArr = new int[zzn.values().length];
        zzbkv = iArr;
        iArr[zzn.TRANSLATE.ordinal()] = 1;
        zzbkv[zzn.CUSTOM.ordinal()] = 2;
        zzbkv[zzn.AUTOML.ordinal()] = 3;
        try {
            zzbkv[zzn.BASE.ordinal()] = 4;
        } catch (NoSuchFieldError unused) {
        }
    }
}
