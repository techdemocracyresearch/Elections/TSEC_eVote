package com.google.firebase.ml.common.internal.modeldownload;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.ml.common.FirebaseMLException;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzx extends BroadcastReceiver {
    private final long zzbme;
    private final TaskCompletionSource<Void> zzbmf;
    private final /* synthetic */ zzv zzbmg;

    private zzx(zzv zzv, long j, TaskCompletionSource<Void> taskCompletionSource) {
        this.zzbmg = zzv;
        this.zzbme = j;
        this.zzbmf = taskCompletionSource;
    }

    public final void onReceive(Context context, Intent intent) {
        long longExtra = intent.getLongExtra("extra_download_id", -1);
        if (longExtra == this.zzbme) {
            Integer zzpc = this.zzbmg.zzpc();
            synchronized (this.zzbmg) {
                try {
                    this.zzbmg.zzbkb.getApplicationContext().unregisterReceiver(this);
                } catch (IllegalArgumentException e) {
                    zzv.zzbin.w("ModelDownloadManager", "Exception thrown while trying to unregister the broadcast receiver for the download", e);
                }
                this.zzbmg.zzblw.remove(this.zzbme);
                this.zzbmg.zzblx.remove(this.zzbme);
            }
            if (zzpc != null) {
                if (zzpc.intValue() == 16) {
                    this.zzbmg.zzbma.zza(false, this.zzbmg.zzpi(), this.zzbmg.zza(Long.valueOf(longExtra)));
                    this.zzbmf.setException(this.zzbmg.zzb(Long.valueOf(longExtra)));
                    return;
                } else if (zzpc.intValue() == 8) {
                    this.zzbmg.zzbma.zza(zzoc.NO_ERROR, this.zzbmg.zzpi(), zzns.zzai.zza.SUCCEEDED);
                    this.zzbmf.setResult(null);
                    return;
                }
            }
            this.zzbmg.zzbma.zza(false, this.zzbmg.zzpi(), 0);
            this.zzbmf.setException(new FirebaseMLException("Model downloading failed", 13));
        }
    }
}
