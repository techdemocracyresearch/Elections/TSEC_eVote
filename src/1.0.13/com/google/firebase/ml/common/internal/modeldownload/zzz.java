package com.google.firebase.ml.common.internal.modeldownload;

import android.os.ParcelFileDescriptor;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.internal.firebase_ml.zzne;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.internal.firebase_ml.zzpt;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqu;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzz {
    private static final GmsLogger zzbin = new GmsLogger("RemoteModelFileManager", "");
    private final zzqu zzbjl;
    private final zzqf zzbkb;
    private final String zzbkn;
    private final zzi zzbko;
    private final FirebaseRemoteModel zzblz;
    private final zzn zzbmh;
    private final zzah zzbmi;
    private final zzk zzbmj;

    public zzz(zzqf zzqf, FirebaseRemoteModel firebaseRemoteModel, zzp zzp, zzn zzn, zzi zzi) {
        String str;
        this.zzbkb = zzqf;
        this.zzblz = firebaseRemoteModel;
        if (zzn == zzn.TRANSLATE) {
            str = firebaseRemoteModel.getModelNameForBackend();
        } else {
            str = firebaseRemoteModel.getUniqueModelNameForPersist();
        }
        this.zzbkn = str;
        this.zzbmh = zzn;
        this.zzbmi = new zzah(zzp);
        this.zzbjl = zzqu.zzb(zzqf);
        this.zzbko = zzi;
        int i = zzac.zzbkv[zzn.ordinal()];
        if (i == 1) {
            this.zzbmj = new zza(zzqf, str);
        } else if (i == 2) {
            this.zzbmj = new zzae(zzqf, str);
        } else if (i == 3 || i == 4) {
            this.zzbmj = new zzab(zzqf, str);
        } else {
            throw new IllegalArgumentException("Unexpected model type");
        }
    }

    public final synchronized File zza(ParcelFileDescriptor parcelFileDescriptor, String str, zzw zzw) throws FirebaseMLException {
        File file;
        FirebaseMLException firebaseMLException;
        zzn zzbw = this.zzbjl.zzbw(str);
        zzn zzn = this.zzbmh;
        boolean z = false;
        if (zzn == zzbw) {
            file = new File(this.zzbko.zzd(this.zzbkn, zzn), "to_be_validated_model.tmp");
            try {
                ParcelFileDescriptor.AutoCloseInputStream autoCloseInputStream = new ParcelFileDescriptor.AutoCloseInputStream(parcelFileDescriptor);
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    try {
                        byte[] bArr = new byte[4096];
                        while (true) {
                            int read = autoCloseInputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        }
                        fileOutputStream.getFD().sync();
                        fileOutputStream.close();
                        autoCloseInputStream.close();
                        boolean zza = zzah.zza(file, str);
                        if (zza) {
                            zzs zzb = this.zzbmi.zzb(file, zzw);
                            z = zzb.isValid();
                            if (zzb.zzoy().equals(zzr.TFLITE_VERSION_INCOMPATIBLE)) {
                                String zzb2 = zzpt.zzb(this.zzbkb.getApplicationContext());
                                this.zzbjl.zza(this.zzblz, str, zzb2);
                                GmsLogger gmsLogger = zzbin;
                                String valueOf = String.valueOf(str);
                                gmsLogger.d("RemoteModelFileManager", valueOf.length() != 0 ? "Model is not compatible. Model hash: ".concat(valueOf) : new String("Model is not compatible. Model hash: "));
                                String valueOf2 = String.valueOf(zzb2);
                                gmsLogger.d("RemoteModelFileManager", valueOf2.length() != 0 ? "The current app version is: ".concat(valueOf2) : new String("The current app version is: "));
                            }
                        }
                        if (!zza || !z) {
                            if (!zza) {
                                GmsLogger gmsLogger2 = zzbin;
                                String valueOf3 = String.valueOf(str);
                                gmsLogger2.d("RemoteModelFileManager", valueOf3.length() != 0 ? "Hash does not match with expected: ".concat(valueOf3) : new String("Hash does not match with expected: "));
                                zzw.zza(zzoc.MODEL_HASH_MISMATCH, true, this.zzbmh, zzns.zzai.zza.SUCCEEDED);
                                firebaseMLException = new FirebaseMLException("Hash does not match with expected", 102);
                            } else {
                                firebaseMLException = new FirebaseMLException("Model is not compatible with TFLite run time", 100);
                            }
                            if (!file.delete()) {
                                GmsLogger gmsLogger3 = zzbin;
                                String valueOf4 = String.valueOf(file.getAbsolutePath());
                                gmsLogger3.d("RemoteModelFileManager", valueOf4.length() != 0 ? "Failed to delete the temp file: ".concat(valueOf4) : new String("Failed to delete the temp file: "));
                            }
                            throw firebaseMLException;
                        }
                    } catch (Throwable th) {
                        zzne.zza(th, th);
                    }
                } catch (Throwable th2) {
                    zzne.zza(th, th2);
                }
            } catch (IOException e) {
                GmsLogger gmsLogger4 = zzbin;
                String valueOf5 = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf5).length() + 56);
                sb.append("Failed to copy downloaded model file to private folder: ");
                sb.append(valueOf5);
                gmsLogger4.e("RemoteModelFileManager", sb.toString());
                return null;
            }
        } else {
            zzw.zza(zzoc.MODEL_TYPE_MISUSE, false, zzbw, zzns.zzai.zza.DOWNLOADED);
            String name = zzbw.name();
            String name2 = this.zzbmh.name();
            StringBuilder sb2 = new StringBuilder(String.valueOf(name).length() + 93 + String.valueOf(name2).length());
            sb2.append("You are trying to use a ");
            sb2.append(name);
            sb2.append(" model as a ");
            sb2.append(name2);
            sb2.append(" model. Please make sure you specified the correct model.");
            throw new FirebaseMLException(sb2.toString(), 3);
        }
        return this.zzbmj.zza(file);
        throw th;
        throw th;
    }

    public final synchronized boolean zzd(File file) throws FirebaseMLException {
        File zzc = this.zzbko.zzc(this.zzbkn, this.zzbmh);
        if (!zzc.exists()) {
            return false;
        }
        File[] listFiles = zzc.listFiles();
        boolean z = true;
        for (File file2 : listFiles) {
            if (!file2.equals(file) && !this.zzbko.zzc(file2)) {
                z = false;
            }
        }
        return z;
    }

    public final synchronized void zze(File file) {
        File zzb = this.zzbko.zzb(this.zzbkn, this.zzbmh, false);
        if (zzb.exists()) {
            for (File file2 : zzb.listFiles()) {
                if (file2.equals(file)) {
                    this.zzbko.zzc(file);
                    return;
                }
            }
        }
    }

    public final synchronized File zzf(File file) throws FirebaseMLException {
        File file2 = new File(String.valueOf(this.zzbko.zzc(this.zzbkn, this.zzbmh).getAbsolutePath()).concat("/0"));
        if (file2.exists()) {
            return file;
        }
        if (file.renameTo(file2)) {
            return file2;
        }
        return file;
    }

    public final synchronized String zzpk() throws FirebaseMLException {
        File zzc = this.zzbko.zzc(this.zzbkn, this.zzbmh);
        int zzb = zzi.zzb(zzc);
        if (zzb < 0) {
            return null;
        }
        String absolutePath = zzc.getAbsolutePath();
        StringBuilder sb = new StringBuilder(String.valueOf(absolutePath).length() + 12);
        sb.append(absolutePath);
        sb.append("/");
        sb.append(zzb);
        return sb.toString();
    }
}
