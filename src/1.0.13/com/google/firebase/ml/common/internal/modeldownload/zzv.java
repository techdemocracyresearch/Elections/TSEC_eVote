package com.google.firebase.ml.common.internal.modeldownload;

import android.app.DownloadManager;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.LongSparseArray;
import androidx.core.app.NotificationCompat;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzne;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.internal.firebase_ml.zzpt;
import com.google.android.gms.internal.firebase_ml.zzpx;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqu;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions;
import com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzv {
    private static final GmsLogger zzbin = new GmsLogger("ModelDownloadManager", "");
    private static final Map<String, zzv> zzblv = new HashMap();
    private final zzqu zzbjl;
    private final zzqf zzbkb;
    private final LongSparseArray<zzx> zzblw = new LongSparseArray<>();
    private final LongSparseArray<TaskCompletionSource<Void>> zzblx = new LongSparseArray<>();
    private final DownloadManager zzbly;
    private final FirebaseRemoteModel zzblz;
    private final zzw zzbma;
    private final zzg zzbmb;
    private FirebaseModelDownloadConditions zzbmc;

    public static synchronized zzv zza(zzqf zzqf, FirebaseRemoteModel firebaseRemoteModel, zzg zzg, zzw zzw) {
        zzv zzv;
        synchronized (zzv.class) {
            String uniqueModelNameForPersist = firebaseRemoteModel.getUniqueModelNameForPersist();
            Map<String, zzv> map = zzblv;
            if (!map.containsKey(uniqueModelNameForPersist)) {
                map.put(uniqueModelNameForPersist, new zzv(zzqf, firebaseRemoteModel, zzg, zzw));
            }
            zzv = map.get(uniqueModelNameForPersist);
        }
        return zzv;
    }

    public final void zza(FirebaseModelDownloadConditions firebaseModelDownloadConditions) {
        Preconditions.checkNotNull(firebaseModelDownloadConditions, "DownloadConditions can not be null");
        this.zzbmc = firebaseModelDownloadConditions;
    }

    private zzv(zzqf zzqf, FirebaseRemoteModel firebaseRemoteModel, zzg zzg, zzw zzw) {
        this.zzbkb = zzqf;
        this.zzblz = firebaseRemoteModel;
        DownloadManager downloadManager = (DownloadManager) zzqf.getApplicationContext().getSystemService("download");
        this.zzbly = downloadManager;
        if (downloadManager == null) {
            zzbin.d("ModelDownloadManager", "Download manager service is not available in the service.");
        }
        this.zzbmb = zzg;
        this.zzbma = zzw;
        this.zzbjl = zzqu.zzb(zzqf);
    }

    /* access modifiers changed from: package-private */
    public final synchronized Long zzoz() {
        return this.zzbjl.zza(this.zzblz);
    }

    /* access modifiers changed from: package-private */
    public final synchronized String zzpa() {
        return this.zzbjl.zzb(this.zzblz);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void zzpb() throws FirebaseMLException {
        Long zzoz = zzoz();
        if (this.zzbly != null) {
            if (zzoz != null) {
                GmsLogger gmsLogger = zzbin;
                String valueOf = String.valueOf(zzoz);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 44);
                sb.append("Cancel or remove existing downloading task: ");
                sb.append(valueOf);
                gmsLogger.d("ModelDownloadManager", sb.toString());
                if (this.zzbly.remove(zzoz.longValue()) > 0 || zzpc() == null) {
                    this.zzbmb.zza(this.zzblz.getUniqueModelNameForPersist(), zzpi());
                    this.zzbjl.zzh(this.zzblz);
                }
            }
        }
    }

    private final synchronized Long zza(DownloadManager.Request request, zzaa zzaa) {
        DownloadManager downloadManager = this.zzbly;
        if (downloadManager == null) {
            return null;
        }
        long enqueue = downloadManager.enqueue(request);
        GmsLogger gmsLogger = zzbin;
        StringBuilder sb = new StringBuilder(53);
        sb.append("Schedule a new downloading task: ");
        sb.append(enqueue);
        gmsLogger.d("ModelDownloadManager", sb.toString());
        this.zzbjl.zza(enqueue, zzaa);
        this.zzbma.zza(zzoc.NO_ERROR, false, zzaa.zzpm(), zzns.zzai.zza.SCHEDULED);
        return Long.valueOf(enqueue);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0048  */
    public final synchronized Integer zzpc() {
        Integer num;
        Long zzoz = zzoz();
        DownloadManager downloadManager = this.zzbly;
        Integer num2 = null;
        if (downloadManager != null) {
            if (zzoz != null) {
                Cursor query = downloadManager.query(new DownloadManager.Query().setFilterById(zzoz.longValue()));
                if (query != null) {
                    try {
                        if (query.moveToFirst()) {
                            num = Integer.valueOf(query.getInt(query.getColumnIndex(NotificationCompat.CATEGORY_STATUS)));
                            if (num != null) {
                                if (query != null) {
                                    query.close();
                                }
                                return null;
                            }
                            if (num.intValue() == 2 || num.intValue() == 4 || num.intValue() == 1 || num.intValue() == 8 || num.intValue() == 16) {
                                num2 = num;
                            }
                            if (query != null) {
                                query.close();
                            }
                            return num2;
                        }
                    } catch (Throwable th) {
                        zzne.zza(th, th);
                    }
                }
                num = null;
                if (num != null) {
                }
            }
        }
        return null;
        throw th;
    }

    /* access modifiers changed from: package-private */
    public final synchronized ParcelFileDescriptor zzpd() {
        Long zzoz = zzoz();
        DownloadManager downloadManager = this.zzbly;
        ParcelFileDescriptor parcelFileDescriptor = null;
        if (downloadManager == null || zzoz == null) {
            return null;
        }
        try {
            parcelFileDescriptor = downloadManager.openDownloadedFile(zzoz.longValue());
        } catch (FileNotFoundException unused) {
            zzbin.e("ModelDownloadManager", "Downloaded file is not found");
        }
        return parcelFileDescriptor;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void zze(String str, zzn zzn) throws FirebaseMLException {
        this.zzbjl.zza(this.zzblz, str, zzn);
        zzpb();
    }

    private final synchronized zzaa zzpe() throws FirebaseMLException {
        boolean zzph = zzph();
        boolean z = false;
        if (zzph) {
            this.zzbma.zza(zzoc.NO_ERROR, false, this.zzbjl.zzd(this.zzblz), zzns.zzai.zza.LIVE);
        }
        zzaa zza = zzad.zza(this.zzbkb, this.zzblz, this.zzbma);
        if (zza == null) {
            return null;
        }
        zzqf zzqf = this.zzbkb;
        FirebaseRemoteModel firebaseRemoteModel = this.zzblz;
        String str = zza.zzbmm;
        zzqu zzb = zzqu.zzb(zzqf);
        if (str.equals(zzb.zze(firebaseRemoteModel)) && zzpt.zzb(zzqf.getApplicationContext()).equals(zzb.zzor())) {
            zzbin.e("ModelDownloadManager", "The model is incompatible with TFLite and the app is not upgraded, do not download");
        } else {
            z = true;
        }
        if (!zzph) {
            this.zzbjl.zzi(this.zzblz);
        }
        boolean z2 = !zza.zzbmm.equals(zzqu.zzb(this.zzbkb).zzc(this.zzblz));
        if (z && (!zzph || z2)) {
            return zza;
        }
        if (zzph && (z2 ^ z)) {
            return null;
        }
        String modelName = this.zzblz.getModelName();
        StringBuilder sb = new StringBuilder(String.valueOf(modelName).length() + 46);
        sb.append("The model ");
        sb.append(modelName);
        sb.append(" is incompatible with TFLite runtime");
        throw new FirebaseMLException(sb.toString(), 100);
    }

    private final synchronized Long zza(zzaa zzaa, FirebaseModelDownloadConditions firebaseModelDownloadConditions) throws FirebaseMLException {
        Preconditions.checkNotNull(firebaseModelDownloadConditions, "DownloadConditions can not be null");
        String zzb = this.zzbjl.zzb(this.zzblz);
        Integer zzpc = zzpc();
        if (zzb == null || !zzb.equals(zzaa.zzbmm) || zzpc == null) {
            GmsLogger gmsLogger = zzbin;
            gmsLogger.d("ModelDownloadManager", "Need to download a new model.");
            zzpb();
            DownloadManager.Request request = new DownloadManager.Request(zzaa.zzbml);
            request.setDestinationUri(null);
            if (this.zzbmb.zza(zzaa)) {
                gmsLogger.d("ModelDownloadManager", "Model update is enabled and have a previous downloaded model, use download condition");
                this.zzbma.zza(zzoc.NO_ERROR, false, zzaa.zzpm(), zzns.zzai.zza.UPDATE_AVAILABLE);
            }
            if (Build.VERSION.SDK_INT >= 24) {
                request.setRequiresCharging(firebaseModelDownloadConditions.isChargingRequired());
                request.setRequiresDeviceIdle(firebaseModelDownloadConditions.isDeviceIdleRequired());
            }
            if (firebaseModelDownloadConditions.isWifiRequired()) {
                request.setAllowedNetworkTypes(2);
            }
            return zza(request, zzaa);
        }
        if (!zzb(zzpc())) {
            this.zzbma.zza(zzoc.NO_ERROR, false, zzpi(), zzns.zzai.zza.DOWNLOADING);
        }
        zzbin.d("ModelDownloadManager", "New model is already in downloading, do nothing.");
        return null;
    }

    public final boolean zzpf() throws FirebaseMLException {
        try {
            return zzph() || Objects.equal(zzpc(), 8);
        } catch (FirebaseMLException e) {
            throw new FirebaseMLException("Failed to check if the model is downloaded.", 13, e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0068 A[Catch:{ FirebaseMLException -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0069 A[Catch:{ FirebaseMLException -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0071 A[Catch:{ FirebaseMLException -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007d A[Catch:{ FirebaseMLException -> 0x00bd }] */
    public final Task<Void> zzpg() {
        FirebaseMLException firebaseMLException;
        zzaa zzaa;
        this.zzbma.zza(zzoc.NO_ERROR, false, zzn.UNKNOWN, zzns.zzai.zza.EXPLICITLY_REQUESTED);
        Long l = null;
        try {
            zzaa = zzpe();
            firebaseMLException = null;
        } catch (FirebaseMLException e) {
            firebaseMLException = e;
            zzaa = null;
        }
        try {
            Integer zzpc = zzpc();
            Long zzoz = zzoz();
            if (!zzph()) {
                if (!zzb(zzpc)) {
                    boolean z = true;
                    if (zzpc != null) {
                        if (!(zzpc.intValue() == 4 || zzpc.intValue() == 2)) {
                            if (zzpc.intValue() == 1) {
                            }
                        }
                        if (z || zzoz == null || zzpa() == null) {
                            if (zzaa == null) {
                                l = zza(zzaa, this.zzbmc);
                            }
                            if (l != null) {
                                return Tasks.forException(new FirebaseMLException("Failed to schedule the download task", 13, firebaseMLException));
                            }
                            return zzt(l.longValue());
                        }
                        this.zzbma.zza(zzoc.NO_ERROR, false, zzpi(), zzns.zzai.zza.DOWNLOADING);
                        return zzt(zzoz.longValue());
                    }
                    z = false;
                    if (z) {
                    }
                    if (zzaa == null) {
                    }
                    if (l != null) {
                    }
                }
            }
            if (zzaa != null) {
                Long zza = zza(zzaa, this.zzbmc);
                if (zza != null) {
                    return zzt(zza.longValue());
                }
                zzbin.i("ModelDownloadManager", "Didn't schedule download for the updated model");
            }
            if (zzpc == null || zzpc.intValue() != 16) {
                return Tasks.forResult(null);
            }
            FirebaseMLException zzb = zzb(zzoz);
            zzpb();
            return Tasks.forException(zzb);
        } catch (FirebaseMLException e2) {
            return Tasks.forException(new FirebaseMLException("Failed to ensure the model is downloaded.", 13, e2));
        }
    }

    /* access modifiers changed from: package-private */
    public final int zza(Long l) {
        int columnIndex;
        DownloadManager downloadManager = this.zzbly;
        Cursor query = (downloadManager == null || l == null) ? null : downloadManager.query(new DownloadManager.Query().setFilterById(l.longValue()));
        if (query == null || !query.moveToFirst() || (columnIndex = query.getColumnIndex("reason")) == -1) {
            return 0;
        }
        return query.getInt(columnIndex);
    }

    private final boolean zzph() throws FirebaseMLException {
        return this.zzbmb.zzb(this.zzblz.getUniqueModelNameForPersist(), this.zzbjl.zzd(this.zzblz));
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private final zzn zzpi() {
        String zzb = this.zzbjl.zzb(this.zzblz);
        if (zzb == null) {
            return zzn.UNKNOWN;
        }
        return this.zzbjl.zzbw(zzb);
    }

    private final synchronized zzx zzr(long j) {
        zzx zzx;
        zzx = this.zzblw.get(j);
        if (zzx == null) {
            zzx = new zzx(this, j, zzs(j));
            this.zzblw.put(j, zzx);
        }
        return zzx;
    }

    private final synchronized TaskCompletionSource<Void> zzs(long j) {
        TaskCompletionSource<Void> taskCompletionSource;
        taskCompletionSource = this.zzblx.get(j);
        if (taskCompletionSource == null) {
            taskCompletionSource = new TaskCompletionSource<>();
            this.zzblx.put(j, taskCompletionSource);
        }
        return taskCompletionSource;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private final FirebaseMLException zzb(Long l) {
        Cursor cursor;
        String str;
        DownloadManager downloadManager = this.zzbly;
        if (downloadManager == null || l == null) {
            cursor = null;
        } else {
            cursor = downloadManager.query(new DownloadManager.Query().setFilterById(l.longValue()));
        }
        int i = 13;
        if (cursor == null || !cursor.moveToFirst()) {
            str = "Model downloading failed";
        } else {
            int i2 = cursor.getInt(cursor.getColumnIndex("reason"));
            if (i2 == 1006) {
                i = 101;
                str = "Model downloading failed due to insufficient space on the device.";
            } else {
                StringBuilder sb = new StringBuilder(84);
                sb.append("Model downloading failed due to error code: ");
                sb.append(i2);
                sb.append(" from Android DownloadManager");
                str = sb.toString();
            }
        }
        return new FirebaseMLException(str, i);
    }

    private static boolean zzb(Integer num) {
        if (num != null) {
            return num.intValue() == 8 || num.intValue() == 16;
        }
        return false;
    }

    private final Task<Void> zzt(long j) {
        this.zzbkb.getApplicationContext().registerReceiver(zzr(j), new IntentFilter("android.intent.action.DOWNLOAD_COMPLETE"), null, zzpx.zzof().getHandler());
        return zzs(j).getTask();
    }
}
