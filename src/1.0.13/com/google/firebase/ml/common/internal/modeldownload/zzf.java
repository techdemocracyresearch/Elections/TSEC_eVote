package com.google.firebase.ml.common.internal.modeldownload;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzne;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.modeldownload.FirebaseLocalModel;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzf {
    private MappedByteBuffer zzbks;
    private final Context zzbkt;
    private final FirebaseLocalModel zzbku;

    public zzf(Context context, FirebaseLocalModel firebaseLocalModel) {
        this.zzbkt = context;
        this.zzbku = firebaseLocalModel;
    }

    public MappedByteBuffer load() throws FirebaseMLException {
        Preconditions.checkNotNull(this.zzbkt, "Context can not be null");
        Preconditions.checkNotNull(this.zzbku, "Model source can not be null");
        MappedByteBuffer mappedByteBuffer = this.zzbks;
        if (mappedByteBuffer != null) {
            return mappedByteBuffer;
        }
        if (this.zzbku.getFilePath() != null) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(this.zzbku.getFilePath(), "r");
                try {
                    FileChannel channel = randomAccessFile.getChannel();
                    try {
                        this.zzbks = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
                        if (channel != null) {
                            channel.close();
                        }
                        randomAccessFile.close();
                        return this.zzbks;
                    } catch (Throwable th) {
                        zzne.zza(th, th);
                    }
                } catch (Throwable th2) {
                    zzne.zza(th, th2);
                }
            } catch (IOException e) {
                String valueOf = String.valueOf(this.zzbku.getFilePath());
                throw new FirebaseMLException(valueOf.length() != 0 ? "Can not open the local file: ".concat(valueOf) : new String("Can not open the local file: "), 14, e);
            }
        } else if (this.zzbku.getAssetFilePath() != null) {
            String assetFilePath = this.zzbku.getAssetFilePath();
            try {
                AssetFileDescriptor openFd = this.zzbkt.getAssets().openFd(assetFilePath);
                try {
                    FileInputStream fileInputStream = new FileInputStream(openFd.getFileDescriptor());
                    try {
                        FileChannel channel2 = fileInputStream.getChannel();
                        try {
                            this.zzbks = channel2.map(FileChannel.MapMode.READ_ONLY, openFd.getStartOffset(), openFd.getDeclaredLength());
                            if (channel2 != null) {
                                channel2.close();
                            }
                            fileInputStream.close();
                            if (openFd != null) {
                                openFd.close();
                            }
                            return this.zzbks;
                        } catch (Throwable th3) {
                            zzne.zza(th, th3);
                        }
                    } catch (Throwable th4) {
                        zzne.zza(th, th4);
                    }
                } catch (Throwable th5) {
                    zzne.zza(th, th5);
                }
            } catch (IOException e2) {
                StringBuilder sb = new StringBuilder(String.valueOf(assetFilePath).length() + 186);
                sb.append("Can not load the file from asset: ");
                sb.append(assetFilePath);
                sb.append(". Please double check your asset file name and ensure it's not compressed. See documentation for details how to use aaptOptions to skip file compression");
                throw new FirebaseMLException(sb.toString(), 14, e2);
            }
        } else {
            throw new FirebaseMLException("Can not load the model. Either filePath or assetFilePath must be set for the model.", 14);
        }
        throw th;
        throw th;
        throw th;
        throw th;
        throw th;
    }

    public final FirebaseLocalModel zzot() {
        return this.zzbku;
    }
}
