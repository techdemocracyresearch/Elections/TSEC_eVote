package com.google.firebase.ml.common.internal.modeldownload;

import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.firebase.ml.common.FirebaseMLException;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzj {
    private static final GmsLogger zzbin = new GmsLogger("ModelLoader", "");
    private final zzag zzbla;
    public final zzf zzblb;
    private final zzl zzblc;
    protected int zzbld = zzo.zzblk;

    public zzj(zzag zzag, zzf zzf, zzl zzl) {
        Preconditions.checkArgument((zzag == null && zzf == null) ? false : true, "At least one of RemoteModelLoader or LocalModelLoader must be non-null.");
        Preconditions.checkNotNull(zzl);
        this.zzbla = zzag;
        this.zzblb = zzf;
        this.zzblc = zzl;
    }

    public final synchronized void zza(zzm zzm) throws FirebaseMLException {
        Exception exc;
        boolean z;
        ArrayList arrayList = new ArrayList();
        boolean z2 = false;
        Exception e = null;
        try {
            z = zza(zzm, arrayList);
            exc = null;
        } catch (Exception e2) {
            exc = e2;
            z = false;
        }
        if (z) {
            this.zzblc.zze(arrayList);
            this.zzbld = zzo.zzbll;
            return;
        }
        try {
            z2 = zzb(zzm, arrayList);
        } catch (Exception e3) {
            e = e3;
        }
        if (z2) {
            this.zzblc.zze(arrayList);
            this.zzbld = zzo.zzblm;
            return;
        }
        arrayList.add(zzoc.NO_VALID_MODEL);
        this.zzblc.zze(arrayList);
        this.zzbld = zzo.zzblk;
        if (exc != null) {
            String valueOf = String.valueOf(zzov());
            throw new FirebaseMLException(valueOf.length() != 0 ? "Remote model load failed with the model options: ".concat(valueOf) : new String("Remote model load failed with the model options: "), 14, exc);
        } else if (e != null) {
            String valueOf2 = String.valueOf(zzov());
            throw new FirebaseMLException(valueOf2.length() != 0 ? "Local model load failed with the model options: ".concat(valueOf2) : new String("Local model load failed with the model options: "), 14, e);
        } else {
            String valueOf3 = String.valueOf(zzov());
            throw new FirebaseMLException(valueOf3.length() != 0 ? "Cannot load any model with the model options: ".concat(valueOf3) : new String("Cannot load any model with the model options: "), 14);
        }
    }

    public final synchronized boolean zzou() {
        return this.zzbld == zzo.zzbll;
    }

    private final synchronized boolean zza(zzm zzm, List<zzoc> list) throws FirebaseMLException {
        zzag zzag = this.zzbla;
        if (zzag != null) {
            try {
                MappedByteBuffer load = zzag.load();
                if (load != null) {
                    try {
                        zzm.zza(load);
                        zzbin.d("ModelLoader", "Remote model source is loaded successfully");
                        return true;
                    } catch (RuntimeException e) {
                        list.add(zzoc.REMOTE_MODEL_INVALID);
                        throw e;
                    }
                } else {
                    zzbin.d("ModelLoader", "Remote model source can NOT be loaded, try local model.");
                    list.add(zzoc.REMOTE_MODEL_LOADER_LOADS_NO_MODEL);
                }
            } catch (FirebaseMLException e2) {
                zzbin.d("ModelLoader", "Remote model source can NOT be loaded, try local model.");
                list.add(zzoc.REMOTE_MODEL_LOADER_ERROR);
                throw e2;
            }
        }
        return false;
    }

    private final synchronized boolean zzb(zzm zzm, List<zzoc> list) throws FirebaseMLException {
        MappedByteBuffer load;
        zzf zzf = this.zzblb;
        if (zzf == null || (load = zzf.load()) == null) {
            return false;
        }
        try {
            zzm.zza(load);
            zzbin.d("ModelLoader", "Local model source is loaded successfully");
            return true;
        } catch (RuntimeException e) {
            list.add(zzoc.LOCAL_MODEL_INVALID);
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    private final String zzov() {
        String str;
        zzag zzag;
        String str2;
        zzf zzf = this.zzblb;
        if (zzf != null) {
            if (zzf.zzot().getAssetFilePath() != null) {
                str = this.zzblb.zzot().getAssetFilePath();
            } else if (this.zzblb.zzot().getFilePath() != null) {
                str = this.zzblb.zzot().getFilePath();
            }
            zzag = this.zzbla;
            if (zzag != null) {
                str2 = "unspecified";
            } else {
                str2 = zzag.zzpn().getUniqueModelNameForPersist();
            }
            return String.format("Local model path: %s. Remote model name: %s. ", str, str2);
        }
        str = null;
        zzag = this.zzbla;
        if (zzag != null) {
        }
        return String.format("Local model path: %s. Remote model name: %s. ", str, str2);
    }
}
