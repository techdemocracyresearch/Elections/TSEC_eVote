package com.google.firebase.iid;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-iid@@20.1.5 */
public final /* synthetic */ class zzl implements Runnable {
    private final FirebaseInstanceId zza;

    zzl(FirebaseInstanceId firebaseInstanceId) {
        this.zza = firebaseInstanceId;
    }

    public final void run() {
        this.zza.zzi();
    }
}
