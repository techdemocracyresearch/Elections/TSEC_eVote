package com.mvote.helloworld;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import com.face.liveness.LivenessController;
import com.face.liveness.T5LivenessListener;
import com.face.liveness.model.Tech5LivenessRequestModel;

public class LivelinessIntegration {
    public String captureError;
    public boolean capture_done = false;
    public boolean capture_error = false;
    public String capturedImage;
    Context context;

    public LivelinessIntegration(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: package-private */
    public String check_liveliness(String str) {
        Log.d("MVOTEC:", "Livelinees start...");
        LivenessController.getInstance(this.context, "eyJna2V5IjoieEFTUlJqYytDbkIrRmZPcVFtaW9OYmxZb2xOWEhiSUhBS28wUU5zUE5WUCtMSHg4emJKdDdsRndZOFhURWowWkZHTU43TzFncUU2QWpqZ1NtRkZ3XC9IcDEyUzJ4OXlhZ3RWWkpoRUVuMXNwOVJaT1BINlFFeE8ycDMyblVDS05lIiwiY24iOiJJS3k3XC9BRUFBQUJoQUFBQUFBQUFBQUFBQUFBQUFBQUFCZ0FBQUFBQUFBQT0ifQ==").detectLivenessFace(new Tech5LivenessRequestModel.Builder().setChallengesCount(0).setChallengeTimeoutInSec(10).setChallengeStartCounterInSec(3).setIsUseBackCamera(false).setIsCompression(false).setLanguageCode(str).setIsShowTextInstructions(false).setIsShowImageInstructions(false).setIsShowSelfieCaptureButton(false).create(), new T5LivenessListener() {
            /* class com.mvote.helloworld.LivelinessIntegration.AnonymousClass1 */

            @Override // com.face.liveness.T5LivenessListener
            public void onSuccess(byte[] bArr, byte[] bArr2) {
                LivelinessIntegration.this.capturedImage = Base64.encodeToString(bArr, 2);
                LivelinessIntegration.this.capture_done = true;
            }

            @Override // com.face.liveness.T5LivenessListener
            public void onError(int i, String str) {
                LivelinessIntegration.this.captureError = String.valueOf(i);
                LivelinessIntegration.this.capture_error = true;
            }
        });
        Log.d("MVOTEC:", "Livelinees done...");
        return "success";
    }

    /* access modifiers changed from: package-private */
    public String getImage() {
        if (!this.capture_error) {
            return this.capturedImage;
        }
        return "errormsg#" + this.captureError;
    }
}
