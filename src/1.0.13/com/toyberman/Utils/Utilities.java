package com.toyberman.Utils;

import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.Request;

public class Utilities {
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0024 A[SYNTHETIC, Splitter:B:18:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x002d  */
    public static void copyInputStreamToFile(InputStream inputStream, File file) {
        Throwable th;
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream fileOutputStream2 = new FileOutputStream(file);
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read > 0) {
                        fileOutputStream2.write(bArr, 0, read);
                    } else {
                        try {
                            break;
                        } catch (IOException unused) {
                            return;
                        }
                    }
                }
                fileOutputStream2.close();
            } catch (Exception unused2) {
                fileOutputStream = fileOutputStream2;
                if (fileOutputStream != null) {
                }
                inputStream.close();
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = fileOutputStream2;
                if (fileOutputStream != null) {
                }
                inputStream.close();
                throw th;
            }
        } catch (Exception unused3) {
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            inputStream.close();
        } catch (Throwable th3) {
            th = th3;
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException unused4) {
                    throw th;
                }
            }
            inputStream.close();
            throw th;
        }
        inputStream.close();
    }

    public static void addHeadersFromMap(ReadableMap readableMap, Request.Builder builder) {
        ReadableMapKeySetIterator keySetIterator = readableMap.keySetIterator();
        while (keySetIterator.hasNextKey()) {
            String nextKey = keySetIterator.nextKey();
            builder.addHeader(nextKey, readableMap.getString(nextKey));
        }
    }
}
