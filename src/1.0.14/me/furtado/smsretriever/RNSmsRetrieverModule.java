package me.furtado.smsretriever;

import android.app.Activity;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import me.furtado.smsretriever.PhoneNumberHelper;

final class RNSmsRetrieverModule extends ReactContextBaseJavaModule {
    private final PhoneNumberHelper mPhoneNumberHelper = new PhoneNumberHelper();
    private final SmsHelper mSmsHelper;

    @Override // com.facebook.react.bridge.NativeModule
    public String getName() {
        return "RNSmsRetrieverModule";
    }

    RNSmsRetrieverModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
        this.mSmsHelper = new SmsHelper(reactApplicationContext);
    }

    @ReactMethod
    public void requestPhoneNumber(Promise promise) {
        final ReactApplicationContext reactApplicationContext = getReactApplicationContext();
        Activity currentActivity = getCurrentActivity();
        final ActivityEventListener activityEventListener = this.mPhoneNumberHelper.getActivityEventListener();
        reactApplicationContext.addActivityEventListener(activityEventListener);
        this.mPhoneNumberHelper.setListener(new PhoneNumberHelper.Listener() {
            /* class me.furtado.smsretriever.RNSmsRetrieverModule.AnonymousClass1 */

            @Override // me.furtado.smsretriever.PhoneNumberHelper.Listener
            public void phoneNumberResultReceived() {
                reactApplicationContext.removeActivityEventListener(activityEventListener);
            }
        });
        this.mPhoneNumberHelper.requestPhoneNumber(reactApplicationContext, currentActivity, promise);
    }

    @ReactMethod
    public void startSmsRetriever(Promise promise) {
        this.mSmsHelper.startRetriever(promise);
    }
}
