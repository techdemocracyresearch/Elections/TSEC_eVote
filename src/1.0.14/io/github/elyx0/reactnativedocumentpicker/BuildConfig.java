package io.github.elyx0.reactnativedocumentpicker;

public final class BuildConfig {
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String LIBRARY_PACKAGE_NAME = "io.github.elyx0.reactnativedocumentpicker";
}
