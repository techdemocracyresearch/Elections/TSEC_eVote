package javax.mail;

import androidx.core.app.NotificationCompat;
import com.sun.mail.util.LineInputStream;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.mail.Provider;

public final class Session {
    private static Session defaultSession;
    private final Properties addressMap = new Properties();
    private final Hashtable authTable = new Hashtable();
    private final Authenticator authenticator;
    private boolean debug = false;
    private PrintStream out;
    private final Properties props;
    private final Vector providers = new Vector();
    private final Hashtable providersByClassName = new Hashtable();
    private final Hashtable providersByProtocol = new Hashtable();

    private Session(Properties properties, Authenticator authenticator2) {
        Class<?> cls;
        this.props = properties;
        this.authenticator = authenticator2;
        if (Boolean.valueOf(properties.getProperty("mail.debug")).booleanValue()) {
            this.debug = true;
        }
        if (this.debug) {
            pr("DEBUG: JavaMail version 1.4.1");
        }
        if (authenticator2 != null) {
            cls = authenticator2.getClass();
        } else {
            cls = getClass();
        }
        loadProviders(cls);
        loadAddressMap(cls);
    }

    public static Session getInstance(Properties properties, Authenticator authenticator2) {
        return new Session(properties, authenticator2);
    }

    public static Session getInstance(Properties properties) {
        return new Session(properties, null);
    }

    public static synchronized Session getDefaultInstance(Properties properties, Authenticator authenticator2) {
        Session session;
        synchronized (Session.class) {
            Session session2 = defaultSession;
            if (session2 == null) {
                defaultSession = new Session(properties, authenticator2);
            } else {
                Authenticator authenticator3 = session2.authenticator;
                if (authenticator3 != authenticator2) {
                    if (authenticator3 == null || authenticator2 == null || authenticator3.getClass().getClassLoader() != authenticator2.getClass().getClassLoader()) {
                        throw new SecurityException("Access to default session denied");
                    }
                }
            }
            session = defaultSession;
        }
        return session;
    }

    public static Session getDefaultInstance(Properties properties) {
        return getDefaultInstance(properties, null);
    }

    public synchronized void setDebug(boolean z) {
        this.debug = z;
        if (z) {
            pr("DEBUG: setDebug: JavaMail version 1.4.1");
        }
    }

    public synchronized boolean getDebug() {
        return this.debug;
    }

    public synchronized void setDebugOut(PrintStream printStream) {
        this.out = printStream;
    }

    public synchronized PrintStream getDebugOut() {
        PrintStream printStream = this.out;
        if (printStream != null) {
            return printStream;
        }
        return System.out;
    }

    public synchronized Provider[] getProviders() {
        Provider[] providerArr;
        providerArr = new Provider[this.providers.size()];
        this.providers.copyInto(providerArr);
        return providerArr;
    }

    public synchronized Provider getProvider(String str) throws NoSuchProviderException {
        if (str != null) {
            if (str.length() > 0) {
                Provider provider = null;
                Properties properties = this.props;
                String property = properties.getProperty("mail." + str + ".class");
                if (property != null) {
                    if (this.debug) {
                        pr("DEBUG: mail." + str + ".class property exists and points to " + property);
                    }
                    provider = (Provider) this.providersByClassName.get(property);
                }
                if (provider != null) {
                    return provider;
                }
                Provider provider2 = (Provider) this.providersByProtocol.get(str);
                if (provider2 != null) {
                    if (this.debug) {
                        pr("DEBUG: getProvider() returning " + provider2.toString());
                    }
                    return provider2;
                }
                throw new NoSuchProviderException("No provider for " + str);
            }
        }
        throw new NoSuchProviderException("Invalid protocol: null");
    }

    public synchronized void setProvider(Provider provider) throws NoSuchProviderException {
        if (provider != null) {
            this.providersByProtocol.put(provider.getProtocol(), provider);
            Properties properties = this.props;
            properties.put("mail." + provider.getProtocol() + ".class", provider.getClassName());
        } else {
            throw new NoSuchProviderException("Can't set null provider");
        }
    }

    public Store getStore() throws NoSuchProviderException {
        return getStore(getProperty("mail.store.protocol"));
    }

    public Store getStore(String str) throws NoSuchProviderException {
        return getStore(new URLName(str, null, -1, null, null, null));
    }

    public Store getStore(URLName uRLName) throws NoSuchProviderException {
        return getStore(getProvider(uRLName.getProtocol()), uRLName);
    }

    public Store getStore(Provider provider) throws NoSuchProviderException {
        return getStore(provider, null);
    }

    private Store getStore(Provider provider, URLName uRLName) throws NoSuchProviderException {
        if (provider == null || provider.getType() != Provider.Type.STORE) {
            throw new NoSuchProviderException("invalid provider");
        }
        try {
            return (Store) getService(provider, uRLName);
        } catch (ClassCastException unused) {
            throw new NoSuchProviderException("incorrect class");
        }
    }

    public Folder getFolder(URLName uRLName) throws MessagingException {
        Store store = getStore(uRLName);
        store.connect();
        return store.getFolder(uRLName);
    }

    public Transport getTransport() throws NoSuchProviderException {
        return getTransport(getProperty("mail.transport.protocol"));
    }

    public Transport getTransport(String str) throws NoSuchProviderException {
        return getTransport(new URLName(str, null, -1, null, null, null));
    }

    public Transport getTransport(URLName uRLName) throws NoSuchProviderException {
        return getTransport(getProvider(uRLName.getProtocol()), uRLName);
    }

    public Transport getTransport(Provider provider) throws NoSuchProviderException {
        return getTransport(provider, null);
    }

    public Transport getTransport(Address address) throws NoSuchProviderException {
        String str = (String) this.addressMap.get(address.getType());
        if (str != null) {
            return getTransport(str);
        }
        throw new NoSuchProviderException("No provider for Address type: " + address.getType());
    }

    private Transport getTransport(Provider provider, URLName uRLName) throws NoSuchProviderException {
        if (provider == null || provider.getType() != Provider.Type.TRANSPORT) {
            throw new NoSuchProviderException("invalid provider");
        }
        try {
            return (Transport) getService(provider, uRLName);
        } catch (ClassCastException unused) {
            throw new NoSuchProviderException("incorrect class");
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:16|17) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r1 = java.lang.Class.forName(r8.getClassName());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007e, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0081, code lost:
        if (r7.debug != false) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0083, code lost:
        r9.printStackTrace(getDebugOut());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0093, code lost:
        throw new javax.mail.NoSuchProviderException(r8.getProtocol());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0044 */
    private Object getService(Provider provider, URLName uRLName) throws NoSuchProviderException {
        ClassLoader classLoader;
        if (provider != null) {
            if (uRLName == null) {
                uRLName = new URLName(provider.getProtocol(), null, -1, null, null, null);
            }
            Authenticator authenticator2 = this.authenticator;
            if (authenticator2 != null) {
                classLoader = authenticator2.getClass().getClassLoader();
            } else {
                classLoader = getClass().getClassLoader();
            }
            Class<?> cls = null;
            ClassLoader contextClassLoader = getContextClassLoader();
            if (contextClassLoader != null) {
                try {
                    cls = contextClassLoader.loadClass(provider.getClassName());
                } catch (ClassNotFoundException unused) {
                }
            }
            if (cls == null) {
                cls = classLoader.loadClass(provider.getClassName());
            }
            try {
                return cls.getConstructor(Session.class, URLName.class).newInstance(this, uRLName);
            } catch (Exception e) {
                if (this.debug) {
                    e.printStackTrace(getDebugOut());
                }
                throw new NoSuchProviderException(provider.getProtocol());
            }
        } else {
            throw new NoSuchProviderException("null");
        }
    }

    public void setPasswordAuthentication(URLName uRLName, PasswordAuthentication passwordAuthentication) {
        if (passwordAuthentication == null) {
            this.authTable.remove(uRLName);
        } else {
            this.authTable.put(uRLName, passwordAuthentication);
        }
    }

    public PasswordAuthentication getPasswordAuthentication(URLName uRLName) {
        return (PasswordAuthentication) this.authTable.get(uRLName);
    }

    public PasswordAuthentication requestPasswordAuthentication(InetAddress inetAddress, int i, String str, String str2, String str3) {
        Authenticator authenticator2 = this.authenticator;
        if (authenticator2 != null) {
            return authenticator2.requestPasswordAuthentication(inetAddress, i, str, str2, str3);
        }
        return null;
    }

    public Properties getProperties() {
        return this.props;
    }

    public String getProperty(String str) {
        return this.props.getProperty(str);
    }

    private void loadProviders(Class cls) {
        AnonymousClass1 r0 = new StreamLoader() {
            /* class javax.mail.Session.AnonymousClass1 */

            @Override // javax.mail.StreamLoader
            public void load(InputStream inputStream) throws IOException {
                Session.this.loadProvidersFromStream(inputStream);
            }
        };
        try {
            loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "javamail.providers", r0);
        } catch (SecurityException e) {
            if (this.debug) {
                pr("DEBUG: can't get java.home: " + e);
            }
        }
        loadAllResources("META-INF/javamail.providers", cls, r0);
        loadResource("/META-INF/javamail.default.providers", cls, r0);
        if (this.providers.size() == 0) {
            if (this.debug) {
                pr("DEBUG: failed to load any providers, using defaults");
            }
            addProvider(new Provider(Provider.Type.STORE, "imap", "com.sun.mail.imap.IMAPStore", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.STORE, "imaps", "com.sun.mail.imap.IMAPSSLStore", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.STORE, "pop3", "com.sun.mail.pop3.POP3Store", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.STORE, "pop3s", "com.sun.mail.pop3.POP3SSLStore", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.TRANSPORT, "smtp", "com.sun.mail.smtp.SMTPTransport", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.TRANSPORT, "smtps", "com.sun.mail.smtp.SMTPSSLTransport", "Sun Microsystems, Inc.", Version.version));
        }
        if (this.debug) {
            pr("DEBUG: Tables of loaded providers");
            pr("DEBUG: Providers Listed By Class Name: " + this.providersByClassName.toString());
            pr("DEBUG: Providers Listed By Protocol: " + this.providersByProtocol.toString());
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void loadProvidersFromStream(InputStream inputStream) throws IOException {
        if (inputStream != null) {
            LineInputStream lineInputStream = new LineInputStream(inputStream);
            while (true) {
                String readLine = lineInputStream.readLine();
                if (readLine != null) {
                    if (!readLine.startsWith("#")) {
                        StringTokenizer stringTokenizer = new StringTokenizer(readLine, ";");
                        Provider.Type type = null;
                        String str = null;
                        String str2 = null;
                        String str3 = null;
                        String str4 = null;
                        while (stringTokenizer.hasMoreTokens()) {
                            String trim = stringTokenizer.nextToken().trim();
                            int indexOf = trim.indexOf("=");
                            if (trim.startsWith("protocol=")) {
                                str = trim.substring(indexOf + 1);
                            } else if (trim.startsWith("type=")) {
                                String substring = trim.substring(indexOf + 1);
                                if (substring.equalsIgnoreCase("store")) {
                                    type = Provider.Type.STORE;
                                } else if (substring.equalsIgnoreCase(NotificationCompat.CATEGORY_TRANSPORT)) {
                                    type = Provider.Type.TRANSPORT;
                                }
                            } else if (trim.startsWith("class=")) {
                                str2 = trim.substring(indexOf + 1);
                            } else if (trim.startsWith("vendor=")) {
                                str3 = trim.substring(indexOf + 1);
                            } else if (trim.startsWith("version=")) {
                                str4 = trim.substring(indexOf + 1);
                            }
                        }
                        if (type != null && str != null && str2 != null && str.length() > 0 && str2.length() > 0) {
                            addProvider(new Provider(type, str, str2, str3, str4));
                        } else if (this.debug) {
                            pr("DEBUG: Bad provider entry: " + readLine);
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    public synchronized void addProvider(Provider provider) {
        this.providers.addElement(provider);
        this.providersByClassName.put(provider.getClassName(), provider);
        if (!this.providersByProtocol.containsKey(provider.getProtocol())) {
            this.providersByProtocol.put(provider.getProtocol(), provider);
        }
    }

    private void loadAddressMap(Class cls) {
        AnonymousClass2 r0 = new StreamLoader() {
            /* class javax.mail.Session.AnonymousClass2 */

            @Override // javax.mail.StreamLoader
            public void load(InputStream inputStream) throws IOException {
                Session.this.addressMap.load(inputStream);
            }
        };
        loadResource("/META-INF/javamail.default.address.map", cls, r0);
        loadAllResources("META-INF/javamail.address.map", cls, r0);
        try {
            loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "javamail.address.map", r0);
        } catch (SecurityException e) {
            if (this.debug) {
                pr("DEBUG: can't get java.home: " + e);
            }
        }
        if (this.addressMap.isEmpty()) {
            if (this.debug) {
                pr("DEBUG: failed to load address map, using defaults");
            }
            this.addressMap.put("rfc822", "smtp");
        }
    }

    public synchronized void setProtocolForAddress(String str, String str2) {
        if (str2 == null) {
            this.addressMap.remove(str);
        } else {
            this.addressMap.put(str, str2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b A[Catch:{ all -> 0x0034 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0088 A[SYNTHETIC, Splitter:B:30:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    private void loadFile(String str, StreamLoader streamLoader) {
        Throwable th;
        IOException e;
        SecurityException e2;
        BufferedInputStream bufferedInputStream = null;
        try {
            BufferedInputStream bufferedInputStream2 = new BufferedInputStream(new FileInputStream(str));
            try {
                streamLoader.load(bufferedInputStream2);
                if (this.debug) {
                    pr("DEBUG: successfully loaded file: " + str);
                }
                try {
                    bufferedInputStream2.close();
                } catch (IOException unused) {
                }
            } catch (IOException e3) {
                e = e3;
                bufferedInputStream = bufferedInputStream2;
                if (this.debug) {
                    pr("DEBUG: not loading file: " + str);
                    pr("DEBUG: " + e);
                }
                if (bufferedInputStream == null) {
                    return;
                }
                bufferedInputStream.close();
            } catch (SecurityException e4) {
                e2 = e4;
                bufferedInputStream = bufferedInputStream2;
                try {
                    if (this.debug) {
                        pr("DEBUG: not loading file: " + str);
                        pr("DEBUG: " + e2);
                    }
                    if (bufferedInputStream == null) {
                        return;
                    }
                    bufferedInputStream.close();
                } catch (Throwable th2) {
                    th = th2;
                    if (bufferedInputStream != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                bufferedInputStream = bufferedInputStream2;
                if (bufferedInputStream != null) {
                    try {
                        bufferedInputStream.close();
                    } catch (IOException unused2) {
                    }
                }
                throw th;
            }
        } catch (IOException e5) {
            e = e5;
            if (this.debug) {
            }
            if (bufferedInputStream == null) {
            }
            bufferedInputStream.close();
        } catch (SecurityException e6) {
            e2 = e6;
            if (this.debug) {
            }
            if (bufferedInputStream == null) {
            }
            bufferedInputStream.close();
        }
    }

    private void loadResource(String str, Class cls, StreamLoader streamLoader) {
        InputStream inputStream = null;
        try {
            inputStream = getResourceAsStream(cls, str);
            if (inputStream != null) {
                streamLoader.load(inputStream);
                if (this.debug) {
                    pr("DEBUG: successfully loaded resource: " + str);
                }
            } else if (this.debug) {
                pr("DEBUG: not loading resource: " + str);
            }
            if (inputStream == null) {
                return;
            }
        } catch (IOException e) {
            if (this.debug) {
                pr("DEBUG: " + e);
            }
            if (0 == 0) {
                return;
            }
        } catch (SecurityException e2) {
            if (this.debug) {
                pr("DEBUG: " + e2);
            }
            if (0 == 0) {
                return;
            }
        } catch (Throwable th) {
            if (0 != 0) {
                try {
                    inputStream.close();
                } catch (IOException unused) {
                }
            }
            throw th;
        }
        try {
            inputStream.close();
        } catch (IOException unused2) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ae, code lost:
        if (r5 == null) goto L_0x00b7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0086 A[Catch:{ IOException -> 0x0098, SecurityException -> 0x007f, all -> 0x007d, all -> 0x00bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0097 A[Catch:{ IOException -> 0x0098, SecurityException -> 0x007f, all -> 0x007d, all -> 0x00bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x009f A[Catch:{ IOException -> 0x0098, SecurityException -> 0x007f, all -> 0x007d, all -> 0x00bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00c0 A[SYNTHETIC, Splitter:B:63:0x00c0] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:83:? A[RETURN, SYNTHETIC] */
    private void loadAllResources(String str, Class cls, StreamLoader streamLoader) {
        Exception e;
        URL[] urlArr;
        Throwable th;
        IOException e2;
        SecurityException e3;
        int i = 0;
        try {
            ClassLoader contextClassLoader = getContextClassLoader();
            if (contextClassLoader == null) {
                contextClassLoader = cls.getClassLoader();
            }
            if (contextClassLoader != null) {
                urlArr = getResources(contextClassLoader, str);
            } else {
                urlArr = getSystemResources(str);
            }
            if (urlArr != null) {
                int i2 = 0;
                while (i < urlArr.length) {
                    try {
                        URL url = urlArr[i];
                        InputStream inputStream = null;
                        if (this.debug) {
                            pr("DEBUG: URL " + url);
                        }
                        int i3 = 1;
                        try {
                            inputStream = openStream(url);
                            if (inputStream != null) {
                                streamLoader.load(inputStream);
                                try {
                                    if (this.debug) {
                                        pr("DEBUG: successfully loaded resource: " + url);
                                    }
                                    i2 = 1;
                                } catch (IOException e4) {
                                    e2 = e4;
                                    if (this.debug) {
                                        pr("DEBUG: " + e2);
                                    }
                                } catch (SecurityException e5) {
                                    e3 = e5;
                                    if (this.debug) {
                                        pr("DEBUG: " + e3);
                                    }
                                    if (inputStream != null) {
                                        try {
                                            inputStream.close();
                                        } catch (IOException unused) {
                                        } catch (Exception e6) {
                                            e = e6;
                                            i = i3;
                                            if (this.debug) {
                                            }
                                            if (i != 0) {
                                            }
                                        }
                                    }
                                    i2 = i3;
                                    i++;
                                } catch (Throwable th2) {
                                    th = th2;
                                    if (inputStream != null) {
                                        try {
                                            inputStream.close();
                                        } catch (IOException unused2) {
                                        }
                                    }
                                    throw th;
                                }
                            } else if (this.debug) {
                                pr("DEBUG: not loading resource: " + url);
                            }
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException unused3) {
                                }
                            }
                        } catch (IOException e7) {
                            i3 = i2;
                            e2 = e7;
                            if (this.debug) {
                            }
                        } catch (SecurityException e8) {
                            i3 = i2;
                            e3 = e8;
                            if (this.debug) {
                            }
                            if (inputStream != null) {
                            }
                            i2 = i3;
                            i++;
                        } catch (Throwable th3) {
                            th = th3;
                            if (inputStream != null) {
                            }
                            throw th;
                        }
                        i++;
                    } catch (Exception e9) {
                        e = e9;
                        i = i2;
                        if (this.debug) {
                        }
                        if (i != 0) {
                        }
                    }
                }
                i = i2;
            }
        } catch (Exception e10) {
            e = e10;
            if (this.debug) {
                pr("DEBUG: " + e);
            }
            if (i != 0) {
            }
        }
        if (i != 0) {
            if (this.debug) {
                pr("DEBUG: !anyLoaded");
            }
            loadResource("/" + str, cls, streamLoader);
        }
    }

    private void pr(String str) {
        getDebugOut().println(str);
    }

    private static ClassLoader getContextClassLoader() {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            /* class javax.mail.Session.AnonymousClass3 */

            @Override // java.security.PrivilegedAction
            public Object run() {
                try {
                    return Thread.currentThread().getContextClassLoader();
                } catch (SecurityException unused) {
                    return null;
                }
            }
        });
    }

    private static InputStream getResourceAsStream(final Class cls, final String str) throws IOException {
        try {
            return (InputStream) AccessController.doPrivileged(new PrivilegedExceptionAction() {
                /* class javax.mail.Session.AnonymousClass4 */

                @Override // java.security.PrivilegedExceptionAction
                public Object run() throws IOException {
                    return cls.getResourceAsStream(str);
                }
            });
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }

    private static URL[] getResources(final ClassLoader classLoader, final String str) {
        return (URL[]) AccessController.doPrivileged(new PrivilegedAction() {
            /* class javax.mail.Session.AnonymousClass5 */

            @Override // java.security.PrivilegedAction
            public Object run() {
                URL[] urlArr = null;
                try {
                    Vector vector = new Vector();
                    Enumeration<URL> resources = classLoader.getResources(str);
                    while (true) {
                        if (resources == null) {
                            break;
                        } else if (!resources.hasMoreElements()) {
                            break;
                        } else {
                            URL nextElement = resources.nextElement();
                            if (nextElement != null) {
                                vector.addElement(nextElement);
                            }
                        }
                    }
                    if (vector.size() <= 0) {
                        return urlArr;
                    }
                    URL[] urlArr2 = new URL[vector.size()];
                    vector.copyInto(urlArr2);
                    return urlArr2;
                } catch (IOException | SecurityException unused) {
                    return urlArr;
                }
            }
        });
    }

    private static URL[] getSystemResources(final String str) {
        return (URL[]) AccessController.doPrivileged(new PrivilegedAction() {
            /* class javax.mail.Session.AnonymousClass6 */

            @Override // java.security.PrivilegedAction
            public Object run() {
                URL[] urlArr = null;
                try {
                    Vector vector = new Vector();
                    Enumeration<URL> systemResources = ClassLoader.getSystemResources(str);
                    while (true) {
                        if (systemResources == null) {
                            break;
                        } else if (!systemResources.hasMoreElements()) {
                            break;
                        } else {
                            URL nextElement = systemResources.nextElement();
                            if (nextElement != null) {
                                vector.addElement(nextElement);
                            }
                        }
                    }
                    if (vector.size() <= 0) {
                        return urlArr;
                    }
                    URL[] urlArr2 = new URL[vector.size()];
                    vector.copyInto(urlArr2);
                    return urlArr2;
                } catch (IOException | SecurityException unused) {
                    return urlArr;
                }
            }
        });
    }

    private static InputStream openStream(final URL url) throws IOException {
        try {
            return (InputStream) AccessController.doPrivileged(new PrivilegedExceptionAction() {
                /* class javax.mail.Session.AnonymousClass7 */

                @Override // java.security.PrivilegedExceptionAction
                public Object run() throws IOException {
                    return url.openStream();
                }
            });
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }
}
