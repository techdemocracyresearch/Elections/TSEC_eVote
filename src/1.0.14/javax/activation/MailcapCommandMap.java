package javax.activation;

import com.sun.activation.registries.LogSupport;
import com.sun.activation.registries.MailcapFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MailcapCommandMap extends CommandMap {
    private static final int PROG = 0;
    private static MailcapFile defDB;
    private MailcapFile[] DB;

    public MailcapCommandMap() {
        ArrayList arrayList = new ArrayList(5);
        arrayList.add(null);
        LogSupport.log("MailcapCommandMap: load HOME");
        try {
            String property = System.getProperty("user.home");
            if (property != null) {
                MailcapFile loadFile = loadFile(String.valueOf(property) + File.separator + ".mailcap");
                if (loadFile != null) {
                    arrayList.add(loadFile);
                }
            }
        } catch (SecurityException unused) {
        }
        LogSupport.log("MailcapCommandMap: load SYS");
        try {
            MailcapFile loadFile2 = loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "mailcap");
            if (loadFile2 != null) {
                arrayList.add(loadFile2);
            }
        } catch (SecurityException unused2) {
        }
        LogSupport.log("MailcapCommandMap: load JAR");
        loadAllResources(arrayList, "mailcap");
        LogSupport.log("MailcapCommandMap: load DEF");
        synchronized (MailcapCommandMap.class) {
            if (defDB == null) {
                defDB = loadResource("mailcap.default");
            }
        }
        MailcapFile mailcapFile = defDB;
        if (mailcapFile != null) {
            arrayList.add(mailcapFile);
        }
        MailcapFile[] mailcapFileArr = new MailcapFile[arrayList.size()];
        this.DB = mailcapFileArr;
        this.DB = (MailcapFile[]) arrayList.toArray(mailcapFileArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0046, code lost:
        if (r2 != null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0083, code lost:
        if (r2 == null) goto L_0x0086;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a A[Catch:{ IOException -> 0x006c, SecurityException -> 0x0052, all -> 0x0050, all -> 0x0087 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006b A[Catch:{ IOException -> 0x006c, SecurityException -> 0x0052, all -> 0x0050, all -> 0x0087 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0074 A[Catch:{ IOException -> 0x006c, SecurityException -> 0x0052, all -> 0x0050, all -> 0x0087 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008b A[SYNTHETIC, Splitter:B:41:0x008b] */
    private MailcapFile loadResource(String str) {
        Throwable th;
        IOException e;
        InputStream inputStream;
        SecurityException e2;
        InputStream inputStream2 = null;
        try {
            inputStream = SecuritySupport.getResourceAsStream(getClass(), str);
            if (inputStream != null) {
                try {
                    MailcapFile mailcapFile = new MailcapFile(inputStream);
                    if (LogSupport.isLoggable()) {
                        LogSupport.log("MailcapCommandMap: successfully loaded mailcap file: " + str);
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused) {
                        }
                    }
                    return mailcapFile;
                } catch (IOException e3) {
                    e = e3;
                    if (LogSupport.isLoggable()) {
                        LogSupport.log("MailcapCommandMap: can't load " + str, e);
                    }
                } catch (SecurityException e4) {
                    e2 = e4;
                    if (LogSupport.isLoggable()) {
                        LogSupport.log("MailcapCommandMap: can't load " + str, e2);
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused2) {
                        }
                    }
                    return null;
                }
            } else if (LogSupport.isLoggable()) {
                LogSupport.log("MailcapCommandMap: not loading mailcap file: " + str);
            }
        } catch (IOException e5) {
            e = e5;
            inputStream = null;
            if (LogSupport.isLoggable()) {
            }
        } catch (SecurityException e6) {
            e2 = e6;
            inputStream = null;
            if (LogSupport.isLoggable()) {
            }
            if (inputStream != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            inputStream2 = inputStream;
            if (inputStream2 != null) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ce, code lost:
        if (r5 == null) goto L_0x00d7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a3 A[Catch:{ IOException -> 0x00b5, SecurityException -> 0x0099, all -> 0x0097, all -> 0x00dc }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b4 A[Catch:{ IOException -> 0x00b5, SecurityException -> 0x0099, all -> 0x0097, all -> 0x00dc }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00bf A[Catch:{ IOException -> 0x00b5, SecurityException -> 0x0099, all -> 0x0097, all -> 0x00dc }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00e0 A[SYNTHETIC, Splitter:B:66:0x00e0] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:88:? A[RETURN, SYNTHETIC] */
    private void loadAllResources(List list, String str) {
        Exception e;
        URL[] urlArr;
        Throwable th;
        IOException e2;
        SecurityException e3;
        int i = 0;
        try {
            ClassLoader contextClassLoader = SecuritySupport.getContextClassLoader();
            if (contextClassLoader == null) {
                contextClassLoader = getClass().getClassLoader();
            }
            if (contextClassLoader != null) {
                urlArr = SecuritySupport.getResources(contextClassLoader, str);
            } else {
                urlArr = SecuritySupport.getSystemResources(str);
            }
            if (urlArr != null) {
                if (LogSupport.isLoggable()) {
                    LogSupport.log("MailcapCommandMap: getResources");
                }
                int i2 = 0;
                while (i < urlArr.length) {
                    try {
                        URL url = urlArr[i];
                        InputStream inputStream = null;
                        if (LogSupport.isLoggable()) {
                            LogSupport.log("MailcapCommandMap: URL " + url);
                        }
                        int i3 = 1;
                        try {
                            inputStream = SecuritySupport.openStream(url);
                            if (inputStream != null) {
                                list.add(new MailcapFile(inputStream));
                                try {
                                    if (LogSupport.isLoggable()) {
                                        LogSupport.log("MailcapCommandMap: successfully loaded mailcap file from URL: " + url);
                                    }
                                    i2 = 1;
                                } catch (IOException e4) {
                                    e2 = e4;
                                    if (LogSupport.isLoggable()) {
                                    }
                                } catch (SecurityException e5) {
                                    e3 = e5;
                                    if (LogSupport.isLoggable()) {
                                    }
                                    if (inputStream != null) {
                                    }
                                    i2 = i3;
                                    i++;
                                } catch (Throwable th2) {
                                    th = th2;
                                    if (inputStream != null) {
                                    }
                                    throw th;
                                }
                            } else if (LogSupport.isLoggable()) {
                                LogSupport.log("MailcapCommandMap: not loading mailcap file from URL: " + url);
                            }
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException unused) {
                                }
                            }
                        } catch (IOException e6) {
                            i3 = i2;
                            e2 = e6;
                            if (LogSupport.isLoggable()) {
                                LogSupport.log("MailcapCommandMap: can't load " + url, e2);
                            }
                        } catch (SecurityException e7) {
                            i3 = i2;
                            e3 = e7;
                            if (LogSupport.isLoggable()) {
                                LogSupport.log("MailcapCommandMap: can't load " + url, e3);
                            }
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException unused2) {
                                } catch (Exception e8) {
                                    e = e8;
                                    i = i3;
                                    if (LogSupport.isLoggable()) {
                                    }
                                    if (i != 0) {
                                    }
                                }
                            }
                            i2 = i3;
                            i++;
                        } catch (Throwable th3) {
                            th = th3;
                            if (inputStream != null) {
                            }
                            throw th;
                        }
                        i++;
                    } catch (Exception e9) {
                        e = e9;
                        i = i2;
                        if (LogSupport.isLoggable()) {
                        }
                        if (i != 0) {
                        }
                    }
                }
                i = i2;
            }
        } catch (Exception e10) {
            e = e10;
            if (LogSupport.isLoggable()) {
                LogSupport.log("MailcapCommandMap: can't load " + str, e);
            }
            if (i != 0) {
            }
        }
        if (i != 0) {
            if (LogSupport.isLoggable()) {
                LogSupport.log("MailcapCommandMap: !anyLoaded");
            }
            MailcapFile loadResource = loadResource("/" + str);
            if (loadResource != null) {
                list.add(loadResource);
            }
        }
    }

    private MailcapFile loadFile(String str) {
        try {
            return new MailcapFile(str);
        } catch (IOException unused) {
            return null;
        }
    }

    public MailcapCommandMap(String str) throws IOException {
        this();
        if (LogSupport.isLoggable()) {
            LogSupport.log("MailcapCommandMap: load PROG from " + str);
        }
        MailcapFile[] mailcapFileArr = this.DB;
        if (mailcapFileArr[0] == null) {
            mailcapFileArr[0] = new MailcapFile(str);
        }
    }

    public MailcapCommandMap(InputStream inputStream) {
        this();
        LogSupport.log("MailcapCommandMap: load PROG");
        MailcapFile[] mailcapFileArr = this.DB;
        if (mailcapFileArr[0] == null) {
            try {
                mailcapFileArr[0] = new MailcapFile(inputStream);
            } catch (IOException unused) {
            }
        }
    }

    @Override // javax.activation.CommandMap
    public synchronized CommandInfo[] getPreferredCommands(String str) {
        ArrayList arrayList;
        Map mailcapFallbackList;
        Map mailcapList;
        arrayList = new ArrayList();
        if (str != null) {
            str = str.toLowerCase(Locale.ENGLISH);
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            MailcapFile[] mailcapFileArr = this.DB;
            if (i2 >= mailcapFileArr.length) {
                break;
            }
            if (!(mailcapFileArr[i2] == null || (mailcapList = mailcapFileArr[i2].getMailcapList(str)) == null)) {
                appendPrefCmdsToList(mailcapList, arrayList);
            }
            i2++;
        }
        while (true) {
            MailcapFile[] mailcapFileArr2 = this.DB;
            if (i >= mailcapFileArr2.length) {
            } else {
                if (!(mailcapFileArr2[i] == null || (mailcapFallbackList = mailcapFileArr2[i].getMailcapFallbackList(str)) == null)) {
                    appendPrefCmdsToList(mailcapFallbackList, arrayList);
                }
                i++;
            }
        }
        return (CommandInfo[]) arrayList.toArray(new CommandInfo[arrayList.size()]);
    }

    private void appendPrefCmdsToList(Map map, List list) {
        for (String str : map.keySet()) {
            if (!checkForVerb(list, str)) {
                list.add(new CommandInfo(str, (String) ((List) map.get(str)).get(0)));
            }
        }
    }

    private boolean checkForVerb(List list, String str) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (((CommandInfo) it.next()).getCommandName().equals(str)) {
                return true;
            }
        }
        return false;
    }

    @Override // javax.activation.CommandMap
    public synchronized CommandInfo[] getAllCommands(String str) {
        ArrayList arrayList;
        Map mailcapFallbackList;
        Map mailcapList;
        arrayList = new ArrayList();
        if (str != null) {
            str = str.toLowerCase(Locale.ENGLISH);
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            MailcapFile[] mailcapFileArr = this.DB;
            if (i2 >= mailcapFileArr.length) {
                break;
            }
            if (!(mailcapFileArr[i2] == null || (mailcapList = mailcapFileArr[i2].getMailcapList(str)) == null)) {
                appendCmdsToList(mailcapList, arrayList);
            }
            i2++;
        }
        while (true) {
            MailcapFile[] mailcapFileArr2 = this.DB;
            if (i >= mailcapFileArr2.length) {
            } else {
                if (!(mailcapFileArr2[i] == null || (mailcapFallbackList = mailcapFileArr2[i].getMailcapFallbackList(str)) == null)) {
                    appendCmdsToList(mailcapFallbackList, arrayList);
                }
                i++;
            }
        }
        return (CommandInfo[]) arrayList.toArray(new CommandInfo[arrayList.size()]);
    }

    private void appendCmdsToList(Map map, List list) {
        for (String str : map.keySet()) {
            for (String str2 : (List) map.get(str)) {
                list.add(new CommandInfo(str, str2));
            }
        }
    }

    @Override // javax.activation.CommandMap
    public synchronized CommandInfo getCommand(String str, String str2) {
        Map mailcapFallbackList;
        List list;
        String str3;
        Map mailcapList;
        List list2;
        String str4;
        if (str != null) {
            try {
                str = str.toLowerCase(Locale.ENGLISH);
            } catch (Throwable th) {
                throw th;
            }
        }
        int i = 0;
        while (true) {
            MailcapFile[] mailcapFileArr = this.DB;
            if (i >= mailcapFileArr.length) {
                int i2 = 0;
                while (true) {
                    MailcapFile[] mailcapFileArr2 = this.DB;
                    if (i2 >= mailcapFileArr2.length) {
                        return null;
                    }
                    if (mailcapFileArr2[i2] != null && (mailcapFallbackList = mailcapFileArr2[i2].getMailcapFallbackList(str)) != null && (list = (List) mailcapFallbackList.get(str2)) != null && (str3 = (String) list.get(0)) != null) {
                        return new CommandInfo(str2, str3);
                    }
                    i2++;
                }
            } else if (mailcapFileArr[i] != null && (mailcapList = mailcapFileArr[i].getMailcapList(str)) != null && (list2 = (List) mailcapList.get(str2)) != null && (str4 = (String) list2.get(0)) != null) {
                return new CommandInfo(str2, str4);
            } else {
                i++;
            }
        }
    }

    public synchronized void addMailcap(String str) {
        LogSupport.log("MailcapCommandMap: add to PROG");
        MailcapFile[] mailcapFileArr = this.DB;
        if (mailcapFileArr[0] == null) {
            mailcapFileArr[0] = new MailcapFile();
        }
        this.DB[0].appendToMailcap(str);
    }

    @Override // javax.activation.CommandMap
    public synchronized DataContentHandler createDataContentHandler(String str) {
        List list;
        DataContentHandler dataContentHandler;
        List list2;
        DataContentHandler dataContentHandler2;
        if (LogSupport.isLoggable()) {
            LogSupport.log("MailcapCommandMap: createDataContentHandler for " + str);
        }
        if (str != null) {
            str = str.toLowerCase(Locale.ENGLISH);
        }
        int i = 0;
        while (true) {
            MailcapFile[] mailcapFileArr = this.DB;
            if (i >= mailcapFileArr.length) {
                int i2 = 0;
                while (true) {
                    MailcapFile[] mailcapFileArr2 = this.DB;
                    if (i2 >= mailcapFileArr2.length) {
                        return null;
                    }
                    if (mailcapFileArr2[i2] != null) {
                        if (LogSupport.isLoggable()) {
                            LogSupport.log("  search fallback DB #" + i2);
                        }
                        Map mailcapFallbackList = this.DB[i2].getMailcapFallbackList(str);
                        if (!(mailcapFallbackList == null || (list = (List) mailcapFallbackList.get("content-handler")) == null || (dataContentHandler = getDataContentHandler((String) list.get(0))) == null)) {
                            return dataContentHandler;
                        }
                    }
                    i2++;
                }
            } else {
                if (mailcapFileArr[i] != null) {
                    if (LogSupport.isLoggable()) {
                        LogSupport.log("  search DB #" + i);
                    }
                    Map mailcapList = this.DB[i].getMailcapList(str);
                    if (!(mailcapList == null || (list2 = (List) mailcapList.get("content-handler")) == null || (dataContentHandler2 = getDataContentHandler((String) list2.get(0))) == null)) {
                        return dataContentHandler2;
                    }
                }
                i++;
            }
        }
    }

    private DataContentHandler getDataContentHandler(String str) {
        Class<?> cls;
        if (LogSupport.isLoggable()) {
            LogSupport.log("    got content-handler");
        }
        if (LogSupport.isLoggable()) {
            LogSupport.log("      class " + str);
        }
        try {
            ClassLoader contextClassLoader = SecuritySupport.getContextClassLoader();
            if (contextClassLoader == null) {
                contextClassLoader = getClass().getClassLoader();
            }
            try {
                cls = contextClassLoader.loadClass(str);
            } catch (Exception unused) {
                cls = Class.forName(str);
            }
            if (cls != null) {
                return (DataContentHandler) cls.newInstance();
            }
            return null;
        } catch (IllegalAccessException e) {
            if (!LogSupport.isLoggable()) {
                return null;
            }
            LogSupport.log("Can't load DCH " + str, e);
            return null;
        } catch (ClassNotFoundException e2) {
            if (!LogSupport.isLoggable()) {
                return null;
            }
            LogSupport.log("Can't load DCH " + str, e2);
            return null;
        } catch (InstantiationException e3) {
            if (!LogSupport.isLoggable()) {
                return null;
            }
            LogSupport.log("Can't load DCH " + str, e3);
            return null;
        }
    }

    @Override // javax.activation.CommandMap
    public synchronized String[] getMimeTypes() {
        ArrayList arrayList;
        String[] mimeTypes;
        arrayList = new ArrayList();
        int i = 0;
        while (true) {
            MailcapFile[] mailcapFileArr = this.DB;
            if (i < mailcapFileArr.length) {
                if (!(mailcapFileArr[i] == null || (mimeTypes = mailcapFileArr[i].getMimeTypes()) == null)) {
                    for (int i2 = 0; i2 < mimeTypes.length; i2++) {
                        if (!arrayList.contains(mimeTypes[i2])) {
                            arrayList.add(mimeTypes[i2]);
                        }
                    }
                }
                i++;
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public synchronized String[] getNativeCommands(String str) {
        ArrayList arrayList;
        String[] nativeCommands;
        arrayList = new ArrayList();
        if (str != null) {
            str = str.toLowerCase(Locale.ENGLISH);
        }
        int i = 0;
        while (true) {
            MailcapFile[] mailcapFileArr = this.DB;
            if (i < mailcapFileArr.length) {
                if (!(mailcapFileArr[i] == null || (nativeCommands = mailcapFileArr[i].getNativeCommands(str)) == null)) {
                    for (int i2 = 0; i2 < nativeCommands.length; i2++) {
                        if (!arrayList.contains(nativeCommands[i2])) {
                            arrayList.add(nativeCommands[i2]);
                        }
                    }
                }
                i++;
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }
}
