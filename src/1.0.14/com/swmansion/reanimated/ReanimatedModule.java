package com.swmansion.reanimated;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.uimanager.NativeViewHierarchyManager;
import com.facebook.react.uimanager.UIBlock;
import com.facebook.react.uimanager.UIManagerModule;
import com.facebook.react.uimanager.UIManagerModuleListener;
import com.swmansion.reanimated.transitions.TransitionModule;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.annotation.Nullable;

@ReactModule(name = ReanimatedModule.NAME)
public class ReanimatedModule extends ReactContextBaseJavaModule implements LifecycleEventListener, UIManagerModuleListener {
    public static final String NAME = "ReanimatedModule";
    @Nullable
    private NodesManager mNodesManager;
    private ArrayList<UIThreadOperation> mOperations = new ArrayList<>();
    @Nullable
    private TransitionModule mTransitionManager;

    private interface UIThreadOperation {
        void execute(NodesManager nodesManager);
    }

    @Override // com.facebook.react.bridge.NativeModule
    public String getName() {
        return NAME;
    }

    @Override // com.facebook.react.bridge.LifecycleEventListener
    public void onHostDestroy() {
    }

    public ReanimatedModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
    }

    @Override // com.facebook.react.bridge.BaseJavaModule, com.facebook.react.bridge.NativeModule
    public void initialize() {
        ReactApplicationContext reactApplicationContext = getReactApplicationContext();
        UIManagerModule uIManagerModule = (UIManagerModule) reactApplicationContext.getNativeModule(UIManagerModule.class);
        reactApplicationContext.addLifecycleEventListener(this);
        uIManagerModule.addUIManagerListener(this);
        this.mTransitionManager = new TransitionModule(uIManagerModule);
    }

    @Override // com.facebook.react.bridge.LifecycleEventListener
    public void onHostPause() {
        NodesManager nodesManager = this.mNodesManager;
        if (nodesManager != null) {
            nodesManager.onHostPause();
        }
    }

    @Override // com.facebook.react.bridge.LifecycleEventListener
    public void onHostResume() {
        NodesManager nodesManager = this.mNodesManager;
        if (nodesManager != null) {
            nodesManager.onHostResume();
        }
    }

    @Override // com.facebook.react.uimanager.UIManagerModuleListener
    public void willDispatchViewUpdates(UIManagerModule uIManagerModule) {
        if (!this.mOperations.isEmpty()) {
            final ArrayList<UIThreadOperation> arrayList = this.mOperations;
            this.mOperations = new ArrayList<>();
            uIManagerModule.addUIBlock(new UIBlock() {
                /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass1 */

                @Override // com.facebook.react.uimanager.UIBlock
                public void execute(NativeViewHierarchyManager nativeViewHierarchyManager) {
                    NodesManager nodesManager = ReanimatedModule.this.getNodesManager();
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        ((UIThreadOperation) it.next()).execute(nodesManager);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private NodesManager getNodesManager() {
        if (this.mNodesManager == null) {
            this.mNodesManager = new NodesManager(getReactApplicationContext());
        }
        return this.mNodesManager;
    }

    @ReactMethod
    public void animateNextTransition(int i, ReadableMap readableMap) {
        this.mTransitionManager.animateNextTransition(i, readableMap);
    }

    @ReactMethod
    public void createNode(final int i, final ReadableMap readableMap) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass2 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.createNode(i, readableMap);
            }
        });
    }

    @ReactMethod
    public void dropNode(final int i) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass3 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.dropNode(i);
            }
        });
    }

    @ReactMethod
    public void connectNodes(final int i, final int i2) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass4 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.connectNodes(i, i2);
            }
        });
    }

    @ReactMethod
    public void disconnectNodes(final int i, final int i2) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass5 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.disconnectNodes(i, i2);
            }
        });
    }

    @ReactMethod
    public void connectNodeToView(final int i, final int i2) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass6 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.connectNodeToView(i, i2);
            }
        });
    }

    @ReactMethod
    public void disconnectNodeFromView(final int i, final int i2) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass7 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.disconnectNodeFromView(i, i2);
            }
        });
    }

    @ReactMethod
    public void attachEvent(final int i, final String str, final int i2) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass8 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.attachEvent(i, str, i2);
            }
        });
    }

    @ReactMethod
    public void detachEvent(final int i, final String str, final int i2) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass9 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.detachEvent(i, str, i2);
            }
        });
    }

    @ReactMethod
    public void configureProps(ReadableArray readableArray, ReadableArray readableArray2) {
        int size = readableArray.size();
        final HashSet hashSet = new HashSet(size);
        for (int i = 0; i < size; i++) {
            hashSet.add(readableArray.getString(i));
        }
        int size2 = readableArray2.size();
        final HashSet hashSet2 = new HashSet(size2);
        for (int i2 = 0; i2 < size2; i2++) {
            hashSet2.add(readableArray2.getString(i2));
        }
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass10 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.configureProps(hashSet, hashSet2);
            }
        });
    }

    @ReactMethod
    public void getValue(final int i, final Callback callback) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass11 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.getValue(i, callback);
            }
        });
    }

    @ReactMethod
    public void setValue(final int i, final Double d) {
        this.mOperations.add(new UIThreadOperation() {
            /* class com.swmansion.reanimated.ReanimatedModule.AnonymousClass12 */

            @Override // com.swmansion.reanimated.ReanimatedModule.UIThreadOperation
            public void execute(NodesManager nodesManager) {
                nodesManager.setValue(i, d);
            }
        });
    }
}
