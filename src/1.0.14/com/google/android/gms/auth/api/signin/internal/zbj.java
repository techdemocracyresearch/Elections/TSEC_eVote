package com.google.android.gms.auth.api.signin.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

/* compiled from: com.google.android.gms:play-services-auth@@19.2.0 */
final class zbj extends zba {
    final /* synthetic */ zbk zba;

    zbj(zbk zbk) {
        this.zba = zbk;
    }

    @Override // com.google.android.gms.auth.api.signin.internal.zbr, com.google.android.gms.auth.api.signin.internal.zba
    public final void zbb(Status status) throws RemoteException {
        this.zba.setResult((Result) status);
    }
}
