package com.google.android.gms.vision.label.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.android.gms:play-services-vision-image-label@@18.0.3 */
public final class zzf extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzf> CREATOR = new zze();
    public final String label;
    public final String zzdo;
    public final float zzdp;

    public zzf(String str, String str2, float f) {
        this.label = str2;
        this.zzdp = f;
        this.zzdo = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.label, false);
        SafeParcelWriter.writeFloat(parcel, 3, this.zzdp);
        SafeParcelWriter.writeString(parcel, 4, this.zzdo, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
