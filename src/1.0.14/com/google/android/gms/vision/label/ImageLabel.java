package com.google.android.gms.vision.label;

/* compiled from: com.google.android.gms:play-services-vision-image-label@@18.0.3 */
public class ImageLabel {
    private final String label;
    private final String zzdo;
    private final float zzdp;

    public ImageLabel(String str, String str2, float f) {
        this.zzdo = str;
        this.label = str2;
        this.zzdp = f;
    }

    public String getMid() {
        return this.zzdo;
    }

    public String getLabel() {
        return this.label;
    }

    public float getConfidence() {
        return this.zzdp;
    }
}
