package com.google.android.gms.vision.face;

import android.content.Context;
import android.graphics.PointF;
import android.os.RemoteException;
import android.os.SystemClock;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.vision.zzbl;
import com.google.android.gms.internal.vision.zzbq;
import com.google.android.gms.internal.vision.zzbr;
import com.google.android.gms.internal.vision.zzbx;
import com.google.android.gms.internal.vision.zzbz;
import com.google.android.gms.internal.vision.zzce;
import com.google.android.gms.internal.vision.zzci;
import com.google.android.gms.internal.vision.zzea;
import com.google.android.gms.internal.vision.zzgs;
import com.google.android.gms.internal.vision.zzkf;
import com.google.android.gms.internal.vision.zzkn;
import com.google.android.gms.internal.vision.zzp;
import com.google.android.gms.vision.clearcut.DynamiteClearcutLogger;
import com.google.android.gms.vision.face.internal.client.FaceParcel;
import com.google.android.gms.vision.face.internal.client.LandmarkParcel;
import com.google.android.gms.vision.face.internal.client.zza;
import com.google.android.gms.vision.face.internal.client.zzf;
import com.google.android.gms.vision.face.internal.client.zzg;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
final class NativeFaceDetectorV2Impl extends zzg {
    private static final GmsLogger zzcr = new GmsLogger("NativeFaceDetectorV2Imp", "");
    private final long zzcs;
    private final DynamiteClearcutLogger zzct;
    private final zzbx.zzd zzcu;
    private final FaceDetectorV2Jni zzcv;

    /* access modifiers changed from: package-private */
    /* renamed from: com.google.android.gms.vision.face.NativeFaceDetectorV2Impl$1  reason: invalid class name */
    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] zzcw;
        static final /* synthetic */ int[] zzcx;

        /* JADX WARNING: Can't wrap try/catch for region: R(59:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|23|(2:25|26)|27|(2:29|30)|31|(2:33|34)|35|(2:37|38)|39|41|42|43|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|80) */
        /* JADX WARNING: Can't wrap try/catch for region: R(61:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|(2:33|34)|35|37|38|39|41|42|43|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|80) */
        /* JADX WARNING: Can't wrap try/catch for region: R(62:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|(2:33|34)|35|37|38|39|41|42|43|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|80) */
        /* JADX WARNING: Can't wrap try/catch for region: R(63:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|33|34|35|37|38|39|41|42|43|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|80) */
        /* JADX WARNING: Can't wrap try/catch for region: R(64:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|33|34|35|37|38|39|41|42|43|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|80) */
        /* JADX WARNING: Can't wrap try/catch for region: R(65:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|29|30|31|33|34|35|37|38|39|41|42|43|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|80) */
        /* JADX WARNING: Can't wrap try/catch for region: R(66:0|(2:1|2)|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|29|30|31|33|34|35|37|38|39|41|42|43|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|80) */
        /* JADX WARNING: Can't wrap try/catch for region: R(68:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|25|26|27|29|30|31|33|34|35|37|38|39|41|42|43|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|80) */
        /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x0090 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x009c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00a8 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x00c5 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x00cf */
        /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x00d9 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:63:0x00e3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:65:0x00ed */
        /* JADX WARNING: Missing exception handler attribute for start block: B:67:0x00f7 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:69:0x0101 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:71:0x010b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x0115 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:75:0x011f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:77:0x0129 */
        static {
            int[] iArr = new int[zzbx.zzb.zzc.values().length];
            zzcx = iArr;
            try {
                iArr[zzbx.zzb.zzc.FACE_OVAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                zzcx[zzbx.zzb.zzc.LEFT_EYEBROW_TOP.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                zzcx[zzbx.zzb.zzc.LEFT_EYEBROW_BOTTOM.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                zzcx[zzbx.zzb.zzc.RIGHT_EYEBROW_TOP.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                zzcx[zzbx.zzb.zzc.RIGHT_EYEBROW_BOTTOM.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                zzcx[zzbx.zzb.zzc.LEFT_EYE.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                zzcx[zzbx.zzb.zzc.RIGHT_EYE.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                zzcx[zzbx.zzb.zzc.UPPER_LIP_TOP.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                zzcx[zzbx.zzb.zzc.UPPER_LIP_BOTTOM.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                zzcx[zzbx.zzb.zzc.LOWER_LIP_TOP.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                zzcx[zzbx.zzb.zzc.LOWER_LIP_BOTTOM.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            zzcx[zzbx.zzb.zzc.NOSE_BRIDGE.ordinal()] = 12;
            zzcx[zzbx.zzb.zzc.NOSE_BOTTOM.ordinal()] = 13;
            zzcx[zzbx.zzb.zzc.LEFT_CHEEK_CENTER.ordinal()] = 14;
            zzcx[zzbx.zzb.zzc.RIGHT_CHEEK_CENTER.ordinal()] = 15;
            int[] iArr2 = new int[zzkf.zze.zza.values().length];
            zzcw = iArr2;
            iArr2[zzkf.zze.zza.LEFT_EYE.ordinal()] = 1;
            zzcw[zzkf.zze.zza.RIGHT_EYE.ordinal()] = 2;
            zzcw[zzkf.zze.zza.NOSE_TIP.ordinal()] = 3;
            zzcw[zzkf.zze.zza.LOWER_LIP.ordinal()] = 4;
            zzcw[zzkf.zze.zza.MOUTH_LEFT.ordinal()] = 5;
            zzcw[zzkf.zze.zza.MOUTH_RIGHT.ordinal()] = 6;
            zzcw[zzkf.zze.zza.LEFT_EAR_TRAGION.ordinal()] = 7;
            zzcw[zzkf.zze.zza.RIGHT_EAR_TRAGION.ordinal()] = 8;
            zzcw[zzkf.zze.zza.LEFT_CHEEK_CENTER.ordinal()] = 9;
            zzcw[zzkf.zze.zza.RIGHT_CHEEK_CENTER.ordinal()] = 10;
            zzcw[zzkf.zze.zza.LEFT_EAR_TOP.ordinal()] = 11;
            zzcw[zzkf.zze.zza.RIGHT_EAR_TOP.ordinal()] = 12;
        }
    }

    NativeFaceDetectorV2Impl(Context context, Context context2, DynamiteClearcutLogger dynamiteClearcutLogger, zzf zzf, FaceDetectorV2Jni faceDetectorV2Jni) {
        zzbx.zzg zzg = (zzbx.zzg) ((zzgs) zzbx.zzg.zzbv().zzj("models").zzgc());
        zzbx.zzd.zza zzf2 = zzbx.zzd.zzbp().zza(zzbx.zze.zzbr().zzh(zzg).zzi(zzg).zzj(zzg)).zza(zzbx.zza.zzbd().zzc(zzg).zzd(zzg)).zza(zzbx.zzf.zzbt().zzk(zzg).zzl(zzg).zzm(zzg).zzn(zzg)).zzd(zzf.zzco).zze(zzf.trackingEnabled).zze(zzf.proportionalMinFaceSize).zzf(true);
        int i = zzf.mode;
        if (i == 0) {
            zzf2.zzb(zzci.FAST);
        } else if (i == 1) {
            zzf2.zzb(zzci.ACCURATE);
        } else if (i == 2) {
            zzf2.zzb(zzci.SELFIE);
        }
        int i2 = zzf.landmarkType;
        if (i2 == 0) {
            zzf2.zzb(zzce.NO_LANDMARK);
        } else if (i2 == 1) {
            zzf2.zzb(zzce.ALL_LANDMARKS);
        } else if (i2 == 2) {
            zzf2.zzb(zzce.CONTOUR_LANDMARKS);
        }
        int i3 = zzf.zzcp;
        if (i3 == 0) {
            zzf2.zzb(zzbz.NO_CLASSIFICATION);
        } else if (i3 == 1) {
            zzf2.zzb(zzbz.ALL_CLASSIFICATIONS);
        }
        zzbx.zzd zzd = (zzbx.zzd) ((zzgs) zzf2.zzgc());
        this.zzcu = zzd;
        this.zzcs = faceDetectorV2Jni.zza(zzd, context2.getAssets());
        this.zzct = dynamiteClearcutLogger;
        this.zzcv = faceDetectorV2Jni;
    }

    @Override // com.google.android.gms.vision.face.internal.client.zzh
    public final boolean zzd(int i) throws RemoteException {
        return true;
    }

    @Override // com.google.android.gms.vision.face.internal.client.zzh
    public final void zzm() throws RemoteException {
        this.zzcv.zza(this.zzcs);
    }

    private static zzbr zze(int i) {
        if (i == 0) {
            return zzbr.ROTATION_0;
        }
        if (i == 1) {
            return zzbr.ROTATION_270;
        }
        if (i == 2) {
            return zzbr.ROTATION_180;
        }
        if (i == 3) {
            return zzbr.ROTATION_90;
        }
        throw new IllegalArgumentException("Unsupported rotation degree.");
    }

    @Override // com.google.android.gms.vision.face.internal.client.zzh
    public final FaceParcel[] zzc(IObjectWrapper iObjectWrapper, zzp zzp) throws RemoteException {
        zzbx.zzc zzc;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            ByteBuffer byteBuffer = (ByteBuffer) ObjectWrapper.unwrap(iObjectWrapper);
            zzbq.zza zzb = zzbq.zzai().zzj(zzp.width).zzk(zzp.height).zzb(zze(zzp.rotation)).zzb(zzbl.NV21);
            if (zzp.zzar > 0) {
                zzb.zzc(zzp.zzar * 1000);
            }
            zzbq zzbq = (zzbq) ((zzgs) zzb.zzgc());
            if (byteBuffer.isDirect()) {
                zzc = this.zzcv.zza(this.zzcs, byteBuffer, zzbq);
            } else if (!byteBuffer.hasArray() || byteBuffer.arrayOffset() != 0) {
                byte[] bArr = new byte[byteBuffer.remaining()];
                byteBuffer.get(bArr);
                zzc = this.zzcv.zza(this.zzcs, bArr, zzbq);
            } else {
                zzc = this.zzcv.zza(this.zzcs, byteBuffer.array(), zzbq);
            }
            FaceParcel[] zza = zza(zzc, this.zzcu.zzbo(), this.zzcu.zzbn());
            zza(this.zzct, zzp, zza, null, SystemClock.elapsedRealtime() - elapsedRealtime);
            return zza;
        } catch (Exception e) {
            zzcr.e("NativeFaceDetectorV2Imp", "Native face detection v2 failed", e);
            return new FaceParcel[0];
        }
    }

    @Override // com.google.android.gms.vision.face.internal.client.zzh
    public final FaceParcel[] zza(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3, int i, int i2, int i3, int i4, int i5, int i6, zzp zzp) {
        zzbx.zzc zzc;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            ByteBuffer byteBuffer = (ByteBuffer) ObjectWrapper.unwrap(iObjectWrapper);
            ByteBuffer byteBuffer2 = (ByteBuffer) ObjectWrapper.unwrap(iObjectWrapper2);
            ByteBuffer byteBuffer3 = (ByteBuffer) ObjectWrapper.unwrap(iObjectWrapper3);
            zzbq.zza zzb = zzbq.zzai().zzj(zzp.width).zzk(zzp.height).zzb(zze(zzp.rotation));
            if (zzp.zzar > 0) {
                zzb.zzc(zzp.zzar * 1000);
            }
            zzbq zzbq = (zzbq) ((zzgs) zzb.zzgc());
            if (byteBuffer.isDirect()) {
                zzc = this.zzcv.zza(this.zzcs, byteBuffer, byteBuffer2, byteBuffer3, i, i2, i3, i4, i5, i6, zzbq);
            } else if (!byteBuffer.hasArray() || byteBuffer.arrayOffset() != 0) {
                byte[] bArr = new byte[byteBuffer.remaining()];
                byteBuffer.get(bArr);
                byteBuffer.get(bArr);
                byteBuffer.get(bArr);
                zzc = this.zzcv.zza(this.zzcs, bArr, new byte[byteBuffer2.remaining()], new byte[byteBuffer3.remaining()], i, i2, i3, i4, i5, i6, zzbq);
            } else {
                byte[] bArr2 = null;
                byte[] array = (byteBuffer2 == null || !byteBuffer2.hasArray() || byteBuffer2.arrayOffset() != 0) ? null : byteBuffer2.array();
                if (byteBuffer3 != null && byteBuffer3.hasArray() && byteBuffer3.arrayOffset() == 0) {
                    bArr2 = byteBuffer3.array();
                }
                zzc = this.zzcv.zza(this.zzcs, byteBuffer.array(), array, bArr2, i, i2, i3, i4, i5, i6, zzbq);
            }
            FaceParcel[] zza = zza(zzc, this.zzcu.zzbo(), this.zzcu.zzbn());
            zza(this.zzct, zzp, zza, null, SystemClock.elapsedRealtime() - elapsedRealtime);
            return zza;
        } catch (Exception e) {
            zzcr.e("NativeFaceDetectorV2Imp", "Native face detection v2 failed", e);
            return new FaceParcel[0];
        }
    }

    private static FaceParcel[] zza(zzbx.zzc zzc, zzbz zzbz, zzce zzce) {
        float f;
        float f2;
        float f3;
        zzkn zzkn;
        LandmarkParcel[] landmarkParcelArr;
        zza[] zzaArr;
        int i;
        zzkn zzkn2;
        List<zzkf.zze> list;
        int i2;
        zzce zzce2 = zzce;
        zzkn zzbl = zzc.zzbl();
        FaceParcel[] faceParcelArr = new FaceParcel[zzbl.zzjl()];
        int i3 = 0;
        while (i3 < zzbl.zzjl()) {
            zzkf zzcc = zzbl.zzcc(i3);
            zzkf.zzb zzir = zzcc.zzir();
            float zzjc = zzir.zzjc() + ((zzir.zzje() - zzir.zzjc()) / 2.0f);
            float zzjd = zzir.zzjd() + ((zzir.zzjf() - zzir.zzjd()) / 2.0f);
            float zzje = zzir.zzje() - zzir.zzjc();
            float zzjf = zzir.zzjf() - zzir.zzjd();
            if (zzbz == zzbz.ALL_CLASSIFICATIONS) {
                float f4 = -1.0f;
                float f5 = -1.0f;
                float f6 = -1.0f;
                for (zzkf.zza zza : zzcc.zzix()) {
                    if (zza.getName().equals("joy")) {
                        f6 = zza.getConfidence();
                    } else if (zza.getName().equals("left_eye_closed")) {
                        f4 = 1.0f - zza.getConfidence();
                    } else if (zza.getName().equals("right_eye_closed")) {
                        f5 = 1.0f - zza.getConfidence();
                    }
                }
                f3 = f4;
                f2 = f5;
                f = f6;
            } else {
                f3 = -1.0f;
                f2 = -1.0f;
                f = -1.0f;
            }
            float confidence = zzcc.zzit() ? zzcc.getConfidence() : -1.0f;
            if (zzce2 == zzce.ALL_LANDMARKS) {
                List<zzkf.zze> zzis = zzcc.zzis();
                ArrayList arrayList = new ArrayList();
                int i4 = 0;
                while (i4 < zzis.size()) {
                    zzkf.zze zze = zzis.get(i4);
                    zzkf.zze.zza zzjj = zze.zzjj();
                    switch (AnonymousClass1.zzcw[zzjj.ordinal()]) {
                        case 1:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 4;
                            break;
                        case 2:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 10;
                            break;
                        case 3:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 6;
                            break;
                        case 4:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 0;
                            break;
                        case 5:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 5;
                            break;
                        case 6:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 11;
                            break;
                        case 7:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 3;
                            break;
                        case 8:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 9;
                            break;
                        case 9:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 1;
                            break;
                        case 10:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 7;
                            break;
                        case 11:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 2;
                            break;
                        case 12:
                            zzkn2 = zzbl;
                            list = zzis;
                            i2 = 8;
                            break;
                        default:
                            GmsLogger gmsLogger = zzcr;
                            String valueOf = String.valueOf(zzjj);
                            zzkn2 = zzbl;
                            list = zzis;
                            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
                            sb.append("Unknown landmark type: ");
                            sb.append(valueOf);
                            gmsLogger.d("NativeFaceDetectorV2Imp", sb.toString());
                            i2 = -1;
                            break;
                    }
                    if (i2 >= 0) {
                        arrayList.add(new LandmarkParcel(-1, zze.getX(), zze.getY(), i2));
                    }
                    i4++;
                    zzis = list;
                    zzbl = zzkn2;
                }
                zzkn = zzbl;
                landmarkParcelArr = (LandmarkParcel[]) arrayList.toArray(new LandmarkParcel[0]);
            } else {
                zzkn = zzbl;
                landmarkParcelArr = new LandmarkParcel[0];
            }
            if (zzce2 == zzce.CONTOUR_LANDMARKS) {
                List list2 = (List) zzcc.zzc(zzbx.zzir);
                zza[] zzaArr2 = new zza[list2.size()];
                int i5 = 0;
                while (i5 < list2.size()) {
                    zzbx.zzb zzb = (zzbx.zzb) list2.get(i5);
                    PointF[] pointFArr = new PointF[zzb.zzbh()];
                    int i6 = 0;
                    while (i6 < zzb.zzbh()) {
                        zzbx.zzb.C0019zzb zzb2 = zzb.zzbg().get(i6);
                        pointFArr[i6] = new PointF(zzb2.getX(), zzb2.getY());
                        i6++;
                        list2 = list2;
                    }
                    zzbx.zzb.zzc zzbf = zzb.zzbf();
                    switch (AnonymousClass1.zzcx[zzbf.ordinal()]) {
                        case 1:
                            i = 1;
                            break;
                        case 2:
                            i = 2;
                            break;
                        case 3:
                            i = 3;
                            break;
                        case 4:
                            i = 4;
                            break;
                        case 5:
                            i = 5;
                            break;
                        case 6:
                            i = 6;
                            break;
                        case 7:
                            i = 7;
                            break;
                        case 8:
                            i = 8;
                            break;
                        case 9:
                            i = 9;
                            break;
                        case 10:
                            i = 10;
                            break;
                        case 11:
                            i = 11;
                            break;
                        case 12:
                            i = 12;
                            break;
                        case 13:
                            i = 13;
                            break;
                        case 14:
                            i = 14;
                            break;
                        case 15:
                            i = 15;
                            break;
                        default:
                            GmsLogger gmsLogger2 = zzcr;
                            int zzag = zzbf.zzag();
                            StringBuilder sb2 = new StringBuilder(33);
                            sb2.append("Unknown contour type: ");
                            sb2.append(zzag);
                            gmsLogger2.e("NativeFaceDetectorV2Imp", sb2.toString());
                            i = -1;
                            break;
                    }
                    zzaArr2[i5] = new zza(pointFArr, i);
                    i5++;
                    list2 = list2;
                }
                zzaArr = zzaArr2;
            } else {
                zzaArr = new zza[0];
            }
            faceParcelArr[i3] = new FaceParcel(3, (int) zzcc.zziy(), zzjc, zzjd, zzje, zzjf, zzcc.zziv(), -zzcc.zziu(), zzcc.zziw(), landmarkParcelArr, f3, f2, f, zzaArr, confidence);
            i3++;
            zzce2 = zzce;
            zzbl = zzkn;
        }
        return faceParcelArr;
    }

    private static void zza(DynamiteClearcutLogger dynamiteClearcutLogger, zzp zzp, FaceParcel[] faceParcelArr, String str, long j) {
        if (zzp.id <= 2 || faceParcelArr.length != 0) {
            ArrayList arrayList = new ArrayList();
            for (FaceParcel faceParcel : faceParcelArr) {
                arrayList.add((zzea.zzn) ((zzgs) zzea.zzn.zzdg().zza(zzea.zzd.zzcn().zzb((zzea.zzm) ((zzgs) zzea.zzm.zzde().zzz((int) (faceParcel.centerX - (faceParcel.width / 2.0f))).zzaa((int) (faceParcel.centerY - (faceParcel.height / 2.0f))).zzgc())).zzb((zzea.zzm) ((zzgs) zzea.zzm.zzde().zzz((int) (faceParcel.centerX + (faceParcel.width / 2.0f))).zzaa((int) (faceParcel.centerY - (faceParcel.height / 2.0f))).zzgc())).zzb((zzea.zzm) ((zzgs) zzea.zzm.zzde().zzz((int) (faceParcel.centerX + (faceParcel.width / 2.0f))).zzaa((int) (faceParcel.centerY + (faceParcel.height / 2.0f))).zzgc())).zzb((zzea.zzm) ((zzgs) zzea.zzm.zzde().zzz((int) (faceParcel.centerX - (faceParcel.width / 2.0f))).zzaa((int) (faceParcel.centerY + (faceParcel.height / 2.0f))).zzgc()))).zzab(faceParcel.id).zzb((zzea.zzh) ((zzgs) zzea.zzh.zzcu().zzj(faceParcel.zzda).zzk(faceParcel.zzdb).zzl(faceParcel.zzdc).zzg(faceParcel.zzcf).zzh(faceParcel.zzcg).zzi(faceParcel.zzch).zzgc())).zzgc()));
            }
            int length = faceParcelArr.length;
            zzea.zzi.zza zzcx = zzea.zzi.zzcx();
            zzea.zzf.zza zze = zzea.zzf.zzcq().zzp("face").zzd(j).zze((long) length);
            zze.zzc(arrayList);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add((zzea.zzf) ((zzgs) zze.zzgc()));
            dynamiteClearcutLogger.zza(3, (zzea.zzo) ((zzgs) zzea.zzo.zzdi().zzb((zzea.zzi) ((zzgs) zzcx.zze(arrayList2).zza((zzea.zzj) ((zzgs) zzea.zzj.zzcz().zzi((long) zzp.height).zzh((long) zzp.width).zzj((long) zzp.id).zzk(zzp.zzar).zzgc())).zzgc())).zzgc()));
        }
    }
}
