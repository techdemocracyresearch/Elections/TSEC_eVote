package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.nio.charset.Charset;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzgq implements zzgt {
    private zzgz zzabo;
    private long zzabp;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    protected zzgq(String str) {
        this(str == null ? null : new zzgz(str));
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgt
    public final boolean zzfr() {
        return true;
    }

    private zzgq(zzgz zzgz) {
        this.zzabp = -1;
        this.zzabo = zzgz;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgt
    public final long getLength() throws IOException {
        if (this.zzabp == -1) {
            this.zzabp = zzjh.zzb(this);
        }
        return this.zzabp;
    }

    /* access modifiers changed from: protected */
    public final Charset zzfs() {
        zzgz zzgz = this.zzabo;
        if (zzgz == null || zzgz.zzfu() == null) {
            return zziw.UTF_8;
        }
        return this.zzabo.zzfu();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgt
    public final String getType() {
        zzgz zzgz = this.zzabo;
        if (zzgz == null) {
            return null;
        }
        return zzgz.zzft();
    }
}
