package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzabh;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzabn implements zzxe {
    static final zzxe zzan = new zzabn();

    private zzabn() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxe
    public final boolean zzb(int i) {
        return zzabh.zzc.zze.zzem(i) != null;
    }
}
