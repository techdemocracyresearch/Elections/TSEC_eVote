package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzxq<K> implements Iterator<Map.Entry<K, Object>> {
    private Iterator<Map.Entry<K, Object>> zzcmz;

    public zzxq(Iterator<Map.Entry<K, Object>> it) {
        this.zzcmz = it;
    }

    public final boolean hasNext() {
        return this.zzcmz.hasNext();
    }

    public final void remove() {
        this.zzcmz.remove();
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        Map.Entry<K, Object> next = this.zzcmz.next();
        return next.getValue() instanceof zzxp ? new zzxr(next) : next;
    }
}
