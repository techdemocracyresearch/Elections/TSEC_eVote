package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzwh {
    private int zzchv;
    private int zzchw;
    private boolean zzchx;

    static zzwh zza(byte[] bArr, int i, int i2, boolean z) {
        zzwj zzwj = new zzwj(bArr, 0, i2, false);
        try {
            zzwj.zzcz(i2);
            return zzwj;
        } catch (zzxk e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static int zzda(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    public static long zzv(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    public abstract int zzcz(int i) throws zzxk;

    public abstract int zztx();

    private zzwh() {
        this.zzchv = 100;
        this.zzchw = Integer.MAX_VALUE;
        this.zzchx = false;
    }
}
