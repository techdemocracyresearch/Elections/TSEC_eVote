package com.google.android.gms.internal.firebase_ml;

import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate;
import com.google.android.gms.internal.firebase_ml.zzwz;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import sun.misc.Unsafe;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzyo<T> implements zzze<T> {
    private static final int[] zzcny = new int[0];
    private static final Unsafe zzcnz = zzaac.zzxe();
    private final int[] zzcoa;
    private final Object[] zzcob;
    private final int zzcoc;
    private final int zzcod;
    private final zzyk zzcoe;
    private final boolean zzcof;
    private final boolean zzcog;
    private final boolean zzcoh;
    private final boolean zzcoi;
    private final int[] zzcoj;
    private final int zzcok;
    private final int zzcol;
    private final zzys zzcom;
    private final zzxu zzcon;
    private final zzzw<?, ?> zzcoo;
    private final zzwq<?> zzcop;
    private final zzyh zzcoq;

    private zzyo(int[] iArr, Object[] objArr, int i, int i2, zzyk zzyk, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzys zzys, zzxu zzxu, zzzw<?, ?> zzzw, zzwq<?> zzwq, zzyh zzyh) {
        this.zzcoa = iArr;
        this.zzcob = objArr;
        this.zzcoc = i;
        this.zzcod = i2;
        this.zzcog = zzyk instanceof zzwz;
        this.zzcoh = z;
        this.zzcof = zzwq != null && zzwq.zze(zzyk);
        this.zzcoi = false;
        this.zzcoj = iArr2;
        this.zzcok = i3;
        this.zzcol = i4;
        this.zzcom = zzys;
        this.zzcon = zzxu;
        this.zzcoo = zzzw;
        this.zzcop = zzwq;
        this.zzcoe = zzyk;
        this.zzcoq = zzyh;
    }

    static <T> zzyo<T> zza(Class<T> cls, zzyi zzyi, zzys zzys, zzxu zzxu, zzzw<?, ?> zzzw, zzwq<?> zzwq, zzyh zzyh) {
        int i;
        int i2;
        int[] iArr;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        String str;
        Object[] objArr;
        int i12;
        int i13;
        int i14;
        int i15;
        boolean z;
        int i16;
        Field field;
        int i17;
        char charAt;
        int i18;
        int i19;
        Field field2;
        Field field3;
        int i20;
        char charAt2;
        int i21;
        char charAt3;
        int i22;
        char charAt4;
        int i23;
        char charAt5;
        int i24;
        char charAt6;
        int i25;
        char charAt7;
        int i26;
        char charAt8;
        int i27;
        char charAt9;
        int i28;
        char charAt10;
        int i29;
        char charAt11;
        int i30;
        char charAt12;
        int i31;
        char charAt13;
        if (zzyi instanceof zzzb) {
            zzzb zzzb = (zzzb) zzyi;
            int i32 = 0;
            boolean z2 = zzzb.zzvx() == zzwz.zzg.zzcmc;
            String zzwi = zzzb.zzwi();
            int length = zzwi.length();
            if (zzwi.charAt(0) >= 55296) {
                int i33 = 1;
                while (true) {
                    i = i33 + 1;
                    if (zzwi.charAt(i33) < 55296) {
                        break;
                    }
                    i33 = i;
                }
            } else {
                i = 1;
            }
            int i34 = i + 1;
            int charAt14 = zzwi.charAt(i);
            if (charAt14 >= 55296) {
                int i35 = charAt14 & 8191;
                int i36 = 13;
                while (true) {
                    i31 = i34 + 1;
                    charAt13 = zzwi.charAt(i34);
                    if (charAt13 < 55296) {
                        break;
                    }
                    i35 |= (charAt13 & 8191) << i36;
                    i36 += 13;
                    i34 = i31;
                }
                charAt14 = i35 | (charAt13 << i36);
                i34 = i31;
            }
            if (charAt14 == 0) {
                iArr = zzcny;
                i7 = 0;
                i6 = 0;
                i5 = 0;
                i4 = 0;
                i3 = 0;
                i2 = 0;
            } else {
                int i37 = i34 + 1;
                int charAt15 = zzwi.charAt(i34);
                if (charAt15 >= 55296) {
                    int i38 = charAt15 & 8191;
                    int i39 = 13;
                    while (true) {
                        i30 = i37 + 1;
                        charAt12 = zzwi.charAt(i37);
                        if (charAt12 < 55296) {
                            break;
                        }
                        i38 |= (charAt12 & 8191) << i39;
                        i39 += 13;
                        i37 = i30;
                    }
                    charAt15 = i38 | (charAt12 << i39);
                    i37 = i30;
                }
                int i40 = i37 + 1;
                int charAt16 = zzwi.charAt(i37);
                if (charAt16 >= 55296) {
                    int i41 = charAt16 & 8191;
                    int i42 = 13;
                    while (true) {
                        i29 = i40 + 1;
                        charAt11 = zzwi.charAt(i40);
                        if (charAt11 < 55296) {
                            break;
                        }
                        i41 |= (charAt11 & 8191) << i42;
                        i42 += 13;
                        i40 = i29;
                    }
                    charAt16 = i41 | (charAt11 << i42);
                    i40 = i29;
                }
                int i43 = i40 + 1;
                i6 = zzwi.charAt(i40);
                if (i6 >= 55296) {
                    int i44 = i6 & 8191;
                    int i45 = 13;
                    while (true) {
                        i28 = i43 + 1;
                        charAt10 = zzwi.charAt(i43);
                        if (charAt10 < 55296) {
                            break;
                        }
                        i44 |= (charAt10 & 8191) << i45;
                        i45 += 13;
                        i43 = i28;
                    }
                    i6 = i44 | (charAt10 << i45);
                    i43 = i28;
                }
                int i46 = i43 + 1;
                i5 = zzwi.charAt(i43);
                if (i5 >= 55296) {
                    int i47 = i5 & 8191;
                    int i48 = 13;
                    while (true) {
                        i27 = i46 + 1;
                        charAt9 = zzwi.charAt(i46);
                        if (charAt9 < 55296) {
                            break;
                        }
                        i47 |= (charAt9 & 8191) << i48;
                        i48 += 13;
                        i46 = i27;
                    }
                    i5 = i47 | (charAt9 << i48);
                    i46 = i27;
                }
                int i49 = i46 + 1;
                i4 = zzwi.charAt(i46);
                if (i4 >= 55296) {
                    int i50 = i4 & 8191;
                    int i51 = 13;
                    while (true) {
                        i26 = i49 + 1;
                        charAt8 = zzwi.charAt(i49);
                        if (charAt8 < 55296) {
                            break;
                        }
                        i50 |= (charAt8 & 8191) << i51;
                        i51 += 13;
                        i49 = i26;
                    }
                    i4 = i50 | (charAt8 << i51);
                    i49 = i26;
                }
                int i52 = i49 + 1;
                i3 = zzwi.charAt(i49);
                if (i3 >= 55296) {
                    int i53 = i3 & 8191;
                    int i54 = 13;
                    while (true) {
                        i25 = i52 + 1;
                        charAt7 = zzwi.charAt(i52);
                        if (charAt7 < 55296) {
                            break;
                        }
                        i53 |= (charAt7 & 8191) << i54;
                        i54 += 13;
                        i52 = i25;
                    }
                    i3 = i53 | (charAt7 << i54);
                    i52 = i25;
                }
                int i55 = i52 + 1;
                int charAt17 = zzwi.charAt(i52);
                if (charAt17 >= 55296) {
                    int i56 = charAt17 & 8191;
                    int i57 = 13;
                    while (true) {
                        i24 = i55 + 1;
                        charAt6 = zzwi.charAt(i55);
                        if (charAt6 < 55296) {
                            break;
                        }
                        i56 |= (charAt6 & 8191) << i57;
                        i57 += 13;
                        i55 = i24;
                    }
                    charAt17 = i56 | (charAt6 << i57);
                    i55 = i24;
                }
                int i58 = i55 + 1;
                i2 = zzwi.charAt(i55);
                if (i2 >= 55296) {
                    int i59 = i2 & 8191;
                    int i60 = i58;
                    int i61 = 13;
                    while (true) {
                        i23 = i60 + 1;
                        charAt5 = zzwi.charAt(i60);
                        if (charAt5 < 55296) {
                            break;
                        }
                        i59 |= (charAt5 & 8191) << i61;
                        i61 += 13;
                        i60 = i23;
                    }
                    i2 = i59 | (charAt5 << i61);
                    i58 = i23;
                }
                i7 = (charAt15 << 1) + charAt16;
                iArr = new int[(i2 + i3 + charAt17)];
                i32 = charAt15;
                i34 = i58;
            }
            Unsafe unsafe = zzcnz;
            Object[] zzwj = zzzb.zzwj();
            Class<?> cls2 = zzzb.zzvz().getClass();
            int[] iArr2 = new int[(i4 * 3)];
            Object[] objArr2 = new Object[(i4 << 1)];
            int i62 = i2 + i3;
            int i63 = i7;
            int i64 = i2;
            int i65 = i34;
            int i66 = i62;
            int i67 = 0;
            int i68 = 0;
            while (i65 < length) {
                int i69 = i65 + 1;
                int charAt18 = zzwi.charAt(i65);
                if (charAt18 >= 55296) {
                    int i70 = charAt18 & 8191;
                    int i71 = i69;
                    int i72 = 13;
                    while (true) {
                        i22 = i71 + 1;
                        charAt4 = zzwi.charAt(i71);
                        i8 = length;
                        if (charAt4 < 55296) {
                            break;
                        }
                        i70 |= (charAt4 & 8191) << i72;
                        i72 += 13;
                        i71 = i22;
                        length = i8;
                    }
                    charAt18 = i70 | (charAt4 << i72);
                    i9 = i22;
                } else {
                    i8 = length;
                    i9 = i69;
                }
                int i73 = i9 + 1;
                int charAt19 = zzwi.charAt(i9);
                if (charAt19 >= 55296) {
                    int i74 = charAt19 & 8191;
                    int i75 = i73;
                    int i76 = 13;
                    while (true) {
                        i21 = i75 + 1;
                        charAt3 = zzwi.charAt(i75);
                        i10 = i2;
                        if (charAt3 < 55296) {
                            break;
                        }
                        i74 |= (charAt3 & 8191) << i76;
                        i76 += 13;
                        i75 = i21;
                        i2 = i10;
                    }
                    charAt19 = i74 | (charAt3 << i76);
                    i11 = i21;
                } else {
                    i10 = i2;
                    i11 = i73;
                }
                int i77 = charAt19 & 255;
                if ((charAt19 & 1024) != 0) {
                    iArr[i67] = i68;
                    i67++;
                }
                if (i77 >= 51) {
                    int i78 = i11 + 1;
                    int charAt20 = zzwi.charAt(i11);
                    char c = 55296;
                    if (charAt20 >= 55296) {
                        int i79 = charAt20 & 8191;
                        int i80 = 13;
                        while (true) {
                            i20 = i78 + 1;
                            charAt2 = zzwi.charAt(i78);
                            if (charAt2 < c) {
                                break;
                            }
                            i79 |= (charAt2 & 8191) << i80;
                            i80 += 13;
                            i78 = i20;
                            c = 55296;
                        }
                        charAt20 = i79 | (charAt2 << i80);
                        i78 = i20;
                    }
                    int i81 = i77 - 51;
                    if (i81 == 9 || i81 == 17) {
                        i19 = 1;
                        objArr2[((i68 / 3) << 1) + 1] = zzwj[i63];
                        i63++;
                    } else {
                        if (i81 == 12 && !z2) {
                            objArr2[((i68 / 3) << 1) + 1] = zzwj[i63];
                            i63++;
                        }
                        i19 = 1;
                    }
                    int i82 = charAt20 << i19;
                    Object obj = zzwj[i82];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = zza(cls2, (String) obj);
                        zzwj[i82] = field2;
                    }
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i83 = i82 + 1;
                    Object obj2 = zzwj[i83];
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = zza(cls2, (String) obj2);
                        zzwj[i83] = field3;
                    }
                    str = zzwi;
                    i14 = (int) unsafe.objectFieldOffset(field3);
                    z = z2;
                    objArr = objArr2;
                    i13 = objectFieldOffset;
                    i12 = i78;
                    i15 = 0;
                } else {
                    int i84 = i63 + 1;
                    Field zza = zza(cls2, (String) zzwj[i63]);
                    if (i77 == 9 || i77 == 17) {
                        objArr2[((i68 / 3) << 1) + 1] = zza.getType();
                    } else {
                        if (i77 == 27 || i77 == 49) {
                            i18 = i84 + 1;
                            objArr2[((i68 / 3) << 1) + 1] = zzwj[i84];
                        } else if (i77 == 12 || i77 == 30 || i77 == 44) {
                            if (!z2) {
                                i18 = i84 + 1;
                                objArr2[((i68 / 3) << 1) + 1] = zzwj[i84];
                            }
                        } else if (i77 == 50) {
                            int i85 = i64 + 1;
                            iArr[i64] = i68;
                            int i86 = (i68 / 3) << 1;
                            i18 = i84 + 1;
                            objArr2[i86] = zzwj[i84];
                            if ((charAt19 & 2048) != 0) {
                                i84 = i18 + 1;
                                objArr2[i86 + 1] = zzwj[i18];
                                i64 = i85;
                            } else {
                                i64 = i85;
                            }
                        }
                        i16 = i18;
                        i13 = (int) unsafe.objectFieldOffset(zza);
                        if ((charAt19 & 4096) == 4096 || i77 > 17) {
                            str = zzwi;
                            z = z2;
                            objArr = objArr2;
                            i14 = 1048575;
                            i12 = i11;
                            i15 = 0;
                        } else {
                            int i87 = i11 + 1;
                            int charAt21 = zzwi.charAt(i11);
                            if (charAt21 >= 55296) {
                                int i88 = charAt21 & 8191;
                                int i89 = 13;
                                while (true) {
                                    i17 = i87 + 1;
                                    charAt = zzwi.charAt(i87);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    i88 |= (charAt & 8191) << i89;
                                    i89 += 13;
                                    i87 = i17;
                                }
                                charAt21 = i88 | (charAt << i89);
                                i87 = i17;
                            }
                            int i90 = (i32 << 1) + (charAt21 / 32);
                            Object obj3 = zzwj[i90];
                            str = zzwi;
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = zza(cls2, (String) obj3);
                                zzwj[i90] = field;
                            }
                            z = z2;
                            objArr = objArr2;
                            i15 = charAt21 % 32;
                            i12 = i87;
                            i14 = (int) unsafe.objectFieldOffset(field);
                        }
                        if (i77 >= 18 && i77 <= 49) {
                            iArr[i66] = i13;
                            i66++;
                        }
                        i63 = i16;
                    }
                    i16 = i84;
                    i13 = (int) unsafe.objectFieldOffset(zza);
                    if ((charAt19 & 4096) == 4096) {
                    }
                    str = zzwi;
                    z = z2;
                    objArr = objArr2;
                    i14 = 1048575;
                    i12 = i11;
                    i15 = 0;
                    iArr[i66] = i13;
                    i66++;
                    i63 = i16;
                }
                int i91 = i68 + 1;
                iArr2[i68] = charAt18;
                int i92 = i91 + 1;
                iArr2[i91] = ((charAt19 & 256) != 0 ? 268435456 : 0) | ((charAt19 & 512) != 0 ? 536870912 : 0) | (i77 << 20) | i13;
                int i93 = i92 + 1;
                iArr2[i92] = (i15 << 20) | i14;
                i65 = i12;
                i32 = i32;
                i5 = i5;
                objArr2 = objArr;
                i2 = i10;
                i6 = i6;
                z2 = z;
                i68 = i93;
                length = i8;
                zzwi = str;
            }
            return new zzyo<>(iArr2, objArr2, i6, i5, zzzb.zzvz(), z2, false, iArr, i2, i62, zzys, zzxu, zzzw, zzwq, zzyh);
        }
        ((zzzt) zzyi).zzvx();
        int i94 = zzwz.zzg.zzcmc;
        throw new NoSuchMethodError();
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final T newInstance() {
        return (T) this.zzcom.newInstance(this.zzcoe);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzzg.zzf(com.google.android.gms.internal.firebase_ml.zzaac.zzp(r10, r6), com.google.android.gms.internal.firebase_ml.zzaac.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzl(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzk(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzl(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzk(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzk(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzk(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzzg.zzf(com.google.android.gms.internal.firebase_ml.zzaac.zzp(r10, r6), com.google.android.gms.internal.firebase_ml.zzaac.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzzg.zzf(com.google.android.gms.internal.firebase_ml.zzaac.zzp(r10, r6), com.google.android.gms.internal.firebase_ml.zzaac.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzzg.zzf(com.google.android.gms.internal.firebase_ml.zzaac.zzp(r10, r6), com.google.android.gms.internal.firebase_ml.zzaac.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzm(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzm(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzk(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzl(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzk(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzl(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zzl(r10, r6) == com.google.android.gms.internal.firebase_ml.zzaac.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.firebase_ml.zzaac.zzn(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.firebase_ml.zzaac.zzn(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.firebase_ml.zzaac.zzo(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.firebase_ml.zzaac.zzo(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzzg.zzf(com.google.android.gms.internal.firebase_ml.zzaac.zzp(r10, r6), com.google.android.gms.internal.firebase_ml.zzaac.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final boolean equals(T t, T t2) {
        int length = this.zzcoa.length;
        int i = 0;
        while (true) {
            boolean z = true;
            if (i < length) {
                int zzdw = zzdw(i);
                long j = (long) (zzdw & 1048575);
                switch ((zzdw & 267386880) >>> 20) {
                    case 0:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 1:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 2:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 3:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 4:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 5:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 6:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 7:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 8:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 9:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 10:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 11:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 12:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 13:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 14:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 15:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 16:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 17:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        z = zzzg.zzf(zzaac.zzp(t, j), zzaac.zzp(t2, j));
                        break;
                    case 50:
                        z = zzzg.zzf(zzaac.zzp(t, j), zzaac.zzp(t2, j));
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68:
                        long zzdx = (long) (zzdx(i) & 1048575);
                        if (zzaac.zzk(t, zzdx) == zzaac.zzk(t2, zzdx)) {
                            break;
                        }
                        z = false;
                        break;
                }
                if (!z) {
                    return false;
                }
                i += 3;
            } else if (!this.zzcoo.zzae(t).equals(this.zzcoo.zzae(t2))) {
                return false;
            } else {
                if (this.zzcof) {
                    return this.zzcop.zzo(t).equals(this.zzcop.zzo(t2));
                }
                return true;
            }
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final int hashCode(T t) {
        int i;
        int i2;
        int length = this.zzcoa.length;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4 += 3) {
            int zzdw = zzdw(i4);
            int i5 = this.zzcoa[i4];
            long j = (long) (1048575 & zzdw);
            int i6 = 37;
            switch ((zzdw & 267386880) >>> 20) {
                case 0:
                    i2 = i3 * 53;
                    i = zzxd.zzaf(Double.doubleToLongBits(zzaac.zzo(t, j)));
                    i3 = i2 + i;
                    break;
                case 1:
                    i2 = i3 * 53;
                    i = Float.floatToIntBits(zzaac.zzn(t, j));
                    i3 = i2 + i;
                    break;
                case 2:
                    i2 = i3 * 53;
                    i = zzxd.zzaf(zzaac.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 3:
                    i2 = i3 * 53;
                    i = zzxd.zzaf(zzaac.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 4:
                    i2 = i3 * 53;
                    i = zzaac.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 5:
                    i2 = i3 * 53;
                    i = zzxd.zzaf(zzaac.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 6:
                    i2 = i3 * 53;
                    i = zzaac.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 7:
                    i2 = i3 * 53;
                    i = zzxd.zzaz(zzaac.zzm(t, j));
                    i3 = i2 + i;
                    break;
                case 8:
                    i2 = i3 * 53;
                    i = ((String) zzaac.zzp(t, j)).hashCode();
                    i3 = i2 + i;
                    break;
                case 9:
                    Object zzp = zzaac.zzp(t, j);
                    if (zzp != null) {
                        i6 = zzp.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 10:
                    i2 = i3 * 53;
                    i = zzaac.zzp(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 11:
                    i2 = i3 * 53;
                    i = zzaac.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 12:
                    i2 = i3 * 53;
                    i = zzaac.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 13:
                    i2 = i3 * 53;
                    i = zzaac.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 14:
                    i2 = i3 * 53;
                    i = zzxd.zzaf(zzaac.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 15:
                    i2 = i3 * 53;
                    i = zzaac.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 16:
                    i2 = i3 * 53;
                    i = zzxd.zzaf(zzaac.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 17:
                    Object zzp2 = zzaac.zzp(t, j);
                    if (zzp2 != null) {
                        i6 = zzp2.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i2 = i3 * 53;
                    i = zzaac.zzp(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 50:
                    i2 = i3 * 53;
                    i = zzaac.zzp(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 51:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzxd.zzaf(Double.doubleToLongBits(zzf(t, j)));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = Float.floatToIntBits(zzg(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzxd.zzaf(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzxd.zzaf(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzxd.zzaf(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzxd.zzaz(zzj(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = ((String) zzaac.zzp(t, j)).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 60:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzaac.zzp(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzaac.zzp(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 65:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzxd.zzaf(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzxd.zzaf(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzaac.zzp(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
            }
        }
        int hashCode = (i3 * 53) + this.zzcoo.zzae(t).hashCode();
        return this.zzcof ? (hashCode * 53) + this.zzcop.zzo(t).hashCode() : hashCode;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final void zze(T t, T t2) {
        Objects.requireNonNull(t2);
        for (int i = 0; i < this.zzcoa.length; i += 3) {
            int zzdw = zzdw(i);
            long j = (long) (1048575 & zzdw);
            int i2 = this.zzcoa[i];
            switch ((zzdw & 267386880) >>> 20) {
                case 0:
                    if (zza(t2, i)) {
                        zzaac.zza(t, j, zzaac.zzo(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 1:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzn(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 2:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 3:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 4:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 5:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 6:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 7:
                    if (zza(t2, i)) {
                        zzaac.zza(t, j, zzaac.zzm(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 8:
                    if (zza(t2, i)) {
                        zzaac.zza(t, j, zzaac.zzp(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 9:
                    zza(t, t2, i);
                    break;
                case 10:
                    if (zza(t2, i)) {
                        zzaac.zza(t, j, zzaac.zzp(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 11:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 12:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 13:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 14:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 15:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 16:
                    if (zza(t2, i)) {
                        zzaac.zza((Object) t, j, zzaac.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 17:
                    zza(t, t2, i);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.zzcon.zza(t, t2, j);
                    break;
                case 50:
                    zzzg.zza(this.zzcoq, t, t2, j);
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (zza(t2, i2, i)) {
                        zzaac.zza(t, j, zzaac.zzp(t2, j));
                        zzb(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 60:
                    zzb(t, t2, i);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                    if (zza(t2, i2, i)) {
                        zzaac.zza(t, j, zzaac.zzp(t2, j));
                        zzb(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 68:
                    zzb(t, t2, i);
                    break;
            }
        }
        zzzg.zza(this.zzcoo, t, t2);
        if (this.zzcof) {
            zzzg.zza(this.zzcop, t, t2);
        }
    }

    private final void zza(T t, T t2, int i) {
        long zzdw = (long) (zzdw(i) & 1048575);
        if (zza(t2, i)) {
            Object zzp = zzaac.zzp(t, zzdw);
            Object zzp2 = zzaac.zzp(t2, zzdw);
            if (zzp != null && zzp2 != null) {
                zzaac.zza(t, zzdw, zzxd.zzc(zzp, zzp2));
                zzb(t, i);
            } else if (zzp2 != null) {
                zzaac.zza(t, zzdw, zzp2);
                zzb(t, i);
            }
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzdw = zzdw(i);
        int i2 = this.zzcoa[i];
        long j = (long) (zzdw & 1048575);
        if (zza(t2, i2, i)) {
            Object zzp = zzaac.zzp(t, j);
            Object zzp2 = zzaac.zzp(t2, j);
            if (zzp != null && zzp2 != null) {
                zzaac.zza(t, j, zzxd.zzc(zzp, zzp2));
                zzb(t, i2, i);
            } else if (zzp2 != null) {
                zzaac.zza(t, j, zzp2);
                zzb(t, i2, i);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final int zzaa(T t) {
        int i;
        int i2;
        long j;
        int i3;
        int zzb;
        int i4;
        int i5;
        int i6;
        int i7;
        int zzb2;
        int i8;
        int i9;
        int i10;
        int i11 = 267386880;
        if (this.zzcoh) {
            Unsafe unsafe = zzcnz;
            int i12 = 0;
            int i13 = 0;
            while (i12 < this.zzcoa.length) {
                int zzdw = zzdw(i12);
                int i14 = (zzdw & i11) >>> 20;
                int i15 = this.zzcoa[i12];
                long j2 = (long) (zzdw & 1048575);
                int i16 = (i14 < zzww.DOUBLE_LIST_PACKED.id() || i14 > zzww.SINT64_LIST_PACKED.id()) ? 0 : this.zzcoa[i12 + 2] & 1048575;
                switch (i14) {
                    case 0:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzb(i15, 0.0d);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 1:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzb(i15, 0.0f);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 2:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzd(i15, zzaac.zzl(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 3:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zze(i15, zzaac.zzl(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 4:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzl(i15, zzaac.zzk(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 5:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzg(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 6:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzo(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 7:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzb(i15, true);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 8:
                        if (zza(t, i12)) {
                            Object zzp = zzaac.zzp(t, j2);
                            if (zzp instanceof zzvv) {
                                zzb2 = zzwi.zzc(i15, (zzvv) zzp);
                                break;
                            } else {
                                zzb2 = zzwi.zzc(i15, (String) zzp);
                                break;
                            }
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 9:
                        if (zza(t, i12)) {
                            zzb2 = zzzg.zzc(i15, zzaac.zzp(t, j2), zzdt(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 10:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzc(i15, (zzvv) zzaac.zzp(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 11:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzm(i15, zzaac.zzk(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 12:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzq(i15, zzaac.zzk(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 13:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzp(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 14:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzh(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 15:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzn(i15, zzaac.zzk(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 16:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzf(i15, zzaac.zzl(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 17:
                        if (zza(t, i12)) {
                            zzb2 = zzwi.zzc(i15, (zzyk) zzaac.zzp(t, j2), zzdt(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 18:
                        zzb2 = zzzg.zzw(i15, zze(t, j2), false);
                        break;
                    case 19:
                        zzb2 = zzzg.zzv(i15, zze(t, j2), false);
                        break;
                    case 20:
                        zzb2 = zzzg.zzo(i15, zze(t, j2), false);
                        break;
                    case 21:
                        zzb2 = zzzg.zzp(i15, zze(t, j2), false);
                        break;
                    case 22:
                        zzb2 = zzzg.zzs(i15, zze(t, j2), false);
                        break;
                    case 23:
                        zzb2 = zzzg.zzw(i15, zze(t, j2), false);
                        break;
                    case 24:
                        zzb2 = zzzg.zzv(i15, zze(t, j2), false);
                        break;
                    case 25:
                        zzb2 = zzzg.zzx(i15, zze(t, j2), false);
                        break;
                    case 26:
                        zzb2 = zzzg.zzc(i15, zze(t, j2));
                        break;
                    case 27:
                        zzb2 = zzzg.zzc(i15, zze(t, j2), zzdt(i12));
                        break;
                    case 28:
                        zzb2 = zzzg.zzd(i15, zze(t, j2));
                        break;
                    case 29:
                        zzb2 = zzzg.zzt(i15, zze(t, j2), false);
                        break;
                    case 30:
                        zzb2 = zzzg.zzr(i15, zze(t, j2), false);
                        break;
                    case 31:
                        zzb2 = zzzg.zzv(i15, zze(t, j2), false);
                        break;
                    case 32:
                        zzb2 = zzzg.zzw(i15, zze(t, j2), false);
                        break;
                    case 33:
                        zzb2 = zzzg.zzu(i15, zze(t, j2), false);
                        break;
                    case 34:
                        zzb2 = zzzg.zzq(i15, zze(t, j2), false);
                        break;
                    case 35:
                        i9 = zzzg.zzq((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 36:
                        i9 = zzzg.zzp((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 37:
                        i9 = zzzg.zzi((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 38:
                        i9 = zzzg.zzj((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 39:
                        i9 = zzzg.zzm((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 40:
                        i9 = zzzg.zzq((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 41:
                        i9 = zzzg.zzp((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 42:
                        i9 = zzzg.zzr((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 43:
                        i9 = zzzg.zzn((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 44:
                        i9 = zzzg.zzl((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 45:
                        i9 = zzzg.zzp((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 46:
                        i9 = zzzg.zzq((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 47:
                        i9 = zzzg.zzo((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 48:
                        i9 = zzzg.zzk((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzcoi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzwi.zzdf(i15);
                            i8 = zzwi.zzdh(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 49:
                        zzb2 = zzzg.zzd(i15, zze(t, j2), zzdt(i12));
                        break;
                    case 50:
                        zzb2 = this.zzcoq.zzd(i15, zzaac.zzp(t, j2), zzdu(i12));
                        break;
                    case 51:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzb(i15, 0.0d);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 52:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzb(i15, 0.0f);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 53:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzd(i15, zzi(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 54:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zze(i15, zzi(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 55:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzl(i15, zzh(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 56:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzg(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 57:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzo(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 58:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzb(i15, true);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 59:
                        if (zza(t, i15, i12)) {
                            Object zzp2 = zzaac.zzp(t, j2);
                            if (zzp2 instanceof zzvv) {
                                zzb2 = zzwi.zzc(i15, (zzvv) zzp2);
                                break;
                            } else {
                                zzb2 = zzwi.zzc(i15, (String) zzp2);
                                break;
                            }
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 60:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzzg.zzc(i15, zzaac.zzp(t, j2), zzdt(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 61:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzc(i15, (zzvv) zzaac.zzp(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 62:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzm(i15, zzh(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 63:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzq(i15, zzh(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 64:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzp(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 65:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzh(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 66:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzn(i15, zzh(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 67:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzf(i15, zzi(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 68:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzwi.zzc(i15, (zzyk) zzaac.zzp(t, j2), zzdt(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    default:
                        i12 += 3;
                        i11 = 267386880;
                }
                i13 += zzb2;
                i12 += 3;
                i11 = 267386880;
            }
            return i13 + zza((zzzw) this.zzcoo, (Object) t);
        }
        Unsafe unsafe2 = zzcnz;
        int i17 = 0;
        int i18 = 1048575;
        int i19 = 0;
        for (int i20 = 0; i20 < this.zzcoa.length; i20 += 3) {
            int zzdw2 = zzdw(i20);
            int[] iArr = this.zzcoa;
            int i21 = iArr[i20];
            int i22 = (zzdw2 & 267386880) >>> 20;
            if (i22 <= 17) {
                int i23 = iArr[i20 + 2];
                int i24 = i23 & 1048575;
                i = 1 << (i23 >>> 20);
                if (i24 != i18) {
                    i19 = unsafe2.getInt(t, (long) i24);
                    i18 = i24;
                }
                i2 = i23;
            } else {
                i2 = (!this.zzcoi || i22 < zzww.DOUBLE_LIST_PACKED.id() || i22 > zzww.SINT64_LIST_PACKED.id()) ? 0 : this.zzcoa[i20 + 2] & 1048575;
                i = 0;
            }
            long j3 = (long) (zzdw2 & 1048575);
            switch (i22) {
                case 0:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i17 += zzwi.zzb(i21, 0.0d);
                        break;
                    }
                    break;
                case 1:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i17 += zzwi.zzb(i21, 0.0f);
                        break;
                    }
                case 2:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzwi.zzd(i21, unsafe2.getLong(t, j3));
                        i17 += i3;
                    }
                    break;
                case 3:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzwi.zze(i21, unsafe2.getLong(t, j3));
                        i17 += i3;
                    }
                    break;
                case 4:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzwi.zzl(i21, unsafe2.getInt(t, j3));
                        i17 += i3;
                    }
                    break;
                case 5:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzwi.zzg(i21, 0);
                        i17 += i3;
                    }
                    break;
                case 6:
                    if ((i19 & i) != 0) {
                        i17 += zzwi.zzo(i21, 0);
                        j = 0;
                        break;
                    }
                    j = 0;
                case 7:
                    if ((i19 & i) != 0) {
                        zzb = zzwi.zzb(i21, true);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 8:
                    if ((i19 & i) != 0) {
                        Object object = unsafe2.getObject(t, j3);
                        zzb = object instanceof zzvv ? zzwi.zzc(i21, (zzvv) object) : zzwi.zzc(i21, (String) object);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 9:
                    if ((i19 & i) != 0) {
                        zzb = zzzg.zzc(i21, unsafe2.getObject(t, j3), zzdt(i20));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 10:
                    if ((i19 & i) != 0) {
                        zzb = zzwi.zzc(i21, (zzvv) unsafe2.getObject(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 11:
                    if ((i19 & i) != 0) {
                        zzb = zzwi.zzm(i21, unsafe2.getInt(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 12:
                    if ((i19 & i) != 0) {
                        zzb = zzwi.zzq(i21, unsafe2.getInt(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 13:
                    if ((i19 & i) != 0) {
                        i4 = zzwi.zzp(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 14:
                    if ((i19 & i) != 0) {
                        zzb = zzwi.zzh(i21, 0L);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 15:
                    if ((i19 & i) != 0) {
                        zzb = zzwi.zzn(i21, unsafe2.getInt(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 16:
                    if ((i19 & i) != 0) {
                        zzb = zzwi.zzf(i21, unsafe2.getLong(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 17:
                    if ((i19 & i) != 0) {
                        zzb = zzwi.zzc(i21, (zzyk) unsafe2.getObject(t, j3), zzdt(i20));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 18:
                    zzb = zzzg.zzw(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 19:
                    zzb = zzzg.zzv(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 20:
                    zzb = zzzg.zzo(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 21:
                    zzb = zzzg.zzp(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 22:
                    zzb = zzzg.zzs(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 23:
                    zzb = zzzg.zzw(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 24:
                    zzb = zzzg.zzv(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 25:
                    zzb = zzzg.zzx(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 26:
                    zzb = zzzg.zzc(i21, (List) unsafe2.getObject(t, j3));
                    i17 += zzb;
                    j = 0;
                    break;
                case 27:
                    zzb = zzzg.zzc(i21, (List<?>) ((List) unsafe2.getObject(t, j3)), zzdt(i20));
                    i17 += zzb;
                    j = 0;
                    break;
                case 28:
                    zzb = zzzg.zzd(i21, (List) unsafe2.getObject(t, j3));
                    i17 += zzb;
                    j = 0;
                    break;
                case 29:
                    zzb = zzzg.zzt(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 30:
                    zzb = zzzg.zzr(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 31:
                    zzb = zzzg.zzv(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 32:
                    zzb = zzzg.zzw(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 33:
                    zzb = zzzg.zzu(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 34:
                    zzb = zzzg.zzq(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 35:
                    i7 = zzzg.zzq((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 36:
                    i7 = zzzg.zzp((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 37:
                    i7 = zzzg.zzi((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 38:
                    i7 = zzzg.zzj((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 39:
                    i7 = zzzg.zzm((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 40:
                    i7 = zzzg.zzq((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 41:
                    i7 = zzzg.zzp((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 42:
                    i7 = zzzg.zzr((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 43:
                    i7 = zzzg.zzn((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 44:
                    i7 = zzzg.zzl((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 45:
                    i7 = zzzg.zzp((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 46:
                    i7 = zzzg.zzq((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 47:
                    i7 = zzzg.zzo((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 48:
                    i7 = zzzg.zzk((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzcoi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzwi.zzdf(i21);
                        i5 = zzwi.zzdh(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 49:
                    zzb = zzzg.zzd(i21, (List) unsafe2.getObject(t, j3), zzdt(i20));
                    i17 += zzb;
                    j = 0;
                    break;
                case 50:
                    zzb = this.zzcoq.zzd(i21, unsafe2.getObject(t, j3), zzdu(i20));
                    i17 += zzb;
                    j = 0;
                    break;
                case 51:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzb(i21, 0.0d);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 52:
                    if (zza(t, i21, i20)) {
                        i4 = zzwi.zzb(i21, 0.0f);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 53:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzd(i21, zzi(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 54:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zze(i21, zzi(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 55:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzl(i21, zzh(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 56:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzg(i21, 0);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 57:
                    if (zza(t, i21, i20)) {
                        i4 = zzwi.zzo(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 58:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzb(i21, true);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 59:
                    if (zza(t, i21, i20)) {
                        Object object2 = unsafe2.getObject(t, j3);
                        if (object2 instanceof zzvv) {
                            zzb = zzwi.zzc(i21, (zzvv) object2);
                        } else {
                            zzb = zzwi.zzc(i21, (String) object2);
                        }
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 60:
                    if (zza(t, i21, i20)) {
                        zzb = zzzg.zzc(i21, unsafe2.getObject(t, j3), zzdt(i20));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 61:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzc(i21, (zzvv) unsafe2.getObject(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 62:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzm(i21, zzh(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 63:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzq(i21, zzh(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 64:
                    if (zza(t, i21, i20)) {
                        i4 = zzwi.zzp(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 65:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzh(i21, 0L);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 66:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzn(i21, zzh(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 67:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzf(i21, zzi(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 68:
                    if (zza(t, i21, i20)) {
                        zzb = zzwi.zzc(i21, (zzyk) unsafe2.getObject(t, j3), zzdt(i20));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                default:
                    j = 0;
                    break;
            }
        }
        int i25 = 0;
        int zza = i17 + zza((zzzw) this.zzcoo, (Object) t);
        if (!this.zzcof) {
            return zza;
        }
        zzwr<?> zzo = this.zzcop.zzo(t);
        for (int i26 = 0; i26 < zzo.zzcil.zzwq(); i26++) {
            Map.Entry<T, Object> zzec = zzo.zzcil.zzec(i26);
            i25 += zzwr.zzb((zzwt<?>) zzec.getKey(), zzec.getValue());
        }
        for (Map.Entry<T, Object> entry : zzo.zzcil.zzwr()) {
            i25 += zzwr.zzb((zzwt<?>) entry.getKey(), entry.getValue());
        }
        return zza + i25;
    }

    private static <UT, UB> int zza(zzzw<UT, UB> zzzw, T t) {
        return zzzw.zzaa(zzzw.zzae(t));
    }

    private static List<?> zze(Object obj, long j) {
        return (List) zzaac.zzp(obj, j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0552  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a2a  */
    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final void zza(T t, zzaat zzaat) throws IOException {
        Map.Entry<?, Object> entry;
        Iterator<Map.Entry<?, Object>> it;
        int length;
        int i;
        Map.Entry<?, Object> entry2;
        Iterator<Map.Entry<?, Object>> it2;
        int length2;
        if (zzaat.zzub() == zzwz.zzg.zzcmf) {
            zza(this.zzcoo, t, zzaat);
            if (this.zzcof) {
                zzwr<?> zzo = this.zzcop.zzo(t);
                if (!zzo.zzcil.isEmpty()) {
                    it2 = zzo.descendingIterator();
                    entry2 = it2.next();
                    for (length2 = this.zzcoa.length - 3; length2 >= 0; length2 -= 3) {
                        int zzdw = zzdw(length2);
                        int i2 = this.zzcoa[length2];
                        while (entry2 != null && this.zzcop.zza(entry2) > i2) {
                            this.zzcop.zza(zzaat, entry2);
                            entry2 = it2.hasNext() ? it2.next() : null;
                        }
                        switch ((zzdw & 267386880) >>> 20) {
                            case 0:
                                if (zza(t, length2)) {
                                    zzaat.zza(i2, zzaac.zzo(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 1:
                                if (zza(t, length2)) {
                                    zzaat.zza(i2, zzaac.zzn(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 2:
                                if (zza(t, length2)) {
                                    zzaat.zzi(i2, zzaac.zzl(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 3:
                                if (zza(t, length2)) {
                                    zzaat.zza(i2, zzaac.zzl(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 4:
                                if (zza(t, length2)) {
                                    zzaat.zzh(i2, zzaac.zzk(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 5:
                                if (zza(t, length2)) {
                                    zzaat.zzc(i2, zzaac.zzl(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 6:
                                if (zza(t, length2)) {
                                    zzaat.zzk(i2, zzaac.zzk(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 7:
                                if (zza(t, length2)) {
                                    zzaat.zza(i2, zzaac.zzm(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 8:
                                if (zza(t, length2)) {
                                    zza(i2, zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat);
                                    break;
                                } else {
                                    break;
                                }
                            case 9:
                                if (zza(t, length2)) {
                                    zzaat.zza(i2, zzaac.zzp(t, (long) (zzdw & 1048575)), zzdt(length2));
                                    break;
                                } else {
                                    break;
                                }
                            case 10:
                                if (zza(t, length2)) {
                                    zzaat.zza(i2, (zzvv) zzaac.zzp(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 11:
                                if (zza(t, length2)) {
                                    zzaat.zzi(i2, zzaac.zzk(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 12:
                                if (zza(t, length2)) {
                                    zzaat.zzs(i2, zzaac.zzk(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 13:
                                if (zza(t, length2)) {
                                    zzaat.zzr(i2, zzaac.zzk(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 14:
                                if (zza(t, length2)) {
                                    zzaat.zzj(i2, zzaac.zzl(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 15:
                                if (zza(t, length2)) {
                                    zzaat.zzj(i2, zzaac.zzk(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 16:
                                if (zza(t, length2)) {
                                    zzaat.zzb(i2, zzaac.zzl(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 17:
                                if (zza(t, length2)) {
                                    zzaat.zzb(i2, zzaac.zzp(t, (long) (zzdw & 1048575)), zzdt(length2));
                                    break;
                                } else {
                                    break;
                                }
                            case 18:
                                zzzg.zza(this.zzcoa[length2], (List<Double>) ((List) zzaac.zzp(t, (long) (zzdw & 1048575))), zzaat, false);
                                break;
                            case 19:
                                zzzg.zzb(this.zzcoa[length2], (List<Float>) ((List) zzaac.zzp(t, (long) (zzdw & 1048575))), zzaat, false);
                                break;
                            case 20:
                                zzzg.zzc(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 21:
                                zzzg.zzd(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 22:
                                zzzg.zzh(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 23:
                                zzzg.zzf(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 24:
                                zzzg.zzk(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 25:
                                zzzg.zzn(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 26:
                                zzzg.zza(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat);
                                break;
                            case 27:
                                zzzg.zza(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, zzdt(length2));
                                break;
                            case 28:
                                zzzg.zzb(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat);
                                break;
                            case 29:
                                zzzg.zzi(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 30:
                                zzzg.zzm(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 31:
                                zzzg.zzl(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 32:
                                zzzg.zzg(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 33:
                                zzzg.zzj(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 34:
                                zzzg.zze(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, false);
                                break;
                            case 35:
                                zzzg.zza(this.zzcoa[length2], (List<Double>) ((List) zzaac.zzp(t, (long) (zzdw & 1048575))), zzaat, true);
                                break;
                            case 36:
                                zzzg.zzb(this.zzcoa[length2], (List<Float>) ((List) zzaac.zzp(t, (long) (zzdw & 1048575))), zzaat, true);
                                break;
                            case 37:
                                zzzg.zzc(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 38:
                                zzzg.zzd(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 39:
                                zzzg.zzh(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 40:
                                zzzg.zzf(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 41:
                                zzzg.zzk(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 42:
                                zzzg.zzn(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 43:
                                zzzg.zzi(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 44:
                                zzzg.zzm(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 45:
                                zzzg.zzl(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 46:
                                zzzg.zzg(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 47:
                                zzzg.zzj(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 48:
                                zzzg.zze(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, true);
                                break;
                            case 49:
                                zzzg.zzb(this.zzcoa[length2], (List) zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat, zzdt(length2));
                                break;
                            case 50:
                                zza(zzaat, i2, zzaac.zzp(t, (long) (zzdw & 1048575)), length2);
                                break;
                            case 51:
                                if (zza(t, i2, length2)) {
                                    zzaat.zza(i2, zzf(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 52:
                                if (zza(t, i2, length2)) {
                                    zzaat.zza(i2, zzg(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 53:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzi(i2, zzi(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 54:
                                if (zza(t, i2, length2)) {
                                    zzaat.zza(i2, zzi(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 55:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzh(i2, zzh(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 56:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzc(i2, zzi(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 57:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzk(i2, zzh(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 58:
                                if (zza(t, i2, length2)) {
                                    zzaat.zza(i2, zzj(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 59:
                                if (zza(t, i2, length2)) {
                                    zza(i2, zzaac.zzp(t, (long) (zzdw & 1048575)), zzaat);
                                    break;
                                } else {
                                    break;
                                }
                            case 60:
                                if (zza(t, i2, length2)) {
                                    zzaat.zza(i2, zzaac.zzp(t, (long) (zzdw & 1048575)), zzdt(length2));
                                    break;
                                } else {
                                    break;
                                }
                            case 61:
                                if (zza(t, i2, length2)) {
                                    zzaat.zza(i2, (zzvv) zzaac.zzp(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 62:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzi(i2, zzh(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 63:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzs(i2, zzh(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 64:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzr(i2, zzh(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 65:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzj(i2, zzi(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 66:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzj(i2, zzh(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 67:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzb(i2, zzi(t, (long) (zzdw & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 68:
                                if (zza(t, i2, length2)) {
                                    zzaat.zzb(i2, zzaac.zzp(t, (long) (zzdw & 1048575)), zzdt(length2));
                                    break;
                                } else {
                                    break;
                                }
                        }
                    }
                    while (entry2 != null) {
                        this.zzcop.zza(zzaat, entry2);
                        entry2 = it2.hasNext() ? it2.next() : null;
                    }
                }
            }
            it2 = null;
            entry2 = null;
            while (length2 >= 0) {
            }
            while (entry2 != null) {
            }
        } else if (this.zzcoh) {
            if (this.zzcof) {
                zzwr<?> zzo2 = this.zzcop.zzo(t);
                if (!zzo2.zzcil.isEmpty()) {
                    it = zzo2.iterator();
                    entry = it.next();
                    length = this.zzcoa.length;
                    for (i = 0; i < length; i += 3) {
                        int zzdw2 = zzdw(i);
                        int i3 = this.zzcoa[i];
                        while (entry != null && this.zzcop.zza(entry) <= i3) {
                            this.zzcop.zza(zzaat, entry);
                            entry = it.hasNext() ? it.next() : null;
                        }
                        switch ((zzdw2 & 267386880) >>> 20) {
                            case 0:
                                if (zza(t, i)) {
                                    zzaat.zza(i3, zzaac.zzo(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 1:
                                if (zza(t, i)) {
                                    zzaat.zza(i3, zzaac.zzn(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 2:
                                if (zza(t, i)) {
                                    zzaat.zzi(i3, zzaac.zzl(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 3:
                                if (zza(t, i)) {
                                    zzaat.zza(i3, zzaac.zzl(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 4:
                                if (zza(t, i)) {
                                    zzaat.zzh(i3, zzaac.zzk(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 5:
                                if (zza(t, i)) {
                                    zzaat.zzc(i3, zzaac.zzl(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 6:
                                if (zza(t, i)) {
                                    zzaat.zzk(i3, zzaac.zzk(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 7:
                                if (zza(t, i)) {
                                    zzaat.zza(i3, zzaac.zzm(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 8:
                                if (zza(t, i)) {
                                    zza(i3, zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat);
                                    break;
                                } else {
                                    break;
                                }
                            case 9:
                                if (zza(t, i)) {
                                    zzaat.zza(i3, zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzdt(i));
                                    break;
                                } else {
                                    break;
                                }
                            case 10:
                                if (zza(t, i)) {
                                    zzaat.zza(i3, (zzvv) zzaac.zzp(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 11:
                                if (zza(t, i)) {
                                    zzaat.zzi(i3, zzaac.zzk(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 12:
                                if (zza(t, i)) {
                                    zzaat.zzs(i3, zzaac.zzk(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 13:
                                if (zza(t, i)) {
                                    zzaat.zzr(i3, zzaac.zzk(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 14:
                                if (zza(t, i)) {
                                    zzaat.zzj(i3, zzaac.zzl(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 15:
                                if (zza(t, i)) {
                                    zzaat.zzj(i3, zzaac.zzk(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 16:
                                if (zza(t, i)) {
                                    zzaat.zzb(i3, zzaac.zzl(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 17:
                                if (zza(t, i)) {
                                    zzaat.zzb(i3, zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzdt(i));
                                    break;
                                } else {
                                    break;
                                }
                            case 18:
                                zzzg.zza(this.zzcoa[i], (List<Double>) ((List) zzaac.zzp(t, (long) (zzdw2 & 1048575))), zzaat, false);
                                break;
                            case 19:
                                zzzg.zzb(this.zzcoa[i], (List<Float>) ((List) zzaac.zzp(t, (long) (zzdw2 & 1048575))), zzaat, false);
                                break;
                            case 20:
                                zzzg.zzc(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 21:
                                zzzg.zzd(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 22:
                                zzzg.zzh(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 23:
                                zzzg.zzf(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 24:
                                zzzg.zzk(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 25:
                                zzzg.zzn(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 26:
                                zzzg.zza(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat);
                                break;
                            case 27:
                                zzzg.zza(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, zzdt(i));
                                break;
                            case 28:
                                zzzg.zzb(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat);
                                break;
                            case 29:
                                zzzg.zzi(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 30:
                                zzzg.zzm(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 31:
                                zzzg.zzl(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 32:
                                zzzg.zzg(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 33:
                                zzzg.zzj(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 34:
                                zzzg.zze(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, false);
                                break;
                            case 35:
                                zzzg.zza(this.zzcoa[i], (List<Double>) ((List) zzaac.zzp(t, (long) (zzdw2 & 1048575))), zzaat, true);
                                break;
                            case 36:
                                zzzg.zzb(this.zzcoa[i], (List<Float>) ((List) zzaac.zzp(t, (long) (zzdw2 & 1048575))), zzaat, true);
                                break;
                            case 37:
                                zzzg.zzc(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 38:
                                zzzg.zzd(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 39:
                                zzzg.zzh(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 40:
                                zzzg.zzf(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 41:
                                zzzg.zzk(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 42:
                                zzzg.zzn(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 43:
                                zzzg.zzi(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 44:
                                zzzg.zzm(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 45:
                                zzzg.zzl(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 46:
                                zzzg.zzg(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 47:
                                zzzg.zzj(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 48:
                                zzzg.zze(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, true);
                                break;
                            case 49:
                                zzzg.zzb(this.zzcoa[i], (List) zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat, zzdt(i));
                                break;
                            case 50:
                                zza(zzaat, i3, zzaac.zzp(t, (long) (zzdw2 & 1048575)), i);
                                break;
                            case 51:
                                if (zza(t, i3, i)) {
                                    zzaat.zza(i3, zzf(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 52:
                                if (zza(t, i3, i)) {
                                    zzaat.zza(i3, zzg(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 53:
                                if (zza(t, i3, i)) {
                                    zzaat.zzi(i3, zzi(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 54:
                                if (zza(t, i3, i)) {
                                    zzaat.zza(i3, zzi(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 55:
                                if (zza(t, i3, i)) {
                                    zzaat.zzh(i3, zzh(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 56:
                                if (zza(t, i3, i)) {
                                    zzaat.zzc(i3, zzi(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 57:
                                if (zza(t, i3, i)) {
                                    zzaat.zzk(i3, zzh(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 58:
                                if (zza(t, i3, i)) {
                                    zzaat.zza(i3, zzj(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 59:
                                if (zza(t, i3, i)) {
                                    zza(i3, zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzaat);
                                    break;
                                } else {
                                    break;
                                }
                            case 60:
                                if (zza(t, i3, i)) {
                                    zzaat.zza(i3, zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzdt(i));
                                    break;
                                } else {
                                    break;
                                }
                            case 61:
                                if (zza(t, i3, i)) {
                                    zzaat.zza(i3, (zzvv) zzaac.zzp(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 62:
                                if (zza(t, i3, i)) {
                                    zzaat.zzi(i3, zzh(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 63:
                                if (zza(t, i3, i)) {
                                    zzaat.zzs(i3, zzh(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 64:
                                if (zza(t, i3, i)) {
                                    zzaat.zzr(i3, zzh(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 65:
                                if (zza(t, i3, i)) {
                                    zzaat.zzj(i3, zzi(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 66:
                                if (zza(t, i3, i)) {
                                    zzaat.zzj(i3, zzh(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 67:
                                if (zza(t, i3, i)) {
                                    zzaat.zzb(i3, zzi(t, (long) (zzdw2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 68:
                                if (zza(t, i3, i)) {
                                    zzaat.zzb(i3, zzaac.zzp(t, (long) (zzdw2 & 1048575)), zzdt(i));
                                    break;
                                } else {
                                    break;
                                }
                        }
                    }
                    while (entry != null) {
                        this.zzcop.zza(zzaat, entry);
                        entry = it.hasNext() ? it.next() : null;
                    }
                    zza(this.zzcoo, t, zzaat);
                }
            }
            it = null;
            entry = null;
            length = this.zzcoa.length;
            while (i < length) {
            }
            while (entry != null) {
            }
            zza(this.zzcoo, t, zzaat);
        } else {
            zzb(t, zzaat);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:170:0x0495  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0031  */
    private final void zzb(T t, zzaat zzaat) throws IOException {
        Map.Entry<?, Object> entry;
        Iterator<Map.Entry<?, Object>> it;
        int length;
        int i;
        int i2;
        if (this.zzcof) {
            zzwr<?> zzo = this.zzcop.zzo(t);
            if (!zzo.zzcil.isEmpty()) {
                it = zzo.iterator();
                entry = it.next();
                length = this.zzcoa.length;
                Unsafe unsafe = zzcnz;
                int i3 = 1048575;
                int i4 = 0;
                for (i = 0; i < length; i += 3) {
                    int zzdw = zzdw(i);
                    int[] iArr = this.zzcoa;
                    int i5 = iArr[i];
                    int i6 = (zzdw & 267386880) >>> 20;
                    if (this.zzcoh || i6 > 17) {
                        i2 = 0;
                    } else {
                        int i7 = iArr[i + 2];
                        int i8 = i7 & 1048575;
                        if (i8 != i3) {
                            i4 = unsafe.getInt(t, (long) i8);
                            i3 = i8;
                        }
                        i2 = 1 << (i7 >>> 20);
                    }
                    while (entry != null && this.zzcop.zza(entry) <= i5) {
                        this.zzcop.zza(zzaat, entry);
                        entry = it.hasNext() ? it.next() : null;
                    }
                    long j = (long) (zzdw & 1048575);
                    switch (i6) {
                        case 0:
                            if ((i2 & i4) != 0) {
                                zzaat.zza(i5, zzaac.zzo(t, j));
                                continue;
                            }
                        case 1:
                            if ((i2 & i4) != 0) {
                                zzaat.zza(i5, zzaac.zzn(t, j));
                            } else {
                                continue;
                            }
                        case 2:
                            if ((i2 & i4) != 0) {
                                zzaat.zzi(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 3:
                            if ((i2 & i4) != 0) {
                                zzaat.zza(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 4:
                            if ((i2 & i4) != 0) {
                                zzaat.zzh(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 5:
                            if ((i2 & i4) != 0) {
                                zzaat.zzc(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 6:
                            if ((i2 & i4) != 0) {
                                zzaat.zzk(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 7:
                            if ((i2 & i4) != 0) {
                                zzaat.zza(i5, zzaac.zzm(t, j));
                            } else {
                                continue;
                            }
                        case 8:
                            if ((i2 & i4) != 0) {
                                zza(i5, unsafe.getObject(t, j), zzaat);
                            } else {
                                continue;
                            }
                        case 9:
                            if ((i2 & i4) != 0) {
                                zzaat.zza(i5, unsafe.getObject(t, j), zzdt(i));
                            } else {
                                continue;
                            }
                        case 10:
                            if ((i2 & i4) != 0) {
                                zzaat.zza(i5, (zzvv) unsafe.getObject(t, j));
                            } else {
                                continue;
                            }
                        case 11:
                            if ((i2 & i4) != 0) {
                                zzaat.zzi(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 12:
                            if ((i2 & i4) != 0) {
                                zzaat.zzs(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 13:
                            if ((i2 & i4) != 0) {
                                zzaat.zzr(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 14:
                            if ((i2 & i4) != 0) {
                                zzaat.zzj(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 15:
                            if ((i2 & i4) != 0) {
                                zzaat.zzj(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 16:
                            if ((i2 & i4) != 0) {
                                zzaat.zzb(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 17:
                            if ((i2 & i4) != 0) {
                                zzaat.zzb(i5, unsafe.getObject(t, j), zzdt(i));
                            } else {
                                continue;
                            }
                        case 18:
                            zzzg.zza(this.zzcoa[i], (List<Double>) ((List) unsafe.getObject(t, j)), zzaat, false);
                            break;
                        case 19:
                            zzzg.zzb(this.zzcoa[i], (List<Float>) ((List) unsafe.getObject(t, j)), zzaat, false);
                            break;
                        case 20:
                            zzzg.zzc(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 21:
                            zzzg.zzd(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 22:
                            zzzg.zzh(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 23:
                            zzzg.zzf(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 24:
                            zzzg.zzk(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 25:
                            zzzg.zzn(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 26:
                            zzzg.zza(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat);
                            break;
                        case 27:
                            zzzg.zza(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, zzdt(i));
                            break;
                        case 28:
                            zzzg.zzb(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat);
                            break;
                        case 29:
                            zzzg.zzi(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 30:
                            zzzg.zzm(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 31:
                            zzzg.zzl(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 32:
                            zzzg.zzg(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 33:
                            zzzg.zzj(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 34:
                            zzzg.zze(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, false);
                            break;
                        case 35:
                            zzzg.zza(this.zzcoa[i], (List<Double>) ((List) unsafe.getObject(t, j)), zzaat, true);
                            break;
                        case 36:
                            zzzg.zzb(this.zzcoa[i], (List<Float>) ((List) unsafe.getObject(t, j)), zzaat, true);
                            break;
                        case 37:
                            zzzg.zzc(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 38:
                            zzzg.zzd(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 39:
                            zzzg.zzh(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 40:
                            zzzg.zzf(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 41:
                            zzzg.zzk(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 42:
                            zzzg.zzn(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 43:
                            zzzg.zzi(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 44:
                            zzzg.zzm(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 45:
                            zzzg.zzl(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 46:
                            zzzg.zzg(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 47:
                            zzzg.zzj(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 48:
                            zzzg.zze(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, true);
                            break;
                        case 49:
                            zzzg.zzb(this.zzcoa[i], (List) unsafe.getObject(t, j), zzaat, zzdt(i));
                            break;
                        case 50:
                            zza(zzaat, i5, unsafe.getObject(t, j), i);
                            break;
                        case 51:
                            if (zza(t, i5, i)) {
                                zzaat.zza(i5, zzf(t, j));
                                break;
                            }
                            break;
                        case 52:
                            if (zza(t, i5, i)) {
                                zzaat.zza(i5, zzg(t, j));
                                break;
                            }
                            break;
                        case 53:
                            if (zza(t, i5, i)) {
                                zzaat.zzi(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 54:
                            if (zza(t, i5, i)) {
                                zzaat.zza(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 55:
                            if (zza(t, i5, i)) {
                                zzaat.zzh(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 56:
                            if (zza(t, i5, i)) {
                                zzaat.zzc(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 57:
                            if (zza(t, i5, i)) {
                                zzaat.zzk(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 58:
                            if (zza(t, i5, i)) {
                                zzaat.zza(i5, zzj(t, j));
                                break;
                            }
                            break;
                        case 59:
                            if (zza(t, i5, i)) {
                                zza(i5, unsafe.getObject(t, j), zzaat);
                                break;
                            }
                            break;
                        case 60:
                            if (zza(t, i5, i)) {
                                zzaat.zza(i5, unsafe.getObject(t, j), zzdt(i));
                                break;
                            }
                            break;
                        case 61:
                            if (zza(t, i5, i)) {
                                zzaat.zza(i5, (zzvv) unsafe.getObject(t, j));
                                break;
                            }
                            break;
                        case 62:
                            if (zza(t, i5, i)) {
                                zzaat.zzi(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 63:
                            if (zza(t, i5, i)) {
                                zzaat.zzs(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 64:
                            if (zza(t, i5, i)) {
                                zzaat.zzr(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 65:
                            if (zza(t, i5, i)) {
                                zzaat.zzj(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 66:
                            if (zza(t, i5, i)) {
                                zzaat.zzj(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 67:
                            if (zza(t, i5, i)) {
                                zzaat.zzb(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 68:
                            if (zza(t, i5, i)) {
                                zzaat.zzb(i5, unsafe.getObject(t, j), zzdt(i));
                                break;
                            }
                            break;
                    }
                }
                while (entry != null) {
                    this.zzcop.zza(zzaat, entry);
                    entry = it.hasNext() ? it.next() : null;
                }
                zza(this.zzcoo, t, zzaat);
            }
        }
        it = null;
        entry = null;
        length = this.zzcoa.length;
        Unsafe unsafe2 = zzcnz;
        int i32 = 1048575;
        int i42 = 0;
        while (i < length) {
        }
        while (entry != null) {
        }
        zza(this.zzcoo, t, zzaat);
    }

    private final <K, V> void zza(zzaat zzaat, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzaat.zza(i, this.zzcoq.zzv(zzdu(i2)), this.zzcoq.zzw(obj));
        }
    }

    private static <UT, UB> void zza(zzzw<UT, UB> zzzw, T t, zzaat zzaat) throws IOException {
        zzzw.zza(zzzw.zzae(t), zzaat);
    }

    private static zzzz zzab(Object obj) {
        zzwz zzwz = (zzwz) obj;
        zzzz zzzz = zzwz.zzclj;
        if (zzzz != zzzz.zzwz()) {
            return zzzz;
        }
        zzzz zzxa = zzzz.zzxa();
        zzwz.zzclj = zzxa;
        return zzxa;
    }

    private static int zza(byte[] bArr, int i, int i2, zzaan zzaan, Class<?> cls, zzvq zzvq) throws IOException {
        switch (zzyr.zzcis[zzaan.ordinal()]) {
            case 1:
                int zzb = zzvr.zzb(bArr, i, zzvq);
                zzvq.zzchl = Boolean.valueOf(zzvq.zzchk != 0);
                return zzb;
            case 2:
                return zzvr.zze(bArr, i, zzvq);
            case 3:
                zzvq.zzchl = Double.valueOf(zzvr.zzc(bArr, i));
                return i + 8;
            case 4:
            case 5:
                zzvq.zzchl = Integer.valueOf(zzvr.zza(bArr, i));
                return i + 4;
            case 6:
            case 7:
                zzvq.zzchl = Long.valueOf(zzvr.zzb(bArr, i));
                return i + 8;
            case 8:
                zzvq.zzchl = Float.valueOf(zzvr.zzd(bArr, i));
                return i + 4;
            case 9:
            case 10:
            case 11:
                int zza = zzvr.zza(bArr, i, zzvq);
                zzvq.zzchl = Integer.valueOf(zzvq.zzchj);
                return zza;
            case 12:
            case 13:
                int zzb2 = zzvr.zzb(bArr, i, zzvq);
                zzvq.zzchl = Long.valueOf(zzvq.zzchk);
                return zzb2;
            case 14:
                return zzvr.zza(zzyz.zzwh().zzl(cls), bArr, i, i2, zzvq);
            case 15:
                int zza2 = zzvr.zza(bArr, i, zzvq);
                zzvq.zzchl = Integer.valueOf(zzwh.zzda(zzvq.zzchj));
                return zza2;
            case 16:
                int zzb3 = zzvr.zzb(bArr, i, zzvq);
                zzvq.zzchl = Long.valueOf(zzwh.zzv(zzvq.zzchk));
                return zzb3;
            case 17:
                return zzvr.zzd(bArr, i, zzvq);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x0420 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x041f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x041f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01e8  */
    private final int zza(T r16, byte[] r17, int r18, int r19, int r20, int r21, int r22, int r23, long r24, int r26, long r27, com.google.android.gms.internal.firebase_ml.zzvq r29) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1126
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_ml.zzyo.zza(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.google.android.gms.internal.firebase_ml.zzvq):int");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:30:0x003e */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:34:0x003e */
    private final <K, V> int zza(T t, byte[] bArr, int i, int i2, int i3, long j, zzvq zzvq) throws IOException {
        Unsafe unsafe = zzcnz;
        Object zzdu = zzdu(i3);
        Object object = unsafe.getObject(t, j);
        if (this.zzcoq.zzx(object)) {
            Object zzz = this.zzcoq.zzz(zzdu);
            this.zzcoq.zzd(zzz, object);
            unsafe.putObject(t, j, zzz);
            object = zzz;
        }
        zzyf<?, ?> zzv = this.zzcoq.zzv(zzdu);
        Map<?, ?> zzu = this.zzcoq.zzu(object);
        int zza = zzvr.zza(bArr, i, zzvq);
        int i4 = zzvq.zzchj;
        if (i4 < 0 || i4 > i2 - zza) {
            throw zzxk.zzve();
        }
        int i5 = i4 + zza;
        RecyclerViewAccessibilityDelegate.ItemDelegate itemDelegate = (K) zzv.zzcnt;
        RecyclerViewAccessibilityDelegate.ItemDelegate itemDelegate2 = (V) zzv.zzcnv;
        while (zza < i5) {
            int i6 = zza + 1;
            byte b = bArr[zza];
            int i7 = b;
            if (b < 0) {
                i6 = zzvr.zza(b, bArr, i6, zzvq);
                i7 = zzvq.zzchj;
            }
            int i8 = (i7 == 1 ? 1 : 0) >>> 3;
            int i9 = (i7 == 1 ? 1 : 0) & 7;
            if (i8 != 1) {
                if (i8 == 2 && i9 == zzv.zzcnu.zzxj()) {
                    zza = zza(bArr, i6, i2, zzv.zzcnu, zzv.zzcnv.getClass(), zzvq);
                    itemDelegate2 = (V) zzvq.zzchl;
                }
            } else if (i9 == zzv.zzcns.zzxj()) {
                zza = zza(bArr, i6, i2, zzv.zzcns, (Class<?>) null, zzvq);
                itemDelegate = (K) zzvq.zzchl;
            }
            zza = zzvr.zza(i7, bArr, i6, i2, zzvq);
        }
        if (zza == i5) {
            zzu.put(itemDelegate, itemDelegate2);
            return i5;
        }
        throw zzxk.zzvi();
    }

    private final int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, zzvq zzvq) throws IOException {
        int i9;
        Unsafe unsafe = zzcnz;
        long j2 = (long) (this.zzcoa[i8 + 2] & 1048575);
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Double.valueOf(zzvr.zzc(bArr, i)));
                    i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 52:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Float.valueOf(zzvr.zzd(bArr, i)));
                    i9 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 53:
            case 54:
                if (i5 == 0) {
                    i9 = zzvr.zzb(bArr, i, zzvq);
                    unsafe.putObject(t, j, Long.valueOf(zzvq.zzchk));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 55:
            case 62:
                if (i5 == 0) {
                    i9 = zzvr.zza(bArr, i, zzvq);
                    unsafe.putObject(t, j, Integer.valueOf(zzvq.zzchj));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 56:
            case 65:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Long.valueOf(zzvr.zzb(bArr, i)));
                    i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 57:
            case 64:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Integer.valueOf(zzvr.zza(bArr, i)));
                    i9 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 58:
                if (i5 == 0) {
                    i9 = zzvr.zzb(bArr, i, zzvq);
                    unsafe.putObject(t, j, Boolean.valueOf(zzvq.zzchk != 0));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 59:
                if (i5 == 2) {
                    int zza = zzvr.zza(bArr, i, zzvq);
                    int i10 = zzvq.zzchj;
                    if (i10 == 0) {
                        unsafe.putObject(t, j, "");
                    } else if ((i6 & 536870912) == 0 || zzaaf.zzf(bArr, zza, zza + i10)) {
                        unsafe.putObject(t, j, new String(bArr, zza, i10, zzxd.UTF_8));
                        zza += i10;
                    } else {
                        throw zzxk.zzvj();
                    }
                    unsafe.putInt(t, j2, i4);
                    return zza;
                }
                return i;
            case 60:
                if (i5 == 2) {
                    int zza2 = zzvr.zza(zzdt(i8), bArr, i, i2, zzvq);
                    Object object = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object == null) {
                        unsafe.putObject(t, j, zzvq.zzchl);
                    } else {
                        unsafe.putObject(t, j, zzxd.zzc(object, zzvq.zzchl));
                    }
                    unsafe.putInt(t, j2, i4);
                    return zza2;
                }
                return i;
            case 61:
                if (i5 == 2) {
                    i9 = zzvr.zze(bArr, i, zzvq);
                    unsafe.putObject(t, j, zzvq.zzchl);
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 63:
                if (i5 == 0) {
                    int zza3 = zzvr.zza(bArr, i, zzvq);
                    int i11 = zzvq.zzchj;
                    zzxe zzdv = zzdv(i8);
                    if (zzdv == null || zzdv.zzb(i11)) {
                        unsafe.putObject(t, j, Integer.valueOf(i11));
                        i9 = zza3;
                        unsafe.putInt(t, j2, i4);
                        return i9;
                    }
                    zzab(t).zzb(i3, Long.valueOf((long) i11));
                    return zza3;
                }
                return i;
            case 66:
                if (i5 == 0) {
                    i9 = zzvr.zza(bArr, i, zzvq);
                    unsafe.putObject(t, j, Integer.valueOf(zzwh.zzda(zzvq.zzchj)));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 67:
                if (i5 == 0) {
                    i9 = zzvr.zzb(bArr, i, zzvq);
                    unsafe.putObject(t, j, Long.valueOf(zzwh.zzv(zzvq.zzchk)));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 68:
                if (i5 == 3) {
                    i9 = zzvr.zza(zzdt(i8), bArr, i, i2, (i3 & -8) | 4, zzvq);
                    Object object2 = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object2 == null) {
                        unsafe.putObject(t, j, zzvq.zzchl);
                    } else {
                        unsafe.putObject(t, j, zzxd.zzc(object2, zzvq.zzchl));
                    }
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            default:
                return i;
        }
    }

    private final zzze zzdt(int i) {
        int i2 = (i / 3) << 1;
        zzze zzze = (zzze) this.zzcob[i2];
        if (zzze != null) {
            return zzze;
        }
        zzze<T> zzl = zzyz.zzwh().zzl((Class) this.zzcob[i2 + 1]);
        this.zzcob[i2] = zzl;
        return zzl;
    }

    private final Object zzdu(int i) {
        return this.zzcob[(i / 3) << 1];
    }

    private final zzxe zzdv(int i) {
        return (zzxe) this.zzcob[((i / 3) << 1) + 1];
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x03bb, code lost:
        if (r0 == r3) goto L_0x0424;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x03fe, code lost:
        if (r0 == r15) goto L_0x0424;
     */
    public final int zza(T t, byte[] bArr, int i, int i2, int i3, zzvq zzvq) throws IOException {
        Unsafe unsafe;
        int i4;
        T t2;
        zzyo<T> zzyo;
        int i5;
        int i6;
        int i7;
        zzxe zzdv;
        byte b;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        T t3;
        int i14;
        zzvq zzvq2;
        int i15;
        int i16;
        int i17;
        int i18;
        int i19;
        int i20;
        int i21;
        zzvq zzvq3;
        int i22;
        zzvq zzvq4;
        int i23;
        zzvq zzvq5;
        zzyo<T> zzyo2 = this;
        T t4 = t;
        byte[] bArr2 = bArr;
        int i24 = i2;
        int i25 = i3;
        zzvq zzvq6 = zzvq;
        Unsafe unsafe2 = zzcnz;
        int i26 = i;
        int i27 = -1;
        int i28 = 0;
        int i29 = 0;
        int i30 = 0;
        int i31 = 1048575;
        while (true) {
            if (i26 < i24) {
                int i32 = i26 + 1;
                byte b2 = bArr2[i26];
                if (b2 < 0) {
                    int zza = zzvr.zza(b2, bArr2, i32, zzvq6);
                    b = zzvq6.zzchj;
                    i32 = zza;
                } else {
                    b = b2;
                }
                int i33 = b >>> 3;
                int i34 = b & 7;
                if (i33 > i27) {
                    i8 = zzyo2.zzt(i33, i28 / 3);
                } else {
                    i8 = zzyo2.zzdy(i33);
                }
                if (i8 == -1) {
                    i9 = i33;
                    i13 = i32;
                    i12 = b;
                    i10 = i30;
                    unsafe = unsafe2;
                    i4 = i25;
                    i11 = 0;
                } else {
                    int[] iArr = zzyo2.zzcoa;
                    int i35 = iArr[i8 + 1];
                    int i36 = (i35 & 267386880) >>> 20;
                    long j = (long) (i35 & 1048575);
                    if (i36 <= 17) {
                        int i37 = iArr[i8 + 2];
                        int i38 = 1 << (i37 >>> 20);
                        int i39 = i37 & 1048575;
                        if (i39 != i31) {
                            if (i31 != 1048575) {
                                unsafe2.putInt(t4, (long) i31, i30);
                            }
                            i30 = unsafe2.getInt(t4, (long) i39);
                            i18 = i39;
                        } else {
                            i18 = i31;
                        }
                        switch (i36) {
                            case 0:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                zzvq3 = zzvq;
                                if (i34 == 1) {
                                    zzaac.zza(t4, j, zzvr.zzc(bArr2, i32));
                                    i26 = i32 + 8;
                                    i30 |= i38;
                                    i31 = i19;
                                    i29 = i12;
                                    i27 = i21;
                                    i28 = i20;
                                    zzvq6 = zzvq3;
                                    i24 = i2;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 1:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                zzvq3 = zzvq;
                                if (i34 == 5) {
                                    zzaac.zza((Object) t4, j, zzvr.zzd(bArr2, i32));
                                    i26 = i32 + 4;
                                    i30 |= i38;
                                    i31 = i19;
                                    i29 = i12;
                                    i27 = i21;
                                    i28 = i20;
                                    zzvq6 = zzvq3;
                                    i24 = i2;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 2:
                            case 3:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                zzvq3 = zzvq;
                                if (i34 == 0) {
                                    i22 = zzvr.zzb(bArr2, i32, zzvq3);
                                    unsafe2.putLong(t, j, zzvq3.zzchk);
                                    i30 |= i38;
                                    i31 = i19;
                                    i26 = i22;
                                    i29 = i12;
                                    i27 = i21;
                                    i28 = i20;
                                    zzvq6 = zzvq3;
                                    i24 = i2;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 4:
                            case 11:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                zzvq3 = zzvq;
                                if (i34 == 0) {
                                    i26 = zzvr.zza(bArr2, i32, zzvq3);
                                    unsafe2.putInt(t4, j, zzvq3.zzchj);
                                    i30 |= i38;
                                    i31 = i19;
                                    i29 = i12;
                                    i27 = i21;
                                    i28 = i20;
                                    zzvq6 = zzvq3;
                                    i24 = i2;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 5:
                            case 14:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                zzvq3 = zzvq;
                                if (i34 == 1) {
                                    unsafe2.putLong(t, j, zzvr.zzb(bArr2, i32));
                                    i26 = i32 + 8;
                                    i30 |= i38;
                                    i31 = i19;
                                    i29 = i12;
                                    i27 = i21;
                                    i28 = i20;
                                    zzvq6 = zzvq3;
                                    i24 = i2;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 6:
                            case 13:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                i23 = i2;
                                zzvq4 = zzvq;
                                if (i34 == 5) {
                                    unsafe2.putInt(t4, j, zzvr.zza(bArr2, i32));
                                    i26 = i32 + 4;
                                    i30 |= i38;
                                    i31 = i19;
                                    i29 = i12;
                                    i27 = i21;
                                    zzvq6 = zzvq4;
                                    i24 = i23;
                                    i28 = i20;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 7:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                i23 = i2;
                                zzvq4 = zzvq;
                                if (i34 == 0) {
                                    int zzb = zzvr.zzb(bArr2, i32, zzvq4);
                                    zzaac.zza(t4, j, zzvq4.zzchk != 0);
                                    i30 |= i38;
                                    i31 = i19;
                                    i26 = zzb;
                                    i29 = i12;
                                    i27 = i21;
                                    zzvq6 = zzvq4;
                                    i24 = i23;
                                    i28 = i20;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 8:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                i23 = i2;
                                zzvq4 = zzvq;
                                if (i34 == 2) {
                                    if ((i35 & 536870912) == 0) {
                                        i26 = zzvr.zzc(bArr2, i32, zzvq4);
                                    } else {
                                        i26 = zzvr.zzd(bArr2, i32, zzvq4);
                                    }
                                    unsafe2.putObject(t4, j, zzvq4.zzchl);
                                    i30 |= i38;
                                    i31 = i19;
                                    i29 = i12;
                                    i27 = i21;
                                    zzvq6 = zzvq4;
                                    i24 = i23;
                                    i28 = i20;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 9:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                zzvq4 = zzvq;
                                if (i34 == 2) {
                                    i23 = i2;
                                    i26 = zzvr.zza(zzyo2.zzdt(i20), bArr2, i32, i23, zzvq4);
                                    if ((i30 & i38) == 0) {
                                        unsafe2.putObject(t4, j, zzvq4.zzchl);
                                    } else {
                                        unsafe2.putObject(t4, j, zzxd.zzc(unsafe2.getObject(t4, j), zzvq4.zzchl));
                                    }
                                    i30 |= i38;
                                    i31 = i19;
                                    i29 = i12;
                                    i27 = i21;
                                    zzvq6 = zzvq4;
                                    i24 = i23;
                                    i28 = i20;
                                    break;
                                } else {
                                    i4 = i3;
                                    i13 = i32;
                                    i10 = i30;
                                    unsafe = unsafe2;
                                    i11 = i20;
                                    i31 = i19;
                                    i9 = i21;
                                    break;
                                }
                            case 10:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                zzvq3 = zzvq;
                                if (i34 == 2) {
                                    i26 = zzvr.zze(bArr2, i32, zzvq3);
                                    unsafe2.putObject(t4, j, zzvq3.zzchl);
                                    i30 |= i38;
                                    i31 = i19;
                                    i29 = i12;
                                    i27 = i21;
                                    i28 = i20;
                                    zzvq6 = zzvq3;
                                    i24 = i2;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 12:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                zzvq3 = zzvq;
                                if (i34 == 0) {
                                    i26 = zzvr.zza(bArr2, i32, zzvq3);
                                    int i40 = zzvq3.zzchj;
                                    zzxe zzdv2 = zzyo2.zzdv(i20);
                                    if (zzdv2 == null || zzdv2.zzb(i40)) {
                                        unsafe2.putInt(t4, j, i40);
                                        i30 |= i38;
                                        i31 = i19;
                                        i29 = i12;
                                        i27 = i21;
                                        i28 = i20;
                                        zzvq6 = zzvq3;
                                        i24 = i2;
                                        break;
                                    } else {
                                        zzab(t).zzb(i12, Long.valueOf((long) i40));
                                        i30 = i30;
                                        i29 = i12;
                                        i27 = i21;
                                        i28 = i20;
                                        zzvq6 = zzvq3;
                                        i31 = i19;
                                        i24 = i2;
                                    }
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 15:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                bArr2 = bArr;
                                zzvq3 = zzvq;
                                if (i34 == 0) {
                                    i26 = zzvr.zza(bArr2, i32, zzvq3);
                                    unsafe2.putInt(t4, j, zzwh.zzda(zzvq3.zzchj));
                                    i30 |= i38;
                                    i31 = i19;
                                    i29 = i12;
                                    i27 = i21;
                                    i28 = i20;
                                    zzvq6 = zzvq3;
                                    i24 = i2;
                                    break;
                                }
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                            case 16:
                                i21 = i33;
                                i20 = i8;
                                if (i34 == 0) {
                                    bArr2 = bArr;
                                    i22 = zzvr.zzb(bArr2, i32, zzvq);
                                    zzvq3 = zzvq;
                                    i19 = i18;
                                    i12 = b;
                                    unsafe2.putLong(t, j, zzwh.zzv(zzvq.zzchk));
                                    i30 |= i38;
                                    i31 = i19;
                                    i26 = i22;
                                    i29 = i12;
                                    i27 = i21;
                                    i28 = i20;
                                    zzvq6 = zzvq3;
                                    i24 = i2;
                                    break;
                                } else {
                                    i19 = i18;
                                    i12 = b;
                                    i4 = i3;
                                    i13 = i32;
                                    i10 = i30;
                                    unsafe = unsafe2;
                                    i11 = i20;
                                    i31 = i19;
                                    i9 = i21;
                                    break;
                                }
                            case 17:
                                if (i34 == 3) {
                                    i26 = zzvr.zza(zzyo2.zzdt(i8), bArr, i32, i2, (i33 << 3) | 4, zzvq);
                                    if ((i30 & i38) == 0) {
                                        zzvq5 = zzvq;
                                        unsafe2.putObject(t4, j, zzvq5.zzchl);
                                    } else {
                                        zzvq5 = zzvq;
                                        unsafe2.putObject(t4, j, zzxd.zzc(unsafe2.getObject(t4, j), zzvq5.zzchl));
                                    }
                                    i30 |= i38;
                                    bArr2 = bArr;
                                    i24 = i2;
                                    i29 = b;
                                    i31 = i18;
                                    i27 = i33;
                                    i28 = i8;
                                    i25 = i3;
                                    zzvq6 = zzvq5;
                                    break;
                                } else {
                                    i21 = i33;
                                    i20 = i8;
                                    i19 = i18;
                                    i12 = b;
                                    i4 = i3;
                                    i13 = i32;
                                    i10 = i30;
                                    unsafe = unsafe2;
                                    i11 = i20;
                                    i31 = i19;
                                    i9 = i21;
                                    break;
                                }
                            default:
                                i21 = i33;
                                i20 = i8;
                                i19 = i18;
                                i12 = b;
                                i4 = i3;
                                i13 = i32;
                                i10 = i30;
                                unsafe = unsafe2;
                                i11 = i20;
                                i31 = i19;
                                i9 = i21;
                                break;
                        }
                    } else {
                        bArr2 = bArr;
                        if (i36 != 27) {
                            i11 = i8;
                            i10 = i30;
                            i15 = i31;
                            if (i36 <= 49) {
                                i16 = b;
                                i9 = i33;
                                unsafe = unsafe2;
                                i26 = zza(t, bArr, i32, i2, b, i33, i34, i11, (long) i35, i36, j, zzvq);
                            } else {
                                i17 = i32;
                                i16 = b;
                                i9 = i33;
                                unsafe = unsafe2;
                                if (i36 != 50) {
                                    i26 = zza(t, bArr, i17, i2, i16, i9, i34, i35, i36, j, i11, zzvq);
                                    if (i26 != i17) {
                                        zzyo2 = this;
                                        t4 = t;
                                        bArr2 = bArr;
                                        i24 = i2;
                                        zzvq6 = zzvq;
                                        i29 = i16;
                                        i27 = i9;
                                        i28 = i11;
                                        i30 = i10;
                                        i31 = i15;
                                        unsafe2 = unsafe;
                                    }
                                } else if (i34 == 2) {
                                    i26 = zza(t, bArr, i17, i2, i11, j, zzvq);
                                }
                                i4 = i3;
                                i13 = i26;
                                i12 = i16;
                                i31 = i15;
                            }
                            zzyo2 = this;
                            t4 = t;
                            bArr2 = bArr;
                            i27 = i9;
                            i24 = i2;
                            i25 = i3;
                            zzvq6 = zzvq;
                            i28 = i11;
                            i29 = i16;
                            i30 = i10;
                            i31 = i15;
                            unsafe2 = unsafe;
                        } else if (i34 == 2) {
                            zzxl zzxl = (zzxl) unsafe2.getObject(t4, j);
                            if (!zzxl.zztl()) {
                                int size = zzxl.size();
                                zzxl = zzxl.zzcv(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t4, j, zzxl);
                            }
                            i26 = zzvr.zza(zzyo2.zzdt(i8), b, bArr, i32, i2, zzxl, zzvq);
                            i25 = i3;
                            i29 = b;
                            i27 = i33;
                            zzvq6 = zzvq6;
                            i28 = i8;
                            i30 = i30;
                            i31 = i31;
                            i24 = i2;
                        } else {
                            i11 = i8;
                            i10 = i30;
                            i15 = i31;
                            i17 = i32;
                            i16 = b;
                            i9 = i33;
                            unsafe = unsafe2;
                        }
                        i4 = i3;
                        i13 = i17;
                        i12 = i16;
                        i31 = i15;
                    }
                    i25 = i3;
                }
                if (i12 != i4 || i4 == 0) {
                    if (this.zzcof) {
                        zzvq2 = zzvq;
                        if (zzvq2.zzit == zzwo.zzuc()) {
                            t3 = t;
                            i14 = i9;
                        } else if (zzvq2.zzit.zza(this.zzcoe, i9) == null) {
                            i26 = zzvr.zza(i12, bArr, i13, i2, zzab(t), zzvq);
                            t4 = t;
                            bArr2 = bArr;
                            i24 = i2;
                            i29 = i12;
                            zzyo2 = this;
                            zzvq6 = zzvq2;
                            i27 = i9;
                            i28 = i11;
                            i30 = i10;
                            unsafe2 = unsafe;
                            i25 = i4;
                        } else {
                            T t5 = t;
                            t5.zzvc();
                            zzwr<zzwz.zzf> zzwr = t5.zzclq;
                            throw new NoSuchMethodError();
                        }
                    } else {
                        t3 = t;
                        i14 = i9;
                        zzvq2 = zzvq;
                    }
                    i26 = zzvr.zza(i12, bArr, i13, i2, zzab(t), zzvq);
                    i24 = i2;
                    i29 = i12;
                    zzyo2 = this;
                    zzvq6 = zzvq2;
                    i27 = i14;
                    t4 = t3;
                    i28 = i11;
                    i30 = i10;
                    unsafe2 = unsafe;
                    bArr2 = bArr;
                    i25 = i4;
                } else {
                    zzyo = this;
                    t2 = t;
                    i6 = i12;
                    i30 = i10;
                    i7 = 1048575;
                    i5 = i13;
                }
            } else {
                unsafe = unsafe2;
                i4 = i25;
                t2 = t4;
                zzyo = zzyo2;
                i5 = i26;
                i6 = i29;
                i7 = 1048575;
            }
        }
        if (i31 != i7) {
            unsafe.putInt(t2, (long) i31, i30);
        }
        Object obj = null;
        for (int i41 = zzyo.zzcok; i41 < zzyo.zzcol; i41++) {
            int i42 = zzyo.zzcoj[i41];
            zzzw<UT, UB> zzzw = (zzzw<UT, UB>) zzyo.zzcoo;
            int i43 = zzyo.zzcoa[i42];
            Object zzp = zzaac.zzp(t2, (long) (zzyo.zzdw(i42) & i7));
            if (!(zzp == null || (zzdv = zzyo.zzdv(i42)) == null)) {
                obj = zza(i42, i43, (Map<K, V>) zzyo.zzcoq.zzu(zzp), zzdv, obj, zzzw);
            }
            obj = (zzzz) obj;
        }
        if (obj != null) {
            zzyo.zzcoo.zzh(t2, obj);
        }
        if (i4 == 0) {
            if (i5 != i2) {
                throw zzxk.zzvi();
            }
        } else if (i5 > i2 || i6 != i4) {
            throw zzxk.zzvi();
        }
        return i5;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v11, types: [int] */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x02dc, code lost:
        if (r0 == r4) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0323, code lost:
        if (r0 == r15) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0346, code lost:
        if (r0 == r15) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0348, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final void zza(T t, byte[] bArr, int i, int i2, zzvq zzvq) throws IOException {
        byte b;
        int i3;
        int i4;
        Unsafe unsafe;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        Unsafe unsafe2;
        int i12;
        Unsafe unsafe3;
        int i13;
        Unsafe unsafe4;
        zzyo<T> zzyo = this;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i14 = i2;
        zzvq zzvq2 = zzvq;
        if (zzyo.zzcoh) {
            Unsafe unsafe5 = zzcnz;
            int i15 = -1;
            int i16 = 1048575;
            int i17 = i;
            int i18 = -1;
            int i19 = 0;
            int i20 = 0;
            int i21 = 1048575;
            while (i17 < i14) {
                int i22 = i17 + 1;
                byte b2 = bArr2[i17];
                if (b2 < 0) {
                    i3 = zzvr.zza(b2, bArr2, i22, zzvq2);
                    b = zzvq2.zzchj;
                } else {
                    b = b2;
                    i3 = i22;
                }
                int i23 = b >>> 3;
                int i24 = b & 7;
                if (i23 > i18) {
                    i4 = zzyo.zzt(i23, i19 / 3);
                } else {
                    i4 = zzyo.zzdy(i23);
                }
                if (i4 == i15) {
                    i7 = i3;
                    i5 = i23;
                    unsafe = unsafe5;
                    i6 = 0;
                } else {
                    int[] iArr = zzyo.zzcoa;
                    int i25 = iArr[i4 + 1];
                    int i26 = (i25 & 267386880) >>> 20;
                    long j = (long) (i25 & i16);
                    if (i26 <= 17) {
                        int i27 = iArr[i4 + 2];
                        int i28 = 1 << (i27 >>> 20);
                        int i29 = i27 & 1048575;
                        if (i29 != i21) {
                            if (i21 != 1048575) {
                                long j2 = (long) i21;
                                unsafe4 = unsafe5;
                                unsafe4.putInt(t2, j2, i20);
                            } else {
                                unsafe4 = unsafe5;
                            }
                            if (i29 != 1048575) {
                                i20 = unsafe4.getInt(t2, (long) i29);
                            }
                            unsafe2 = unsafe4;
                            i21 = i29;
                        } else {
                            unsafe2 = unsafe5;
                        }
                        switch (i26) {
                            case 0:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 1) {
                                    zzaac.zza(t2, j, zzvr.zzc(bArr2, i13));
                                    i17 = i13 + 8;
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 1:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 5) {
                                    zzaac.zza((Object) t2, j, zzvr.zzd(bArr2, i13));
                                    i17 = i13 + 4;
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 2:
                            case 3:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    int zzb = zzvr.zzb(bArr2, i13, zzvq2);
                                    unsafe3.putLong(t, j, zzvq2.zzchk);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    i17 = zzb;
                                    break;
                                }
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 4:
                            case 11:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    i17 = zzvr.zza(bArr2, i13, zzvq2);
                                    unsafe3.putInt(t2, j, zzvq2.zzchj);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 5:
                            case 14:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 1) {
                                    unsafe3.putLong(t, j, zzvr.zzb(bArr2, i3));
                                    i17 = i3 + 8;
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 6:
                            case 13:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 5) {
                                    unsafe3.putInt(t2, j, zzvr.zza(bArr2, i3));
                                    i17 = i3 + 4;
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 7:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    i17 = zzvr.zzb(bArr2, i3, zzvq2);
                                    zzaac.zza(t2, j, zzvq2.zzchk != 0);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 8:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 2) {
                                    if ((i25 & 536870912) == 0) {
                                        i17 = zzvr.zzc(bArr2, i3, zzvq2);
                                    } else {
                                        i17 = zzvr.zzd(bArr2, i3, zzvq2);
                                    }
                                    unsafe3.putObject(t2, j, zzvq2.zzchl);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 9:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 2) {
                                    i17 = zzvr.zza(zzyo.zzdt(i12), bArr2, i3, i14, zzvq2);
                                    Object object = unsafe3.getObject(t2, j);
                                    if (object == null) {
                                        unsafe3.putObject(t2, j, zzvq2.zzchl);
                                    } else {
                                        unsafe3.putObject(t2, j, zzxd.zzc(object, zzvq2.zzchl));
                                    }
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 10:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 2) {
                                    i17 = zzvr.zze(bArr2, i3, zzvq2);
                                    unsafe3.putObject(t2, j, zzvq2.zzchl);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 12:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    i17 = zzvr.zza(bArr2, i3, zzvq2);
                                    unsafe3.putInt(t2, j, zzvq2.zzchj);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 15:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    i17 = zzvr.zza(bArr2, i3, zzvq2);
                                    unsafe3.putInt(t2, j, zzwh.zzda(zzvq2.zzchj));
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 16:
                                if (i24 != 0) {
                                    i5 = i23;
                                    i8 = i21;
                                    unsafe3 = unsafe2;
                                    i13 = i3;
                                    i12 = i4;
                                    i7 = i13;
                                    unsafe = unsafe3;
                                    i6 = i12;
                                    i21 = i8;
                                    break;
                                } else {
                                    int zzb2 = zzvr.zzb(bArr2, i3, zzvq2);
                                    i8 = i21;
                                    i5 = i23;
                                    unsafe2.putLong(t, j, zzwh.zzv(zzvq2.zzchk));
                                    i20 |= i28;
                                    unsafe5 = unsafe2;
                                    i19 = i4;
                                    i17 = zzb2;
                                    break;
                                }
                            default:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                        }
                    } else {
                        i5 = i23;
                        i8 = i21;
                        if (i26 != 27) {
                            i6 = i4;
                            if (i26 <= 49) {
                                i10 = i20;
                                i9 = i8;
                                unsafe = unsafe5;
                                i17 = zza(t, bArr, i3, i2, b, i5, i24, i6, (long) i25, i26, j, zzvq);
                            } else {
                                i11 = i3;
                                i10 = i20;
                                unsafe = unsafe5;
                                i9 = i8;
                                if (i26 != 50) {
                                    i17 = zza(t, bArr, i11, i2, b, i5, i24, i25, i26, j, i6, zzvq);
                                } else if (i24 == 2) {
                                    i17 = zza(t, bArr, i11, i2, i6, j, zzvq);
                                }
                            }
                            zzyo = this;
                            t2 = t;
                            bArr2 = bArr;
                            i14 = i2;
                            zzvq2 = zzvq;
                            i19 = i6;
                            i18 = i5;
                            i20 = i10;
                            i21 = i9;
                            unsafe5 = unsafe;
                            i16 = 1048575;
                            i15 = -1;
                        } else if (i24 == 2) {
                            zzxl zzxl = (zzxl) unsafe5.getObject(t2, j);
                            if (!zzxl.zztl()) {
                                int size = zzxl.size();
                                zzxl = zzxl.zzcv(size == 0 ? 10 : size << 1);
                                unsafe5.putObject(t2, j, zzxl);
                            }
                            i17 = zzvr.zza(zzyo.zzdt(i4), b, bArr, i3, i2, zzxl, zzvq);
                            unsafe5 = unsafe5;
                            i20 = i20;
                            i19 = i4;
                        } else {
                            i6 = i4;
                            i11 = i3;
                            i10 = i20;
                            unsafe = unsafe5;
                            i9 = i8;
                        }
                        i7 = i11;
                        i20 = i10;
                        i21 = i9;
                        i17 = zzvr.zza(b, bArr, i7, i2, zzab(t), zzvq);
                        zzyo = this;
                        t2 = t;
                        bArr2 = bArr;
                        i14 = i2;
                        zzvq2 = zzvq;
                        i19 = i6;
                        i18 = i5;
                        unsafe5 = unsafe;
                        i16 = 1048575;
                        i15 = -1;
                    }
                    i21 = i8;
                    i18 = i5;
                    i16 = 1048575;
                    i15 = -1;
                }
                i17 = zzvr.zza(b, bArr, i7, i2, zzab(t), zzvq);
                zzyo = this;
                t2 = t;
                bArr2 = bArr;
                i14 = i2;
                zzvq2 = zzvq;
                i19 = i6;
                i18 = i5;
                unsafe5 = unsafe;
                i16 = 1048575;
                i15 = -1;
            }
            if (i21 != 1048575) {
                unsafe5.putInt(t, (long) i21, i20);
            }
            if (i17 != i2) {
                throw zzxk.zzvi();
            }
            return;
        }
        zza(t, bArr, i, i2, 0, zzvq);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final void zzq(T t) {
        int i;
        int i2 = this.zzcok;
        while (true) {
            i = this.zzcol;
            if (i2 >= i) {
                break;
            }
            long zzdw = (long) (zzdw(this.zzcoj[i2]) & 1048575);
            Object zzp = zzaac.zzp(t, zzdw);
            if (zzp != null) {
                zzaac.zza(t, zzdw, this.zzcoq.zzy(zzp));
            }
            i2++;
        }
        int length = this.zzcoj.length;
        while (i < length) {
            this.zzcon.zzb(t, (long) this.zzcoj[i]);
            i++;
        }
        this.zzcoo.zzq(t);
        if (this.zzcof) {
            this.zzcop.zzq(t);
        }
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map<K, V> map, zzxe zzxe, UB ub, zzzw<UT, UB> zzzw) {
        zzyf<?, ?> zzv = this.zzcoq.zzv(zzdu(i));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (!zzxe.zzb(next.getValue().intValue())) {
                if (ub == null) {
                    ub = zzzw.zzwy();
                }
                zzwd zzcy = zzvv.zzcy(zzyc.zza(zzv, next.getKey(), next.getValue()));
                try {
                    zzyc.zza(zzcy.zztw(), zzv, next.getKey(), next.getValue());
                    zzzw.zza(ub, i2, zzcy.zztv());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v8, types: [com.google.android.gms.internal.firebase_ml.zzze] */
    /* JADX WARN: Type inference failed for: r1v21 */
    /* JADX WARN: Type inference failed for: r1v23, types: [com.google.android.gms.internal.firebase_ml.zzze] */
    /* JADX WARN: Type inference failed for: r1v30 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final boolean zzac(T t) {
        int i;
        int i2;
        int i3 = 1048575;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            boolean z = true;
            if (i5 >= this.zzcok) {
                return !this.zzcof || this.zzcop.zzo(t).isInitialized();
            }
            int i6 = this.zzcoj[i5];
            int i7 = this.zzcoa[i6];
            int zzdw = zzdw(i6);
            int i8 = this.zzcoa[i6 + 2];
            int i9 = i8 & 1048575;
            int i10 = 1 << (i8 >>> 20);
            if (i9 != i3) {
                if (i9 != 1048575) {
                    i4 = zzcnz.getInt(t, (long) i9);
                }
                i = i4;
                i2 = i9;
            } else {
                i2 = i3;
                i = i4;
            }
            if (((268435456 & zzdw) != 0) && !zza(t, i6, i2, i, i10)) {
                return false;
            }
            int i11 = (267386880 & zzdw) >>> 20;
            if (i11 != 9 && i11 != 17) {
                if (i11 != 27) {
                    if (i11 == 60 || i11 == 68) {
                        if (zza(t, i7, i6) && !zza(t, zzdw, zzdt(i6))) {
                            return false;
                        }
                    } else if (i11 != 49) {
                        if (i11 != 50) {
                            continue;
                        } else {
                            Map<?, ?> zzw = this.zzcoq.zzw(zzaac.zzp(t, (long) (zzdw & 1048575)));
                            if (!zzw.isEmpty()) {
                                if (this.zzcoq.zzv(zzdu(i6)).zzcnu.zzxi() == zzaaq.MESSAGE) {
                                    zzze<T> zzze = 0;
                                    Iterator<?> it = zzw.values().iterator();
                                    while (true) {
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        Object next = it.next();
                                        if (zzze == null) {
                                            zzze = zzyz.zzwh().zzl(next.getClass());
                                        }
                                        boolean zzac = zzze.zzac(next);
                                        zzze = zzze;
                                        if (!zzac) {
                                            z = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!z) {
                                return false;
                            }
                        }
                    }
                }
                List list = (List) zzaac.zzp(t, (long) (zzdw & 1048575));
                if (!list.isEmpty()) {
                    ?? zzdt = zzdt(i6);
                    int i12 = 0;
                    while (true) {
                        if (i12 >= list.size()) {
                            break;
                        } else if (!zzdt.zzac(list.get(i12))) {
                            z = false;
                            break;
                        } else {
                            i12++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (zza(t, i6, i2, i, i10) && !zza(t, zzdw, zzdt(i6))) {
                return false;
            }
            i5++;
            i3 = i2;
            i4 = i;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.google.android.gms.internal.firebase_ml.zzze */
    /* JADX WARN: Multi-variable type inference failed */
    private static boolean zza(Object obj, int i, zzze zzze) {
        return zzze.zzac(zzaac.zzp(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zzaat zzaat) throws IOException {
        if (obj instanceof String) {
            zzaat.zzb(i, (String) obj);
        } else {
            zzaat.zza(i, (zzvv) obj);
        }
    }

    private final int zzdw(int i) {
        return this.zzcoa[i + 1];
    }

    private final int zzdx(int i) {
        return this.zzcoa[i + 2];
    }

    private static <T> double zzf(T t, long j) {
        return ((Double) zzaac.zzp(t, j)).doubleValue();
    }

    private static <T> float zzg(T t, long j) {
        return ((Float) zzaac.zzp(t, j)).floatValue();
    }

    private static <T> int zzh(T t, long j) {
        return ((Integer) zzaac.zzp(t, j)).intValue();
    }

    private static <T> long zzi(T t, long j) {
        return ((Long) zzaac.zzp(t, j)).longValue();
    }

    private static <T> boolean zzj(T t, long j) {
        return ((Boolean) zzaac.zzp(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zza(t, i) == zza(t2, i);
    }

    private final boolean zza(T t, int i, int i2, int i3, int i4) {
        if (i2 == 1048575) {
            return zza(t, i);
        }
        return (i3 & i4) != 0;
    }

    private final boolean zza(T t, int i) {
        int zzdx = zzdx(i);
        long j = (long) (zzdx & 1048575);
        if (j == 1048575) {
            int zzdw = zzdw(i);
            long j2 = (long) (zzdw & 1048575);
            switch ((zzdw & 267386880) >>> 20) {
                case 0:
                    return zzaac.zzo(t, j2) != 0.0d;
                case 1:
                    return zzaac.zzn(t, j2) != 0.0f;
                case 2:
                    return zzaac.zzl(t, j2) != 0;
                case 3:
                    return zzaac.zzl(t, j2) != 0;
                case 4:
                    return zzaac.zzk(t, j2) != 0;
                case 5:
                    return zzaac.zzl(t, j2) != 0;
                case 6:
                    return zzaac.zzk(t, j2) != 0;
                case 7:
                    return zzaac.zzm(t, j2);
                case 8:
                    Object zzp = zzaac.zzp(t, j2);
                    if (zzp instanceof String) {
                        return !((String) zzp).isEmpty();
                    }
                    if (zzp instanceof zzvv) {
                        return !zzvv.zzchp.equals(zzp);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzaac.zzp(t, j2) != null;
                case 10:
                    return !zzvv.zzchp.equals(zzaac.zzp(t, j2));
                case 11:
                    return zzaac.zzk(t, j2) != 0;
                case 12:
                    return zzaac.zzk(t, j2) != 0;
                case 13:
                    return zzaac.zzk(t, j2) != 0;
                case 14:
                    return zzaac.zzl(t, j2) != 0;
                case 15:
                    return zzaac.zzk(t, j2) != 0;
                case 16:
                    return zzaac.zzl(t, j2) != 0;
                case 17:
                    return zzaac.zzp(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            return (zzaac.zzk(t, j) & (1 << (zzdx >>> 20))) != 0;
        }
    }

    private final void zzb(T t, int i) {
        int zzdx = zzdx(i);
        long j = (long) (1048575 & zzdx);
        if (j != 1048575) {
            zzaac.zza((Object) t, j, (1 << (zzdx >>> 20)) | zzaac.zzk(t, j));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzaac.zzk(t, (long) (zzdx(i2) & 1048575)) == i;
    }

    private final void zzb(T t, int i, int i2) {
        zzaac.zza((Object) t, (long) (zzdx(i2) & 1048575), i);
    }

    private final int zzdy(int i) {
        if (i < this.zzcoc || i > this.zzcod) {
            return -1;
        }
        return zzu(i, 0);
    }

    private final int zzt(int i, int i2) {
        if (i < this.zzcoc || i > this.zzcod) {
            return -1;
        }
        return zzu(i, i2);
    }

    private final int zzu(int i, int i2) {
        int length = (this.zzcoa.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.zzcoa[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }
}
