package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final /* synthetic */ class zzyr {
    static final /* synthetic */ int[] zzcis;

    /* JADX WARNING: Can't wrap try/catch for region: R(36:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0060 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0078 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0084 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0090 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00a8 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x00b4 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00c0 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
    static {
        int[] iArr = new int[zzaan.values().length];
        zzcis = iArr;
        iArr[zzaan.BOOL.ordinal()] = 1;
        zzcis[zzaan.BYTES.ordinal()] = 2;
        zzcis[zzaan.DOUBLE.ordinal()] = 3;
        zzcis[zzaan.FIXED32.ordinal()] = 4;
        zzcis[zzaan.SFIXED32.ordinal()] = 5;
        zzcis[zzaan.FIXED64.ordinal()] = 6;
        zzcis[zzaan.SFIXED64.ordinal()] = 7;
        zzcis[zzaan.FLOAT.ordinal()] = 8;
        zzcis[zzaan.ENUM.ordinal()] = 9;
        zzcis[zzaan.INT32.ordinal()] = 10;
        zzcis[zzaan.UINT32.ordinal()] = 11;
        zzcis[zzaan.INT64.ordinal()] = 12;
        zzcis[zzaan.UINT64.ordinal()] = 13;
        zzcis[zzaan.MESSAGE.ordinal()] = 14;
        zzcis[zzaan.SINT32.ordinal()] = 15;
        zzcis[zzaan.SINT64.ordinal()] = 16;
        zzcis[zzaan.STRING.ordinal()] = 17;
    }
}
