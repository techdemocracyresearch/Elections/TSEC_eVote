package com.google.android.gms.internal.firebase_ml;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzzf implements Iterator<zzwc> {
    private final ArrayDeque<zzza> zzcph;
    private zzwc zzcpi;

    private zzzf(zzvv zzvv) {
        if (zzvv instanceof zzza) {
            zzza zzza = (zzza) zzvv;
            ArrayDeque<zzza> arrayDeque = new ArrayDeque<>(zzza.zztr());
            this.zzcph = arrayDeque;
            arrayDeque.push(zzza);
            this.zzcpi = zzg(zzza.zzcoz);
            return;
        }
        this.zzcph = null;
        this.zzcpi = (zzwc) zzvv;
    }

    private final zzwc zzg(zzvv zzvv) {
        while (zzvv instanceof zzza) {
            zzza zzza = (zzza) zzvv;
            this.zzcph.push(zzza);
            zzvv = zzza.zzcoz;
        }
        return (zzwc) zzvv;
    }

    public final boolean hasNext() {
        return this.zzcpi != null;
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public final /* synthetic */ zzwc next() {
        zzwc zzwc;
        boolean z;
        zzwc zzwc2 = this.zzcpi;
        if (zzwc2 != null) {
            while (true) {
                ArrayDeque<zzza> arrayDeque = this.zzcph;
                if (arrayDeque != null && !arrayDeque.isEmpty()) {
                    zzwc = zzg(this.zzcph.pop().zzcpa);
                    if (zzwc.size() == 0) {
                        z = true;
                        continue;
                    } else {
                        z = false;
                        continue;
                    }
                    if (!z) {
                        break;
                    }
                } else {
                    zzwc = null;
                }
            }
            zzwc = null;
            this.zzcpi = zzwc;
            return zzwc2;
        }
        throw new NoSuchElementException();
    }

    /* synthetic */ zzzf(zzvv zzvv, zzzd zzzd) {
        this(zzvv);
    }
}
