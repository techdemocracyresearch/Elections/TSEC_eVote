package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionCloudTextRecognizerOptions;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzss extends zzrr<FirebaseVisionText> {
    private static final Map<zzqh<FirebaseVisionCloudTextRecognizerOptions>, zzss> zzbim = new HashMap();
    private final FirebaseVisionCloudTextRecognizerOptions zzbva;

    public static synchronized zzss zza(zzqf zzqf, FirebaseVisionCloudTextRecognizerOptions firebaseVisionCloudTextRecognizerOptions) {
        zzss zzss;
        synchronized (zzss.class) {
            Preconditions.checkNotNull(zzqf, "MlKitContext must not be null");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Persistence key must not be null");
            Preconditions.checkNotNull(firebaseVisionCloudTextRecognizerOptions, "Options must not be null");
            zzqh<FirebaseVisionCloudTextRecognizerOptions> zzj = zzqh.zzj(zzqf.getPersistenceKey(), firebaseVisionCloudTextRecognizerOptions);
            Map<zzqh<FirebaseVisionCloudTextRecognizerOptions>, zzss> map = zzbim;
            zzss = map.get(zzj);
            if (zzss == null) {
                zzss = new zzss(zzqf, firebaseVisionCloudTextRecognizerOptions);
                map.put(zzj, zzss);
            }
        }
        return zzss;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final int zzqk() {
        return 1024;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final int zzql() {
        return 768;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private zzss(zzqf zzqf, FirebaseVisionCloudTextRecognizerOptions firebaseVisionCloudTextRecognizerOptions) {
        super(zzqf, firebaseVisionCloudTextRecognizerOptions.getModelType() == 1 ? "TEXT_DETECTION" : "DOCUMENT_TEXT_DETECTION", new zzkz(), firebaseVisionCloudTextRecognizerOptions.isEnforceCertFingerprintMatch());
        this.zzbva = firebaseVisionCloudTextRecognizerOptions;
        zzqg.zza(zzqf, 1).zza(zzns.zzad.zzma(), firebaseVisionCloudTextRecognizerOptions.getModelType() == 2 ? zzod.CLOUD_DOCUMENT_TEXT_CREATE : zzod.CLOUD_TEXT_CREATE);
    }

    public final Task<FirebaseVisionText> processImage(FirebaseVisionImage firebaseVisionImage) {
        zzod zzod = zzod.CLOUD_TEXT_DETECT;
        if (this.zzbva.getModelType() == 2) {
            zzod = zzod.CLOUD_DOCUMENT_TEXT_DETECT;
        }
        zzqg.zza(this.zzbkb, 1).zza(zzns.zzad.zzma(), zzod);
        return super.zza(firebaseVisionImage);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final /* synthetic */ FirebaseVisionText zza(zzkl zzkl, float f) {
        return zzsy.zzb(zzkl.zzii(), 1.0f / f);
    }
}
