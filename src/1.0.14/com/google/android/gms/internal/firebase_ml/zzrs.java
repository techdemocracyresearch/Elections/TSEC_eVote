package com.google.android.gms.internal.firebase_ml;

import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzrs implements zzpy {
    public final List<zzku> features;
    public final zzkz imageContext;
    public final byte[] zzbrd;
    public final float zzbre;

    public zzrs(byte[] bArr, float f, List<zzku> list, zzkz zzkz) {
        this.zzbrd = bArr;
        this.zzbre = f;
        this.features = list;
        this.imageContext = zzkz;
    }
}
