package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzzl implements Iterator<Map.Entry<K, V>> {
    private int pos;
    private final /* synthetic */ zzzj zzcpt;
    private Iterator<Map.Entry<K, V>> zzcpu;

    private zzzl(zzzj zzzj) {
        this.zzcpt = zzzj;
        this.pos = zzzj.zzcpo.size();
    }

    public final boolean hasNext() {
        int i = this.pos;
        return (i > 0 && i <= this.zzcpt.zzcpo.size()) || zzwv().hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    private final Iterator<Map.Entry<K, V>> zzwv() {
        if (this.zzcpu == null) {
            this.zzcpu = this.zzcpt.zzcpr.entrySet().iterator();
        }
        return this.zzcpu;
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        if (zzwv().hasNext()) {
            return (Map.Entry) zzwv().next();
        }
        List list = this.zzcpt.zzcpo;
        int i = this.pos - 1;
        this.pos = i;
        return (Map.Entry) list.get(i);
    }

    /* synthetic */ zzzl(zzzj zzzj, zzzi zzzi) {
        this(zzzj);
    }
}
