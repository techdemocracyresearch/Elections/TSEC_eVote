package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzhh {
    static final Logger zzadf = Logger.getLogger(zzhh.class.getName());
    private static final String[] zzadg;

    /* access modifiers changed from: protected */
    public abstract zzhk zzc(String str, String str2) throws IOException;

    public final zzhb zza(zzhe zzhe) {
        return new zzhb(this, zzhe);
    }

    public boolean zzaj(String str) throws IOException {
        return Arrays.binarySearch(zzadg, str) >= 0;
    }

    static {
        String[] strArr = {"DELETE", "GET", "POST", "PUT"};
        zzadg = strArr;
        Arrays.sort(strArr);
    }
}
