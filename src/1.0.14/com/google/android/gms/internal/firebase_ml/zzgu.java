package com.google.android.gms.internal.firebase_ml;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import kotlin.text.Typography;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzgu extends zzjf {
    private static final zzjz zzabq = new zzjy("=&-_.!~*'()@:$,;/?:", false);
    private String fragment;
    private int port;
    private String zzabr;
    private String zzabs;
    private String zzabt;
    private List<String> zzabu;

    public zzgu() {
        this.port = -1;
    }

    public zzgu(String str) {
        this(zzw(str));
    }

    public zzgu(URL url) {
        this(url.getProtocol(), url.getHost(), url.getPort(), url.getPath(), url.getRef(), url.getQuery(), url.getUserInfo());
    }

    private zzgu(String str, String str2, int i, String str3, String str4, String str5, String str6) {
        this.port = -1;
        this.zzabr = str.toLowerCase(Locale.US);
        this.zzabs = str2;
        this.port = i;
        this.zzabu = zzv(str3);
        String str7 = null;
        this.fragment = str4 != null ? zzjw.zzar(str4) : null;
        if (str5 != null) {
            zzhn.zze(str5, this);
        }
        this.zzabt = str6 != null ? zzjw.zzar(str6) : str7;
    }

    public final int hashCode() {
        return zzft().hashCode();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof zzgu)) {
            return false;
        }
        return zzft().equals(((zzgu) obj).zzft());
    }

    public final String toString() {
        return zzft();
    }

    public final String zzft() {
        StringBuilder sb = new StringBuilder();
        sb.append((String) zzml.checkNotNull(this.zzabr));
        sb.append("://");
        String str = this.zzabt;
        if (str != null) {
            sb.append(zzjw.zzau(str));
            sb.append('@');
        }
        sb.append((String) zzml.checkNotNull(this.zzabs));
        int i = this.port;
        if (i != -1) {
            sb.append(':');
            sb.append(i);
        }
        String valueOf = String.valueOf(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        List<String> list = this.zzabu;
        if (list != null) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                String str2 = this.zzabu.get(i2);
                if (i2 != 0) {
                    sb2.append('/');
                }
                if (str2.length() != 0) {
                    sb2.append(zzjw.zzas(str2));
                }
            }
        }
        zza(entrySet(), sb2);
        String str3 = this.fragment;
        if (str3 != null) {
            sb2.append('#');
            sb2.append(zzabq.zzaw(str3));
        }
        String valueOf2 = String.valueOf(sb2.toString());
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    public final URL zzt(String str) {
        try {
            return new URL(zzw(zzft()), str);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public final void zzu(String str) {
        this.zzabu = zzv(null);
    }

    private static List<String> zzv(String str) {
        String str2;
        if (str == null || str.length() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        int i = 0;
        while (z) {
            int indexOf = str.indexOf(47, i);
            boolean z2 = indexOf != -1;
            if (z2) {
                str2 = str.substring(i, indexOf);
            } else {
                str2 = str.substring(i);
            }
            arrayList.add(zzjw.zzar(str2));
            i = indexOf + 1;
            z = z2;
        }
        return arrayList;
    }

    static void zza(Set<Map.Entry<String, Object>> set, StringBuilder sb) {
        boolean z = true;
        for (Map.Entry<String, Object> entry : set) {
            Object value = entry.getValue();
            if (value != null) {
                String zzav = zzjw.zzav(entry.getKey());
                if (value instanceof Collection) {
                    for (Object obj : (Collection) value) {
                        z = zza(z, sb, zzav, obj);
                    }
                } else {
                    z = zza(z, sb, zzav, value);
                }
            }
        }
    }

    private static boolean zza(boolean z, StringBuilder sb, String str, Object obj) {
        if (z) {
            z = false;
            sb.append('?');
        } else {
            sb.append(Typography.amp);
        }
        sb.append(str);
        String zzav = zzjw.zzav(obj.toString());
        if (zzav.length() != 0) {
            sb.append('=');
            sb.append(zzav);
        }
        return z;
    }

    private static URL zzw(String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf
    public final /* synthetic */ zzjf zzfd() {
        return (zzgu) clone();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf
    public final /* synthetic */ zzjf zzb(String str, Object obj) {
        return (zzgu) super.zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf, java.util.AbstractMap, java.lang.Object
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzgu zzgu = (zzgu) super.clone();
        if (this.zzabu != null) {
            zzgu.zzabu = new ArrayList(this.zzabu);
        }
        return zzgu;
    }
}
