package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzma extends zzlx {
    static final zzma zzajw = new zzma();

    private zzma() {
        super("CharMatcher.none()");
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzlw
    public final boolean zzb(char c) {
        return false;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzlw
    public final int zza(CharSequence charSequence, int i) {
        zzml.zza(i, charSequence.length(), "index");
        return -1;
    }
}
