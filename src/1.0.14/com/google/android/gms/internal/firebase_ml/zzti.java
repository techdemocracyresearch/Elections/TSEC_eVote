package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zztg;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzti implements zzxe {
    static final zzxe zzan = new zzti();

    private zzti() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxe
    public final boolean zzb(int i) {
        return zztg.zzb.zza.zzce(i) != null;
    }
}
