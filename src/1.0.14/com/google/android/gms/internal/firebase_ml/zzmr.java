package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzmr extends zzlr<String> {
    private int limit;
    private int offset = 0;
    private final zzlw zzakc;
    private final boolean zzakd;
    final CharSequence zzaki;

    protected zzmr(zzmm zzmm, CharSequence charSequence) {
        this.zzakc = zzmm.zzakc;
        this.zzakd = false;
        this.limit = zzmm.limit;
        this.zzaki = charSequence;
    }

    /* access modifiers changed from: package-private */
    public abstract int zzan(int i);

    /* access modifiers changed from: package-private */
    public abstract int zzao(int i);

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzlr
    public final /* synthetic */ String zzjc() {
        int zzan;
        int i = this.offset;
        while (true) {
            int i2 = this.offset;
            if (i2 != -1) {
                zzan = zzan(i2);
                if (zzan == -1) {
                    zzan = this.zzaki.length();
                    this.offset = -1;
                } else {
                    this.offset = zzao(zzan);
                }
                int i3 = this.offset;
                if (i3 == i) {
                    int i4 = i3 + 1;
                    this.offset = i4;
                    if (i4 > this.zzaki.length()) {
                        this.offset = -1;
                    }
                } else {
                    while (i < zzan && this.zzakc.zzb(this.zzaki.charAt(i))) {
                        i++;
                    }
                    while (zzan > i && this.zzakc.zzb(this.zzaki.charAt(zzan - 1))) {
                        zzan--;
                    }
                    if (!this.zzakd || i != zzan) {
                        int i5 = this.limit;
                    } else {
                        i = this.offset;
                    }
                }
            } else {
                zzjd();
                return null;
            }
        }
        int i52 = this.limit;
        if (i52 == 1) {
            zzan = this.zzaki.length();
            this.offset = -1;
            while (zzan > i && this.zzakc.zzb(this.zzaki.charAt(zzan - 1))) {
                zzan--;
            }
        } else {
            this.limit = i52 - 1;
        }
        return this.zzaki.subSequence(i, zzan).toString();
    }
}
