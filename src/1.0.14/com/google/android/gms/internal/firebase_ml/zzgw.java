package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.OutputStream;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzgw {
    String getName();

    void zza(zzjq zzjq, OutputStream outputStream) throws IOException;
}
