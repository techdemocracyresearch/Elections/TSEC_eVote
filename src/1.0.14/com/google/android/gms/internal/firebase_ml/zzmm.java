package com.google.android.gms.internal.firebase_ml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzmm {
    private final int limit;
    private final zzlw zzakc;
    private final boolean zzakd;
    private final zzmq zzake;

    private zzmm(zzmq zzmq) {
        this(zzmq, false, zzma.zzajw, Integer.MAX_VALUE);
    }

    private zzmm(zzmq zzmq, boolean z, zzlw zzlw, int i) {
        this.zzake = zzmq;
        this.zzakd = false;
        this.zzakc = zzlw;
        this.limit = Integer.MAX_VALUE;
    }

    public static zzmm zza(zzlw zzlw) {
        zzml.checkNotNull(zzlw);
        return new zzmm(new zzmp(zzlw));
    }

    public final List<String> zza(CharSequence charSequence) {
        zzml.checkNotNull(charSequence);
        Iterator<String> zza = this.zzake.zza(this, charSequence);
        ArrayList arrayList = new ArrayList();
        while (zza.hasNext()) {
            arrayList.add(zza.next());
        }
        return Collections.unmodifiableList(arrayList);
    }
}
