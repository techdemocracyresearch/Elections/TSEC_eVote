package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzwe implements zzwb {
    private zzwe() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzwb
    public final byte[] zzd(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    /* synthetic */ zzwe(zzvu zzvu) {
        this();
    }
}
