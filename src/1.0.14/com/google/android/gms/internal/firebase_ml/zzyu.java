package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzyu {
    private static final zzys zzcor = zzwf();
    private static final zzys zzcos = new zzyv();

    static zzys zzwd() {
        return zzcor;
    }

    static zzys zzwe() {
        return zzcos;
    }

    private static zzys zzwf() {
        try {
            return (zzys) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
