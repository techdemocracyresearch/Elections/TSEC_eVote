package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzwo {
    private static volatile boolean zzcig = false;
    private static boolean zzcih = true;
    private static volatile zzwo zzcii;
    private static final zzwo zzcij = new zzwo(true);
    private final Map<zza, zzwz.zze<?, ?>> zzcik;

    public static zzwo zzuc() {
        zzwo zzwo = zzcii;
        if (zzwo == null) {
            synchronized (zzwo.class) {
                zzwo = zzcii;
                if (zzwo == null) {
                    zzwo = zzcij;
                    zzcii = zzwo;
                }
            }
        }
        return zzwo;
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    static final class zza {
        private final int number;
        private final Object object;

        zza(Object obj, int i) {
            this.object = obj;
            this.number = i;
        }

        public final int hashCode() {
            return (System.identityHashCode(this.object) * 65535) + this.number;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.object == zza.object && this.number == zza.number) {
                return true;
            }
            return false;
        }
    }

    public final <ContainingType extends zzyk> zzwz.zze<ContainingType, ?> zza(ContainingType containingtype, int i) {
        return (zzwz.zze<ContainingType, ?>) this.zzcik.get(new zza(containingtype, i));
    }

    zzwo() {
        this.zzcik = new HashMap();
    }

    private zzwo(boolean z) {
        this.zzcik = Collections.emptyMap();
    }
}
