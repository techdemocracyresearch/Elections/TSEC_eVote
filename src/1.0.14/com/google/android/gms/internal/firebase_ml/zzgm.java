package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzgm extends zzge {
    protected zzgm(zzgl zzgl) {
        super(zzgl);
    }

    public final zzhx zzfp() {
        return ((zzhz) zzfi()).zzfp();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzge
    public final /* synthetic */ zzjm zzfi() {
        return (zzhz) super.zzfi();
    }
}
