package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzaaw implements zzxe {
    static final zzxe zzan = new zzaaw();

    private zzaaw() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxe
    public final boolean zzb(int i) {
        return zzaav.zzeg(i) != null;
    }
}
