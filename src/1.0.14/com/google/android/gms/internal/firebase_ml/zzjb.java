package com.google.android.gms.internal.firebase_ml;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzjb extends AbstractSet<Map.Entry<String, Object>> {
    private final /* synthetic */ zzja zzahg;

    zzjb(zzja zzja) {
        this.zzahg = zzja;
    }

    public final int size() {
        int i = 0;
        for (String str : this.zzahg.zzacg.zzagq) {
            if (this.zzahg.zzacg.zzao(str).zzh(this.zzahg.object) != null) {
                i++;
            }
        }
        return i;
    }

    public final void clear() {
        for (String str : this.zzahg.zzacg.zzagq) {
            this.zzahg.zzacg.zzao(str).zzb(this.zzahg.object, null);
        }
    }

    public final boolean isEmpty() {
        for (String str : this.zzahg.zzacg.zzagq) {
            if (this.zzahg.zzacg.zzao(str).zzh(this.zzahg.object) != null) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final /* synthetic */ Iterator iterator() {
        return new zzjc(this.zzahg);
    }
}
