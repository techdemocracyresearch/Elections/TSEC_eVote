package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzgi implements zzhi {
    private final /* synthetic */ zzhi zzabj;
    private final /* synthetic */ zzhc zzabk;
    private final /* synthetic */ zzgg zzabl;

    zzgi(zzgg zzgg, zzhi zzhi, zzhc zzhc) {
        this.zzabl = zzgg;
        this.zzabj = zzhi;
        this.zzabk = zzhc;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhi
    public final void zzb(zzhd zzhd) throws IOException {
        zzhi zzhi = this.zzabj;
        if (zzhi != null) {
            zzhi.zzb(zzhd);
        }
        if (!zzhd.zzgg() && this.zzabk.zzge()) {
            throw this.zzabl.zza(zzhd);
        }
    }
}
