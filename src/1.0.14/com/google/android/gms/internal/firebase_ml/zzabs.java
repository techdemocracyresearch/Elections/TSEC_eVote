package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzabh;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzabs implements zzxe {
    static final zzxe zzan = new zzabs();

    private zzabs() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxe
    public final boolean zzb(int i) {
        return zzabh.zzi.zza.zzeo(i) != null;
    }
}
