package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzzb implements zzyi {
    private final int flags;
    private final String info;
    private final Object[] zzcob;
    private final zzyk zzcoe;

    zzzb(zzyk zzyk, String str, Object[] objArr) {
        this.zzcoe = zzyk;
        this.info = str;
        this.zzcob = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.flags = charAt;
            return;
        }
        int i = charAt & 8191;
        int i2 = 13;
        int i3 = 1;
        while (true) {
            int i4 = i3 + 1;
            char charAt2 = str.charAt(i3);
            if (charAt2 >= 55296) {
                i |= (charAt2 & 8191) << i2;
                i2 += 13;
                i3 = i4;
            } else {
                this.flags = i | (charAt2 << i2);
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final String zzwi() {
        return this.info;
    }

    /* access modifiers changed from: package-private */
    public final Object[] zzwj() {
        return this.zzcob;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyi
    public final zzyk zzvz() {
        return this.zzcoe;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyi
    public final int zzvx() {
        return (this.flags & 1) == 1 ? zzwz.zzg.zzcmb : zzwz.zzg.zzcmc;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyi
    public final boolean zzvy() {
        return (this.flags & 2) == 2;
    }
}
