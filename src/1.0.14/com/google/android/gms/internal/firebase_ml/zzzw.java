package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzzw<T, B> {
    zzzw() {
    }

    /* access modifiers changed from: package-private */
    public abstract void zza(B b, int i, long j);

    /* access modifiers changed from: package-private */
    public abstract void zza(B b, int i, zzvv zzvv);

    /* access modifiers changed from: package-private */
    public abstract void zza(T t, zzaat zzaat) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract int zzaa(T t);

    /* access modifiers changed from: package-private */
    public abstract T zzae(Object obj);

    /* access modifiers changed from: package-private */
    public abstract int zzaf(T t);

    /* access modifiers changed from: package-private */
    public abstract void zzc(T t, zzaat zzaat) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zzg(Object obj, T t);

    /* access modifiers changed from: package-private */
    public abstract void zzh(Object obj, B b);

    /* access modifiers changed from: package-private */
    public abstract T zzi(T t, T t2);

    /* access modifiers changed from: package-private */
    public abstract void zzq(Object obj);

    /* access modifiers changed from: package-private */
    public abstract B zzwy();
}
