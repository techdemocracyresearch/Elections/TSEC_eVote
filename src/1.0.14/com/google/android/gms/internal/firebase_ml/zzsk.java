package com.google.android.gms.internal.firebase_ml;

import android.content.Context;
import android.os.SystemClock;
import android.util.SparseArray;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.vision.label.ImageLabel;
import com.google.android.gms.vision.label.ImageLabeler;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceImageLabelerOptions;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsk implements zzpu<List<FirebaseVisionImageLabel>, zzsf>, zzqp {
    private static boolean zzbqs = true;
    private static volatile Boolean zzbtu;
    private final Context zzbkt;
    private final zzqg zzbmd;
    private final FirebaseVisionOnDeviceImageLabelerOptions zzbtv;
    private ImageLabeler zzbtw;

    zzsk(zzqf zzqf, FirebaseVisionOnDeviceImageLabelerOptions firebaseVisionOnDeviceImageLabelerOptions) {
        Preconditions.checkNotNull(zzqf, "Context can not be null");
        Preconditions.checkNotNull(firebaseVisionOnDeviceImageLabelerOptions, "FirebaseVisionOnDeviceImageLabelerOptions can not be null");
        this.zzbkt = zzqf.getApplicationContext();
        this.zzbtv = firebaseVisionOnDeviceImageLabelerOptions;
        this.zzbmd = zzqg.zza(zzqf, 1);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzpu
    public final zzqp zzoc() {
        return this;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0020  */
    public final synchronized List<FirebaseVisionImageLabel> zza(zzsf zzsf) throws FirebaseMLException {
        ArrayList arrayList;
        boolean z;
        if (zzbtu == null) {
            Context context = this.zzbkt;
            boolean z2 = true;
            if (DynamiteModule.getLocalVersion(context, "com.google.android.gms.vision.dynamite.ica") <= 0) {
                if (DynamiteModule.getLocalVersion(context, "com.google.android.gms.vision.dynamite.imagelabel") <= 0) {
                    z = false;
                    if (!z) {
                        z2 = false;
                    }
                    zzbtu = Boolean.valueOf(z2);
                }
            }
            z = true;
            if (!z) {
            }
            zzbtu = Boolean.valueOf(z2);
        }
        if (!zzbtu.booleanValue()) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            ImageLabeler imageLabeler = this.zzbtw;
            if (imageLabeler == null) {
                zza(zzoc.UNKNOWN_ERROR, elapsedRealtime, zzsf);
                throw new FirebaseMLException("Model source is unavailable. Please load the model resource first.", 13);
            } else if (imageLabeler.isOperational()) {
                SparseArray<ImageLabel> detect = this.zzbtw.detect(zzsf.zzbrv);
                arrayList = new ArrayList();
                if (detect != null) {
                    for (int i = 0; i < detect.size(); i++) {
                        arrayList.add(new FirebaseVisionImageLabel(detect.get(detect.keyAt(i))));
                    }
                }
                zza(zzoc.NO_ERROR, elapsedRealtime, zzsf);
                zzbqs = false;
            } else {
                zza(zzoc.MODEL_NOT_DOWNLOADED, elapsedRealtime, zzsf);
                throw new FirebaseMLException("Waiting for the label detection model to be downloaded. Please wait.", 14);
            }
        } else {
            throw new FirebaseMLException("No model is bundled. Please check your app setup to includefirebase-ml-vision-image-label-model dependency.", 14);
        }
        return arrayList;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void zzol() {
        if (this.zzbtw == null) {
            this.zzbtw = new ImageLabeler.Builder(this.zzbkt).setScoreThreshold(this.zzbtv.getConfidenceThreshold()).build();
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void release() {
        ImageLabeler imageLabeler = this.zzbtw;
        if (imageLabeler != null) {
            imageLabeler.release();
            this.zzbtw = null;
        }
        zzbqs = true;
    }

    private final void zza(zzoc zzoc, long j, zzsf zzsf) {
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        this.zzbmd.zza(new zzsn(this, elapsedRealtime, zzoc, zzsf), zzod.ON_DEVICE_IMAGE_LABEL_DETECT);
        zzqm zzqm = zzsm.zzboj;
        this.zzbmd.zza((zzns.zze.zzb) ((zzwz) zzns.zze.zzb.zzkj().zzf(zzoc).zzr(zzbqs).zze(zzsa.zzc(zzsf)).zza(this.zzbtv.zzqv()).zzvb()), elapsedRealtime, zzod.AGGREGATED_ON_DEVICE_IMAGE_LABEL_DETECTION, zzqm);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzns.zzad.zza zza(long j, zzoc zzoc, zzsf zzsf) {
        return zzns.zzad.zzma().zza((zzns.zzao) ((zzwz) zzns.zzao.zzmx().zze(zzns.zzaf.zzme().zzk(j).zzk(zzoc).zzah(zzbqs).zzai(true).zzaj(true)).zzd(this.zzbtv.zzqv()).zzk(zzsa.zzc(zzsf)).zzvb()));
    }
}
