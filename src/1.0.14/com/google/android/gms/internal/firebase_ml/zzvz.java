package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzvz implements zzwb {
    private zzvz() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzwb
    public final byte[] zzd(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }

    /* synthetic */ zzvz(zzvu zzvu) {
        this();
    }
}
