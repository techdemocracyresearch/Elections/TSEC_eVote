package com.google.android.gms.internal.firebase_ml;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zznj extends WeakReference<Throwable> {
    private final int zzamb;

    public zznj(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        Objects.requireNonNull(th, "The referent cannot be null");
        this.zzamb = System.identityHashCode(th);
    }

    public final int hashCode() {
        return this.zzamb;
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            if (this == obj) {
                return true;
            }
            zznj zznj = (zznj) obj;
            return this.zzamb == zznj.zzamb && get() == zznj.get();
        }
    }
}
