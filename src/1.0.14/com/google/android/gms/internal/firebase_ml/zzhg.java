package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzhg extends IOException {
    private final String content;
    private final int statusCode;
    private final transient zzgx zzack;
    private final String zznl;

    public zzhg(zzhd zzhd) {
        this(new zzhf(zzhd));
    }

    protected zzhg(zzhf zzhf) {
        super(zzhf.message);
        this.statusCode = zzhf.statusCode;
        this.zznl = zzhf.zznl;
        this.zzack = zzhf.zzack;
        this.content = zzhf.content;
    }

    public final int getStatusCode() {
        return this.statusCode;
    }

    public static StringBuilder zzc(zzhd zzhd) {
        StringBuilder sb = new StringBuilder();
        int statusCode2 = zzhd.getStatusCode();
        if (statusCode2 != 0) {
            sb.append(statusCode2);
        }
        String statusMessage = zzhd.getStatusMessage();
        if (statusMessage != null) {
            if (statusCode2 != 0) {
                sb.append(' ');
            }
            sb.append(statusMessage);
        }
        return sb;
    }
}
