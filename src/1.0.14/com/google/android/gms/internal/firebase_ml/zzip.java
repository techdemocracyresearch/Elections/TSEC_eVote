package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzip {
    @Deprecated
    private final byte zzafv;
    protected final byte zzafw;
    private final int zzafx;
    private final int zzafy;
    protected final int zzafz;
    private final int zzaga;

    protected zzip(int i, int i2, int i3, int i4) {
        this(3, 4, i3, i4, (byte) 61);
    }

    /* access modifiers changed from: package-private */
    public abstract void zza(byte[] bArr, int i, int i2, zzio zzio);

    /* access modifiers changed from: protected */
    public abstract boolean zza(byte b);

    private zzip(int i, int i2, int i3, int i4, byte b) {
        this.zzafv = 61;
        this.zzafx = 3;
        this.zzafy = 4;
        this.zzafz = i3 > 0 && i4 > 0 ? (i3 / 4) << 2 : 0;
        this.zzaga = i4;
        this.zzafw = 61;
    }

    /* access modifiers changed from: protected */
    public final byte[] zza(int i, zzio zzio) {
        if (zzio.buffer != null && zzio.buffer.length >= zzio.pos + i) {
            return zzio.buffer;
        }
        if (zzio.buffer == null) {
            zzio.buffer = new byte[8192];
            zzio.pos = 0;
            zzio.zzafs = 0;
        } else {
            byte[] bArr = new byte[(zzio.buffer.length << 1)];
            System.arraycopy(zzio.buffer, 0, bArr, 0, zzio.buffer.length);
            zzio.buffer = bArr;
        }
        return zzio.buffer;
    }

    public final long zzc(byte[] bArr) {
        int length = bArr.length;
        int i = this.zzafx;
        long j = ((long) (((length + i) - 1) / i)) * ((long) this.zzafy);
        int i2 = this.zzafz;
        return i2 > 0 ? j + ((((((long) i2) + j) - 1) / ((long) i2)) * ((long) this.zzaga)) : j;
    }
}
