package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zznk extends zznh {
    zznk() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zznh
    public final void zza(Throwable th, Throwable th2) {
        th.addSuppressed(th2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zznh
    public final void zzb(Throwable th) {
        th.printStackTrace();
    }
}
