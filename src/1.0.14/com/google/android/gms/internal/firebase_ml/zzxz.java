package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzxz extends zzxu {
    private zzxz() {
        super();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzxu
    public final void zzb(Object obj, long j) {
        zzd(obj, j).zztm();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [com.google.android.gms.internal.firebase_ml.zzxl] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzxu
    public final <E> void zza(Object obj, Object obj2, long j) {
        zzxl<E> zzd = zzd(obj, j);
        zzxl<E> zzd2 = zzd(obj2, j);
        int size = zzd.size();
        int size2 = zzd2.size();
        zzxl<E> zzxl = zzd;
        zzxl = zzd;
        if (size > 0 && size2 > 0) {
            boolean zztl = zzd.zztl();
            zzxl<E> zzxl2 = zzd;
            if (!zztl) {
                zzxl2 = zzd.zzcv(size2 + size);
            }
            zzxl2.addAll(zzd2);
            zzxl = zzxl2;
        }
        if (size > 0) {
            zzd2 = zzxl;
        }
        zzaac.zza(obj, j, zzd2);
    }

    private static <E> zzxl<E> zzd(Object obj, long j) {
        return (zzxl) zzaac.zzp(obj, j);
    }
}
