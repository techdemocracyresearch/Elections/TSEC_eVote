package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzzg {
    private static final Class<?> zzcpj = zzwo();
    private static final zzzw<?, ?> zzcpk = zzba(false);
    private static final zzzw<?, ?> zzcpl = zzba(true);
    private static final zzzw<?, ?> zzcpm = new zzzy();

    public static void zzm(Class<?> cls) {
        Class<?> cls2;
        if (!zzwz.class.isAssignableFrom(cls) && (cls2 = zzcpj) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static void zza(int i, List<Double> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzg(i, list, z);
        }
    }

    public static void zzb(int i, List<Float> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzf(i, list, z);
        }
    }

    public static void zzc(int i, List<Long> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzc(i, list, z);
        }
    }

    public static void zzd(int i, List<Long> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzd(i, list, z);
        }
    }

    public static void zze(int i, List<Long> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzn(i, list, z);
        }
    }

    public static void zzf(int i, List<Long> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zze(i, list, z);
        }
    }

    public static void zzg(int i, List<Long> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzl(i, list, z);
        }
    }

    public static void zzh(int i, List<Integer> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zza(i, list, z);
        }
    }

    public static void zzi(int i, List<Integer> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzj(i, list, z);
        }
    }

    public static void zzj(int i, List<Integer> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzm(i, list, z);
        }
    }

    public static void zzk(int i, List<Integer> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzb(i, list, z);
        }
    }

    public static void zzl(int i, List<Integer> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzk(i, list, z);
        }
    }

    public static void zzm(int i, List<Integer> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzh(i, list, z);
        }
    }

    public static void zzn(int i, List<Boolean> list, zzaat zzaat, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzi(i, list, z);
        }
    }

    public static void zza(int i, List<String> list, zzaat zzaat) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zza(i, list);
        }
    }

    public static void zzb(int i, List<zzvv> list, zzaat zzaat) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzb(i, list);
        }
    }

    public static void zza(int i, List<?> list, zzaat zzaat, zzze zzze) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zza(i, list, zzze);
        }
    }

    public static void zzb(int i, List<?> list, zzaat zzaat, zzze zzze) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzaat.zzb(i, list, zzze);
        }
    }

    static int zzi(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzxy) {
            zzxy zzxy = (zzxy) list;
            i = 0;
            while (i2 < size) {
                i += zzwi.zzz(zzxy.getLong(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzwi.zzz(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int zzo(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return zzi(list) + (list.size() * zzwi.zzdf(i));
    }

    static int zzj(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzxy) {
            zzxy zzxy = (zzxy) list;
            i = 0;
            while (i2 < size) {
                i += zzwi.zzaa(zzxy.getLong(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzwi.zzaa(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int zzp(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzj(list) + (size * zzwi.zzdf(i));
    }

    static int zzk(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzxy) {
            zzxy zzxy = (zzxy) list;
            i = 0;
            while (i2 < size) {
                i += zzwi.zzab(zzxy.getLong(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzwi.zzab(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int zzq(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzk(list) + (size * zzwi.zzdf(i));
    }

    static int zzl(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzxb) {
            zzxb zzxb = (zzxb) list;
            i = 0;
            while (i2 < size) {
                i += zzwi.zzdl(zzxb.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzwi.zzdl(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzr(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzl(list) + (size * zzwi.zzdf(i));
    }

    static int zzm(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzxb) {
            zzxb zzxb = (zzxb) list;
            i = 0;
            while (i2 < size) {
                i += zzwi.zzdg(zzxb.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzwi.zzdg(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzs(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzm(list) + (size * zzwi.zzdf(i));
    }

    static int zzn(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzxb) {
            zzxb zzxb = (zzxb) list;
            i = 0;
            while (i2 < size) {
                i += zzwi.zzdh(zzxb.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzwi.zzdh(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzt(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzn(list) + (size * zzwi.zzdf(i));
    }

    static int zzo(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzxb) {
            zzxb zzxb = (zzxb) list;
            i = 0;
            while (i2 < size) {
                i += zzwi.zzdi(zzxb.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzwi.zzdi(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzu(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzo(list) + (size * zzwi.zzdf(i));
    }

    static int zzp(List<?> list) {
        return list.size() << 2;
    }

    static int zzv(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzwi.zzo(i, 0);
    }

    static int zzq(List<?> list) {
        return list.size() << 3;
    }

    static int zzw(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzwi.zzg(i, 0);
    }

    static int zzr(List<?> list) {
        return list.size();
    }

    static int zzx(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzwi.zzb(i, true);
    }

    static int zzc(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int zzdf = zzwi.zzdf(i) * size;
        if (list instanceof zzxv) {
            zzxv zzxv = (zzxv) list;
            while (i4 < size) {
                Object raw = zzxv.getRaw(i4);
                if (raw instanceof zzvv) {
                    i3 = zzwi.zzd((zzvv) raw);
                } else {
                    i3 = zzwi.zzcl((String) raw);
                }
                zzdf += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof zzvv) {
                    i2 = zzwi.zzd((zzvv) obj);
                } else {
                    i2 = zzwi.zzcl((String) obj);
                }
                zzdf += i2;
                i4++;
            }
        }
        return zzdf;
    }

    static int zzc(int i, Object obj, zzze zzze) {
        if (obj instanceof zzxt) {
            return zzwi.zza(i, (zzxt) obj);
        }
        return zzwi.zzb(i, (zzyk) obj, zzze);
    }

    static int zzc(int i, List<?> list, zzze zzze) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int zzdf = zzwi.zzdf(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof zzxt) {
                i2 = zzwi.zza((zzxt) obj);
            } else {
                i2 = zzwi.zza((zzyk) obj, zzze);
            }
            zzdf += i2;
        }
        return zzdf;
    }

    static int zzd(int i, List<zzvv> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int zzdf = size * zzwi.zzdf(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            zzdf += zzwi.zzd(list.get(i2));
        }
        return zzdf;
    }

    static int zzd(int i, List<zzyk> list, zzze zzze) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zzwi.zzc(i, list.get(i3), zzze);
        }
        return i2;
    }

    public static zzzw<?, ?> zzwl() {
        return zzcpk;
    }

    public static zzzw<?, ?> zzwm() {
        return zzcpl;
    }

    public static zzzw<?, ?> zzwn() {
        return zzcpm;
    }

    private static zzzw<?, ?> zzba(boolean z) {
        try {
            Class<?> zzwp = zzwp();
            if (zzwp == null) {
                return null;
            }
            return (zzzw) zzwp.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> zzwo() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> zzwp() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    static boolean zzf(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    static <T> void zza(zzyh zzyh, T t, T t2, long j) {
        zzaac.zza(t, j, zzyh.zzd(zzaac.zzp(t, j), zzaac.zzp(t2, j)));
    }

    static <T, FT extends zzwt<FT>> void zza(zzwq<FT> zzwq, T t, T t2) {
        zzwr<FT> zzo = zzwq.zzo(t2);
        if (!zzo.zzcil.isEmpty()) {
            zzwq.zzp(t).zza(zzo);
        }
    }

    static <T, UT, UB> void zza(zzzw<UT, UB> zzzw, T t, T t2) {
        zzzw.zzg(t, zzzw.zzi(zzzw.zzae(t), zzzw.zzae(t2)));
    }

    static <UT, UB> UB zza(int i, List<Integer> list, zzxe zzxe, UB ub, zzzw<UT, UB> zzzw) {
        if (zzxe == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                if (zzxe.zzb(intValue)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue));
                    }
                    i2++;
                } else {
                    ub = (UB) zza(i, intValue, ub, zzzw);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        } else {
            Iterator<Integer> it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = it.next().intValue();
                if (!zzxe.zzb(intValue2)) {
                    ub = (UB) zza(i, intValue2, ub, zzzw);
                    it.remove();
                }
            }
        }
        return ub;
    }

    private static <UT, UB> UB zza(int i, int i2, UB ub, zzzw<UT, UB> zzzw) {
        if (ub == null) {
            ub = zzzw.zzwy();
        }
        zzzw.zza(ub, i, (long) i2);
        return ub;
    }
}
