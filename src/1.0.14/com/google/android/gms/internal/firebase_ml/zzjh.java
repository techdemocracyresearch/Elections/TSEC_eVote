package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzjh {
    /* JADX INFO: finally extract failed */
    public static long zzb(zzjq zzjq) throws IOException {
        zzit zzit = new zzit();
        try {
            zzjq.writeTo(zzit);
            zzit.close();
            return zzit.zzagi;
        } catch (Throwable th) {
            zzit.close();
            throw th;
        }
    }
}
