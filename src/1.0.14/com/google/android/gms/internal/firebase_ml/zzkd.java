package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzkd extends zzjz {
    /* access modifiers changed from: protected */
    public abstract int zza(CharSequence charSequence, int i, int i2);

    /* access modifiers changed from: protected */
    public abstract char[] zzam(int i);

    /* access modifiers changed from: protected */
    public final String zza(String str, int i) {
        int length = str.length();
        char[] zzig = zzkb.zzig();
        int i2 = 0;
        int i3 = 0;
        while (i < length) {
            if (i < length) {
                int i4 = i + 1;
                char charAt = str.charAt(i);
                int i5 = 1;
                int i6 = charAt;
                if (charAt >= 55296) {
                    i6 = charAt;
                    if (charAt <= 57343) {
                        if (charAt > 56319) {
                            StringBuilder sb = new StringBuilder(82);
                            sb.append("Unexpected low surrogate character '");
                            sb.append(charAt);
                            sb.append("' with value ");
                            sb.append((int) charAt);
                            sb.append(" at index ");
                            sb.append(i4 - 1);
                            throw new IllegalArgumentException(sb.toString());
                        } else if (i4 == length) {
                            i6 = -charAt;
                        } else {
                            char charAt2 = str.charAt(i4);
                            if (Character.isLowSurrogate(charAt2)) {
                                i6 = Character.toCodePoint(charAt, charAt2);
                            } else {
                                StringBuilder sb2 = new StringBuilder(83);
                                sb2.append("Expected low surrogate but got char '");
                                sb2.append(charAt2);
                                sb2.append("' with value ");
                                sb2.append((int) charAt2);
                                sb2.append(" at index ");
                                sb2.append(i4);
                                throw new IllegalArgumentException(sb2.toString());
                            }
                        }
                    }
                }
                if (i6 >= 0) {
                    char[] zzam = zzam(i6 == 1 ? 1 : 0);
                    if (Character.isSupplementaryCodePoint(i6)) {
                        i5 = 2;
                    }
                    int i7 = i5 + i;
                    if (zzam != null) {
                        int i8 = i - i2;
                        int i9 = i3 + i8;
                        int length2 = zzam.length + i9;
                        if (zzig.length < length2) {
                            zzig = zza(zzig, i3, ((length2 + length) - i) + 32);
                        }
                        if (i8 > 0) {
                            str.getChars(i2, i, zzig, i3);
                            i3 = i9;
                        }
                        if (zzam.length > 0) {
                            System.arraycopy(zzam, 0, zzig, i3, zzam.length);
                            i3 += zzam.length;
                        }
                        i2 = i7;
                    }
                    i = zza(str, i7, length);
                } else {
                    throw new IllegalArgumentException("Trailing high surrogate at end of input");
                }
            } else {
                throw new IndexOutOfBoundsException("Index exceeds specified range");
            }
        }
        int i10 = length - i2;
        if (i10 > 0) {
            int i11 = i10 + i3;
            if (zzig.length < i11) {
                zzig = zza(zzig, i3, i11);
            }
            str.getChars(i2, length, zzig, i3);
            i3 = i11;
        }
        return new String(zzig, 0, i3);
    }

    private static char[] zza(char[] cArr, int i, int i2) {
        char[] cArr2 = new char[i2];
        if (i > 0) {
            System.arraycopy(cArr, 0, cArr2, 0, i);
        }
        return cArr2;
    }
}
