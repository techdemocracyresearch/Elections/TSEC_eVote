package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzabh;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzabl implements zzxe {
    static final zzxe zzan = new zzabl();

    private zzabl() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxe
    public final boolean zzb(int i) {
        return zzabh.zzc.zza.zzel(i) != null;
    }
}
