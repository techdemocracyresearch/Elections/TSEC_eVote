package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzns;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final /* synthetic */ class zzql implements Runnable {
    private final zzqg zzbjy;
    private final zzns.zzad.zza zzbjz;
    private final zzod zzbka;

    zzql(zzqg zzqg, zzns.zzad.zza zza, zzod zzod) {
        this.zzbjy = zzqg;
        this.zzbjz = zza;
        this.zzbka = zzod;
    }

    public final void run() {
        this.zzbjy.zzb(this.zzbjz, this.zzbka);
    }
}
