package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;
import java.io.IOException;
import java.util.Arrays;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzzz {
    private static final zzzz zzcqb = new zzzz(0, new int[0], new Object[0], false);
    private int count;
    private boolean zzchf;
    private int zzclk;
    private Object[] zzcob;
    private int[] zzcqc;

    public static zzzz zzwz() {
        return zzcqb;
    }

    static zzzz zzxa() {
        return new zzzz();
    }

    static zzzz zza(zzzz zzzz, zzzz zzzz2) {
        int i = zzzz.count + zzzz2.count;
        int[] copyOf = Arrays.copyOf(zzzz.zzcqc, i);
        System.arraycopy(zzzz2.zzcqc, 0, copyOf, zzzz.count, zzzz2.count);
        Object[] copyOf2 = Arrays.copyOf(zzzz.zzcob, i);
        System.arraycopy(zzzz2.zzcob, 0, copyOf2, zzzz.count, zzzz2.count);
        return new zzzz(i, copyOf, copyOf2, true);
    }

    private zzzz() {
        this(0, new int[8], new Object[8], true);
    }

    private zzzz(int i, int[] iArr, Object[] objArr, boolean z) {
        this.zzclk = -1;
        this.count = i;
        this.zzcqc = iArr;
        this.zzcob = objArr;
        this.zzchf = z;
    }

    public final void zztm() {
        this.zzchf = false;
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzaat zzaat) throws IOException {
        if (zzaat.zzub() == zzwz.zzg.zzcmf) {
            for (int i = this.count - 1; i >= 0; i--) {
                zzaat.zza(this.zzcqc[i] >>> 3, this.zzcob[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.count; i2++) {
            zzaat.zza(this.zzcqc[i2] >>> 3, this.zzcob[i2]);
        }
    }

    public final void zzb(zzaat zzaat) throws IOException {
        if (this.count != 0) {
            if (zzaat.zzub() == zzwz.zzg.zzcme) {
                for (int i = 0; i < this.count; i++) {
                    zzb(this.zzcqc[i], this.zzcob[i], zzaat);
                }
                return;
            }
            for (int i2 = this.count - 1; i2 >= 0; i2--) {
                zzb(this.zzcqc[i2], this.zzcob[i2], zzaat);
            }
        }
    }

    private static void zzb(int i, Object obj, zzaat zzaat) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            zzaat.zzi(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            zzaat.zzc(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            zzaat.zza(i2, (zzvv) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                zzaat.zzk(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(zzxk.zzvh());
        } else if (zzaat.zzub() == zzwz.zzg.zzcme) {
            zzaat.zzdp(i2);
            ((zzzz) obj).zzb(zzaat);
            zzaat.zzdq(i2);
        } else {
            zzaat.zzdq(i2);
            ((zzzz) obj).zzb(zzaat);
            zzaat.zzdp(i2);
        }
    }

    public final int zzxb() {
        int i = this.zzclk;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.count; i3++) {
            i2 += zzwi.zzd(this.zzcqc[i3] >>> 3, (zzvv) this.zzcob[i3]);
        }
        this.zzclk = i2;
        return i2;
    }

    public final int zzuo() {
        int i;
        int i2 = this.zzclk;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.count; i4++) {
            int i5 = this.zzcqc[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 == 0) {
                i = zzwi.zze(i6, ((Long) this.zzcob[i4]).longValue());
            } else if (i7 == 1) {
                i = zzwi.zzg(i6, ((Long) this.zzcob[i4]).longValue());
            } else if (i7 == 2) {
                i = zzwi.zzc(i6, (zzvv) this.zzcob[i4]);
            } else if (i7 == 3) {
                i = (zzwi.zzdf(i6) << 1) + ((zzzz) this.zzcob[i4]).zzuo();
            } else if (i7 == 5) {
                i = zzwi.zzo(i6, ((Integer) this.zzcob[i4]).intValue());
            } else {
                throw new IllegalStateException(zzxk.zzvh());
            }
            i3 += i;
        }
        this.zzclk = i3;
        return i3;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof zzzz)) {
            return false;
        }
        zzzz zzzz = (zzzz) obj;
        int i = this.count;
        if (i == zzzz.count) {
            int[] iArr = this.zzcqc;
            int[] iArr2 = zzzz.zzcqc;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.zzcob;
                Object[] objArr2 = zzzz.zzcob;
                int i3 = this.count;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                return z2;
            }
        }
    }

    public final int hashCode() {
        int i = this.count;
        int i2 = (i + 527) * 31;
        int[] iArr = this.zzcqc;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.zzcob;
        int i7 = this.count;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    /* access modifiers changed from: package-private */
    public final void zzb(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.count; i2++) {
            zzyp.zza(sb, i, String.valueOf(this.zzcqc[i2] >>> 3), this.zzcob[i2]);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(int i, Object obj) {
        if (this.zzchf) {
            int i2 = this.count;
            int[] iArr = this.zzcqc;
            if (i2 == iArr.length) {
                int i3 = i2 + (i2 < 4 ? 8 : i2 >> 1);
                this.zzcqc = Arrays.copyOf(iArr, i3);
                this.zzcob = Arrays.copyOf(this.zzcob, i3);
            }
            int[] iArr2 = this.zzcqc;
            int i4 = this.count;
            iArr2[i4] = i;
            this.zzcob[i4] = obj;
            this.count = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }
}
