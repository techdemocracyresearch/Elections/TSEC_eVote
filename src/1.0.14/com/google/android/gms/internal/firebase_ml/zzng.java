package com.google.android.gms.internal.firebase_ml;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzng {
    private final ConcurrentHashMap<zznj, List<Throwable>> zzalx = new ConcurrentHashMap<>(16, 0.75f, 10);
    private final ReferenceQueue<Throwable> zzaly = new ReferenceQueue<>();

    zzng() {
    }

    public final List<Throwable> zza(Throwable th, boolean z) {
        Reference<? extends Throwable> poll = this.zzaly.poll();
        while (poll != null) {
            this.zzalx.remove(poll);
            poll = this.zzaly.poll();
        }
        List<Throwable> list = this.zzalx.get(new zznj(th, null));
        if (!z || list != null) {
            return list;
        }
        Vector vector = new Vector(2);
        List<Throwable> putIfAbsent = this.zzalx.putIfAbsent(new zznj(th, this.zzaly), vector);
        return putIfAbsent == null ? vector : putIfAbsent;
    }
}
