package com.google.android.gms.internal.firebase_ml;

import java.util.ArrayDeque;
import java.util.Arrays;

public final class zzzc {
    private final ArrayDeque<zzvv> zzcpd;

    private zzzc() {
        this.zzcpd = new ArrayDeque<>();
    }

    /* access modifiers changed from: public */
    private final zzvv zzc(zzvv zzvv, zzvv zzvv2) {
        zzf(zzvv);
        zzf(zzvv2);
        zzvv pop = this.zzcpd.pop();
        while (!this.zzcpd.isEmpty()) {
            pop = new zzza(this.zzcpd.pop(), pop, null);
        }
        return pop;
    }

    private final void zzf(zzvv zzvv) {
        while (!zzvv.zzts()) {
            if (zzvv instanceof zzza) {
                zzza zzza = (zzza) zzvv;
                zzf(zzza.zzcoz);
                zzvv = zzza.zzcpa;
            } else {
                String valueOf = String.valueOf(zzvv.getClass());
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 49);
                sb.append("Has a new type of ByteString been created? Found ");
                sb.append(valueOf);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        int zzea = zzea(zzvv.size());
        int zzdz = zzza.zzdz(zzea + 1);
        if (this.zzcpd.isEmpty() || this.zzcpd.peek().size() >= zzdz) {
            this.zzcpd.push(zzvv);
            return;
        }
        int zzdz2 = zzza.zzdz(zzea);
        zzvv pop = this.zzcpd.pop();
        while (!this.zzcpd.isEmpty() && this.zzcpd.peek().size() < zzdz2) {
            pop = new zzza(this.zzcpd.pop(), pop, null);
        }
        zzza zzza2 = new zzza(pop, zzvv, null);
        while (!this.zzcpd.isEmpty() && this.zzcpd.peek().size() < zzza.zzdz(zzea(zzza2.size()) + 1)) {
            zzza2 = new zzza(this.zzcpd.pop(), zzza2, null);
        }
        this.zzcpd.push(zzza2);
    }

    private static int zzea(int i) {
        int binarySearch = Arrays.binarySearch(zzza.zzcox, i);
        return binarySearch < 0 ? (-(binarySearch + 1)) - 1 : binarySearch;
    }

    /* synthetic */ zzzc(zzzd zzzd) {
        this();
    }
}
