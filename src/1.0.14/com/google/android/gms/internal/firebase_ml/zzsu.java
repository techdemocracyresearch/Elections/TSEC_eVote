package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import java.io.Closeable;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsu extends zzrz<FirebaseVisionText> implements Closeable {
    private static final Map<String, zzsu> zzbim = new HashMap();

    public static synchronized zzsu zzc(zzqf zzqf) {
        zzsu zzsu;
        synchronized (zzsu.class) {
            Preconditions.checkNotNull(zzqf, "MlKitContext can not be null.");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Persistence key must not be null");
            Map<String, zzsu> map = zzbim;
            zzsu = map.get(zzqf.getPersistenceKey());
            if (zzsu == null) {
                zzsu = new zzsu(zzqf);
                map.put(zzqf.getPersistenceKey(), zzsu);
            }
        }
        return zzsu;
    }

    private zzsu(zzqf zzqf) {
        super(zzqf, new zzst(zzqf));
        zzqg.zza(zzqf, 1).zza(zzns.zzad.zzma().zza(zzns.zzaz.zznt()), zzod.ON_DEVICE_TEXT_CREATE);
    }

    public final Task<FirebaseVisionText> processImage(FirebaseVisionImage firebaseVisionImage) {
        return super.zza(firebaseVisionImage, false, true);
    }
}
