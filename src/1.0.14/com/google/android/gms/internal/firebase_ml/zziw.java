package com.google.android.gms.internal.firebase_ml;

import com.bumptech.glide.load.Key;
import java.nio.charset.Charset;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zziw {
    public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    public static final Charset UTF_8 = Charset.forName(Key.STRING_CHARSET_NAME);
}
