package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzwa extends Iterator<Byte> {
    byte nextByte();
}
