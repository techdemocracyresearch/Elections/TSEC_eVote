package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzhz implements zzjm {
    private final zzhx zzaeg;
    private final Set<String> zzaei;

    protected zzhz(zzic zzic) {
        this.zzaeg = zzic.zzaeg;
        this.zzaei = new HashSet(zzic.zzael);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjm
    public final <T> T zza(InputStream inputStream, Charset charset, Class<T> cls) throws IOException {
        zzib zza = this.zzaeg.zza(inputStream, charset);
        if (!this.zzaei.isEmpty()) {
            try {
                boolean z = (zza.zza(this.zzaei) == null || zza.zzhd() == zzih.END_OBJECT) ? false : true;
                Object[] objArr = {this.zzaei};
                if (!z) {
                    throw new IllegalArgumentException(zzms.zzb("wrapper key(s) not found: %s", objArr));
                }
            } catch (Throwable th) {
                zza.close();
                throw th;
            }
        }
        return (T) zza.zza(cls, true, null);
    }

    public final zzhx zzfp() {
        return this.zzaeg;
    }

    public final Set<String> zzgu() {
        return Collections.unmodifiableSet(this.zzaei);
    }
}
