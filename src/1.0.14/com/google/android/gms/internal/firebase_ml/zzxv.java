package com.google.android.gms.internal.firebase_ml;

import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzxv extends List {
    Object getRaw(int i);

    void zze(zzvv zzvv);

    List<?> zzvn();

    zzxv zzvo();
}
