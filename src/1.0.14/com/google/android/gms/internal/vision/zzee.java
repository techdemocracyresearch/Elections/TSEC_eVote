package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzea;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzee implements zzgv<zzea.zzf.zzb> {
    zzee() {
    }

    /* Return type fixed from 'com.google.android.gms.internal.vision.zzgw' to match base method */
    @Override // com.google.android.gms.internal.vision.zzgv
    public final /* synthetic */ zzea.zzf.zzb zzh(int i) {
        return zzea.zzf.zzb.zzu(i);
    }
}
