package com.google.android.gms.internal.vision;

import kotlin.text.Typography;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
public enum zzbl implements zzgw {
    RGBA(0),
    NV12(5),
    NV21(1),
    YV12(6),
    YV21(7),
    RGB(2),
    GRAY(3),
    GRAY16(4);
    
    private static final zzgv<zzbl> zzhc = new zzbo();
    private final int value;

    @Override // com.google.android.gms.internal.vision.zzgw
    public final int zzag() {
        return this.value;
    }

    public static zzbl zzf(int i) {
        switch (i) {
            case 0:
                return RGBA;
            case 1:
                return NV21;
            case 2:
                return RGB;
            case 3:
                return GRAY;
            case 4:
                return GRAY16;
            case 5:
                return NV12;
            case 6:
                return YV12;
            case 7:
                return YV21;
            default:
                return null;
        }
    }

    public static zzgy zzah() {
        return zzbn.zzhf;
    }

    public final String toString() {
        return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
    }

    private zzbl(int i) {
        this.value = i;
    }
}
