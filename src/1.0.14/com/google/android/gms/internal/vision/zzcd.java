package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzbx;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
final class zzcd implements zzgy {
    static final zzgy zzhf = new zzcd();

    private zzcd() {
    }

    @Override // com.google.android.gms.internal.vision.zzgy
    public final boolean zzg(int i) {
        return zzbx.zzb.zzc.zzp(i) != null;
    }
}
