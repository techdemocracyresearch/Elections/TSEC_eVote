package com.google.android.gms.internal.vision;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzjo extends AbstractList<String> implements zzhj, RandomAccess {
    private final zzhj zzaau;

    public zzjo(zzhj zzhj) {
        this.zzaau = zzhj;
    }

    @Override // com.google.android.gms.internal.vision.zzhj
    public final zzhj zzgy() {
        return this;
    }

    @Override // com.google.android.gms.internal.vision.zzhj
    public final Object getRaw(int i) {
        return this.zzaau.getRaw(i);
    }

    public final int size() {
        return this.zzaau.size();
    }

    @Override // com.google.android.gms.internal.vision.zzhj
    public final void zzc(zzfh zzfh) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.List, java.util.AbstractList
    public final ListIterator<String> listIterator(int i) {
        return new zzjn(this, i);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, java.lang.Iterable
    public final Iterator<String> iterator() {
        return new zzjq(this);
    }

    @Override // com.google.android.gms.internal.vision.zzhj
    public final List<?> zzgx() {
        return this.zzaau.zzgx();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ String get(int i) {
        return (String) this.zzaau.get(i);
    }
}
