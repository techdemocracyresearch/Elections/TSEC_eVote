package com.google.android.gms.internal.vision;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Unsafe;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzjp {
    private static final Logger logger = Logger.getLogger(zzjp.class.getName());
    private static final boolean zzaav;
    private static final boolean zzaaw;
    private static final zzd zzaax;
    private static final boolean zzaay = zzin();
    private static final long zzaaz;
    private static final long zzaba = ((long) zzi(boolean[].class));
    private static final long zzabb = ((long) zzj(boolean[].class));
    private static final long zzabc = ((long) zzi(int[].class));
    private static final long zzabd = ((long) zzj(int[].class));
    private static final long zzabe = ((long) zzi(long[].class));
    private static final long zzabf = ((long) zzj(long[].class));
    private static final long zzabg = ((long) zzi(float[].class));
    private static final long zzabh = ((long) zzj(float[].class));
    private static final long zzabi = ((long) zzi(double[].class));
    private static final long zzabj = ((long) zzj(double[].class));
    private static final long zzabk = ((long) zzi(Object[].class));
    private static final long zzabl = ((long) zzj(Object[].class));
    private static final long zzabm;
    private static final int zzabn;
    static final boolean zzabo = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);
    private static final Class<?> zzrs = zzfa.zzds();
    private static final boolean zzsx = zzim();
    private static final Unsafe zzyz;

    private zzjp() {
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    static final class zza extends zzd {
        zza(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final byte zzy(Object obj, long j) {
            if (zzjp.zzabo) {
                return zzjp.zzq(obj, j);
            }
            return zzjp.zzr(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zze(Object obj, long j, byte b) {
            if (zzjp.zzabo) {
                zzjp.zza(obj, j, b);
            } else {
                zzjp.zzb(obj, j, b);
            }
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final boolean zzm(Object obj, long j) {
            if (zzjp.zzabo) {
                return zzjp.zzs(obj, j);
            }
            return zzjp.zzt(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zza(Object obj, long j, boolean z) {
            if (zzjp.zzabo) {
                zzjp.zzb(obj, j, z);
            } else {
                zzjp.zzc(obj, j, z);
            }
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final float zzn(Object obj, long j) {
            return Float.intBitsToFloat(zzk(obj, j));
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zza(Object obj, long j, float f) {
            zzb(obj, j, Float.floatToIntBits(f));
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final double zzo(Object obj, long j) {
            return Double.longBitsToDouble(zzl(obj, j));
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    static final class zzb extends zzd {
        zzb(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final byte zzy(Object obj, long j) {
            return this.zzabr.getByte(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zze(Object obj, long j, byte b) {
            this.zzabr.putByte(obj, j, b);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final boolean zzm(Object obj, long j) {
            return this.zzabr.getBoolean(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zza(Object obj, long j, boolean z) {
            this.zzabr.putBoolean(obj, j, z);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final float zzn(Object obj, long j) {
            return this.zzabr.getFloat(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zza(Object obj, long j, float f) {
            this.zzabr.putFloat(obj, j, f);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final double zzo(Object obj, long j) {
            return this.zzabr.getDouble(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zza(Object obj, long j, double d) {
            this.zzabr.putDouble(obj, j, d);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    static final class zzc extends zzd {
        zzc(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final byte zzy(Object obj, long j) {
            if (zzjp.zzabo) {
                return zzjp.zzq(obj, j);
            }
            return zzjp.zzr(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zze(Object obj, long j, byte b) {
            if (zzjp.zzabo) {
                zzjp.zza(obj, j, b);
            } else {
                zzjp.zzb(obj, j, b);
            }
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final boolean zzm(Object obj, long j) {
            if (zzjp.zzabo) {
                return zzjp.zzs(obj, j);
            }
            return zzjp.zzt(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zza(Object obj, long j, boolean z) {
            if (zzjp.zzabo) {
                zzjp.zzb(obj, j, z);
            } else {
                zzjp.zzc(obj, j, z);
            }
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final float zzn(Object obj, long j) {
            return Float.intBitsToFloat(zzk(obj, j));
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zza(Object obj, long j, float f) {
            zzb(obj, j, Float.floatToIntBits(f));
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final double zzo(Object obj, long j) {
            return Double.longBitsToDouble(zzl(obj, j));
        }

        @Override // com.google.android.gms.internal.vision.zzjp.zzd
        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }
    }

    static boolean zzij() {
        return zzsx;
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static abstract class zzd {
        Unsafe zzabr;

        zzd(Unsafe unsafe) {
            this.zzabr = unsafe;
        }

        public abstract void zza(Object obj, long j, double d);

        public abstract void zza(Object obj, long j, float f);

        public abstract void zza(Object obj, long j, boolean z);

        public abstract void zze(Object obj, long j, byte b);

        public abstract boolean zzm(Object obj, long j);

        public abstract float zzn(Object obj, long j);

        public abstract double zzo(Object obj, long j);

        public abstract byte zzy(Object obj, long j);

        public final int zzk(Object obj, long j) {
            return this.zzabr.getInt(obj, j);
        }

        public final void zzb(Object obj, long j, int i) {
            this.zzabr.putInt(obj, j, i);
        }

        public final long zzl(Object obj, long j) {
            return this.zzabr.getLong(obj, j);
        }

        public final void zza(Object obj, long j, long j2) {
            this.zzabr.putLong(obj, j, j2);
        }
    }

    static boolean zzik() {
        return zzaay;
    }

    static <T> T zzh(Class<T> cls) {
        try {
            return (T) zzyz.allocateInstance(cls);
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        }
    }

    private static int zzi(Class<?> cls) {
        if (zzsx) {
            return zzaax.zzabr.arrayBaseOffset(cls);
        }
        return -1;
    }

    private static int zzj(Class<?> cls) {
        if (zzsx) {
            return zzaax.zzabr.arrayIndexScale(cls);
        }
        return -1;
    }

    static int zzk(Object obj, long j) {
        return zzaax.zzk(obj, j);
    }

    static void zzb(Object obj, long j, int i) {
        zzaax.zzb(obj, j, i);
    }

    static long zzl(Object obj, long j) {
        return zzaax.zzl(obj, j);
    }

    static void zza(Object obj, long j, long j2) {
        zzaax.zza(obj, j, j2);
    }

    static boolean zzm(Object obj, long j) {
        return zzaax.zzm(obj, j);
    }

    static void zza(Object obj, long j, boolean z) {
        zzaax.zza(obj, j, z);
    }

    static float zzn(Object obj, long j) {
        return zzaax.zzn(obj, j);
    }

    static void zza(Object obj, long j, float f) {
        zzaax.zza(obj, j, f);
    }

    static double zzo(Object obj, long j) {
        return zzaax.zzo(obj, j);
    }

    static void zza(Object obj, long j, double d) {
        zzaax.zza(obj, j, d);
    }

    static Object zzp(Object obj, long j) {
        return zzaax.zzabr.getObject(obj, j);
    }

    static void zza(Object obj, long j, Object obj2) {
        zzaax.zzabr.putObject(obj, j, obj2);
    }

    static byte zza(byte[] bArr, long j) {
        return zzaax.zzy(bArr, zzaaz + j);
    }

    static void zza(byte[] bArr, long j, byte b) {
        zzaax.zze(bArr, zzaaz + j, b);
    }

    static Unsafe zzil() {
        try {
            return (Unsafe) AccessController.doPrivileged(new zzjr());
        } catch (Throwable unused) {
            return null;
        }
    }

    private static boolean zzim() {
        Unsafe unsafe = zzyz;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("arrayBaseOffset", Class.class);
            cls.getMethod("arrayIndexScale", Class.class);
            cls.getMethod("getInt", Object.class, Long.TYPE);
            cls.getMethod("putInt", Object.class, Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            cls.getMethod("putLong", Object.class, Long.TYPE, Long.TYPE);
            cls.getMethod("getObject", Object.class, Long.TYPE);
            cls.getMethod("putObject", Object.class, Long.TYPE, Object.class);
            if (zzfa.zzdr()) {
                return true;
            }
            cls.getMethod("getByte", Object.class, Long.TYPE);
            cls.getMethod("putByte", Object.class, Long.TYPE, Byte.TYPE);
            cls.getMethod("getBoolean", Object.class, Long.TYPE);
            cls.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE);
            cls.getMethod("getFloat", Object.class, Long.TYPE);
            cls.getMethod("putFloat", Object.class, Long.TYPE, Float.TYPE);
            cls.getMethod("getDouble", Object.class, Long.TYPE);
            cls.getMethod("putDouble", Object.class, Long.TYPE, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    private static boolean zzin() {
        Unsafe unsafe = zzyz;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            if (zzio() == null) {
                return false;
            }
            if (zzfa.zzdr()) {
                return true;
            }
            cls.getMethod("getByte", Long.TYPE);
            cls.getMethod("putByte", Long.TYPE, Byte.TYPE);
            cls.getMethod("getInt", Long.TYPE);
            cls.getMethod("putInt", Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Long.TYPE);
            cls.getMethod("putLong", Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Long.TYPE, Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    private static boolean zzk(Class<?> cls) {
        if (!zzfa.zzdr()) {
            return false;
        }
        try {
            Class<?> cls2 = zzrs;
            cls2.getMethod("peekLong", cls, Boolean.TYPE);
            cls2.getMethod("pokeLong", cls, Long.TYPE, Boolean.TYPE);
            cls2.getMethod("pokeInt", cls, Integer.TYPE, Boolean.TYPE);
            cls2.getMethod("peekInt", cls, Boolean.TYPE);
            cls2.getMethod("pokeByte", cls, Byte.TYPE);
            cls2.getMethod("peekByte", cls);
            cls2.getMethod("pokeByteArray", cls, byte[].class, Integer.TYPE, Integer.TYPE);
            cls2.getMethod("peekByteArray", cls, byte[].class, Integer.TYPE, Integer.TYPE);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    private static Field zzio() {
        Field zzb2;
        if (zzfa.zzdr() && (zzb2 = zzb(Buffer.class, "effectiveDirectAddress")) != null) {
            return zzb2;
        }
        Field zzb3 = zzb(Buffer.class, "address");
        if (zzb3 == null || zzb3.getType() != Long.TYPE) {
            return null;
        }
        return zzb3;
    }

    private static Field zzb(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static byte zzq(Object obj, long j) {
        return (byte) (zzk(obj, -4 & j) >>> ((int) (((~j) & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static byte zzr(Object obj, long j) {
        return (byte) (zzk(obj, -4 & j) >>> ((int) ((j & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static void zza(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int zzk = zzk(obj, j2);
        int i = ((~((int) j)) & 3) << 3;
        zzb(obj, j2, ((255 & b) << i) | (zzk & (~(255 << i))));
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int i = (((int) j) & 3) << 3;
        zzb(obj, j2, ((255 & b) << i) | (zzk(obj, j2) & (~(255 << i))));
    }

    /* access modifiers changed from: private */
    public static boolean zzs(Object obj, long j) {
        return zzq(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    public static boolean zzt(Object obj, long j) {
        return zzr(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, boolean z) {
        zza(obj, j, z ? (byte) 1 : 0);
    }

    /* access modifiers changed from: private */
    public static void zzc(Object obj, long j, boolean z) {
        zzb(obj, j, z ? (byte) 1 : 0);
    }

    static {
        Unsafe zzil = zzil();
        zzyz = zzil;
        boolean zzk = zzk(Long.TYPE);
        zzaav = zzk;
        boolean zzk2 = zzk(Integer.TYPE);
        zzaaw = zzk2;
        zzd zzd2 = null;
        if (zzil != null) {
            if (!zzfa.zzdr()) {
                zzd2 = new zzb(zzil);
            } else if (zzk) {
                zzd2 = new zzc(zzil);
            } else if (zzk2) {
                zzd2 = new zza(zzil);
            }
        }
        zzaax = zzd2;
        long zzi = (long) zzi(byte[].class);
        zzaaz = zzi;
        Field zzio = zzio();
        zzabm = (zzio == null || zzd2 == null) ? -1 : zzd2.zzabr.objectFieldOffset(zzio);
        zzabn = (int) (7 & zzi);
    }
}
