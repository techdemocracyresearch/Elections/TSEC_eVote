package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzkv implements zzcu<zzku> {
    private static zzkv zzagw = new zzkv();
    private final zzcu<zzku> zzagt;

    public static boolean zzjp() {
        return ((zzku) zzagw.get()).zzjp();
    }

    public static boolean zzjq() {
        return ((zzku) zzagw.get()).zzjq();
    }

    private zzkv(zzcu<zzku> zzcu) {
        this.zzagt = zzcx.zza(zzcu);
    }

    public zzkv() {
        this(zzcx.zze(new zzkw()));
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.google.android.gms.internal.vision.zzcu
    public final /* synthetic */ zzku get() {
        return this.zzagt.get();
    }
}
