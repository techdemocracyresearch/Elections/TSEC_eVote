package com.google.android.gms.internal.vision;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzfy implements zzis {
    private int tag;
    private int zzsc;
    private final zzft zzsv;
    private int zzsw = 0;

    public static zzfy zza(zzft zzft) {
        if (zzft.zzso != null) {
            return zzft.zzso;
        }
        return new zzfy(zzft);
    }

    private zzfy(zzft zzft) {
        zzft zzft2 = (zzft) zzgt.zza(zzft, "input");
        this.zzsv = zzft2;
        zzft2.zzso = this;
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzdu() throws IOException {
        int i = this.zzsw;
        if (i != 0) {
            this.tag = i;
            this.zzsw = 0;
        } else {
            this.tag = this.zzsv.zzex();
        }
        int i2 = this.tag;
        if (i2 == 0 || i2 == this.zzsc) {
            return Integer.MAX_VALUE;
        }
        return i2 >>> 3;
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int getTag() {
        return this.tag;
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final boolean zzdv() throws IOException {
        int i;
        if (this.zzsv.zzdt() || (i = this.tag) == this.zzsc) {
            return false;
        }
        return this.zzsv.zzas(i);
    }

    private final void zzak(int i) throws IOException {
        if ((this.tag & 7) != i) {
            throw zzhc.zzgr();
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final double readDouble() throws IOException {
        zzak(1);
        return this.zzsv.readDouble();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final float readFloat() throws IOException {
        zzak(5);
        return this.zzsv.readFloat();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzdw() throws IOException {
        zzak(0);
        return this.zzsv.zzdw();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzdx() throws IOException {
        zzak(0);
        return this.zzsv.zzdx();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzdy() throws IOException {
        zzak(0);
        return this.zzsv.zzdy();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzdz() throws IOException {
        zzak(1);
        return this.zzsv.zzdz();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzea() throws IOException {
        zzak(5);
        return this.zzsv.zzea();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final boolean zzeb() throws IOException {
        zzak(0);
        return this.zzsv.zzeb();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final String readString() throws IOException {
        zzak(2);
        return this.zzsv.readString();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final String zzec() throws IOException {
        zzak(2);
        return this.zzsv.zzec();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> T zza(Class<T> cls, zzgd zzgd) throws IOException {
        zzak(2);
        return (T) zzb(zzin.zzho().zzf(cls), zzgd);
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> T zza(zzir<T> zzir, zzgd zzgd) throws IOException {
        zzak(2);
        return (T) zzb(zzir, zzgd);
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> T zzb(Class<T> cls, zzgd zzgd) throws IOException {
        zzak(3);
        return (T) zzd(zzin.zzho().zzf(cls), zzgd);
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> T zzc(zzir<T> zzir, zzgd zzgd) throws IOException {
        zzak(3);
        return (T) zzd(zzir, zzgd);
    }

    private final <T> T zzb(zzir<T> zzir, zzgd zzgd) throws IOException {
        int zzee = this.zzsv.zzee();
        if (this.zzsv.zzsl < this.zzsv.zzsm) {
            int zzat = this.zzsv.zzat(zzee);
            T newInstance = zzir.newInstance();
            this.zzsv.zzsl++;
            zzir.zza(newInstance, this, zzgd);
            zzir.zzh(newInstance);
            this.zzsv.zzar(0);
            zzft zzft = this.zzsv;
            zzft.zzsl--;
            this.zzsv.zzau(zzat);
            return newInstance;
        }
        throw new zzhc("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    private final <T> T zzd(zzir<T> zzir, zzgd zzgd) throws IOException {
        int i = this.zzsc;
        this.zzsc = ((this.tag >>> 3) << 3) | 4;
        try {
            T newInstance = zzir.newInstance();
            zzir.zza(newInstance, this, zzgd);
            zzir.zzh(newInstance);
            if (this.tag == this.zzsc) {
                return newInstance;
            }
            throw zzhc.zzgs();
        } finally {
            this.zzsc = i;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final zzfh zzed() throws IOException {
        zzak(2);
        return this.zzsv.zzed();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzee() throws IOException {
        zzak(0);
        return this.zzsv.zzee();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzef() throws IOException {
        zzak(0);
        return this.zzsv.zzef();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzeg() throws IOException {
        zzak(5);
        return this.zzsv.zzeg();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzeh() throws IOException {
        zzak(1);
        return this.zzsv.zzeh();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzei() throws IOException {
        zzak(0);
        return this.zzsv.zzei();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzej() throws IOException {
        zzak(0);
        return this.zzsv.zzej();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zza(List<Double> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzgb) {
            zzgb zzgb = (zzgb) list;
            int i = this.tag & 7;
            if (i == 1) {
                do {
                    zzgb.zzc(this.zzsv.readDouble());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzee = this.zzsv.zzee();
                zzal(zzee);
                int zzez = this.zzsv.zzez() + zzee;
                do {
                    zzgb.zzc(this.zzsv.readDouble());
                } while (this.zzsv.zzez() < zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 1) {
                do {
                    list.add(Double.valueOf(this.zzsv.readDouble()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzee2 = this.zzsv.zzee();
                zzal(zzee2);
                int zzez2 = this.zzsv.zzez() + zzee2;
                do {
                    list.add(Double.valueOf(this.zzsv.readDouble()));
                } while (this.zzsv.zzez() < zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzb(List<Float> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzgo) {
            zzgo zzgo = (zzgo) list;
            int i = this.tag & 7;
            if (i == 2) {
                int zzee = this.zzsv.zzee();
                zzam(zzee);
                int zzez = this.zzsv.zzez() + zzee;
                do {
                    zzgo.zzu(this.zzsv.readFloat());
                } while (this.zzsv.zzez() < zzez);
            } else if (i == 5) {
                do {
                    zzgo.zzu(this.zzsv.readFloat());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 2) {
                int zzee2 = this.zzsv.zzee();
                zzam(zzee2);
                int zzez2 = this.zzsv.zzez() + zzee2;
                do {
                    list.add(Float.valueOf(this.zzsv.readFloat()));
                } while (this.zzsv.zzez() < zzez2);
            } else if (i2 == 5) {
                do {
                    list.add(Float.valueOf(this.zzsv.readFloat()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzc(List<Long> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzhq.zzac(this.zzsv.zzdw());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzez = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    zzhq.zzac(this.zzsv.zzdw());
                } while (this.zzsv.zzez() < zzez);
                zzan(zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.zzsv.zzdw()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzez2 = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    list.add(Long.valueOf(this.zzsv.zzdw()));
                } while (this.zzsv.zzez() < zzez2);
                zzan(zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzd(List<Long> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzhq.zzac(this.zzsv.zzdx());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzez = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    zzhq.zzac(this.zzsv.zzdx());
                } while (this.zzsv.zzez() < zzez);
                zzan(zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.zzsv.zzdx()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzez2 = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    list.add(Long.valueOf(this.zzsv.zzdx()));
                } while (this.zzsv.zzez() < zzez2);
                zzan(zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zze(List<Integer> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzgu.zzbm(this.zzsv.zzdy());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzez = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    zzgu.zzbm(this.zzsv.zzdy());
                } while (this.zzsv.zzez() < zzez);
                zzan(zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzsv.zzdy()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzez2 = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    list.add(Integer.valueOf(this.zzsv.zzdy()));
                } while (this.zzsv.zzez() < zzez2);
                zzan(zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzf(List<Long> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i = this.tag & 7;
            if (i == 1) {
                do {
                    zzhq.zzac(this.zzsv.zzdz());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzee = this.zzsv.zzee();
                zzal(zzee);
                int zzez = this.zzsv.zzez() + zzee;
                do {
                    zzhq.zzac(this.zzsv.zzdz());
                } while (this.zzsv.zzez() < zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.zzsv.zzdz()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzee2 = this.zzsv.zzee();
                zzal(zzee2);
                int zzez2 = this.zzsv.zzez() + zzee2;
                do {
                    list.add(Long.valueOf(this.zzsv.zzdz()));
                } while (this.zzsv.zzez() < zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzg(List<Integer> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i = this.tag & 7;
            if (i == 2) {
                int zzee = this.zzsv.zzee();
                zzam(zzee);
                int zzez = this.zzsv.zzez() + zzee;
                do {
                    zzgu.zzbm(this.zzsv.zzea());
                } while (this.zzsv.zzez() < zzez);
            } else if (i == 5) {
                do {
                    zzgu.zzbm(this.zzsv.zzea());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 2) {
                int zzee2 = this.zzsv.zzee();
                zzam(zzee2);
                int zzez2 = this.zzsv.zzez() + zzee2;
                do {
                    list.add(Integer.valueOf(this.zzsv.zzea()));
                } while (this.zzsv.zzez() < zzez2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.zzsv.zzea()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzh(List<Boolean> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzff) {
            zzff zzff = (zzff) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzff.addBoolean(this.zzsv.zzeb());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzez = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    zzff.addBoolean(this.zzsv.zzeb());
                } while (this.zzsv.zzez() < zzez);
                zzan(zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Boolean.valueOf(this.zzsv.zzeb()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzez2 = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    list.add(Boolean.valueOf(this.zzsv.zzeb()));
                } while (this.zzsv.zzez() < zzez2);
                zzan(zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void readStringList(List<String> list) throws IOException {
        zza(list, false);
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzi(List<String> list) throws IOException {
        zza(list, true);
    }

    private final void zza(List<String> list, boolean z) throws IOException {
        int zzex;
        int zzex2;
        if ((this.tag & 7) != 2) {
            throw zzhc.zzgr();
        } else if (!(list instanceof zzhj) || z) {
            do {
                list.add(z ? zzec() : readString());
                if (!this.zzsv.zzdt()) {
                    zzex = this.zzsv.zzex();
                } else {
                    return;
                }
            } while (zzex == this.tag);
            this.zzsw = zzex;
        } else {
            zzhj zzhj = (zzhj) list;
            do {
                zzhj.zzc(zzed());
                if (!this.zzsv.zzdt()) {
                    zzex2 = this.zzsv.zzex();
                } else {
                    return;
                }
            } while (zzex2 == this.tag);
            this.zzsw = zzex2;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> void zza(List<T> list, zzir<T> zzir, zzgd zzgd) throws IOException {
        int zzex;
        int i = this.tag;
        if ((i & 7) == 2) {
            do {
                list.add(zzb(zzir, zzgd));
                if (!this.zzsv.zzdt() && this.zzsw == 0) {
                    zzex = this.zzsv.zzex();
                } else {
                    return;
                }
            } while (zzex == i);
            this.zzsw = zzex;
            return;
        }
        throw zzhc.zzgr();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> void zzb(List<T> list, zzir<T> zzir, zzgd zzgd) throws IOException {
        int zzex;
        int i = this.tag;
        if ((i & 7) == 3) {
            do {
                list.add(zzd(zzir, zzgd));
                if (!this.zzsv.zzdt() && this.zzsw == 0) {
                    zzex = this.zzsv.zzex();
                } else {
                    return;
                }
            } while (zzex == i);
            this.zzsw = zzex;
            return;
        }
        throw zzhc.zzgr();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzj(List<zzfh> list) throws IOException {
        int zzex;
        if ((this.tag & 7) == 2) {
            do {
                list.add(zzed());
                if (!this.zzsv.zzdt()) {
                    zzex = this.zzsv.zzex();
                } else {
                    return;
                }
            } while (zzex == this.tag);
            this.zzsw = zzex;
            return;
        }
        throw zzhc.zzgr();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzk(List<Integer> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzgu.zzbm(this.zzsv.zzee());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzez = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    zzgu.zzbm(this.zzsv.zzee());
                } while (this.zzsv.zzez() < zzez);
                zzan(zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzsv.zzee()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzez2 = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    list.add(Integer.valueOf(this.zzsv.zzee()));
                } while (this.zzsv.zzez() < zzez2);
                zzan(zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzl(List<Integer> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzgu.zzbm(this.zzsv.zzef());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzez = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    zzgu.zzbm(this.zzsv.zzef());
                } while (this.zzsv.zzez() < zzez);
                zzan(zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzsv.zzef()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzez2 = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    list.add(Integer.valueOf(this.zzsv.zzef()));
                } while (this.zzsv.zzez() < zzez2);
                zzan(zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzm(List<Integer> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i = this.tag & 7;
            if (i == 2) {
                int zzee = this.zzsv.zzee();
                zzam(zzee);
                int zzez = this.zzsv.zzez() + zzee;
                do {
                    zzgu.zzbm(this.zzsv.zzeg());
                } while (this.zzsv.zzez() < zzez);
            } else if (i == 5) {
                do {
                    zzgu.zzbm(this.zzsv.zzeg());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 2) {
                int zzee2 = this.zzsv.zzee();
                zzam(zzee2);
                int zzez2 = this.zzsv.zzez() + zzee2;
                do {
                    list.add(Integer.valueOf(this.zzsv.zzeg()));
                } while (this.zzsv.zzez() < zzez2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.zzsv.zzeg()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzn(List<Long> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i = this.tag & 7;
            if (i == 1) {
                do {
                    zzhq.zzac(this.zzsv.zzeh());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzee = this.zzsv.zzee();
                zzal(zzee);
                int zzez = this.zzsv.zzez() + zzee;
                do {
                    zzhq.zzac(this.zzsv.zzeh());
                } while (this.zzsv.zzez() < zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.zzsv.zzeh()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzee2 = this.zzsv.zzee();
                zzal(zzee2);
                int zzez2 = this.zzsv.zzez() + zzee2;
                do {
                    list.add(Long.valueOf(this.zzsv.zzeh()));
                } while (this.zzsv.zzez() < zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzo(List<Integer> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzgu.zzbm(this.zzsv.zzei());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzez = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    zzgu.zzbm(this.zzsv.zzei());
                } while (this.zzsv.zzez() < zzez);
                zzan(zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzsv.zzei()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzez2 = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    list.add(Integer.valueOf(this.zzsv.zzei()));
                } while (this.zzsv.zzez() < zzez2);
                zzan(zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzp(List<Long> list) throws IOException {
        int zzex;
        int zzex2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzhq.zzac(this.zzsv.zzej());
                    if (!this.zzsv.zzdt()) {
                        zzex2 = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex2 == this.tag);
                this.zzsw = zzex2;
            } else if (i == 2) {
                int zzez = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    zzhq.zzac(this.zzsv.zzej());
                } while (this.zzsv.zzez() < zzez);
                zzan(zzez);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.zzsv.zzej()));
                    if (!this.zzsv.zzdt()) {
                        zzex = this.zzsv.zzex();
                    } else {
                        return;
                    }
                } while (zzex == this.tag);
                this.zzsw = zzex;
            } else if (i2 == 2) {
                int zzez2 = this.zzsv.zzez() + this.zzsv.zzee();
                do {
                    list.add(Long.valueOf(this.zzsv.zzej()));
                } while (this.zzsv.zzez() < zzez2);
                zzan(zzez2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    private static void zzal(int i) throws IOException {
        if ((i & 7) != 0) {
            throw zzhc.zzgs();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.util.Map<K, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.vision.zzis
    public final <K, V> void zza(Map<K, V> map, zzht<K, V> zzht, zzgd zzgd) throws IOException {
        zzak(2);
        int zzat = this.zzsv.zzat(this.zzsv.zzee());
        Object obj = zzht.zzyt;
        Object obj2 = zzht.zzgd;
        while (true) {
            try {
                int zzdu = zzdu();
                if (zzdu == Integer.MAX_VALUE || this.zzsv.zzdt()) {
                    map.put(obj, obj2);
                } else if (zzdu == 1) {
                    obj = zza(zzht.zzys, (Class<?>) null, (zzgd) null);
                } else if (zzdu != 2) {
                    try {
                        if (!zzdv()) {
                            throw new zzhc("Unable to parse map entry.");
                        }
                    } catch (zzhb unused) {
                        if (!zzdv()) {
                            throw new zzhc("Unable to parse map entry.");
                        }
                    }
                } else {
                    obj2 = zza(zzht.zzyu, zzht.zzgd.getClass(), zzgd);
                }
            } finally {
                this.zzsv.zzau(zzat);
            }
        }
        map.put(obj, obj2);
    }

    private final Object zza(zzka zzka, Class<?> cls, zzgd zzgd) throws IOException {
        switch (zzfx.zzrx[zzka.ordinal()]) {
            case 1:
                return Boolean.valueOf(zzeb());
            case 2:
                return zzed();
            case 3:
                return Double.valueOf(readDouble());
            case 4:
                return Integer.valueOf(zzef());
            case 5:
                return Integer.valueOf(zzea());
            case 6:
                return Long.valueOf(zzdz());
            case 7:
                return Float.valueOf(readFloat());
            case 8:
                return Integer.valueOf(zzdy());
            case 9:
                return Long.valueOf(zzdx());
            case 10:
                return zza(cls, zzgd);
            case 11:
                return Integer.valueOf(zzeg());
            case 12:
                return Long.valueOf(zzeh());
            case 13:
                return Integer.valueOf(zzei());
            case 14:
                return Long.valueOf(zzej());
            case 15:
                return zzec();
            case 16:
                return Integer.valueOf(zzee());
            case 17:
                return Long.valueOf(zzdw());
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    private static void zzam(int i) throws IOException {
        if ((i & 3) != 0) {
            throw zzhc.zzgs();
        }
    }

    private final void zzan(int i) throws IOException {
        if (this.zzsv.zzez() != i) {
            throw zzhc.zzgm();
        }
    }
}
