package com.google.android.gms.internal.vision;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzin {
    private static final zzin zzzt = new zzin();
    private final zziu zzzu = new zzhp();
    private final ConcurrentMap<Class<?>, zzir<?>> zzzv = new ConcurrentHashMap();

    public static zzin zzho() {
        return zzzt;
    }

    public final <T> zzir<T> zzf(Class<T> cls) {
        zzgt.zza(cls, "messageType");
        zzir<T> zzir = (zzir<T>) this.zzzv.get(cls);
        if (zzir != null) {
            return zzir;
        }
        zzir<T> zze = this.zzzu.zze(cls);
        zzgt.zza(cls, "messageType");
        zzgt.zza(zze, "schema");
        zzir<T> zzir2 = (zzir<T>) this.zzzv.putIfAbsent(cls, zze);
        return zzir2 != null ? zzir2 : zze;
    }

    public final <T> zzir<T> zzv(T t) {
        return zzf(t.getClass());
    }

    private zzin() {
    }
}
