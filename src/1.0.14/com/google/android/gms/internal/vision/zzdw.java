package com.google.android.gms.internal.vision;

import java.util.List;
import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzdw extends zzdv {
    private final zzdu zzml = new zzdu();

    zzdw() {
    }

    @Override // com.google.android.gms.internal.vision.zzdv
    public final void zza(Throwable th, Throwable th2) {
        if (th2 != th) {
            Objects.requireNonNull(th2, "The suppressed exception cannot be null.");
            this.zzml.zza(th, true).add(th2);
            return;
        }
        throw new IllegalArgumentException("Self suppression is not allowed.", th2);
    }

    @Override // com.google.android.gms.internal.vision.zzdv
    public final void zza(Throwable th) {
        th.printStackTrace();
        List<Throwable> zza = this.zzml.zza(th, false);
        if (zza != null) {
            synchronized (zza) {
                for (Throwable th2 : zza) {
                    System.err.print("Suppressed: ");
                    th2.printStackTrace();
                }
            }
        }
    }
}
