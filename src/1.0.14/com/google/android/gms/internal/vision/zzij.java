package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzgs;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzij implements zzik {
    zzij() {
    }

    @Override // com.google.android.gms.internal.vision.zzik
    public final Object newInstance(Object obj) {
        return ((zzgs) obj).zza(zzgs.zzf.zzwu, (Object) null, (Object) null);
    }
}
