package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public abstract class zzev implements zzih {
    private boolean zzrp = true;
    private int zzrq = -1;

    @Override // com.google.android.gms.internal.vision.zzih
    /* renamed from: zzdm */
    public final zzih clone() {
        throw new UnsupportedOperationException("clone() should be implemented by subclasses.");
    }
}
