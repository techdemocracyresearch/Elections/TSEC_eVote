package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzdy extends zzdv {
    zzdy() {
    }

    @Override // com.google.android.gms.internal.vision.zzdv
    public final void zza(Throwable th, Throwable th2) {
        th.addSuppressed(th2);
    }

    @Override // com.google.android.gms.internal.vision.zzdv
    public final void zza(Throwable th) {
        th.printStackTrace();
    }
}
