package com.google.android.gms.internal.vision;

import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public interface zzhv {
    int zzb(int i, Object obj, Object obj2);

    Object zzc(Object obj, Object obj2);

    Map<?, ?> zzl(Object obj);

    Map<?, ?> zzm(Object obj);

    boolean zzn(Object obj);

    Object zzo(Object obj);

    Object zzp(Object obj);

    zzht<?, ?> zzq(Object obj);
}
