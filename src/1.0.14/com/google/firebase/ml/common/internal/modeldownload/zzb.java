package com.google.firebase.ml.common.internal.modeldownload;

import java.io.BufferedWriter;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final /* synthetic */ class zzb implements zze {
    private final List zzbkq;

    zzb(List list) {
        this.zzbkq = list;
    }

    @Override // com.google.firebase.ml.common.internal.modeldownload.zze
    public final void zza(BufferedWriter bufferedWriter) {
        zza.zza(this.zzbkq, bufferedWriter);
    }
}
