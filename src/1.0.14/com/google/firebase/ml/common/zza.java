package com.google.firebase.ml.common;

import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.ComponentFactory;
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final /* synthetic */ class zza implements ComponentFactory {
    static final ComponentFactory zzbil = new zza();

    private zza() {
    }

    @Override // com.google.firebase.components.ComponentFactory
    public final Object create(ComponentContainer componentContainer) {
        return new FirebaseModelManager(componentContainer.setOf(FirebaseModelManager.RemoteModelManagerRegistration.class));
    }
}
