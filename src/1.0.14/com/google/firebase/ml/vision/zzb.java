package com.google.firebase.ml.vision;

import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.ComponentFactory;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zzb implements ComponentFactory {
    static final ComponentFactory zzbil = new zzb();

    private zzb() {
    }

    @Override // com.google.firebase.components.ComponentFactory
    public final Object create(ComponentContainer componentContainer) {
        return new FirebaseVision((zzqf) componentContainer.get(zzqf.class));
    }
}
