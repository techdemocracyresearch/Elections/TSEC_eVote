package com.google.firebase.ml.vision.barcode.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.firebase_ml.zzb;
import com.google.android.gms.internal.firebase_ml.zzd;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzj extends zzb implements zzi {
    zzj(IBinder iBinder) {
        super(iBinder, "com.google.firebase.ml.vision.barcode.internal.IBarcodeDetectorCreator");
    }

    @Override // com.google.firebase.ml.vision.barcode.internal.zzi
    public final IBarcodeDetector newBarcodeDetector(BarcodeDetectorOptionsParcel barcodeDetectorOptionsParcel) throws RemoteException {
        IBarcodeDetector iBarcodeDetector;
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzd.zza(obtainAndWriteInterfaceToken, barcodeDetectorOptionsParcel);
        Parcel zza = zza(1, obtainAndWriteInterfaceToken);
        IBinder readStrongBinder = zza.readStrongBinder();
        if (readStrongBinder == null) {
            iBarcodeDetector = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.firebase.ml.vision.barcode.internal.IBarcodeDetector");
            if (queryLocalInterface instanceof IBarcodeDetector) {
                iBarcodeDetector = (IBarcodeDetector) queryLocalInterface;
            } else {
                iBarcodeDetector = new zzg(readStrongBinder);
            }
        }
        zza.recycle();
        return iBarcodeDetector;
    }
}
