package com.google.firebase.ml.vision;

import com.google.android.gms.internal.firebase_ml.zzmw;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.firebase.components.Component;
import com.google.firebase.components.ComponentRegistrar;
import com.google.firebase.components.Dependency;
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager;
import com.google.firebase.ml.vision.automl.internal.zzb;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class VisionRegistrar implements ComponentRegistrar {
    @Override // com.google.firebase.components.ComponentRegistrar
    public List<Component<?>> getComponents() {
        return zzmw.zza(Component.builder(FirebaseVision.class).add(Dependency.required(zzqf.class)).factory(zzb.zzbil).build(), Component.builder(zzb.class).add(Dependency.required(zzqg.zza.class)).add(Dependency.required(zzqf.class)).factory(zza.zzbil).build(), Component.intoSetBuilder(FirebaseModelManager.RemoteModelManagerRegistration.class).add(Dependency.requiredProvider(zzb.class)).factory(zzc.zzbil).build());
    }
}
