package com.google.firebase.ml.vision.document;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionCloudDocumentRecognizerOptions {
    private final boolean zzbra;
    private final List<String> zzbsj;

    public List<String> getHintedLanguages() {
        return this.zzbsj;
    }

    public final boolean isEnforceCertFingerprintMatch() {
        return this.zzbra;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private boolean zzbra = false;
        private List<String> zzbsj = new ArrayList();

        public Builder setLanguageHints(List<String> list) {
            Preconditions.checkNotNull(list, "Provided hinted languages can not be null");
            this.zzbsj = list;
            Collections.sort(list);
            return this;
        }

        public Builder enforceCertFingerprintMatch() {
            this.zzbra = true;
            return this;
        }

        public FirebaseVisionCloudDocumentRecognizerOptions build() {
            return new FirebaseVisionCloudDocumentRecognizerOptions(this.zzbsj, this.zzbra);
        }
    }

    private FirebaseVisionCloudDocumentRecognizerOptions(List<String> list, boolean z) {
        Preconditions.checkNotNull(list, "Provided hinted languages can not be null");
        this.zzbsj = list;
        this.zzbra = z;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseVisionCloudDocumentRecognizerOptions)) {
            return false;
        }
        FirebaseVisionCloudDocumentRecognizerOptions firebaseVisionCloudDocumentRecognizerOptions = (FirebaseVisionCloudDocumentRecognizerOptions) obj;
        return this.zzbsj.equals(firebaseVisionCloudDocumentRecognizerOptions.getHintedLanguages()) && this.zzbra == firebaseVisionCloudDocumentRecognizerOptions.zzbra;
    }

    public int hashCode() {
        return Objects.hashCode(this.zzbsj, Boolean.valueOf(this.zzbra));
    }
}
