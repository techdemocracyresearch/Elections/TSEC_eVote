package com.google.firebase.ml.vision.objects;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqh;
import com.google.android.gms.internal.firebase_ml.zzrz;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.objects.internal.zze;
import java.io.Closeable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionObjectDetector extends zzrz<List<FirebaseVisionObject>> implements Closeable {
    private static final Map<zzqh<FirebaseVisionObjectDetectorOptions>, FirebaseVisionObjectDetector> zzbim = new HashMap();

    public static synchronized FirebaseVisionObjectDetector zza(zzqf zzqf, FirebaseVisionObjectDetectorOptions firebaseVisionObjectDetectorOptions) {
        FirebaseVisionObjectDetector firebaseVisionObjectDetector;
        synchronized (FirebaseVisionObjectDetector.class) {
            Preconditions.checkNotNull(zzqf, "You must provide a valid MlKitContext.");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Firebase app name must not be null");
            Preconditions.checkNotNull(zzqf.getApplicationContext(), "You must provide a valid Context.");
            Preconditions.checkNotNull(firebaseVisionObjectDetectorOptions, "You must provide a valid FirebaseVisionObjectDetectorOptions.");
            zzqh<FirebaseVisionObjectDetectorOptions> zzj = zzqh.zzj(zzqf.getPersistenceKey(), firebaseVisionObjectDetectorOptions);
            Map<zzqh<FirebaseVisionObjectDetectorOptions>, FirebaseVisionObjectDetector> map = zzbim;
            firebaseVisionObjectDetector = map.get(zzj);
            if (firebaseVisionObjectDetector == null) {
                firebaseVisionObjectDetector = new FirebaseVisionObjectDetector(zzqf, firebaseVisionObjectDetectorOptions);
                map.put(zzj, firebaseVisionObjectDetector);
            }
        }
        return firebaseVisionObjectDetector;
    }

    private FirebaseVisionObjectDetector(zzqf zzqf, FirebaseVisionObjectDetectorOptions firebaseVisionObjectDetectorOptions) {
        super(zzqf, new zze(zzqf, firebaseVisionObjectDetectorOptions));
    }

    public Task<List<FirebaseVisionObject>> processImage(FirebaseVisionImage firebaseVisionImage) {
        firebaseVisionImage.zzqn();
        return super.zza(firebaseVisionImage, false, true);
    }
}
