package com.google.firebase.ml.vision.objects.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.firebase_ml.zzb;
import com.google.android.gms.internal.firebase_ml.zzd;
import com.google.android.gms.internal.firebase_ml.zzsb;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zza extends zzb implements IObjectDetector {
    zza(IBinder iBinder) {
        super(iBinder, "com.google.firebase.ml.vision.objects.internal.IObjectDetector");
    }

    @Override // com.google.firebase.ml.vision.objects.internal.IObjectDetector
    public final zzj[] zzc(IObjectWrapper iObjectWrapper, zzsb zzsb) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzd.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzd.zza(obtainAndWriteInterfaceToken, zzsb);
        Parcel zza = zza(1, obtainAndWriteInterfaceToken);
        zzj[] zzjArr = (zzj[]) zza.createTypedArray(zzj.CREATOR);
        zza.recycle();
        return zzjArr;
    }

    @Override // com.google.firebase.ml.vision.objects.internal.IObjectDetector
    public final void start() throws RemoteException {
        zzb(2, obtainAndWriteInterfaceToken());
    }

    @Override // com.google.firebase.ml.vision.objects.internal.IObjectDetector
    public final void stop() throws RemoteException {
        zzb(3, obtainAndWriteInterfaceToken());
    }
}
