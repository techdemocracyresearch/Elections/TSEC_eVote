package com.google.firebase.ml.vision.automl.internal;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zzd implements OnCompleteListener {
    private final zzb zzbps;

    zzd(zzb zzb) {
        this.zzbps = zzb;
    }

    @Override // com.google.android.gms.tasks.OnCompleteListener
    public final void onComplete(Task task) {
        this.zzbps.zzd(task);
    }
}
