package com.google.firebase.ml.vision.automl.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzl extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzl> CREATOR = new zzk();
    public final String text;
    public final String zzbpu;
    public final float zzbpv;

    public zzl(String str, String str2, float f) {
        this.zzbpu = str;
        this.text = str2;
        this.zzbpv = f;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zzbpu, false);
        SafeParcelWriter.writeString(parcel, 2, this.text, false);
        SafeParcelWriter.writeFloat(parcel, 3, this.zzbpv);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzl)) {
            return false;
        }
        zzl zzl = (zzl) obj;
        return Objects.equal(this.zzbpu, zzl.zzbpu) && Objects.equal(this.text, zzl.text) && Float.compare(this.zzbpv, zzl.zzbpv) == 0;
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzbpu, this.text, Float.valueOf(this.zzbpv));
    }
}
