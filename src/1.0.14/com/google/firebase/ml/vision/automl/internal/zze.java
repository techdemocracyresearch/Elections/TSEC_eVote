package com.google.firebase.ml.vision.automl.internal;

import com.google.android.gms.tasks.SuccessContinuation;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.common.internal.modeldownload.zzv;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zze implements SuccessContinuation {
    private final zzv zzbny;

    zze(zzv zzv) {
        this.zzbny = zzv;
    }

    @Override // com.google.android.gms.tasks.SuccessContinuation
    public final Task then(Object obj) {
        return this.zzbny.zzpg();
    }
}
