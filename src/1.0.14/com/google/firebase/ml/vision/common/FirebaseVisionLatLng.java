package com.google.firebase.ml.vision.common;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionLatLng {
    private final double zzbry;
    private final double zzbrz;

    public double getLatitude() {
        return this.zzbry;
    }

    public double getLongitude() {
        return this.zzbrz;
    }

    public FirebaseVisionLatLng(double d, double d2) {
        this.zzbry = d;
        this.zzbrz = d2;
    }
}
