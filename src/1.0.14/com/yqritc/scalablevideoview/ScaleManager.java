package com.yqritc.scalablevideoview;

import android.graphics.Matrix;

public class ScaleManager {
    private Size mVideoSize;
    private Size mViewSize;

    public ScaleManager(Size size, Size size2) {
        this.mViewSize = size;
        this.mVideoSize = size2;
    }

    public Matrix getScaleMatrix(ScalableType scalableType) {
        switch (AnonymousClass1.$SwitchMap$com$yqritc$scalablevideoview$ScalableType[scalableType.ordinal()]) {
            case 1:
                return getNoScale();
            case 2:
                return fitXY();
            case 3:
                return fitCenter();
            case 4:
                return fitStart();
            case 5:
                return fitEnd();
            case 6:
                return getOriginalScale(PivotPoint.LEFT_TOP);
            case 7:
                return getOriginalScale(PivotPoint.LEFT_CENTER);
            case 8:
                return getOriginalScale(PivotPoint.LEFT_BOTTOM);
            case 9:
                return getOriginalScale(PivotPoint.CENTER_TOP);
            case 10:
                return getOriginalScale(PivotPoint.CENTER);
            case 11:
                return getOriginalScale(PivotPoint.CENTER_BOTTOM);
            case 12:
                return getOriginalScale(PivotPoint.RIGHT_TOP);
            case 13:
                return getOriginalScale(PivotPoint.RIGHT_CENTER);
            case 14:
                return getOriginalScale(PivotPoint.RIGHT_BOTTOM);
            case 15:
                return getCropScale(PivotPoint.LEFT_TOP);
            case 16:
                return getCropScale(PivotPoint.LEFT_CENTER);
            case 17:
                return getCropScale(PivotPoint.LEFT_BOTTOM);
            case 18:
                return getCropScale(PivotPoint.CENTER_TOP);
            case 19:
                return getCropScale(PivotPoint.CENTER);
            case 20:
                return getCropScale(PivotPoint.CENTER_BOTTOM);
            case 21:
                return getCropScale(PivotPoint.RIGHT_TOP);
            case 22:
                return getCropScale(PivotPoint.RIGHT_CENTER);
            case 23:
                return getCropScale(PivotPoint.RIGHT_BOTTOM);
            case 24:
                return startInside();
            case 25:
                return centerInside();
            case 26:
                return endInside();
            default:
                return null;
        }
    }

    private Matrix getMatrix(float f, float f2, float f3, float f4) {
        Matrix matrix = new Matrix();
        matrix.setScale(f, f2, f3, f4);
        return matrix;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.yqritc.scalablevideoview.ScaleManager$1  reason: invalid class name */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$yqritc$scalablevideoview$PivotPoint;
        static final /* synthetic */ int[] $SwitchMap$com$yqritc$scalablevideoview$ScalableType;

        /* JADX WARNING: Can't wrap try/catch for region: R(71:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|23|(2:25|26)|27|(2:29|30)|31|(2:33|34)|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|(3:87|88|90)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(72:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|(2:33|34)|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|(3:87|88|90)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(73:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|(2:33|34)|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|(3:87|88|90)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(74:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|33|34|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|(3:87|88|90)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(75:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|33|34|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|(3:87|88|90)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(76:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|29|30|31|33|34|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|(3:87|88|90)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(77:0|(2:1|2)|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|29|30|31|33|34|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|(3:87|88|90)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(79:0|(2:1|2)|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|29|30|31|33|34|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|90) */
        /* JADX WARNING: Can't wrap try/catch for region: R(81:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|25|26|27|29|30|31|33|34|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|90) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x007d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x0087 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x0091 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x009b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00a5 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x00af */
        /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00b9 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x00c3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x00cd */
        /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x00d9 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x00e5 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x00f1 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:63:0x00fd */
        /* JADX WARNING: Missing exception handler attribute for start block: B:65:0x0109 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:67:0x0115 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:69:0x0121 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:71:0x012d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x0139 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:75:0x0145 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:77:0x0151 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:79:0x015d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:81:0x0169 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:83:0x0175 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:85:0x0181 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:87:0x018d */
        static {
            int[] iArr = new int[PivotPoint.values().length];
            $SwitchMap$com$yqritc$scalablevideoview$PivotPoint = iArr;
            try {
                iArr[PivotPoint.LEFT_TOP.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$yqritc$scalablevideoview$PivotPoint[PivotPoint.LEFT_CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$yqritc$scalablevideoview$PivotPoint[PivotPoint.LEFT_BOTTOM.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$yqritc$scalablevideoview$PivotPoint[PivotPoint.CENTER_TOP.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$yqritc$scalablevideoview$PivotPoint[PivotPoint.CENTER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$yqritc$scalablevideoview$PivotPoint[PivotPoint.CENTER_BOTTOM.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$yqritc$scalablevideoview$PivotPoint[PivotPoint.RIGHT_TOP.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$com$yqritc$scalablevideoview$PivotPoint[PivotPoint.RIGHT_CENTER.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$com$yqritc$scalablevideoview$PivotPoint[PivotPoint.RIGHT_BOTTOM.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            int[] iArr2 = new int[ScalableType.values().length];
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType = iArr2;
            iArr2[ScalableType.NONE.ordinal()] = 1;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.FIT_XY.ordinal()] = 2;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.FIT_CENTER.ordinal()] = 3;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.FIT_START.ordinal()] = 4;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.FIT_END.ordinal()] = 5;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.LEFT_TOP.ordinal()] = 6;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.LEFT_CENTER.ordinal()] = 7;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.LEFT_BOTTOM.ordinal()] = 8;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.CENTER_TOP.ordinal()] = 9;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.CENTER.ordinal()] = 10;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.CENTER_BOTTOM.ordinal()] = 11;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.RIGHT_TOP.ordinal()] = 12;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.RIGHT_CENTER.ordinal()] = 13;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.RIGHT_BOTTOM.ordinal()] = 14;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.LEFT_TOP_CROP.ordinal()] = 15;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.LEFT_CENTER_CROP.ordinal()] = 16;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.LEFT_BOTTOM_CROP.ordinal()] = 17;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.CENTER_TOP_CROP.ordinal()] = 18;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.CENTER_CROP.ordinal()] = 19;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.CENTER_BOTTOM_CROP.ordinal()] = 20;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.RIGHT_TOP_CROP.ordinal()] = 21;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.RIGHT_CENTER_CROP.ordinal()] = 22;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.RIGHT_BOTTOM_CROP.ordinal()] = 23;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.START_INSIDE.ordinal()] = 24;
            $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.CENTER_INSIDE.ordinal()] = 25;
            try {
                $SwitchMap$com$yqritc$scalablevideoview$ScalableType[ScalableType.END_INSIDE.ordinal()] = 26;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    private Matrix getMatrix(float f, float f2, PivotPoint pivotPoint) {
        switch (AnonymousClass1.$SwitchMap$com$yqritc$scalablevideoview$PivotPoint[pivotPoint.ordinal()]) {
            case 1:
                return getMatrix(f, f2, 0.0f, 0.0f);
            case 2:
                return getMatrix(f, f2, 0.0f, ((float) this.mViewSize.getHeight()) / 2.0f);
            case 3:
                return getMatrix(f, f2, 0.0f, (float) this.mViewSize.getHeight());
            case 4:
                return getMatrix(f, f2, ((float) this.mViewSize.getWidth()) / 2.0f, 0.0f);
            case 5:
                return getMatrix(f, f2, ((float) this.mViewSize.getWidth()) / 2.0f, ((float) this.mViewSize.getHeight()) / 2.0f);
            case 6:
                return getMatrix(f, f2, ((float) this.mViewSize.getWidth()) / 2.0f, (float) this.mViewSize.getHeight());
            case 7:
                return getMatrix(f, f2, (float) this.mViewSize.getWidth(), 0.0f);
            case 8:
                return getMatrix(f, f2, (float) this.mViewSize.getWidth(), ((float) this.mViewSize.getHeight()) / 2.0f);
            case 9:
                return getMatrix(f, f2, (float) this.mViewSize.getWidth(), (float) this.mViewSize.getHeight());
            default:
                throw new IllegalArgumentException("Illegal PivotPoint");
        }
    }

    private Matrix getNoScale() {
        return getMatrix(((float) this.mVideoSize.getWidth()) / ((float) this.mViewSize.getWidth()), ((float) this.mVideoSize.getHeight()) / ((float) this.mViewSize.getHeight()), PivotPoint.LEFT_TOP);
    }

    private Matrix getFitScale(PivotPoint pivotPoint) {
        float width = ((float) this.mViewSize.getWidth()) / ((float) this.mVideoSize.getWidth());
        float height = ((float) this.mViewSize.getHeight()) / ((float) this.mVideoSize.getHeight());
        float min = Math.min(width, height);
        return getMatrix(min / width, min / height, pivotPoint);
    }

    private Matrix fitXY() {
        return getMatrix(1.0f, 1.0f, PivotPoint.LEFT_TOP);
    }

    private Matrix fitStart() {
        return getFitScale(PivotPoint.LEFT_TOP);
    }

    private Matrix fitCenter() {
        return getFitScale(PivotPoint.CENTER);
    }

    private Matrix fitEnd() {
        return getFitScale(PivotPoint.RIGHT_BOTTOM);
    }

    private Matrix getOriginalScale(PivotPoint pivotPoint) {
        return getMatrix(((float) this.mVideoSize.getWidth()) / ((float) this.mViewSize.getWidth()), ((float) this.mVideoSize.getHeight()) / ((float) this.mViewSize.getHeight()), pivotPoint);
    }

    private Matrix getCropScale(PivotPoint pivotPoint) {
        float width = ((float) this.mViewSize.getWidth()) / ((float) this.mVideoSize.getWidth());
        float height = ((float) this.mViewSize.getHeight()) / ((float) this.mVideoSize.getHeight());
        float max = Math.max(width, height);
        return getMatrix(max / width, max / height, pivotPoint);
    }

    private Matrix startInside() {
        if (this.mVideoSize.getHeight() > this.mViewSize.getWidth() || this.mVideoSize.getHeight() > this.mViewSize.getHeight()) {
            return fitStart();
        }
        return getOriginalScale(PivotPoint.LEFT_TOP);
    }

    private Matrix centerInside() {
        if (this.mVideoSize.getHeight() > this.mViewSize.getWidth() || this.mVideoSize.getHeight() > this.mViewSize.getHeight()) {
            return fitCenter();
        }
        return getOriginalScale(PivotPoint.CENTER);
    }

    private Matrix endInside() {
        if (this.mVideoSize.getHeight() > this.mViewSize.getWidth() || this.mVideoSize.getHeight() > this.mViewSize.getHeight()) {
            return fitEnd();
        }
        return getOriginalScale(PivotPoint.RIGHT_BOTTOM);
    }
}
