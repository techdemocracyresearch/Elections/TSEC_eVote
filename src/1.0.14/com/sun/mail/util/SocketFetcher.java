package com.sun.mail.util;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SocketFetcher {
    private SocketFetcher() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e8, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e9, code lost:
        r5 = r0;
        r3 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0106, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0107, code lost:
        r5 = r0;
        r0 = r17;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0116, code lost:
        if ((r5 instanceof java.lang.reflect.InvocationTargetException) != false) goto L_0x0118;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0118, code lost:
        r1 = ((java.lang.reflect.InvocationTargetException) r5).getTargetException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0121, code lost:
        if ((r1 instanceof java.lang.Exception) != false) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0123, code lost:
        r5 = (java.lang.Exception) r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0128, code lost:
        if ((r5 instanceof java.io.IOException) != false) goto L_0x012a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x012c, code lost:
        throw ((java.io.IOException) r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x012d, code lost:
        r1 = new java.io.IOException("Couldn't connect using \"" + r3 + "\" socket factory to host, port: " + r18 + ", " + r0 + "; Exception: " + r5);
        r1.initCause(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x015d, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0180, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0181, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0082 A[SYNTHETIC, Splitter:B:19:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00cb A[Catch:{ SocketTimeoutException -> 0x0180, Exception -> 0x010e }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0171 A[SYNTHETIC, Splitter:B:71:0x0171] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0179  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0180 A[ExcHandler: SocketTimeoutException (r0v16 'e' java.net.SocketTimeoutException A[CUSTOM_DECLARE]), Splitter:B:31:0x00c5] */
    public static Socket getSocket(String str, int i, Properties properties, String str2, boolean z) throws IOException {
        int i2;
        String property;
        String property2;
        int i3;
        boolean z2;
        String property3;
        Socket socket;
        String str3;
        SocketFactory socketFactory;
        int i4;
        String str4 = str2 == null ? "socket" : str2;
        Properties properties2 = properties == null ? new Properties() : properties;
        String property4 = properties2.getProperty(String.valueOf(str4) + ".connectiontimeout", null);
        int i5 = -1;
        if (property4 != null) {
            try {
                i2 = Integer.parseInt(property4);
            } catch (NumberFormatException unused) {
            }
            property = properties2.getProperty(String.valueOf(str4) + ".timeout", null);
            String property5 = properties2.getProperty(String.valueOf(str4) + ".localaddress", null);
            InetAddress byName = property5 == null ? InetAddress.getByName(property5) : null;
            property2 = properties2.getProperty(String.valueOf(str4) + ".localport", null);
            if (property2 != null) {
                try {
                    i3 = Integer.parseInt(property2);
                } catch (NumberFormatException unused2) {
                }
                String property6 = properties2.getProperty(String.valueOf(str4) + ".socketFactory.fallback", null);
                z2 = property6 != null || !property6.equalsIgnoreCase("false");
                property3 = properties2.getProperty(String.valueOf(str4) + ".socketFactory.class", null);
                socketFactory = getSocketFactory(property3);
                if (socketFactory != null) {
                    String property7 = properties2.getProperty(String.valueOf(str4) + ".socketFactory.port", null);
                    if (property7 != null) {
                        i4 = Integer.parseInt(property7);
                        int i6 = i4 != -1 ? i : i4;
                        str3 = property3;
                        socket = createSocket(byName, i3, str, i6, i2, socketFactory, z);
                        if (socket == null) {
                            socket = createSocket(byName, i3, str, i, i2, null, z);
                        }
                        if (property != null) {
                            try {
                                i5 = Integer.parseInt(property);
                            } catch (NumberFormatException unused3) {
                            }
                        }
                        if (i5 >= 0) {
                            socket.setSoTimeout(i5);
                        }
                        configureSSLSocket(socket, properties2, str4);
                        return socket;
                    }
                    i4 = -1;
                    if (i4 != -1) {
                    }
                    str3 = property3;
                    socket = createSocket(byName, i3, str, i6, i2, socketFactory, z);
                    if (socket == null) {
                    }
                    if (property != null) {
                    }
                    if (i5 >= 0) {
                    }
                    configureSSLSocket(socket, properties2, str4);
                    return socket;
                }
                socket = null;
                if (socket == null) {
                }
                if (property != null) {
                }
                if (i5 >= 0) {
                }
                configureSSLSocket(socket, properties2, str4);
                return socket;
            }
            i3 = 0;
            String property62 = properties2.getProperty(String.valueOf(str4) + ".socketFactory.fallback", null);
            if (property62 != null) {
            }
            property3 = properties2.getProperty(String.valueOf(str4) + ".socketFactory.class", null);
            socketFactory = getSocketFactory(property3);
            if (socketFactory != null) {
            }
            socket = null;
            if (socket == null) {
            }
            if (property != null) {
            }
            if (i5 >= 0) {
            }
            configureSSLSocket(socket, properties2, str4);
            return socket;
        }
        i2 = -1;
        property = properties2.getProperty(String.valueOf(str4) + ".timeout", null);
        String property52 = properties2.getProperty(String.valueOf(str4) + ".localaddress", null);
        if (property52 == null) {
        }
        property2 = properties2.getProperty(String.valueOf(str4) + ".localport", null);
        if (property2 != null) {
        }
        i3 = 0;
        String property622 = properties2.getProperty(String.valueOf(str4) + ".socketFactory.fallback", null);
        if (property622 != null) {
        }
        property3 = properties2.getProperty(String.valueOf(str4) + ".socketFactory.class", null);
        try {
            socketFactory = getSocketFactory(property3);
            if (socketFactory != null) {
            }
        } catch (SocketTimeoutException e) {
        } catch (Exception e2) {
            str3 = property3;
            Exception exc = e2;
            int i7 = -1;
            if (!z2) {
            }
            socket = null;
            if (socket == null) {
            }
            if (property != null) {
            }
            if (i5 >= 0) {
            }
            configureSSLSocket(socket, properties2, str4);
            return socket;
        }
        socket = null;
        if (socket == null) {
        }
        if (property != null) {
        }
        if (i5 >= 0) {
        }
        configureSSLSocket(socket, properties2, str4);
        return socket;
    }

    public static Socket getSocket(String str, int i, Properties properties, String str2) throws IOException {
        return getSocket(str, i, properties, str2, false);
    }

    private static Socket createSocket(InetAddress inetAddress, int i, String str, int i2, int i3, SocketFactory socketFactory, boolean z) throws IOException {
        Socket socket;
        if (socketFactory != null) {
            socket = socketFactory.createSocket();
        } else if (z) {
            socket = SSLSocketFactory.getDefault().createSocket();
        } else {
            socket = new Socket();
        }
        if (inetAddress != null) {
            socket.bind(new InetSocketAddress(inetAddress, i));
        }
        if (i3 >= 0) {
            socket.connect(new InetSocketAddress(str, i2), i3);
        } else {
            socket.connect(new InetSocketAddress(str, i2));
        }
        return socket;
    }

    private static SocketFactory getSocketFactory(String str) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Class<?> cls = null;
        if (str == null || str.length() == 0) {
            return null;
        }
        ClassLoader contextClassLoader = getContextClassLoader();
        if (contextClassLoader != null) {
            try {
                cls = contextClassLoader.loadClass(str);
            } catch (ClassNotFoundException unused) {
            }
        }
        if (cls == null) {
            cls = Class.forName(str);
        }
        return (SocketFactory) cls.getMethod("getDefault", new Class[0]).invoke(new Object(), new Object[0]);
    }

    public static Socket startTLS(Socket socket) throws IOException {
        return startTLS(socket, new Properties(), "socket");
    }

    public static Socket startTLS(Socket socket, Properties properties, String str) throws IOException {
        SSLSocketFactory sSLSocketFactory;
        String hostName = socket.getInetAddress().getHostName();
        int port = socket.getPort();
        try {
            SocketFactory socketFactory = getSocketFactory(properties.getProperty(String.valueOf(str) + ".socketFactory.class", null));
            if (socketFactory == null || !(socketFactory instanceof SSLSocketFactory)) {
                sSLSocketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            } else {
                sSLSocketFactory = (SSLSocketFactory) socketFactory;
            }
            Socket createSocket = sSLSocketFactory.createSocket(socket, hostName, port, true);
            configureSSLSocket(createSocket, properties, str);
            return createSocket;
        } catch (Exception e) {
            e = e;
            if (e instanceof InvocationTargetException) {
                Throwable targetException = ((InvocationTargetException) e).getTargetException();
                if (targetException instanceof Exception) {
                    e = (Exception) targetException;
                }
            }
            if (e instanceof IOException) {
                throw ((IOException) e);
            }
            IOException iOException = new IOException("Exception in startTLS: host " + hostName + ", port " + port + "; Exception: " + e);
            iOException.initCause(e);
            throw iOException;
        }
    }

    private static void configureSSLSocket(Socket socket, Properties properties, String str) {
        if (socket instanceof SSLSocket) {
            SSLSocket sSLSocket = (SSLSocket) socket;
            String property = properties.getProperty(String.valueOf(str) + ".ssl.protocols", null);
            if (property != null) {
                sSLSocket.setEnabledProtocols(stringArray(property));
            } else {
                sSLSocket.setEnabledProtocols(new String[]{"TLSv1"});
            }
            String property2 = properties.getProperty(String.valueOf(str) + ".ssl.ciphersuites", null);
            if (property2 != null) {
                sSLSocket.setEnabledCipherSuites(stringArray(property2));
            }
        }
    }

    private static String[] stringArray(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str);
        ArrayList arrayList = new ArrayList();
        while (stringTokenizer.hasMoreTokens()) {
            arrayList.add(stringTokenizer.nextToken());
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    private static ClassLoader getContextClassLoader() {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            /* class com.sun.mail.util.SocketFetcher.AnonymousClass1 */

            @Override // java.security.PrivilegedAction
            public Object run() {
                try {
                    return Thread.currentThread().getContextClassLoader();
                } catch (SecurityException unused) {
                    return null;
                }
            }
        });
    }
}
