package com.gantix.JailMonkey;

import android.os.Build;
import android.os.Debug;
import android.provider.Settings;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.gantix.JailMonkey.AdbEnabled.AdbEnabled;
import com.gantix.JailMonkey.ExternalStorage.ExternalStorageCheck;
import com.gantix.JailMonkey.HookDetection.HookDetectionCheck;
import com.gantix.JailMonkey.MockLocation.MockLocationCheck;
import com.gantix.JailMonkey.Rooted.RootedCheck;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;

public class JailMonkeyModule extends ReactContextBaseJavaModule {
    ReactApplicationContext reactContext;

    @Override // com.facebook.react.bridge.NativeModule
    public String getName() {
        return "JailMonkey";
    }

    public JailMonkeyModule(ReactApplicationContext reactApplicationContext, boolean z) {
        super(reactApplicationContext);
        this.reactContext = reactApplicationContext;
    }

    @ReactMethod
    public void isDevelopmentSettingsMode(Promise promise) {
        boolean z = true;
        if (Build.VERSION.SDK_INT >= 17 ? Settings.Global.getInt(this.reactContext.getContentResolver(), "development_settings_enabled", 0) != 1 : Settings.System.getInt(this.reactContext.getContentResolver(), "development_settings_enabled", 0) == 1) {
            z = false;
        }
        promise.resolve(Boolean.valueOf(z));
    }

    @ReactMethod
    public void isDebuggedMode(Promise promise) {
        boolean z = true;
        if (!Debug.isDebuggerConnected() && (this.reactContext.getApplicationContext().getApplicationInfo().flags & 2) == 0) {
            z = false;
        }
        promise.resolve(Boolean.valueOf(z));
    }

    @Override // com.facebook.react.bridge.BaseJavaModule
    @Nullable
    public Map<String, Object> getConstants() {
        ReactApplicationContext reactApplicationContext = getReactApplicationContext();
        HashMap hashMap = new HashMap();
        hashMap.put("isJailBroken", Boolean.valueOf(RootedCheck.isJailBroken(reactApplicationContext)));
        hashMap.put("hookDetected", Boolean.valueOf(HookDetectionCheck.hookDetected(reactApplicationContext)));
        hashMap.put("canMockLocation", Boolean.valueOf(MockLocationCheck.isMockLocationOn(reactApplicationContext)));
        hashMap.put("isOnExternalStorage", Boolean.valueOf(ExternalStorageCheck.isOnExternalStorage(reactApplicationContext)));
        hashMap.put("AdbEnabled", Boolean.valueOf(AdbEnabled.AdbEnabled(reactApplicationContext)));
        return hashMap;
    }
}
