package com.gantix.JailMonkey.Debugged;

import android.content.Context;
import android.os.Debug;

public class DebuggedCheck {
    public static boolean isDebugged(Context context) {
        if (!Debug.isDebuggerConnected() && (context.getApplicationContext().getApplicationInfo().flags & 2) == 0) {
            return false;
        }
        return true;
    }
}
