package com.sdklicense;

public class LivenessSDK {
    public static native String getPackageName();

    public static native String stringFromJNI();
}
