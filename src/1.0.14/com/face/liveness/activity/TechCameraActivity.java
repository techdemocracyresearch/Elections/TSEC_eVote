package com.face.liveness.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.internal.view.SupportMenu;
import androidx.recyclerview.widget.ItemTouchHelper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.face.liveness.LivenessController;
import com.face.liveness.R;
import com.face.liveness.T5LivenessListener;
import com.face.liveness.activity.TechCameraActivity;
import com.face.liveness.camera.CameraPreview;
import com.face.liveness.constants.Constants;
import com.face.liveness.exception.ExceptionUtility;
import com.face.liveness.utils.ErrorUtils;
import com.face.liveness.utils.T5ContextWrapper;
import com.face.liveness.utils.Utils;
import com.face.liveness.view.FaceGraphic;
import com.face.liveness.view.GraphicOverlay;
import com.face.liveness.view.gif.GifImageView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

public class TechCameraActivity extends Activity implements SensorEventListener {
    private int CAPTURE_COUNTDOWN_IN_SEC = 3;
    private int FINAL_SELFIE_COUNTDOWN_IN_SEC = 3;
    float RightFace;
    private final String TAG = "TechCameraActivity";
    private final String[] animGifs = {"lookleft.gif", "lookright.gif", "tiltright.gif", "tiltleft.gif", "blinkeye.gif", "faceFront.png"};
    float antiClock;
    Camera.PreviewCallback cameraPreviewCallBack = new Camera.PreviewCallback() {
        /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass6 */

        public void onPreviewFrame(final byte[] bArr, final Camera camera) {
            final int i = camera.getParameters().getPreviewSize().width;
            final int i2 = camera.getParameters().getPreviewSize().height;
            TechCameraActivity.this.runOnUiThread(new Runnable() {
                /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass6.AnonymousClass1 */

                public void run() {
                    Utils.showLogs("TechCameraActivity", "UI_THREAD");
                    try {
                        if (TechCameraActivity.this.isReady && TechCameraActivity.this.isStartCountdownCompleted) {
                            TechCameraActivity.this.isReady = false;
                            if (TechCameraActivity.this.delaytime != 1500) {
                                TechCameraActivity.this.delaytime = 50;
                            }
                            if (TechCameraActivity.this.metadata == null) {
                                TechCameraActivity.this.metadata = new FirebaseVisionImageMetadata.Builder().setWidth(i).setHeight(i2).setFormat(17).setRotation(TechCameraActivity.this.useBackCamera ? 1 : 3).build();
                                TechCameraActivity.this.graphicOverlay.setCameraInfo(i2, i, 1);
                                TechCameraActivity.this.graphicOverlay.clear();
                                TechCameraActivity.this.faceGraphic = new FaceGraphic(TechCameraActivity.this.graphicOverlay);
                                TechCameraActivity.this.graphicOverlay.add(TechCameraActivity.this.faceGraphic);
                            }
                            final byte[] bArr = bArr;
                            final FirebaseVisionImage fromByteArray = FirebaseVisionImage.fromByteArray(bArr, TechCameraActivity.this.metadata);
                            fromByteArray.getBitmap().getWidth();
                            TechCameraActivity.this.detector.detectInImage(fromByteArray).addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionFace>>() {
                                /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass6.AnonymousClass1.AnonymousClass2 */

                                public void onSuccess(List<FirebaseVisionFace> list) {
                                    if (list != null) {
                                        Utils.showLogs("TechCameraActivity", "Faces " + list.size());
                                    }
                                    if (list == null || list.size() <= 0 || !(list.size() == 1 || TechCameraActivity.this.challengeCompleteCount == TechCameraActivity.this.totalChallengeCount)) {
                                        if (TechCameraActivity.this.totalChallengeCount == TechCameraActivity.this.challengeCompleteCount) {
                                            TechCameraActivity.this.drawBorderCircle(false);
                                            TechCameraActivity.this.enableCaptureButton(false);
                                        }
                                        TechCameraActivity.this.textFace.setVisibility(8);
                                        TechCameraActivity.this.textFaceForward.setVisibility(8);
                                        if (TechCameraActivity.this.startTime == 1 && !TechCameraActivity.this.isActivityFinished) {
                                            TechCameraActivity.this.isActivityFinished = true;
                                            LivenessController.getT5LivenessListener().onError(ErrorUtils.contoursCode, ErrorUtils.contoursMessage);
                                            TechCameraActivity.this.userActionFinish();
                                        }
                                        if (list != null && list.size() > 1) {
                                            LivenessController.getT5LivenessListener().onError(ErrorUtils.multipleFaceDetectedCode, ErrorUtils.multipleFaceDetectedMessage);
                                            TechCameraActivity.this.userActionFinish();
                                        }
                                    } else {
                                        TechCameraActivity.this.face = list.get(0);
                                        Utils.showLogs("TechCameraActivity", "face properties " + TechCameraActivity.this.face);
                                        TechCameraActivity.this.textFaceForward.setVisibility(8);
                                        TechCameraActivity.this.textFace.setVisibility(0);
                                        Utils.showLogs("TechCameraActivity", TechCameraActivity.this.face.getHeadEulerAngleY() + ", " + TechCameraActivity.this.face.getHeadEulerAngleZ());
                                        if (TechCameraActivity.this.presentSide == null) {
                                            TechCameraActivity.this.stopTimeOutCountDown();
                                            TechCameraActivity.this.textFace.setVisibility(0);
                                            boolean isEyesOpen = TechCameraActivity.this.isEyesOpen(TechCameraActivity.this.face);
                                            boolean isLookingStraight = TechCameraActivity.this.isLookingStraight(TechCameraActivity.this.face);
                                            if (TechCameraActivity.this.challengeCompleteCount == TechCameraActivity.this.totalChallengeCount) {
                                                float differenceBetweenEyes = TechCameraActivity.this.getDifferenceBetweenEyes(fromByteArray);
                                                float eyesYPosition = TechCameraActivity.this.getEyesYPosition(fromByteArray);
                                                if (list.size() == 1 && isLookingStraight && isEyesOpen && differenceBetweenEyes >= ((float) Constants.EYES_X_MIN_THRESHOLD) && differenceBetweenEyes < ((float) Constants.EYES_X_MAX_THRESHOLD) && TechCameraActivity.this.isDeviceAccelerometerCorrect && eyesYPosition >= ((float) Constants.EYES_Y_MIN_THRESHOLD)) {
                                                    if (eyesYPosition < ((float) (Constants.EYES_Y_MAX_THRESHOLD + (TechCameraActivity.this.useBackCamera ? Constants.BACK_CAMERA_EXTRA_THRESHOLD : 0)))) {
                                                        TechCameraActivity.this.drawBorderCircle(true);
                                                        TechCameraActivity.this.enableCaptureButton(true);
                                                        TechCameraActivity.this.textFace.setText(TechCameraActivity.this.getString(R.string.label_look_straight));
                                                        if (!TechCameraActivity.this.isFinalSelfieImageInProgress) {
                                                            TechCameraActivity.access$2408(TechCameraActivity.this);
                                                            if (TechCameraActivity.this.isShowCaptureButton) {
                                                                if (TechCameraActivity.this.captureSelfie) {
                                                                    TechCameraActivity.this.vibrate();
                                                                    TechCameraActivity.this.checkFinalSelfie(bArr, camera);
                                                                }
                                                            } else if (TechCameraActivity.this.successFrameCount > 1) {
                                                                TechCameraActivity.this.vibrate();
                                                                TechCameraActivity.this.checkFinalSelfie(bArr, camera);
                                                            }
                                                            if (TechCameraActivity.this.successFrameCount > 1) {
                                                                TechCameraActivity.this.failedFrameCount = 0;
                                                            }
                                                        }
                                                    }
                                                }
                                                TechCameraActivity.this.captureSelfie = false;
                                                TechCameraActivity.access$2908(TechCameraActivity.this);
                                                if (TechCameraActivity.this.failedFrameCount > 2) {
                                                    TechCameraActivity.this.successFrameCount = 0;
                                                }
                                                TechCameraActivity.this.stopFinalSelfieCapture();
                                                if (TechCameraActivity.this.failedFrameCount > 1 || TechCameraActivity.this.successFrameCount == 0) {
                                                    TechCameraActivity.this.drawBorderCircle(false);
                                                    TechCameraActivity.this.enableCaptureButton(false);
                                                }
                                                if (list.size() > 1) {
                                                    TechCameraActivity.this.textFace.setText(TechCameraActivity.this.getString(R.string.multiple_faces_error));
                                                } else if (!isLookingStraight) {
                                                    TechCameraActivity.this.textFace.setText(TechCameraActivity.this.getString(R.string.label_look_straight));
                                                } else if (!isEyesOpen) {
                                                    TechCameraActivity.this.textFace.setText(TechCameraActivity.this.getString(R.string.open_eyes_message));
                                                } else if (differenceBetweenEyes < ((float) Constants.EYES_X_MIN_THRESHOLD)) {
                                                    TechCameraActivity.this.textFace.setText(TechCameraActivity.this.getString(R.string.camera_close_message));
                                                } else if (differenceBetweenEyes >= ((float) Constants.EYES_X_MAX_THRESHOLD)) {
                                                    TechCameraActivity.this.textFace.setText(TechCameraActivity.this.getString(R.string.camera_far_message));
                                                } else if (eyesYPosition < ((float) Constants.EYES_Y_MIN_THRESHOLD)) {
                                                    TechCameraActivity.this.textFace.setText(TechCameraActivity.this.getString(R.string.camera_up_message));
                                                } else if (eyesYPosition >= ((float) Constants.EYES_Y_MAX_THRESHOLD)) {
                                                    TechCameraActivity.this.textFace.setText(TechCameraActivity.this.getString(R.string.camera_down_message));
                                                } else if (!TechCameraActivity.this.isDeviceAccelerometerCorrect) {
                                                    TechCameraActivity.this.isDeviceAccelerometerCorrect = true;
                                                    TechCameraActivity.this.textFace.setText(TechCameraActivity.this.getString(R.string.device_accelerometer_error_message));
                                                }
                                            }
                                            TechCameraActivity.this.isReady = true;
                                            return;
                                        }
                                        if (TechCameraActivity.this.face.getSmilingProbability() != -1.0f) {
                                            float smilingProbability = TechCameraActivity.this.face.getSmilingProbability();
                                            TechCameraActivity techCameraActivity = TechCameraActivity.this;
                                            techCameraActivity.log("Classifications", "Smiling Probablity " + smilingProbability);
                                        }
                                        if (TechCameraActivity.this.face.getRightEyeOpenProbability() != -1.0f) {
                                            float rightEyeOpenProbability = TechCameraActivity.this.face.getRightEyeOpenProbability();
                                            TechCameraActivity techCameraActivity2 = TechCameraActivity.this;
                                            techCameraActivity2.log("Classifications", "rightEyeOpenProb Probablity " + rightEyeOpenProbability);
                                        }
                                        if (TechCameraActivity.this.face.getTrackingId() != -1) {
                                            int trackingId = TechCameraActivity.this.face.getTrackingId();
                                            TechCameraActivity techCameraActivity3 = TechCameraActivity.this;
                                            techCameraActivity3.log("Classifications", "Tracking Probablity " + trackingId);
                                        }
                                        int i = AnonymousClass12.$SwitchMap$com$face$liveness$activity$TechCameraActivity$FaceSides[TechCameraActivity.this.presentSide.ordinal()];
                                        if (i != 1) {
                                            if (i == 2) {
                                                TechCameraActivity techCameraActivity4 = TechCameraActivity.this;
                                                techCameraActivity4.log("face", "face.getHeadEulerAngleY() " + TechCameraActivity.this.face.getHeadEulerAngleY());
                                                TechCameraActivity.this.leftFace = TechCameraActivity.this.face.getHeadEulerAngleY();
                                                if (((double) TechCameraActivity.this.leftFace) > 36.0d) {
                                                    TechCameraActivity.this.delaytime = ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED;
                                                    TechCameraActivity.this.vibrate();
                                                    TechCameraActivity.this.nextFace(false);
                                                    TechCameraActivity.this.showChallengeSuccess();
                                                } else if (((double) TechCameraActivity.this.face.getHeadEulerAngleY()) <= -12.0d || ((double) TechCameraActivity.this.face.getHeadEulerAngleY()) >= 12.0d || TechCameraActivity.this.face.getHeadEulerAngleZ() < -25.0f || TechCameraActivity.this.face.getHeadEulerAngleZ() > 25.0f) {
                                                    if (TechCameraActivity.this.face.getHeadEulerAngleZ() > 25.0f) {
                                                        T5LivenessListener t5LivenessListener = LivenessController.getT5LivenessListener();
                                                        int i2 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener.onError(i2, "You did Anti-Clockwise movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    } else if (TechCameraActivity.this.face.getHeadEulerAngleZ() < -17.0f) {
                                                        T5LivenessListener t5LivenessListener2 = LivenessController.getT5LivenessListener();
                                                        int i3 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener2.onError(i3, "You did Clockwise movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    } else if (TechCameraActivity.this.face.getHeadEulerAngleY() < -36.0f) {
                                                        T5LivenessListener t5LivenessListener3 = LivenessController.getT5LivenessListener();
                                                        int i4 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener3.onError(i4, "You did Right movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    }
                                                }
                                            } else if (i == 3) {
                                                TechCameraActivity techCameraActivity5 = TechCameraActivity.this;
                                                techCameraActivity5.log("RIGHT face", "face.getHeadEulerAngleY() " + TechCameraActivity.this.face.getHeadEulerAngleY());
                                                TechCameraActivity.this.RightFace = TechCameraActivity.this.face.getHeadEulerAngleY();
                                                if (((double) TechCameraActivity.this.RightFace) < -36.0d) {
                                                    TechCameraActivity.this.delaytime = ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED;
                                                    TechCameraActivity.this.vibrate();
                                                    TechCameraActivity.this.nextFace(false);
                                                    TechCameraActivity.this.showChallengeSuccess();
                                                } else if (((double) TechCameraActivity.this.face.getHeadEulerAngleY()) <= -12.0d || ((double) TechCameraActivity.this.face.getHeadEulerAngleY()) >= 12.0d || TechCameraActivity.this.face.getHeadEulerAngleZ() < -25.0f || TechCameraActivity.this.face.getHeadEulerAngleZ() > 25.0f) {
                                                    if (TechCameraActivity.this.face.getHeadEulerAngleZ() > 25.0f) {
                                                        T5LivenessListener t5LivenessListener4 = LivenessController.getT5LivenessListener();
                                                        int i5 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener4.onError(i5, "You did Anti-Clockwise movement instead of  " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    } else if (TechCameraActivity.this.face.getHeadEulerAngleZ() < -17.0f) {
                                                        T5LivenessListener t5LivenessListener5 = LivenessController.getT5LivenessListener();
                                                        int i6 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener5.onError(i6, "You did Clockwise movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    } else if (TechCameraActivity.this.face.getHeadEulerAngleY() > 36.0f) {
                                                        T5LivenessListener t5LivenessListener6 = LivenessController.getT5LivenessListener();
                                                        int i7 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener6.onError(i7, "You did Left movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    }
                                                }
                                            } else if (i == 4) {
                                                TechCameraActivity.this.clockFace = TechCameraActivity.this.face.getHeadEulerAngleZ();
                                                if (TechCameraActivity.this.clockFace > 15.0f) {
                                                    TechCameraActivity.this.delaytime = ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED;
                                                    TechCameraActivity.this.vibrate();
                                                    TechCameraActivity.this.nextFace(false);
                                                    TechCameraActivity.this.showChallengeSuccess();
                                                } else if (((double) TechCameraActivity.this.face.getHeadEulerAngleZ()) <= -12.0d || ((double) TechCameraActivity.this.face.getHeadEulerAngleZ()) >= 12.0d || TechCameraActivity.this.face.getHeadEulerAngleY() > 36.0f || TechCameraActivity.this.face.getHeadEulerAngleY() < -36.0f) {
                                                    if (TechCameraActivity.this.face.getHeadEulerAngleY() > 36.0f) {
                                                        T5LivenessListener t5LivenessListener7 = LivenessController.getT5LivenessListener();
                                                        int i8 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener7.onError(i8, "You did Left movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    } else if (TechCameraActivity.this.face.getHeadEulerAngleY() < -36.0f) {
                                                        T5LivenessListener t5LivenessListener8 = LivenessController.getT5LivenessListener();
                                                        int i9 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener8.onError(i9, "You did Right movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    } else if (TechCameraActivity.this.face.getHeadEulerAngleZ() < -17.0f) {
                                                        T5LivenessListener t5LivenessListener9 = LivenessController.getT5LivenessListener();
                                                        int i10 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener9.onError(i10, "You did Anti-Clockwise movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    }
                                                }
                                            } else if (i == 5) {
                                                TechCameraActivity.this.antiClock = TechCameraActivity.this.face.getHeadEulerAngleZ();
                                                if (TechCameraActivity.this.antiClock < -15.0f) {
                                                    TechCameraActivity.this.delaytime = ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED;
                                                    TechCameraActivity.this.vibrate();
                                                    TechCameraActivity.this.nextFace(false);
                                                    TechCameraActivity.this.showChallengeSuccess();
                                                } else if (((double) TechCameraActivity.this.face.getHeadEulerAngleZ()) <= -12.0d || ((double) TechCameraActivity.this.face.getHeadEulerAngleZ()) >= 12.0d || TechCameraActivity.this.face.getHeadEulerAngleY() > 36.0f || TechCameraActivity.this.face.getHeadEulerAngleY() < -36.0f) {
                                                    if (TechCameraActivity.this.face.getHeadEulerAngleY() < -36.0f) {
                                                        T5LivenessListener t5LivenessListener10 = LivenessController.getT5LivenessListener();
                                                        int i11 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener10.onError(i11, "You did Right movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    } else if (TechCameraActivity.this.face.getHeadEulerAngleY() > 36.0f) {
                                                        T5LivenessListener t5LivenessListener11 = LivenessController.getT5LivenessListener();
                                                        int i12 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener11.onError(i12, "You did Left movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    } else if (TechCameraActivity.this.face.getHeadEulerAngleZ() > 25.0f) {
                                                        T5LivenessListener t5LivenessListener12 = LivenessController.getT5LivenessListener();
                                                        int i13 = ErrorUtils.wrongMovementCode;
                                                        t5LivenessListener12.onError(i13, "You did Clockwise movement instead of " + TechCameraActivity.this.presentSide);
                                                        TechCameraActivity.this.userActionFinish();
                                                    }
                                                }
                                            } else if (TechCameraActivity.this.challengeCompleteCount == TechCameraActivity.this.totalChallengeCount && ((double) TechCameraActivity.this.face.getHeadEulerAngleZ()) > -12.0d && ((double) TechCameraActivity.this.face.getHeadEulerAngleZ()) < 12.0d && ((double) TechCameraActivity.this.face.getHeadEulerAngleY()) > -12.0d && ((double) TechCameraActivity.this.face.getHeadEulerAngleY()) < 12.0d) {
                                                TechCameraActivity.this.vibrate();
                                                TechCameraActivity.this.nextFace(true);
                                            }
                                        } else if (((double) TechCameraActivity.this.face.getHeadEulerAngleZ()) <= -12.0d || ((double) TechCameraActivity.this.face.getHeadEulerAngleZ()) >= 12.0d || ((double) TechCameraActivity.this.face.getHeadEulerAngleY()) <= -12.0d || ((double) TechCameraActivity.this.face.getHeadEulerAngleY()) >= 12.0d) {
                                            if (((double) TechCameraActivity.this.face.getHeadEulerAngleY()) <= -12.0d || ((double) TechCameraActivity.this.face.getHeadEulerAngleY()) >= 12.0d || TechCameraActivity.this.face.getHeadEulerAngleZ() < -25.0f || TechCameraActivity.this.face.getHeadEulerAngleZ() > 25.0f) {
                                                if (TechCameraActivity.this.face.getHeadEulerAngleZ() > 25.0f) {
                                                    T5LivenessListener t5LivenessListener13 = LivenessController.getT5LivenessListener();
                                                    int i14 = ErrorUtils.wrongMovementCode;
                                                    t5LivenessListener13.onError(i14, "you did Anti-Clockwise movement instead of " + TechCameraActivity.this.presentSide);
                                                    TechCameraActivity.this.userActionFinish();
                                                } else if (TechCameraActivity.this.face.getHeadEulerAngleZ() < -25.0f) {
                                                    T5LivenessListener t5LivenessListener14 = LivenessController.getT5LivenessListener();
                                                    int i15 = ErrorUtils.wrongMovementCode;
                                                    t5LivenessListener14.onError(i15, "You did Clockwise movement instead of " + TechCameraActivity.this.presentSide);
                                                    TechCameraActivity.this.userActionFinish();
                                                } else if (TechCameraActivity.this.face.getHeadEulerAngleY() > 36.0f) {
                                                    T5LivenessListener t5LivenessListener15 = LivenessController.getT5LivenessListener();
                                                    int i16 = ErrorUtils.wrongMovementCode;
                                                    t5LivenessListener15.onError(i16, "You did Right movement instead of " + TechCameraActivity.this.presentSide);
                                                    TechCameraActivity.this.userActionFinish();
                                                } else if (TechCameraActivity.this.face.getHeadEulerAngleY() < -36.0f) {
                                                    T5LivenessListener t5LivenessListener16 = LivenessController.getT5LivenessListener();
                                                    int i17 = ErrorUtils.wrongMovementCode;
                                                    t5LivenessListener16.onError(i17, "You did Left movement instead of " + TechCameraActivity.this.presentSide);
                                                    TechCameraActivity.this.userActionFinish();
                                                }
                                            }
                                        } else if (((double) TechCameraActivity.this.face.getLeftEyeOpenProbability()) <= 0.8d || ((double) TechCameraActivity.this.face.getRightEyeOpenProbability()) <= 0.8d) {
                                            TechCameraActivity.this.delaytime = ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED;
                                            TechCameraActivity.this.vibrate();
                                            TechCameraActivity.this.nextFace(false);
                                            TechCameraActivity.this.showChallengeSuccess();
                                        }
                                    }
                                    new Handler().postDelayed(new Runnable() {
                                        /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass6.AnonymousClass1.AnonymousClass2.AnonymousClass1 */

                                        public void run() {
                                            TechCameraActivity.this.isReady = true;
                                        }
                                    }, (long) TechCameraActivity.this.delaytime);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass6.AnonymousClass1.AnonymousClass1 */

                                @Override // com.google.android.gms.tasks.OnFailureListener
                                public void onFailure(Exception exc) {
                                    TechCameraActivity.this.isReady = true;
                                    ExceptionUtility.logError("TechCameraActivity", "detectInImage_ onFailure", exc);
                                    Utils.showLogs("livenessTask fail", exc.getMessage());
                                }
                            });
                        }
                    } catch (Exception e) {
                        ExceptionUtility.logError("TechCameraActivity", "detectInImage Listner", e);
                    }
                }
            });
        }
    };
    private boolean captureSelfie;
    private int challengeCompleteCount = 0;
    private CountDownTimer challengeStartCountDownTimer;
    private int challengeStartCountdownInSec = 0;
    float clockFace;
    private CountDownTimer countDownTimer;
    private int currentRandomPosition = 0;
    private CountDownTimer delayCounterTimer;
    int delaytime = 100;
    private FirebaseVisionFaceDetector detector;
    private ProgressDialog dialog;
    FirebaseVisionFace face;
    Drawable faceFront;
    FaceGraphic faceGraphic;
    private FaceSides[] faceSides;
    private int failedFrameCount;
    private CountDownTimer finalSelfieCountDownTimer;
    private GifImageView gifImageView;
    private GraphicOverlay graphicOverlay;
    private boolean isActivityFinished;
    boolean isContoursDetected = false;
    private boolean isDeviceAccelerometerCorrect = true;
    private boolean isFinalSelfieImageInProgress = false;
    private boolean isImageCompressed = false;
    private boolean isLivenessTaken;
    private boolean isPictureTaken;
    boolean isReady = true;
    private boolean isShowCaptureButton = false;
    private boolean isStartCountdownCompleted = false;
    private boolean isTimerFinish = false;
    private boolean isUserActionFinish;
    private ImageView ivCaptureSelfie;
    private ImageView ivChallengeSuccess;
    private float leftEyeOpenProbability;
    float leftFace;
    private int leftcloseCount;
    private int leftopenEyeCount;
    private byte[] livenessImage;
    private byte[] livenessImageFrontSelfie;
    private LinearLayout llStartCountDownLayout;
    private Camera mCamera;
    private int mScreenHeight;
    private int mScreenWidth;
    private FrameLayout maLayoutPreview;
    private CameraPreview maPreview;
    FirebaseVisionImageMetadata metadata = null;
    private ImageView mtransparentView;
    private ProgressBar pbChallengeCompleted;
    private FaceSides presentSide;
    private int randomCapturesCount = 5;
    private float rightEyeOpenProbability;
    private RelativeLayout rlAnimationLayout;
    private RelativeLayout rlChallengeIndicator;
    private RelativeLayout rlChallengeSuccessLayout;
    private RelativeLayout rlProgressBarLayout;
    private Sensor sensor;
    private SensorManager sensorManager;
    int startCount;
    long startTime = 0;
    private int successFrameCount;
    private TextView textFace;
    private TextView textFaceForward;
    private TextView textTimer;
    private int timeOutInSec = 3;
    private CountDownTimer timeoutCountDownTimer;
    private ToneGenerator toneGenerator;
    private int totalChallengeCount;
    private TextView tvCountDownTimer;
    private TextView tvCountDownTitle;
    private TextView tvProgressCount;
    private boolean useBackCamera;
    private Vibrator vibe;
    private Animation zoomIn;
    private Animation zoomOut;

    /* access modifiers changed from: private */
    public enum FaceSides {
        LEFT,
        RIGHT,
        CLOCK,
        ANTI_CLOCK,
        EYE_BLINK
    }

    public void onAccuracyChanged(Sensor sensor2, int i) {
    }

    static /* synthetic */ int access$2408(TechCameraActivity techCameraActivity) {
        int i = techCameraActivity.successFrameCount;
        techCameraActivity.successFrameCount = i + 1;
        return i;
    }

    static /* synthetic */ int access$2908(TechCameraActivity techCameraActivity) {
        int i = techCameraActivity.failedFrameCount;
        techCameraActivity.failedFrameCount = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(T5ContextWrapper.wrap(context, LivenessController.t5LanguageCode));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_techcam);
        this.vibe = (Vibrator) getSystemService("vibrator");
        Intent intent = getIntent();
        this.randomCapturesCount = intent.getIntExtra(Constants.RANDOM_CAPTURES_COUNT_, 5);
        this.timeOutInSec = intent.getIntExtra(Constants.TIME_OUT_IN_SEC, 3);
        this.CAPTURE_COUNTDOWN_IN_SEC = intent.getIntExtra(Constants.CAPTURE_TIME_OUT_IN_SEC, 3);
        this.challengeStartCountdownInSec = intent.getIntExtra(Constants.CHALLENGE_START_COUNT_IN_SEC, 3);
        this.useBackCamera = intent.getBooleanExtra(Constants.USE_BACK_CAMERA, false);
        this.isImageCompressed = intent.getBooleanExtra(Constants.IS_IMAGE_COMPRESSION, false);
        this.isShowCaptureButton = intent.getBooleanExtra(Constants.IS_CAPTURE_BUTTON_ENABLE, false);
        int i = this.randomCapturesCount;
        this.totalChallengeCount = i;
        if (i > 3) {
            this.randomCapturesCount = i + 1;
        }
        SensorManager sensorManager2 = (SensorManager) getSystemService("sensor");
        this.sensorManager = sensorManager2;
        if (sensorManager2 != null) {
            this.sensor = sensorManager2.getDefaultSensor(1);
        }
        loadJSONFromAsset();
        init();
        initStartCountdown();
        initCaptureFinalSelfie();
        setProgressCount();
        startCameraPreview();
    }

    private void initFaceDetection() {
        this.detector = FirebaseVision.getInstance().getVisionFaceDetector(new FirebaseVisionFaceDetectorOptions.Builder().setPerformanceMode(1).setLandmarkMode(2).setClassificationMode(2).setContourMode(2).setMinFaceSize(0.15f).enableTracking().build());
    }

    public void loadJSONFromAsset() {
        try {
            InputStream open = getAssets().open("t5Configuration.json");
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            JSONObject jSONObject = new JSONObject(new String(bArr, Key.STRING_CHARSET_NAME)).getJSONObject("faceGif");
            if (this.randomCapturesCount == 0) {
                log("Tech5", "randomCaptureCount is " + this.randomCapturesCount);
                this.animGifs[0] = jSONObject.getString("blink_eye");
            } else {
                this.animGifs[0] = jSONObject.getString("look_left");
                this.animGifs[1] = jSONObject.getString("look_right");
                this.animGifs[2] = jSONObject.getString("look_anticlockwise");
                this.animGifs[3] = jSONObject.getString("look_clockwise");
                this.animGifs[4] = jSONObject.getString("blink_eye");
            }
            this.faceFront = Drawable.createFromStream(getAssets().open(jSONObject.getString("look_straight")), null);
        } catch (Exception e) {
            ExceptionUtility.logError("TechCameraActivity", "loadJSONFromAsset", e);
        }
    }

    private void init() {
        this.timeoutCountDownTimer = new CountDownTimer((long) (this.timeOutInSec * 1000), 1000) {
            /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass1 */

            public void onTick(long j) {
                Utils.showLogs("techCameraActivity", "timeoutCountDownTimer: " + j);
            }

            public void onFinish() {
                LivenessController.getT5LivenessListener().onError(ErrorUtils.timeoutCode, ErrorUtils.timeOutMessage);
                TechCameraActivity.this.userActionFinish();
            }
        };
        this.delayCounterTimer = new CountDownTimer(800, 1000) {
            /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass2 */

            public void onTick(long j) {
            }

            public void onFinish() {
                TechCameraActivity.this.delaytime = 100;
            }
        };
        this.mtransparentView = (ImageView) findViewById(R.id.transparentView);
        draw();
        this.graphicOverlay = (GraphicOverlay) findViewById(R.id.graphicOverlay);
        this.gifImageView = (GifImageView) findViewById(R.id.gifImageView);
        this.textTimer = (TextView) findViewById(R.id.textTimer);
        this.tvProgressCount = (TextView) findViewById(R.id.tv_progress_count);
        this.ivChallengeSuccess = (ImageView) findViewById(R.id.iv_challenge_success);
        this.rlChallengeSuccessLayout = (RelativeLayout) findViewById(R.id.rl_challenge_success_layout);
        this.rlProgressBarLayout = (RelativeLayout) findViewById(R.id.rl_progress_bar_layout);
        this.pbChallengeCompleted = (ProgressBar) findViewById(R.id.pb_challenge_completed);
        this.llStartCountDownLayout = (LinearLayout) findViewById(R.id.ll_start_count_down_layout);
        this.tvCountDownTitle = (TextView) findViewById(R.id.tv_count_down_title);
        this.tvCountDownTimer = (TextView) findViewById(R.id.tv_count_down_timer);
        this.rlAnimationLayout = (RelativeLayout) findViewById(R.id.rl_animation_layout);
        this.rlChallengeIndicator = (RelativeLayout) findViewById(R.id.rl_challenge_indicator);
        this.ivCaptureSelfie = (ImageView) findViewById(R.id.iv_capture_selfie);
        this.maLayoutPreview = (FrameLayout) findViewById(R.id.camera_preview);
        if (this.randomCapturesCount == 0) {
            this.rlProgressBarLayout.setVisibility(8);
        }
        setGIFBytes(this.animGifs[0]);
        this.gifImageView.startAnimation();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.mScreenWidth = displayMetrics.widthPixels;
        this.mScreenHeight = displayMetrics.heightPixels;
        this.textFace = (TextView) findViewById(R.id.txt_challenge_indicator);
        this.textFaceForward = (TextView) findViewById(R.id.textFaceForward);
        FaceSides[] values = FaceSides.values();
        this.faceSides = values;
        Collections.shuffle(Arrays.asList(values));
        this.zoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
        this.zoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
    }

    public void captureFinalSelfie(View view) {
        this.captureSelfie = true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void enableCaptureButton(boolean z) {
        if (this.isShowCaptureButton) {
            if (this.ivCaptureSelfie.getVisibility() == 8) {
                this.ivCaptureSelfie.setVisibility(0);
            }
            if (z) {
                this.ivCaptureSelfie.setImageResource(R.drawable.camera_active_icon);
            } else {
                this.ivCaptureSelfie.setImageResource(R.drawable.camera_inactive_icon);
            }
            this.ivCaptureSelfie.setClickable(z);
            this.ivCaptureSelfie.setEnabled(z);
            return;
        }
        this.ivCaptureSelfie.setVisibility(8);
    }

    private void setGIFBytes(String str) {
        try {
            if (this.presentSide != null) {
                this.gifImageView.setFramesDisplayDuration(800);
                this.gifImageView.setVisibility(0);
                setLableName(this.presentSide);
                DrawableImageViewTarget drawableImageViewTarget = new DrawableImageViewTarget(this.gifImageView);
                RequestManager with = Glide.with((Activity) this);
                with.load(Uri.parse("file:///android_asset/" + str)).into(drawableImageViewTarget);
            }
        } catch (Exception e) {
            ExceptionUtility.logError("TechCameraActivity", "setGIFBytes", e);
        }
    }

    public void captureSecretPhoto() {
        try {
            this.isLivenessTaken = true;
            this.mCamera.takePicture(null, null, new Camera.PictureCallback() {
                /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass3 */

                public void onPictureTaken(byte[] bArr, Camera camera) {
                    TechCameraActivity.this.mCamera.stopPreview();
                    AsyncTask.execute(new Runnable(bArr) {
                        /* class com.face.liveness.activity.$$Lambda$TechCameraActivity$3$v5NknoJlQOjnAEIYWpyYe_15MX4 */
                        public final /* synthetic */ byte[] f$1;

                        {
                            this.f$1 = r2;
                        }

                        public final void run() {
                            TechCameraActivity.AnonymousClass3.this.lambda$onPictureTaken$0$TechCameraActivity$3(this.f$1);
                        }
                    });
                    TechCameraActivity.this.mCamera.startPreview();
                    TechCameraActivity.this.mCamera.setPreviewCallback(TechCameraActivity.this.cameraPreviewCallBack);
                }

                public /* synthetic */ void lambda$onPictureTaken$0$TechCameraActivity$3(byte[] bArr) {
                    try {
                        TechCameraActivity techCameraActivity = TechCameraActivity.this;
                        byte[] rotateImageData = Utils.rotateImageData(techCameraActivity, bArr, techCameraActivity.useBackCamera ? 0 : 1);
                        if (TechCameraActivity.this.isImageCompressed) {
                            TechCameraActivity.this.livenessImage = Utils.compressFaceImage(rotateImageData, Constants.REQUIRED_HEIGHT, Constants.COMPRESSION_RATE);
                            return;
                        }
                        TechCameraActivity.this.livenessImage = Utils.compressFaceImage(rotateImageData, Constants.REQUIRED_HEIGHT, Constants.NO_COMPRESSION_RATE);
                        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(TechCameraActivity.this.livenessImage, 0, TechCameraActivity.this.livenessImage.length);
                        TechCameraActivity techCameraActivity2 = TechCameraActivity.this;
                        techCameraActivity2.log("Size", "length :  File size humanReadableByteCount" + TechCameraActivity.humanReadableByteCount((long) rotateImageData.length, false) + " " + decodeByteArray.getWidth() + "x" + decodeByteArray.getHeight());
                    } catch (Exception e) {
                        ExceptionUtility.logError("TechCameraActivity", "captureSecretPhoto", e);
                    }
                }
            });
        } catch (Exception e) {
            ExceptionUtility.logError("TechCameraActivity", "captureSecretPhoto", e);
        }
    }

    public int sizeOf(Bitmap bitmap) {
        int width;
        int height;
        log("widthHeight", "Width is == " + bitmap.getWidth() + " Height is == " + bitmap.getHeight());
        if (Build.VERSION.SDK_INT < 12) {
            width = bitmap.getWidth();
            height = bitmap.getHeight();
        } else {
            width = bitmap.getWidth();
            height = bitmap.getHeight();
        }
        return width * height;
    }

    public static String humanReadableByteCount(long j, boolean z) {
        int i = z ? 1000 : 1024;
        if (j < ((long) i)) {
            return j + " B";
        }
        double d = (double) j;
        double d2 = (double) i;
        int log = (int) (Math.log(d) / Math.log(d2));
        StringBuilder sb = new StringBuilder();
        sb.append((z ? "kMGTPE" : "KMGTPE").charAt(log - 1));
        sb.append(z ? "" : "i");
        return String.format("%.1f %sB", Double.valueOf(d / Math.pow(d2, (double) log)), sb.toString());
    }

    public void capturePhoto(View view) {
        try {
            if (!this.isPictureTaken) {
                this.isPictureTaken = true;
                this.mCamera.takePicture(new Camera.ShutterCallback() {
                    /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass4 */

                    public void onShutter() {
                    }
                }, null, new Camera.PictureCallback() {
                    /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass5 */

                    public void onPictureTaken(byte[] bArr, Camera camera) {
                        TechCameraActivity.this.mCamera.stopPreview();
                        try {
                            TechCameraActivity techCameraActivity = TechCameraActivity.this;
                            byte[] rotateImageData = Utils.rotateImageData(techCameraActivity, bArr, techCameraActivity.useBackCamera ? 0 : 1);
                            if (TechCameraActivity.this.isPictureTaken) {
                                if (TechCameraActivity.this.isImageCompressed) {
                                    TechCameraActivity.this.livenessImageFrontSelfie = Utils.compressFaceImage(rotateImageData, Constants.REQUIRED_HEIGHT, Constants.COMPRESSION_RATE);
                                    TechCameraActivity.this.userActionFinish();
                                    LivenessController.getT5LivenessListener().onSuccess(TechCameraActivity.this.livenessImageFrontSelfie, TechCameraActivity.this.livenessImage);
                                } else {
                                    TechCameraActivity.this.livenessImageFrontSelfie = Utils.compressFaceImage(rotateImageData, Constants.REQUIRED_HEIGHT, Constants.NO_COMPRESSION_RATE);
                                    TechCameraActivity.this.userActionFinish();
                                    LivenessController.getT5LivenessListener().onSuccess(TechCameraActivity.this.livenessImageFrontSelfie, TechCameraActivity.this.livenessImage);
                                }
                                TechCameraActivity techCameraActivity2 = TechCameraActivity.this;
                                techCameraActivity2.log("Size", "length :  humanReadableByteCount: " + TechCameraActivity.humanReadableByteCount((long) rotateImageData.length, false));
                                Bitmap convertByteArrayToBitmap = Utils.convertByteArrayToBitmap(TechCameraActivity.this.livenessImageFrontSelfie);
                                TechCameraActivity techCameraActivity3 = TechCameraActivity.this;
                                techCameraActivity3.log("Size", "length :  File size humanReadableByteCount" + TechCameraActivity.humanReadableByteCount((long) TechCameraActivity.this.livenessImageFrontSelfie.length, false) + " " + convertByteArrayToBitmap.getWidth() + "x" + convertByteArrayToBitmap.getHeight());
                            }
                        } catch (Exception e) {
                            ExceptionUtility.logError("TechCameraActivity", "capturePhoto", e);
                        }
                    }
                });
            }
        } catch (Exception e) {
            ExceptionUtility.logError("TechCameraActivity", "capturePhoto", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Sensor sensor2;
        super.onResume();
        CountDownTimer countDownTimer2 = this.countDownTimer;
        if (countDownTimer2 != null) {
            countDownTimer2.start();
        }
        SensorManager sensorManager2 = this.sensorManager;
        if (sensorManager2 != null && (sensor2 = this.sensor) != null) {
            sensorManager2.registerListener(this, sensor2, 3);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.face.liveness.activity.TechCameraActivity$12  reason: invalid class name */
    public static /* synthetic */ class AnonymousClass12 {
        static final /* synthetic */ int[] $SwitchMap$com$face$liveness$activity$TechCameraActivity$FaceSides;

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        static {
            int[] iArr = new int[FaceSides.values().length];
            $SwitchMap$com$face$liveness$activity$TechCameraActivity$FaceSides = iArr;
            iArr[FaceSides.EYE_BLINK.ordinal()] = 1;
            $SwitchMap$com$face$liveness$activity$TechCameraActivity$FaceSides[FaceSides.LEFT.ordinal()] = 2;
            $SwitchMap$com$face$liveness$activity$TechCameraActivity$FaceSides[FaceSides.RIGHT.ordinal()] = 3;
            $SwitchMap$com$face$liveness$activity$TechCameraActivity$FaceSides[FaceSides.CLOCK.ordinal()] = 4;
            $SwitchMap$com$face$liveness$activity$TechCameraActivity$FaceSides[FaceSides.ANTI_CLOCK.ordinal()] = 5;
        }
    }

    private void validateMouth(FirebaseVisionFace firebaseVisionFace) {
        if (firebaseVisionFace.getLandmark(5) == null || firebaseVisionFace.getLandmark(11) == null || firebaseVisionFace.getLandmark(0) == null) {
            log("Values", "Mouth*******************************************************************************************");
            if (firebaseVisionFace.getLandmark(5) == null) {
                log("Values", "Mouth------MOUTH_LEFT value Not Found");
            }
            if (firebaseVisionFace.getLandmark(11) == null) {
                log("Values", "Mouth------MOUTH_RIGHT value Not Found");
            }
            if (firebaseVisionFace.getLandmark(0) == null) {
                log("Values", "Mouth------MOUTH_BOTTOM value Not Found");
            }
            if (firebaseVisionFace.getLandmark(6) == null) {
                log("Values", "Mouth------NOSE_BASE value Not Found");
            }
            if (firebaseVisionFace.getLandmark(1) == null) {
                log("Values", "Mouth------LEFT_CHEEK value Not Found");
            }
            if (firebaseVisionFace.getLandmark(7) == null) {
                log("Values", "Mouth------RIGHT_CHEEK value Not Found");
            }
            log("Values", "Mouth*******************************************************************************************");
            return;
        }
        FirebaseVisionPoint position = firebaseVisionFace.getLandmark(5).getPosition();
        float floatValue = position.getX().floatValue();
        float floatValue2 = position.getY().floatValue();
        FirebaseVisionPoint position2 = firebaseVisionFace.getLandmark(11).getPosition();
        float floatValue3 = position2.getX().floatValue();
        float floatValue4 = position2.getY().floatValue();
        FirebaseVisionPoint position3 = firebaseVisionFace.getLandmark(0).getPosition();
        float floatValue5 = position3.getX().floatValue();
        float floatValue6 = position3.getY().floatValue();
        FirebaseVisionPoint position4 = firebaseVisionFace.getLandmark(6).getPosition();
        float floatValue7 = position4.getX().floatValue();
        float floatValue8 = position4.getY().floatValue();
        FirebaseVisionPoint position5 = firebaseVisionFace.getLandmark(1).getPosition();
        float floatValue9 = position5.getX().floatValue();
        float floatValue10 = position5.getY().floatValue();
        FirebaseVisionPoint position6 = firebaseVisionFace.getLandmark(7).getPosition();
        float floatValue11 = position6.getX().floatValue();
        float floatValue12 = position6.getY().floatValue();
        log("Values", "Mouth------ mouthLeftXPosition == " + floatValue + "  mouthLeftYPosition == " + floatValue2);
        log("Values", "Mouth------mouthRightXPosition == " + floatValue3 + "  mouthRightYPosition == " + floatValue4);
        log("Values", "Mouth------mouthBottomXPosition == " + floatValue5 + "  mouthBottomYPosition == " + floatValue6);
        log("Values", "Mouth  NOSE------noseXPosition == " + floatValue7 + "  noseYPosition == " + floatValue8);
        log("Values", "Mouth  CHEEK------cheekLeftXPosition == " + floatValue9 + "  cheekLeftYPosition == " + floatValue10);
        log("Values", "Mouth  CHEEK------cheekRightXPosition == " + floatValue11 + "  cheekRightYPosition == " + floatValue12);
        log("Values", "Mouth-------------------------------------------------------------------------------------");
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float getDifferenceBetweenEyes(FirebaseVisionImage firebaseVisionImage) {
        FirebaseVisionFace firebaseVisionFace = this.face;
        if (firebaseVisionFace == null || firebaseVisionFace.getLandmark(4) == null || this.face.getLandmark(4).getPosition() == null) {
            return 0.0f;
        }
        float floatValue = this.face.getLandmark(4).getPosition().getX().floatValue();
        float floatValue2 = this.face.getLandmark(10).getPosition().getX().floatValue();
        float f = floatValue2 - floatValue;
        log("Values", "Eye Positions centerLeftX == " + floatValue + "centerRightX == " + floatValue2);
        log("Values", "Eye Positions Face in image X = " + (Utils.getUserEnrollCount(this) + 1) + " == " + ((f / ((float) firebaseVisionImage.getBitmap().getWidth())) * 100.0f));
        float width = (f / ((float) firebaseVisionImage.getBitmap().getWidth())) * 100.0f;
        StringBuilder sb = new StringBuilder();
        sb.append("X==  distance requiredPercent");
        sb.append(width);
        log("Values", sb.toString());
        return width;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float getEyesYPosition(FirebaseVisionImage firebaseVisionImage) {
        FirebaseVisionFace firebaseVisionFace = this.face;
        if (firebaseVisionFace == null || firebaseVisionFace.getLandmark(4) == null || this.face.getLandmark(4).getPosition() == null) {
            return 0.0f;
        }
        float floatValue = this.face.getLandmark(4).getPosition().getY().floatValue();
        float floatValue2 = this.face.getLandmark(10).getPosition().getY().floatValue();
        log("Values", "Eye Positions centerLeftY == " + floatValue + "centerRightY == " + floatValue2);
        log("Values", "Eye Positions Face in image Y= " + (Utils.getUserEnrollCount(this) + 1) + " == " + ((Math.min(floatValue, floatValue2) / ((float) firebaseVisionImage.getBitmap().getHeight())) * 100.0f));
        float min = (Math.min(floatValue, floatValue2) / ((float) firebaseVisionImage.getBitmap().getHeight())) * 100.0f;
        StringBuilder sb = new StringBuilder();
        sb.append("Y==  distance requiredPercent");
        sb.append(min);
        log("Values", sb.toString());
        return min;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean isEyesOpen(FirebaseVisionFace firebaseVisionFace) {
        return ((double) firebaseVisionFace.getLeftEyeOpenProbability()) >= 0.8d || ((double) firebaseVisionFace.getRightEyeOpenProbability()) >= 0.8d;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean isLookingStraight(FirebaseVisionFace firebaseVisionFace) {
        return firebaseVisionFace.getHeadEulerAngleZ() > -12.0f && firebaseVisionFace.getHeadEulerAngleZ() < 12.0f && firebaseVisionFace.getHeadEulerAngleY() > -12.0f && firebaseVisionFace.getHeadEulerAngleY() < 12.0f;
    }

    private boolean detectEyeblink(FirebaseVisionFace firebaseVisionFace) {
        if (firebaseVisionFace.getHeadEulerAngleY() >= -36.0f && firebaseVisionFace.getHeadEulerAngleY() <= 36.0f) {
            if (((double) this.leftEyeOpenProbability) <= 0.0d) {
                this.leftEyeOpenProbability = firebaseVisionFace.getLeftEyeOpenProbability();
            }
            if (((double) this.rightEyeOpenProbability) <= 0.0d) {
                this.rightEyeOpenProbability = firebaseVisionFace.getRightEyeOpenProbability();
            }
            if (((double) (this.rightEyeOpenProbability - firebaseVisionFace.getRightEyeOpenProbability())) > 0.1d || ((double) (firebaseVisionFace.getRightEyeOpenProbability() - this.rightEyeOpenProbability)) > 0.1d) {
                this.leftopenEyeCount++;
                this.leftcloseCount++;
            }
            if (((double) (this.leftEyeOpenProbability - firebaseVisionFace.getLeftEyeOpenProbability())) > 0.1d || ((double) (firebaseVisionFace.getLeftEyeOpenProbability() - this.leftEyeOpenProbability)) > 0.1d) {
                this.leftopenEyeCount++;
                this.leftcloseCount++;
            }
        }
        if (this.leftopenEyeCount > 1 || this.leftcloseCount > 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void nextFace(boolean z) {
        FaceSides faceSides2;
        if ((this.currentRandomPosition > 0 && !this.isLivenessTaken) || ((faceSides2 = this.presentSide) != null && faceSides2.equals(FaceSides.EYE_BLINK))) {
            captureSecretPhoto();
        }
        this.timeoutCountDownTimer.cancel();
        int i = this.challengeCompleteCount;
        int i2 = i + 1;
        int i3 = this.totalChallengeCount;
        if (i3 < 3) {
            i2 = i - 2;
        }
        if (this.currentRandomPosition >= this.randomCapturesCount || i2 == i3) {
            this.presentSide = null;
            this.gifImageView.stopAnimation();
            Drawable drawable = this.faceFront;
            if (drawable != null) {
                this.gifImageView.setImageDrawable(drawable);
            }
            this.textFace.setText(getString(R.string.label_look_straight));
            return;
        }
        try {
            this.timeoutCountDownTimer.start();
            int i4 = this.randomCapturesCount;
            if (i4 == 1) {
                FaceSides[] faceSidesArr = new FaceSides[2];
                this.faceSides = faceSidesArr;
                faceSidesArr[0] = FaceSides.LEFT;
                this.faceSides[1] = FaceSides.RIGHT;
                Collections.shuffle(Arrays.asList(this.faceSides));
                this.presentSide = this.faceSides[this.currentRandomPosition];
            } else if (i4 == 2) {
                if (this.currentRandomPosition == 0) {
                    FaceSides[] faceSidesArr2 = new FaceSides[2];
                    this.faceSides = faceSidesArr2;
                    faceSidesArr2[0] = FaceSides.LEFT;
                    this.faceSides[1] = FaceSides.RIGHT;
                    Collections.shuffle(Arrays.asList(this.faceSides));
                    FaceSides[] faceSidesArr3 = this.faceSides;
                    faceSidesArr3[0] = faceSidesArr3[0];
                    faceSidesArr3[1] = FaceSides.EYE_BLINK;
                    Collections.shuffle(Arrays.asList(this.faceSides));
                }
                this.presentSide = this.faceSides[this.currentRandomPosition];
            } else if (i4 == 3) {
                if (this.currentRandomPosition == 0) {
                    FaceSides[] faceSidesArr4 = new FaceSides[3];
                    this.faceSides = faceSidesArr4;
                    faceSidesArr4[0] = FaceSides.LEFT;
                    this.faceSides[1] = FaceSides.RIGHT;
                    this.faceSides[2] = FaceSides.EYE_BLINK;
                    Collections.shuffle(Arrays.asList(this.faceSides));
                }
                this.presentSide = this.faceSides[this.currentRandomPosition];
            } else {
                this.presentSide = this.faceSides[this.currentRandomPosition];
            }
            this.currentRandomPosition++;
            if (this.useBackCamera) {
                int i5 = AnonymousClass12.$SwitchMap$com$face$liveness$activity$TechCameraActivity$FaceSides[this.presentSide.ordinal()];
                if (i5 == 1) {
                    setGIFBytes(this.animGifs[FaceSides.EYE_BLINK.ordinal()]);
                } else if (i5 == 2) {
                    setGIFBytes(this.animGifs[FaceSides.RIGHT.ordinal()]);
                } else if (i5 == 3) {
                    setGIFBytes(this.animGifs[FaceSides.LEFT.ordinal()]);
                } else if (i5 == 4) {
                    setGIFBytes(this.animGifs[FaceSides.ANTI_CLOCK.ordinal()]);
                } else if (i5 == 5) {
                    setGIFBytes(this.animGifs[FaceSides.CLOCK.ordinal()]);
                }
            } else {
                setGIFBytes(this.animGifs[this.presentSide.ordinal()]);
            }
            GifImageView gifImageView2 = this.gifImageView;
            if (gifImageView2 != null) {
                gifImageView2.resetAnimation();
                this.gifImageView.stopAnimation();
            }
            this.delayCounterTimer.start();
            this.textFace.setVisibility(0);
            GifImageView gifImageView3 = this.gifImageView;
            if (gifImageView3 != null) {
                gifImageView3.startAnimation();
            }
        } catch (Exception e) {
            ExceptionUtility.logError("TechCameraActivity", "nextFace", e);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void vibrate() {
        this.vibe.vibrate(100);
        playBeep();
    }

    private void setLableName(FaceSides faceSides2) {
        int i = AnonymousClass12.$SwitchMap$com$face$liveness$activity$TechCameraActivity$FaceSides[faceSides2.ordinal()];
        if (i == 1) {
            this.textFace.setText(getString(R.string.label_blink_your_eyes));
        } else if (i == 2) {
            this.textFace.setText(getString(R.string.label_look_left));
        } else if (i == 3) {
            this.textFace.setText(getString(R.string.label_look_right));
        } else if (i == 4) {
            this.textFace.setText(getString(R.string.label_clockwise));
        } else if (i == 5) {
            this.textFace.setText(getString(R.string.label_anticlockwise));
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void playBeep() {
        ToneGenerator toneGenerator2 = new ToneGenerator(3, 100);
        this.toneGenerator = toneGenerator2;
        toneGenerator2.startTone(44, ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass7 */

            public void run() {
                if (TechCameraActivity.this.toneGenerator != null) {
                    Utils.showLogs("TechCameraActivity", "ToneGenerator released");
                    TechCameraActivity.this.toneGenerator.release();
                    TechCameraActivity.this.toneGenerator = null;
                }
            }
        }, 100);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        CountDownTimer countDownTimer2 = this.countDownTimer;
        if (countDownTimer2 != null) {
            countDownTimer2.cancel();
        }
        CountDownTimer countDownTimer3 = this.timeoutCountDownTimer;
        if (countDownTimer3 != null) {
            countDownTimer3.cancel();
        }
        CountDownTimer countDownTimer4 = this.delayCounterTimer;
        if (countDownTimer4 != null) {
            countDownTimer4.cancel();
        }
        try {
            FirebaseVision.getInstance().getVisionFaceDetector().close();
        } catch (Exception e) {
            ExceptionUtility.logError("TechCameraActivity", "onPause", e);
        }
        Camera camera = this.mCamera;
        if (camera != null) {
            camera.stopPreview();
            this.mCamera.setPreviewCallback(null);
            this.mCamera.release();
            this.mCamera = null;
        }
        if (!this.isUserActionFinish) {
            LivenessController.getT5LivenessListener().onError(ErrorUtils.activityPauseCode, ErrorUtils.activityPauseMessage);
        }
        userActionFinish();
        SensorManager sensorManager2 = this.sensorManager;
        if (sensorManager2 != null) {
            sensorManager2.unregisterListener(this);
        }
    }

    public void onBackPressed() {
        userActionFinish();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    private void draw() {
        try {
            Display defaultDisplay = getWindowManager().getDefaultDisplay();
            Bitmap createBitmap = Bitmap.createBitmap(defaultDisplay.getWidth(), defaultDisplay.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint(1);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawColor(-1);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            canvas.drawCircle((float) (canvas.getWidth() / 2), (float) (((double) canvas.getHeight()) * 0.51d), (float) (((double) canvas.getWidth()) * 0.44d), paint);
            this.mtransparentView.setImageBitmap(createBitmap);
        } catch (Exception e) {
            ExceptionUtility.logError("TechCameraActivity", "draw", e);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void drawBorderCircle(boolean z) {
        try {
            Display defaultDisplay = getWindowManager().getDefaultDisplay();
            Bitmap createBitmap = Bitmap.createBitmap(defaultDisplay.getWidth(), defaultDisplay.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(15.0f);
            paint.setColor(z ? -16711936 : SupportMenu.CATEGORY_MASK);
            canvas.drawCircle((float) (canvas.getWidth() / 2), (float) (((double) canvas.getHeight()) * 0.51d), (float) (((double) canvas.getWidth()) * 0.445d), paint);
            ((ImageView) findViewById(R.id.iv_border_view)).setImageBitmap(createBitmap);
        } catch (Exception e) {
            ExceptionUtility.logError("TechCameraActivity", "draw", e);
        }
    }

    private void initStartCountdown() {
        this.tvCountDownTimer.setText(String.valueOf(this.challengeStartCountdownInSec));
        this.challengeStartCountDownTimer = new CountDownTimer((long) (this.challengeStartCountdownInSec * 1000), 900) {
            /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass8 */

            public void onTick(long j) {
                TechCameraActivity.this.isStartCountdownCompleted = false;
                TechCameraActivity.this.tvCountDownTimer.setText(String.valueOf(TechCameraActivity.this.startCount));
                TechCameraActivity.this.playBeep();
                TechCameraActivity techCameraActivity = TechCameraActivity.this;
                techCameraActivity.startCount--;
            }

            public void onFinish() {
                TechCameraActivity.this.rlChallengeIndicator.clearAnimation();
                TechCameraActivity.this.isStartCountdownCompleted = true;
                TechCameraActivity.this.rlChallengeSuccessLayout.setVisibility(8);
                TechCameraActivity.this.llStartCountDownLayout.setVisibility(8);
                if (TechCameraActivity.this.timeoutCountDownTimer != null) {
                    TechCameraActivity.this.timeoutCountDownTimer.start();
                }
            }
        };
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showChallengeSuccess() {
        this.isStartCountdownCompleted = false;
        this.challengeCompleteCount++;
        setProgressCount();
        this.rlAnimationLayout.setVisibility(0);
        this.rlChallengeSuccessLayout.setVisibility(0);
        startAnimation();
        CountDownTimer countDownTimer2 = this.timeoutCountDownTimer;
        if (countDownTimer2 != null) {
            countDownTimer2.cancel();
        }
        new Handler().postDelayed(new Runnable() {
            /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass9 */

            public void run() {
                TechCameraActivity.this.rlAnimationLayout.setVisibility(8);
                if (TechCameraActivity.this.challengeCompleteCount < TechCameraActivity.this.totalChallengeCount) {
                    TechCameraActivity.this.challengeStartCountdown();
                    return;
                }
                TechCameraActivity.this.rlChallengeSuccessLayout.setVisibility(8);
                TechCameraActivity.this.isStartCountdownCompleted = true;
                if (TechCameraActivity.this.timeoutCountDownTimer != null) {
                    TechCameraActivity.this.timeoutCountDownTimer.start();
                }
            }
        }, 1000);
    }

    private void setProgressCount() {
        TextView textView = this.tvProgressCount;
        textView.setText(this.challengeCompleteCount + "/" + this.totalChallengeCount);
        int i = this.challengeCompleteCount;
        if (i > 0) {
            this.pbChallengeCompleted.setProgress((int) ((((double) i) / ((double) this.totalChallengeCount)) * 100.0d));
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void challengeStartCountdown() {
        this.isStartCountdownCompleted = false;
        startChallengeIndicatorAnimation();
        this.startCount = this.challengeStartCountdownInSec;
        CountDownTimer countDownTimer2 = this.challengeStartCountDownTimer;
        if (countDownTimer2 != null) {
            countDownTimer2.cancel();
        }
        CountDownTimer countDownTimer3 = this.timeoutCountDownTimer;
        if (countDownTimer3 != null) {
            countDownTimer3.cancel();
        }
        this.rlChallengeSuccessLayout.setVisibility(0);
        this.llStartCountDownLayout.setVisibility(0);
        this.challengeStartCountDownTimer.start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        CountDownTimer countDownTimer2 = this.challengeStartCountDownTimer;
        if (countDownTimer2 != null) {
            countDownTimer2.cancel();
        }
        CountDownTimer countDownTimer3 = this.timeoutCountDownTimer;
        if (countDownTimer3 != null) {
            countDownTimer3.cancel();
        }
        CountDownTimer countDownTimer4 = this.countDownTimer;
        if (countDownTimer4 != null) {
            countDownTimer4.cancel();
        }
        CountDownTimer countDownTimer5 = this.delayCounterTimer;
        if (countDownTimer5 != null) {
            countDownTimer5.cancel();
        }
    }

    private void startAnimation() {
        this.ivChallengeSuccess.startAnimation(this.zoomOut);
    }

    private void startChallengeIndicatorAnimation() {
        this.rlChallengeIndicator.startAnimation(this.zoomIn);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void userActionFinish() {
        this.isUserActionFinish = true;
        finish();
    }

    private void startCameraPreview() {
        try {
            releaseCameraAndPreview();
            this.mCamera = getCameraInstance();
            CameraPreview cameraPreview = new CameraPreview(this, this.mCamera, this.useBackCamera ? 0 : 1, this.cameraPreviewCallBack);
            this.maPreview = cameraPreview;
            this.maLayoutPreview.addView(cameraPreview);
            ((FrameLayout.LayoutParams) this.maPreview.getLayoutParams()).gravity = 17;
            initFaceDetection();
            nextFace(false);
            new Handler().postDelayed(new Runnable() {
                /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass10 */

                public void run() {
                    if (TechCameraActivity.this.totalChallengeCount > 0) {
                        TechCameraActivity.this.challengeStartCountdown();
                    } else {
                        TechCameraActivity.this.isStartCountdownCompleted = true;
                    }
                }
            }, 100);
        } catch (Exception e) {
            ExceptionUtility.logError("TechCameraActivity", "startCameraPreview", e);
        }
    }

    private Camera getCameraInstance() {
        if (this.mCamera == null) {
            this.mCamera = Camera.open(!this.useBackCamera ? 1 : 0);
        }
        return this.mCamera;
    }

    private void releaseCameraAndPreview() {
        Camera camera = this.mCamera;
        if (camera != null) {
            camera.release();
            this.mCamera = null;
        }
    }

    private void initCaptureFinalSelfie() {
        this.finalSelfieCountDownTimer = new CountDownTimer((long) (this.FINAL_SELFIE_COUNTDOWN_IN_SEC * 1000), 900) {
            /* class com.face.liveness.activity.TechCameraActivity.AnonymousClass11 */

            public void onTick(long j) {
                TechCameraActivity.this.isFinalSelfieImageInProgress = true;
                TechCameraActivity.this.textTimer.setText(String.valueOf(TechCameraActivity.this.startCount));
                TechCameraActivity.this.playBeep();
                TechCameraActivity.this.startCount--;
            }

            public void onFinish() {
                TechCameraActivity.this.isFinalSelfieImageInProgress = true;
                TechCameraActivity.this.textTimer.setVisibility(8);
                TechCameraActivity.this.capturePhoto(null);
            }
        };
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void stopFinalSelfieCapture() {
        this.isFinalSelfieImageInProgress = false;
        CountDownTimer countDownTimer2 = this.finalSelfieCountDownTimer;
        if (countDownTimer2 != null) {
            countDownTimer2.cancel();
        }
        this.textTimer.setVisibility(8);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void stopTimeOutCountDown() {
        CountDownTimer countDownTimer2 = this.timeoutCountDownTimer;
        if (countDownTimer2 != null) {
            countDownTimer2.cancel();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void checkFinalSelfie(byte[] bArr, Camera camera) {
        try {
            this.isFinalSelfieImageInProgress = true;
            showDialog(true);
            FirebaseVisionImage fromByteArray = FirebaseVisionImage.fromByteArray(bArr, this.metadata);
            this.detector.detectInImage(fromByteArray).addOnSuccessListener(new OnSuccessListener(fromByteArray, bArr, camera) {
                /* class com.face.liveness.activity.$$Lambda$TechCameraActivity$32Jhyrlmcu_5hRNjB4GSGAM7yM */
                public final /* synthetic */ FirebaseVisionImage f$1;
                public final /* synthetic */ byte[] f$2;
                public final /* synthetic */ Camera f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // com.google.android.gms.tasks.OnSuccessListener
                public final void onSuccess(Object obj) {
                    TechCameraActivity.this.lambda$checkFinalSelfie$0$TechCameraActivity(this.f$1, this.f$2, this.f$3, (List) obj);
                }
            }).addOnFailureListener(new OnFailureListener() {
                /* class com.face.liveness.activity.$$Lambda$TechCameraActivity$FpCXJ1isExrxYKmc3xFh3IcKtwg */

                @Override // com.google.android.gms.tasks.OnFailureListener
                public final void onFailure(Exception exc) {
                    TechCameraActivity.this.lambda$checkFinalSelfie$1$TechCameraActivity(exc);
                }
            });
        } catch (Exception e) {
            reTake();
            ExceptionUtility.logError("TechCameraActivity", "detectInImage Listner", e);
        }
    }

    public /* synthetic */ void lambda$checkFinalSelfie$0$TechCameraActivity(FirebaseVisionImage firebaseVisionImage, byte[] bArr, Camera camera, List list) {
        if (list == null || list.size() <= 0) {
            reTake();
            if (this.startTime == 1 && !this.isActivityFinished) {
                this.isActivityFinished = true;
                LivenessController.getT5LivenessListener().onError(ErrorUtils.contoursCode, ErrorUtils.contoursMessage);
                userActionFinish();
                return;
            }
            return;
        }
        Utils.showLogs("TechCameraActivity", "Faces " + list.size());
        FirebaseVisionFace firebaseVisionFace = (FirebaseVisionFace) list.get(0);
        stopTimeOutCountDown();
        boolean isEyesOpen = isEyesOpen(firebaseVisionFace);
        boolean isLookingStraight = isLookingStraight(firebaseVisionFace);
        float differenceBetweenEyes = getDifferenceBetweenEyes(firebaseVisionImage);
        float eyesYPosition = getEyesYPosition(firebaseVisionImage);
        if (list.size() != 1 || !isLookingStraight || !isEyesOpen || differenceBetweenEyes < ((float) Constants.EYES_X_MIN_THRESHOLD) || differenceBetweenEyes >= ((float) Constants.EYES_X_MAX_THRESHOLD) || eyesYPosition < ((float) Constants.EYES_Y_MIN_THRESHOLD) || eyesYPosition >= ((float) Constants.EYES_Y_MAX_THRESHOLD)) {
            stopFinalSelfieCapture();
            reTake();
            if (list.size() > 1) {
                this.textFace.setText(getString(R.string.multiple_faces_error));
            } else if (!isLookingStraight) {
                showMessage(getString(R.string.label_look_straight));
            } else if (!isEyesOpen) {
                showMessage(getString(R.string.open_eyes_message));
            } else if (differenceBetweenEyes < ((float) Constants.EYES_X_MIN_THRESHOLD)) {
                showMessage(getString(R.string.camera_close_message));
            } else if (differenceBetweenEyes >= ((float) Constants.EYES_X_MAX_THRESHOLD)) {
                showMessage(getString(R.string.camera_far_message));
            } else if (eyesYPosition < ((float) Constants.EYES_Y_MIN_THRESHOLD)) {
                showMessage(getString(R.string.camera_up_message));
            } else if (eyesYPosition >= ((float) Constants.EYES_Y_MAX_THRESHOLD)) {
                showMessage(getString(R.string.camera_down_message));
            } else if (!this.isDeviceAccelerometerCorrect) {
                this.isDeviceAccelerometerCorrect = true;
                this.textFace.setText(getString(R.string.device_accelerometer_error_message));
            }
        } else {
            sendResult(convertYuvToJpeg(bArr, camera));
        }
    }

    public /* synthetic */ void lambda$checkFinalSelfie$1$TechCameraActivity(Exception exc) {
        reTake();
        ExceptionUtility.logError("TechCameraActivity", "detectInImage_ onFailure", exc);
        Utils.showLogs("livenessTask fail", exc.getMessage());
    }

    private void reTake() {
        showDialog(false);
        this.mCamera.startPreview();
        this.mCamera.setPreviewCallback(this.cameraPreviewCallBack);
        this.isFinalSelfieImageInProgress = false;
        this.isPictureTaken = false;
    }

    private void sendResult(byte[] bArr) {
        int i = 0;
        try {
            showDialog(false);
            if (!this.useBackCamera) {
                i = 1;
            }
            byte[] rotateImageData = Utils.rotateImageData(this, bArr, i);
            if (this.isImageCompressed) {
                this.livenessImageFrontSelfie = Utils.compressFaceImage(rotateImageData, Constants.REQUIRED_HEIGHT, Constants.COMPRESSION_RATE);
            } else {
                this.livenessImageFrontSelfie = Utils.compressFaceImage(rotateImageData, Constants.REQUIRED_HEIGHT, Constants.NO_COMPRESSION_RATE);
            }
            userActionFinish();
            LivenessController.getT5LivenessListener().onSuccess(this.livenessImageFrontSelfie, this.livenessImage);
        } catch (Exception e) {
            reTake();
            ExceptionUtility.logError("TechCameraActivity", "sendResult", e);
        }
    }

    private void showMessage(String str) {
        runOnUiThread(new Runnable(str) {
            /* class com.face.liveness.activity.$$Lambda$TechCameraActivity$SJhc_pr6clgNc55jBqyDxBALms */
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                TechCameraActivity.this.lambda$showMessage$2$TechCameraActivity(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$showMessage$2$TechCameraActivity(String str) {
        Toast.makeText(this, str, 1).show();
    }

    private void showDialog(boolean z) {
        if (this.dialog == null) {
            ProgressDialog progressDialog = new ProgressDialog(this);
            this.dialog = progressDialog;
            progressDialog.setMessage(getString(R.string.please_wait));
            this.dialog.setCancelable(false);
        }
        if (z) {
            this.dialog.show();
        } else {
            this.dialog.dismiss();
        }
    }

    public byte[] convertYuvToJpeg(byte[] bArr, Camera camera) {
        YuvImage yuvImage = new YuvImage(bArr, 17, camera.getParameters().getPreviewSize().width, camera.getParameters().getPreviewSize().height, null);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, camera.getParameters().getPreviewSize().width, camera.getParameters().getPreviewSize().height), 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        float f = sensorEvent.values[0];
        float f2 = sensorEvent.values[1];
        log("TechCameraActivity", "Accelerometer X= " + f + " Y= " + f2);
        double d = (double) f;
        if (d >= -2.5d && d <= 2.5d) {
            double d2 = (double) f2;
            if (d2 >= 7.5d && d2 <= 11.5d) {
                return;
            }
        }
        this.isDeviceAccelerometerCorrect = false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void log(String str, String str2) {
        Log.i(str, str2);
    }
}
