package com.face.liveness;

import android.graphics.Bitmap;
import java.util.ArrayList;

public interface T5OnFacesResultListener {
    void onFacesError(int i, String str);

    void onFacesResult(ArrayList<Bitmap> arrayList);
}
