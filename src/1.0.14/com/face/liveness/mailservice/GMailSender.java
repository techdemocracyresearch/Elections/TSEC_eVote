package com.face.liveness.mailservice;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Security;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class GMailSender extends Authenticator {
    private Multipart _multipart;
    private String mailhost = "smtp.gmail.com";
    private String password;
    private Session session;
    private String user;

    static {
        Security.addProvider(new JSSEProvider());
    }

    public GMailSender(String str, String str2) {
        this.user = str;
        this.password = str2;
        Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol", "smtp");
        properties.setProperty("mail.host", this.mailhost);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.quitwait", "false");
        this.session = Session.getDefaultInstance(properties, this);
        this._multipart = new MimeMultipart();
    }

    /* access modifiers changed from: protected */
    @Override // javax.mail.Authenticator
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(this.user, this.password);
    }

    public synchronized void sendMail(String str, String str2, String str3, String str4) throws Exception {
        MimeMessage mimeMessage = new MimeMessage(this.session);
        DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(str2.getBytes(), "text/plain"));
        mimeMessage.setSender(new InternetAddress(str3));
        mimeMessage.setSubject(str);
        mimeMessage.setDataHandler(dataHandler);
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setText(str2);
        this._multipart.addBodyPart(mimeBodyPart);
        mimeMessage.setContent(this._multipart);
        if (str4.indexOf(44) > 0) {
            mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(str4));
        } else {
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(str4));
        }
        Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
        Transport.send(mimeMessage);
    }

    public void addAttachment(String str) throws Exception {
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(str)));
        mimeBodyPart.setFileName(str);
        this._multipart.addBodyPart(mimeBodyPart);
    }

    public class ByteArrayDataSource implements DataSource {
        private byte[] data;
        private String type;

        @Override // javax.activation.DataSource
        public String getName() {
            return "ByteArrayDataSource";
        }

        public ByteArrayDataSource(byte[] bArr, String str) {
            this.data = bArr;
            this.type = str;
        }

        public ByteArrayDataSource(byte[] bArr) {
            this.data = bArr;
        }

        public void setType(String str) {
            this.type = str;
        }

        @Override // javax.activation.DataSource
        public String getContentType() {
            String str = this.type;
            return str == null ? "application/octet-stream" : str;
        }

        @Override // javax.activation.DataSource
        public InputStream getInputStream() throws IOException {
            return new ByteArrayInputStream(this.data);
        }

        @Override // javax.activation.DataSource
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("Not Supported");
        }
    }
}
