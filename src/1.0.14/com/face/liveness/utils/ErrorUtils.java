package com.face.liveness.utils;

public class ErrorUtils {
    public static int activityPauseCode = 5016;
    public static String activityPauseMessage = "Liveness check process is paused";
    public static int camErrorCode = 5000;
    public static String camErrorMessage = "You cant access to liveness, as camera permission is not enable.";
    public static String captureTimeOutRangeMessage = "Photo capture timeout in sec should be between 0 to 3 seconds";
    public static int captureTimeoutRangeCode = 5014;
    public static int contoursCode = 5012;
    public static String contoursMessage = "Please open your eye, look straight and try again.";
    public static int faceNotFoundCode = 5004;
    public static String faceNotFoundMessage = "Face not found";
    public static int faceToFarCode = 5007;
    public static String faceToFarMessage = "Face is too Far";
    public static int invalidIdentifierKeyCode = 5009;
    public static String invalidIdentifierKeyMessage = "This key is not valid for this identifier ";
    public static int invalidImageCode = 5005;
    public static String invalidImageMessage = "Invalid Image";
    public static int invalidKeyCode = 5011;
    public static String invalidKeyMessage = "Invalid Key";
    public static int invalidRandomCode = 5001;
    public static String invalidRandomMessage = "Please enter challenges between 0 to 5";
    public static int licenseKeyExpiredCode = 5010;
    public static String licenseKeyExpiredMessage = "This license key has expired on ";
    public static int multipleFaceDetectedCode = 5006;
    public static String multipleFaceDetectedMessage = "Multiple faces detected";
    public static String spoofing = " You are not blinking your eyes.";
    public static String startCountdownTimeMessage = "Preparation time should be in between 1 to 5 seconds";
    public static int startCountdownTimeRangeCode = 5013;
    public static String timeOutMessage = "Unable to capture liveness selfie, timeout error";
    public static String timeOutRangeMessage = "Challenge timeout in sec should be between 3 to 10 seconds";
    public static int timeoutCode = 5003;
    public static int timeoutRangeCode = 5002;
    public static int timeoutValidationCode = 5015;
    public static String timeoutValidationMessage = "Challenge timeout in sec must be greater than photo capture timeout by at least 2 seconds";
    public static int wrongMovementCode = 5008;
    public static String wrongMovementMessage = "You did %S movement it should be %S";
}
