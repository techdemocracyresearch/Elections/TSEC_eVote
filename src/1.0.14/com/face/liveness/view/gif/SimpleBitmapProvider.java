package com.face.liveness.view.gif;

import android.graphics.Bitmap;
import com.face.liveness.view.gif.GifDecoder;

final class SimpleBitmapProvider implements GifDecoder.BitmapProvider {
    @Override // com.face.liveness.view.gif.GifDecoder.BitmapProvider
    public void release(byte[] bArr) {
    }

    @Override // com.face.liveness.view.gif.GifDecoder.BitmapProvider
    public void release(int[] iArr) {
    }

    SimpleBitmapProvider() {
    }

    @Override // com.face.liveness.view.gif.GifDecoder.BitmapProvider
    public Bitmap obtain(int i, int i2, Bitmap.Config config) {
        return Bitmap.createBitmap(i, i2, config);
    }

    @Override // com.face.liveness.view.gif.GifDecoder.BitmapProvider
    public void release(Bitmap bitmap) {
        bitmap.recycle();
    }

    @Override // com.face.liveness.view.gif.GifDecoder.BitmapProvider
    public byte[] obtainByteArray(int i) {
        return new byte[i];
    }

    @Override // com.face.liveness.view.gif.GifDecoder.BitmapProvider
    public int[] obtainIntArray(int i) {
        return new int[i];
    }
}
