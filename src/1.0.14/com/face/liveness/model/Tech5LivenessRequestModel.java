package com.face.liveness.model;

import com.face.liveness.T5LivenessListener;
import java.io.Serializable;

public class Tech5LivenessRequestModel implements Serializable {
    private int challengeStartCounterInSec;
    private int challengeTimeoutInSec;
    private int challengesCount;
    private boolean isCompression;
    private boolean isShowImageInstructions;
    private boolean isShowSelfieCaptureButton;
    private boolean isShowTextInstructions;
    private boolean isUseBackCamera;
    private String languageCode;

    private Tech5LivenessRequestModel() {
        this.challengesCount = 5;
        this.challengeTimeoutInSec = 10;
        this.challengeStartCounterInSec = 3;
        this.languageCode = "en";
    }

    private Tech5LivenessRequestModel(int i, int i2, boolean z, int i3, String str, boolean z2, int i4, boolean z3, boolean z4, boolean z5, T5LivenessListener t5LivenessListener) {
        this.challengesCount = i;
        this.challengeTimeoutInSec = i2;
        this.isUseBackCamera = z;
        this.languageCode = str;
        this.isCompression = z2;
        this.challengeStartCounterInSec = i4;
        this.isShowTextInstructions = z3;
        this.isShowImageInstructions = z4;
        this.isShowSelfieCaptureButton = z5;
    }

    private Tech5LivenessRequestModel(Builder builder) {
        this.challengesCount = builder.challengesCount;
        this.challengeTimeoutInSec = builder.challengeTimeoutInSec;
        this.isUseBackCamera = builder.isUseBackCamera;
        this.languageCode = builder.languageCode;
        this.isCompression = builder.isCompression;
        this.challengeStartCounterInSec = builder.challengeStartCounterInSec;
        this.isShowTextInstructions = builder.isShowTextInstructions;
        this.isShowImageInstructions = builder.isShowImageInstructions;
        this.isShowSelfieCaptureButton = builder.isShowSelfieCaptureButton;
    }

    public int getChallengesCount() {
        return this.challengesCount;
    }

    public void setChallengesCount(int i) {
        this.challengesCount = i;
    }

    public int getChallengeTimeoutInSec() {
        return this.challengeTimeoutInSec;
    }

    public void setChallengeTimeoutInSec(int i) {
        this.challengeTimeoutInSec = i;
    }

    public boolean isUseBackCamera() {
        return this.isUseBackCamera;
    }

    public void setUseBackCamera(boolean z) {
        this.isUseBackCamera = z;
    }

    public String getLanguageCode() {
        return this.languageCode;
    }

    public void setLanguageCode(String str) {
        this.languageCode = str;
    }

    public boolean isCompression() {
        return this.isCompression;
    }

    public void setCompression(boolean z) {
        this.isCompression = z;
    }

    public int getChallengeStartCounterInSec() {
        return this.challengeStartCounterInSec;
    }

    public void setChallengeStartCounterInSec(int i) {
        this.challengeStartCounterInSec = i;
    }

    public boolean isShowTextInstructions() {
        return this.isShowTextInstructions;
    }

    public void setShowTextInstructions(boolean z) {
        this.isShowTextInstructions = z;
    }

    public boolean isShowImageInstructions() {
        return this.isShowImageInstructions;
    }

    public void setShowImageInstructions(boolean z) {
        this.isShowImageInstructions = z;
    }

    public boolean isShowSelfieCaptureButton() {
        return this.isShowSelfieCaptureButton;
    }

    public void setShowSelfieCaptureButton(boolean z) {
        this.isShowSelfieCaptureButton = z;
    }

    public String toString() {
        return "Tech5LivenessRequestModel{challengesCount=" + this.challengesCount + ", challengeTimeoutInSec=" + this.challengeTimeoutInSec + ", isUseBackCamera=" + this.isUseBackCamera + ", languageCode='" + this.languageCode + '\'' + ", isCompression=" + this.isCompression + ", challengeStartCounterInSec=" + this.challengeStartCounterInSec + ", isShowTextInstructions=" + this.isShowTextInstructions + ", isShowImageInstructions=" + this.isShowImageInstructions + ", isShowSelfieCaptureButton=" + this.isShowSelfieCaptureButton + '}';
    }

    public static class Builder implements Serializable {
        private Builder builder;
        private int challengeStartCounterInSec = 3;
        private int challengeTimeoutInSec = 10;
        private int challengesCount = 5;
        private boolean isCompression;
        private boolean isShowImageInstructions;
        private boolean isShowSelfieCaptureButton;
        private boolean isShowTextInstructions;
        private boolean isUseBackCamera;
        private String languageCode = "en";

        public Builder setChallengesCount(int i) {
            this.challengesCount = i;
            return this;
        }

        public Builder setChallengeTimeoutInSec(int i) {
            this.challengeTimeoutInSec = i;
            return this;
        }

        public Builder setIsUseBackCamera(boolean z) {
            this.isUseBackCamera = z;
            return this;
        }

        public Builder setLanguageCode(String str) {
            this.languageCode = str;
            return this;
        }

        public Builder setIsCompression(boolean z) {
            this.isCompression = z;
            return this;
        }

        public Builder setChallengeStartCounterInSec(int i) {
            this.challengeStartCounterInSec = i;
            return this;
        }

        public Builder setIsShowTextInstructions(boolean z) {
            this.isShowTextInstructions = z;
            return this;
        }

        public Builder setIsShowImageInstructions(boolean z) {
            this.isShowImageInstructions = z;
            return this;
        }

        public Builder setIsShowSelfieCaptureButton(boolean z) {
            this.isShowSelfieCaptureButton = z;
            return this;
        }

        public Builder setBuilder(Builder builder2) {
            this.builder = builder2;
            return this;
        }

        public Tech5LivenessRequestModel create() {
            return new Tech5LivenessRequestModel(this);
        }
    }
}
