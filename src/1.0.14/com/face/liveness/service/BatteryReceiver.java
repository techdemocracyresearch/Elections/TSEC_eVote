package com.face.liveness.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BatteryReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        int intExtra = intent.getIntExtra("level", 0);
        int intExtra2 = intent.getIntExtra("scale", 0);
        int intExtra3 = intent.getIntExtra("health", 1);
        Log.i("AlaramReceiver", "battery Percentage@@@2" + ((((float) intExtra) / ((float) intExtra2)) * 100.0f) + "\n battery health" + intExtra3);
    }
}
