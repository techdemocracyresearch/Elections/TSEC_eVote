package com.face.liveness.live;

import com.google.firebase.ml.vision.face.FirebaseVisionFace;

public class LivenessProcess {
    private double curTol;
    private int currentMaxNumTasks;
    private ProcessingState currentState = ProcessingState.NO_FACE;
    private Task currentTask;
    private int maxNumTask;
    private final double minTol = 7.0d;
    private int numTaskSuccess = 0;
    private int numToSuceed;
    private int waitingTime = 0;
    private int waitingTimeout;

    /* access modifiers changed from: private */
    public enum ProcessingState {
        NO_FACE,
        WAITING,
        MATCHED
    }

    public LivenessProcess(int i, int i2, int i3) {
        this.maxNumTask = i;
        this.numToSuceed = i2;
        this.waitingTimeout = i3;
        this.currentMaxNumTasks = i;
        this.currentTask = new Task();
        this.curTol = 7.0d;
    }

    public void init() {
        this.currentState = ProcessingState.NO_FACE;
        this.currentTask.init();
        this.numTaskSuccess = 0;
        this.currentMaxNumTasks = this.maxNumTask;
    }

    /* renamed from: com.face.liveness.live.LivenessProcess$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$face$liveness$live$LivenessProcess$ProcessingState;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|(3:5|6|8)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        static {
            int[] iArr = new int[ProcessingState.values().length];
            $SwitchMap$com$face$liveness$live$LivenessProcess$ProcessingState = iArr;
            iArr[ProcessingState.NO_FACE.ordinal()] = 1;
            $SwitchMap$com$face$liveness$live$LivenessProcess$ProcessingState[ProcessingState.WAITING.ordinal()] = 2;
            try {
                $SwitchMap$com$face$liveness$live$LivenessProcess$ProcessingState[ProcessingState.MATCHED.ordinal()] = 3;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public Task updateFrame(FirebaseVisionFace firebaseVisionFace) {
        int i = AnonymousClass1.$SwitchMap$com$face$liveness$live$LivenessProcess$ProcessingState[this.currentState.ordinal()];
        if (i == 1) {
            this.currentTask.next();
            this.currentState = ProcessingState.WAITING;
        } else if (i != 2) {
            if (i != 3) {
                init();
            } else {
                suceedTask();
            }
        } else if (this.currentTask.check(firebaseVisionFace, 7.0d).booleanValue()) {
            suceedTask();
            setTol(firebaseVisionFace);
        } else {
            int i2 = this.waitingTime + 1;
            this.waitingTime = i2;
            if (i2 >= this.waitingTimeout || !this.currentTask.check(firebaseVisionFace, this.curTol).booleanValue()) {
                failTask();
                setTol(firebaseVisionFace);
            }
        }
        decreaseTol();
        return this.currentTask;
    }

    private void suceedTask() {
        int i = this.numTaskSuccess + 1;
        this.numTaskSuccess = i;
        if (i >= this.numToSuceed) {
            this.currentTask.pass();
            this.currentState = ProcessingState.NO_FACE;
        } else {
            this.currentTask.next();
            this.currentState = ProcessingState.WAITING;
        }
        this.waitingTime = 0;
    }

    private void failTask() {
        int i = this.currentMaxNumTasks - 1;
        this.currentMaxNumTasks = i;
        if (i < this.numToSuceed) {
            this.currentTask.fail();
            this.currentState = ProcessingState.NO_FACE;
        } else {
            this.currentTask.next();
            this.currentState = ProcessingState.WAITING;
        }
        this.waitingTime = 0;
    }

    private void decreaseTol() {
        this.curTol = Math.max(7.0d, this.curTol - 0.1d);
    }

    private void setTol(FirebaseVisionFace firebaseVisionFace) {
        this.curTol = this.currentTask.destination.distanceLM(new Point((double) firebaseVisionFace.getHeadEulerAngleY(), (double) firebaseVisionFace.getHeadEulerAngleZ())) + 7.0d;
    }
}
