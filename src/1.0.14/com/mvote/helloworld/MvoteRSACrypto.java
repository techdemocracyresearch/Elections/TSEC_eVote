package com.mvote.helloworld;

import android.util.Base64;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.json.JSONException;
import org.json.JSONObject;

public class MvoteRSACrypto {
    private static final int CRYPTO_BITS = 2048;
    private static final String CRYPTO_METHOD = "RSA/NONE/PKCS1Padding";
    public static String keyHash;
    public static String keyHashVVPAT;
    public static String plain;
    Cipher cipher_priv;
    Cipher cipher_pub;
    String decrypted;
    byte[] decryptedBytes;
    String encrypted;
    byte[] encryptedBytes;

    public String encrypt(String str, String str2, String str3, String str4) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("vek", str2);
            jSONObject.put("iv", str3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        plain = jSONObject.toString().replace("\\n", "");
        SingleTone.getInstance().setKEY_HASH(plain);
        PublicKey stringToPublicKey = stringToPublicKey(str4);
        Cipher instance = Cipher.getInstance(CRYPTO_METHOD);
        this.cipher_pub = instance;
        instance.init(1, stringToPublicKey);
        byte[] doFinal = this.cipher_pub.doFinal(plain.getBytes(StandardCharsets.UTF_8));
        this.encryptedBytes = doFinal;
        return Base64.encodeToString(doFinal, 2);
    }

    public String decrypt(String str, String str2) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, JSONException {
        PrivateKey stringToPrivateKey = stringToPrivateKey(str2);
        Cipher instance = Cipher.getInstance(CRYPTO_METHOD);
        this.cipher_priv = instance;
        instance.init(2, stringToPrivateKey);
        this.decryptedBytes = this.cipher_priv.doFinal(Base64.decode(str, 2));
        this.decrypted = new String(this.decryptedBytes);
        new JSONObject(this.decrypted);
        return this.decrypted;
    }

    public static PublicKey stringToPublicKey(String str) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        try {
            return (RSAPublicKey) KeyFactory.getInstance(CRYPTO_METHOD).generatePublic(new X509EncodedKeySpec(Base64.decode(str, 2)));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PrivateKey stringToPrivateKey(String str) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        try {
            return KeyFactory.getInstance(CRYPTO_METHOD).generatePrivate(new PKCS8EncodedKeySpec(Base64.decode(str, 2)));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }

    public PublicKey extractPublicKeyFromCertificate(String str) {
        try {
            return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.decode(str, 2)))).getPublicKey();
        } catch (CertificateException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String encrypt(String str, String str2, String str3, PublicKey publicKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("vek", str2);
            jSONObject.put("iv", str3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        plain = jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
        SingleTone.getInstance().setKEY_HASH(plain);
        Cipher instance = Cipher.getInstance(CRYPTO_METHOD);
        this.cipher_pub = instance;
        instance.init(1, publicKey);
        byte[] doFinal = this.cipher_pub.doFinal(plain.getBytes(StandardCharsets.UTF_8));
        this.encryptedBytes = doFinal;
        return Base64.encodeToString(doFinal, 2);
    }
}
