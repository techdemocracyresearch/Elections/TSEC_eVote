package com.mvote.helloworld;

import android.util.Base64;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.json.JSONObject;

public class VoteEncryptBridge {
    public String getStrFrom64(String str) {
        return new String(Base64.decode(str, 2));
    }

    public String encryptVote(String str, String str2) {
        String str3;
        NoSuchAlgorithmException e;
        NoSuchPaddingException e2;
        IllegalBlockSizeException e3;
        BadPaddingException e4;
        InvalidKeyException e5;
        UnsupportedEncodingException e6;
        InvalidAlgorithmParameterException e7;
        JSONObject jSONObject = new JSONObject();
        MvoteCrypto mvoteCrypto = new MvoteCrypto();
        MvoteRSACrypto mvoteRSACrypto = new MvoteRSACrypto();
        byte[] vek = mvoteCrypto.getVek();
        byte[] iv = mvoteCrypto.getIv();
        try {
            str3 = mvoteCrypto.encrypt(str2);
            try {
                Log.d("MVOTE", "Encrypted Vote: " + str3);
            } catch (NoSuchAlgorithmException e8) {
                e = e8;
            } catch (NoSuchPaddingException e9) {
                e2 = e9;
                e2.printStackTrace();
                PublicKey extractPublicKeyFromCertificate = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString = Base64.encodeToString(vek, 2);
                String encodeToString2 = Base64.encodeToString(iv, 2);
                Arrays.toString(extractPublicKeyFromCertificate.getEncoded());
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2);
                String encrypt = mvoteRSACrypto.encrypt(str3, encodeToString, encodeToString2, extractPublicKeyFromCertificate);
                String sha256 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                String sha2562 = MvoteHash.getSHA256(str2);
                jSONObject.put("encrypted_vote", str3);
                jSONObject.put("encrypted_vek", encrypt);
                jSONObject.put("encrypted_vote_hash", sha2562);
                jSONObject.put("key_hash", sha256);
                Log.d("MVOTE", "Encrypted Vote Hash: " + sha2562);
                return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
            } catch (IllegalBlockSizeException e10) {
                e3 = e10;
                e3.printStackTrace();
                PublicKey extractPublicKeyFromCertificate2 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString3 = Base64.encodeToString(vek, 2);
                String encodeToString22 = Base64.encodeToString(iv, 2);
                Arrays.toString(extractPublicKeyFromCertificate2.getEncoded());
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22);
                String encrypt2 = mvoteRSACrypto.encrypt(str3, encodeToString3, encodeToString22, extractPublicKeyFromCertificate2);
                String sha2563 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                String sha25622 = MvoteHash.getSHA256(str2);
                jSONObject.put("encrypted_vote", str3);
                jSONObject.put("encrypted_vek", encrypt2);
                jSONObject.put("encrypted_vote_hash", sha25622);
                jSONObject.put("key_hash", sha2563);
                Log.d("MVOTE", "Encrypted Vote Hash: " + sha25622);
                return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
            } catch (BadPaddingException e11) {
                e4 = e11;
                e4.printStackTrace();
                PublicKey extractPublicKeyFromCertificate22 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString32 = Base64.encodeToString(vek, 2);
                String encodeToString222 = Base64.encodeToString(iv, 2);
                Arrays.toString(extractPublicKeyFromCertificate22.getEncoded());
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString32);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString222);
                String encrypt22 = mvoteRSACrypto.encrypt(str3, encodeToString32, encodeToString222, extractPublicKeyFromCertificate22);
                String sha25632 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                String sha256222 = MvoteHash.getSHA256(str2);
                jSONObject.put("encrypted_vote", str3);
                jSONObject.put("encrypted_vek", encrypt22);
                jSONObject.put("encrypted_vote_hash", sha256222);
                jSONObject.put("key_hash", sha25632);
                Log.d("MVOTE", "Encrypted Vote Hash: " + sha256222);
                return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
            } catch (InvalidKeyException e12) {
                e5 = e12;
                e5.printStackTrace();
                PublicKey extractPublicKeyFromCertificate222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString322 = Base64.encodeToString(vek, 2);
                String encodeToString2222 = Base64.encodeToString(iv, 2);
                Arrays.toString(extractPublicKeyFromCertificate222.getEncoded());
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString322);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2222);
                String encrypt222 = mvoteRSACrypto.encrypt(str3, encodeToString322, encodeToString2222, extractPublicKeyFromCertificate222);
                String sha256322 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                String sha2562222 = MvoteHash.getSHA256(str2);
                jSONObject.put("encrypted_vote", str3);
                jSONObject.put("encrypted_vek", encrypt222);
                jSONObject.put("encrypted_vote_hash", sha2562222);
                jSONObject.put("key_hash", sha256322);
                Log.d("MVOTE", "Encrypted Vote Hash: " + sha2562222);
                return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
            } catch (UnsupportedEncodingException e13) {
                e6 = e13;
                e6.printStackTrace();
                PublicKey extractPublicKeyFromCertificate2222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString3222 = Base64.encodeToString(vek, 2);
                String encodeToString22222 = Base64.encodeToString(iv, 2);
                Arrays.toString(extractPublicKeyFromCertificate2222.getEncoded());
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3222);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22222);
                String encrypt2222 = mvoteRSACrypto.encrypt(str3, encodeToString3222, encodeToString22222, extractPublicKeyFromCertificate2222);
                String sha2563222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                String sha25622222 = MvoteHash.getSHA256(str2);
                jSONObject.put("encrypted_vote", str3);
                jSONObject.put("encrypted_vek", encrypt2222);
                jSONObject.put("encrypted_vote_hash", sha25622222);
                jSONObject.put("key_hash", sha2563222);
                Log.d("MVOTE", "Encrypted Vote Hash: " + sha25622222);
                return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
            } catch (InvalidAlgorithmParameterException e14) {
                e7 = e14;
                e7.printStackTrace();
                PublicKey extractPublicKeyFromCertificate22222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString32222 = Base64.encodeToString(vek, 2);
                String encodeToString222222 = Base64.encodeToString(iv, 2);
                Arrays.toString(extractPublicKeyFromCertificate22222.getEncoded());
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString32222);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString222222);
                String encrypt22222 = mvoteRSACrypto.encrypt(str3, encodeToString32222, encodeToString222222, extractPublicKeyFromCertificate22222);
                String sha25632222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                String sha256222222 = MvoteHash.getSHA256(str2);
                jSONObject.put("encrypted_vote", str3);
                jSONObject.put("encrypted_vek", encrypt22222);
                jSONObject.put("encrypted_vote_hash", sha256222222);
                jSONObject.put("key_hash", sha25632222);
                Log.d("MVOTE", "Encrypted Vote Hash: " + sha256222222);
                return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
            }
        } catch (NoSuchAlgorithmException e15) {
            e = e15;
            str3 = "";
            e.printStackTrace();
            PublicKey extractPublicKeyFromCertificate222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString322222 = Base64.encodeToString(vek, 2);
            String encodeToString2222222 = Base64.encodeToString(iv, 2);
            Arrays.toString(extractPublicKeyFromCertificate222222.getEncoded());
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString322222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2222222);
            String encrypt222222 = mvoteRSACrypto.encrypt(str3, encodeToString322222, encodeToString2222222, extractPublicKeyFromCertificate222222);
            String sha256322222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            String sha2562222222 = MvoteHash.getSHA256(str2);
            jSONObject.put("encrypted_vote", str3);
            jSONObject.put("encrypted_vek", encrypt222222);
            jSONObject.put("encrypted_vote_hash", sha2562222222);
            jSONObject.put("key_hash", sha256322222);
            Log.d("MVOTE", "Encrypted Vote Hash: " + sha2562222222);
            return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
        } catch (NoSuchPaddingException e16) {
            e2 = e16;
            str3 = "";
            e2.printStackTrace();
            PublicKey extractPublicKeyFromCertificate2222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString3222222 = Base64.encodeToString(vek, 2);
            String encodeToString22222222 = Base64.encodeToString(iv, 2);
            Arrays.toString(extractPublicKeyFromCertificate2222222.getEncoded());
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22222222);
            String encrypt2222222 = mvoteRSACrypto.encrypt(str3, encodeToString3222222, encodeToString22222222, extractPublicKeyFromCertificate2222222);
            String sha2563222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            String sha25622222222 = MvoteHash.getSHA256(str2);
            jSONObject.put("encrypted_vote", str3);
            jSONObject.put("encrypted_vek", encrypt2222222);
            jSONObject.put("encrypted_vote_hash", sha25622222222);
            jSONObject.put("key_hash", sha2563222222);
            Log.d("MVOTE", "Encrypted Vote Hash: " + sha25622222222);
            return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
        } catch (IllegalBlockSizeException e17) {
            e3 = e17;
            str3 = "";
            e3.printStackTrace();
            PublicKey extractPublicKeyFromCertificate22222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString32222222 = Base64.encodeToString(vek, 2);
            String encodeToString222222222 = Base64.encodeToString(iv, 2);
            Arrays.toString(extractPublicKeyFromCertificate22222222.getEncoded());
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString32222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString222222222);
            String encrypt22222222 = mvoteRSACrypto.encrypt(str3, encodeToString32222222, encodeToString222222222, extractPublicKeyFromCertificate22222222);
            String sha25632222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            String sha256222222222 = MvoteHash.getSHA256(str2);
            jSONObject.put("encrypted_vote", str3);
            jSONObject.put("encrypted_vek", encrypt22222222);
            jSONObject.put("encrypted_vote_hash", sha256222222222);
            jSONObject.put("key_hash", sha25632222222);
            Log.d("MVOTE", "Encrypted Vote Hash: " + sha256222222222);
            return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
        } catch (BadPaddingException e18) {
            e4 = e18;
            str3 = "";
            e4.printStackTrace();
            PublicKey extractPublicKeyFromCertificate222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString322222222 = Base64.encodeToString(vek, 2);
            String encodeToString2222222222 = Base64.encodeToString(iv, 2);
            Arrays.toString(extractPublicKeyFromCertificate222222222.getEncoded());
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString322222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2222222222);
            String encrypt222222222 = mvoteRSACrypto.encrypt(str3, encodeToString322222222, encodeToString2222222222, extractPublicKeyFromCertificate222222222);
            String sha256322222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            String sha2562222222222 = MvoteHash.getSHA256(str2);
            jSONObject.put("encrypted_vote", str3);
            jSONObject.put("encrypted_vek", encrypt222222222);
            jSONObject.put("encrypted_vote_hash", sha2562222222222);
            jSONObject.put("key_hash", sha256322222222);
            Log.d("MVOTE", "Encrypted Vote Hash: " + sha2562222222222);
            return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
        } catch (InvalidKeyException e19) {
            e5 = e19;
            str3 = "";
            e5.printStackTrace();
            PublicKey extractPublicKeyFromCertificate2222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString3222222222 = Base64.encodeToString(vek, 2);
            String encodeToString22222222222 = Base64.encodeToString(iv, 2);
            Arrays.toString(extractPublicKeyFromCertificate2222222222.getEncoded());
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3222222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22222222222);
            String encrypt2222222222 = mvoteRSACrypto.encrypt(str3, encodeToString3222222222, encodeToString22222222222, extractPublicKeyFromCertificate2222222222);
            String sha2563222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            String sha25622222222222 = MvoteHash.getSHA256(str2);
            jSONObject.put("encrypted_vote", str3);
            jSONObject.put("encrypted_vek", encrypt2222222222);
            jSONObject.put("encrypted_vote_hash", sha25622222222222);
            jSONObject.put("key_hash", sha2563222222222);
            Log.d("MVOTE", "Encrypted Vote Hash: " + sha25622222222222);
            return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
        } catch (UnsupportedEncodingException e20) {
            e6 = e20;
            str3 = "";
            e6.printStackTrace();
            PublicKey extractPublicKeyFromCertificate22222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString32222222222 = Base64.encodeToString(vek, 2);
            String encodeToString222222222222 = Base64.encodeToString(iv, 2);
            Arrays.toString(extractPublicKeyFromCertificate22222222222.getEncoded());
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString32222222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString222222222222);
            String encrypt22222222222 = mvoteRSACrypto.encrypt(str3, encodeToString32222222222, encodeToString222222222222, extractPublicKeyFromCertificate22222222222);
            String sha25632222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            String sha256222222222222 = MvoteHash.getSHA256(str2);
            jSONObject.put("encrypted_vote", str3);
            jSONObject.put("encrypted_vek", encrypt22222222222);
            jSONObject.put("encrypted_vote_hash", sha256222222222222);
            jSONObject.put("key_hash", sha25632222222222);
            Log.d("MVOTE", "Encrypted Vote Hash: " + sha256222222222222);
            return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
        } catch (InvalidAlgorithmParameterException e21) {
            e7 = e21;
            str3 = "";
            e7.printStackTrace();
            PublicKey extractPublicKeyFromCertificate222222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString322222222222 = Base64.encodeToString(vek, 2);
            String encodeToString2222222222222 = Base64.encodeToString(iv, 2);
            Arrays.toString(extractPublicKeyFromCertificate222222222222.getEncoded());
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString322222222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2222222222222);
            String encrypt222222222222 = mvoteRSACrypto.encrypt(str3, encodeToString322222222222, encodeToString2222222222222, extractPublicKeyFromCertificate222222222222);
            String sha256322222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            String sha2562222222222222 = MvoteHash.getSHA256(str2);
            jSONObject.put("encrypted_vote", str3);
            jSONObject.put("encrypted_vek", encrypt222222222222);
            jSONObject.put("encrypted_vote_hash", sha2562222222222222);
            jSONObject.put("key_hash", sha256322222222222);
            Log.d("MVOTE", "Encrypted Vote Hash: " + sha2562222222222222);
            return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
        }
        try {
            PublicKey extractPublicKeyFromCertificate2222222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString3222222222222 = Base64.encodeToString(vek, 2);
            String encodeToString22222222222222 = Base64.encodeToString(iv, 2);
            Arrays.toString(extractPublicKeyFromCertificate2222222222222.getEncoded());
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3222222222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22222222222222);
            String encrypt2222222222222 = mvoteRSACrypto.encrypt(str3, encodeToString3222222222222, encodeToString22222222222222, extractPublicKeyFromCertificate2222222222222);
            String sha2563222222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            String sha25622222222222222 = MvoteHash.getSHA256(str2);
            jSONObject.put("encrypted_vote", str3);
            jSONObject.put("encrypted_vek", encrypt2222222222222);
            jSONObject.put("encrypted_vote_hash", sha25622222222222222);
            jSONObject.put("key_hash", sha2563222222222222);
            Log.d("MVOTE", "Encrypted Vote Hash: " + sha25622222222222222);
        } catch (NoSuchAlgorithmException e22) {
            e22.printStackTrace();
        } catch (NoSuchPaddingException e23) {
            e23.printStackTrace();
        } catch (InvalidKeyException e24) {
            e24.printStackTrace();
        } catch (IllegalBlockSizeException e25) {
            e25.printStackTrace();
        } catch (Exception e26) {
            e26.printStackTrace();
        }
        return jSONObject.toString().replace("\\n", "").replaceAll("\\\\", "").trim();
    }

    public String encryptVVPAT(String str, String str2) {
        String str3;
        NoSuchAlgorithmException e;
        NoSuchPaddingException e2;
        IllegalBlockSizeException e3;
        BadPaddingException e4;
        InvalidKeyException e5;
        UnsupportedEncodingException e6;
        InvalidAlgorithmParameterException e7;
        JSONObject jSONObject = new JSONObject();
        MvoteCrypto mvoteCrypto = new MvoteCrypto();
        MvoteRSACrypto mvoteRSACrypto = new MvoteRSACrypto();
        byte[] vek = mvoteCrypto.getVek();
        byte[] iv = mvoteCrypto.getIv();
        try {
            str3 = mvoteCrypto.encrypt(str2);
            try {
                Log.d("MVOTE", "Encrypted Vote: " + str3);
            } catch (NoSuchAlgorithmException e8) {
                e = e8;
            } catch (NoSuchPaddingException e9) {
                e2 = e9;
                e2.printStackTrace();
                PublicKey extractPublicKeyFromCertificate = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString = Base64.encodeToString(vek, 2);
                String encodeToString2 = Base64.encodeToString(iv, 2);
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2);
                String encrypt = mvoteRSACrypto.encrypt(str3, encodeToString, encodeToString2, extractPublicKeyFromCertificate);
                String sha256 = MvoteHash.getSHA256(str2);
                String sha2562 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                jSONObject.put("encrypted_vvpat", str3);
                jSONObject.put("encrypted_vvpat_vek", encrypt);
                jSONObject.put("key_hash_vvpat", sha2562);
                jSONObject.put("vvpat_hash", sha256);
                return jSONObject.toString().replace("\\n", "");
            } catch (IllegalBlockSizeException e10) {
                e3 = e10;
                e3.printStackTrace();
                PublicKey extractPublicKeyFromCertificate2 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString3 = Base64.encodeToString(vek, 2);
                String encodeToString22 = Base64.encodeToString(iv, 2);
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22);
                String encrypt2 = mvoteRSACrypto.encrypt(str3, encodeToString3, encodeToString22, extractPublicKeyFromCertificate2);
                String sha2563 = MvoteHash.getSHA256(str2);
                String sha25622 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                jSONObject.put("encrypted_vvpat", str3);
                jSONObject.put("encrypted_vvpat_vek", encrypt2);
                jSONObject.put("key_hash_vvpat", sha25622);
                jSONObject.put("vvpat_hash", sha2563);
                return jSONObject.toString().replace("\\n", "");
            } catch (BadPaddingException e11) {
                e4 = e11;
                e4.printStackTrace();
                PublicKey extractPublicKeyFromCertificate22 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString32 = Base64.encodeToString(vek, 2);
                String encodeToString222 = Base64.encodeToString(iv, 2);
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString32);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString222);
                String encrypt22 = mvoteRSACrypto.encrypt(str3, encodeToString32, encodeToString222, extractPublicKeyFromCertificate22);
                String sha25632 = MvoteHash.getSHA256(str2);
                String sha256222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                jSONObject.put("encrypted_vvpat", str3);
                jSONObject.put("encrypted_vvpat_vek", encrypt22);
                jSONObject.put("key_hash_vvpat", sha256222);
                jSONObject.put("vvpat_hash", sha25632);
                return jSONObject.toString().replace("\\n", "");
            } catch (InvalidKeyException e12) {
                e5 = e12;
                e5.printStackTrace();
                PublicKey extractPublicKeyFromCertificate222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString322 = Base64.encodeToString(vek, 2);
                String encodeToString2222 = Base64.encodeToString(iv, 2);
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString322);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2222);
                String encrypt222 = mvoteRSACrypto.encrypt(str3, encodeToString322, encodeToString2222, extractPublicKeyFromCertificate222);
                String sha256322 = MvoteHash.getSHA256(str2);
                String sha2562222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                jSONObject.put("encrypted_vvpat", str3);
                jSONObject.put("encrypted_vvpat_vek", encrypt222);
                jSONObject.put("key_hash_vvpat", sha2562222);
                jSONObject.put("vvpat_hash", sha256322);
                return jSONObject.toString().replace("\\n", "");
            } catch (UnsupportedEncodingException e13) {
                e6 = e13;
                e6.printStackTrace();
                PublicKey extractPublicKeyFromCertificate2222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString3222 = Base64.encodeToString(vek, 2);
                String encodeToString22222 = Base64.encodeToString(iv, 2);
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3222);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22222);
                String encrypt2222 = mvoteRSACrypto.encrypt(str3, encodeToString3222, encodeToString22222, extractPublicKeyFromCertificate2222);
                String sha2563222 = MvoteHash.getSHA256(str2);
                String sha25622222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                jSONObject.put("encrypted_vvpat", str3);
                jSONObject.put("encrypted_vvpat_vek", encrypt2222);
                jSONObject.put("key_hash_vvpat", sha25622222);
                jSONObject.put("vvpat_hash", sha2563222);
                return jSONObject.toString().replace("\\n", "");
            } catch (InvalidAlgorithmParameterException e14) {
                e7 = e14;
                e7.printStackTrace();
                PublicKey extractPublicKeyFromCertificate22222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
                String encodeToString32222 = Base64.encodeToString(vek, 2);
                String encodeToString222222 = Base64.encodeToString(iv, 2);
                Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString32222);
                Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString222222);
                String encrypt22222 = mvoteRSACrypto.encrypt(str3, encodeToString32222, encodeToString222222, extractPublicKeyFromCertificate22222);
                String sha25632222 = MvoteHash.getSHA256(str2);
                String sha256222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
                jSONObject.put("encrypted_vvpat", str3);
                jSONObject.put("encrypted_vvpat_vek", encrypt22222);
                jSONObject.put("key_hash_vvpat", sha256222222);
                jSONObject.put("vvpat_hash", sha25632222);
                return jSONObject.toString().replace("\\n", "");
            }
        } catch (NoSuchAlgorithmException e15) {
            e = e15;
            str3 = "";
            e.printStackTrace();
            PublicKey extractPublicKeyFromCertificate222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString322222 = Base64.encodeToString(vek, 2);
            String encodeToString2222222 = Base64.encodeToString(iv, 2);
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString322222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2222222);
            String encrypt222222 = mvoteRSACrypto.encrypt(str3, encodeToString322222, encodeToString2222222, extractPublicKeyFromCertificate222222);
            String sha256322222 = MvoteHash.getSHA256(str2);
            String sha2562222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            jSONObject.put("encrypted_vvpat", str3);
            jSONObject.put("encrypted_vvpat_vek", encrypt222222);
            jSONObject.put("key_hash_vvpat", sha2562222222);
            jSONObject.put("vvpat_hash", sha256322222);
            return jSONObject.toString().replace("\\n", "");
        } catch (NoSuchPaddingException e16) {
            e2 = e16;
            str3 = "";
            e2.printStackTrace();
            PublicKey extractPublicKeyFromCertificate2222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString3222222 = Base64.encodeToString(vek, 2);
            String encodeToString22222222 = Base64.encodeToString(iv, 2);
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22222222);
            String encrypt2222222 = mvoteRSACrypto.encrypt(str3, encodeToString3222222, encodeToString22222222, extractPublicKeyFromCertificate2222222);
            String sha2563222222 = MvoteHash.getSHA256(str2);
            String sha25622222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            jSONObject.put("encrypted_vvpat", str3);
            jSONObject.put("encrypted_vvpat_vek", encrypt2222222);
            jSONObject.put("key_hash_vvpat", sha25622222222);
            jSONObject.put("vvpat_hash", sha2563222222);
            return jSONObject.toString().replace("\\n", "");
        } catch (IllegalBlockSizeException e17) {
            e3 = e17;
            str3 = "";
            e3.printStackTrace();
            PublicKey extractPublicKeyFromCertificate22222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString32222222 = Base64.encodeToString(vek, 2);
            String encodeToString222222222 = Base64.encodeToString(iv, 2);
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString32222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString222222222);
            String encrypt22222222 = mvoteRSACrypto.encrypt(str3, encodeToString32222222, encodeToString222222222, extractPublicKeyFromCertificate22222222);
            String sha25632222222 = MvoteHash.getSHA256(str2);
            String sha256222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            jSONObject.put("encrypted_vvpat", str3);
            jSONObject.put("encrypted_vvpat_vek", encrypt22222222);
            jSONObject.put("key_hash_vvpat", sha256222222222);
            jSONObject.put("vvpat_hash", sha25632222222);
            return jSONObject.toString().replace("\\n", "");
        } catch (BadPaddingException e18) {
            e4 = e18;
            str3 = "";
            e4.printStackTrace();
            PublicKey extractPublicKeyFromCertificate222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString322222222 = Base64.encodeToString(vek, 2);
            String encodeToString2222222222 = Base64.encodeToString(iv, 2);
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString322222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2222222222);
            String encrypt222222222 = mvoteRSACrypto.encrypt(str3, encodeToString322222222, encodeToString2222222222, extractPublicKeyFromCertificate222222222);
            String sha256322222222 = MvoteHash.getSHA256(str2);
            String sha2562222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            jSONObject.put("encrypted_vvpat", str3);
            jSONObject.put("encrypted_vvpat_vek", encrypt222222222);
            jSONObject.put("key_hash_vvpat", sha2562222222222);
            jSONObject.put("vvpat_hash", sha256322222222);
            return jSONObject.toString().replace("\\n", "");
        } catch (InvalidKeyException e19) {
            e5 = e19;
            str3 = "";
            e5.printStackTrace();
            PublicKey extractPublicKeyFromCertificate2222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString3222222222 = Base64.encodeToString(vek, 2);
            String encodeToString22222222222 = Base64.encodeToString(iv, 2);
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3222222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22222222222);
            String encrypt2222222222 = mvoteRSACrypto.encrypt(str3, encodeToString3222222222, encodeToString22222222222, extractPublicKeyFromCertificate2222222222);
            String sha2563222222222 = MvoteHash.getSHA256(str2);
            String sha25622222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            jSONObject.put("encrypted_vvpat", str3);
            jSONObject.put("encrypted_vvpat_vek", encrypt2222222222);
            jSONObject.put("key_hash_vvpat", sha25622222222222);
            jSONObject.put("vvpat_hash", sha2563222222222);
            return jSONObject.toString().replace("\\n", "");
        } catch (UnsupportedEncodingException e20) {
            e6 = e20;
            str3 = "";
            e6.printStackTrace();
            PublicKey extractPublicKeyFromCertificate22222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString32222222222 = Base64.encodeToString(vek, 2);
            String encodeToString222222222222 = Base64.encodeToString(iv, 2);
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString32222222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString222222222222);
            String encrypt22222222222 = mvoteRSACrypto.encrypt(str3, encodeToString32222222222, encodeToString222222222222, extractPublicKeyFromCertificate22222222222);
            String sha25632222222222 = MvoteHash.getSHA256(str2);
            String sha256222222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            jSONObject.put("encrypted_vvpat", str3);
            jSONObject.put("encrypted_vvpat_vek", encrypt22222222222);
            jSONObject.put("key_hash_vvpat", sha256222222222222);
            jSONObject.put("vvpat_hash", sha25632222222222);
            return jSONObject.toString().replace("\\n", "");
        } catch (InvalidAlgorithmParameterException e21) {
            e7 = e21;
            str3 = "";
            e7.printStackTrace();
            PublicKey extractPublicKeyFromCertificate222222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString322222222222 = Base64.encodeToString(vek, 2);
            String encodeToString2222222222222 = Base64.encodeToString(iv, 2);
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString322222222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString2222222222222);
            String encrypt222222222222 = mvoteRSACrypto.encrypt(str3, encodeToString322222222222, encodeToString2222222222222, extractPublicKeyFromCertificate222222222222);
            String sha256322222222222 = MvoteHash.getSHA256(str2);
            String sha2562222222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            jSONObject.put("encrypted_vvpat", str3);
            jSONObject.put("encrypted_vvpat_vek", encrypt222222222222);
            jSONObject.put("key_hash_vvpat", sha2562222222222222);
            jSONObject.put("vvpat_hash", sha256322222222222);
            return jSONObject.toString().replace("\\n", "");
        }
        try {
            PublicKey extractPublicKeyFromCertificate2222222222222 = mvoteRSACrypto.extractPublicKeyFromCertificate(str);
            String encodeToString3222222222222 = Base64.encodeToString(vek, 2);
            String encodeToString22222222222222 = Base64.encodeToString(iv, 2);
            Log.d("MVOTE", "Enc: VEK: Base64: " + encodeToString3222222222222);
            Log.d("MVOTE", "Enc: IV: Base64: " + encodeToString22222222222222);
            String encrypt2222222222222 = mvoteRSACrypto.encrypt(str3, encodeToString3222222222222, encodeToString22222222222222, extractPublicKeyFromCertificate2222222222222);
            String sha2563222222222222 = MvoteHash.getSHA256(str2);
            String sha25622222222222222 = MvoteHash.getSHA256(SingleTone.getInstance().getKEY_HASH());
            jSONObject.put("encrypted_vvpat", str3);
            jSONObject.put("encrypted_vvpat_vek", encrypt2222222222222);
            jSONObject.put("key_hash_vvpat", sha25622222222222222);
            jSONObject.put("vvpat_hash", sha2563222222222222);
        } catch (NoSuchAlgorithmException e22) {
            e22.printStackTrace();
        } catch (NoSuchPaddingException e23) {
            e23.printStackTrace();
        } catch (InvalidKeyException e24) {
            e24.printStackTrace();
        } catch (IllegalBlockSizeException e25) {
            e25.printStackTrace();
        } catch (Exception e26) {
            e26.printStackTrace();
        }
        return jSONObject.toString().replace("\\n", "");
    }
}
