package com.mvote.helloworld;

import android.util.Base64;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MvoteCrypto {
    private byte[] iv;
    private byte[] vek;

    public MvoteCrypto() {
        try {
            SecureRandom secureRandom = new SecureRandom();
            byte[] bArr = new byte[32];
            this.vek = bArr;
            secureRandom.nextBytes(bArr);
            SecureRandom secureRandom2 = new SecureRandom();
            byte[] bArr2 = new byte[16];
            this.iv = bArr2;
            secureRandom2.nextBytes(bArr2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String encrypt(String str) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        byte[] bytes = str.getBytes("UTF8");
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.vek, "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(this.iv);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS7Padding");
        instance.init(1, secretKeySpec, ivParameterSpec);
        return Base64.encodeToString(instance.doFinal(bytes), 2);
    }

    public String decrypt(String str) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.vek, "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(this.iv);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS7Padding");
        instance.init(2, secretKeySpec, ivParameterSpec);
        return new String(instance.doFinal(Base64.decode(str, 2)), "UTF8");
    }

    public String decrypt(String str, byte[] bArr, byte[] bArr2) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr2);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS7Padding");
        instance.init(2, secretKeySpec, ivParameterSpec);
        return new String(instance.doFinal(Base64.decode(str, 2)), "UTF8");
    }

    public byte[] getVek() {
        return this.vek;
    }

    public void setVek(byte[] bArr) {
        this.vek = bArr;
    }

    public byte[] getIv() {
        return this.iv;
    }

    public void setIv(byte[] bArr) {
        this.iv = bArr;
    }
}
