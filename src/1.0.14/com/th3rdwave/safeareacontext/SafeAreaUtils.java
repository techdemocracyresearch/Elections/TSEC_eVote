package com.th3rdwave.safeareacontext;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;

class SafeAreaUtils {
    SafeAreaUtils() {
    }

    private static EdgeInsets getRootWindowInsetsCompat(View view) {
        if (Build.VERSION.SDK_INT >= 23) {
            WindowInsets rootWindowInsets = view.getRootWindowInsets();
            if (rootWindowInsets == null) {
                return null;
            }
            return new EdgeInsets((float) rootWindowInsets.getSystemWindowInsetTop(), (float) rootWindowInsets.getSystemWindowInsetRight(), (float) Math.min(rootWindowInsets.getSystemWindowInsetBottom(), rootWindowInsets.getStableInsetBottom()), (float) rootWindowInsets.getSystemWindowInsetLeft());
        }
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        return new EdgeInsets((float) rect.top, (float) (view.getWidth() - rect.right), (float) (view.getHeight() - rect.bottom), (float) rect.left);
    }

    static EdgeInsets getSafeAreaInsets(View view) {
        View rootView;
        EdgeInsets rootWindowInsetsCompat;
        if (view.getHeight() == 0 || (rootWindowInsetsCompat = getRootWindowInsetsCompat((rootView = view.getRootView()))) == null) {
            return null;
        }
        Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        rootWindowInsetsCompat.top = Math.max(rootWindowInsetsCompat.top - ((float) rect.top), 0.0f);
        rootWindowInsetsCompat.left = Math.max(rootWindowInsetsCompat.left - ((float) rect.left), 0.0f);
        rootWindowInsetsCompat.bottom = Math.max(Math.min(((float) (rect.top + view.getHeight())) - ((float) rootView.getHeight()), 0.0f) + rootWindowInsetsCompat.bottom, 0.0f);
        rootWindowInsetsCompat.right = Math.max(Math.min(((float) (rect.left + view.getWidth())) - ((float) rootView.getWidth()), 0.0f) + rootWindowInsetsCompat.right, 0.0f);
        return rootWindowInsetsCompat;
    }

    static Rect getFrame(ViewGroup viewGroup, View view) {
        if (view.getParent() == null) {
            return null;
        }
        Rect rect = new Rect();
        view.getDrawingRect(rect);
        try {
            viewGroup.offsetDescendantRectToMyCoords(view, rect);
            return new Rect((float) rect.left, (float) rect.top, (float) view.getWidth(), (float) view.getHeight());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        }
    }
}
