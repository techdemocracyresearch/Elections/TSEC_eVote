package com.rnfs;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class Downloader extends AsyncTask<DownloadParams, long[], DownloadResult> {
    private AtomicBoolean mAbort = new AtomicBoolean(false);
    private DownloadParams mParam;
    DownloadResult res;

    /* access modifiers changed from: protected */
    public void onPostExecute(Exception exc) {
    }

    /* access modifiers changed from: protected */
    public DownloadResult doInBackground(DownloadParams... downloadParamsArr) {
        this.mParam = downloadParamsArr[0];
        this.res = new DownloadResult();
        new Thread(new Runnable() {
            /* class com.rnfs.Downloader.AnonymousClass1 */

            public void run() {
                try {
                    Downloader downloader = Downloader.this;
                    downloader.download(downloader.mParam, Downloader.this.res);
                    Downloader.this.mParam.onTaskCompleted.onTaskCompleted(Downloader.this.res);
                } catch (Exception e) {
                    Downloader.this.res.exception = e;
                    Downloader.this.mParam.onTaskCompleted.onTaskCompleted(Downloader.this.res);
                }
            }
        }).start();
        return this.res;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:57:0x0109 */
    /* JADX WARN: Type inference failed for: r10v10, types: [int] */
    /* JADX WARN: Type inference failed for: r10v16 */
    /* JADX WARN: Type inference failed for: r10v18 */
    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0221  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0226  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x022b  */
    /* JADX WARNING: Unknown variable types count: 1 */
    private void download(DownloadParams downloadParams, DownloadResult downloadResult) throws Exception {
        BufferedInputStream bufferedInputStream;
        HttpURLConnection httpURLConnection;
        Throwable th;
        int i;
        BufferedInputStream bufferedInputStream2;
        FileOutputStream fileOutputStream;
        int i2;
        HttpURLConnection httpURLConnection2;
        int i3;
        int i4;
        int i5;
        DownloadParams downloadParams2 = downloadParams;
        FileOutputStream fileOutputStream2 = null;
        try {
            HttpURLConnection httpURLConnection3 = (HttpURLConnection) downloadParams2.src.openConnection();
            try {
                ReadableMapKeySetIterator keySetIterator = downloadParams2.headers.keySetIterator();
                while (keySetIterator.hasNextKey()) {
                    String nextKey = keySetIterator.nextKey();
                    httpURLConnection3.setRequestProperty(nextKey, downloadParams2.headers.getString(nextKey));
                }
                httpURLConnection3.setConnectTimeout(downloadParams2.connectionTimeout);
                httpURLConnection3.setReadTimeout(downloadParams2.readTimeout);
                httpURLConnection3.connect();
                int responseCode = httpURLConnection3.getResponseCode();
                long contentLength = getContentLength(httpURLConnection3);
                if (responseCode != 200 && (responseCode == 301 || responseCode == 302 || responseCode == 307 || responseCode == 308)) {
                    String headerField = httpURLConnection3.getHeaderField("Location");
                    httpURLConnection3.disconnect();
                    HttpURLConnection httpURLConnection4 = (HttpURLConnection) new URL(headerField).openConnection();
                    try {
                        httpURLConnection4.setConnectTimeout(5000);
                        httpURLConnection4.connect();
                        int responseCode2 = httpURLConnection4.getResponseCode();
                        contentLength = getContentLength(httpURLConnection4);
                        responseCode = responseCode2;
                        httpURLConnection3 = httpURLConnection4;
                    } catch (Throwable th2) {
                        th = th2;
                        bufferedInputStream = null;
                        httpURLConnection = httpURLConnection4;
                        if (fileOutputStream2 != null) {
                        }
                        if (bufferedInputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                }
                if (responseCode < 200 || responseCode >= 300) {
                    httpURLConnection = httpURLConnection3;
                    i = responseCode;
                    bufferedInputStream = null;
                } else {
                    try {
                        Map<String, List<String>> headerFields = httpURLConnection3.getHeaderFields();
                        HashMap hashMap = new HashMap();
                        for (Map.Entry<String, List<String>> entry : headerFields.entrySet()) {
                            String key = entry.getKey();
                            String str = entry.getValue().get(0);
                            if (!(key == null || str == null)) {
                                hashMap.put(key, str);
                            }
                        }
                        if (this.mParam.onDownloadBegin != null) {
                            this.mParam.onDownloadBegin.onDownloadBegin(responseCode, contentLength, hashMap);
                        }
                        bufferedInputStream2 = new BufferedInputStream(httpURLConnection3.getInputStream(), 8192);
                    } catch (Throwable th3) {
                        th = th3;
                        httpURLConnection = httpURLConnection3;
                        bufferedInputStream = null;
                        if (fileOutputStream2 != null) {
                        }
                        if (bufferedInputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                    try {
                        fileOutputStream = new FileOutputStream(downloadParams2.dest);
                    } catch (Throwable th4) {
                        th = th4;
                        httpURLConnection = httpURLConnection3;
                        bufferedInputStream = bufferedInputStream2;
                        if (fileOutputStream2 != null) {
                            fileOutputStream2.close();
                        }
                        if (bufferedInputStream != null) {
                            bufferedInputStream.close();
                        }
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        throw th;
                    }
                    try {
                        byte[] bArr = new byte[8192];
                        boolean z = this.mParam.onDownloadProgress != null;
                        long j = 0;
                        long j2 = 0;
                        double d = 0.0d;
                        while (true) {
                            ?? read = bufferedInputStream2.read(bArr);
                            if (read == -1) {
                                httpURLConnection = httpURLConnection3;
                                i = responseCode;
                                bufferedInputStream = bufferedInputStream2;
                                fileOutputStream.flush();
                                downloadResult.bytesWritten = j2;
                                fileOutputStream2 = fileOutputStream;
                                break;
                            } else if (!this.mAbort.get()) {
                                bufferedInputStream = bufferedInputStream2;
                                j2 += (long) read;
                                if (z) {
                                    try {
                                        if (downloadParams2.progressInterval > 0) {
                                            long currentTimeMillis = System.currentTimeMillis();
                                            if (currentTimeMillis - j > ((long) downloadParams2.progressInterval)) {
                                                publishProgress(new long[]{contentLength, j2});
                                                j = currentTimeMillis;
                                            }
                                            httpURLConnection2 = httpURLConnection3;
                                            i2 = responseCode;
                                            i4 = read;
                                            i3 = 0;
                                            fileOutputStream.write(bArr, i3, i4);
                                            downloadParams2 = downloadParams;
                                            httpURLConnection3 = httpURLConnection2;
                                            bufferedInputStream2 = bufferedInputStream;
                                            responseCode = i2;
                                        } else {
                                            i5 = read;
                                            if (downloadParams2.progressDivider <= 0.0f) {
                                                publishProgress(new long[]{contentLength, j2});
                                                httpURLConnection2 = httpURLConnection3;
                                                i2 = responseCode;
                                            } else {
                                                read = httpURLConnection3;
                                                i2 = responseCode;
                                                double round = (double) Math.round((((double) j2) * 100.0d) / ((double) contentLength));
                                                if (round % ((double) downloadParams2.progressDivider) != 0.0d || (round == d && j2 != contentLength)) {
                                                    httpURLConnection2 = read;
                                                } else {
                                                    Log.d("Downloader", "EMIT: " + String.valueOf(round) + ", TOTAL:" + String.valueOf(j2));
                                                    publishProgress(new long[]{contentLength, j2});
                                                    d = round;
                                                    httpURLConnection2 = read;
                                                }
                                            }
                                        }
                                    } catch (Throwable th5) {
                                        th = th5;
                                        httpURLConnection = read;
                                        fileOutputStream2 = fileOutputStream;
                                        if (fileOutputStream2 != null) {
                                        }
                                        if (bufferedInputStream != null) {
                                        }
                                        if (httpURLConnection != null) {
                                        }
                                        throw th;
                                    }
                                } else {
                                    i2 = responseCode;
                                    i5 = read;
                                    httpURLConnection2 = httpURLConnection3;
                                }
                                i4 = i5;
                                i3 = 0;
                                fileOutputStream.write(bArr, i3, i4);
                                downloadParams2 = downloadParams;
                                httpURLConnection3 = httpURLConnection2;
                                bufferedInputStream2 = bufferedInputStream;
                                responseCode = i2;
                            } else {
                                throw new Exception("Download has been aborted");
                            }
                        }
                    } catch (Throwable th6) {
                        th = th6;
                        httpURLConnection = httpURLConnection3;
                        bufferedInputStream = bufferedInputStream2;
                        fileOutputStream2 = fileOutputStream;
                        if (fileOutputStream2 != null) {
                        }
                        if (bufferedInputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                }
            } catch (Throwable th7) {
                th = th7;
                bufferedInputStream = null;
                httpURLConnection = httpURLConnection3;
                if (fileOutputStream2 != null) {
                }
                if (bufferedInputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                throw th;
            }
            try {
                downloadResult.statusCode = i;
                if (fileOutputStream2 != null) {
                    fileOutputStream2.close();
                }
                if (bufferedInputStream != null) {
                    bufferedInputStream.close();
                }
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            } catch (Throwable th8) {
                th = th8;
                if (fileOutputStream2 != null) {
                }
                if (bufferedInputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                throw th;
            }
        } catch (Throwable th9) {
            th = th9;
            httpURLConnection = null;
            bufferedInputStream = null;
            if (fileOutputStream2 != null) {
            }
            if (bufferedInputStream != null) {
            }
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }

    private long getContentLength(HttpURLConnection httpURLConnection) {
        if (Build.VERSION.SDK_INT >= 24) {
            return httpURLConnection.getContentLengthLong();
        }
        return (long) httpURLConnection.getContentLength();
    }

    /* access modifiers changed from: protected */
    public void stop() {
        this.mAbort.set(true);
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(long[]... jArr) {
        super.onProgressUpdate((Object[]) jArr);
        if (this.mParam.onDownloadProgress != null) {
            this.mParam.onDownloadProgress.onDownloadProgress(jArr[0][0], jArr[0][1]);
        }
    }
}
