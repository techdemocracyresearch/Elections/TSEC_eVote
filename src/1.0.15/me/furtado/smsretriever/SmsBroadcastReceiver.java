package me.furtado.smsretriever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.Status;

public final class SmsBroadcastReceiver extends BroadcastReceiver {
    private static final String EXTRAS_KEY = "extras";
    private static final String EXTRAS_NULL_ERROR_MESSAGE = "Extras is null.";
    private static final String MESSAGE_KEY = "message";
    private static final String SMS_EVENT = "me.furtado.smsretriever:SmsEvent";
    private static final String STATUS_KEY = "status";
    private static final String STATUS_NULL_ERROR_MESSAGE = "Status is null.";
    private static final String TIMEOUT_ERROR_MESSAGE = "Timeout error.";
    private static final String TIMEOUT_KEY = "timeout";
    private ReactApplicationContext mContext;

    public SmsBroadcastReceiver(ReactApplicationContext reactApplicationContext) {
        this.mContext = reactApplicationContext;
    }

    public void onReceive(Context context, Intent intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            if (extras == null) {
                emitJSEvent(EXTRAS_KEY, EXTRAS_NULL_ERROR_MESSAGE);
                return;
            }
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
            if (status == null) {
                emitJSEvent("status", STATUS_NULL_ERROR_MESSAGE);
                return;
            }
            int statusCode = status.getStatusCode();
            if (statusCode == 0) {
                emitJSEvent(MESSAGE_KEY, (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE));
            } else if (statusCode == 15) {
                emitJSEvent(TIMEOUT_KEY, TIMEOUT_ERROR_MESSAGE);
            }
        }
    }

    private void emitJSEvent(String str, String str2) {
        ReactApplicationContext reactApplicationContext = this.mContext;
        if (reactApplicationContext != null && reactApplicationContext.hasActiveCatalystInstance()) {
            WritableNativeMap writableNativeMap = new WritableNativeMap();
            writableNativeMap.putString(str, str2);
            ((DeviceEventManagerModule.RCTDeviceEventEmitter) this.mContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)).emit(SMS_EVENT, writableNativeMap);
        }
    }
}
