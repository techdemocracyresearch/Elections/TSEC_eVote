package ru.lazard.tamperingprotection;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.text.TextUtils;
import androidx.core.os.EnvironmentCompat;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipFile;
import kotlin.UByte;

public class TamperingProtection {
    public static final String AMAZON_APP_STORE_PACKAGE = "com.amazon.venezia";
    public static final String GOOGLE_PLAY_STORE_PACKAGE = "com.android.vending";
    public static final String SAMSUNG_APP_STORE_PACKAGE = "com.sec.android.app.samsungapps";
    private final Context context;
    private long[] dexCrcs = new long[0];
    private boolean isDebugAvailable = true;
    private boolean isEmulatorAvailable = true;
    private List<String> packageNames = Arrays.asList(new String[0]);
    private List<String> signatures = Arrays.asList(new String[0]);
    private List<String> stores = Arrays.asList(new String[0]);

    public TamperingProtection(Context context2) {
        this.context = context2;
    }

    public static long getDexCRC(Context context2) throws IOException {
        return new ZipFile(context2.getPackageCodePath()).getEntry("classes.dex").getCrc();
    }

    public static String[] getSignatures(Context context2) throws PackageManager.NameNotFoundException, NoSuchAlgorithmException {
        PackageInfo packageInfo = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 64);
        if (packageInfo.signatures == null || packageInfo.signatures.length <= 0) {
            return new String[0];
        }
        String[] strArr = new String[packageInfo.signatures.length];
        for (int i = 0; i < packageInfo.signatures.length; i++) {
            Signature signature = packageInfo.signatures[i];
            if (signature != null) {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(signature.toByteArray());
                byte[] digest = instance.digest();
                char[] charArray = "0123456789ABCDEF".toCharArray();
                String str = "";
                for (byte b : digest) {
                    int i2 = b & UByte.MAX_VALUE;
                    str = str + "" + charArray[i2 >> 4] + charArray[i2 & 15] + ":";
                }
                if (str.length() > 0) {
                    str = str.substring(0, str.length() - 1);
                }
                strArr[i] = str;
            }
        }
        return strArr;
    }

    public static boolean isEmulator() {
        int i = (Build.PRODUCT.equals("sdk") || Build.PRODUCT.equals("google_sdk") || Build.PRODUCT.equals("sdk_x86") || Build.PRODUCT.equals("vbox86p")) ? 1 : 0;
        if (Build.MANUFACTURER.equals(EnvironmentCompat.MEDIA_UNKNOWN) || Build.MANUFACTURER.equals("Genymotion")) {
            i++;
        }
        if (Build.BRAND.equals("generic") || Build.BRAND.equals("generic_x86")) {
            i++;
        }
        if (Build.DEVICE.equals("generic") || Build.DEVICE.equals("generic_x86") || Build.DEVICE.equals("vbox86p")) {
            i++;
        }
        if (Build.MODEL.equals("sdk") || Build.MODEL.equals("google_sdk") || Build.MODEL.equals("Android SDK built for x86")) {
            i++;
        }
        if (Build.HARDWARE.equals("goldfish") || Build.HARDWARE.equals("vbox86")) {
            i++;
        }
        if (Build.FINGERPRINT.contains("generic/sdk/generic") || Build.FINGERPRINT.contains("generic_x86/sdk_x86/generic_x86") || Build.FINGERPRINT.contains("generic/google_sdk/generic") || Build.FINGERPRINT.contains("generic/vbox86p/vbox86p")) {
            i++;
        }
        if (i > 4) {
            return true;
        }
        return false;
    }

    public static boolean isDebug(Context context2) {
        return (context2.getApplicationInfo().flags & 2) != 0;
    }

    public static String getCurrentStore(Context context2) {
        return context2.getPackageManager().getInstallerPackageName(context2.getPackageName());
    }

    public static String getPackageName(Context context2) {
        return context2.getApplicationContext().getPackageName();
    }

    public void setAcceptedStores(String... strArr) {
        this.stores = Arrays.asList(strArr);
    }

    public void setAcceptedPackageNames(String... strArr) {
        this.packageNames = Arrays.asList(strArr);
    }

    public void setAcceptedSignatures(String... strArr) {
        this.signatures = Arrays.asList(strArr);
    }

    public void setAcceptStartOnEmulator(boolean z) {
        this.isEmulatorAvailable = z;
    }

    public void setAcceptStartInDebugMode(boolean z) {
        this.isDebugAvailable = z;
    }

    public void setAcceptedDexCrcs(long... jArr) {
        this.dexCrcs = jArr;
    }

    public boolean validateAll() {
        try {
            validateAllOrThrowException();
            return true;
        } catch (ValidationException unused) {
            return false;
        }
    }

    public void validateAllOrThrowException() throws ValidationException {
        validateDebugMode();
        validateEmulator();
        validatePackage();
        validateStore();
        validateSignature();
        validateDexCRC();
    }

    private void validateDebugMode() throws ValidationException {
        if (!this.isDebugAvailable && isDebug(this.context)) {
            throw new ValidationException(2, "Run in debug mode checked by ApplicationInfo. Flags=" + this.context.getApplicationInfo().flags);
        }
    }

    private void validateEmulator() throws ValidationException {
        if (!this.isEmulatorAvailable && isEmulator()) {
            throw new ValidationException(3, "Device looks like emulator.\nBuild.PRODUCT: " + Build.PRODUCT + "\nBuild.MANUFACTURER: " + Build.MANUFACTURER + "\nBuild.BRAND: " + Build.BRAND + "\nBuild.DEVICE: " + Build.DEVICE + "\nBuild.MODEL: " + Build.MODEL + "\nBuild.HARDWARE: " + Build.HARDWARE + "\nBuild.FINGERPRINT: " + Build.FINGERPRINT);
        }
    }

    private void validatePackage() throws ValidationException {
        List<String> list = this.packageNames;
        if (list != null && list.size() > 0) {
            String packageName = getPackageName(this.context);
            if (!TextUtils.isEmpty(packageName)) {
                for (String str : this.packageNames) {
                    if (packageName.equalsIgnoreCase(str)) {
                        return;
                    }
                }
                throw new ValidationException(5, "Not valid package name:  CurrentPackageName=\"" + packageName + "\";  validPackageNames=" + this.packageNames.toString() + ";");
            }
            throw new ValidationException(4, "Current package name is empty: packageName=\"" + packageName + "\";");
        }
    }

    private void validateStore() throws ValidationException {
        List<String> list = this.stores;
        if (list != null && list.size() > 0) {
            String currentStore = getCurrentStore(this.context);
            if (!TextUtils.isEmpty(currentStore)) {
                for (String str : this.stores) {
                    if (currentStore.equalsIgnoreCase(str)) {
                        return;
                    }
                }
                throw new ValidationException(7, "Not valid store:  CurrentStore=\"" + currentStore + "\";  validStores=" + this.stores.toString() + ";");
            }
            throw new ValidationException(6, "Current store is empty: store=\"" + currentStore + "\"; App installed by user (not by store).");
        }
    }

    private void validateDexCRC() throws ValidationException {
        long[] jArr = this.dexCrcs;
        if (jArr != null && jArr.length > 0) {
            try {
                long dexCRC = getDexCRC(this.context);
                for (long j : this.dexCrcs) {
                    if (j == dexCRC) {
                        return;
                    }
                }
                throw new ValidationException(12, "Crc code of .dex not valid. CurrentDexCrc=" + dexCRC + "  acceptedDexCrcs=" + Arrays.toString(this.dexCrcs) + ";");
            } catch (IOException e) {
                e.printStackTrace();
                throw new ValidationException(13, "Exception on .dex CNC validation.", e);
            }
        }
    }

    private void validateSignature() throws ValidationException {
        List<String> list = this.signatures;
        if (list != null && list.size() > 0) {
            try {
                String[] signatures2 = getSignatures(this.context);
                if (signatures2 == null || signatures2.length <= 0) {
                    throw new ValidationException(8, "No signatures found.");
                }
                for (String str : signatures2) {
                    for (String str2 : this.signatures) {
                        if (str.equalsIgnoreCase(str2)) {
                            return;
                        }
                    }
                }
                throw new ValidationException(10, "Not valid signature: CurrentSignatures=" + signatures2 + ";  validSignatures=" + this.signatures.toString() + ";");
            } catch (PackageManager.NameNotFoundException e) {
                throw new ValidationException(11, "Exception on signature validation.", e);
            } catch (NoSuchAlgorithmException e2) {
                throw new ValidationException(11, "Exception on signature validation.", e2);
            }
        }
    }

    public static final class ValidationException extends Exception {
        public static final int ERROR_CODE_CRC_NOT_VALID = 12;
        public static final int ERROR_CODE_CRC_UNKNOWN_EXCEPTION = 13;
        public static final int ERROR_CODE_DEBUG_MODE = 2;
        public static final int ERROR_CODE_PACKAGE_NAME_IS_EMPTY = 4;
        public static final int ERROR_CODE_PACKAGE_NAME_NOT_VALID = 5;
        public static final int ERROR_CODE_RUN_ON_EMULATOR = 3;
        public static final int ERROR_CODE_SIGNATURE_IS_EMPTY = 8;
        public static final int ERROR_CODE_SIGNATURE_MULTIPLE = 9;
        public static final int ERROR_CODE_SIGNATURE_NOT_VALID = 10;
        public static final int ERROR_CODE_SIGNATURE_UNKNOWN_EXCEPTION = 11;
        public static final int ERROR_CODE_STORE_IS_EMPTY = 6;
        public static final int ERROR_CODE_STORE_NOT_VALID = 7;
        public static final int ERROR_CODE_UNKNOWN_EXCEPTION = 1;
        private final int code;

        public ValidationException(int i, String str) {
            super(str);
            this.code = i;
        }

        public ValidationException(int i, String str, Throwable th) {
            super(str, th);
            this.code = i;
        }

        public int getErrorCode() {
            return this.code;
        }
    }
}
