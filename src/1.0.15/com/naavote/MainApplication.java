package com.naavote;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import com.mvote.helloworld.HelloWorldPackage;
import com.mvote.helloworld.SingleTone;
import java.util.ArrayList;
import java.util.List;
import ru.lazard.tamperingprotection.TamperingProtection;

public class MainApplication extends Application implements ReactApplication {
    public static String ANDROID_ID = "";
    private static Activity mCurrentActivity;
    private Context context;
    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        /* class com.naavote.MainApplication.AnonymousClass1 */

        /* access modifiers changed from: protected */
        @Override // com.facebook.react.ReactNativeHost
        public String getJSMainModuleName() {
            return "index";
        }

        @Override // com.facebook.react.ReactNativeHost
        public boolean getUseDeveloperSupport() {
            return false;
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.react.ReactNativeHost
        public List<ReactPackage> getPackages() {
            ArrayList<ReactPackage> packages = new PackageList(this).getPackages();
            packages.add(new HelloWorldPackage());
            return packages;
        }
    };

    private static void initializeFlipper(Context context2, ReactInstanceManager reactInstanceManager) {
    }

    @Override // com.facebook.react.ReactApplication
    public ReactNativeHost getReactNativeHost() {
        return this.mReactNativeHost;
    }

    public void onCreate() {
        super.onCreate();
        this.context = getApplicationContext();
        temperCheck();
        setupActivityListener();
        SingleTone.getInstance().setAndroidId(Settings.Secure.getString(getContentResolver(), "android_id"));
        SoLoader.init((Context) this, false);
        initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
    }

    private void temperCheck() {
        TamperingProtection tamperingProtection = new TamperingProtection(this);
        tamperingProtection.setAcceptedDexCrcs(new long[0]);
        tamperingProtection.setAcceptedStores(new String[0]);
        tamperingProtection.setAcceptedPackageNames(BuildConfig.APPLICATION_ID);
        tamperingProtection.setAcceptedSignatures("9F:0F:0E:92:DC:0E:CB:80:97:D3:13:20:04:CA:B2:7B");
        tamperingProtection.setAcceptStartOnEmulator(false);
        tamperingProtection.setAcceptStartInDebugMode(false);
        final boolean validateAll = tamperingProtection.validateAll();
        registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            /* class com.naavote.MainApplication.AnonymousClass2 */

            public void onActivityDestroyed(Activity activity) {
            }

            public void onActivityPaused(Activity activity) {
            }

            public void onActivityResumed(Activity activity) {
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            }

            public void onActivityStarted(Activity activity) {
            }

            public void onActivityStopped(Activity activity) {
            }

            public void onActivityCreated(Activity activity, Bundle bundle) {
                if (!validateAll) {
                    System.out.println("Tampered APK");
                    activity.finishAndRemoveTask();
                }
            }
        });
    }

    private void setupActivityListener() {
        registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            /* class com.naavote.MainApplication.AnonymousClass3 */

            public void onActivityDestroyed(Activity activity) {
            }

            public void onActivityPaused(Activity activity) {
            }

            public void onActivityResumed(Activity activity) {
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            }

            public void onActivityStarted(Activity activity) {
            }

            public void onActivityStopped(Activity activity) {
            }

            public void onActivityCreated(Activity activity, Bundle bundle) {
                activity.getWindow().setFlags(8192, 8192);
            }
        });
    }
}
