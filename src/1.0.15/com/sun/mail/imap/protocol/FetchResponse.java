package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ParsingException;
import com.sun.mail.iap.Protocol;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import java.io.IOException;
import java.util.Vector;

public class FetchResponse extends IMAPResponse {
    private static final char[] HEADER = {'.', 'H', 'E', 'A', 'D', 'E', 'R'};
    private static final char[] TEXT = {'.', 'T', 'E', 'X', 'T'};
    private Item[] items;

    public FetchResponse(Protocol protocol) throws IOException, ProtocolException {
        super(protocol);
        parse();
    }

    public FetchResponse(IMAPResponse iMAPResponse) throws IOException, ProtocolException {
        super(iMAPResponse);
        parse();
    }

    public int getItemCount() {
        return this.items.length;
    }

    public Item getItem(int i) {
        return this.items[i];
    }

    public Item getItem(Class cls) {
        int i = 0;
        while (true) {
            Item[] itemArr = this.items;
            if (i >= itemArr.length) {
                return null;
            }
            if (cls.isInstance(itemArr[i])) {
                return this.items[i];
            }
            i++;
        }
    }

    public static Item getItem(Response[] responseArr, int i, Class cls) {
        if (responseArr == null) {
            return null;
        }
        for (int i2 = 0; i2 < responseArr.length; i2++) {
            if (responseArr[i2] != null && (responseArr[i2] instanceof FetchResponse) && ((FetchResponse) responseArr[i2]).getNumber() == i) {
                FetchResponse fetchResponse = (FetchResponse) responseArr[i2];
                int i3 = 0;
                while (true) {
                    Item[] itemArr = fetchResponse.items;
                    if (i3 >= itemArr.length) {
                        continue;
                        break;
                    } else if (cls.isInstance(itemArr[i3])) {
                        return fetchResponse.items[i3];
                    } else {
                        i3++;
                    }
                }
            }
        }
        return null;
    }

    private void parse() throws ParsingException {
        skipSpaces();
        if (this.buffer[this.index] == 40) {
            Vector vector = new Vector();
            Object obj = null;
            do {
                this.index++;
                if (this.index < this.size) {
                    byte b = this.buffer[this.index];
                    if (b != 66) {
                        if (b != 73) {
                            if (b != 82) {
                                if (b != 85) {
                                    if (b != 69) {
                                        if (b == 70 && match(FLAGS.name)) {
                                            this.index += FLAGS.name.length;
                                            obj = new FLAGS(this);
                                        }
                                    } else if (match(ENVELOPE.name)) {
                                        this.index += ENVELOPE.name.length;
                                        obj = new ENVELOPE(this);
                                    }
                                } else if (match(UID.name)) {
                                    this.index += UID.name.length;
                                    obj = new UID(this);
                                }
                            } else if (match(RFC822SIZE.name)) {
                                this.index += RFC822SIZE.name.length;
                                obj = new RFC822SIZE(this);
                            } else if (match(RFC822DATA.name)) {
                                this.index += RFC822DATA.name.length;
                                char[] cArr = HEADER;
                                if (match(cArr)) {
                                    this.index += cArr.length;
                                } else {
                                    char[] cArr2 = TEXT;
                                    if (match(cArr2)) {
                                        this.index += cArr2.length;
                                    }
                                }
                                obj = new RFC822DATA(this);
                            }
                        } else if (match(INTERNALDATE.name)) {
                            this.index += INTERNALDATE.name.length;
                            obj = new INTERNALDATE(this);
                        }
                    } else if (match(BODY.name)) {
                        if (this.buffer[this.index + 4] == 91) {
                            this.index += BODY.name.length;
                            obj = new BODY(this);
                        } else {
                            if (match(BODYSTRUCTURE.name)) {
                                this.index += BODYSTRUCTURE.name.length;
                            } else {
                                this.index += BODY.name.length;
                            }
                            obj = new BODYSTRUCTURE(this);
                        }
                    }
                    if (obj != null) {
                        vector.addElement(obj);
                    }
                } else {
                    throw new ParsingException("error in FETCH parsing, ran off end of buffer, size " + this.size);
                }
            } while (this.buffer[this.index] != 41);
            this.index++;
            Item[] itemArr = new Item[vector.size()];
            this.items = itemArr;
            vector.copyInto(itemArr);
            return;
        }
        throw new ParsingException("error in FETCH parsing, missing '(' at index " + this.index);
    }

    private boolean match(char[] cArr) {
        int length = cArr.length;
        int i = this.index;
        int i2 = 0;
        while (i2 < length) {
            int i3 = i + 1;
            int i4 = i2 + 1;
            if (Character.toUpperCase((char) this.buffer[i]) != cArr[i2]) {
                return false;
            }
            i2 = i4;
            i = i3;
        }
        return true;
    }
}
