package com.google.firebase.ml.vision.objects.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class ObjectDetectorOptionsParcel extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ObjectDetectorOptionsParcel> CREATOR = new zzf();
    public final int zzbue;
    public final boolean zzbuf;
    public final boolean zzbug;

    public ObjectDetectorOptionsParcel(int i, boolean z, boolean z2) {
        this.zzbue = i;
        this.zzbuf = z;
        this.zzbug = z2;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zzbue);
        SafeParcelWriter.writeBoolean(parcel, 2, this.zzbuf);
        SafeParcelWriter.writeBoolean(parcel, 3, this.zzbug);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
