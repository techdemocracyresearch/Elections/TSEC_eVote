package com.google.firebase.ml.vision.automl.internal;

import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzod;
import com.google.android.gms.internal.firebase_ml.zzpx;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.android.gms.internal.firebase_ml.zzwz;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.internal.modeldownload.RemoteModelManagerInterface;
import com.google.firebase.ml.common.internal.modeldownload.zzg;
import com.google.firebase.ml.common.internal.modeldownload.zzi;
import com.google.firebase.ml.common.internal.modeldownload.zzn;
import com.google.firebase.ml.common.internal.modeldownload.zzv;
import com.google.firebase.ml.common.internal.modeldownload.zzw;
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLRemoteModel;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzb implements RemoteModelManagerInterface<FirebaseAutoMLRemoteModel> {
    private final zzqf zzbkb;
    private final zzqg zzbmd;

    public zzb(zzqf zzqf, zzqg zzqg) {
        this.zzbkb = zzqf;
        this.zzbmd = zzqg;
    }

    @Override // com.google.firebase.ml.common.internal.modeldownload.RemoteModelManagerInterface
    public Task<Set<FirebaseAutoMLRemoteModel>> getDownloadedModels() {
        return Tasks.forException(new FirebaseMLException("AutoML Remote model does not support listing downloaded models", 12));
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel] */
    @Override // com.google.firebase.ml.common.internal.modeldownload.RemoteModelManagerInterface
    public /* synthetic */ Task getLatestModelFile(FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel) {
        return Tasks.forException(new FirebaseMLException("Getting latest model file not supported for AutoML models.", 12));
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel] */
    @Override // com.google.firebase.ml.common.internal.modeldownload.RemoteModelManagerInterface
    public /* synthetic */ Task isModelDownloaded(FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel) {
        return zzpx.zzof().zza(new zzc(this, firebaseAutoMLRemoteModel)).addOnCompleteListener(new zzf(this));
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel] */
    @Override // com.google.firebase.ml.common.internal.modeldownload.RemoteModelManagerInterface
    public /* synthetic */ Task deleteDownloadedModel(FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel) {
        return Tasks.call(zzpx.zzoe(), new zza(this, firebaseAutoMLRemoteModel)).addOnCompleteListener(new zzd(this));
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel, com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions] */
    @Override // com.google.firebase.ml.common.internal.modeldownload.RemoteModelManagerInterface
    public /* synthetic */ Task download(FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel, FirebaseModelDownloadConditions firebaseModelDownloadConditions) {
        FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel2 = firebaseAutoMLRemoteModel;
        zzqf zzqf = this.zzbkb;
        zzv zza = zzv.zza(zzqf, firebaseAutoMLRemoteModel2, new zzg(zzqf), new zzw(this.zzbkb, firebaseAutoMLRemoteModel2));
        zza.zza(firebaseModelDownloadConditions);
        return Tasks.forResult(null).onSuccessTask(zzpx.zzoe(), new zze(zza));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzc(Task task) {
        this.zzbmd.zza(zzns.zzad.zzma().zza((zzns.zzag) ((zzwz) zzns.zzag.zzmg().zzc(zzns.zzaj.zzb.AUTOML_IMAGE_LABELING).zzal(((Boolean) task.getResult()).booleanValue()).zzvb())), zzod.REMOTE_MODEL_IS_DOWNLOADED);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Boolean zza(FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel) throws Exception {
        zzqf zzqf = this.zzbkb;
        return Boolean.valueOf(zzv.zza(zzqf, firebaseAutoMLRemoteModel, new zzg(zzqf), new zzw(this.zzbkb, firebaseAutoMLRemoteModel)).zzpf());
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(Task task) {
        this.zzbmd.zza(zzns.zzad.zzma().zza((zzns.zzaa) ((zzwz) zzns.zzaa.zzlu().zza(zzns.zzaj.zzb.AUTOML_IMAGE_LABELING).zzy(task.isSuccessful()).zzvb())), zzod.REMOTE_MODEL_DELETE_ON_DEVICE);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Void zzb(FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel) throws Exception {
        new zzi(this.zzbkb).zza(zzn.AUTOML, firebaseAutoMLRemoteModel.getModelName());
        return null;
    }
}
