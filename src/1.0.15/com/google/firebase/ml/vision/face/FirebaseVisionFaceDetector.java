package com.google.firebase.ml.vision.face;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzod;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.android.gms.internal.firebase_ml.zzqh;
import com.google.android.gms.internal.firebase_ml.zzrz;
import com.google.android.gms.internal.firebase_ml.zzsh;
import com.google.android.gms.internal.firebase_ml.zzwz;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionFaceDetector extends zzrz<List<FirebaseVisionFace>> implements Closeable {
    private static final Map<zzqh<FirebaseVisionFaceDetectorOptions>, FirebaseVisionFaceDetector> zzbim = new HashMap();

    public static synchronized FirebaseVisionFaceDetector zza(zzqf zzqf, FirebaseVisionFaceDetectorOptions firebaseVisionFaceDetectorOptions) {
        FirebaseVisionFaceDetector firebaseVisionFaceDetector;
        synchronized (FirebaseVisionFaceDetector.class) {
            Preconditions.checkNotNull(zzqf, "You must provide a valid MlKitContext.");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Persistence key must not be null");
            Preconditions.checkNotNull(zzqf.getApplicationContext(), "You must provide a valid Context.");
            Preconditions.checkNotNull(firebaseVisionFaceDetectorOptions, "You must provide a valid FirebaseVisionFaceDetectorOptions.");
            zzqh<FirebaseVisionFaceDetectorOptions> zzj = zzqh.zzj(zzqf.getPersistenceKey(), firebaseVisionFaceDetectorOptions);
            Map<zzqh<FirebaseVisionFaceDetectorOptions>, FirebaseVisionFaceDetector> map = zzbim;
            firebaseVisionFaceDetector = map.get(zzj);
            if (firebaseVisionFaceDetector == null) {
                firebaseVisionFaceDetector = new FirebaseVisionFaceDetector(zzqf, firebaseVisionFaceDetectorOptions);
                map.put(zzj, firebaseVisionFaceDetector);
            }
        }
        return firebaseVisionFaceDetector;
    }

    private FirebaseVisionFaceDetector(zzqf zzqf, FirebaseVisionFaceDetectorOptions firebaseVisionFaceDetectorOptions) {
        super(zzqf, new zzsh(zzqf, firebaseVisionFaceDetectorOptions));
        zzqg.zza(zzqf, 1).zza(zzns.zzad.zzma().zza((zzns.zzan) ((zzwz) zzns.zzan.zzmv().zzc(firebaseVisionFaceDetectorOptions.zzqs()).zzvb())), zzod.ON_DEVICE_FACE_CREATE);
    }

    public Task<List<FirebaseVisionFace>> detectInImage(FirebaseVisionImage firebaseVisionImage) {
        return super.zza(firebaseVisionImage, false, true);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzrz, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        super.close();
    }
}
