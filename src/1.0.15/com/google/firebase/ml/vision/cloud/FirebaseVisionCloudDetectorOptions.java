package com.google.firebase.ml.vision.cloud;

import com.google.android.gms.common.internal.Objects;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionCloudDetectorOptions {
    public static final FirebaseVisionCloudDetectorOptions DEFAULT = new Builder().build();
    public static final int LATEST_MODEL = 2;
    public static final int STABLE_MODEL = 1;
    private final int zzbqy;
    private final int zzbqz;
    private final boolean zzbra;

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface ModelType {
    }

    private FirebaseVisionCloudDetectorOptions(int i, int i2, boolean z) {
        this.zzbqy = i;
        this.zzbqz = i2;
        this.zzbra = z;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private int zzbqy = 10;
        private int zzbqz = 1;
        private boolean zzbra = false;

        public Builder setMaxResults(int i) {
            this.zzbqy = i;
            return this;
        }

        public Builder setModelType(int i) {
            this.zzbqz = i;
            return this;
        }

        public Builder enforceCertFingerprintMatch() {
            this.zzbra = true;
            return this;
        }

        public FirebaseVisionCloudDetectorOptions build() {
            return new FirebaseVisionCloudDetectorOptions(this.zzbqy, this.zzbqz, this.zzbra);
        }
    }

    public int getMaxResults() {
        return this.zzbqy;
    }

    public int getModelType() {
        return this.zzbqz;
    }

    public final boolean isEnforceCertFingerprintMatch() {
        return this.zzbra;
    }

    public Builder builder() {
        return new Builder();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseVisionCloudDetectorOptions)) {
            return false;
        }
        FirebaseVisionCloudDetectorOptions firebaseVisionCloudDetectorOptions = (FirebaseVisionCloudDetectorOptions) obj;
        return this.zzbqy == firebaseVisionCloudDetectorOptions.zzbqy && this.zzbqz == firebaseVisionCloudDetectorOptions.zzbqz && this.zzbra == firebaseVisionCloudDetectorOptions.zzbra;
    }

    public int hashCode() {
        return Objects.hashCode(Integer.valueOf(this.zzbqy), Integer.valueOf(this.zzbqz), Boolean.valueOf(this.zzbra));
    }
}
