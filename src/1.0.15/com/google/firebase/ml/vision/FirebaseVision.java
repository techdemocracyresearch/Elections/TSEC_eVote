package com.google.firebase.ml.vision;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqu;
import com.google.firebase.FirebaseApp;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.cloud.FirebaseVisionCloudDetectorOptions;
import com.google.firebase.ml.vision.cloud.landmark.FirebaseVisionCloudLandmarkDetector;
import com.google.firebase.ml.vision.document.FirebaseVisionCloudDocumentRecognizerOptions;
import com.google.firebase.ml.vision.document.FirebaseVisionDocumentTextRecognizer;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.ml.vision.label.FirebaseVisionCloudImageLabelerOptions;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler;
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceAutoMLImageLabelerOptions;
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceImageLabelerOptions;
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetector;
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetectorOptions;
import com.google.firebase.ml.vision.text.FirebaseVisionCloudTextRecognizerOptions;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVision {
    private static final FirebaseVisionCloudDetectorOptions zzboq = new FirebaseVisionCloudDetectorOptions.Builder().build();
    private static final FirebaseVisionFaceDetectorOptions zzbor = new FirebaseVisionFaceDetectorOptions.Builder().build();
    private static final FirebaseVisionBarcodeDetectorOptions zzbos = new FirebaseVisionBarcodeDetectorOptions.Builder().build();
    private static final FirebaseVisionCloudTextRecognizerOptions zzbot = new FirebaseVisionCloudTextRecognizerOptions.Builder().build();
    private static final FirebaseVisionCloudDocumentRecognizerOptions zzbou = new FirebaseVisionCloudDocumentRecognizerOptions.Builder().build();
    private static final FirebaseVisionOnDeviceImageLabelerOptions zzbov = new FirebaseVisionOnDeviceImageLabelerOptions.Builder().build();
    private static final FirebaseVisionCloudImageLabelerOptions zzbow = new FirebaseVisionCloudImageLabelerOptions.Builder().build();
    private static final FirebaseVisionObjectDetectorOptions zzbox = new FirebaseVisionObjectDetectorOptions.Builder().build();
    private final zzqu zzbjl;
    private final zzqf zzbkb;

    public static FirebaseVision getInstance() {
        return getInstance(FirebaseApp.getInstance());
    }

    public static FirebaseVision getInstance(FirebaseApp firebaseApp) {
        Preconditions.checkNotNull(firebaseApp, "MlKitContext can not be null");
        return (FirebaseVision) firebaseApp.get(FirebaseVision.class);
    }

    public void setStatsCollectionEnabled(boolean z) {
        this.zzbjl.zzar(z);
    }

    public boolean isStatsCollectionEnabled() {
        return this.zzbjl.zzop();
    }

    public FirebaseVisionFaceDetector getVisionFaceDetector(FirebaseVisionFaceDetectorOptions firebaseVisionFaceDetectorOptions) {
        Preconditions.checkNotNull(firebaseVisionFaceDetectorOptions, "Please provide a valid FirebaseVisionFaceDetectorOptions");
        return FirebaseVisionFaceDetector.zza(this.zzbkb, firebaseVisionFaceDetectorOptions);
    }

    public FirebaseVisionFaceDetector getVisionFaceDetector() {
        return FirebaseVisionFaceDetector.zza(this.zzbkb, zzbor);
    }

    public FirebaseVisionTextRecognizer getOnDeviceTextRecognizer() {
        return FirebaseVisionTextRecognizer.zza(this.zzbkb, null, true);
    }

    public FirebaseVisionTextRecognizer getCloudTextRecognizer() {
        return FirebaseVisionTextRecognizer.zza(this.zzbkb, zzbot, false);
    }

    public FirebaseVisionTextRecognizer getCloudTextRecognizer(FirebaseVisionCloudTextRecognizerOptions firebaseVisionCloudTextRecognizerOptions) {
        return FirebaseVisionTextRecognizer.zza(this.zzbkb, firebaseVisionCloudTextRecognizerOptions, false);
    }

    public FirebaseVisionBarcodeDetector getVisionBarcodeDetector(FirebaseVisionBarcodeDetectorOptions firebaseVisionBarcodeDetectorOptions) {
        return FirebaseVisionBarcodeDetector.zza(this.zzbkb, (FirebaseVisionBarcodeDetectorOptions) Preconditions.checkNotNull(firebaseVisionBarcodeDetectorOptions, "Please provide a valid FirebaseVisionBarcodeDetectorOptions"));
    }

    public FirebaseVisionBarcodeDetector getVisionBarcodeDetector() {
        return FirebaseVisionBarcodeDetector.zza(this.zzbkb, zzbos);
    }

    public FirebaseVisionImageLabeler getOnDeviceImageLabeler(FirebaseVisionOnDeviceImageLabelerOptions firebaseVisionOnDeviceImageLabelerOptions) {
        return FirebaseVisionImageLabeler.zza(this.zzbkb, (FirebaseVisionOnDeviceImageLabelerOptions) Preconditions.checkNotNull(firebaseVisionOnDeviceImageLabelerOptions, "Please provide a valid FirebaseVisionOnDeviceImageLabelerOptions"));
    }

    public FirebaseVisionImageLabeler getOnDeviceImageLabeler() {
        return FirebaseVisionImageLabeler.zza(this.zzbkb, zzbov);
    }

    public FirebaseVisionImageLabeler getOnDeviceAutoMLImageLabeler(FirebaseVisionOnDeviceAutoMLImageLabelerOptions firebaseVisionOnDeviceAutoMLImageLabelerOptions) throws FirebaseMLException {
        return FirebaseVisionImageLabeler.zza(this.zzbkb, (FirebaseVisionOnDeviceAutoMLImageLabelerOptions) Preconditions.checkNotNull(firebaseVisionOnDeviceAutoMLImageLabelerOptions, "Please provide a valid FirebaseVisionOnDeviceAutoMLImageLabelerOptions"));
    }

    public FirebaseVisionImageLabeler getCloudImageLabeler(FirebaseVisionCloudImageLabelerOptions firebaseVisionCloudImageLabelerOptions) {
        return FirebaseVisionImageLabeler.zza(this.zzbkb, firebaseVisionCloudImageLabelerOptions);
    }

    public FirebaseVisionImageLabeler getCloudImageLabeler() {
        return FirebaseVisionImageLabeler.zza(this.zzbkb, zzbow);
    }

    public FirebaseVisionDocumentTextRecognizer getCloudDocumentTextRecognizer(FirebaseVisionCloudDocumentRecognizerOptions firebaseVisionCloudDocumentRecognizerOptions) {
        return FirebaseVisionDocumentTextRecognizer.zza(this.zzbkb, firebaseVisionCloudDocumentRecognizerOptions);
    }

    public FirebaseVisionDocumentTextRecognizer getCloudDocumentTextRecognizer() {
        return FirebaseVisionDocumentTextRecognizer.zza(this.zzbkb, zzbou);
    }

    public FirebaseVisionCloudLandmarkDetector getVisionCloudLandmarkDetector(FirebaseVisionCloudDetectorOptions firebaseVisionCloudDetectorOptions) {
        return FirebaseVisionCloudLandmarkDetector.zza(this.zzbkb, firebaseVisionCloudDetectorOptions);
    }

    public FirebaseVisionCloudLandmarkDetector getVisionCloudLandmarkDetector() {
        return FirebaseVisionCloudLandmarkDetector.zza(this.zzbkb, zzboq);
    }

    public FirebaseVisionObjectDetector getOnDeviceObjectDetector(FirebaseVisionObjectDetectorOptions firebaseVisionObjectDetectorOptions) {
        return FirebaseVisionObjectDetector.zza(this.zzbkb, firebaseVisionObjectDetectorOptions);
    }

    public FirebaseVisionObjectDetector getOnDeviceObjectDetector() {
        return FirebaseVisionObjectDetector.zza(this.zzbkb, zzbox);
    }

    FirebaseVision(zzqf zzqf) {
        this.zzbkb = zzqf;
        this.zzbjl = zzqu.zzb(zzqf);
    }
}
