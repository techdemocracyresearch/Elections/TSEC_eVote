package com.google.firebase.ml.vision.text;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzss;
import com.google.android.gms.internal.firebase_ml.zzsu;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import java.io.Closeable;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionTextRecognizer implements Closeable {
    public static final int CLOUD = 2;
    public static final int ON_DEVICE = 1;
    private static final Map<zzsu, FirebaseVisionTextRecognizer> zzbtr = new HashMap();
    private static final Map<zzss, FirebaseVisionTextRecognizer> zzbts = new HashMap();
    private final zzsu zzbux;
    private final zzss zzbuy;
    private final int zzbuz;

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface RecognizerType {
    }

    public static synchronized FirebaseVisionTextRecognizer zza(zzqf zzqf, FirebaseVisionCloudTextRecognizerOptions firebaseVisionCloudTextRecognizerOptions, boolean z) {
        synchronized (FirebaseVisionTextRecognizer.class) {
            Preconditions.checkNotNull(zzqf, "MlKitContext must not be null");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Persistence key must not be null");
            if (!z) {
                Preconditions.checkNotNull(firebaseVisionCloudTextRecognizerOptions, "Options must not be null");
            }
            if (z) {
                zzsu zzc = zzsu.zzc(zzqf);
                Map<zzsu, FirebaseVisionTextRecognizer> map = zzbtr;
                FirebaseVisionTextRecognizer firebaseVisionTextRecognizer = map.get(zzc);
                if (firebaseVisionTextRecognizer == null) {
                    firebaseVisionTextRecognizer = new FirebaseVisionTextRecognizer(zzc, null, 1);
                    map.put(zzc, firebaseVisionTextRecognizer);
                }
                return firebaseVisionTextRecognizer;
            }
            zzss zza = zzss.zza(zzqf, firebaseVisionCloudTextRecognizerOptions);
            Map<zzss, FirebaseVisionTextRecognizer> map2 = zzbts;
            FirebaseVisionTextRecognizer firebaseVisionTextRecognizer2 = map2.get(zza);
            if (firebaseVisionTextRecognizer2 == null) {
                firebaseVisionTextRecognizer2 = new FirebaseVisionTextRecognizer(null, zza, 2);
                map2.put(zza, firebaseVisionTextRecognizer2);
            }
            return firebaseVisionTextRecognizer2;
        }
    }

    private FirebaseVisionTextRecognizer(zzsu zzsu, zzss zzss, int i) {
        this.zzbuz = i;
        this.zzbux = zzsu;
        this.zzbuy = zzss;
    }

    public Task<FirebaseVisionText> processImage(FirebaseVisionImage firebaseVisionImage) {
        Preconditions.checkArgument((this.zzbux == null && this.zzbuy == null) ? false : true, "Either on-device or cloud text recognizer should be enabled.");
        zzsu zzsu = this.zzbux;
        if (zzsu != null) {
            return zzsu.processImage(firebaseVisionImage);
        }
        return this.zzbuy.processImage(firebaseVisionImage);
    }

    public int getRecognizerType() {
        return this.zzbuz;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        zzsu zzsu = this.zzbux;
        if (zzsu != null) {
            zzsu.close();
        }
        zzss zzss = this.zzbuy;
        if (zzss != null) {
            zzss.close();
        }
    }
}
