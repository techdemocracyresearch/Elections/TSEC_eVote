package com.google.firebase.ml.common;

import android.content.Context;
import com.google.android.gms.internal.firebase_ml.zzmw;
import com.google.android.gms.internal.firebase_ml.zzqb;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.android.gms.internal.firebase_ml.zzqr;
import com.google.android.gms.internal.firebase_ml.zzqu;
import com.google.firebase.components.Component;
import com.google.firebase.components.ComponentRegistrar;
import com.google.firebase.components.Dependency;
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class CommonComponentRegistrar implements ComponentRegistrar {
    @Override // com.google.firebase.components.ComponentRegistrar
    public List<Component<?>> getComponents() {
        return zzmw.zza(zzqg.zzbjt, zzqb.zzbja, zzqr.zzbja, zzqu.zzbja, zzqf.zzbja, Component.builder(zzqg.zzb.class).add(Dependency.required(Context.class)).factory(zzb.zzbil).build(), Component.builder(FirebaseModelManager.class).add(Dependency.setOf(FirebaseModelManager.RemoteModelManagerRegistration.class)).factory(zza.zzbil).build());
    }
}
