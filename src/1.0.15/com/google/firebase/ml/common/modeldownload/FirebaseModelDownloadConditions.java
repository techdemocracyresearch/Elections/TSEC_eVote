package com.google.firebase.ml.common.modeldownload;

import com.google.android.gms.common.internal.Objects;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class FirebaseModelDownloadConditions {
    private final boolean zzbmx;
    private final boolean zzbmy;
    private final boolean zzbmz;

    private FirebaseModelDownloadConditions(boolean z, boolean z2, boolean z3) {
        this.zzbmx = z;
        this.zzbmy = z2;
        this.zzbmz = z3;
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static class Builder {
        private boolean zzbmx = false;
        private boolean zzbmy = false;
        private boolean zzbmz = false;

        public Builder requireCharging() {
            this.zzbmx = true;
            return this;
        }

        public Builder requireWifi() {
            this.zzbmy = true;
            return this;
        }

        public Builder requireDeviceIdle() {
            this.zzbmz = true;
            return this;
        }

        public FirebaseModelDownloadConditions build() {
            return new FirebaseModelDownloadConditions(this.zzbmx, this.zzbmy, this.zzbmz);
        }
    }

    public boolean isChargingRequired() {
        return this.zzbmx;
    }

    public boolean isWifiRequired() {
        return this.zzbmy;
    }

    public boolean isDeviceIdleRequired() {
        return this.zzbmz;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseModelDownloadConditions)) {
            return false;
        }
        FirebaseModelDownloadConditions firebaseModelDownloadConditions = (FirebaseModelDownloadConditions) obj;
        return this.zzbmx == firebaseModelDownloadConditions.zzbmx && this.zzbmz == firebaseModelDownloadConditions.zzbmz && this.zzbmy == firebaseModelDownloadConditions.zzbmy;
    }

    public int hashCode() {
        return Objects.hashCode(Boolean.valueOf(this.zzbmx), Boolean.valueOf(this.zzbmy), Boolean.valueOf(this.zzbmz));
    }
}
