package com.google.firebase.ml.common.internal.modeldownload;

import android.os.ParcelFileDescriptor;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqu;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel;
import java.io.File;
import java.nio.MappedByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzag {
    private static final GmsLogger zzbin = new GmsLogger("RemoteModelLoader", "");
    private static final Map<String, zzag> zzblv = new HashMap();
    private final zzqf zzbkb;
    private final FirebaseRemoteModel zzblz;
    private final zzw zzbma;
    private final zzn zzbmh;
    private final zzv zzbmn;
    private final zzz zzbmo;
    private final zzaf zzbmp;
    private boolean zzbmq = true;

    private zzag(zzqf zzqf, FirebaseRemoteModel firebaseRemoteModel, zzp zzp, zzaf zzaf, zzn zzn) {
        this.zzbmo = new zzz(zzqf, firebaseRemoteModel, zzp, zzn, new zzi(zzqf));
        zzw zzw = new zzw(zzqf, firebaseRemoteModel);
        this.zzbma = zzw;
        this.zzbmn = zzv.zza(zzqf, firebaseRemoteModel, new zzg(zzqf), zzw);
        this.zzbmp = zzaf;
        this.zzbkb = zzqf;
        this.zzblz = firebaseRemoteModel;
        this.zzbmh = zzn;
    }

    public static synchronized zzag zza(zzqf zzqf, FirebaseRemoteModel firebaseRemoteModel, zzp zzp, zzaf zzaf, zzn zzn) {
        zzag zzag;
        synchronized (zzag.class) {
            String uniqueModelNameForPersist = firebaseRemoteModel.getUniqueModelNameForPersist();
            Map<String, zzag> map = zzblv;
            if (!map.containsKey(uniqueModelNameForPersist)) {
                map.put(uniqueModelNameForPersist, new zzag(zzqf, firebaseRemoteModel, zzp, zzaf, zzn));
            }
            zzag = map.get(uniqueModelNameForPersist);
        }
        return zzag;
    }

    public final synchronized MappedByteBuffer load() throws FirebaseMLException {
        MappedByteBuffer zzat;
        GmsLogger gmsLogger = zzbin;
        gmsLogger.d("RemoteModelLoader", "Try to load newly downloaded model file.");
        zzat = zzat(this.zzbmq);
        if (zzat == null) {
            gmsLogger.d("RemoteModelLoader", "Loading existing model file.");
            zzat = zzpo();
        }
        return zzat;
    }

    public final FirebaseRemoteModel zzpn() {
        return this.zzblz;
    }

    private final MappedByteBuffer zzat(boolean z) throws FirebaseMLException {
        Long zzoz = this.zzbmn.zzoz();
        String zzpa = this.zzbmn.zzpa();
        if (zzoz == null || zzpa == null) {
            zzbin.d("RemoteModelLoader", "No new model is downloading.");
            this.zzbmn.zzpb();
            return null;
        }
        Integer zzpc = this.zzbmn.zzpc();
        if (zzpc == null) {
            this.zzbmn.zzpb();
            return null;
        }
        GmsLogger gmsLogger = zzbin;
        String valueOf = String.valueOf(zzpc);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
        sb.append("Download Status code: ");
        sb.append(valueOf);
        gmsLogger.d("RemoteModelLoader", sb.toString());
        if (zzpc.intValue() == 8) {
            gmsLogger.d("RemoteModelLoader", "Model downloaded successfully");
            this.zzbma.zza(zzoc.NO_ERROR, true, this.zzbmh, zzns.zzai.zza.SUCCEEDED);
            ParcelFileDescriptor zzpd = this.zzbmn.zzpd();
            if (zzpd == null) {
                this.zzbmn.zzpb();
                return null;
            }
            gmsLogger.d("RemoteModelLoader", "moving downloaded model from external storage to private folder.");
            try {
                File zza = this.zzbmo.zza(zzpd, zzpa, this.zzbma);
                if (zza == null) {
                    return null;
                }
                MappedByteBuffer zzg = zzg(zza);
                String valueOf2 = String.valueOf(zza.getParent());
                gmsLogger.d("RemoteModelLoader", valueOf2.length() != 0 ? "Moved the downloaded model to private folder successfully: ".concat(valueOf2) : new String("Moved the downloaded model to private folder successfully: "));
                this.zzbmn.zze(zzpa, this.zzbmh);
                if (!z || !this.zzbmo.zzd(zza)) {
                    return zzg;
                }
                gmsLogger.d("RemoteModelLoader", "All old models are deleted.");
                return zzg(this.zzbmo.zzf(zza));
            } finally {
                this.zzbmn.zzpb();
            }
        } else {
            if (zzpc.intValue() == 16) {
                this.zzbma.zza(false, this.zzbmh, this.zzbmn.zza(zzoz));
                this.zzbmn.zzpb();
            }
            return null;
        }
    }

    private final MappedByteBuffer zzg(File file) throws FirebaseMLException {
        try {
            return zzca(file.getAbsolutePath());
        } catch (Exception e) {
            this.zzbmo.zze(file);
            throw new FirebaseMLException("Failed to load newly downloaded model.", 14, e);
        }
    }

    private final MappedByteBuffer zzpo() throws FirebaseMLException {
        String zzpk = this.zzbmo.zzpk();
        if (zzpk == null) {
            zzbin.d("RemoteModelLoader", "No existing model file");
            return null;
        }
        try {
            return zzca(zzpk);
        } catch (Exception e) {
            this.zzbmo.zze(new File(zzpk));
            zzqu.zzb(this.zzbkb).zzi(this.zzblz);
            throw new FirebaseMLException("Failed to load an already downloaded model.", 14, e);
        }
    }

    private final MappedByteBuffer zzca(String str) throws FirebaseMLException {
        return this.zzbmp.zzbz(str);
    }
}
