package com.google.firebase.ml.common.internal.modeldownload;

import java.io.BufferedWriter;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final /* synthetic */ class zzc implements zze {
    private final String zzbkr;

    zzc(String str) {
        this.zzbkr = str;
    }

    @Override // com.google.firebase.ml.common.internal.modeldownload.zze
    public final void zza(BufferedWriter bufferedWriter) {
        zza.zza(this.zzbkr, bufferedWriter);
    }
}
