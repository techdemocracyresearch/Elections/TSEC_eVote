package com.google.android.gms.common.config;

import com.google.android.gms.common.config.GservicesValue;
import com.google.android.gms.common.internal.Preconditions;

/* compiled from: com.google.android.gms:play-services-basement@@17.5.0 */
final class zzd extends GservicesValue<Integer> {
    zzd(String str, Integer num) {
        super(str, num);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.config.GservicesValue
    public final /* synthetic */ Integer zza(String str) {
        return ((GservicesValue.zza) Preconditions.checkNotNull(null)).zza(this.zza, (Integer) this.zzb);
    }
}
