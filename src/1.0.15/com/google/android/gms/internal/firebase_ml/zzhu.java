package com.google.android.gms.internal.firebase_ml;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final class zzhu extends FilterInputStream {
    private long zzaee = 0;
    private final /* synthetic */ zzhr zzaef;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzhu(zzhr zzhr, InputStream inputStream) {
        super(inputStream);
        this.zzaef = zzhr;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public final int read(byte[] bArr, int i, int i2) throws IOException {
        int read = this.in.read(bArr, i, i2);
        if (read == -1) {
            zzgs();
        } else {
            this.zzaee += (long) read;
        }
        return read;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public final int read() throws IOException {
        int read = this.in.read();
        if (read == -1) {
            zzgs();
        } else {
            this.zzaee++;
        }
        return read;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public final long skip(long j) throws IOException {
        long skip = this.in.skip(j);
        this.zzaee += skip;
        return skip;
    }

    private final void zzgs() throws IOException {
        long contentLength = this.zzaef.getContentLength();
        if (contentLength != -1) {
            long j = this.zzaee;
            if (j != 0 && j < contentLength) {
                long j2 = this.zzaee;
                StringBuilder sb = new StringBuilder(102);
                sb.append("Connection closed prematurely: bytesRead = ");
                sb.append(j2);
                sb.append(", Content-Length = ");
                sb.append(contentLength);
                throw new IOException(sb.toString());
            }
        }
    }
}
