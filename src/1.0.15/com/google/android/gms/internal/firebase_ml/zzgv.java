package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.OutputStream;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzgv implements zzjq {
    private final zzjq zzabv;
    private final zzgw zzabw;

    public zzgv(zzjq zzjq, zzgw zzgw) {
        this.zzabv = (zzjq) zzml.checkNotNull(zzjq);
        this.zzabw = (zzgw) zzml.checkNotNull(zzgw);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjq
    public final void writeTo(OutputStream outputStream) throws IOException {
        this.zzabw.zza(this.zzabv, outputStream);
    }
}
