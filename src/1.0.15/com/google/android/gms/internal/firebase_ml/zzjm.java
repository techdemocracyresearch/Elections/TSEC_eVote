package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzjm {
    <T> T zza(InputStream inputStream, Charset charset, Class<T> cls) throws IOException;
}
