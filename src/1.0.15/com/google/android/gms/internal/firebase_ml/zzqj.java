package com.google.android.gms.internal.firebase_ml;

import java.util.concurrent.Callable;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final /* synthetic */ class zzqj implements Callable {
    static final Callable zzbjx = new zzqj();

    private zzqj() {
    }

    @Override // java.util.concurrent.Callable
    public final Object call() {
        return zzqg.zzoj();
    }
}
