package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzxb extends zzvp<Integer> implements zzxg, zzyw, RandomAccess {
    private static final zzxb zzclr;
    private int size;
    private int[] zzcls;

    public static zzxb zzvd() {
        return zzclr;
    }

    zzxb() {
        this(new int[10], 0);
    }

    private zzxb(int[] iArr, int i) {
        this.zzcls = iArr;
        this.size = i;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zztn();
        if (i2 >= i) {
            int[] iArr = this.zzcls;
            System.arraycopy(iArr, i2, iArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzxb)) {
            return super.equals(obj);
        }
        zzxb zzxb = (zzxb) obj;
        if (this.size != zzxb.size) {
            return false;
        }
        int[] iArr = zzxb.zzcls;
        for (int i = 0; i < this.size; i++) {
            if (this.zzcls[i] != iArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + this.zzcls[i2];
        }
        return i;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxg
    /* renamed from: zzdr */
    public final zzxg zzcv(int i) {
        if (i >= this.size) {
            return new zzxb(Arrays.copyOf(this.zzcls, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    public final int getInt(int i) {
        zzct(i);
        return this.zzcls[i];
    }

    public final int indexOf(Object obj) {
        if (!(obj instanceof Integer)) {
            return -1;
        }
        int intValue = ((Integer) obj).intValue();
        int size2 = size();
        for (int i = 0; i < size2; i++) {
            if (this.zzcls[i] == intValue) {
                return i;
            }
        }
        return -1;
    }

    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    public final int size() {
        return this.size;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxg
    public final void zzds(int i) {
        zztn();
        int i2 = this.size;
        int[] iArr = this.zzcls;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[(((i2 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.zzcls = iArr2;
        }
        int[] iArr3 = this.zzcls;
        int i3 = this.size;
        this.size = i3 + 1;
        iArr3[i3] = i;
    }

    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection
    public final boolean addAll(Collection<? extends Integer> collection) {
        zztn();
        zzxd.checkNotNull(collection);
        if (!(collection instanceof zzxb)) {
            return super.addAll(collection);
        }
        zzxb zzxb = (zzxb) collection;
        int i = zzxb.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.zzcls;
            if (i3 > iArr.length) {
                this.zzcls = Arrays.copyOf(iArr, i3);
            }
            System.arraycopy(zzxb.zzcls, 0, this.zzcls, this.size, zzxb.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean remove(Object obj) {
        zztn();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Integer.valueOf(this.zzcls[i]))) {
                int[] iArr = this.zzcls;
                System.arraycopy(iArr, i + 1, iArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    private final void zzct(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
    }

    private final String zzcu(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Integer set(int i, Integer num) {
        int intValue = num.intValue();
        zztn();
        zzct(i);
        int[] iArr = this.zzcls;
        int i2 = iArr[i];
        iArr[i] = intValue;
        return Integer.valueOf(i2);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Integer remove(int i) {
        zztn();
        zzct(i);
        int[] iArr = this.zzcls;
        int i2 = iArr[i];
        int i3 = this.size;
        if (i < i3 - 1) {
            System.arraycopy(iArr, i + 1, iArr, i, (i3 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Integer.valueOf(i2);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ void add(int i, Integer num) {
        int i2;
        int intValue = num.intValue();
        zztn();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
        int[] iArr = this.zzcls;
        if (i2 < iArr.length) {
            System.arraycopy(iArr, i, iArr, i + 1, i2 - i);
        } else {
            int[] iArr2 = new int[(((i2 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i);
            System.arraycopy(this.zzcls, i, iArr2, i + 1, this.size - i);
            this.zzcls = iArr2;
        }
        this.zzcls[i] = intValue;
        this.size++;
        this.modCount++;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection, java.util.AbstractList
    public final /* synthetic */ boolean add(Integer num) {
        zzds(num.intValue());
        return true;
    }

    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        return Integer.valueOf(getInt(i));
    }

    static {
        zzxb zzxb = new zzxb(new int[0], 0);
        zzclr = zzxb;
        zzxb.zztm();
    }
}
