package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public interface zzgj {
    void zza(zzgg<?> zzgg) throws IOException;
}
