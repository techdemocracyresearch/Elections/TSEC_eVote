package com.google.android.gms.internal.firebase_ml;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzig extends zzhx {
    public static zzig zzht() {
        return zzij.zzafc;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhx
    public final zzib zza(InputStream inputStream) {
        return zza(new InputStreamReader(inputStream, zziw.UTF_8));
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhx
    public final zzib zza(InputStream inputStream, Charset charset) {
        if (charset == null) {
            return zza(inputStream);
        }
        return zza(new InputStreamReader(inputStream, charset));
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhx
    public final zzib zzam(String str) {
        return zza(new StringReader(str));
    }

    private final zzib zza(Reader reader) {
        return new zzil(this, new zzsz(reader));
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhx
    public final zzia zza(OutputStream outputStream, Charset charset) {
        return new zzii(this, new zzte(new OutputStreamWriter(outputStream, charset)));
    }
}
