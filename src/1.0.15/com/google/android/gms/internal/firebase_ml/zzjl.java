package com.google.android.gms.internal.firebase_ml;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzjl extends FilterOutputStream {
    private final zzjj zzaib;

    public zzjl(OutputStream outputStream, Logger logger, Level level, int i) {
        super(outputStream);
        this.zzaib = new zzjj(logger, level, i);
    }

    @Override // java.io.OutputStream, java.io.FilterOutputStream
    public final void write(int i) throws IOException {
        this.out.write(i);
        this.zzaib.write(i);
    }

    @Override // java.io.OutputStream, java.io.FilterOutputStream
    public final void write(byte[] bArr, int i, int i2) throws IOException {
        this.out.write(bArr, i, i2);
        this.zzaib.write(bArr, i, i2);
    }

    @Override // java.io.OutputStream, java.io.Closeable, java.io.FilterOutputStream, java.lang.AutoCloseable
    public final void close() throws IOException {
        this.zzaib.close();
        super.close();
    }

    public final zzjj zzif() {
        return this.zzaib;
    }
}
