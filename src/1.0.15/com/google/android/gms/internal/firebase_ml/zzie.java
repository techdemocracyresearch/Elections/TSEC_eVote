package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final /* synthetic */ class zzie {
    static final /* synthetic */ int[] zzaem;

    /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|(3:21|22|24)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(24:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|24) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0060 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0078 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
    static {
        int[] iArr = new int[zzih.values().length];
        zzaem = iArr;
        iArr[zzih.START_OBJECT.ordinal()] = 1;
        zzaem[zzih.START_ARRAY.ordinal()] = 2;
        zzaem[zzih.END_ARRAY.ordinal()] = 3;
        zzaem[zzih.FIELD_NAME.ordinal()] = 4;
        zzaem[zzih.END_OBJECT.ordinal()] = 5;
        zzaem[zzih.VALUE_TRUE.ordinal()] = 6;
        zzaem[zzih.VALUE_FALSE.ordinal()] = 7;
        zzaem[zzih.VALUE_NUMBER_FLOAT.ordinal()] = 8;
        zzaem[zzih.VALUE_NUMBER_INT.ordinal()] = 9;
        zzaem[zzih.VALUE_STRING.ordinal()] = 10;
        try {
            zzaem[zzih.VALUE_NULL.ordinal()] = 11;
        } catch (NoSuchFieldError unused) {
        }
    }
}
