package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.vision.Frame;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import java.io.Closeable;
import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzrz<TDetectionResult> implements Closeable {
    private final zzqb zzbnm;
    private final zzpu<TDetectionResult, zzsf> zzbsf;

    protected zzrz(zzqf zzqf, zzpu<TDetectionResult, zzsf> zzpu) {
        Preconditions.checkNotNull(zzqf, "MlKitContext must not be null");
        Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Persistence key must not be null");
        this.zzbsf = zzpu;
        zzqb zza = zzqb.zza(zzqf);
        this.zzbnm = zza;
        zza.zza(zzpu);
    }

    /* access modifiers changed from: protected */
    public final Task<TDetectionResult> zza(FirebaseVisionImage firebaseVisionImage, boolean z, boolean z2) {
        Preconditions.checkNotNull(firebaseVisionImage, "FirebaseVisionImage can not be null");
        Frame zza = firebaseVisionImage.zza(z, z2);
        if (zza.getMetadata().getWidth() < 32 || zza.getMetadata().getHeight() < 32) {
            return Tasks.forException(new FirebaseMLException("Image width and height should be at least 32!", 3));
        }
        return this.zzbnm.zza(this.zzbsf, new zzsf(firebaseVisionImage, zza));
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.zzbnm.zzb(this.zzbsf);
    }
}
