package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzhb {
    private final zzhh zzabf;
    private final zzhe zzaci;

    zzhb(zzhh zzhh, zzhe zzhe) {
        this.zzabf = zzhh;
        this.zzaci = zzhe;
    }

    public final zzhc zza(String str, zzgu zzgu, zzgt zzgt) throws IOException {
        zzhc zzhc = new zzhc(this.zzabf, null);
        zzhe zzhe = this.zzaci;
        if (zzhe != null) {
            zzhe.zza(zzhc);
        }
        zzhc.zzaf(str);
        zzhc.zza(zzgu);
        if (zzgt != null) {
            zzhc.zza(zzgt);
        }
        return zzhc;
    }
}
