package com.google.android.gms.internal.firebase_ml;

import java.lang.reflect.Field;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzis {
    private final Map<String, zziu> zzagf = new zziq();
    private final Map<Field, zziu> zzagg = new zziq();
    private final Object zzagh;

    public zzis(Object obj) {
        this.zzagh = obj;
    }

    public final void zzhw() {
        for (Map.Entry<String, zziu> entry : this.zzagf.entrySet()) {
            ((Map) this.zzagh).put(entry.getKey(), entry.getValue().zzhx());
        }
        for (Map.Entry<Field, zziu> entry2 : this.zzagg.entrySet()) {
            zzjd.zza(entry2.getKey(), this.zzagh, entry2.getValue().zzhx());
        }
    }

    public final void zza(Field field, Class<?> cls, Object obj) {
        zziu zziu = this.zzagg.get(field);
        if (zziu == null) {
            zziu = new zziu(cls);
            this.zzagg.put(field, zziu);
        }
        zzml.checkArgument(cls == zziu.zzagj);
        zziu.zzagk.add(obj);
    }
}
