package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import kotlin.UByte;
import kotlin.jvm.internal.ByteCompanionObject;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzvr {
    static int zza(byte[] bArr, int i, zzvq zzvq) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            return zza(b, bArr, i2, zzvq);
        }
        zzvq.zzchj = b;
        return i2;
    }

    static int zza(int i, byte[] bArr, int i2, zzvq zzvq) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            zzvq.zzchj = i3 | (b << 7);
            return i4;
        }
        int i5 = i3 | ((b & ByteCompanionObject.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b2 = bArr[i4];
        if (b2 >= 0) {
            zzvq.zzchj = i5 | (b2 << 14);
            return i6;
        }
        int i7 = i5 | ((b2 & ByteCompanionObject.MAX_VALUE) << 14);
        int i8 = i6 + 1;
        byte b3 = bArr[i6];
        if (b3 >= 0) {
            zzvq.zzchj = i7 | (b3 << 21);
            return i8;
        }
        int i9 = i7 | ((b3 & ByteCompanionObject.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b4 = bArr[i8];
        if (b4 >= 0) {
            zzvq.zzchj = i9 | (b4 << 28);
            return i10;
        }
        int i11 = i9 | ((b4 & ByteCompanionObject.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                zzvq.zzchj = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    static int zzb(byte[] bArr, int i, zzvq zzvq) {
        int i2 = i + 1;
        long j = (long) bArr[i];
        if (j >= 0) {
            zzvq.zzchk = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b = bArr[i2];
        long j2 = (j & 127) | (((long) (b & ByteCompanionObject.MAX_VALUE)) << 7);
        int i4 = 7;
        while (b < 0) {
            int i5 = i3 + 1;
            byte b2 = bArr[i3];
            i4 += 7;
            j2 |= ((long) (b2 & ByteCompanionObject.MAX_VALUE)) << i4;
            b = b2;
            i3 = i5;
        }
        zzvq.zzchk = j2;
        return i3;
    }

    static int zza(byte[] bArr, int i) {
        return ((bArr[i + 3] & UByte.MAX_VALUE) << 24) | (bArr[i] & UByte.MAX_VALUE) | ((bArr[i + 1] & UByte.MAX_VALUE) << 8) | ((bArr[i + 2] & UByte.MAX_VALUE) << 16);
    }

    static long zzb(byte[] bArr, int i) {
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    static double zzc(byte[] bArr, int i) {
        return Double.longBitsToDouble(zzb(bArr, i));
    }

    static float zzd(byte[] bArr, int i) {
        return Float.intBitsToFloat(zza(bArr, i));
    }

    static int zzc(byte[] bArr, int i, zzvq zzvq) throws zzxk {
        int zza = zza(bArr, i, zzvq);
        int i2 = zzvq.zzchj;
        if (i2 < 0) {
            throw zzxk.zzvf();
        } else if (i2 == 0) {
            zzvq.zzchl = "";
            return zza;
        } else {
            zzvq.zzchl = new String(bArr, zza, i2, zzxd.UTF_8);
            return zza + i2;
        }
    }

    static int zzd(byte[] bArr, int i, zzvq zzvq) throws zzxk {
        int zza = zza(bArr, i, zzvq);
        int i2 = zzvq.zzchj;
        if (i2 < 0) {
            throw zzxk.zzvf();
        } else if (i2 == 0) {
            zzvq.zzchl = "";
            return zza;
        } else {
            zzvq.zzchl = zzaaf.zzh(bArr, zza, i2);
            return zza + i2;
        }
    }

    static int zze(byte[] bArr, int i, zzvq zzvq) throws zzxk {
        int zza = zza(bArr, i, zzvq);
        int i2 = zzvq.zzchj;
        if (i2 < 0) {
            throw zzxk.zzvf();
        } else if (i2 > bArr.length - zza) {
            throw zzxk.zzve();
        } else if (i2 == 0) {
            zzvq.zzchl = zzvv.zzchp;
            return zza;
        } else {
            zzvq.zzchl = zzvv.zzc(bArr, zza, i2);
            return zza + i2;
        }
    }

    static int zza(zzze zzze, byte[] bArr, int i, int i2, zzvq zzvq) throws IOException {
        int i3 = i + 1;
        byte b = bArr[i];
        byte b2 = b;
        if (b < 0) {
            i3 = zza(b, bArr, i3, zzvq);
            b2 = zzvq.zzchj;
        }
        if (b2 < 0 || b2 > i2 - i3) {
            throw zzxk.zzve();
        }
        Object newInstance = zzze.newInstance();
        int i4 = (b2 == 1 ? 1 : 0) + i3;
        zzze.zza(newInstance, bArr, i3, i4, zzvq);
        zzze.zzq(newInstance);
        zzvq.zzchl = newInstance;
        return i4;
    }

    static int zza(zzze zzze, byte[] bArr, int i, int i2, int i3, zzvq zzvq) throws IOException {
        zzyo zzyo = (zzyo) zzze;
        Object newInstance = zzyo.newInstance();
        int zza = zzyo.zza(newInstance, bArr, i, i2, i3, zzvq);
        zzyo.zzq(newInstance);
        zzvq.zzchl = newInstance;
        return zza;
    }

    static int zza(int i, byte[] bArr, int i2, int i3, zzxl<?> zzxl, zzvq zzvq) {
        zzxb zzxb = (zzxb) zzxl;
        int zza = zza(bArr, i2, zzvq);
        zzxb.zzds(zzvq.zzchj);
        while (zza < i3) {
            int zza2 = zza(bArr, zza, zzvq);
            if (i != zzvq.zzchj) {
                break;
            }
            zza = zza(bArr, zza2, zzvq);
            zzxb.zzds(zzvq.zzchj);
        }
        return zza;
    }

    static int zza(byte[] bArr, int i, zzxl<?> zzxl, zzvq zzvq) throws IOException {
        zzxb zzxb = (zzxb) zzxl;
        int zza = zza(bArr, i, zzvq);
        int i2 = zzvq.zzchj + zza;
        while (zza < i2) {
            zza = zza(bArr, zza, zzvq);
            zzxb.zzds(zzvq.zzchj);
        }
        if (zza == i2) {
            return zza;
        }
        throw zzxk.zzve();
    }

    static int zza(zzze<?> zzze, int i, byte[] bArr, int i2, int i3, zzxl<?> zzxl, zzvq zzvq) throws IOException {
        int zza = zza(zzze, bArr, i2, i3, zzvq);
        zzxl.add(zzvq.zzchl);
        while (zza < i3) {
            int zza2 = zza(bArr, zza, zzvq);
            if (i != zzvq.zzchj) {
                break;
            }
            zza = zza(zzze, bArr, zza2, i3, zzvq);
            zzxl.add(zzvq.zzchl);
        }
        return zza;
    }

    static int zza(int i, byte[] bArr, int i2, int i3, zzzz zzzz, zzvq zzvq) throws zzxk {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int zzb = zzb(bArr, i2, zzvq);
                zzzz.zzb(i, Long.valueOf(zzvq.zzchk));
                return zzb;
            } else if (i4 == 1) {
                zzzz.zzb(i, Long.valueOf(zzb(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int zza = zza(bArr, i2, zzvq);
                int i5 = zzvq.zzchj;
                if (i5 < 0) {
                    throw zzxk.zzvf();
                } else if (i5 <= bArr.length - zza) {
                    if (i5 == 0) {
                        zzzz.zzb(i, zzvv.zzchp);
                    } else {
                        zzzz.zzb(i, zzvv.zzc(bArr, zza, i5));
                    }
                    return zza + i5;
                } else {
                    throw zzxk.zzve();
                }
            } else if (i4 == 3) {
                zzzz zzxa = zzzz.zzxa();
                int i6 = (i & -8) | 4;
                int i7 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int zza2 = zza(bArr, i2, zzvq);
                    int i8 = zzvq.zzchj;
                    i7 = i8;
                    if (i8 == i6) {
                        i2 = zza2;
                        break;
                    }
                    int zza3 = zza(i7, bArr, zza2, i3, zzxa, zzvq);
                    i7 = i8;
                    i2 = zza3;
                }
                if (i2 > i3 || i7 != i6) {
                    throw zzxk.zzvi();
                }
                zzzz.zzb(i, zzxa);
                return i2;
            } else if (i4 == 5) {
                zzzz.zzb(i, Integer.valueOf(zza(bArr, i2)));
                return i2 + 4;
            } else {
                throw zzxk.zzvg();
            }
        } else {
            throw zzxk.zzvg();
        }
    }

    static int zza(int i, byte[] bArr, int i2, int i3, zzvq zzvq) throws zzxk {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                return zzb(bArr, i2, zzvq);
            }
            if (i4 == 1) {
                return i2 + 8;
            }
            if (i4 == 2) {
                return zza(bArr, i2, zzvq) + zzvq.zzchj;
            }
            if (i4 == 3) {
                int i5 = (i & -8) | 4;
                int i6 = 0;
                while (i2 < i3) {
                    i2 = zza(bArr, i2, zzvq);
                    i6 = zzvq.zzchj;
                    if (i6 == i5) {
                        break;
                    }
                    i2 = zza(i6, bArr, i2, i3, zzvq);
                }
                if (i2 <= i3 && i6 == i5) {
                    return i2;
                }
                throw zzxk.zzvi();
            } else if (i4 == 5) {
                return i2 + 4;
            } else {
                throw zzxk.zzvg();
            }
        } else {
            throw zzxk.zzvg();
        }
    }
}
