package com.google.android.gms.internal.firebase_ml;

import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzxl<E> extends List<E>, RandomAccess {
    zzxl<E> zzcv(int i);

    boolean zztl();

    void zztm();
}
