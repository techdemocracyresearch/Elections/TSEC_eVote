package com.google.android.gms.internal.firebase_ml;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzzj<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    private boolean zzcim;
    private final int zzcpn;
    private List<zzzo> zzcpo;
    private Map<K, V> zzcpp;
    private volatile zzzq zzcpq;
    private Map<K, V> zzcpr;
    private volatile zzzk zzcps;

    static <FieldDescriptorType extends zzwt<FieldDescriptorType>> zzzj<FieldDescriptorType, Object> zzeb(int i) {
        return new zzzi(i);
    }

    private zzzj(int i) {
        this.zzcpn = i;
        this.zzcpo = Collections.emptyList();
        this.zzcpp = Collections.emptyMap();
        this.zzcpr = Collections.emptyMap();
    }

    public void zztm() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.zzcim) {
            if (this.zzcpp.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.zzcpp);
            }
            this.zzcpp = map;
            if (this.zzcpr.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.zzcpr);
            }
            this.zzcpr = map2;
            this.zzcim = true;
        }
    }

    public final boolean isImmutable() {
        return this.zzcim;
    }

    public final int zzwq() {
        return this.zzcpo.size();
    }

    public final Map.Entry<K, V> zzec(int i) {
        return this.zzcpo.get(i);
    }

    public final Iterable<Map.Entry<K, V>> zzwr() {
        if (this.zzcpp.isEmpty()) {
            return zzzn.zzww();
        }
        return this.zzcpp.entrySet();
    }

    public int size() {
        return this.zzcpo.size() + this.zzcpp.size();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.google.android.gms.internal.firebase_ml.zzzj<K extends java.lang.Comparable<K>, V> */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return zza(comparable) >= 0 || this.zzcpp.containsKey(comparable);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.google.android.gms.internal.firebase_ml.zzzj<K extends java.lang.Comparable<K>, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        return zza >= 0 ? (V) this.zzcpo.get(zza).getValue() : this.zzcpp.get(comparable);
    }

    /* renamed from: zza */
    public final V put(K k, V v) {
        zzwt();
        int zza = zza(k);
        if (zza >= 0) {
            return (V) this.zzcpo.get(zza).setValue(v);
        }
        zzwt();
        if (this.zzcpo.isEmpty() && !(this.zzcpo instanceof ArrayList)) {
            this.zzcpo = new ArrayList(this.zzcpn);
        }
        int i = -(zza + 1);
        if (i >= this.zzcpn) {
            return zzwu().put(k, v);
        }
        int size = this.zzcpo.size();
        int i2 = this.zzcpn;
        if (size == i2) {
            zzzo remove = this.zzcpo.remove(i2 - 1);
            zzwu().put((K) ((Comparable) remove.getKey()), (V) remove.getValue());
        }
        this.zzcpo.add(i, new zzzo(this, k, v));
        return null;
    }

    public void clear() {
        zzwt();
        if (!this.zzcpo.isEmpty()) {
            this.zzcpo.clear();
        }
        if (!this.zzcpp.isEmpty()) {
            this.zzcpp.clear();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.google.android.gms.internal.firebase_ml.zzzj<K extends java.lang.Comparable<K>, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        zzwt();
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return (V) zzed(zza);
        }
        if (this.zzcpp.isEmpty()) {
            return null;
        }
        return this.zzcpp.remove(comparable);
    }

    /* access modifiers changed from: private */
    public final V zzed(int i) {
        zzwt();
        V v = (V) this.zzcpo.remove(i).getValue();
        if (!this.zzcpp.isEmpty()) {
            Iterator<Map.Entry<K, V>> it = zzwu().entrySet().iterator();
            this.zzcpo.add(new zzzo(this, it.next()));
            it.remove();
        }
        return v;
    }

    private final int zza(K k) {
        int size = this.zzcpo.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo((Comparable) this.zzcpo.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo((Comparable) this.zzcpo.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.zzcpq == null) {
            this.zzcpq = new zzzq(this, null);
        }
        return this.zzcpq;
    }

    /* access modifiers changed from: package-private */
    public final Set<Map.Entry<K, V>> zzws() {
        if (this.zzcps == null) {
            this.zzcps = new zzzk(this, null);
        }
        return this.zzcps;
    }

    /* access modifiers changed from: private */
    public final void zzwt() {
        if (this.zzcim) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> zzwu() {
        zzwt();
        if (this.zzcpp.isEmpty() && !(this.zzcpp instanceof TreeMap)) {
            TreeMap treeMap = new TreeMap();
            this.zzcpp = treeMap;
            this.zzcpr = treeMap.descendingMap();
        }
        return (SortedMap) this.zzcpp;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzzj)) {
            return super.equals(obj);
        }
        zzzj zzzj = (zzzj) obj;
        int size = size();
        if (size != zzzj.size()) {
            return false;
        }
        int zzwq = zzwq();
        if (zzwq != zzzj.zzwq()) {
            return entrySet().equals(zzzj.entrySet());
        }
        for (int i = 0; i < zzwq; i++) {
            if (!zzec(i).equals(zzzj.zzec(i))) {
                return false;
            }
        }
        if (zzwq != size) {
            return this.zzcpp.equals(zzzj.zzcpp);
        }
        return true;
    }

    public int hashCode() {
        int zzwq = zzwq();
        int i = 0;
        for (int i2 = 0; i2 < zzwq; i2++) {
            i += this.zzcpo.get(i2).hashCode();
        }
        return this.zzcpp.size() > 0 ? i + this.zzcpp.hashCode() : i;
    }

    /* synthetic */ zzzj(int i, zzzi zzzi) {
        this(i);
    }
}
