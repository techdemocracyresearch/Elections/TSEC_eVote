package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzns;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zzsr implements zzqo {
    private final long zzbok;
    private final zzoc zzbol;
    private final zzsf zzbty;
    private final zzso zzbub;
    private final boolean zzbuc;

    zzsr(zzso zzso, long j, zzoc zzoc, zzsf zzsf, boolean z) {
        this.zzbub = zzso;
        this.zzbok = j;
        this.zzbol = zzoc;
        this.zzbty = zzsf;
        this.zzbuc = z;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqo
    public final zzns.zzad.zza zzok() {
        return this.zzbub.zza(this.zzbok, this.zzbol, this.zzbty, this.zzbuc);
    }
}
