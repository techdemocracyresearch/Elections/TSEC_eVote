package com.google.android.gms.internal.firebase_ml;

import java.util.Locale;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zziz implements Map.Entry<String, Object> {
    private Object zzahe;
    private final zzjd zzahf;
    private final /* synthetic */ zzja zzahg;

    zziz(zzja zzja, zzjd zzjd, Object obj) {
        this.zzahg = zzja;
        this.zzahf = zzjd;
        this.zzahe = zzml.checkNotNull(obj);
    }

    @Override // java.util.Map.Entry
    public final Object getValue() {
        return this.zzahe;
    }

    @Override // java.util.Map.Entry
    public final Object setValue(Object obj) {
        Object obj2 = this.zzahe;
        this.zzahe = zzml.checkNotNull(obj);
        this.zzahf.zzb(this.zzahg.object, obj);
        return obj2;
    }

    public final int hashCode() {
        return ((String) getKey()).hashCode() ^ getValue().hashCode();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return ((String) getKey()).equals(entry.getKey()) && getValue().equals(entry.getValue());
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Map.Entry
    public final /* synthetic */ String getKey() {
        String name = this.zzahf.getName();
        return this.zzahg.zzacg.zzhy() ? name.toLowerCase(Locale.US) : name;
    }
}
