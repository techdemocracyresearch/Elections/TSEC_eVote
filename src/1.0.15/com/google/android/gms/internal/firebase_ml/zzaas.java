package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzaas {

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zza extends zzwz<zza, C0007zza> implements zzym {
        private static final zza zzcsm;
        private static volatile zzyx<zza> zzh;
        private int zzcsj;
        private zzk zzcsk;
        private zzf zzcsl;
        private int zzj;

        private zza() {
        }

        /* renamed from: com.google.android.gms.internal.firebase_ml.zzaas$zza$zza  reason: collision with other inner class name */
        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class C0007zza extends zzwz.zzb<zza, C0007zza> implements zzym {
            private C0007zza() {
                super(zza.zzcsm);
            }

            /* synthetic */ C0007zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0007zza(null);
                case 3:
                    return zza(zzcsm, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzcsj", zzaaz.zzf(), "zzcsk", "zzcsl"});
                case 4:
                    return zzcsm;
                case 5:
                    zzyx<zza> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zza.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcsm);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public static zza zzxk() {
            return zzcsm;
        }

        static {
            zza zza = new zza();
            zzcsm = zza;
            zzwz.zza(zza.class, zza);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzb extends zzwz<zzb, zza> implements zzym {
        private static final zzb zzcta;
        private static volatile zzyx<zzb> zzh;
        private boolean zzcsy;
        private boolean zzcsz;
        private int zzj;

        private zzb() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
            private zza() {
                super(zzb.zzcta);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcta, "\u0001\u0002\u0000\u0001\u0005\u0006\u0002\u0000\u0000\u0000\u0005ဇ\u0000\u0006ဇ\u0001", new Object[]{"zzj", "zzcsy", "zzcsz"});
                case 4:
                    return zzcta;
                case 5:
                    zzyx<zzb> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzb.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcta);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzb zzb = new zzb();
            zzcta = zzb;
            zzwz.zza(zzb.class, zzb);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzc extends zzwz<zzc, zza> implements zzym {
        private static final zzc zzctc;
        private static volatile zzyx<zzc> zzh;
        private boolean zzctb;
        private int zzj;

        private zzc() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzc, zza> implements zzym {
            private zza() {
                super(zzc.zzctc);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzc();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzctc, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001ဇ\u0000", new Object[]{"zzj", "zzctb"});
                case 4:
                    return zzctc;
                case 5:
                    zzyx<zzc> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzc.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzctc);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzc zzc = new zzc();
            zzctc = zzc;
            zzwz.zza(zzc.class, zzc);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzd extends zzwz<zzd, zza> implements zzym {
        private static final zzd zzctm;
        private static volatile zzyx<zzd> zzh;
        private int zzctj;
        private int zzctk = 100;
        private int zzctl;
        private int zzj;

        private zzd() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzd, zza> implements zzym {
            private zza() {
                super(zzd.zzctm);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzd();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzctm, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002င\u0001\u0003င\u0002", new Object[]{"zzj", "zzctj", zzabb.zzf(), "zzctk", "zzctl"});
                case 4:
                    return zzctm;
                case 5:
                    zzyx<zzd> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzd.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzctm);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzd zzd = new zzd();
            zzctm = zzd;
            zzwz.zza(zzd.class, zzd);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zze extends zzwz<zze, zza> implements zzym {
        private static final zze zzcto;
        private static volatile zzyx<zze> zzh;
        private int zzctn;
        private int zzj;

        private zze() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zze, zza> implements zzym {
            private zza() {
                super(zze.zzcto);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zze();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcto, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001င\u0000", new Object[]{"zzj", "zzctn"});
                case 4:
                    return zzcto;
                case 5:
                    zzyx<zze> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zze.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcto);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zze zze = new zze();
            zzcto = zze;
            zzwz.zza(zze.class, zze);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzf extends zzwz<zzf, zza> implements zzym {
        private static final zzf zzctr;
        private static volatile zzyx<zzf> zzh;
        private String zzctp = "";
        private zzg zzctq;
        private int zzj;

        private zzf() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzf, zza> implements zzym {
            private zza() {
                super(zzf.zzctr);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzf();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzctr, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဉ\u0001", new Object[]{"zzj", "zzctp", "zzctq"});
                case 4:
                    return zzctr;
                case 5:
                    zzyx<zzf> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzf.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzctr);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzf zzf = new zzf();
            zzctr = zzf;
            zzwz.zza(zzf.class, zzf);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzg extends zzwz<zzg, zza> implements zzym {
        private static final zzg zzctu;
        private static volatile zzyx<zzg> zzh;
        private String zzcts = "";
        private String zzctt = "";
        private int zzj;

        private zzg() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzg, zza> implements zzym {
            private zza() {
                super(zzg.zzctu);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzg();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzctu, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001", new Object[]{"zzj", "zzcts", "zzctt"});
                case 4:
                    return zzctu;
                case 5:
                    zzyx<zzg> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzg.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzctu);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzg zzg = new zzg();
            zzctu = zzg;
            zzwz.zza(zzg.class, zzg);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzh extends zzwz<zzh, zza> implements zzym {
        private static final zzh zzcuc;
        private static volatile zzyx<zzh> zzh;
        private String zzctv = "";
        private String zzctw = "";
        private String zzctx = "";
        private int zzcty;
        private int zzctz;
        private zzb zzcua;
        private boolean zzcub;
        private int zzj;

        private zzh() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzh, zza> implements zzym {
            private zza() {
                super(zzh.zzcuc);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzh();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcuc, "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003ဈ\u0002\u0004ဌ\u0003\u0005င\u0004\u0006ဉ\u0005\u0007ဇ\u0006", new Object[]{"zzj", "zzctv", "zzctw", "zzctx", "zzcty", zzabf.zzf(), "zzctz", "zzcua", "zzcub"});
                case 4:
                    return zzcuc;
                case 5:
                    zzyx<zzh> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzh.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcuc);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzh zzh2 = new zzh();
            zzcuc = zzh2;
            zzwz.zza(zzh.class, zzh2);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzi extends zzwz<zzi, zza> implements zzym {
        private static final zzi zzcue;
        private static volatile zzyx<zzi> zzh;
        private String zzbak = "";
        private String zzbal = "";
        private int zzcud;
        private int zzj;

        private zzi() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzi, zza> implements zzym {
            private zza() {
                super(zzi.zzcue);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzi();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcue, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003င\u0002", new Object[]{"zzj", "zzbak", "zzbal", "zzcud"});
                case 4:
                    return zzcue;
                case 5:
                    zzyx<zzi> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzi.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcue);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzi zzi = new zzi();
            zzcue = zzi;
            zzwz.zza(zzi.class, zzi);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzj extends zzwz<zzj, zza> implements zzym {
        private static final zzj zzcus;
        private static volatile zzyx<zzj> zzh;
        private String zzcuk = "";
        private int zzcul;
        private String zzcum = "";
        private boolean zzcun;
        private String zzcuo = "";
        private zzxl<String> zzcup = zzwz.zzus();
        private zzxl<zzi> zzcuq = zzus();
        private zzxl<String> zzcur = zzwz.zzus();
        private int zzj;

        private zzj() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzj, zza> implements zzym {
            private zza() {
                super(zzj.zzcus);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzj();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcus, "\u0001\b\u0000\u0001\u0001\t\b\u0000\u0003\u0000\u0001ဈ\u0000\u0002င\u0001\u0003\u001a\u0004\u001b\u0006ဈ\u0002\u0007ဇ\u0003\bဈ\u0004\t\u001a", new Object[]{"zzj", "zzcuk", "zzcul", "zzcup", "zzcuq", zzi.class, "zzcum", "zzcun", "zzcuo", "zzcur"});
                case 4:
                    return zzcus;
                case 5:
                    zzyx<zzj> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzj.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcus);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzj zzj2 = new zzj();
            zzcus = zzj2;
            zzwz.zza(zzj.class, zzj2);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzk extends zzwz<zzk, zza> implements zzym {
        private static final zzk zzcva;
        private static volatile zzyx<zzk> zzh;
        private int zzcut;
        private zzh zzcuu;
        private zzc zzcuv;
        private zze zzcuw;
        private zzd zzcux;
        private zzd zzcuy;
        private zzd zzcuz;
        private int zzj;

        private zzk() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzk, zza> implements zzym {
            private zza() {
                super(zzk.zzcva);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzk();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcva, "\u0001\u0007\u0000\u0001\u0001\b\u0007\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0005ဉ\u0003\u0006ဉ\u0004\u0007ဉ\u0005\bဉ\u0006", new Object[]{"zzj", "zzcut", zzaav.zzf(), "zzcuu", "zzcuv", "zzcuw", "zzcux", "zzcuy", "zzcuz"});
                case 4:
                    return zzcva;
                case 5:
                    zzyx<zzk> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzk.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcva);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzk zzk = new zzk();
            zzcva = zzk;
            zzwz.zza(zzk.class, zzk);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzl extends zzwz<zzl, zza> implements zzym {
        private static final zzl zzcvc;
        private static volatile zzyx<zzl> zzh;
        private zzxl<zzm> zzcvb = zzus();

        private zzl() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzl, zza> implements zzym {
            private zza() {
                super(zzl.zzcvc);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzl();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcvc, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzcvb", zzm.class});
                case 4:
                    return zzcvc;
                case 5:
                    zzyx<zzl> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzl.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcvc);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzl zzl = new zzl();
            zzcvc = zzl;
            zzwz.zza(zzl.class, zzl);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static final class zzm extends zzwz<zzm, zza> implements zzym {
        private static final zzm zzcvg;
        private static volatile zzyx<zzm> zzh;
        private zzf zzcvd;
        private zzxl<zzj> zzcve = zzus();
        private zzxl<zza> zzcvf = zzus();
        private int zzj;

        private zzm() {
        }

        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public static final class zza extends zzwz.zzb<zzm, zza> implements zzym {
            private zza() {
                super(zzm.zzcvg);
            }

            /* synthetic */ zza(zzaau zzaau) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzaau.zzi[i - 1]) {
                case 1:
                    return new zzm();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcvg, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0002\u0000\u0001ဉ\u0000\u0002\u001b\u0003\u001b", new Object[]{"zzj", "zzcvd", "zzcve", zzj.class, "zzcvf", zza.class});
                case 4:
                    return zzcvg;
                case 5:
                    zzyx<zzm> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzm.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcvg);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzm zzm = new zzm();
            zzcvg = zzm;
            zzwz.zza(zzm.class, zzm);
        }
    }
}
