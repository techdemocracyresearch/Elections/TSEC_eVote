package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzid;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public abstract class zzib {
    private static WeakHashMap<Class<?>, Field> zzaej = new WeakHashMap<>();
    private static final Lock zzaek = new ReentrantLock();

    public abstract void close() throws IOException;

    public abstract int getIntValue() throws IOException;

    public abstract String getText() throws IOException;

    public abstract zzhx zzhb();

    public abstract zzih zzhc() throws IOException;

    public abstract zzih zzhd();

    public abstract String zzhe() throws IOException;

    public abstract zzib zzhf() throws IOException;

    public abstract byte zzhg() throws IOException;

    public abstract short zzhh() throws IOException;

    public abstract float zzhi() throws IOException;

    public abstract long zzhj() throws IOException;

    public abstract double zzhk() throws IOException;

    public abstract BigInteger zzhl() throws IOException;

    public abstract BigDecimal zzhm() throws IOException;

    public final <T> T zza(Class<T> cls, zzhv zzhv) throws IOException {
        try {
            return (T) zza(cls, false, null);
        } finally {
            close();
        }
    }

    public final String zza(Set<String> set) throws IOException {
        zzih zzho = zzho();
        while (zzho == zzih.FIELD_NAME) {
            String text = getText();
            zzhc();
            if (set.contains(text)) {
                return text;
            }
            zzhf();
            zzho = zzhc();
        }
        return null;
    }

    private final zzih zzhn() throws IOException {
        zzih zzhd = zzhd();
        if (zzhd == null) {
            zzhd = zzhc();
        }
        zzml.checkArgument(zzhd != null, "no JSON input found");
        return zzhd;
    }

    private final zzih zzho() throws IOException {
        zzih zzhn = zzhn();
        int i = zzie.zzaem[zzhn.ordinal()];
        boolean z = true;
        if (i == 1) {
            zzih zzhc = zzhc();
            if (!(zzhc == zzih.FIELD_NAME || zzhc == zzih.END_OBJECT)) {
                z = false;
            }
            zzml.checkArgument(z, zzhc);
            return zzhc;
        } else if (i != 2) {
            return zzhn;
        } else {
            return zzhc();
        }
    }

    public final Object zza(Type type, boolean z, zzhv zzhv) throws IOException {
        try {
            if (!Void.class.equals(type)) {
                zzhn();
            }
            return zza(null, type, new ArrayList<>(), null, null, true);
        } finally {
            if (z) {
                close();
            }
        }
    }

    private final void zza(Field field, Map<String, Object> map, Type type, ArrayList<Type> arrayList, zzhv zzhv) throws IOException {
        zzih zzho = zzho();
        while (zzho == zzih.FIELD_NAME) {
            String text = getText();
            zzhc();
            if (zzhv == null) {
                map.put(text, zza(field, type, arrayList, map, zzhv, true));
                zzho = zzhc();
            } else {
                throw new NoSuchMethodError();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:128:0x01bd A[Catch:{ IllegalArgumentException -> 0x043d }] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x01c7 A[Catch:{ IllegalArgumentException -> 0x043d }] */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x01ef A[Catch:{ IllegalArgumentException -> 0x043d }] */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x0242 A[Catch:{ IllegalArgumentException -> 0x043d }] */
    private final Object zza(Field field, Type type, ArrayList<Type> arrayList, Object obj, zzhv zzhv, boolean z) throws IOException {
        Object obj2;
        zziv zziv;
        Type type2;
        boolean z2;
        boolean z3;
        Type zza = zzix.zza((List<Type>) arrayList, type);
        Type type3 = null;
        Class<?> cls = zza instanceof Class ? (Class) zza : null;
        if (zza instanceof ParameterizedType) {
            cls = zzjs.zza((ParameterizedType) zza);
        }
        if (cls == Void.class) {
            zzhf();
            return null;
        }
        zzih zzhd = zzhd();
        try {
            boolean z4 = false;
            switch (zzie.zzaem[zzhd().ordinal()]) {
                case 1:
                case 4:
                case 5:
                    Object[] objArr = {zza};
                    if (!zzjs.zzc(zza)) {
                        Field zzb = z ? zzb(cls) : null;
                        if (cls != null) {
                            if (zzhv != null) {
                                throw new NoSuchMethodError();
                            }
                        }
                        boolean z5 = cls != null && zzjs.zza(cls, Map.class);
                        if (zzb != null) {
                            obj2 = new zzhy();
                        } else {
                            if (!z5) {
                                if (cls != null) {
                                    obj2 = zzjs.zzf((Class) cls);
                                }
                            }
                            obj2 = zzix.zze(cls);
                        }
                        int size = arrayList.size();
                        if (zza != null) {
                            arrayList.add(zza);
                        }
                        if (z5 && !zzjf.class.isAssignableFrom(cls)) {
                            Type zzf = Map.class.isAssignableFrom(cls) ? zzjs.zzf(zza) : null;
                            if (zzf != null) {
                                zza(field, (Map) obj2, zzf, arrayList, zzhv);
                                return obj2;
                            }
                        }
                        if (obj2 instanceof zzhy) {
                            ((zzhy) obj2).zza(zzhb());
                        }
                        zzih zzho = zzho();
                        Class<?> cls2 = obj2.getClass();
                        zziv zzc = zziv.zzc(cls2);
                        boolean isAssignableFrom = zzjf.class.isAssignableFrom(cls2);
                        if (isAssignableFrom || !Map.class.isAssignableFrom(cls2)) {
                            while (zzho == zzih.FIELD_NAME) {
                                String text = getText();
                                zzhc();
                                if (zzhv == null) {
                                    zzjd zzao = zzc.zzao(text);
                                    if (zzao != null) {
                                        if (zzao.zzib()) {
                                            if (!zzao.isPrimitive()) {
                                                throw new IllegalArgumentException("final array/object fields are not supported");
                                            }
                                        }
                                        Field zzia = zzao.zzia();
                                        int size2 = arrayList.size();
                                        arrayList.add(zzia.getGenericType());
                                        zziv = zzc;
                                        Object zza2 = zza(zzia, zzao.getGenericType(), arrayList, obj2, zzhv, true);
                                        arrayList.remove(size2);
                                        zzao.zzb(obj2, zza2);
                                    } else {
                                        zziv = zzc;
                                        if (isAssignableFrom) {
                                            ((zzjf) obj2).zzb(text, zza(null, null, arrayList, obj2, zzhv, true));
                                        } else if (zzhv == null) {
                                            zzhf();
                                        } else {
                                            throw new NoSuchMethodError();
                                        }
                                    }
                                    zzho = zzhc();
                                    zzc = zziv;
                                } else {
                                    throw new NoSuchMethodError();
                                }
                            }
                        } else {
                            zza(null, (Map) obj2, zzjs.zzf((Type) cls2), arrayList, zzhv);
                        }
                        if (zza != null) {
                            arrayList.remove(size);
                        }
                        if (zzb == null) {
                            return obj2;
                        }
                        Object obj3 = ((zzhy) obj2).get(zzb.getName());
                        zzml.checkArgument(obj3 != null, "No value specified for @JsonPolymorphicTypeMap field");
                        String obj4 = obj3.toString();
                        zzid.zza[] zzhq = ((zzid) zzb.getAnnotation(zzid.class)).zzhq();
                        int length = zzhq.length;
                        int i = 0;
                        while (true) {
                            if (i < length) {
                                zzid.zza zza3 = zzhq[i];
                                if (zza3.zzhr().equals(obj4)) {
                                    type2 = zza3.zzhs();
                                } else {
                                    i++;
                                }
                            } else {
                                type2 = null;
                            }
                        }
                        boolean z6 = type2 != null;
                        String valueOf = String.valueOf(obj4);
                        zzml.checkArgument(z6, valueOf.length() != 0 ? "No TypeDef annotation found with key: ".concat(valueOf) : new String("No TypeDef annotation found with key: "));
                        zzhx zzhb = zzhb();
                        zzib zzam = zzhb.zzam(zzhb.toString(obj2));
                        zzam.zzhn();
                        return zzam.zza(field, type2, arrayList, null, null, false);
                    }
                    throw new IllegalArgumentException(zzms.zzb("expected object or map type but got %s", objArr));
                case 2:
                case 3:
                    boolean zzc2 = zzjs.zzc(zza);
                    if (zza != null && !zzc2) {
                        if (cls == null || !zzjs.zza(cls, (Class<?>) Collection.class)) {
                            z2 = false;
                            Object[] objArr2 = {zza};
                            if (!z2) {
                                if (zzhv != null) {
                                    if (field != null) {
                                        throw new NoSuchMethodError();
                                    }
                                }
                                Collection<Object> zzb2 = zzix.zzb(zza);
                                if (zzc2) {
                                    type3 = zzjs.zzd(zza);
                                } else if (cls != null && Iterable.class.isAssignableFrom(cls)) {
                                    type3 = zzjs.zze(zza);
                                }
                                Type zza4 = zzix.zza((List<Type>) arrayList, type3);
                                zzih zzho2 = zzho();
                                while (zzho2 != zzih.END_ARRAY) {
                                    zzb2.add(zza(field, zza4, arrayList, zzb2, zzhv, true));
                                    zzho2 = zzhc();
                                }
                                return zzc2 ? zzjs.zza(zzb2, zzjs.zzb(arrayList, zza4)) : zzb2;
                            }
                            throw new IllegalArgumentException(zzms.zzb("expected collection or array type but got %s", objArr2));
                        }
                    }
                    z2 = true;
                    Object[] objArr22 = {zza};
                    if (!z2) {
                    }
                    break;
                case 6:
                case 7:
                    if (!(zza == null || cls == Boolean.TYPE)) {
                        if (cls == null || !cls.isAssignableFrom(Boolean.class)) {
                            z3 = false;
                            Object[] objArr3 = {zza};
                            if (!z3) {
                                return zzhd == zzih.VALUE_TRUE ? Boolean.TRUE : Boolean.FALSE;
                            }
                            throw new IllegalArgumentException(zzms.zzb("expected type Boolean or boolean but got %s", objArr3));
                        }
                    }
                    z3 = true;
                    Object[] objArr32 = {zza};
                    if (!z3) {
                    }
                    break;
                case 8:
                case 9:
                    if (field == null || field.getAnnotation(zzif.class) == null) {
                        z4 = true;
                    }
                    zzml.checkArgument(z4, "number type formatted as a JSON number cannot use @JsonString annotation");
                    if (cls != null) {
                        if (!cls.isAssignableFrom(BigDecimal.class)) {
                            if (cls == BigInteger.class) {
                                return zzhl();
                            }
                            if (cls != Double.class) {
                                if (cls != Double.TYPE) {
                                    if (cls != Long.class) {
                                        if (cls != Long.TYPE) {
                                            if (cls != Float.class) {
                                                if (cls != Float.TYPE) {
                                                    if (cls != Integer.class) {
                                                        if (cls != Integer.TYPE) {
                                                            if (cls != Short.class) {
                                                                if (cls != Short.TYPE) {
                                                                    if (cls != Byte.class) {
                                                                        if (cls != Byte.TYPE) {
                                                                            String valueOf2 = String.valueOf(zza);
                                                                            StringBuilder sb = new StringBuilder(String.valueOf(valueOf2).length() + 30);
                                                                            sb.append("expected numeric type but got ");
                                                                            sb.append(valueOf2);
                                                                            throw new IllegalArgumentException(sb.toString());
                                                                        }
                                                                    }
                                                                    return Byte.valueOf(zzhg());
                                                                }
                                                            }
                                                            return Short.valueOf(zzhh());
                                                        }
                                                    }
                                                    return Integer.valueOf(getIntValue());
                                                }
                                            }
                                            return Float.valueOf(zzhi());
                                        }
                                    }
                                    return Long.valueOf(zzhj());
                                }
                            }
                            return Double.valueOf(zzhk());
                        }
                    }
                    return zzhm();
                case 10:
                    String lowerCase = getText().trim().toLowerCase(Locale.US);
                    if (!(cls == Float.TYPE || cls == Float.class || cls == Double.TYPE || cls == Double.class) || (!lowerCase.equals("nan") && !lowerCase.equals("infinity") && !lowerCase.equals("-infinity"))) {
                        if (cls == null || !Number.class.isAssignableFrom(cls) || !(field == null || field.getAnnotation(zzif.class) == null)) {
                            z4 = true;
                        }
                        zzml.checkArgument(z4, "number field formatted as a JSON string must use the @JsonString annotation");
                    }
                    return zzix.zza(zza, getText());
                case 11:
                    if (cls == null || !cls.isPrimitive()) {
                        z4 = true;
                    }
                    zzml.checkArgument(z4, "primitive number field but found a JSON null");
                    if (!(cls == null || (cls.getModifiers() & 1536) == 0)) {
                        if (zzjs.zza(cls, (Class<?>) Collection.class)) {
                            return zzix.zzd(zzix.zzb(zza).getClass());
                        }
                        if (zzjs.zza(cls, (Class<?>) Map.class)) {
                            return zzix.zzd(zzix.zze(cls).getClass());
                        }
                    }
                    return zzix.zzd(zzjs.zzb(arrayList, zza));
                default:
                    String valueOf3 = String.valueOf(zzhd);
                    StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf3).length() + 27);
                    sb2.append("unexpected JSON node type: ");
                    sb2.append(valueOf3);
                    throw new IllegalArgumentException(sb2.toString());
            }
        } catch (IllegalArgumentException e) {
            StringBuilder sb3 = new StringBuilder();
            String zzhe = zzhe();
            if (zzhe != null) {
                sb3.append("key ");
                sb3.append(zzhe);
            }
            if (field != null) {
                if (zzhe != null) {
                    sb3.append(", ");
                }
                sb3.append("field ");
                sb3.append(field);
            }
            throw new IllegalArgumentException(sb3.toString(), e);
        }
    }

    private static Field zzb(Class<?> cls) {
        Field field = null;
        if (cls == null) {
            return null;
        }
        Lock lock = zzaek;
        lock.lock();
        try {
            if (zzaej.containsKey(cls)) {
                Field field2 = zzaej.get(cls);
                lock.unlock();
                return field2;
            }
            for (zzjd zzjd : zziv.zzc(cls).zzhz()) {
                Field zzia = zzjd.zzia();
                zzid zzid = (zzid) zzia.getAnnotation(zzid.class);
                if (zzid != null) {
                    Object[] objArr = {cls};
                    if (field == null) {
                        boolean zza = zzix.zza(zzia.getType());
                        Object[] objArr2 = {cls, zzia.getType()};
                        if (zza) {
                            zzid.zza[] zzhq = zzid.zzhq();
                            HashSet hashSet = new HashSet();
                            zzml.checkArgument(zzhq.length > 0, "@JsonPolymorphicTypeMap must have at least one @TypeDef");
                            for (zzid.zza zza2 : zzhq) {
                                boolean add = hashSet.add(zza2.zzhr());
                                Object[] objArr3 = {zza2.zzhr()};
                                if (!add) {
                                    throw new IllegalArgumentException(zzms.zzb("Class contains two @TypeDef annotations with identical key: %s", objArr3));
                                }
                            }
                            field = zzia;
                        } else {
                            throw new IllegalArgumentException(zzms.zzb("Field which has the @JsonPolymorphicTypeMap, %s, is not a supported type: %s", objArr2));
                        }
                    } else {
                        throw new IllegalArgumentException(zzms.zzb("Class contains more than one field with @JsonPolymorphicTypeMap annotation: %s", objArr));
                    }
                }
            }
            zzaej.put(cls, field);
            return field;
        } finally {
            zzaek.unlock();
        }
    }
}
