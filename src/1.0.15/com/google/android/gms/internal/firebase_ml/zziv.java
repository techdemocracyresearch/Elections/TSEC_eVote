package com.google.android.gms.internal.firebase_ml;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zziv {
    private static final ConcurrentMap<Class<?>, zziv> zzagl = new ConcurrentHashMap();
    private static final ConcurrentMap<Class<?>, zziv> zzagm = new ConcurrentHashMap();
    private final Class<?> zzagn;
    private final boolean zzago;
    private final IdentityHashMap<String, zzjd> zzagp = new IdentityHashMap<>();
    final List<String> zzagq;

    public static zziv zzc(Class<?> cls) {
        return zza(cls, false);
    }

    public static zziv zza(Class<?> cls, boolean z) {
        if (cls == null) {
            return null;
        }
        ConcurrentMap<Class<?>, zziv> concurrentMap = z ? zzagm : zzagl;
        zziv zziv = concurrentMap.get(cls);
        if (zziv != null) {
            return zziv;
        }
        zziv zziv2 = new zziv(cls, z);
        zziv putIfAbsent = concurrentMap.putIfAbsent(cls, zziv2);
        return putIfAbsent == null ? zziv2 : putIfAbsent;
    }

    public final boolean zzhy() {
        return this.zzago;
    }

    public final zzjd zzao(String str) {
        if (str != null) {
            if (this.zzago) {
                str = str.toLowerCase(Locale.US);
            }
            str = str.intern();
        }
        return this.zzagp.get(str);
    }

    public final boolean isEnum() {
        return this.zzagn.isEnum();
    }

    private zziv(Class<?> cls, boolean z) {
        List<String> list;
        Field field;
        this.zzagn = cls;
        this.zzago = z;
        boolean z2 = !z || !cls.isEnum();
        String valueOf = String.valueOf(cls);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 31);
        sb.append("cannot ignore case on an enum: ");
        sb.append(valueOf);
        zzml.checkArgument(z2, sb.toString());
        TreeSet treeSet = new TreeSet(new zziy(this));
        Field[] declaredFields = cls.getDeclaredFields();
        for (Field field2 : declaredFields) {
            zzjd zza = zzjd.zza(field2);
            if (zza != null) {
                String name = zza.getName();
                name = z ? name.toLowerCase(Locale.US).intern() : name;
                zzjd zzjd = this.zzagp.get(name);
                boolean z3 = zzjd == null;
                Object[] objArr = new Object[4];
                objArr[0] = z ? "case-insensitive " : "";
                objArr[1] = name;
                objArr[2] = field2;
                if (zzjd == null) {
                    field = null;
                } else {
                    field = zzjd.zzia();
                }
                objArr[3] = field;
                if (z3) {
                    this.zzagp.put(name, zza);
                    treeSet.add(name);
                } else {
                    throw new IllegalArgumentException(zzms.zzb("two fields have the same %sname <%s>: %s and %s", objArr));
                }
            }
        }
        Class<? super Object> superclass = cls.getSuperclass();
        if (superclass != null) {
            zziv zza2 = zza(superclass, z);
            treeSet.addAll(zza2.zzagq);
            for (Map.Entry<String, zzjd> entry : zza2.zzagp.entrySet()) {
                String key = entry.getKey();
                if (!this.zzagp.containsKey(key)) {
                    this.zzagp.put(key, entry.getValue());
                }
            }
        }
        if (treeSet.isEmpty()) {
            list = Collections.emptyList();
        } else {
            list = Collections.unmodifiableList(new ArrayList(treeSet));
        }
        this.zzagq = list;
    }

    public final Collection<zzjd> zzhz() {
        return Collections.unmodifiableCollection(this.zzagp.values());
    }
}
