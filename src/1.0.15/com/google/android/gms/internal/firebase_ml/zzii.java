package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final class zzii extends zzia {
    private final zzte zzafa;
    private final zzig zzafb;

    zzii(zzig zzig, zzte zzte) {
        this.zzafb = zzig;
        this.zzafa = zzte;
        zzte.setLenient(true);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void flush() throws IOException {
        this.zzafa.flush();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void writeBoolean(boolean z) throws IOException {
        this.zzafa.zzaw(z);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zzgw() throws IOException {
        this.zzafa.zzrj();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zzgy() throws IOException {
        this.zzafa.zzrl();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zzan(String str) throws IOException {
        this.zzafa.zzcf(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zzgz() throws IOException {
        this.zzafa.zzrn();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zzah(int i) throws IOException {
        this.zzafa.zzu((long) i);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zze(long j) throws IOException {
        this.zzafa.zzu(j);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zza(BigInteger bigInteger) throws IOException {
        this.zzafa.zza(bigInteger);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zza(double d) throws IOException {
        this.zzafa.zzb(d);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zzj(float f) throws IOException {
        this.zzafa.zzb((double) f);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zza(BigDecimal bigDecimal) throws IOException {
        this.zzafa.zza(bigDecimal);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zzgv() throws IOException {
        this.zzafa.zzri();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zzgx() throws IOException {
        this.zzafa.zzrk();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void writeString(String str) throws IOException {
        this.zzafa.zzcg(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzia
    public final void zzha() throws IOException {
        this.zzafa.setIndent("  ");
    }
}
