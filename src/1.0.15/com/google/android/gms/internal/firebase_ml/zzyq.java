package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzyq<T> implements zzze<T> {
    private final zzyk zzcoe;
    private final boolean zzcof;
    private final zzzw<?, ?> zzcoo;
    private final zzwq<?> zzcop;

    private zzyq(zzzw<?, ?> zzzw, zzwq<?> zzwq, zzyk zzyk) {
        this.zzcoo = zzzw;
        this.zzcof = zzwq.zze(zzyk);
        this.zzcop = zzwq;
        this.zzcoe = zzyk;
    }

    static <T> zzyq<T> zza(zzzw<?, ?> zzzw, zzwq<?> zzwq, zzyk zzyk) {
        return new zzyq<>(zzzw, zzwq, zzyk);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final T newInstance() {
        return (T) this.zzcoe.zzuu().zzva();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final boolean equals(T t, T t2) {
        if (!this.zzcoo.zzae(t).equals(this.zzcoo.zzae(t2))) {
            return false;
        }
        if (this.zzcof) {
            return this.zzcop.zzo(t).equals(this.zzcop.zzo(t2));
        }
        return true;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final int hashCode(T t) {
        int hashCode = this.zzcoo.zzae(t).hashCode();
        return this.zzcof ? (hashCode * 53) + this.zzcop.zzo(t).hashCode() : hashCode;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final void zze(T t, T t2) {
        zzzg.zza(this.zzcoo, t, t2);
        if (this.zzcof) {
            zzzg.zza(this.zzcop, t, t2);
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final void zza(T t, zzaat zzaat) throws IOException {
        Iterator<Map.Entry<?, Object>> it = this.zzcop.zzo(t).iterator();
        while (it.hasNext()) {
            Map.Entry<?, Object> next = it.next();
            zzwt zzwt = (zzwt) next.getKey();
            if (zzwt.zzuj() != zzaaq.MESSAGE || zzwt.zzuk() || zzwt.zzul()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof zzxr) {
                zzaat.zza(zzwt.zzd(), (Object) ((zzxr) next).zzvm().zztg());
            } else {
                zzaat.zza(zzwt.zzd(), next.getValue());
            }
        }
        zzzw<?, ?> zzzw = this.zzcoo;
        zzzw.zzc(zzzw.zzae(t), zzaat);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final void zza(T t, byte[] bArr, int i, int i2, zzvq zzvq) throws IOException {
        T t2 = t;
        zzzz zzzz = t2.zzclj;
        if (zzzz == zzzz.zzwz()) {
            zzzz = zzzz.zzxa();
            t2.zzclj = zzzz;
        }
        t.zzvc();
        zzwz.zze zze = null;
        while (i < i2) {
            int zza = zzvr.zza(bArr, i, zzvq);
            int i3 = zzvq.zzchj;
            if (i3 == 11) {
                int i4 = 0;
                zzvv zzvv = null;
                while (zza < i2) {
                    zza = zzvr.zza(bArr, zza, zzvq);
                    int i5 = zzvq.zzchj;
                    int i6 = i5 >>> 3;
                    int i7 = i5 & 7;
                    if (i6 != 2) {
                        if (i6 == 3) {
                            if (zze != null) {
                                zzyz.zzwh();
                                throw new NoSuchMethodError();
                            } else if (i7 == 2) {
                                zza = zzvr.zze(bArr, zza, zzvq);
                                zzvv = (zzvv) zzvq.zzchl;
                            }
                        }
                    } else if (i7 == 0) {
                        zza = zzvr.zza(bArr, zza, zzvq);
                        i4 = zzvq.zzchj;
                        zze = (zzwz.zze) this.zzcop.zza(zzvq.zzit, this.zzcoe, i4);
                    }
                    if (i5 == 12) {
                        break;
                    }
                    zza = zzvr.zza(i5, bArr, zza, i2, zzvq);
                }
                if (zzvv != null) {
                    zzzz.zzb((i4 << 3) | 2, zzvv);
                }
                i = zza;
            } else if ((i3 & 7) == 2) {
                zze = (zzwz.zze) this.zzcop.zza(zzvq.zzit, this.zzcoe, i3 >>> 3);
                if (zze == null) {
                    i = zzvr.zza(i3, bArr, zza, i2, zzzz, zzvq);
                } else {
                    zzyz.zzwh();
                    throw new NoSuchMethodError();
                }
            } else {
                i = zzvr.zza(i3, bArr, zza, i2, zzvq);
            }
        }
        if (i != i2) {
            throw zzxk.zzvi();
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final void zzq(T t) {
        this.zzcoo.zzq(t);
        this.zzcop.zzq(t);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final boolean zzac(T t) {
        return this.zzcop.zzo(t).isInitialized();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzze
    public final int zzaa(T t) {
        zzzw<?, ?> zzzw = this.zzcoo;
        int zzaf = zzzw.zzaf(zzzw.zzae(t)) + 0;
        return this.zzcof ? zzaf + this.zzcop.zzo(t).zzue() : zzaf;
    }
}
