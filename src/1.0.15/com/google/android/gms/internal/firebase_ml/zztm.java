package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zztg;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zztm implements zzxe {
    static final zzxe zzan = new zztm();

    private zztm() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxe
    public final boolean zzb(int i) {
        return zztg.zzb.zzd.zzcg(i) != null;
    }
}
