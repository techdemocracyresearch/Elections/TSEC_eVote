package com.google.android.gms.internal.firebase_ml;

import java.util.concurrent.Callable;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final /* synthetic */ class zzqa implements Callable {
    private final zzqb zzbiv;
    private final zzqp zzbiw;
    private final Callable zzbix;

    zzqa(zzqb zzqb, zzqp zzqp, Callable callable) {
        this.zzbiv = zzqb;
        this.zzbiw = zzqp;
        this.zzbix = callable;
    }

    @Override // java.util.concurrent.Callable
    public final Object call() {
        return this.zzbiv.zzb(this.zzbiw, this.zzbix);
    }
}
