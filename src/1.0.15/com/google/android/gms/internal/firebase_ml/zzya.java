package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzya implements zzyl {
    zzya() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyl
    public final boolean zzi(Class<?> cls) {
        return false;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyl
    public final zzyi zzj(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
