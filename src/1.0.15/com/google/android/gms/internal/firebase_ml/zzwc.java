package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzwc extends zzvv {
    zzwc() {
    }

    /* access modifiers changed from: package-private */
    public abstract boolean zza(zzvv zzvv, int i, int i2);

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final int zztr() {
        return 0;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final boolean zzts() {
        return true;
    }
}
