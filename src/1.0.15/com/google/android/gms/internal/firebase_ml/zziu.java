package com.google.android.gms.internal.firebase_ml;

import java.util.ArrayList;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zziu {
    final Class<?> zzagj;
    final ArrayList<Object> zzagk = new ArrayList<>();

    zziu(Class<?> cls) {
        this.zzagj = cls;
    }

    /* access modifiers changed from: package-private */
    public final Object zzhx() {
        return zzjs.zza(this.zzagk, this.zzagj);
    }
}
