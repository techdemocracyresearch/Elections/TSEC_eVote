package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzvy extends zzwf {
    private final int zzchs;
    private final int zzcht;

    zzvy(byte[] bArr, int i, int i2) {
        super(bArr);
        zzd(i, i + i2, bArr.length);
        this.zzchs = i;
        this.zzcht = i2;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzwf, com.google.android.gms.internal.firebase_ml.zzvv
    public final byte zzcw(int i) {
        zzg(i, size());
        return this.bytes[this.zzchs + i];
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzwf, com.google.android.gms.internal.firebase_ml.zzvv
    public final byte zzcx(int i) {
        return this.bytes[this.zzchs + i];
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzwf, com.google.android.gms.internal.firebase_ml.zzvv
    public final int size() {
        return this.zzcht;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzwf
    public final int zztu() {
        return this.zzchs;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzwf, com.google.android.gms.internal.firebase_ml.zzvv
    public final void zzb(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.bytes, zztu() + i, bArr, i2, i3);
    }
}
