package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzhc {
    private zzjm zzaas;
    private String zzaax;
    private final zzhh zzabf;
    private zzgw zzabw;
    private zzgy zzacj;
    private zzgx zzack = new zzgx();
    private zzgx zzacl = new zzgx();
    private int zzacm = 10;
    private int zzacn = 16384;
    private boolean zzaco = true;
    private boolean zzacp = true;
    private zzgt zzacq;
    private zzgu zzacr;
    private int zzacs = 20000;
    private int zzact = 20000;
    private zzhi zzacu;
    private boolean zzacv = true;
    private boolean zzacw = true;
    @Deprecated
    private boolean zzacx = false;
    private zzjo zzacy = zzjo.zzaif;

    zzhc(zzhh zzhh, String str) {
        this.zzabf = zzhh;
        zzaf(null);
    }

    public final zzhh zzfv() {
        return this.zzabf;
    }

    public final String getRequestMethod() {
        return this.zzaax;
    }

    public final zzhc zzaf(String str) {
        zzml.checkArgument(str == null || zzgz.zzae(str));
        this.zzaax = str;
        return this;
    }

    public final zzgu zzfw() {
        return this.zzacr;
    }

    public final zzhc zza(zzgu zzgu) {
        this.zzacr = (zzgu) zzml.checkNotNull(zzgu);
        return this;
    }

    public final zzgt zzfx() {
        return this.zzacq;
    }

    public final zzhc zza(zzgt zzgt) {
        this.zzacq = zzgt;
        return this;
    }

    public final zzhc zza(zzgw zzgw) {
        this.zzabw = zzgw;
        return this;
    }

    public final int zzfy() {
        return this.zzacn;
    }

    public final boolean zzfz() {
        return this.zzaco;
    }

    public final zzhc zzad(int i) {
        zzml.checkArgument(true);
        this.zzacs = 5000;
        return this;
    }

    public final zzhc zzae(int i) {
        zzml.checkArgument(true);
        this.zzact = 10000;
        return this;
    }

    public final zzgx zzga() {
        return this.zzack;
    }

    public final zzgx zzgb() {
        return this.zzacl;
    }

    public final zzhc zza(zzgy zzgy) {
        this.zzacj = zzgy;
        return this;
    }

    public final zzhi zzgc() {
        return this.zzacu;
    }

    public final zzhc zza(zzhi zzhi) {
        this.zzacu = zzhi;
        return this;
    }

    public final zzhc zza(zzjm zzjm) {
        this.zzaas = zzjm;
        return this;
    }

    public final zzjm zzgd() {
        return this.zzaas;
    }

    public final boolean zzge() {
        return this.zzacw;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v16, types: [com.google.android.gms.internal.firebase_ml.zzgv, com.google.android.gms.internal.firebase_ml.zzjq] */
    /* JADX WARN: Type inference failed for: r15v1, types: [com.google.android.gms.internal.firebase_ml.zzjk] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0280 A[Catch:{ all -> 0x02ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0284  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x02a8 A[LOOP:0: B:5:0x0019->B:122:0x02a8, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x028a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01d4  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0206  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0208  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0221 A[Catch:{ all -> 0x02ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0251 A[Catch:{ all -> 0x02ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x025a A[Catch:{ all -> 0x02ab }] */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final zzhd zzgf() throws IOException {
        StringBuilder sb;
        StringBuilder sb2;
        String userAgent;
        zzgt zzgt;
        int i;
        String str;
        zzhj zzgm;
        boolean z;
        boolean z2;
        boolean z3;
        String str2;
        String str3;
        long j;
        zzhc zzhc = this;
        zzml.checkArgument(zzhc.zzacm >= 0);
        int i2 = zzhc.zzacm;
        zzml.checkNotNull(zzhc.zzaax);
        zzml.checkNotNull(zzhc.zzacr);
        zzhd zzhd = null;
        while (true) {
            if (zzhd != null) {
                zzhd.ignore();
            }
            zzgy zzgy = zzhc.zzacj;
            if (zzgy != null) {
                zzgy.zzb(zzhc);
            }
            String zzft = zzhc.zzacr.zzft();
            zzhk zzc = zzhc.zzabf.zzc(zzhc.zzaax, zzft);
            Logger logger = zzhh.zzadf;
            boolean z4 = zzhc.zzaco && logger.isLoggable(Level.CONFIG);
            if (z4) {
                sb2 = new StringBuilder();
                sb2.append("-------------- REQUEST  --------------");
                sb2.append(zzjt.zzaig);
                sb2.append(zzhc.zzaax);
                sb2.append(' ');
                sb2.append(zzft);
                sb2.append(zzjt.zzaig);
                if (zzhc.zzacp) {
                    sb = new StringBuilder("curl -v --compressed");
                    if (!zzhc.zzaax.equals("GET")) {
                        sb.append(" -X ");
                        sb.append(zzhc.zzaax);
                    }
                    userAgent = zzhc.zzack.getUserAgent();
                    if (userAgent != null) {
                        zzhc.zzack.zzad("Google-HTTP-Java-Client/1.26.0-SNAPSHOT (gzip)");
                    } else {
                        zzgx zzgx = zzhc.zzack;
                        StringBuilder sb3 = new StringBuilder(String.valueOf(userAgent).length() + 47);
                        sb3.append(userAgent);
                        sb3.append(" Google-HTTP-Java-Client/1.26.0-SNAPSHOT (gzip)");
                        zzgx.zzad(sb3.toString());
                    }
                    zzgx.zza(zzhc.zzack, sb2, sb, logger, zzc);
                    zzhc.zzack.zzad(userAgent);
                    zzgt = zzhc.zzacq;
                    if (zzgt != null) {
                        zzgt.zzfr();
                    }
                    if (zzgt == null) {
                        String type = zzhc.zzacq.getType();
                        if (z4) {
                            zzgt = new zzjk(zzgt, zzhh.zzadf, Level.CONFIG, zzhc.zzacn);
                        }
                        zzgw zzgw = zzhc.zzabw;
                        if (zzgw == null) {
                            j = zzhc.zzacq.getLength();
                            str3 = null;
                        } else {
                            String name = zzgw.getName();
                            ?? zzgv = new zzgv(zzgt, zzhc.zzabw);
                            str3 = name;
                            zzgt = zzgv;
                            j = zzjh.zzb(zzgv);
                        }
                        if (z4) {
                            if (type != null) {
                                str = "GET";
                                String valueOf = String.valueOf(type);
                                String concat = valueOf.length() != 0 ? "Content-Type: ".concat(valueOf) : new String("Content-Type: ");
                                sb2.append(concat);
                                sb2.append(zzjt.zzaig);
                                if (sb != null) {
                                    i = i2;
                                    StringBuilder sb4 = new StringBuilder(String.valueOf(concat).length() + 6);
                                    sb4.append(" -H '");
                                    sb4.append(concat);
                                    sb4.append("'");
                                    sb.append(sb4.toString());
                                } else {
                                    i = i2;
                                }
                            } else {
                                i = i2;
                                str = "GET";
                            }
                            if (str3 != null) {
                                String valueOf2 = String.valueOf(str3);
                                String concat2 = valueOf2.length() != 0 ? "Content-Encoding: ".concat(valueOf2) : new String("Content-Encoding: ");
                                sb2.append(concat2);
                                sb2.append(zzjt.zzaig);
                                if (sb != null) {
                                    StringBuilder sb5 = new StringBuilder(String.valueOf(concat2).length() + 6);
                                    sb5.append(" -H '");
                                    sb5.append(concat2);
                                    sb5.append("'");
                                    sb.append(sb5.toString());
                                }
                            }
                            if (j >= 0) {
                                StringBuilder sb6 = new StringBuilder(36);
                                sb6.append("Content-Length: ");
                                sb6.append(j);
                                sb2.append(sb6.toString());
                                sb2.append(zzjt.zzaig);
                            }
                        } else {
                            i = i2;
                            str = "GET";
                        }
                        if (sb != null) {
                            sb.append(" -d '@-'");
                        }
                        zzc.setContentType(type);
                        zzc.setContentEncoding(str3);
                        zzc.setContentLength(j);
                        zzc.zza(zzgt);
                    } else {
                        i = i2;
                        str = "GET";
                    }
                    if (z4) {
                        logger.logp(Level.CONFIG, "com.google.api.client.http.HttpRequest", "execute", sb2.toString());
                        if (sb != null) {
                            sb.append(" -- '");
                            sb.append(zzft.replaceAll("'", "'\"'\"'"));
                            sb.append("'");
                            if (zzgt != null) {
                                sb.append(" << $$$");
                            }
                            logger.logp(Level.CONFIG, "com.google.api.client.http.HttpRequest", "execute", sb.toString());
                        }
                    }
                    boolean z5 = i <= 0;
                    zzhc = this;
                    zzc.zza(zzhc.zzacs, zzhc.zzact);
                    zzgm = zzc.zzgm();
                    try {
                        zzhd = new zzhd(zzhc, zzgm);
                        try {
                            if (zzhd.zzgg()) {
                                int statusCode = zzhd.getStatusCode();
                                String location = zzhd.zzga().getLocation();
                                if (zzhc.zzacv) {
                                    if (statusCode != 307) {
                                        switch (statusCode) {
                                            case 301:
                                            case 302:
                                            case 303:
                                                break;
                                            default:
                                                z3 = false;
                                                break;
                                        }
                                        if (z3 && location != null) {
                                            zzhc.zza(new zzgu(zzhc.zzacr.zzt(location)));
                                            if (statusCode != 303) {
                                                zzhc.zzaf(str);
                                                str2 = null;
                                                zzhc.zzacq = null;
                                            } else {
                                                str2 = null;
                                            }
                                            zzhc.zzack.zzx(str2);
                                            zzhc.zzack.zzz(str2);
                                            zzhc.zzack.zzaa(str2);
                                            zzhc.zzack.zzy(str2);
                                            zzhc.zzack.zzab(str2);
                                            zzhc.zzack.zzac(str2);
                                            z2 = true;
                                            z = z5 & z2;
                                            if (z) {
                                                zzhd.ignore();
                                            }
                                        }
                                    }
                                    z3 = true;
                                    zzhc.zza(new zzgu(zzhc.zzacr.zzt(location)));
                                    if (statusCode != 303) {
                                    }
                                    zzhc.zzack.zzx(str2);
                                    zzhc.zzack.zzz(str2);
                                    zzhc.zzack.zzaa(str2);
                                    zzhc.zzack.zzy(str2);
                                    zzhc.zzack.zzab(str2);
                                    zzhc.zzack.zzac(str2);
                                    z2 = true;
                                    z = z5 & z2;
                                    if (z) {
                                    }
                                }
                                z2 = false;
                                z = z5 & z2;
                                if (z) {
                                }
                            } else {
                                z = false;
                            }
                            int i3 = i - 1;
                            if (z) {
                                zzhi zzhi = zzhc.zzacu;
                                if (zzhi != null) {
                                    zzhi.zzb(zzhd);
                                }
                                if (!zzhc.zzacw || zzhd.zzgg()) {
                                    return zzhd;
                                }
                                try {
                                    throw new zzhg(zzhd);
                                } catch (Throwable th) {
                                    throw th;
                                }
                            } else {
                                i2 = i3;
                            }
                        } finally {
                            zzhd.disconnect();
                        }
                    } catch (Throwable th2) {
                        InputStream content = zzgm.getContent();
                        if (content != null) {
                            content.close();
                        }
                        throw th2;
                    }
                }
            } else {
                sb2 = null;
            }
            sb = null;
            userAgent = zzhc.zzack.getUserAgent();
            if (userAgent != null) {
            }
            zzgx.zza(zzhc.zzack, sb2, sb, logger, zzc);
            zzhc.zzack.zzad(userAgent);
            zzgt = zzhc.zzacq;
            if (zzgt != null) {
            }
            if (zzgt == null) {
            }
            if (z4) {
            }
            if (i <= 0) {
            }
            zzhc = this;
            zzc.zza(zzhc.zzacs, zzhc.zzact);
            try {
                zzgm = zzc.zzgm();
                zzhd = new zzhd(zzhc, zzgm);
                if (zzhd.zzgg()) {
                }
                int i32 = i - 1;
                if (z) {
                }
            } catch (IOException e) {
                throw e;
            }
        }
    }
}
