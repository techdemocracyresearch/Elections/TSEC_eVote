package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zzaau {
    static final /* synthetic */ int[] zzi;

    /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0037 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x000f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0017 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0027 */
    static {
        int[] iArr = new int[zzwz.zzg.values$50KLMJ33DTMIUPRFDTJMOP9FE1P6UT3FC9QMCBQ7CLN6ASJ1EHIM8JB5EDPM2PR59HKN8P949LIN8Q3FCHA6UIBEEPNMMP9R0().length];
        zzi = iArr;
        iArr[zzwz.zzg.zzclw - 1] = 1;
        zzi[zzwz.zzg.zzclx - 1] = 2;
        zzi[zzwz.zzg.zzclv - 1] = 3;
        zzi[zzwz.zzg.zzcly - 1] = 4;
        zzi[zzwz.zzg.zzclz - 1] = 5;
        zzi[zzwz.zzg.zzclt - 1] = 6;
        zzi[zzwz.zzg.zzclu - 1] = 7;
    }
}
