package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzlt {
    public static final int zzajp = 1;
    public static final int zzajq = 2;
    public static final int zzajr = 3;
    public static final int zzajs = 4;
    private static final /* synthetic */ int[] zzajt = {1, 2, 3, 4};

    public static int[] zzje() {
        return (int[]) zzajt.clone();
    }
}
