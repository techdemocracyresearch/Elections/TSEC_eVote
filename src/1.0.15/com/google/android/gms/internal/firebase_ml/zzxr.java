package com.google.android.gms.internal.firebase_ml;

import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzxr<K> implements Map.Entry<K, Object> {
    private Map.Entry<K, zzxp> zzcna;

    private zzxr(Map.Entry<K, zzxp> entry) {
        this.zzcna = entry;
    }

    @Override // java.util.Map.Entry
    public final K getKey() {
        return this.zzcna.getKey();
    }

    @Override // java.util.Map.Entry
    public final Object getValue() {
        if (this.zzcna.getValue() == null) {
            return null;
        }
        return zzxp.zzvl();
    }

    public final zzxp zzvm() {
        return this.zzcna.getValue();
    }

    @Override // java.util.Map.Entry
    public final Object setValue(Object obj) {
        if (obj instanceof zzyk) {
            return this.zzcna.getValue().zzi((zzyk) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
