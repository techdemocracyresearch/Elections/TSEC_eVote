package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzvn implements zzyt {
    private boolean zzchf = true;
    private int zzchg = -1;

    @Override // com.google.android.gms.internal.firebase_ml.zzyt
    /* renamed from: zzti */
    public final zzyt clone() {
        throw new UnsupportedOperationException("clone() should be implemented by subclasses.");
    }
}
