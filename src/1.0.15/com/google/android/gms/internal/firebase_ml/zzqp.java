package com.google.android.gms.internal.firebase_ml;

import com.google.firebase.ml.common.FirebaseMLException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzqp {
    void release();

    void zzol() throws FirebaseMLException;
}
