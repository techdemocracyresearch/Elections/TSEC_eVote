package com.google.android.gms.internal.firebase_ml;

import android.content.Context;
import com.google.firebase.FirebaseApp;
import com.google.firebase.components.Component;
import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.Dependency;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzqf {
    public static final Component<zzqf> zzbja = Component.builder(zzqf.class).add(Dependency.required(FirebaseApp.class)).factory(zzqe.zzbil).build();
    private final FirebaseApp zzbjd;

    private zzqf(FirebaseApp firebaseApp) {
        this.zzbjd = firebaseApp;
    }

    public final <T> T get(Class<T> cls) {
        return (T) this.zzbjd.get(cls);
    }

    public static zzqf zzog() {
        return (zzqf) FirebaseApp.getInstance().get(zzqf.class);
    }

    public final Context getApplicationContext() {
        return this.zzbjd.getApplicationContext();
    }

    public final FirebaseApp zzoh() {
        return this.zzbjd;
    }

    public final String getPersistenceKey() {
        return this.zzbjd.getPersistenceKey();
    }

    static final /* synthetic */ zzqf zzb(ComponentContainer componentContainer) {
        return new zzqf((FirebaseApp) componentContainer.get(FirebaseApp.class));
    }
}
