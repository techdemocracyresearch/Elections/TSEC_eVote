package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzkc extends zzgm {
    zzkc(zzkf zzkf) {
        super(zzkf);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzge
    public final void zza(zzgg<?> zzgg) throws IOException {
        super.zza(zzgg);
    }

    static {
        boolean z = zzgc.zzaaj.intValue() == 1 && zzgc.zzaak.intValue() >= 15;
        Object[] objArr = {zzgc.VERSION};
        if (!z) {
            throw new IllegalStateException(zzms.zzb("You are currently running with version %s of google-api-client. You need at least version 1.15 of google-api-client to run version 1.25.0-SNAPSHOT of the Cloud Vision API library.", objArr));
        }
    }
}
