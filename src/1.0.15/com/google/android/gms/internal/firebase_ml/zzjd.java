package com.google.android.gms.internal.firebase_ml;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.WeakHashMap;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzjd {
    private static final Map<Field, zzjd> zzahn = new WeakHashMap();
    private final String name;
    private final boolean zzaho;
    private final Field zzahp;

    public static zzjd zza(Enum<?> r5) {
        try {
            zzjd zza = zza(r5.getClass().getField(r5.name()));
            Object[] objArr = {r5};
            if (zza != null) {
                return zza;
            }
            throw new IllegalArgumentException(zzms.zzb("enum constant missing @Value or @NullValue annotation: %s", objArr));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    public static zzjd zza(Field field) {
        String str = null;
        if (field == null) {
            return null;
        }
        Map<Field, zzjd> map = zzahn;
        synchronized (map) {
            zzjd zzjd = map.get(field);
            boolean isEnumConstant = field.isEnumConstant();
            if (zzjd == null && (isEnumConstant || !Modifier.isStatic(field.getModifiers()))) {
                if (isEnumConstant) {
                    zzjx zzjx = (zzjx) field.getAnnotation(zzjx.class);
                    if (zzjx != null) {
                        str = zzjx.value();
                    } else if (((zzjn) field.getAnnotation(zzjn.class)) == null) {
                        return null;
                    }
                } else {
                    zzjg zzjg = (zzjg) field.getAnnotation(zzjg.class);
                    if (zzjg == null) {
                        return null;
                    }
                    str = zzjg.value();
                    field.setAccessible(true);
                }
                if ("##default".equals(str)) {
                    str = field.getName();
                }
                zzjd = new zzjd(field, str);
                map.put(field, zzjd);
            }
            return zzjd;
        }
    }

    private zzjd(Field field, String str) {
        String str2;
        this.zzahp = field;
        if (str == null) {
            str2 = null;
        } else {
            str2 = str.intern();
        }
        this.name = str2;
        this.zzaho = zzix.zza(field.getType());
    }

    public final Field zzia() {
        return this.zzahp;
    }

    public final String getName() {
        return this.name;
    }

    public final Type getGenericType() {
        return this.zzahp.getGenericType();
    }

    public final boolean zzib() {
        return Modifier.isFinal(this.zzahp.getModifiers());
    }

    public final boolean isPrimitive() {
        return this.zzaho;
    }

    public final Object zzh(Object obj) {
        return zza(this.zzahp, obj);
    }

    public final void zzb(Object obj, Object obj2) {
        zza(this.zzahp, obj, obj2);
    }

    public final <T extends Enum<T>> T zzic() {
        return (T) Enum.valueOf(this.zzahp.getDeclaringClass(), this.zzahp.getName());
    }

    private static Object zza(Field field, Object obj) {
        try {
            return field.get(obj);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void zza(Field field, Object obj, Object obj2) {
        if (Modifier.isFinal(field.getModifiers())) {
            Object zza = zza(field, obj);
            if (obj2 == null) {
                if (zza == null) {
                    return;
                }
            } else if (obj2.equals(zza)) {
                return;
            }
            String valueOf = String.valueOf(zza);
            String valueOf2 = String.valueOf(obj2);
            String name2 = field.getName();
            String name3 = obj.getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48 + String.valueOf(valueOf2).length() + String.valueOf(name2).length() + String.valueOf(name3).length());
            sb.append("expected final value <");
            sb.append(valueOf);
            sb.append("> but was <");
            sb.append(valueOf2);
            sb.append("> on ");
            sb.append(name2);
            sb.append(" field in ");
            sb.append(name3);
            throw new IllegalArgumentException(sb.toString());
        }
        try {
            field.set(obj, obj2);
        } catch (SecurityException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e2) {
            throw new IllegalArgumentException(e2);
        }
    }
}
