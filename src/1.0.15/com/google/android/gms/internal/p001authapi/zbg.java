package com.google.android.gms.internal.p001authapi;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

/* access modifiers changed from: package-private */
/* renamed from: com.google.android.gms.internal.auth-api.zbg  reason: invalid package */
/* compiled from: com.google.android.gms:play-services-auth@@19.2.0 */
public final class zbg extends zbm<CredentialRequestResult> {
    final /* synthetic */ CredentialRequest zba;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zbg(zbl zbl, GoogleApiClient googleApiClient, CredentialRequest credentialRequest) {
        super(googleApiClient);
        this.zba = credentialRequest;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* bridge */ /* synthetic */ Result createFailedResult(Status status) {
        return new zbe(status, null);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.p001authapi.zbm
    public final void zba(Context context, zbt zbt) throws RemoteException {
        zbt.zbd(new zbf(this), this.zba);
    }
}
