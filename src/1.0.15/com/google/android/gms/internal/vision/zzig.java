package com.google.android.gms.internal.vision;

import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate;
import com.google.android.gms.internal.vision.zzgs;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import sun.misc.Unsafe;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzig<T> implements zzir<T> {
    private static final int[] zzyy = new int[0];
    private static final Unsafe zzyz = zzjp.zzil();
    private final int[] zzza;
    private final Object[] zzzb;
    private final int zzzc;
    private final int zzzd;
    private final zzic zzze;
    private final boolean zzzf;
    private final boolean zzzg;
    private final boolean zzzh;
    private final boolean zzzi;
    private final int[] zzzj;
    private final int zzzk;
    private final int zzzl;
    private final zzik zzzm;
    private final zzhm zzzn;
    private final zzjj<?, ?> zzzo;
    private final zzgf<?> zzzp;
    private final zzhv zzzq;

    private zzig(int[] iArr, Object[] objArr, int i, int i2, zzic zzic, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzik zzik, zzhm zzhm, zzjj<?, ?> zzjj, zzgf<?> zzgf, zzhv zzhv) {
        this.zzza = iArr;
        this.zzzb = objArr;
        this.zzzc = i;
        this.zzzd = i2;
        this.zzzg = zzic instanceof zzgs;
        this.zzzh = z;
        this.zzzf = zzgf != null && zzgf.zze(zzic);
        this.zzzi = false;
        this.zzzj = iArr2;
        this.zzzk = i3;
        this.zzzl = i4;
        this.zzzm = zzik;
        this.zzzn = zzhm;
        this.zzzo = zzjj;
        this.zzzp = zzgf;
        this.zzze = zzic;
        this.zzzq = zzhv;
    }

    private static boolean zzbs(int i) {
        return (i & 536870912) != 0;
    }

    static <T> zzig<T> zza(Class<T> cls, zzia zzia, zzik zzik, zzhm zzhm, zzjj<?, ?> zzjj, zzgf<?> zzgf, zzhv zzhv) {
        int i;
        int i2;
        int[] iArr;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        String str;
        Object[] objArr;
        int i12;
        int i13;
        int i14;
        int i15;
        boolean z;
        int i16;
        Field field;
        int i17;
        char charAt;
        int i18;
        int i19;
        Field field2;
        Field field3;
        int i20;
        char charAt2;
        int i21;
        char charAt3;
        int i22;
        char charAt4;
        int i23;
        char charAt5;
        int i24;
        char charAt6;
        int i25;
        char charAt7;
        int i26;
        char charAt8;
        int i27;
        char charAt9;
        int i28;
        char charAt10;
        int i29;
        char charAt11;
        int i30;
        char charAt12;
        int i31;
        char charAt13;
        if (zzia instanceof zzip) {
            zzip zzip = (zzip) zzia;
            int i32 = 0;
            boolean z2 = zzip.zzhi() == zzgs.zzf.zzxa;
            String zzhp = zzip.zzhp();
            int length = zzhp.length();
            if (zzhp.charAt(0) >= 55296) {
                int i33 = 1;
                while (true) {
                    i = i33 + 1;
                    if (zzhp.charAt(i33) < 55296) {
                        break;
                    }
                    i33 = i;
                }
            } else {
                i = 1;
            }
            int i34 = i + 1;
            int charAt14 = zzhp.charAt(i);
            if (charAt14 >= 55296) {
                int i35 = charAt14 & 8191;
                int i36 = 13;
                while (true) {
                    i31 = i34 + 1;
                    charAt13 = zzhp.charAt(i34);
                    if (charAt13 < 55296) {
                        break;
                    }
                    i35 |= (charAt13 & 8191) << i36;
                    i36 += 13;
                    i34 = i31;
                }
                charAt14 = i35 | (charAt13 << i36);
                i34 = i31;
            }
            if (charAt14 == 0) {
                iArr = zzyy;
                i7 = 0;
                i6 = 0;
                i5 = 0;
                i4 = 0;
                i3 = 0;
                i2 = 0;
            } else {
                int i37 = i34 + 1;
                int charAt15 = zzhp.charAt(i34);
                if (charAt15 >= 55296) {
                    int i38 = charAt15 & 8191;
                    int i39 = 13;
                    while (true) {
                        i30 = i37 + 1;
                        charAt12 = zzhp.charAt(i37);
                        if (charAt12 < 55296) {
                            break;
                        }
                        i38 |= (charAt12 & 8191) << i39;
                        i39 += 13;
                        i37 = i30;
                    }
                    charAt15 = i38 | (charAt12 << i39);
                    i37 = i30;
                }
                int i40 = i37 + 1;
                int charAt16 = zzhp.charAt(i37);
                if (charAt16 >= 55296) {
                    int i41 = charAt16 & 8191;
                    int i42 = 13;
                    while (true) {
                        i29 = i40 + 1;
                        charAt11 = zzhp.charAt(i40);
                        if (charAt11 < 55296) {
                            break;
                        }
                        i41 |= (charAt11 & 8191) << i42;
                        i42 += 13;
                        i40 = i29;
                    }
                    charAt16 = i41 | (charAt11 << i42);
                    i40 = i29;
                }
                int i43 = i40 + 1;
                i6 = zzhp.charAt(i40);
                if (i6 >= 55296) {
                    int i44 = i6 & 8191;
                    int i45 = 13;
                    while (true) {
                        i28 = i43 + 1;
                        charAt10 = zzhp.charAt(i43);
                        if (charAt10 < 55296) {
                            break;
                        }
                        i44 |= (charAt10 & 8191) << i45;
                        i45 += 13;
                        i43 = i28;
                    }
                    i6 = i44 | (charAt10 << i45);
                    i43 = i28;
                }
                int i46 = i43 + 1;
                i5 = zzhp.charAt(i43);
                if (i5 >= 55296) {
                    int i47 = i5 & 8191;
                    int i48 = 13;
                    while (true) {
                        i27 = i46 + 1;
                        charAt9 = zzhp.charAt(i46);
                        if (charAt9 < 55296) {
                            break;
                        }
                        i47 |= (charAt9 & 8191) << i48;
                        i48 += 13;
                        i46 = i27;
                    }
                    i5 = i47 | (charAt9 << i48);
                    i46 = i27;
                }
                int i49 = i46 + 1;
                i4 = zzhp.charAt(i46);
                if (i4 >= 55296) {
                    int i50 = i4 & 8191;
                    int i51 = 13;
                    while (true) {
                        i26 = i49 + 1;
                        charAt8 = zzhp.charAt(i49);
                        if (charAt8 < 55296) {
                            break;
                        }
                        i50 |= (charAt8 & 8191) << i51;
                        i51 += 13;
                        i49 = i26;
                    }
                    i4 = i50 | (charAt8 << i51);
                    i49 = i26;
                }
                int i52 = i49 + 1;
                i3 = zzhp.charAt(i49);
                if (i3 >= 55296) {
                    int i53 = i3 & 8191;
                    int i54 = 13;
                    while (true) {
                        i25 = i52 + 1;
                        charAt7 = zzhp.charAt(i52);
                        if (charAt7 < 55296) {
                            break;
                        }
                        i53 |= (charAt7 & 8191) << i54;
                        i54 += 13;
                        i52 = i25;
                    }
                    i3 = i53 | (charAt7 << i54);
                    i52 = i25;
                }
                int i55 = i52 + 1;
                int charAt17 = zzhp.charAt(i52);
                if (charAt17 >= 55296) {
                    int i56 = charAt17 & 8191;
                    int i57 = 13;
                    while (true) {
                        i24 = i55 + 1;
                        charAt6 = zzhp.charAt(i55);
                        if (charAt6 < 55296) {
                            break;
                        }
                        i56 |= (charAt6 & 8191) << i57;
                        i57 += 13;
                        i55 = i24;
                    }
                    charAt17 = i56 | (charAt6 << i57);
                    i55 = i24;
                }
                int i58 = i55 + 1;
                i2 = zzhp.charAt(i55);
                if (i2 >= 55296) {
                    int i59 = i2 & 8191;
                    int i60 = i58;
                    int i61 = 13;
                    while (true) {
                        i23 = i60 + 1;
                        charAt5 = zzhp.charAt(i60);
                        if (charAt5 < 55296) {
                            break;
                        }
                        i59 |= (charAt5 & 8191) << i61;
                        i61 += 13;
                        i60 = i23;
                    }
                    i2 = i59 | (charAt5 << i61);
                    i58 = i23;
                }
                i7 = (charAt15 << 1) + charAt16;
                iArr = new int[(i2 + i3 + charAt17)];
                i32 = charAt15;
                i34 = i58;
            }
            Unsafe unsafe = zzyz;
            Object[] zzhq = zzip.zzhq();
            Class<?> cls2 = zzip.zzhk().getClass();
            int[] iArr2 = new int[(i4 * 3)];
            Object[] objArr2 = new Object[(i4 << 1)];
            int i62 = i2 + i3;
            int i63 = i7;
            int i64 = i2;
            int i65 = i34;
            int i66 = i62;
            int i67 = 0;
            int i68 = 0;
            while (i65 < length) {
                int i69 = i65 + 1;
                int charAt18 = zzhp.charAt(i65);
                if (charAt18 >= 55296) {
                    int i70 = charAt18 & 8191;
                    int i71 = i69;
                    int i72 = 13;
                    while (true) {
                        i22 = i71 + 1;
                        charAt4 = zzhp.charAt(i71);
                        i8 = length;
                        if (charAt4 < 55296) {
                            break;
                        }
                        i70 |= (charAt4 & 8191) << i72;
                        i72 += 13;
                        i71 = i22;
                        length = i8;
                    }
                    charAt18 = i70 | (charAt4 << i72);
                    i9 = i22;
                } else {
                    i8 = length;
                    i9 = i69;
                }
                int i73 = i9 + 1;
                int charAt19 = zzhp.charAt(i9);
                if (charAt19 >= 55296) {
                    int i74 = charAt19 & 8191;
                    int i75 = i73;
                    int i76 = 13;
                    while (true) {
                        i21 = i75 + 1;
                        charAt3 = zzhp.charAt(i75);
                        i10 = i2;
                        if (charAt3 < 55296) {
                            break;
                        }
                        i74 |= (charAt3 & 8191) << i76;
                        i76 += 13;
                        i75 = i21;
                        i2 = i10;
                    }
                    charAt19 = i74 | (charAt3 << i76);
                    i11 = i21;
                } else {
                    i10 = i2;
                    i11 = i73;
                }
                int i77 = charAt19 & 255;
                if ((charAt19 & 1024) != 0) {
                    iArr[i67] = i68;
                    i67++;
                }
                if (i77 >= 51) {
                    int i78 = i11 + 1;
                    int charAt20 = zzhp.charAt(i11);
                    char c = 55296;
                    if (charAt20 >= 55296) {
                        int i79 = charAt20 & 8191;
                        int i80 = 13;
                        while (true) {
                            i20 = i78 + 1;
                            charAt2 = zzhp.charAt(i78);
                            if (charAt2 < c) {
                                break;
                            }
                            i79 |= (charAt2 & 8191) << i80;
                            i80 += 13;
                            i78 = i20;
                            c = 55296;
                        }
                        charAt20 = i79 | (charAt2 << i80);
                        i78 = i20;
                    }
                    int i81 = i77 - 51;
                    if (i81 == 9 || i81 == 17) {
                        i19 = 1;
                        objArr2[((i68 / 3) << 1) + 1] = zzhq[i63];
                        i63++;
                    } else {
                        if (i81 == 12 && !z2) {
                            objArr2[((i68 / 3) << 1) + 1] = zzhq[i63];
                            i63++;
                        }
                        i19 = 1;
                    }
                    int i82 = charAt20 << i19;
                    Object obj = zzhq[i82];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = zza(cls2, (String) obj);
                        zzhq[i82] = field2;
                    }
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i83 = i82 + 1;
                    Object obj2 = zzhq[i83];
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = zza(cls2, (String) obj2);
                        zzhq[i83] = field3;
                    }
                    str = zzhp;
                    i14 = (int) unsafe.objectFieldOffset(field3);
                    z = z2;
                    objArr = objArr2;
                    i13 = objectFieldOffset;
                    i12 = i78;
                    i15 = 0;
                } else {
                    int i84 = i63 + 1;
                    Field zza = zza(cls2, (String) zzhq[i63]);
                    if (i77 == 9 || i77 == 17) {
                        objArr2[((i68 / 3) << 1) + 1] = zza.getType();
                    } else {
                        if (i77 == 27 || i77 == 49) {
                            i18 = i84 + 1;
                            objArr2[((i68 / 3) << 1) + 1] = zzhq[i84];
                        } else if (i77 == 12 || i77 == 30 || i77 == 44) {
                            if (!z2) {
                                i18 = i84 + 1;
                                objArr2[((i68 / 3) << 1) + 1] = zzhq[i84];
                            }
                        } else if (i77 == 50) {
                            int i85 = i64 + 1;
                            iArr[i64] = i68;
                            int i86 = (i68 / 3) << 1;
                            i18 = i84 + 1;
                            objArr2[i86] = zzhq[i84];
                            if ((charAt19 & 2048) != 0) {
                                i84 = i18 + 1;
                                objArr2[i86 + 1] = zzhq[i18];
                                i64 = i85;
                            } else {
                                i64 = i85;
                            }
                        }
                        i16 = i18;
                        i13 = (int) unsafe.objectFieldOffset(zza);
                        if ((charAt19 & 4096) == 4096 || i77 > 17) {
                            str = zzhp;
                            z = z2;
                            objArr = objArr2;
                            i14 = 1048575;
                            i12 = i11;
                            i15 = 0;
                        } else {
                            int i87 = i11 + 1;
                            int charAt21 = zzhp.charAt(i11);
                            if (charAt21 >= 55296) {
                                int i88 = charAt21 & 8191;
                                int i89 = 13;
                                while (true) {
                                    i17 = i87 + 1;
                                    charAt = zzhp.charAt(i87);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    i88 |= (charAt & 8191) << i89;
                                    i89 += 13;
                                    i87 = i17;
                                }
                                charAt21 = i88 | (charAt << i89);
                                i87 = i17;
                            }
                            int i90 = (i32 << 1) + (charAt21 / 32);
                            Object obj3 = zzhq[i90];
                            str = zzhp;
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = zza(cls2, (String) obj3);
                                zzhq[i90] = field;
                            }
                            z = z2;
                            objArr = objArr2;
                            i15 = charAt21 % 32;
                            i12 = i87;
                            i14 = (int) unsafe.objectFieldOffset(field);
                        }
                        if (i77 >= 18 && i77 <= 49) {
                            iArr[i66] = i13;
                            i66++;
                        }
                        i63 = i16;
                    }
                    i16 = i84;
                    i13 = (int) unsafe.objectFieldOffset(zza);
                    if ((charAt19 & 4096) == 4096) {
                    }
                    str = zzhp;
                    z = z2;
                    objArr = objArr2;
                    i14 = 1048575;
                    i12 = i11;
                    i15 = 0;
                    iArr[i66] = i13;
                    i66++;
                    i63 = i16;
                }
                int i91 = i68 + 1;
                iArr2[i68] = charAt18;
                int i92 = i91 + 1;
                iArr2[i91] = ((charAt19 & 256) != 0 ? 268435456 : 0) | ((charAt19 & 512) != 0 ? 536870912 : 0) | (i77 << 20) | i13;
                int i93 = i92 + 1;
                iArr2[i92] = (i15 << 20) | i14;
                i65 = i12;
                i32 = i32;
                i5 = i5;
                objArr2 = objArr;
                i2 = i10;
                i6 = i6;
                z2 = z;
                i68 = i93;
                length = i8;
                zzhp = str;
            }
            return new zzig<>(iArr2, objArr2, i6, i5, zzip.zzhk(), z2, false, iArr, i2, i62, zzik, zzhm, zzjj, zzgf, zzhv);
        }
        ((zzjg) zzia).zzhi();
        int i94 = zzgs.zzf.zzxa;
        throw new NoSuchMethodError();
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    @Override // com.google.android.gms.internal.vision.zzir
    public final T newInstance() {
        return (T) this.zzzm.newInstance(this.zzze);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.vision.zzit.zze(com.google.android.gms.internal.vision.zzjp.zzp(r10, r6), com.google.android.gms.internal.vision.zzjp.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzl(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzk(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzl(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzk(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzk(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzk(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.vision.zzit.zze(com.google.android.gms.internal.vision.zzjp.zzp(r10, r6), com.google.android.gms.internal.vision.zzjp.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.vision.zzit.zze(com.google.android.gms.internal.vision.zzjp.zzp(r10, r6), com.google.android.gms.internal.vision.zzjp.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.vision.zzit.zze(com.google.android.gms.internal.vision.zzjp.zzp(r10, r6), com.google.android.gms.internal.vision.zzjp.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzm(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzm(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzk(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzl(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzk(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzl(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.vision.zzjp.zzl(r10, r6) == com.google.android.gms.internal.vision.zzjp.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.vision.zzjp.zzn(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.vision.zzjp.zzn(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.vision.zzjp.zzo(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.vision.zzjp.zzo(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.vision.zzit.zze(com.google.android.gms.internal.vision.zzjp.zzp(r10, r6), com.google.android.gms.internal.vision.zzjp.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    @Override // com.google.android.gms.internal.vision.zzir
    public final boolean equals(T t, T t2) {
        int length = this.zzza.length;
        int i = 0;
        while (true) {
            boolean z = true;
            if (i < length) {
                int zzbq = zzbq(i);
                long j = (long) (zzbq & 1048575);
                switch ((zzbq & 267386880) >>> 20) {
                    case 0:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 1:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 2:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 3:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 4:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 5:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 6:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 7:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 8:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 9:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 10:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 11:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 12:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 13:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 14:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 15:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 16:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 17:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                        z = false;
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        z = zzit.zze(zzjp.zzp(t, j), zzjp.zzp(t2, j));
                        break;
                    case 50:
                        z = zzit.zze(zzjp.zzp(t, j), zzjp.zzp(t2, j));
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68:
                        long zzbr = (long) (zzbr(i) & 1048575);
                        if (zzjp.zzk(t, zzbr) == zzjp.zzk(t2, zzbr)) {
                            break;
                        }
                        z = false;
                        break;
                }
                if (!z) {
                    return false;
                }
                i += 3;
            } else if (!this.zzzo.zzw(t).equals(this.zzzo.zzw(t2))) {
                return false;
            } else {
                if (this.zzzf) {
                    return this.zzzp.zzf(t).equals(this.zzzp.zzf(t2));
                }
                return true;
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzir
    public final int hashCode(T t) {
        int i;
        int i2;
        int length = this.zzza.length;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4 += 3) {
            int zzbq = zzbq(i4);
            int i5 = this.zzza[i4];
            long j = (long) (1048575 & zzbq);
            int i6 = 37;
            switch ((zzbq & 267386880) >>> 20) {
                case 0:
                    i2 = i3 * 53;
                    i = zzgt.zzab(Double.doubleToLongBits(zzjp.zzo(t, j)));
                    i3 = i2 + i;
                    break;
                case 1:
                    i2 = i3 * 53;
                    i = Float.floatToIntBits(zzjp.zzn(t, j));
                    i3 = i2 + i;
                    break;
                case 2:
                    i2 = i3 * 53;
                    i = zzgt.zzab(zzjp.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 3:
                    i2 = i3 * 53;
                    i = zzgt.zzab(zzjp.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 4:
                    i2 = i3 * 53;
                    i = zzjp.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 5:
                    i2 = i3 * 53;
                    i = zzgt.zzab(zzjp.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 6:
                    i2 = i3 * 53;
                    i = zzjp.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 7:
                    i2 = i3 * 53;
                    i = zzgt.zzm(zzjp.zzm(t, j));
                    i3 = i2 + i;
                    break;
                case 8:
                    i2 = i3 * 53;
                    i = ((String) zzjp.zzp(t, j)).hashCode();
                    i3 = i2 + i;
                    break;
                case 9:
                    Object zzp = zzjp.zzp(t, j);
                    if (zzp != null) {
                        i6 = zzp.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 10:
                    i2 = i3 * 53;
                    i = zzjp.zzp(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 11:
                    i2 = i3 * 53;
                    i = zzjp.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 12:
                    i2 = i3 * 53;
                    i = zzjp.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 13:
                    i2 = i3 * 53;
                    i = zzjp.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 14:
                    i2 = i3 * 53;
                    i = zzgt.zzab(zzjp.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 15:
                    i2 = i3 * 53;
                    i = zzjp.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 16:
                    i2 = i3 * 53;
                    i = zzgt.zzab(zzjp.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 17:
                    Object zzp2 = zzjp.zzp(t, j);
                    if (zzp2 != null) {
                        i6 = zzp2.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i2 = i3 * 53;
                    i = zzjp.zzp(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 50:
                    i2 = i3 * 53;
                    i = zzjp.zzp(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 51:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzgt.zzab(Double.doubleToLongBits(zzf(t, j)));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = Float.floatToIntBits(zzg(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzgt.zzab(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzgt.zzab(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzgt.zzab(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzgt.zzm(zzj(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = ((String) zzjp.zzp(t, j)).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 60:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjp.zzp(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjp.zzp(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 65:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzgt.zzab(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzgt.zzab(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (zza(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjp.zzp(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
            }
        }
        int hashCode = (i3 * 53) + this.zzzo.zzw(t).hashCode();
        return this.zzzf ? (hashCode * 53) + this.zzzp.zzf(t).hashCode() : hashCode;
    }

    @Override // com.google.android.gms.internal.vision.zzir
    public final void zzd(T t, T t2) {
        Objects.requireNonNull(t2);
        for (int i = 0; i < this.zzza.length; i += 3) {
            int zzbq = zzbq(i);
            long j = (long) (1048575 & zzbq);
            int i2 = this.zzza[i];
            switch ((zzbq & 267386880) >>> 20) {
                case 0:
                    if (zza(t2, i)) {
                        zzjp.zza(t, j, zzjp.zzo(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 1:
                    if (zza(t2, i)) {
                        zzjp.zza((Object) t, j, zzjp.zzn(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 2:
                    if (zza(t2, i)) {
                        zzjp.zza((Object) t, j, zzjp.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 3:
                    if (zza(t2, i)) {
                        zzjp.zza((Object) t, j, zzjp.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 4:
                    if (zza(t2, i)) {
                        zzjp.zzb(t, j, zzjp.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 5:
                    if (zza(t2, i)) {
                        zzjp.zza((Object) t, j, zzjp.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 6:
                    if (zza(t2, i)) {
                        zzjp.zzb(t, j, zzjp.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 7:
                    if (zza(t2, i)) {
                        zzjp.zza(t, j, zzjp.zzm(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 8:
                    if (zza(t2, i)) {
                        zzjp.zza(t, j, zzjp.zzp(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 9:
                    zza(t, t2, i);
                    break;
                case 10:
                    if (zza(t2, i)) {
                        zzjp.zza(t, j, zzjp.zzp(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 11:
                    if (zza(t2, i)) {
                        zzjp.zzb(t, j, zzjp.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 12:
                    if (zza(t2, i)) {
                        zzjp.zzb(t, j, zzjp.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 13:
                    if (zza(t2, i)) {
                        zzjp.zzb(t, j, zzjp.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 14:
                    if (zza(t2, i)) {
                        zzjp.zza((Object) t, j, zzjp.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 15:
                    if (zza(t2, i)) {
                        zzjp.zzb(t, j, zzjp.zzk(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 16:
                    if (zza(t2, i)) {
                        zzjp.zza((Object) t, j, zzjp.zzl(t2, j));
                        zzb(t, i);
                        break;
                    } else {
                        break;
                    }
                case 17:
                    zza(t, t2, i);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.zzzn.zza(t, t2, j);
                    break;
                case 50:
                    zzit.zza(this.zzzq, t, t2, j);
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (zza(t2, i2, i)) {
                        zzjp.zza(t, j, zzjp.zzp(t2, j));
                        zzb(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 60:
                    zzb(t, t2, i);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                    if (zza(t2, i2, i)) {
                        zzjp.zza(t, j, zzjp.zzp(t2, j));
                        zzb(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 68:
                    zzb(t, t2, i);
                    break;
            }
        }
        zzit.zza(this.zzzo, t, t2);
        if (this.zzzf) {
            zzit.zza(this.zzzp, t, t2);
        }
    }

    private final void zza(T t, T t2, int i) {
        long zzbq = (long) (zzbq(i) & 1048575);
        if (zza(t2, i)) {
            Object zzp = zzjp.zzp(t, zzbq);
            Object zzp2 = zzjp.zzp(t2, zzbq);
            if (zzp != null && zzp2 != null) {
                zzjp.zza(t, zzbq, zzgt.zzb(zzp, zzp2));
                zzb(t, i);
            } else if (zzp2 != null) {
                zzjp.zza(t, zzbq, zzp2);
                zzb(t, i);
            }
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzbq = zzbq(i);
        int i2 = this.zzza[i];
        long j = (long) (zzbq & 1048575);
        if (zza(t2, i2, i)) {
            Object zzp = zzjp.zzp(t, j);
            Object zzp2 = zzjp.zzp(t2, j);
            if (zzp != null && zzp2 != null) {
                zzjp.zza(t, j, zzgt.zzb(zzp, zzp2));
                zzb(t, i2, i);
            } else if (zzp2 != null) {
                zzjp.zza(t, j, zzp2);
                zzb(t, i2, i);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // com.google.android.gms.internal.vision.zzir
    public final int zzs(T t) {
        int i;
        int i2;
        long j;
        int i3;
        int zzb;
        int i4;
        int i5;
        int i6;
        int i7;
        int zzb2;
        int i8;
        int i9;
        int i10;
        int i11 = 267386880;
        if (this.zzzh) {
            Unsafe unsafe = zzyz;
            int i12 = 0;
            int i13 = 0;
            while (i12 < this.zzza.length) {
                int zzbq = zzbq(i12);
                int i14 = (zzbq & i11) >>> 20;
                int i15 = this.zzza[i12];
                long j2 = (long) (zzbq & 1048575);
                int i16 = (i14 < zzgn.DOUBLE_LIST_PACKED.id() || i14 > zzgn.SINT64_LIST_PACKED.id()) ? 0 : this.zzza[i12 + 2] & 1048575;
                switch (i14) {
                    case 0:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzb(i15, 0.0d);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 1:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzb(i15, 0.0f);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 2:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzd(i15, zzjp.zzl(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 3:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zze(i15, zzjp.zzl(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 4:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzk(i15, zzjp.zzk(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 5:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzg(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 6:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzn(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 7:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzb(i15, true);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 8:
                        if (zza(t, i12)) {
                            Object zzp = zzjp.zzp(t, j2);
                            if (zzp instanceof zzfh) {
                                zzb2 = zzga.zzc(i15, (zzfh) zzp);
                                break;
                            } else {
                                zzb2 = zzga.zzb(i15, (String) zzp);
                                break;
                            }
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 9:
                        if (zza(t, i12)) {
                            zzb2 = zzit.zzc(i15, zzjp.zzp(t, j2), zzbn(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 10:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzc(i15, (zzfh) zzjp.zzp(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 11:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzl(i15, zzjp.zzk(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 12:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzp(i15, zzjp.zzk(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 13:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzo(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 14:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzh(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 15:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzm(i15, zzjp.zzk(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 16:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzf(i15, zzjp.zzl(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 17:
                        if (zza(t, i12)) {
                            zzb2 = zzga.zzc(i15, (zzic) zzjp.zzp(t, j2), zzbn(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 18:
                        zzb2 = zzit.zzw(i15, zze(t, j2), false);
                        break;
                    case 19:
                        zzb2 = zzit.zzv(i15, zze(t, j2), false);
                        break;
                    case 20:
                        zzb2 = zzit.zzo(i15, zze(t, j2), false);
                        break;
                    case 21:
                        zzb2 = zzit.zzp(i15, zze(t, j2), false);
                        break;
                    case 22:
                        zzb2 = zzit.zzs(i15, zze(t, j2), false);
                        break;
                    case 23:
                        zzb2 = zzit.zzw(i15, zze(t, j2), false);
                        break;
                    case 24:
                        zzb2 = zzit.zzv(i15, zze(t, j2), false);
                        break;
                    case 25:
                        zzb2 = zzit.zzx(i15, zze(t, j2), false);
                        break;
                    case 26:
                        zzb2 = zzit.zzc(i15, zze(t, j2));
                        break;
                    case 27:
                        zzb2 = zzit.zzc(i15, zze(t, j2), zzbn(i12));
                        break;
                    case 28:
                        zzb2 = zzit.zzd(i15, zze(t, j2));
                        break;
                    case 29:
                        zzb2 = zzit.zzt(i15, zze(t, j2), false);
                        break;
                    case 30:
                        zzb2 = zzit.zzr(i15, zze(t, j2), false);
                        break;
                    case 31:
                        zzb2 = zzit.zzv(i15, zze(t, j2), false);
                        break;
                    case 32:
                        zzb2 = zzit.zzw(i15, zze(t, j2), false);
                        break;
                    case 33:
                        zzb2 = zzit.zzu(i15, zze(t, j2), false);
                        break;
                    case 34:
                        zzb2 = zzit.zzq(i15, zze(t, j2), false);
                        break;
                    case 35:
                        i9 = zzit.zzy((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 36:
                        i9 = zzit.zzx((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 37:
                        i9 = zzit.zzq((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 38:
                        i9 = zzit.zzr((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 39:
                        i9 = zzit.zzu((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 40:
                        i9 = zzit.zzy((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 41:
                        i9 = zzit.zzx((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 42:
                        i9 = zzit.zzz((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 43:
                        i9 = zzit.zzv((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 44:
                        i9 = zzit.zzt((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 45:
                        i9 = zzit.zzx((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 46:
                        i9 = zzit.zzy((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 47:
                        i9 = zzit.zzw((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 48:
                        i9 = zzit.zzs((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            if (this.zzzi) {
                                unsafe.putInt(t, (long) i16, i9);
                            }
                            i10 = zzga.zzbb(i15);
                            i8 = zzga.zzbd(i9);
                            zzb2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 49:
                        zzb2 = zzit.zzd(i15, zze(t, j2), zzbn(i12));
                        break;
                    case 50:
                        zzb2 = this.zzzq.zzb(i15, zzjp.zzp(t, j2), zzbo(i12));
                        break;
                    case 51:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzb(i15, 0.0d);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 52:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzb(i15, 0.0f);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 53:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzd(i15, zzi(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 54:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zze(i15, zzi(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 55:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzk(i15, zzh(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 56:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzg(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 57:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzn(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 58:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzb(i15, true);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 59:
                        if (zza(t, i15, i12)) {
                            Object zzp2 = zzjp.zzp(t, j2);
                            if (zzp2 instanceof zzfh) {
                                zzb2 = zzga.zzc(i15, (zzfh) zzp2);
                                break;
                            } else {
                                zzb2 = zzga.zzb(i15, (String) zzp2);
                                break;
                            }
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 60:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzit.zzc(i15, zzjp.zzp(t, j2), zzbn(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 61:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzc(i15, (zzfh) zzjp.zzp(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 62:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzl(i15, zzh(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 63:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzp(i15, zzh(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 64:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzo(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 65:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzh(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 66:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzm(i15, zzh(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 67:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzf(i15, zzi(t, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 68:
                        if (zza(t, i15, i12)) {
                            zzb2 = zzga.zzc(i15, (zzic) zzjp.zzp(t, j2), zzbn(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    default:
                        i12 += 3;
                        i11 = 267386880;
                }
                i13 += zzb2;
                i12 += 3;
                i11 = 267386880;
            }
            return i13 + zza((zzjj) this.zzzo, (Object) t);
        }
        Unsafe unsafe2 = zzyz;
        int i17 = 0;
        int i18 = 1048575;
        int i19 = 0;
        for (int i20 = 0; i20 < this.zzza.length; i20 += 3) {
            int zzbq2 = zzbq(i20);
            int[] iArr = this.zzza;
            int i21 = iArr[i20];
            int i22 = (zzbq2 & 267386880) >>> 20;
            if (i22 <= 17) {
                int i23 = iArr[i20 + 2];
                int i24 = i23 & 1048575;
                i = 1 << (i23 >>> 20);
                if (i24 != i18) {
                    i19 = unsafe2.getInt(t, (long) i24);
                    i18 = i24;
                }
                i2 = i23;
            } else {
                i2 = (!this.zzzi || i22 < zzgn.DOUBLE_LIST_PACKED.id() || i22 > zzgn.SINT64_LIST_PACKED.id()) ? 0 : this.zzza[i20 + 2] & 1048575;
                i = 0;
            }
            long j3 = (long) (zzbq2 & 1048575);
            switch (i22) {
                case 0:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i17 += zzga.zzb(i21, 0.0d);
                        break;
                    }
                    break;
                case 1:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i17 += zzga.zzb(i21, 0.0f);
                        break;
                    }
                case 2:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzga.zzd(i21, unsafe2.getLong(t, j3));
                        i17 += i3;
                    }
                    break;
                case 3:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzga.zze(i21, unsafe2.getLong(t, j3));
                        i17 += i3;
                    }
                    break;
                case 4:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzga.zzk(i21, unsafe2.getInt(t, j3));
                        i17 += i3;
                    }
                    break;
                case 5:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzga.zzg(i21, 0L);
                        i17 += i3;
                    }
                    break;
                case 6:
                    if ((i19 & i) != 0) {
                        i17 += zzga.zzn(i21, 0);
                        j = 0;
                        break;
                    }
                    j = 0;
                case 7:
                    if ((i19 & i) != 0) {
                        zzb = zzga.zzb(i21, true);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 8:
                    if ((i19 & i) != 0) {
                        Object object = unsafe2.getObject(t, j3);
                        zzb = object instanceof zzfh ? zzga.zzc(i21, (zzfh) object) : zzga.zzb(i21, (String) object);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 9:
                    if ((i19 & i) != 0) {
                        zzb = zzit.zzc(i21, unsafe2.getObject(t, j3), zzbn(i20));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 10:
                    if ((i19 & i) != 0) {
                        zzb = zzga.zzc(i21, (zzfh) unsafe2.getObject(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 11:
                    if ((i19 & i) != 0) {
                        zzb = zzga.zzl(i21, unsafe2.getInt(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 12:
                    if ((i19 & i) != 0) {
                        zzb = zzga.zzp(i21, unsafe2.getInt(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 13:
                    if ((i19 & i) != 0) {
                        i4 = zzga.zzo(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 14:
                    if ((i19 & i) != 0) {
                        zzb = zzga.zzh(i21, 0L);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 15:
                    if ((i19 & i) != 0) {
                        zzb = zzga.zzm(i21, unsafe2.getInt(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 16:
                    if ((i19 & i) != 0) {
                        zzb = zzga.zzf(i21, unsafe2.getLong(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 17:
                    if ((i19 & i) != 0) {
                        zzb = zzga.zzc(i21, (zzic) unsafe2.getObject(t, j3), zzbn(i20));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 18:
                    zzb = zzit.zzw(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 19:
                    zzb = zzit.zzv(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 20:
                    zzb = zzit.zzo(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 21:
                    zzb = zzit.zzp(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 22:
                    zzb = zzit.zzs(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 23:
                    zzb = zzit.zzw(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 24:
                    zzb = zzit.zzv(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 25:
                    zzb = zzit.zzx(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 26:
                    zzb = zzit.zzc(i21, (List) unsafe2.getObject(t, j3));
                    i17 += zzb;
                    j = 0;
                    break;
                case 27:
                    zzb = zzit.zzc(i21, (List<?>) ((List) unsafe2.getObject(t, j3)), zzbn(i20));
                    i17 += zzb;
                    j = 0;
                    break;
                case 28:
                    zzb = zzit.zzd(i21, (List) unsafe2.getObject(t, j3));
                    i17 += zzb;
                    j = 0;
                    break;
                case 29:
                    zzb = zzit.zzt(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 30:
                    zzb = zzit.zzr(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 31:
                    zzb = zzit.zzv(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 32:
                    zzb = zzit.zzw(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 33:
                    zzb = zzit.zzu(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 34:
                    zzb = zzit.zzq(i21, (List) unsafe2.getObject(t, j3), false);
                    i17 += zzb;
                    j = 0;
                    break;
                case 35:
                    i7 = zzit.zzy((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 36:
                    i7 = zzit.zzx((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 37:
                    i7 = zzit.zzq((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 38:
                    i7 = zzit.zzr((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 39:
                    i7 = zzit.zzu((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 40:
                    i7 = zzit.zzy((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 41:
                    i7 = zzit.zzx((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 42:
                    i7 = zzit.zzz((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 43:
                    i7 = zzit.zzv((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 44:
                    i7 = zzit.zzt((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 45:
                    i7 = zzit.zzx((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 46:
                    i7 = zzit.zzy((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 47:
                    i7 = zzit.zzw((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 48:
                    i7 = zzit.zzs((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        if (this.zzzi) {
                            unsafe2.putInt(t, (long) i2, i7);
                        }
                        i6 = zzga.zzbb(i21);
                        i5 = zzga.zzbd(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 49:
                    zzb = zzit.zzd(i21, (List) unsafe2.getObject(t, j3), zzbn(i20));
                    i17 += zzb;
                    j = 0;
                    break;
                case 50:
                    zzb = this.zzzq.zzb(i21, unsafe2.getObject(t, j3), zzbo(i20));
                    i17 += zzb;
                    j = 0;
                    break;
                case 51:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzb(i21, 0.0d);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 52:
                    if (zza(t, i21, i20)) {
                        i4 = zzga.zzb(i21, 0.0f);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 53:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzd(i21, zzi(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 54:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zze(i21, zzi(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 55:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzk(i21, zzh(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 56:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzg(i21, 0L);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 57:
                    if (zza(t, i21, i20)) {
                        i4 = zzga.zzn(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 58:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzb(i21, true);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 59:
                    if (zza(t, i21, i20)) {
                        Object object2 = unsafe2.getObject(t, j3);
                        if (object2 instanceof zzfh) {
                            zzb = zzga.zzc(i21, (zzfh) object2);
                        } else {
                            zzb = zzga.zzb(i21, (String) object2);
                        }
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 60:
                    if (zza(t, i21, i20)) {
                        zzb = zzit.zzc(i21, unsafe2.getObject(t, j3), zzbn(i20));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 61:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzc(i21, (zzfh) unsafe2.getObject(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 62:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzl(i21, zzh(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 63:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzp(i21, zzh(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 64:
                    if (zza(t, i21, i20)) {
                        i4 = zzga.zzo(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 65:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzh(i21, 0L);
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 66:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzm(i21, zzh(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 67:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzf(i21, zzi(t, j3));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                case 68:
                    if (zza(t, i21, i20)) {
                        zzb = zzga.zzc(i21, (zzic) unsafe2.getObject(t, j3), zzbn(i20));
                        i17 += zzb;
                    }
                    j = 0;
                    break;
                default:
                    j = 0;
                    break;
            }
        }
        int i25 = 0;
        int zza = i17 + zza((zzjj) this.zzzo, (Object) t);
        if (!this.zzzf) {
            return zza;
        }
        zzgi<?> zzf = this.zzzp.zzf(t);
        for (int i26 = 0; i26 < zzf.zzth.zzhx(); i26++) {
            Map.Entry<T, Object> zzbv = zzf.zzth.zzbv(i26);
            i25 += zzgi.zzc(zzbv.getKey(), zzbv.getValue());
        }
        for (Map.Entry<T, Object> entry : zzf.zzth.zzhy()) {
            i25 += zzgi.zzc(entry.getKey(), entry.getValue());
        }
        return zza + i25;
    }

    private static <UT, UB> int zza(zzjj<UT, UB> zzjj, T t) {
        return zzjj.zzs(zzjj.zzw(t));
    }

    private static List<?> zze(Object obj, long j) {
        return (List) zzjp.zzp(obj, j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0552  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a2a  */
    @Override // com.google.android.gms.internal.vision.zzir
    public final void zza(T t, zzkg zzkg) throws IOException {
        Map.Entry<?, Object> entry;
        Iterator<Map.Entry<?, Object>> it;
        int length;
        int i;
        Map.Entry<?, Object> entry2;
        Iterator<Map.Entry<?, Object>> it2;
        int length2;
        if (zzkg.zzfj() == zzgs.zzf.zzxd) {
            zza(this.zzzo, t, zzkg);
            if (this.zzzf) {
                zzgi<?> zzf = this.zzzp.zzf(t);
                if (!zzf.zzth.isEmpty()) {
                    it2 = zzf.descendingIterator();
                    entry2 = it2.next();
                    for (length2 = this.zzza.length - 3; length2 >= 0; length2 -= 3) {
                        int zzbq = zzbq(length2);
                        int i2 = this.zzza[length2];
                        while (entry2 != null && this.zzzp.zza(entry2) > i2) {
                            this.zzzp.zza(zzkg, entry2);
                            entry2 = it2.hasNext() ? it2.next() : null;
                        }
                        switch ((zzbq & 267386880) >>> 20) {
                            case 0:
                                if (zza(t, length2)) {
                                    zzkg.zza(i2, zzjp.zzo(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 1:
                                if (zza(t, length2)) {
                                    zzkg.zza(i2, zzjp.zzn(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 2:
                                if (zza(t, length2)) {
                                    zzkg.zzi(i2, zzjp.zzl(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 3:
                                if (zza(t, length2)) {
                                    zzkg.zza(i2, zzjp.zzl(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 4:
                                if (zza(t, length2)) {
                                    zzkg.zzg(i2, zzjp.zzk(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 5:
                                if (zza(t, length2)) {
                                    zzkg.zzc(i2, zzjp.zzl(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 6:
                                if (zza(t, length2)) {
                                    zzkg.zzj(i2, zzjp.zzk(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 7:
                                if (zza(t, length2)) {
                                    zzkg.zza(i2, zzjp.zzm(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 8:
                                if (zza(t, length2)) {
                                    zza(i2, zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg);
                                    break;
                                } else {
                                    break;
                                }
                            case 9:
                                if (zza(t, length2)) {
                                    zzkg.zza(i2, zzjp.zzp(t, (long) (zzbq & 1048575)), zzbn(length2));
                                    break;
                                } else {
                                    break;
                                }
                            case 10:
                                if (zza(t, length2)) {
                                    zzkg.zza(i2, (zzfh) zzjp.zzp(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 11:
                                if (zza(t, length2)) {
                                    zzkg.zzh(i2, zzjp.zzk(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 12:
                                if (zza(t, length2)) {
                                    zzkg.zzr(i2, zzjp.zzk(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 13:
                                if (zza(t, length2)) {
                                    zzkg.zzq(i2, zzjp.zzk(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 14:
                                if (zza(t, length2)) {
                                    zzkg.zzj(i2, zzjp.zzl(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 15:
                                if (zza(t, length2)) {
                                    zzkg.zzi(i2, zzjp.zzk(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 16:
                                if (zza(t, length2)) {
                                    zzkg.zzb(i2, zzjp.zzl(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 17:
                                if (zza(t, length2)) {
                                    zzkg.zzb(i2, zzjp.zzp(t, (long) (zzbq & 1048575)), zzbn(length2));
                                    break;
                                } else {
                                    break;
                                }
                            case 18:
                                zzit.zza(this.zzza[length2], (List<Double>) ((List) zzjp.zzp(t, (long) (zzbq & 1048575))), zzkg, false);
                                break;
                            case 19:
                                zzit.zzb(this.zzza[length2], (List<Float>) ((List) zzjp.zzp(t, (long) (zzbq & 1048575))), zzkg, false);
                                break;
                            case 20:
                                zzit.zzc(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 21:
                                zzit.zzd(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 22:
                                zzit.zzh(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 23:
                                zzit.zzf(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 24:
                                zzit.zzk(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 25:
                                zzit.zzn(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 26:
                                zzit.zza(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg);
                                break;
                            case 27:
                                zzit.zza(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, zzbn(length2));
                                break;
                            case 28:
                                zzit.zzb(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg);
                                break;
                            case 29:
                                zzit.zzi(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 30:
                                zzit.zzm(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 31:
                                zzit.zzl(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 32:
                                zzit.zzg(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 33:
                                zzit.zzj(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 34:
                                zzit.zze(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, false);
                                break;
                            case 35:
                                zzit.zza(this.zzza[length2], (List<Double>) ((List) zzjp.zzp(t, (long) (zzbq & 1048575))), zzkg, true);
                                break;
                            case 36:
                                zzit.zzb(this.zzza[length2], (List<Float>) ((List) zzjp.zzp(t, (long) (zzbq & 1048575))), zzkg, true);
                                break;
                            case 37:
                                zzit.zzc(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 38:
                                zzit.zzd(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 39:
                                zzit.zzh(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 40:
                                zzit.zzf(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 41:
                                zzit.zzk(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 42:
                                zzit.zzn(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 43:
                                zzit.zzi(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 44:
                                zzit.zzm(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 45:
                                zzit.zzl(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 46:
                                zzit.zzg(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 47:
                                zzit.zzj(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 48:
                                zzit.zze(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, true);
                                break;
                            case 49:
                                zzit.zzb(this.zzza[length2], (List) zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg, zzbn(length2));
                                break;
                            case 50:
                                zza(zzkg, i2, zzjp.zzp(t, (long) (zzbq & 1048575)), length2);
                                break;
                            case 51:
                                if (zza(t, i2, length2)) {
                                    zzkg.zza(i2, zzf(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 52:
                                if (zza(t, i2, length2)) {
                                    zzkg.zza(i2, zzg(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 53:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzi(i2, zzi(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 54:
                                if (zza(t, i2, length2)) {
                                    zzkg.zza(i2, zzi(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 55:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzg(i2, zzh(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 56:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzc(i2, zzi(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 57:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzj(i2, zzh(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 58:
                                if (zza(t, i2, length2)) {
                                    zzkg.zza(i2, zzj(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 59:
                                if (zza(t, i2, length2)) {
                                    zza(i2, zzjp.zzp(t, (long) (zzbq & 1048575)), zzkg);
                                    break;
                                } else {
                                    break;
                                }
                            case 60:
                                if (zza(t, i2, length2)) {
                                    zzkg.zza(i2, zzjp.zzp(t, (long) (zzbq & 1048575)), zzbn(length2));
                                    break;
                                } else {
                                    break;
                                }
                            case 61:
                                if (zza(t, i2, length2)) {
                                    zzkg.zza(i2, (zzfh) zzjp.zzp(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 62:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzh(i2, zzh(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 63:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzr(i2, zzh(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 64:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzq(i2, zzh(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 65:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzj(i2, zzi(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 66:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzi(i2, zzh(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 67:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzb(i2, zzi(t, (long) (zzbq & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 68:
                                if (zza(t, i2, length2)) {
                                    zzkg.zzb(i2, zzjp.zzp(t, (long) (zzbq & 1048575)), zzbn(length2));
                                    break;
                                } else {
                                    break;
                                }
                        }
                    }
                    while (entry2 != null) {
                        this.zzzp.zza(zzkg, entry2);
                        entry2 = it2.hasNext() ? it2.next() : null;
                    }
                }
            }
            it2 = null;
            entry2 = null;
            while (length2 >= 0) {
            }
            while (entry2 != null) {
            }
        } else if (this.zzzh) {
            if (this.zzzf) {
                zzgi<?> zzf2 = this.zzzp.zzf(t);
                if (!zzf2.zzth.isEmpty()) {
                    it = zzf2.iterator();
                    entry = it.next();
                    length = this.zzza.length;
                    for (i = 0; i < length; i += 3) {
                        int zzbq2 = zzbq(i);
                        int i3 = this.zzza[i];
                        while (entry != null && this.zzzp.zza(entry) <= i3) {
                            this.zzzp.zza(zzkg, entry);
                            entry = it.hasNext() ? it.next() : null;
                        }
                        switch ((zzbq2 & 267386880) >>> 20) {
                            case 0:
                                if (zza(t, i)) {
                                    zzkg.zza(i3, zzjp.zzo(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 1:
                                if (zza(t, i)) {
                                    zzkg.zza(i3, zzjp.zzn(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 2:
                                if (zza(t, i)) {
                                    zzkg.zzi(i3, zzjp.zzl(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 3:
                                if (zza(t, i)) {
                                    zzkg.zza(i3, zzjp.zzl(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 4:
                                if (zza(t, i)) {
                                    zzkg.zzg(i3, zzjp.zzk(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 5:
                                if (zza(t, i)) {
                                    zzkg.zzc(i3, zzjp.zzl(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 6:
                                if (zza(t, i)) {
                                    zzkg.zzj(i3, zzjp.zzk(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 7:
                                if (zza(t, i)) {
                                    zzkg.zza(i3, zzjp.zzm(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 8:
                                if (zza(t, i)) {
                                    zza(i3, zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg);
                                    break;
                                } else {
                                    break;
                                }
                            case 9:
                                if (zza(t, i)) {
                                    zzkg.zza(i3, zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzbn(i));
                                    break;
                                } else {
                                    break;
                                }
                            case 10:
                                if (zza(t, i)) {
                                    zzkg.zza(i3, (zzfh) zzjp.zzp(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 11:
                                if (zza(t, i)) {
                                    zzkg.zzh(i3, zzjp.zzk(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 12:
                                if (zza(t, i)) {
                                    zzkg.zzr(i3, zzjp.zzk(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 13:
                                if (zza(t, i)) {
                                    zzkg.zzq(i3, zzjp.zzk(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 14:
                                if (zza(t, i)) {
                                    zzkg.zzj(i3, zzjp.zzl(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 15:
                                if (zza(t, i)) {
                                    zzkg.zzi(i3, zzjp.zzk(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 16:
                                if (zza(t, i)) {
                                    zzkg.zzb(i3, zzjp.zzl(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 17:
                                if (zza(t, i)) {
                                    zzkg.zzb(i3, zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzbn(i));
                                    break;
                                } else {
                                    break;
                                }
                            case 18:
                                zzit.zza(this.zzza[i], (List<Double>) ((List) zzjp.zzp(t, (long) (zzbq2 & 1048575))), zzkg, false);
                                break;
                            case 19:
                                zzit.zzb(this.zzza[i], (List<Float>) ((List) zzjp.zzp(t, (long) (zzbq2 & 1048575))), zzkg, false);
                                break;
                            case 20:
                                zzit.zzc(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 21:
                                zzit.zzd(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 22:
                                zzit.zzh(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 23:
                                zzit.zzf(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 24:
                                zzit.zzk(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 25:
                                zzit.zzn(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 26:
                                zzit.zza(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg);
                                break;
                            case 27:
                                zzit.zza(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, zzbn(i));
                                break;
                            case 28:
                                zzit.zzb(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg);
                                break;
                            case 29:
                                zzit.zzi(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 30:
                                zzit.zzm(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 31:
                                zzit.zzl(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 32:
                                zzit.zzg(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 33:
                                zzit.zzj(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 34:
                                zzit.zze(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, false);
                                break;
                            case 35:
                                zzit.zza(this.zzza[i], (List<Double>) ((List) zzjp.zzp(t, (long) (zzbq2 & 1048575))), zzkg, true);
                                break;
                            case 36:
                                zzit.zzb(this.zzza[i], (List<Float>) ((List) zzjp.zzp(t, (long) (zzbq2 & 1048575))), zzkg, true);
                                break;
                            case 37:
                                zzit.zzc(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 38:
                                zzit.zzd(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 39:
                                zzit.zzh(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 40:
                                zzit.zzf(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 41:
                                zzit.zzk(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 42:
                                zzit.zzn(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 43:
                                zzit.zzi(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 44:
                                zzit.zzm(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 45:
                                zzit.zzl(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 46:
                                zzit.zzg(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 47:
                                zzit.zzj(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 48:
                                zzit.zze(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, true);
                                break;
                            case 49:
                                zzit.zzb(this.zzza[i], (List) zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg, zzbn(i));
                                break;
                            case 50:
                                zza(zzkg, i3, zzjp.zzp(t, (long) (zzbq2 & 1048575)), i);
                                break;
                            case 51:
                                if (zza(t, i3, i)) {
                                    zzkg.zza(i3, zzf(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 52:
                                if (zza(t, i3, i)) {
                                    zzkg.zza(i3, zzg(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 53:
                                if (zza(t, i3, i)) {
                                    zzkg.zzi(i3, zzi(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 54:
                                if (zza(t, i3, i)) {
                                    zzkg.zza(i3, zzi(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 55:
                                if (zza(t, i3, i)) {
                                    zzkg.zzg(i3, zzh(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 56:
                                if (zza(t, i3, i)) {
                                    zzkg.zzc(i3, zzi(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 57:
                                if (zza(t, i3, i)) {
                                    zzkg.zzj(i3, zzh(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 58:
                                if (zza(t, i3, i)) {
                                    zzkg.zza(i3, zzj(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 59:
                                if (zza(t, i3, i)) {
                                    zza(i3, zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzkg);
                                    break;
                                } else {
                                    break;
                                }
                            case 60:
                                if (zza(t, i3, i)) {
                                    zzkg.zza(i3, zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzbn(i));
                                    break;
                                } else {
                                    break;
                                }
                            case 61:
                                if (zza(t, i3, i)) {
                                    zzkg.zza(i3, (zzfh) zzjp.zzp(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 62:
                                if (zza(t, i3, i)) {
                                    zzkg.zzh(i3, zzh(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 63:
                                if (zza(t, i3, i)) {
                                    zzkg.zzr(i3, zzh(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 64:
                                if (zza(t, i3, i)) {
                                    zzkg.zzq(i3, zzh(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 65:
                                if (zza(t, i3, i)) {
                                    zzkg.zzj(i3, zzi(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 66:
                                if (zza(t, i3, i)) {
                                    zzkg.zzi(i3, zzh(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 67:
                                if (zza(t, i3, i)) {
                                    zzkg.zzb(i3, zzi(t, (long) (zzbq2 & 1048575)));
                                    break;
                                } else {
                                    break;
                                }
                            case 68:
                                if (zza(t, i3, i)) {
                                    zzkg.zzb(i3, zzjp.zzp(t, (long) (zzbq2 & 1048575)), zzbn(i));
                                    break;
                                } else {
                                    break;
                                }
                        }
                    }
                    while (entry != null) {
                        this.zzzp.zza(zzkg, entry);
                        entry = it.hasNext() ? it.next() : null;
                    }
                    zza(this.zzzo, t, zzkg);
                }
            }
            it = null;
            entry = null;
            length = this.zzza.length;
            while (i < length) {
            }
            while (entry != null) {
            }
            zza(this.zzzo, t, zzkg);
        } else {
            zzb(t, zzkg);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:170:0x0495  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0031  */
    private final void zzb(T t, zzkg zzkg) throws IOException {
        Map.Entry<?, Object> entry;
        Iterator<Map.Entry<?, Object>> it;
        int length;
        int i;
        int i2;
        if (this.zzzf) {
            zzgi<?> zzf = this.zzzp.zzf(t);
            if (!zzf.zzth.isEmpty()) {
                it = zzf.iterator();
                entry = it.next();
                length = this.zzza.length;
                Unsafe unsafe = zzyz;
                int i3 = 1048575;
                int i4 = 0;
                for (i = 0; i < length; i += 3) {
                    int zzbq = zzbq(i);
                    int[] iArr = this.zzza;
                    int i5 = iArr[i];
                    int i6 = (zzbq & 267386880) >>> 20;
                    if (this.zzzh || i6 > 17) {
                        i2 = 0;
                    } else {
                        int i7 = iArr[i + 2];
                        int i8 = i7 & 1048575;
                        if (i8 != i3) {
                            i4 = unsafe.getInt(t, (long) i8);
                            i3 = i8;
                        }
                        i2 = 1 << (i7 >>> 20);
                    }
                    while (entry != null && this.zzzp.zza(entry) <= i5) {
                        this.zzzp.zza(zzkg, entry);
                        entry = it.hasNext() ? it.next() : null;
                    }
                    long j = (long) (zzbq & 1048575);
                    switch (i6) {
                        case 0:
                            if ((i2 & i4) != 0) {
                                zzkg.zza(i5, zzjp.zzo(t, j));
                                continue;
                            }
                        case 1:
                            if ((i2 & i4) != 0) {
                                zzkg.zza(i5, zzjp.zzn(t, j));
                            } else {
                                continue;
                            }
                        case 2:
                            if ((i2 & i4) != 0) {
                                zzkg.zzi(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 3:
                            if ((i2 & i4) != 0) {
                                zzkg.zza(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 4:
                            if ((i2 & i4) != 0) {
                                zzkg.zzg(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 5:
                            if ((i2 & i4) != 0) {
                                zzkg.zzc(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 6:
                            if ((i2 & i4) != 0) {
                                zzkg.zzj(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 7:
                            if ((i2 & i4) != 0) {
                                zzkg.zza(i5, zzjp.zzm(t, j));
                            } else {
                                continue;
                            }
                        case 8:
                            if ((i2 & i4) != 0) {
                                zza(i5, unsafe.getObject(t, j), zzkg);
                            } else {
                                continue;
                            }
                        case 9:
                            if ((i2 & i4) != 0) {
                                zzkg.zza(i5, unsafe.getObject(t, j), zzbn(i));
                            } else {
                                continue;
                            }
                        case 10:
                            if ((i2 & i4) != 0) {
                                zzkg.zza(i5, (zzfh) unsafe.getObject(t, j));
                            } else {
                                continue;
                            }
                        case 11:
                            if ((i2 & i4) != 0) {
                                zzkg.zzh(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 12:
                            if ((i2 & i4) != 0) {
                                zzkg.zzr(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 13:
                            if ((i2 & i4) != 0) {
                                zzkg.zzq(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 14:
                            if ((i2 & i4) != 0) {
                                zzkg.zzj(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 15:
                            if ((i2 & i4) != 0) {
                                zzkg.zzi(i5, unsafe.getInt(t, j));
                            } else {
                                continue;
                            }
                        case 16:
                            if ((i2 & i4) != 0) {
                                zzkg.zzb(i5, unsafe.getLong(t, j));
                            } else {
                                continue;
                            }
                        case 17:
                            if ((i2 & i4) != 0) {
                                zzkg.zzb(i5, unsafe.getObject(t, j), zzbn(i));
                            } else {
                                continue;
                            }
                        case 18:
                            zzit.zza(this.zzza[i], (List<Double>) ((List) unsafe.getObject(t, j)), zzkg, false);
                            break;
                        case 19:
                            zzit.zzb(this.zzza[i], (List<Float>) ((List) unsafe.getObject(t, j)), zzkg, false);
                            break;
                        case 20:
                            zzit.zzc(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 21:
                            zzit.zzd(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 22:
                            zzit.zzh(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 23:
                            zzit.zzf(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 24:
                            zzit.zzk(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 25:
                            zzit.zzn(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 26:
                            zzit.zza(this.zzza[i], (List) unsafe.getObject(t, j), zzkg);
                            break;
                        case 27:
                            zzit.zza(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, zzbn(i));
                            break;
                        case 28:
                            zzit.zzb(this.zzza[i], (List) unsafe.getObject(t, j), zzkg);
                            break;
                        case 29:
                            zzit.zzi(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 30:
                            zzit.zzm(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 31:
                            zzit.zzl(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 32:
                            zzit.zzg(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 33:
                            zzit.zzj(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 34:
                            zzit.zze(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, false);
                            break;
                        case 35:
                            zzit.zza(this.zzza[i], (List<Double>) ((List) unsafe.getObject(t, j)), zzkg, true);
                            break;
                        case 36:
                            zzit.zzb(this.zzza[i], (List<Float>) ((List) unsafe.getObject(t, j)), zzkg, true);
                            break;
                        case 37:
                            zzit.zzc(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 38:
                            zzit.zzd(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 39:
                            zzit.zzh(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 40:
                            zzit.zzf(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 41:
                            zzit.zzk(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 42:
                            zzit.zzn(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 43:
                            zzit.zzi(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 44:
                            zzit.zzm(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 45:
                            zzit.zzl(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 46:
                            zzit.zzg(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 47:
                            zzit.zzj(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 48:
                            zzit.zze(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, true);
                            break;
                        case 49:
                            zzit.zzb(this.zzza[i], (List) unsafe.getObject(t, j), zzkg, zzbn(i));
                            break;
                        case 50:
                            zza(zzkg, i5, unsafe.getObject(t, j), i);
                            break;
                        case 51:
                            if (zza(t, i5, i)) {
                                zzkg.zza(i5, zzf(t, j));
                                break;
                            }
                            break;
                        case 52:
                            if (zza(t, i5, i)) {
                                zzkg.zza(i5, zzg(t, j));
                                break;
                            }
                            break;
                        case 53:
                            if (zza(t, i5, i)) {
                                zzkg.zzi(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 54:
                            if (zza(t, i5, i)) {
                                zzkg.zza(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 55:
                            if (zza(t, i5, i)) {
                                zzkg.zzg(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 56:
                            if (zza(t, i5, i)) {
                                zzkg.zzc(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 57:
                            if (zza(t, i5, i)) {
                                zzkg.zzj(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 58:
                            if (zza(t, i5, i)) {
                                zzkg.zza(i5, zzj(t, j));
                                break;
                            }
                            break;
                        case 59:
                            if (zza(t, i5, i)) {
                                zza(i5, unsafe.getObject(t, j), zzkg);
                                break;
                            }
                            break;
                        case 60:
                            if (zza(t, i5, i)) {
                                zzkg.zza(i5, unsafe.getObject(t, j), zzbn(i));
                                break;
                            }
                            break;
                        case 61:
                            if (zza(t, i5, i)) {
                                zzkg.zza(i5, (zzfh) unsafe.getObject(t, j));
                                break;
                            }
                            break;
                        case 62:
                            if (zza(t, i5, i)) {
                                zzkg.zzh(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 63:
                            if (zza(t, i5, i)) {
                                zzkg.zzr(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 64:
                            if (zza(t, i5, i)) {
                                zzkg.zzq(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 65:
                            if (zza(t, i5, i)) {
                                zzkg.zzj(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 66:
                            if (zza(t, i5, i)) {
                                zzkg.zzi(i5, zzh(t, j));
                                break;
                            }
                            break;
                        case 67:
                            if (zza(t, i5, i)) {
                                zzkg.zzb(i5, zzi(t, j));
                                break;
                            }
                            break;
                        case 68:
                            if (zza(t, i5, i)) {
                                zzkg.zzb(i5, unsafe.getObject(t, j), zzbn(i));
                                break;
                            }
                            break;
                    }
                }
                while (entry != null) {
                    this.zzzp.zza(zzkg, entry);
                    entry = it.hasNext() ? it.next() : null;
                }
                zza(this.zzzo, t, zzkg);
            }
        }
        it = null;
        entry = null;
        length = this.zzza.length;
        Unsafe unsafe2 = zzyz;
        int i32 = 1048575;
        int i42 = 0;
        while (i < length) {
        }
        while (entry != null) {
        }
        zza(this.zzzo, t, zzkg);
    }

    private final <K, V> void zza(zzkg zzkg, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzkg.zza(i, this.zzzq.zzq(zzbo(i2)), this.zzzq.zzm(obj));
        }
    }

    private static <UT, UB> void zza(zzjj<UT, UB> zzjj, T t, zzkg zzkg) throws IOException {
        zzjj.zza(zzjj.zzw(t), zzkg);
    }

    /*  JADX ERROR: StackOverflowError in pass: MarkFinallyVisitor
        java.lang.StackOverflowError
        	at jadx.core.dex.instructions.IndexInsnNode.isSame(IndexInsnNode.java:36)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.sameInsns(MarkFinallyVisitor.java:451)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.compareBlocks(MarkFinallyVisitor.java:436)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.checkBlocksTree(MarkFinallyVisitor.java:408)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.checkBlocksTree(MarkFinallyVisitor.java:411)
        */
    @Override // com.google.android.gms.internal.vision.zzir
    public final void zza(T r13, com.google.android.gms.internal.vision.zzis r14, com.google.android.gms.internal.vision.zzgd r15) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1644
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzig.zza(java.lang.Object, com.google.android.gms.internal.vision.zzis, com.google.android.gms.internal.vision.zzgd):void");
    }

    private static zzjm zzt(Object obj) {
        zzgs zzgs = (zzgs) obj;
        zzjm zzjm = zzgs.zzwj;
        if (zzjm != zzjm.zzig()) {
            return zzjm;
        }
        zzjm zzih = zzjm.zzih();
        zzgs.zzwj = zzih;
        return zzih;
    }

    private static int zza(byte[] bArr, int i, int i2, zzka zzka, Class<?> cls, zzfb zzfb) throws IOException {
        switch (zzif.zzrx[zzka.ordinal()]) {
            case 1:
                int zzb = zzez.zzb(bArr, i, zzfb);
                zzfb.zzrw = Boolean.valueOf(zzfb.zzrv != 0);
                return zzb;
            case 2:
                return zzez.zze(bArr, i, zzfb);
            case 3:
                zzfb.zzrw = Double.valueOf(zzez.zzc(bArr, i));
                return i + 8;
            case 4:
            case 5:
                zzfb.zzrw = Integer.valueOf(zzez.zza(bArr, i));
                return i + 4;
            case 6:
            case 7:
                zzfb.zzrw = Long.valueOf(zzez.zzb(bArr, i));
                return i + 8;
            case 8:
                zzfb.zzrw = Float.valueOf(zzez.zzd(bArr, i));
                return i + 4;
            case 9:
            case 10:
            case 11:
                int zza = zzez.zza(bArr, i, zzfb);
                zzfb.zzrw = Integer.valueOf(zzfb.zzru);
                return zza;
            case 12:
            case 13:
                int zzb2 = zzez.zzb(bArr, i, zzfb);
                zzfb.zzrw = Long.valueOf(zzfb.zzrv);
                return zzb2;
            case 14:
                return zzez.zza(zzin.zzho().zzf(cls), bArr, i, i2, zzfb);
            case 15:
                int zza2 = zzez.zza(bArr, i, zzfb);
                zzfb.zzrw = Integer.valueOf(zzft.zzav(zzfb.zzru));
                return zza2;
            case 16:
                int zzb3 = zzez.zzb(bArr, i, zzfb);
                zzfb.zzrw = Long.valueOf(zzft.zzr(zzfb.zzrv));
                return zzb3;
            case 17:
                return zzez.zzd(bArr, i, zzfb);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x0350 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x0350 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01b0  */
    private final int zza(T r16, byte[] r17, int r18, int r19, int r20, int r21, int r22, int r23, long r24, int r26, long r27, com.google.android.gms.internal.vision.zzfb r29) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 918
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzig.zza(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.google.android.gms.internal.vision.zzfb):int");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:30:0x003e */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:34:0x003e */
    private final <K, V> int zza(T t, byte[] bArr, int i, int i2, int i3, long j, zzfb zzfb) throws IOException {
        Unsafe unsafe = zzyz;
        Object zzbo = zzbo(i3);
        Object object = unsafe.getObject(t, j);
        if (this.zzzq.zzn(object)) {
            Object zzp = this.zzzq.zzp(zzbo);
            this.zzzq.zzc(zzp, object);
            unsafe.putObject(t, j, zzp);
            object = zzp;
        }
        zzht<?, ?> zzq = this.zzzq.zzq(zzbo);
        Map<?, ?> zzl = this.zzzq.zzl(object);
        int zza = zzez.zza(bArr, i, zzfb);
        int i4 = zzfb.zzru;
        if (i4 < 0 || i4 > i2 - zza) {
            throw zzhc.zzgm();
        }
        int i5 = i4 + zza;
        RecyclerViewAccessibilityDelegate.ItemDelegate itemDelegate = (K) zzq.zzyt;
        RecyclerViewAccessibilityDelegate.ItemDelegate itemDelegate2 = (V) zzq.zzgd;
        while (zza < i5) {
            int i6 = zza + 1;
            byte b = bArr[zza];
            int i7 = b;
            if (b < 0) {
                i6 = zzez.zza(b, bArr, i6, zzfb);
                i7 = zzfb.zzru;
            }
            int i8 = (i7 == 1 ? 1 : 0) >>> 3;
            int i9 = (i7 == 1 ? 1 : 0) & 7;
            if (i8 != 1) {
                if (i8 == 2 && i9 == zzq.zzyu.zziq()) {
                    zza = zza(bArr, i6, i2, zzq.zzyu, zzq.zzgd.getClass(), zzfb);
                    itemDelegate2 = (V) zzfb.zzrw;
                }
            } else if (i9 == zzq.zzys.zziq()) {
                zza = zza(bArr, i6, i2, zzq.zzys, (Class<?>) null, zzfb);
                itemDelegate = (K) zzfb.zzrw;
            }
            zza = zzez.zza(i7, bArr, i6, i2, zzfb);
        }
        if (zza == i5) {
            zzl.put(itemDelegate, itemDelegate2);
            return i5;
        }
        throw zzhc.zzgs();
    }

    private final int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, zzfb zzfb) throws IOException {
        int i9;
        Unsafe unsafe = zzyz;
        long j2 = (long) (this.zzza[i8 + 2] & 1048575);
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Double.valueOf(zzez.zzc(bArr, i)));
                    i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 52:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Float.valueOf(zzez.zzd(bArr, i)));
                    i9 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 53:
            case 54:
                if (i5 == 0) {
                    i9 = zzez.zzb(bArr, i, zzfb);
                    unsafe.putObject(t, j, Long.valueOf(zzfb.zzrv));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 55:
            case 62:
                if (i5 == 0) {
                    i9 = zzez.zza(bArr, i, zzfb);
                    unsafe.putObject(t, j, Integer.valueOf(zzfb.zzru));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 56:
            case 65:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Long.valueOf(zzez.zzb(bArr, i)));
                    i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 57:
            case 64:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Integer.valueOf(zzez.zza(bArr, i)));
                    i9 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 58:
                if (i5 == 0) {
                    i9 = zzez.zzb(bArr, i, zzfb);
                    unsafe.putObject(t, j, Boolean.valueOf(zzfb.zzrv != 0));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 59:
                if (i5 == 2) {
                    int zza = zzez.zza(bArr, i, zzfb);
                    int i10 = zzfb.zzru;
                    if (i10 == 0) {
                        unsafe.putObject(t, j, "");
                    } else if ((i6 & 536870912) == 0 || zzjs.zzf(bArr, zza, zza + i10)) {
                        unsafe.putObject(t, j, new String(bArr, zza, i10, zzgt.UTF_8));
                        zza += i10;
                    } else {
                        throw zzhc.zzgt();
                    }
                    unsafe.putInt(t, j2, i4);
                    return zza;
                }
                return i;
            case 60:
                if (i5 == 2) {
                    int zza2 = zzez.zza(zzbn(i8), bArr, i, i2, zzfb);
                    Object object = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object == null) {
                        unsafe.putObject(t, j, zzfb.zzrw);
                    } else {
                        unsafe.putObject(t, j, zzgt.zzb(object, zzfb.zzrw));
                    }
                    unsafe.putInt(t, j2, i4);
                    return zza2;
                }
                return i;
            case 61:
                if (i5 == 2) {
                    i9 = zzez.zze(bArr, i, zzfb);
                    unsafe.putObject(t, j, zzfb.zzrw);
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 63:
                if (i5 == 0) {
                    int zza3 = zzez.zza(bArr, i, zzfb);
                    int i11 = zzfb.zzru;
                    zzgy zzbp = zzbp(i8);
                    if (zzbp == null || zzbp.zzg(i11)) {
                        unsafe.putObject(t, j, Integer.valueOf(i11));
                        i9 = zza3;
                        unsafe.putInt(t, j2, i4);
                        return i9;
                    }
                    zzt(t).zzb(i3, Long.valueOf((long) i11));
                    return zza3;
                }
                return i;
            case 66:
                if (i5 == 0) {
                    i9 = zzez.zza(bArr, i, zzfb);
                    unsafe.putObject(t, j, Integer.valueOf(zzft.zzav(zzfb.zzru)));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 67:
                if (i5 == 0) {
                    i9 = zzez.zzb(bArr, i, zzfb);
                    unsafe.putObject(t, j, Long.valueOf(zzft.zzr(zzfb.zzrv)));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 68:
                if (i5 == 3) {
                    i9 = zzez.zza(zzbn(i8), bArr, i, i2, (i3 & -8) | 4, zzfb);
                    Object object2 = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object2 == null) {
                        unsafe.putObject(t, j, zzfb.zzrw);
                    } else {
                        unsafe.putObject(t, j, zzgt.zzb(object2, zzfb.zzrw));
                    }
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            default:
                return i;
        }
    }

    private final zzir zzbn(int i) {
        int i2 = (i / 3) << 1;
        zzir zzir = (zzir) this.zzzb[i2];
        if (zzir != null) {
            return zzir;
        }
        zzir<T> zzf = zzin.zzho().zzf((Class) this.zzzb[i2 + 1]);
        this.zzzb[i2] = zzf;
        return zzf;
    }

    private final Object zzbo(int i) {
        return this.zzzb[(i / 3) << 1];
    }

    private final zzgy zzbp(int i) {
        return (zzgy) this.zzzb[((i / 3) << 1) + 1];
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v50, types: [com.google.android.gms.internal.vision.zzgo, com.google.android.gms.internal.vision.zzgz] */
    /* JADX WARN: Type inference failed for: r0v51, types: [com.google.android.gms.internal.vision.zzhq, com.google.android.gms.internal.vision.zzgz] */
    /* JADX WARN: Type inference failed for: r0v52, types: [com.google.android.gms.internal.vision.zzgu, com.google.android.gms.internal.vision.zzgz] */
    /* JADX WARN: Type inference failed for: r0v53, types: [com.google.android.gms.internal.vision.zzhq, com.google.android.gms.internal.vision.zzgz] */
    /* JADX WARN: Type inference failed for: r0v54, types: [com.google.android.gms.internal.vision.zzgu, com.google.android.gms.internal.vision.zzgz] */
    /* JADX WARN: Type inference failed for: r0v55, types: [com.google.android.gms.internal.vision.zzff, com.google.android.gms.internal.vision.zzgz] */
    /* JADX WARN: Type inference failed for: r0v56, types: [com.google.android.gms.internal.vision.zzgu, com.google.android.gms.internal.vision.zzgz] */
    /* JADX WARN: Type inference failed for: r0v57, types: [com.google.android.gms.internal.vision.zzhq, com.google.android.gms.internal.vision.zzgz] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x036e, code lost:
        if (r0 == r3) goto L_0x03d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x03b4, code lost:
        if (r0 == r14) goto L_0x03d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x0680, code lost:
        if (r1 != 18) goto L_0x068f;
     */
    /* JADX WARNING: Unknown variable types count: 8 */
    public final int zza(T t, byte[] bArr, int i, int i2, int i3, zzfb zzfb) throws IOException {
        Unsafe unsafe;
        int i4;
        int i5;
        T t2;
        zzig<T> zzig;
        int i6;
        int i7;
        byte b;
        int i8;
        int i9;
        int i10;
        int i11;
        boolean z;
        int i12;
        int i13;
        int i14;
        int i15;
        byte[] bArr2;
        T t3;
        int i16;
        zzfb zzfb2;
        int i17;
        zzgb zzgb;
        Object obj;
        int i18;
        zzgb zzgb2;
        int i19;
        int i20;
        long j;
        int i21;
        int i22;
        int i23;
        int i24;
        int i25;
        int i26;
        T t4;
        zzig<T> zzig2 = this;
        T t5 = t;
        byte[] bArr3 = bArr;
        int i27 = i2;
        int i28 = i3;
        zzfb zzfb3 = zzfb;
        Unsafe unsafe2 = zzyz;
        int i29 = i;
        int i30 = -1;
        int i31 = 0;
        int i32 = 0;
        int i33 = 0;
        int i34 = 1048575;
        while (true) {
            Object obj2 = null;
            if (i29 < i27) {
                int i35 = i29 + 1;
                byte b2 = bArr3[i29];
                if (b2 < 0) {
                    int zza = zzez.zza(b2, bArr3, i35, zzfb3);
                    b = zzfb3.zzru;
                    i35 = zza;
                } else {
                    b = b2;
                }
                int i36 = b >>> 3;
                int i37 = b & 7;
                if (i36 > i30) {
                    i8 = zzig2.zzs(i36, i31 / 3);
                } else {
                    i8 = zzig2.zzbt(i36);
                }
                if (i8 == -1) {
                    i9 = i36;
                    i14 = i35;
                    i13 = b;
                    i11 = i33;
                    unsafe = unsafe2;
                    i12 = i28;
                    z = true;
                    i10 = 0;
                } else {
                    int[] iArr = zzig2.zzza;
                    int i38 = iArr[i8 + 1];
                    int i39 = (i38 & 267386880) >>> 20;
                    long j2 = (long) (i38 & 1048575);
                    if (i39 <= 17) {
                        int i40 = iArr[i8 + 2];
                        int i41 = 1 << (i40 >>> 20);
                        int i42 = i40 & 1048575;
                        if (i42 != i34) {
                            if (i34 != 1048575) {
                                long j3 = (long) i34;
                                t4 = t;
                                j = j2;
                                unsafe2.putInt(t4, j3, i33);
                            } else {
                                t4 = t;
                                j = j2;
                            }
                            i33 = unsafe2.getInt(t4, (long) i42);
                            t5 = t4;
                        } else {
                            t5 = t;
                            j = j2;
                            i42 = i34;
                        }
                        switch (i39) {
                            case 0:
                                i22 = i36;
                                i23 = i8;
                                i11 = i33;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 1) {
                                    zzjp.zza(t5, j, zzez.zzc(bArr3, i35));
                                    i29 = i35 + 8;
                                    i33 = i11 | i41;
                                    i34 = i21;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 1:
                                i22 = i36;
                                i23 = i8;
                                i11 = i33;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 5) {
                                    zzjp.zza((Object) t5, j, zzez.zzd(bArr3, i35));
                                    i29 = i35 + 4;
                                    i33 = i11 | i41;
                                    i34 = i21;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 2:
                            case 3:
                                i22 = i36;
                                i23 = i8;
                                i11 = i33;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 0) {
                                    i25 = zzez.zzb(bArr3, i35, zzfb3);
                                    unsafe2.putLong(t, j, zzfb3.zzrv);
                                    i33 = i11 | i41;
                                    i34 = i21;
                                    i29 = i25;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 4:
                            case 11:
                                i22 = i36;
                                i23 = i8;
                                i11 = i33;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 0) {
                                    i29 = zzez.zza(bArr3, i35, zzfb3);
                                    unsafe2.putInt(t5, j, zzfb3.zzru);
                                    i33 = i11 | i41;
                                    i34 = i21;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 5:
                            case 14:
                                i22 = i36;
                                i23 = i8;
                                i11 = i33;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 1) {
                                    unsafe2.putLong(t, j, zzez.zzb(bArr3, i35));
                                    i29 = i35 + 8;
                                    i33 = i11 | i41;
                                    i34 = i21;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 6:
                            case 13:
                                i22 = i36;
                                i23 = i8;
                                i11 = i33;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 5) {
                                    unsafe2.putInt(t5, j, zzez.zza(bArr3, i35));
                                    i29 = i35 + 4;
                                    i33 = i11 | i41;
                                    i34 = i21;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 7:
                                i22 = i36;
                                i23 = i8;
                                i11 = i33;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 0) {
                                    i29 = zzez.zzb(bArr3, i35, zzfb3);
                                    zzjp.zza(t5, j, zzfb3.zzrv != 0);
                                    i33 = i11 | i41;
                                    i34 = i21;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 8:
                                i22 = i36;
                                i23 = i8;
                                i11 = i33;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 2) {
                                    if ((536870912 & i38) == 0) {
                                        i29 = zzez.zzc(bArr3, i35, zzfb3);
                                    } else {
                                        i29 = zzez.zzd(bArr3, i35, zzfb3);
                                    }
                                    unsafe2.putObject(t5, j, zzfb3.zzrw);
                                    i33 = i11 | i41;
                                    i34 = i21;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 9:
                                i22 = i36;
                                i23 = i8;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 2) {
                                    int zza2 = zzez.zza(zzig2.zzbn(i23), bArr3, i35, i2, zzfb3);
                                    if ((i33 & i41) == 0) {
                                        unsafe2.putObject(t5, j, zzfb3.zzrw);
                                    } else {
                                        unsafe2.putObject(t5, j, zzgt.zzb(unsafe2.getObject(t5, j), zzfb3.zzrw));
                                    }
                                    i34 = i21;
                                    i32 = i24;
                                    i30 = i22;
                                    i27 = i2;
                                    i33 |= i41;
                                    i29 = zza2;
                                    i31 = i23;
                                    i28 = i3;
                                    break;
                                } else {
                                    i11 = i33;
                                    i34 = i21;
                                    i12 = i3;
                                    i14 = i35;
                                    i13 = i24;
                                    unsafe = unsafe2;
                                    i10 = i23;
                                    i9 = i22;
                                    z = true;
                                    break;
                                }
                            case 10:
                                i22 = i36;
                                i23 = i8;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 2) {
                                    i26 = zzez.zze(bArr3, i35, zzfb3);
                                    unsafe2.putObject(t5, j, zzfb3.zzrw);
                                    i33 |= i41;
                                    i34 = i21;
                                    i29 = i26;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i11 = i33;
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 12:
                                i22 = i36;
                                i23 = i8;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 0) {
                                    i26 = zzez.zza(bArr3, i35, zzfb3);
                                    int i43 = zzfb3.zzru;
                                    zzgy zzbp = zzig2.zzbp(i23);
                                    if (zzbp == null || zzbp.zzg(i43)) {
                                        unsafe2.putInt(t5, j, i43);
                                        i33 |= i41;
                                        i34 = i21;
                                        i29 = i26;
                                        i32 = i24;
                                        i31 = i23;
                                        i30 = i22;
                                        i27 = i2;
                                        i28 = i3;
                                        break;
                                    } else {
                                        zzt(t).zzb(i24, Long.valueOf((long) i43));
                                        i29 = i26;
                                        i33 = i33;
                                        i32 = i24;
                                        i31 = i23;
                                        i30 = i22;
                                        i34 = i21;
                                        i27 = i2;
                                        i28 = i3;
                                    }
                                }
                                i11 = i33;
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 15:
                                i22 = i36;
                                i23 = i8;
                                i21 = i42;
                                i24 = b;
                                if (i37 == 0) {
                                    i26 = zzez.zza(bArr3, i35, zzfb3);
                                    unsafe2.putInt(t5, j, zzft.zzav(zzfb3.zzru));
                                    i33 |= i41;
                                    i34 = i21;
                                    i29 = i26;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                                i11 = i33;
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                            case 16:
                                i22 = i36;
                                i23 = i8;
                                if (i37 == 0) {
                                    i25 = zzez.zzb(bArr3, i35, zzfb3);
                                    i21 = i42;
                                    i24 = b;
                                    unsafe2.putLong(t, j, zzft.zzr(zzfb3.zzrv));
                                    i33 |= i41;
                                    i34 = i21;
                                    i29 = i25;
                                    i32 = i24;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                } else {
                                    i21 = i42;
                                    i24 = b;
                                    i11 = i33;
                                    i34 = i21;
                                    i12 = i3;
                                    i14 = i35;
                                    i13 = i24;
                                    unsafe = unsafe2;
                                    i10 = i23;
                                    i9 = i22;
                                    z = true;
                                    break;
                                }
                            case 17:
                                if (i37 == 3) {
                                    i22 = i36;
                                    i23 = i8;
                                    i29 = zzez.zza(zzig2.zzbn(i8), bArr, i35, i2, (i36 << 3) | 4, zzfb);
                                    if ((i33 & i41) == 0) {
                                        unsafe2.putObject(t5, j, zzfb3.zzrw);
                                    } else {
                                        unsafe2.putObject(t5, j, zzgt.zzb(unsafe2.getObject(t5, j), zzfb3.zzrw));
                                    }
                                    i33 |= i41;
                                    i32 = b;
                                    i34 = i42;
                                    i31 = i23;
                                    i30 = i22;
                                    i27 = i2;
                                    i28 = i3;
                                    break;
                                }
                            default:
                                i22 = i36;
                                i23 = i8;
                                i11 = i33;
                                i21 = i42;
                                i24 = b;
                                i34 = i21;
                                i12 = i3;
                                i14 = i35;
                                i13 = i24;
                                unsafe = unsafe2;
                                i10 = i23;
                                i9 = i22;
                                z = true;
                                break;
                        }
                    } else {
                        i11 = i33;
                        t5 = t;
                        if (i39 != 27) {
                            i10 = i8;
                            if (i39 <= 49) {
                                i19 = b;
                                z = true;
                                unsafe = unsafe2;
                                i12 = i3;
                                i9 = i36;
                                i29 = zza(t, bArr, i35, i2, b, i36, i37, i10, (long) i38, i39, j2, zzfb);
                            } else {
                                i12 = i3;
                                i20 = i35;
                                i19 = b;
                                unsafe = unsafe2;
                                i9 = i36;
                                z = true;
                                if (i39 != 50) {
                                    i29 = zza(t, bArr, i20, i2, i19, i9, i37, i38, i39, j2, i10, zzfb);
                                    if (i29 != i20) {
                                        zzig2 = this;
                                        t5 = t;
                                        bArr3 = bArr;
                                        i27 = i2;
                                        i32 = i19;
                                        i30 = i9;
                                        i34 = i34;
                                        i33 = i11;
                                        i31 = i10;
                                        unsafe2 = unsafe;
                                        i28 = i12;
                                        zzfb3 = zzfb;
                                    }
                                } else if (i37 == 2) {
                                    i29 = zza(t, bArr, i20, i2, i10, j2, zzfb);
                                }
                                i14 = i29;
                                i13 = i19;
                                i34 = i34;
                            }
                            t5 = t;
                            bArr3 = bArr;
                            i27 = i2;
                            zzfb3 = zzfb;
                            i28 = i12;
                            i32 = i19;
                            i34 = i34;
                            i33 = i11;
                            i31 = i10;
                            i30 = i9;
                            unsafe2 = unsafe;
                            zzig2 = this;
                        } else if (i37 == 2) {
                            zzgz zzgz = (zzgz) unsafe2.getObject(t5, j2);
                            if (!zzgz.zzdo()) {
                                int size = zzgz.size();
                                zzgz = zzgz.zzah(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t5, j2, zzgz);
                            }
                            i29 = zzez.zza(zzig2.zzbn(i8), b, bArr, i35, i2, zzgz, zzfb);
                            i28 = i3;
                            i32 = b;
                            i30 = i36;
                            i34 = i34;
                            i33 = i11;
                            i31 = i8;
                            i27 = i2;
                        } else {
                            i10 = i8;
                            i12 = i3;
                            i20 = i35;
                            i19 = b;
                            unsafe = unsafe2;
                            i9 = i36;
                            z = true;
                        }
                        i14 = i20;
                        i13 = i19;
                        i34 = i34;
                    }
                }
                if (i13 != i12 || i12 == 0) {
                    if (this.zzzf) {
                        zzfb2 = zzfb;
                        if (zzfb2.zzcn != zzgd.zzfl()) {
                            zzic zzic = this.zzze;
                            zzjj<?, ?> zzjj = this.zzzo;
                            i16 = i9;
                            zzgs.zzg zza3 = zzfb2.zzcn.zza(zzic, i16);
                            if (zza3 == null) {
                                i29 = zzez.zza(i13, bArr, i14, i2, zzt(t), zzfb);
                                t3 = t;
                                bArr2 = bArr;
                                i15 = i34;
                                i17 = i2;
                            } else {
                                t3 = t;
                                T t6 = t3;
                                t6.zzgk();
                                zzgi<zzgs.zzd> zzgi = t6.zzwq;
                                if (!zza3.zzxh.zzwo || !zza3.zzxh.zzwp) {
                                    bArr2 = bArr;
                                    if (zza3.zzxh.zzwn != zzka.ENUM) {
                                        switch (zzfc.zzrx[zza3.zzxh.zzwn.ordinal()]) {
                                            case 1:
                                                i15 = i34;
                                                i17 = i2;
                                                obj2 = Double.valueOf(zzez.zzc(bArr2, i14));
                                                i14 += 8;
                                                obj = obj2;
                                                break;
                                            case 2:
                                                i15 = i34;
                                                i17 = i2;
                                                obj2 = Float.valueOf(zzez.zzd(bArr2, i14));
                                                i14 += 4;
                                                obj = obj2;
                                                break;
                                            case 3:
                                            case 4:
                                                i15 = i34;
                                                i17 = i2;
                                                i14 = zzez.zzb(bArr2, i14, zzfb2);
                                                obj2 = Long.valueOf(zzfb2.zzrv);
                                                obj = obj2;
                                                break;
                                            case 5:
                                            case 6:
                                                i15 = i34;
                                                i17 = i2;
                                                i14 = zzez.zza(bArr2, i14, zzfb2);
                                                obj2 = Integer.valueOf(zzfb2.zzru);
                                                obj = obj2;
                                                break;
                                            case 7:
                                            case 8:
                                                i15 = i34;
                                                i17 = i2;
                                                obj2 = Long.valueOf(zzez.zzb(bArr2, i14));
                                                i14 += 8;
                                                obj = obj2;
                                                break;
                                            case 9:
                                            case 10:
                                                i15 = i34;
                                                i17 = i2;
                                                obj2 = Integer.valueOf(zzez.zza(bArr2, i14));
                                                i14 += 4;
                                                obj = obj2;
                                                break;
                                            case 11:
                                                i15 = i34;
                                                i17 = i2;
                                                i14 = zzez.zzb(bArr2, i14, zzfb2);
                                                if (zzfb2.zzrv == 0) {
                                                    z = false;
                                                }
                                                obj2 = Boolean.valueOf(z);
                                                obj = obj2;
                                                break;
                                            case 12:
                                                i15 = i34;
                                                i17 = i2;
                                                i14 = zzez.zza(bArr2, i14, zzfb2);
                                                obj2 = Integer.valueOf(zzft.zzav(zzfb2.zzru));
                                                obj = obj2;
                                                break;
                                            case 13:
                                                i15 = i34;
                                                i17 = i2;
                                                i14 = zzez.zzb(bArr2, i14, zzfb2);
                                                obj2 = Long.valueOf(zzft.zzr(zzfb2.zzrv));
                                                obj = obj2;
                                                break;
                                            case 14:
                                                throw new IllegalStateException("Shouldn't reach here.");
                                            case 15:
                                                i15 = i34;
                                                i17 = i2;
                                                i14 = zzez.zze(bArr2, i14, zzfb2);
                                                obj = zzfb2.zzrw;
                                                break;
                                            case 16:
                                                i15 = i34;
                                                i17 = i2;
                                                i14 = zzez.zzc(bArr2, i14, zzfb2);
                                                obj = zzfb2.zzrw;
                                                break;
                                            case 17:
                                                i15 = i34;
                                                i17 = i2;
                                                i14 = zzez.zza(zzin.zzho().zzf(zza3.zzxg.getClass()), bArr, i14, i2, (i16 << 3) | 4, zzfb);
                                                obj = zzfb2.zzrw;
                                                break;
                                            case 18:
                                                i14 = zzez.zza(zzin.zzho().zzf(zza3.zzxg.getClass()), bArr2, i14, i2, zzfb2);
                                                obj = zzfb2.zzrw;
                                                i15 = i34;
                                                i17 = i2;
                                                break;
                                            default:
                                                i15 = i34;
                                                i17 = i2;
                                                obj = obj2;
                                                break;
                                        }
                                    } else {
                                        i14 = zzez.zza(bArr2, i14, zzfb2);
                                        if (zza3.zzxh.zzwm.zzh(zzfb2.zzru) == null) {
                                            zzjm zzjm = t6.zzwj;
                                            if (zzjm == zzjm.zzig()) {
                                                zzjm = zzjm.zzih();
                                                t6.zzwj = zzjm;
                                            }
                                            zzit.zza(i16, zzfb2.zzru, zzjm, zzjj);
                                        } else {
                                            i15 = i34;
                                            obj = Integer.valueOf(zzfb2.zzru);
                                            i17 = i2;
                                        }
                                    }
                                    if (zza3.zzxh.zzwo) {
                                        zzgi.zzb(zza3.zzxh, obj);
                                        i29 = i14;
                                    } else {
                                        int i44 = zzfc.zzrx[zza3.zzxh.zzwn.ordinal()];
                                        if (i44 != 17) {
                                            zzgb = obj;
                                        }
                                        Object zza4 = zzgi.zza(zza3.zzxh);
                                        zzgb = obj;
                                        if (zza4 != null) {
                                            zzgb = zzgt.zzb(zza4, obj);
                                        }
                                        zzgi.zza(zza3.zzxh, zzgb);
                                        i29 = i14;
                                    }
                                } else {
                                    switch (zzfc.zzrx[zza3.zzxh.zzwn.ordinal()]) {
                                        case 1:
                                            bArr2 = bArr;
                                            zzgb zzgb3 = new zzgb();
                                            i18 = zzez.zzf(bArr2, i14, zzgb3, zzfb2);
                                            zzgb2 = zzgb3;
                                            i14 = i18;
                                            i15 = i34;
                                            i17 = i2;
                                            zzgb = zzgb2;
                                            zzgi.zza(zza3.zzxh, zzgb);
                                            i29 = i14;
                                            break;
                                        case 2:
                                            bArr2 = bArr;
                                            ?? zzgo = new zzgo();
                                            i18 = zzez.zze(bArr2, i14, zzgo, zzfb2);
                                            zzgb2 = zzgo;
                                            i14 = i18;
                                            i15 = i34;
                                            i17 = i2;
                                            zzgb = zzgb2;
                                            zzgi.zza(zza3.zzxh, zzgb);
                                            i29 = i14;
                                            break;
                                        case 3:
                                        case 4:
                                            bArr2 = bArr;
                                            ?? zzhq = new zzhq();
                                            i18 = zzez.zzb(bArr2, i14, zzhq, zzfb2);
                                            zzgb2 = zzhq;
                                            i14 = i18;
                                            i15 = i34;
                                            i17 = i2;
                                            zzgb = zzgb2;
                                            zzgi.zza(zza3.zzxh, zzgb);
                                            i29 = i14;
                                            break;
                                        case 5:
                                        case 6:
                                            bArr2 = bArr;
                                            ?? zzgu = new zzgu();
                                            i18 = zzez.zza(bArr2, i14, (zzgz<?>) zzgu, zzfb2);
                                            zzgb2 = zzgu;
                                            i14 = i18;
                                            i15 = i34;
                                            i17 = i2;
                                            zzgb = zzgb2;
                                            zzgi.zza(zza3.zzxh, zzgb);
                                            i29 = i14;
                                            break;
                                        case 7:
                                        case 8:
                                            bArr2 = bArr;
                                            ?? zzhq2 = new zzhq();
                                            i18 = zzez.zzd(bArr2, i14, zzhq2, zzfb2);
                                            zzgb2 = zzhq2;
                                            i14 = i18;
                                            i15 = i34;
                                            i17 = i2;
                                            zzgb = zzgb2;
                                            zzgi.zza(zza3.zzxh, zzgb);
                                            i29 = i14;
                                            break;
                                        case 9:
                                        case 10:
                                            bArr2 = bArr;
                                            ?? zzgu2 = new zzgu();
                                            i18 = zzez.zzc(bArr2, i14, zzgu2, zzfb2);
                                            zzgb2 = zzgu2;
                                            i14 = i18;
                                            i15 = i34;
                                            i17 = i2;
                                            zzgb = zzgb2;
                                            zzgi.zza(zza3.zzxh, zzgb);
                                            i29 = i14;
                                            break;
                                        case 11:
                                            bArr2 = bArr;
                                            ?? zzff = new zzff();
                                            i18 = zzez.zzg(bArr2, i14, zzff, zzfb2);
                                            zzgb2 = zzff;
                                            i14 = i18;
                                            i15 = i34;
                                            i17 = i2;
                                            zzgb = zzgb2;
                                            zzgi.zza(zza3.zzxh, zzgb);
                                            i29 = i14;
                                            break;
                                        case 12:
                                            bArr2 = bArr;
                                            ?? zzgu3 = new zzgu();
                                            i18 = zzez.zzh(bArr2, i14, zzgu3, zzfb2);
                                            zzgb2 = zzgu3;
                                            i14 = i18;
                                            i15 = i34;
                                            i17 = i2;
                                            zzgb = zzgb2;
                                            zzgi.zza(zza3.zzxh, zzgb);
                                            i29 = i14;
                                            break;
                                        case 13:
                                            bArr2 = bArr;
                                            ?? zzhq3 = new zzhq();
                                            i18 = zzez.zzi(bArr2, i14, zzhq3, zzfb2);
                                            zzgb2 = zzhq3;
                                            i14 = i18;
                                            i15 = i34;
                                            i17 = i2;
                                            zzgb = zzgb2;
                                            zzgi.zza(zza3.zzxh, zzgb);
                                            i29 = i14;
                                            break;
                                        case 14:
                                            zzgu zzgu4 = new zzgu();
                                            bArr2 = bArr;
                                            i14 = zzez.zza(bArr2, i14, zzgu4, zzfb2);
                                            zzjm zzjm2 = ((zzgs.zze) t6).zzwj;
                                            if (zzjm2 == zzjm.zzig()) {
                                                zzjm2 = null;
                                            }
                                            zzjm zzjm3 = (zzjm) zzit.zza(i16, zzgu4, zza3.zzxh.zzwm, zzjm2, zzjj);
                                            if (zzjm3 != null) {
                                                ((zzgs.zze) t6).zzwj = zzjm3;
                                            }
                                            zzgi.zza(zza3.zzxh, zzgu4);
                                            break;
                                        default:
                                            String valueOf = String.valueOf(zza3.zzxh.zzwn);
                                            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
                                            sb.append("Type cannot be packed: ");
                                            sb.append(valueOf);
                                            throw new IllegalStateException(sb.toString());
                                    }
                                }
                                i15 = i34;
                                i17 = i2;
                                i29 = i14;
                            }
                            i32 = i13;
                            i30 = i16;
                            t5 = t3;
                            bArr3 = bArr2;
                            i33 = i11;
                            i31 = i10;
                            i27 = i17;
                            zzig2 = this;
                            i28 = i12;
                            zzfb3 = zzfb2;
                            unsafe2 = unsafe;
                            i34 = i15;
                        } else {
                            t3 = t;
                            bArr2 = bArr;
                        }
                    } else {
                        t3 = t;
                        bArr2 = bArr;
                        zzfb2 = zzfb;
                    }
                    i15 = i34;
                    i16 = i9;
                    i17 = i2;
                    i29 = zzez.zza(i13, bArr, i14, i2, zzt(t), zzfb);
                    i32 = i13;
                    i30 = i16;
                    t5 = t3;
                    bArr3 = bArr2;
                    i33 = i11;
                    i31 = i10;
                    i27 = i17;
                    zzig2 = this;
                    i28 = i12;
                    zzfb3 = zzfb2;
                    unsafe2 = unsafe;
                    i34 = i15;
                } else {
                    zzig = this;
                    t2 = t;
                    i29 = i14;
                    i6 = i34;
                    i32 = i13;
                    i4 = i12;
                    i33 = i11;
                    i7 = 1048575;
                    i5 = i2;
                }
            } else {
                unsafe = unsafe2;
                i4 = i28;
                i5 = i27;
                t2 = t5;
                zzig = zzig2;
                i6 = i34;
                i7 = 1048575;
            }
        }
        if (i6 != i7) {
            unsafe.putInt(t2, (long) i6, i33);
        }
        zzjm zzjm4 = null;
        for (int i45 = zzig.zzzk; i45 < zzig.zzzl; i45++) {
            zzjm4 = (zzjm) zzig.zza(t2, zzig.zzzj[i45], zzjm4, (zzjj<UT, UB>) zzig.zzzo);
        }
        if (zzjm4 != null) {
            zzig.zzzo.zzg(t2, zzjm4);
        }
        if (i4 == 0) {
            if (i29 != i5) {
                throw zzhc.zzgs();
            }
        } else if (i29 > i5 || i32 != i4) {
            throw zzhc.zzgs();
        }
        return i29;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v11, types: [int] */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x02dc, code lost:
        if (r0 == r4) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0323, code lost:
        if (r0 == r15) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0346, code lost:
        if (r0 == r15) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0348, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzir
    public final void zza(T t, byte[] bArr, int i, int i2, zzfb zzfb) throws IOException {
        byte b;
        int i3;
        int i4;
        Unsafe unsafe;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        Unsafe unsafe2;
        int i12;
        Unsafe unsafe3;
        int i13;
        Unsafe unsafe4;
        zzig<T> zzig = this;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i14 = i2;
        zzfb zzfb2 = zzfb;
        if (zzig.zzzh) {
            Unsafe unsafe5 = zzyz;
            int i15 = -1;
            int i16 = 1048575;
            int i17 = i;
            int i18 = -1;
            int i19 = 0;
            int i20 = 0;
            int i21 = 1048575;
            while (i17 < i14) {
                int i22 = i17 + 1;
                byte b2 = bArr2[i17];
                if (b2 < 0) {
                    i3 = zzez.zza(b2, bArr2, i22, zzfb2);
                    b = zzfb2.zzru;
                } else {
                    b = b2;
                    i3 = i22;
                }
                int i23 = b >>> 3;
                int i24 = b & 7;
                if (i23 > i18) {
                    i4 = zzig.zzs(i23, i19 / 3);
                } else {
                    i4 = zzig.zzbt(i23);
                }
                if (i4 == i15) {
                    i7 = i3;
                    i5 = i23;
                    unsafe = unsafe5;
                    i6 = 0;
                } else {
                    int[] iArr = zzig.zzza;
                    int i25 = iArr[i4 + 1];
                    int i26 = (i25 & 267386880) >>> 20;
                    long j = (long) (i25 & i16);
                    if (i26 <= 17) {
                        int i27 = iArr[i4 + 2];
                        int i28 = 1 << (i27 >>> 20);
                        int i29 = i27 & 1048575;
                        if (i29 != i21) {
                            if (i21 != 1048575) {
                                long j2 = (long) i21;
                                unsafe4 = unsafe5;
                                unsafe4.putInt(t2, j2, i20);
                            } else {
                                unsafe4 = unsafe5;
                            }
                            if (i29 != 1048575) {
                                i20 = unsafe4.getInt(t2, (long) i29);
                            }
                            unsafe2 = unsafe4;
                            i21 = i29;
                        } else {
                            unsafe2 = unsafe5;
                        }
                        switch (i26) {
                            case 0:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 1) {
                                    zzjp.zza(t2, j, zzez.zzc(bArr2, i13));
                                    i17 = i13 + 8;
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 1:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 5) {
                                    zzjp.zza((Object) t2, j, zzez.zzd(bArr2, i13));
                                    i17 = i13 + 4;
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 2:
                            case 3:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    int zzb = zzez.zzb(bArr2, i13, zzfb2);
                                    unsafe3.putLong(t, j, zzfb2.zzrv);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    i17 = zzb;
                                    break;
                                }
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 4:
                            case 11:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    i17 = zzez.zza(bArr2, i13, zzfb2);
                                    unsafe3.putInt(t2, j, zzfb2.zzru);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 5:
                            case 14:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 1) {
                                    unsafe3.putLong(t, j, zzez.zzb(bArr2, i3));
                                    i17 = i3 + 8;
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 6:
                            case 13:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 5) {
                                    unsafe3.putInt(t2, j, zzez.zza(bArr2, i3));
                                    i17 = i3 + 4;
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 7:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    i17 = zzez.zzb(bArr2, i3, zzfb2);
                                    zzjp.zza(t2, j, zzfb2.zzrv != 0);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 8:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 2) {
                                    if ((i25 & 536870912) == 0) {
                                        i17 = zzez.zzc(bArr2, i3, zzfb2);
                                    } else {
                                        i17 = zzez.zzd(bArr2, i3, zzfb2);
                                    }
                                    unsafe3.putObject(t2, j, zzfb2.zzrw);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 9:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 2) {
                                    i17 = zzez.zza(zzig.zzbn(i12), bArr2, i3, i14, zzfb2);
                                    Object object = unsafe3.getObject(t2, j);
                                    if (object == null) {
                                        unsafe3.putObject(t2, j, zzfb2.zzrw);
                                    } else {
                                        unsafe3.putObject(t2, j, zzgt.zzb(object, zzfb2.zzrw));
                                    }
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 10:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 2) {
                                    i17 = zzez.zze(bArr2, i3, zzfb2);
                                    unsafe3.putObject(t2, j, zzfb2.zzrw);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 12:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    i17 = zzez.zza(bArr2, i3, zzfb2);
                                    unsafe3.putInt(t2, j, zzfb2.zzru);
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 15:
                                i5 = i23;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                if (i24 == 0) {
                                    i17 = zzez.zza(bArr2, i3, zzfb2);
                                    unsafe3.putInt(t2, j, zzft.zzav(zzfb2.zzru));
                                    i20 |= i28;
                                    unsafe5 = unsafe3;
                                    i19 = i12;
                                    break;
                                }
                                i13 = i3;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                            case 16:
                                if (i24 != 0) {
                                    i5 = i23;
                                    i8 = i21;
                                    unsafe3 = unsafe2;
                                    i13 = i3;
                                    i12 = i4;
                                    i7 = i13;
                                    unsafe = unsafe3;
                                    i6 = i12;
                                    i21 = i8;
                                    break;
                                } else {
                                    int zzb2 = zzez.zzb(bArr2, i3, zzfb2);
                                    i8 = i21;
                                    i5 = i23;
                                    unsafe2.putLong(t, j, zzft.zzr(zzfb2.zzrv));
                                    i20 |= i28;
                                    unsafe5 = unsafe2;
                                    i19 = i4;
                                    i17 = zzb2;
                                    break;
                                }
                            default:
                                i5 = i23;
                                i13 = i3;
                                i12 = i4;
                                i8 = i21;
                                unsafe3 = unsafe2;
                                i7 = i13;
                                unsafe = unsafe3;
                                i6 = i12;
                                i21 = i8;
                                break;
                        }
                    } else {
                        i5 = i23;
                        i8 = i21;
                        if (i26 != 27) {
                            i6 = i4;
                            if (i26 <= 49) {
                                i10 = i20;
                                i9 = i8;
                                unsafe = unsafe5;
                                i17 = zza(t, bArr, i3, i2, b, i5, i24, i6, (long) i25, i26, j, zzfb);
                            } else {
                                i11 = i3;
                                i10 = i20;
                                unsafe = unsafe5;
                                i9 = i8;
                                if (i26 != 50) {
                                    i17 = zza(t, bArr, i11, i2, b, i5, i24, i25, i26, j, i6, zzfb);
                                } else if (i24 == 2) {
                                    i17 = zza(t, bArr, i11, i2, i6, j, zzfb);
                                }
                            }
                            zzig = this;
                            t2 = t;
                            bArr2 = bArr;
                            i14 = i2;
                            zzfb2 = zzfb;
                            i19 = i6;
                            i18 = i5;
                            i20 = i10;
                            i21 = i9;
                            unsafe5 = unsafe;
                            i16 = 1048575;
                            i15 = -1;
                        } else if (i24 == 2) {
                            zzgz zzgz = (zzgz) unsafe5.getObject(t2, j);
                            if (!zzgz.zzdo()) {
                                int size = zzgz.size();
                                zzgz = zzgz.zzah(size == 0 ? 10 : size << 1);
                                unsafe5.putObject(t2, j, zzgz);
                            }
                            i17 = zzez.zza(zzig.zzbn(i4), b, bArr, i3, i2, zzgz, zzfb);
                            unsafe5 = unsafe5;
                            i20 = i20;
                            i19 = i4;
                        } else {
                            i6 = i4;
                            i11 = i3;
                            i10 = i20;
                            unsafe = unsafe5;
                            i9 = i8;
                        }
                        i7 = i11;
                        i20 = i10;
                        i21 = i9;
                        i17 = zzez.zza(b, bArr, i7, i2, zzt(t), zzfb);
                        zzig = this;
                        t2 = t;
                        bArr2 = bArr;
                        i14 = i2;
                        zzfb2 = zzfb;
                        i19 = i6;
                        i18 = i5;
                        unsafe5 = unsafe;
                        i16 = 1048575;
                        i15 = -1;
                    }
                    i21 = i8;
                    i18 = i5;
                    i16 = 1048575;
                    i15 = -1;
                }
                i17 = zzez.zza(b, bArr, i7, i2, zzt(t), zzfb);
                zzig = this;
                t2 = t;
                bArr2 = bArr;
                i14 = i2;
                zzfb2 = zzfb;
                i19 = i6;
                i18 = i5;
                unsafe5 = unsafe;
                i16 = 1048575;
                i15 = -1;
            }
            if (i21 != 1048575) {
                unsafe5.putInt(t, (long) i21, i20);
            }
            if (i17 != i2) {
                throw zzhc.zzgs();
            }
            return;
        }
        zza(t, bArr, i, i2, 0, zzfb);
    }

    @Override // com.google.android.gms.internal.vision.zzir
    public final void zzh(T t) {
        int i;
        int i2 = this.zzzk;
        while (true) {
            i = this.zzzl;
            if (i2 >= i) {
                break;
            }
            long zzbq = (long) (zzbq(this.zzzj[i2]) & 1048575);
            Object zzp = zzjp.zzp(t, zzbq);
            if (zzp != null) {
                zzjp.zza(t, zzbq, this.zzzq.zzo(zzp));
            }
            i2++;
        }
        int length = this.zzzj.length;
        while (i < length) {
            this.zzzn.zzb(t, (long) this.zzzj[i]);
            i++;
        }
        this.zzzo.zzh(t);
        if (this.zzzf) {
            this.zzzp.zzh(t);
        }
    }

    private final <UT, UB> UB zza(Object obj, int i, UB ub, zzjj<UT, UB> zzjj) {
        zzgy zzbp;
        int i2 = this.zzza[i];
        Object zzp = zzjp.zzp(obj, (long) (zzbq(i) & 1048575));
        return (zzp == null || (zzbp = zzbp(i)) == null) ? ub : (UB) zza(i, i2, (Map<K, V>) this.zzzq.zzl(zzp), zzbp, ub, zzjj);
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map<K, V> map, zzgy zzgy, UB ub, zzjj<UT, UB> zzjj) {
        zzht<?, ?> zzq = this.zzzq.zzq(zzbo(i));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (!zzgy.zzg(next.getValue().intValue())) {
                if (ub == null) {
                    ub = zzjj.zzif();
                }
                zzfp zzaq = zzfh.zzaq(zzhu.zza(zzq, next.getKey(), next.getValue()));
                try {
                    zzhu.zza(zzaq.zzew(), zzq, next.getKey(), next.getValue());
                    zzjj.zza(ub, i2, zzaq.zzev());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v8, types: [com.google.android.gms.internal.vision.zzir] */
    /* JADX WARN: Type inference failed for: r1v21 */
    /* JADX WARN: Type inference failed for: r1v23, types: [com.google.android.gms.internal.vision.zzir] */
    /* JADX WARN: Type inference failed for: r1v30 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzir
    public final boolean zzu(T t) {
        int i;
        int i2;
        int i3 = 1048575;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            boolean z = true;
            if (i5 >= this.zzzk) {
                return !this.zzzf || this.zzzp.zzf(t).isInitialized();
            }
            int i6 = this.zzzj[i5];
            int i7 = this.zzza[i6];
            int zzbq = zzbq(i6);
            int i8 = this.zzza[i6 + 2];
            int i9 = i8 & 1048575;
            int i10 = 1 << (i8 >>> 20);
            if (i9 != i3) {
                if (i9 != 1048575) {
                    i4 = zzyz.getInt(t, (long) i9);
                }
                i = i4;
                i2 = i9;
            } else {
                i2 = i3;
                i = i4;
            }
            if (((268435456 & zzbq) != 0) && !zza(t, i6, i2, i, i10)) {
                return false;
            }
            int i11 = (267386880 & zzbq) >>> 20;
            if (i11 != 9 && i11 != 17) {
                if (i11 != 27) {
                    if (i11 == 60 || i11 == 68) {
                        if (zza(t, i7, i6) && !zza(t, zzbq, zzbn(i6))) {
                            return false;
                        }
                    } else if (i11 != 49) {
                        if (i11 != 50) {
                            continue;
                        } else {
                            Map<?, ?> zzm = this.zzzq.zzm(zzjp.zzp(t, (long) (zzbq & 1048575)));
                            if (!zzm.isEmpty()) {
                                if (this.zzzq.zzq(zzbo(i6)).zzyu.zzip() == zzkd.MESSAGE) {
                                    zzir<T> zzir = 0;
                                    Iterator<?> it = zzm.values().iterator();
                                    while (true) {
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        Object next = it.next();
                                        if (zzir == null) {
                                            zzir = zzin.zzho().zzf(next.getClass());
                                        }
                                        boolean zzu = zzir.zzu(next);
                                        zzir = zzir;
                                        if (!zzu) {
                                            z = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!z) {
                                return false;
                            }
                        }
                    }
                }
                List list = (List) zzjp.zzp(t, (long) (zzbq & 1048575));
                if (!list.isEmpty()) {
                    ?? zzbn = zzbn(i6);
                    int i12 = 0;
                    while (true) {
                        if (i12 >= list.size()) {
                            break;
                        } else if (!zzbn.zzu(list.get(i12))) {
                            z = false;
                            break;
                        } else {
                            i12++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (zza(t, i6, i2, i, i10) && !zza(t, zzbq, zzbn(i6))) {
                return false;
            }
            i5++;
            i3 = i2;
            i4 = i;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.google.android.gms.internal.vision.zzir */
    /* JADX WARN: Multi-variable type inference failed */
    private static boolean zza(Object obj, int i, zzir zzir) {
        return zzir.zzu(zzjp.zzp(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zzkg zzkg) throws IOException {
        if (obj instanceof String) {
            zzkg.zza(i, (String) obj);
        } else {
            zzkg.zza(i, (zzfh) obj);
        }
    }

    private final void zza(Object obj, int i, zzis zzis) throws IOException {
        if (zzbs(i)) {
            zzjp.zza(obj, (long) (i & 1048575), zzis.zzec());
        } else if (this.zzzg) {
            zzjp.zza(obj, (long) (i & 1048575), zzis.readString());
        } else {
            zzjp.zza(obj, (long) (i & 1048575), zzis.zzed());
        }
    }

    private final int zzbq(int i) {
        return this.zzza[i + 1];
    }

    private final int zzbr(int i) {
        return this.zzza[i + 2];
    }

    private static <T> double zzf(T t, long j) {
        return ((Double) zzjp.zzp(t, j)).doubleValue();
    }

    private static <T> float zzg(T t, long j) {
        return ((Float) zzjp.zzp(t, j)).floatValue();
    }

    private static <T> int zzh(T t, long j) {
        return ((Integer) zzjp.zzp(t, j)).intValue();
    }

    private static <T> long zzi(T t, long j) {
        return ((Long) zzjp.zzp(t, j)).longValue();
    }

    private static <T> boolean zzj(T t, long j) {
        return ((Boolean) zzjp.zzp(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zza(t, i) == zza(t2, i);
    }

    private final boolean zza(T t, int i, int i2, int i3, int i4) {
        if (i2 == 1048575) {
            return zza(t, i);
        }
        return (i3 & i4) != 0;
    }

    private final boolean zza(T t, int i) {
        int zzbr = zzbr(i);
        long j = (long) (zzbr & 1048575);
        if (j == 1048575) {
            int zzbq = zzbq(i);
            long j2 = (long) (zzbq & 1048575);
            switch ((zzbq & 267386880) >>> 20) {
                case 0:
                    return zzjp.zzo(t, j2) != 0.0d;
                case 1:
                    return zzjp.zzn(t, j2) != 0.0f;
                case 2:
                    return zzjp.zzl(t, j2) != 0;
                case 3:
                    return zzjp.zzl(t, j2) != 0;
                case 4:
                    return zzjp.zzk(t, j2) != 0;
                case 5:
                    return zzjp.zzl(t, j2) != 0;
                case 6:
                    return zzjp.zzk(t, j2) != 0;
                case 7:
                    return zzjp.zzm(t, j2);
                case 8:
                    Object zzp = zzjp.zzp(t, j2);
                    if (zzp instanceof String) {
                        return !((String) zzp).isEmpty();
                    }
                    if (zzp instanceof zzfh) {
                        return !zzfh.zzsd.equals(zzp);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzjp.zzp(t, j2) != null;
                case 10:
                    return !zzfh.zzsd.equals(zzjp.zzp(t, j2));
                case 11:
                    return zzjp.zzk(t, j2) != 0;
                case 12:
                    return zzjp.zzk(t, j2) != 0;
                case 13:
                    return zzjp.zzk(t, j2) != 0;
                case 14:
                    return zzjp.zzl(t, j2) != 0;
                case 15:
                    return zzjp.zzk(t, j2) != 0;
                case 16:
                    return zzjp.zzl(t, j2) != 0;
                case 17:
                    return zzjp.zzp(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            return (zzjp.zzk(t, j) & (1 << (zzbr >>> 20))) != 0;
        }
    }

    private final void zzb(T t, int i) {
        int zzbr = zzbr(i);
        long j = (long) (1048575 & zzbr);
        if (j != 1048575) {
            zzjp.zzb(t, j, (1 << (zzbr >>> 20)) | zzjp.zzk(t, j));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzjp.zzk(t, (long) (zzbr(i2) & 1048575)) == i;
    }

    private final void zzb(T t, int i, int i2) {
        zzjp.zzb(t, (long) (zzbr(i2) & 1048575), i);
    }

    private final int zzbt(int i) {
        if (i < this.zzzc || i > this.zzzd) {
            return -1;
        }
        return zzt(i, 0);
    }

    private final int zzs(int i, int i2) {
        if (i < this.zzzc || i > this.zzzd) {
            return -1;
        }
        return zzt(i, i2);
    }

    private final int zzt(int i, int i2) {
        int length = (this.zzza.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.zzza[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }
}
