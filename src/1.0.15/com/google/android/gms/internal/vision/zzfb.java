package com.google.android.gms.internal.vision;

import java.util.Objects;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzfb {
    public final zzgd zzcn;
    public int zzru;
    public long zzrv;
    public Object zzrw;

    zzfb() {
        this.zzcn = zzgd.zzfl();
    }

    zzfb(zzgd zzgd) {
        Objects.requireNonNull(zzgd);
        this.zzcn = zzgd;
    }
}
