package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzht<K, V> {
    public final V zzgd;
    public final zzka zzys;
    public final K zzyt;
    public final zzka zzyu;
}
