package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzgs;
import kotlin.text.Typography;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzea {

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzb extends zzgs<zzb, zza> implements zzie {
        private static volatile zzil<zzb> zzbd;
        private static final zzha<Integer, zzeo> zzmr = new zzeb();
        private static final zzb zzms;
        private zzgx zzmq = zzgg();

        private zzb() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzb, zza> implements zzie {
            private zza() {
                super(zzb.zzms);
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzb>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzb> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzms, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001e", new Object[]{"zzmq", zzeo.zzah()});
                case 4:
                    return zzms;
                case 5:
                    zzil<zzb> zzil2 = zzbd;
                    zzil<zzb> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzb.class) {
                            zzil<zzb> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzms);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.gms.internal.vision.zzeb, com.google.android.gms.internal.vision.zzha<java.lang.Integer, com.google.android.gms.internal.vision.zzeo>] */
        static {
            zzb zzb = new zzb();
            zzms = zzb;
            zzgs.zza(zzb.class, zzb);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzc extends zzgs<zzc, zza> implements zzie {
        private static volatile zzil<zzc> zzbd;
        private static final zzc zzmw;
        private int zzbf;
        private int zzmt;
        private int zzmu;
        private String zzmv = "";

        private zzc() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzc, zza> implements zzie {
            private zza() {
                super(zzc.zzmw);
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzc>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzc> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzc();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzmw, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဈ\u0002", new Object[]{"zzbf", "zzmt", zzeo.zzah(), "zzmu", zzes.zzah(), "zzmv"});
                case 4:
                    return zzmw;
                case 5:
                    zzil<zzc> zzil2 = zzbd;
                    zzil<zzc> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzc.class) {
                            zzil<zzc> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzmw);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzc zzc = new zzc();
            zzmw = zzc;
            zzgs.zza(zzc.class, zzc);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zze extends zzgs<zze, zzb> implements zzie {
        private static volatile zzil<zze> zzbd;
        private static final zze zznh;
        private int zzbf;
        private String zzmz = "";
        private boolean zzna;
        private int zznb;
        private long zznc;
        private long zznd;
        private long zzne;
        private String zznf = "";
        private boolean zzng;

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public enum zza implements zzgw {
            REASON_UNKNOWN(0),
            REASON_MISSING(1),
            REASON_UPGRADE(2),
            REASON_INVALID(3);
            
            private static final zzgv<zza> zzhc = new zzed();
            private final int value;

            @Override // com.google.android.gms.internal.vision.zzgw
            public final int zzag() {
                return this.value;
            }

            public static zza zzt(int i) {
                if (i == 0) {
                    return REASON_UNKNOWN;
                }
                if (i == 1) {
                    return REASON_MISSING;
                }
                if (i == 2) {
                    return REASON_UPGRADE;
                }
                if (i != 3) {
                    return null;
                }
                return REASON_INVALID;
            }

            public static zzgy zzah() {
                return zzec.zzhf;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        private zze() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zzb extends zzgs.zza<zze, zzb> implements zzie {
            private zzb() {
                super(zze.zznh);
            }

            /* synthetic */ zzb(zzdz zzdz) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zze>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zze> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zze();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zznh, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဇ\u0001\u0003ဌ\u0002\u0004ဂ\u0003\u0005ဂ\u0004\u0006ဂ\u0005\u0007ဈ\u0006\bဇ\u0007", new Object[]{"zzbf", "zzmz", "zzna", "zznb", zza.zzah(), "zznc", "zznd", "zzne", "zznf", "zzng"});
                case 4:
                    return zznh;
                case 5:
                    zzil<zze> zzil2 = zzbd;
                    zzil<zze> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zze.class) {
                            zzil<zze> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zznh);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zze zze = new zze();
            zznh = zze;
            zzgs.zza(zze.class, zze);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzl extends zzgs<zzl, zza> implements zzie {
        private static volatile zzil<zzl> zzbd;
        private static final zzl zzpu;
        private int zzbf;
        private long zzns;
        private long zznt;

        private zzl() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzl, zza> implements zzie {
            private zza() {
                super(zzl.zzpu);
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzl>, com.google.android.gms.internal.vision.zzgs$zzc] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzl> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzl();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzpu, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဂ\u0000\u0002ဂ\u0001", new Object[]{"zzbf", "zzns", "zznt"});
                case 4:
                    return zzpu;
                case 5:
                    zzil<zzl> zzil2 = zzbd;
                    zzil<zzl> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzl.class) {
                            zzil<zzl> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzpu);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzl zzl = new zzl();
            zzpu = zzl;
            zzgs.zza(zzl.class, zzl);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zza extends zzgs<zza, C0020zza> implements zzie {
        private static volatile zzil<zza> zzbd;
        private static final zza zzmp;
        private int zzbf;
        private String zzmn = "";
        private String zzmo = "";

        private zza() {
        }

        /* renamed from: com.google.android.gms.internal.vision.zzea$zza$zza  reason: collision with other inner class name */
        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class C0020zza extends zzgs.zza<zza, C0020zza> implements zzie {
            private C0020zza() {
                super(zza.zzmp);
            }

            public final C0020zza zzl(String str) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zza) this.zzwh).zzn(str);
                return this;
            }

            public final C0020zza zzm(String str) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zza) this.zzwh).zzo(str);
                return this;
            }

            /* synthetic */ C0020zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzn(String str) {
            str.getClass();
            this.zzbf |= 1;
            this.zzmn = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzo(String str) {
            str.getClass();
            this.zzbf |= 2;
            this.zzmo = str;
        }

        public static C0020zza zzcj() {
            return (C0020zza) zzmp.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zza>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zza> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0020zza(null);
                case 3:
                    return zza(zzmp, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001", new Object[]{"zzbf", "zzmn", "zzmo"});
                case 4:
                    return zzmp;
                case 5:
                    zzil<zza> zzil2 = zzbd;
                    zzil<zza> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zza.class) {
                            zzil<zza> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzmp);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zza zza = new zza();
            zzmp = zza;
            zzgs.zza(zza.class, zza);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzd extends zzgs<zzd, zza> implements zzie {
        private static volatile zzil<zzd> zzbd;
        private static final zzd zzmy;
        private zzgz<zzm> zzmx = zzgh();

        private zzd() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzd, zza> implements zzie {
            private zza() {
                super(zzd.zzmy);
            }

            public final zza zzb(zzm zzm) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zza((zzd) zzm);
                return this;
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzm zzm) {
            zzm.getClass();
            if (!this.zzmx.zzdo()) {
                this.zzmx = zzgs.zza(this.zzmx);
            }
            this.zzmx.add(zzm);
        }

        public static zza zzcn() {
            return (zza) zzmy.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzd>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzd> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzd();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzmy, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzmx", zzm.class});
                case 4:
                    return zzmy;
                case 5:
                    zzil<zzd> zzil2 = zzbd;
                    zzil<zzd> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzd.class) {
                            zzil<zzd> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzmy);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzd zzd = new zzd();
            zzmy = zzd;
            zzgs.zza(zzd.class, zzd);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzf extends zzgs<zzf, zza> implements zzie {
        private static volatile zzil<zzf> zzbd;
        private static final zzf zznv;
        private int zzbf;
        private String zznn = "";
        private String zzno = "";
        private zzgz<String> zznp = zzgs.zzgh();
        private int zznq;
        private String zznr = "";
        private long zzns;
        private long zznt;
        private zzgz<zzn> zznu = zzgh();

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public enum zzb implements zzgw {
            RESULT_UNKNOWN(0),
            RESULT_SUCCESS(1),
            RESULT_FAIL(2),
            RESULT_SKIPPED(3);
            
            private static final zzgv<zzb> zzhc = new zzee();
            private final int value;

            @Override // com.google.android.gms.internal.vision.zzgw
            public final int zzag() {
                return this.value;
            }

            public static zzb zzu(int i) {
                if (i == 0) {
                    return RESULT_UNKNOWN;
                }
                if (i == 1) {
                    return RESULT_SUCCESS;
                }
                if (i == 2) {
                    return RESULT_FAIL;
                }
                if (i != 3) {
                    return null;
                }
                return RESULT_SKIPPED;
            }

            public static zzgy zzah() {
                return zzef.zzhf;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        private zzf() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzf, zza> implements zzie {
            private zza() {
                super(zzf.zznv);
            }

            public final zza zzp(String str) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzf) this.zzwh).setName(str);
                return this;
            }

            public final zza zzq(String str) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzf) this.zzwh).zzr(str);
                return this;
            }

            public final zza zzd(long j) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzf) this.zzwh).zzf(j);
                return this;
            }

            public final zza zze(long j) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzf) this.zzwh).zzg(j);
                return this;
            }

            public final zza zzc(Iterable<? extends zzn> iterable) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzf) this.zzwh).zzd((zzf) iterable);
                return this;
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void setName(String str) {
            str.getClass();
            this.zzbf |= 1;
            this.zznn = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzr(String str) {
            str.getClass();
            this.zzbf |= 8;
            this.zznr = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzf(long j) {
            this.zzbf |= 16;
            this.zzns = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzg(long j) {
            this.zzbf |= 32;
            this.zznt = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzd(Iterable<? extends zzn> iterable) {
            if (!this.zznu.zzdo()) {
                this.zznu = zzgs.zza(this.zznu);
            }
            zzet.zza(iterable, this.zznu);
        }

        public static zza zzcq() {
            return (zza) zznv.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzf>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzf> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzf();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zznv, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0002\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003\u001a\u0004ဌ\u0002\u0005ဈ\u0003\u0006ဂ\u0004\u0007ဂ\u0005\b\u001b", new Object[]{"zzbf", "zznn", "zzno", "zznp", "zznq", zzb.zzah(), "zznr", "zzns", "zznt", "zznu", zzn.class});
                case 4:
                    return zznv;
                case 5:
                    zzil<zzf> zzil2 = zzbd;
                    zzil<zzf> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzf.class) {
                            zzil<zzf> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zznv);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzf zzf = new zzf();
            zznv = zzf;
            zzgs.zza(zzf.class, zzf);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzg extends zzgs<zzg, zzb> implements zzie {
        private static volatile zzil<zzg> zzbd;
        private static final zzg zzof;
        private int zzbf;
        private float zzka;
        private boolean zzke;
        private int zzob;
        private int zzoc;
        private int zzod;
        private boolean zzoe;

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public enum zza implements zzgw {
            CLASSIFICATION_UNKNOWN(0),
            CLASSIFICATION_NONE(1),
            CLASSIFICATION_ALL(2);
            
            private static final zzgv<zza> zzhc = new zzeh();
            private final int value;

            @Override // com.google.android.gms.internal.vision.zzgw
            public final int zzag() {
                return this.value;
            }

            public static zza zzv(int i) {
                if (i == 0) {
                    return CLASSIFICATION_UNKNOWN;
                }
                if (i == 1) {
                    return CLASSIFICATION_NONE;
                }
                if (i != 2) {
                    return null;
                }
                return CLASSIFICATION_ALL;
            }

            public static zzgy zzah() {
                return zzeg.zzhf;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public enum zzc implements zzgw {
            LANDMARK_UNKNOWN(0),
            LANDMARK_NONE(1),
            LANDMARK_ALL(2),
            LANDMARK_CONTOUR(3);
            
            private static final zzgv<zzc> zzhc = new zzei();
            private final int value;

            @Override // com.google.android.gms.internal.vision.zzgw
            public final int zzag() {
                return this.value;
            }

            public static zzc zzw(int i) {
                if (i == 0) {
                    return LANDMARK_UNKNOWN;
                }
                if (i == 1) {
                    return LANDMARK_NONE;
                }
                if (i == 2) {
                    return LANDMARK_ALL;
                }
                if (i != 3) {
                    return null;
                }
                return LANDMARK_CONTOUR;
            }

            public static zzgy zzah() {
                return zzej.zzhf;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzc(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public enum zzd implements zzgw {
            MODE_UNKNOWN(0),
            MODE_ACCURATE(1),
            MODE_FAST(2),
            MODE_SELFIE(3);
            
            private static final zzgv<zzd> zzhc = new zzel();
            private final int value;

            @Override // com.google.android.gms.internal.vision.zzgw
            public final int zzag() {
                return this.value;
            }

            public static zzd zzx(int i) {
                if (i == 0) {
                    return MODE_UNKNOWN;
                }
                if (i == 1) {
                    return MODE_ACCURATE;
                }
                if (i == 2) {
                    return MODE_FAST;
                }
                if (i != 3) {
                    return null;
                }
                return MODE_SELFIE;
            }

            public static zzgy zzah() {
                return zzek.zzhf;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzd(int i) {
                this.value = i;
            }
        }

        private zzg() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zzb extends zzgs.zza<zzg, zzb> implements zzie {
            private zzb() {
                super(zzg.zzof);
            }

            public final zzb zzb(zzd zzd) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzg) this.zzwh).zza((zzg) zzd);
                return this;
            }

            public final zzb zzb(zzc zzc) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzg) this.zzwh).zza((zzg) zzc);
                return this;
            }

            public final zzb zzb(zza zza) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzg) this.zzwh).zza((zzg) zza);
                return this;
            }

            public final zzb zzh(boolean z) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzg) this.zzwh).zza((zzg) z);
                return this;
            }

            public final zzb zzi(boolean z) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzg) this.zzwh).zzg(z);
                return this;
            }

            public final zzb zzf(float f) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzg) this.zzwh).zzd((zzg) f);
                return this;
            }

            /* synthetic */ zzb(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzd zzd2) {
            this.zzob = zzd2.zzag();
            this.zzbf |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzc zzc2) {
            this.zzoc = zzc2.zzag();
            this.zzbf |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zza zza2) {
            this.zzod = zza2.zzag();
            this.zzbf |= 4;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(boolean z) {
            this.zzbf |= 8;
            this.zzke = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzg(boolean z) {
            this.zzbf |= 16;
            this.zzoe = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzd(float f) {
            this.zzbf |= 32;
            this.zzka = f;
        }

        public static zzb zzcs() {
            return (zzb) zzof.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzg>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzg> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzg();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzof, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဌ\u0002\u0004ဇ\u0003\u0005ဇ\u0004\u0006ခ\u0005", new Object[]{"zzbf", "zzob", zzd.zzah(), "zzoc", zzc.zzah(), "zzod", zza.zzah(), "zzke", "zzoe", "zzka"});
                case 4:
                    return zzof;
                case 5:
                    zzil<zzg> zzil2 = zzbd;
                    zzil<zzg> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzg.class) {
                            zzil<zzg> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc2 = new zzgs.zzc(zzof);
                                zzbd = zzc2;
                                zzil = zzc2;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzg zzg = new zzg();
            zzof = zzg;
            zzgs.zza(zzg.class, zzg);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzh extends zzgs<zzh, zza> implements zzie {
        private static volatile zzil<zzh> zzbd;
        private static final zzh zzpa;
        private int zzbf;
        private float zzou;
        private float zzov;
        private float zzow;
        private float zzox;
        private float zzoy;
        private float zzoz;

        private zzh() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzh, zza> implements zzie {
            private zza() {
                super(zzh.zzpa);
            }

            public final zza zzg(float f) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzh) this.zzwh).zzm(f);
                return this;
            }

            public final zza zzh(float f) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzh) this.zzwh).zzn(f);
                return this;
            }

            public final zza zzi(float f) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzh) this.zzwh).zzo(f);
                return this;
            }

            public final zza zzj(float f) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzh) this.zzwh).zzp(f);
                return this;
            }

            public final zza zzk(float f) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzh) this.zzwh).zzq(f);
                return this;
            }

            public final zza zzl(float f) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzh) this.zzwh).zzr(f);
                return this;
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzm(float f) {
            this.zzbf |= 1;
            this.zzou = f;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzn(float f) {
            this.zzbf |= 2;
            this.zzov = f;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzo(float f) {
            this.zzbf |= 4;
            this.zzow = f;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzp(float f) {
            this.zzbf |= 8;
            this.zzox = f;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzq(float f) {
            this.zzbf |= 16;
            this.zzoy = f;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzr(float f) {
            this.zzbf |= 32;
            this.zzoz = f;
        }

        public static zza zzcu() {
            return (zza) zzpa.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzh>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzh> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzh();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzpa, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ခ\u0000\u0002ခ\u0001\u0003ခ\u0002\u0004ခ\u0003\u0005ခ\u0004\u0006ခ\u0005", new Object[]{"zzbf", "zzou", "zzov", "zzow", "zzox", "zzoy", "zzoz"});
                case 4:
                    return zzpa;
                case 5:
                    zzil<zzh> zzil2 = zzbd;
                    zzil<zzh> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzh.class) {
                            zzil<zzh> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzpa);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzh zzh = new zzh();
            zzpa = zzh;
            zzgs.zza(zzh.class, zzh);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzi extends zzgs<zzi, zza> implements zzie {
        private static volatile zzil<zzi> zzbd;
        private static final zzi zzpe;
        private int zzbf;
        private zzj zzpb;
        private zzl zzpc;
        private zzgz<zzf> zzpd = zzgh();

        private zzi() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzi, zza> implements zzie {
            private zza() {
                super(zzi.zzpe);
            }

            public final zza zza(zzj zzj) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzi) this.zzwh).zzb((zzi) zzj);
                return this;
            }

            public final zza zza(zzf.zza zza) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzi) this.zzwh).zza((zzi) ((zzf) ((zzgs) zza.zzgc())));
                return this;
            }

            public final zza zze(Iterable<? extends zzf> iterable) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzi) this.zzwh).zzf(iterable);
                return this;
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzj zzj) {
            zzj.getClass();
            this.zzpb = zzj;
            this.zzbf |= 1;
        }

        private final void zzcw() {
            if (!this.zzpd.zzdo()) {
                this.zzpd = zzgs.zza(this.zzpd);
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzf zzf) {
            zzf.getClass();
            zzcw();
            this.zzpd.add(zzf);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzf(Iterable<? extends zzf> iterable) {
            zzcw();
            zzet.zza(iterable, this.zzpd);
        }

        public static zza zzcx() {
            return (zza) zzpe.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzi>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzi> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzi();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzpe, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0001\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003\u001b", new Object[]{"zzbf", "zzpb", "zzpc", "zzpd", zzf.class});
                case 4:
                    return zzpe;
                case 5:
                    zzil<zzi> zzil2 = zzbd;
                    zzil<zzi> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzi.class) {
                            zzil<zzi> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzpe);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzi zzi = new zzi();
            zzpe = zzi;
            zzgs.zza(zzi.class, zzi);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzj extends zzgs<zzj, zza> implements zzie {
        private static volatile zzil<zzj> zzbd;
        private static final zzj zzpj;
        private int zzbf;
        private int zzmt;
        private long zzpf;
        private long zzpg;
        private long zzph;
        private long zzpi;

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public enum zzb implements zzgw {
            FORMAT_UNKNOWN(0),
            FORMAT_LUMINANCE(1),
            FORMAT_RGB8(2),
            FORMAT_MONOCHROME(3);
            
            private static final zzgv<zzb> zzhc = new zzem();
            private final int value;

            @Override // com.google.android.gms.internal.vision.zzgw
            public final int zzag() {
                return this.value;
            }

            public static zzb zzy(int i) {
                if (i == 0) {
                    return FORMAT_UNKNOWN;
                }
                if (i == 1) {
                    return FORMAT_LUMINANCE;
                }
                if (i == 2) {
                    return FORMAT_RGB8;
                }
                if (i != 3) {
                    return null;
                }
                return FORMAT_MONOCHROME;
            }

            public static zzgy zzah() {
                return zzen.zzhf;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        private zzj() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzj, zza> implements zzie {
            private zza() {
                super(zzj.zzpj);
            }

            public final zza zzh(long j) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzj) this.zzwh).zzl(j);
                return this;
            }

            public final zza zzi(long j) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzj) this.zzwh).zzm(j);
                return this;
            }

            public final zza zzj(long j) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzj) this.zzwh).zzn(j);
                return this;
            }

            public final zza zzk(long j) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzj) this.zzwh).zzo(j);
                return this;
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzl(long j) {
            this.zzbf |= 2;
            this.zzpf = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzm(long j) {
            this.zzbf |= 4;
            this.zzpg = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzn(long j) {
            this.zzbf |= 8;
            this.zzph = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzo(long j) {
            this.zzbf |= 16;
            this.zzpi = j;
        }

        public static zza zzcz() {
            return (zza) zzpj.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzj>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzj> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzj();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzpj, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဂ\u0001\u0003ဂ\u0002\u0004ဂ\u0004\u0005ဂ\u0003", new Object[]{"zzbf", "zzmt", zzb.zzah(), "zzpf", "zzpg", "zzpi", "zzph"});
                case 4:
                    return zzpj;
                case 5:
                    zzil<zzj> zzil2 = zzbd;
                    zzil<zzj> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzj.class) {
                            zzil<zzj> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzpj);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzj zzj = new zzj();
            zzpj = zzj;
            zzgs.zza(zzj.class, zzj);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzk extends zzgs<zzk, zza> implements zzie {
        private static volatile zzil<zzk> zzbd;
        private static final zzk zzpt;
        private int zzbf;
        private String zznf = "";
        private String zznn = "";
        private long zzpp;
        private zza zzpq;
        private zzg zzpr;
        private zzb zzps;

        private zzk() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzk, zza> implements zzie {
            private zza() {
                super(zzk.zzpt);
            }

            public final zza zzt(String str) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzk) this.zzwh).setName(str);
                return this;
            }

            public final zza zzq(long j) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzk) this.zzwh).zzp(j);
                return this;
            }

            public final zza zzb(zza zza) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzk) this.zzwh).zza((zzk) zza);
                return this;
            }

            public final zza zzu(String str) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzk) this.zzwh).zzs(str);
                return this;
            }

            public final zza zza(zzg.zzb zzb) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzk) this.zzwh).zza((zzk) ((zzg) ((zzgs) zzb.zzgc())));
                return this;
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void setName(String str) {
            str.getClass();
            this.zzbf |= 1;
            this.zznn = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzp(long j) {
            this.zzbf |= 2;
            this.zzpp = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zza zza2) {
            zza2.getClass();
            this.zzpq = zza2;
            this.zzbf |= 4;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzs(String str) {
            str.getClass();
            this.zzbf |= 8;
            this.zznf = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzg zzg) {
            zzg.getClass();
            this.zzpr = zzg;
            this.zzbf |= 16;
        }

        public static zza zzdb() {
            return (zza) zzpt.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzk>, com.google.android.gms.internal.vision.zzgs$zzc] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzk> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzk();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzpt, "\u0001\u0006\u0000\u0001\u0001\u0011\u0006\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဂ\u0001\u0003ဉ\u0002\u0006ဈ\u0003\u0010ဉ\u0004\u0011ဉ\u0005", new Object[]{"zzbf", "zznn", "zzpp", "zzpq", "zznf", "zzpr", "zzps"});
                case 4:
                    return zzpt;
                case 5:
                    zzil<zzk> zzil2 = zzbd;
                    zzil<zzk> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzk.class) {
                            zzil<zzk> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzpt);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzk zzk = new zzk();
            zzpt = zzk;
            zzgs.zza(zzk.class, zzk);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzm extends zzgs<zzm, zza> implements zzie {
        private static volatile zzil<zzm> zzbd;
        private static final zzm zzpx;
        private int zzbf;
        private int zzpv;
        private int zzpw;

        private zzm() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzm, zza> implements zzie {
            private zza() {
                super(zzm.zzpx);
            }

            public final zza zzz(int i) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzm) this.zzwh).setX(i);
                return this;
            }

            public final zza zzaa(int i) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzm) this.zzwh).setY(i);
                return this;
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void setX(int i) {
            this.zzbf |= 1;
            this.zzpv = i;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void setY(int i) {
            this.zzbf |= 2;
            this.zzpw = i;
        }

        public static zza zzde() {
            return (zza) zzpx.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzm>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzm> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzm();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzpx, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001င\u0000\u0002င\u0001", new Object[]{"zzbf", "zzpv", "zzpw"});
                case 4:
                    return zzpx;
                case 5:
                    zzil<zzm> zzil2 = zzbd;
                    zzil<zzm> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzm.class) {
                            zzil<zzm> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzpx);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzm zzm = new zzm();
            zzpx = zzm;
            zzgs.zza(zzm.class, zzm);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzn extends zzgs<zzn, zza> implements zzie {
        private static volatile zzil<zzn> zzbd;
        private static final zzn zzqc;
        private int zzbf;
        private zzd zzpy;
        private int zzpz;
        private zzh zzqa;
        private zzc zzqb;

        private zzn() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzn, zza> implements zzie {
            private zza() {
                super(zzn.zzqc);
            }

            public final zza zza(zzd.zza zza) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzn) this.zzwh).zza((zzn) ((zzd) ((zzgs) zza.zzgc())));
                return this;
            }

            public final zza zzab(int i) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzn) this.zzwh).setId(i);
                return this;
            }

            public final zza zzb(zzh zzh) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzn) this.zzwh).zza((zzn) zzh);
                return this;
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzd zzd) {
            zzd.getClass();
            this.zzpy = zzd;
            this.zzbf |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void setId(int i) {
            this.zzbf |= 2;
            this.zzpz = i;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzh zzh) {
            zzh.getClass();
            this.zzqa = zzh;
            this.zzbf |= 4;
        }

        public static zza zzdg() {
            return (zza) zzqc.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzn>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzn> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzn();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzqc, "\u0001\u0004\u0000\u0001\u0001\u0011\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002င\u0001\u0010ဉ\u0002\u0011ဉ\u0003", new Object[]{"zzbf", "zzpy", "zzpz", "zzqa", "zzqb"});
                case 4:
                    return zzqc;
                case 5:
                    zzil<zzn> zzil2 = zzbd;
                    zzil<zzn> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzn.class) {
                            zzil<zzn> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzqc);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzn zzn = new zzn();
            zzqc = zzn;
            zzgs.zza(zzn.class, zzn);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static final class zzo extends zzgs<zzo, zza> implements zzie {
        private static volatile zzil<zzo> zzbd;
        private static final zzo zzqh;
        private int zzbf;
        private zze zzqd;
        private zzk zzqe;
        private zzi zzqf;
        private int zzqg;

        private zzo() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
        public static final class zza extends zzgs.zza<zzo, zza> implements zzie {
            private zza() {
                super(zzo.zzqh);
            }

            public final zza zza(zzk.zza zza) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzo) this.zzwh).zza((zzo) ((zzk) ((zzgs) zza.zzgc())));
                return this;
            }

            public final zza zzb(zzi zzi) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzo) this.zzwh).zza((zzo) zzi);
                return this;
            }

            /* synthetic */ zza(zzdz zzdz) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzk zzk) {
            zzk.getClass();
            this.zzqe = zzk;
            this.zzbf |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzi zzi) {
            zzi.getClass();
            this.zzqf = zzi;
            this.zzbf |= 4;
        }

        public static zza zzdi() {
            return (zza) zzqh.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzea$zzo>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzo> zzil;
            switch (zzdz.zzbe[i - 1]) {
                case 1:
                    return new zzo();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzqh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004င\u0003", new Object[]{"zzbf", "zzqd", "zzqe", "zzqf", "zzqg"});
                case 4:
                    return zzqh;
                case 5:
                    zzil<zzo> zzil2 = zzbd;
                    zzil<zzo> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzo.class) {
                            zzil<zzo> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzqh);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzo zzo = new zzo();
            zzqh = zzo;
            zzgs.zza(zzo.class, zzo);
        }
    }
}
