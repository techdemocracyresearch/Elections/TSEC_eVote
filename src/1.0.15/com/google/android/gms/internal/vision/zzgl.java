package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final /* synthetic */ class zzgl {
    static final /* synthetic */ int[] zzrx;
    static final /* synthetic */ int[] zztn;

    /* JADX WARNING: Can't wrap try/catch for region: R(55:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|(3:71|72|74)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(56:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|(3:71|72|74)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(58:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(59:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(60:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(62:0|(2:1|2)|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|(2:25|26)|27|29|30|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(65:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|25|26|27|29|30|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x006c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0078 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x0084 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x0090 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x009c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00a8 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00b4 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x00c0 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00cc */
    /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x00e9 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x00f3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x00fd */
    /* JADX WARNING: Missing exception handler attribute for start block: B:63:0x0107 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:65:0x0111 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:67:0x011b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:69:0x0125 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:71:0x012f */
    static {
        int[] iArr = new int[zzka.values().length];
        zzrx = iArr;
        try {
            iArr[zzka.DOUBLE.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            zzrx[zzka.FLOAT.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            zzrx[zzka.INT64.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        try {
            zzrx[zzka.UINT64.ordinal()] = 4;
        } catch (NoSuchFieldError unused4) {
        }
        try {
            zzrx[zzka.INT32.ordinal()] = 5;
        } catch (NoSuchFieldError unused5) {
        }
        try {
            zzrx[zzka.FIXED64.ordinal()] = 6;
        } catch (NoSuchFieldError unused6) {
        }
        try {
            zzrx[zzka.FIXED32.ordinal()] = 7;
        } catch (NoSuchFieldError unused7) {
        }
        try {
            zzrx[zzka.BOOL.ordinal()] = 8;
        } catch (NoSuchFieldError unused8) {
        }
        zzrx[zzka.GROUP.ordinal()] = 9;
        zzrx[zzka.MESSAGE.ordinal()] = 10;
        zzrx[zzka.STRING.ordinal()] = 11;
        zzrx[zzka.BYTES.ordinal()] = 12;
        zzrx[zzka.UINT32.ordinal()] = 13;
        zzrx[zzka.SFIXED32.ordinal()] = 14;
        zzrx[zzka.SFIXED64.ordinal()] = 15;
        zzrx[zzka.SINT32.ordinal()] = 16;
        zzrx[zzka.SINT64.ordinal()] = 17;
        try {
            zzrx[zzka.ENUM.ordinal()] = 18;
        } catch (NoSuchFieldError unused9) {
        }
        int[] iArr2 = new int[zzkd.values().length];
        zztn = iArr2;
        iArr2[zzkd.INT.ordinal()] = 1;
        zztn[zzkd.LONG.ordinal()] = 2;
        zztn[zzkd.FLOAT.ordinal()] = 3;
        zztn[zzkd.DOUBLE.ordinal()] = 4;
        zztn[zzkd.BOOLEAN.ordinal()] = 5;
        zztn[zzkd.STRING.ordinal()] = 6;
        zztn[zzkd.BYTE_STRING.ordinal()] = 7;
        zztn[zzkd.ENUM.ordinal()] = 8;
        try {
            zztn[zzkd.MESSAGE.ordinal()] = 9;
        } catch (NoSuchFieldError unused10) {
        }
    }
}
