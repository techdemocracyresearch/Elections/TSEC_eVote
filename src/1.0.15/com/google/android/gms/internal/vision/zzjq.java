package com.google.android.gms.internal.vision;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzjq implements Iterator<String> {
    private final /* synthetic */ zzjo zzaat;
    private Iterator<String> zzabp;

    zzjq(zzjo zzjo) {
        this.zzaat = zzjo;
        this.zzabp = zzjo.zzaau.iterator();
    }

    public final boolean hasNext() {
        return this.zzabp.hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public final /* synthetic */ String next() {
        return this.zzabp.next();
    }
}
