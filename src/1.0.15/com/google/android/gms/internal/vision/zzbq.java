package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzgs;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
public final class zzbq extends zzgs<zzbq, zza> implements zzie {
    private static volatile zzil<zzbq> zzbd;
    private static final zzbq zzhm;
    private int zzbf;
    private int zzhg;
    private int zzhh;
    private int zzhi;
    private int zzhj;
    private boolean zzhk;
    private long zzhl;

    private zzbq() {
    }

    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static final class zza extends zzgs.zza<zzbq, zza> implements zzie {
        private zza() {
            super(zzbq.zzhm);
        }

        public final zza zzj(int i) {
            if (this.zzwi) {
                zzfy();
                this.zzwi = false;
            }
            ((zzbq) this.zzwh).setWidth(i);
            return this;
        }

        public final zza zzk(int i) {
            if (this.zzwi) {
                zzfy();
                this.zzwi = false;
            }
            ((zzbq) this.zzwh).setHeight(i);
            return this;
        }

        public final zza zzb(zzbl zzbl) {
            if (this.zzwi) {
                zzfy();
                this.zzwi = false;
            }
            ((zzbq) this.zzwh).zza((zzbq) zzbl);
            return this;
        }

        public final zza zzb(zzbr zzbr) {
            if (this.zzwi) {
                zzfy();
                this.zzwi = false;
            }
            ((zzbq) this.zzwh).zza((zzbq) zzbr);
            return this;
        }

        public final zza zzc(long j) {
            if (this.zzwi) {
                zzfy();
                this.zzwi = false;
            }
            ((zzbq) this.zzwh).zzb((zzbq) j);
            return this;
        }

        /* synthetic */ zza(zzbp zzbp) {
            this();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private final void setWidth(int i) {
        this.zzbf |= 1;
        this.zzhg = i;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private final void setHeight(int i) {
        this.zzbf |= 2;
        this.zzhh = i;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private final void zza(zzbl zzbl) {
        this.zzhi = zzbl.zzag();
        this.zzbf |= 4;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private final void zza(zzbr zzbr) {
        this.zzhj = zzbr.zzag();
        this.zzbf |= 8;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private final void zzb(long j) {
        this.zzbf |= 32;
        this.zzhl = j;
    }

    public static zza zzai() {
        return (zza) zzhm.zzge();
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzbq>] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzgs
    public final Object zza(int i, Object obj, Object obj2) {
        zzil<zzbq> zzil;
        switch (zzbp.zzbe[i - 1]) {
            case 1:
                return new zzbq();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhm, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001င\u0000\u0002င\u0001\u0003ဌ\u0002\u0004ဌ\u0003\u0005ဇ\u0004\u0006ဂ\u0005", new Object[]{"zzbf", "zzhg", "zzhh", "zzhi", zzbl.zzah(), "zzhj", zzbr.zzah(), "zzhk", "zzhl"});
            case 4:
                return zzhm;
            case 5:
                zzil<zzbq> zzil2 = zzbd;
                zzil<zzbq> zzil3 = zzil2;
                if (zzil2 == null) {
                    synchronized (zzbq.class) {
                        zzil<zzbq> zzil4 = zzbd;
                        zzil = zzil4;
                        if (zzil4 == null) {
                            ?? zzc = new zzgs.zzc(zzhm);
                            zzbd = zzc;
                            zzil = zzc;
                        }
                    }
                    zzil3 = zzil;
                }
                return zzil3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzbq zzbq = new zzbq();
        zzhm = zzbq;
        zzgs.zza(zzbq.class, zzbq);
    }
}
