package com.face.liveness.utils;

import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import java.util.concurrent.ThreadLocalRandom;

public class Liveness {
    private int currentMaxNumTasks;
    private ProcessingState currentState = ProcessingState.NO_FACE;
    private LivenessTask currentTask = LivenessTask.UNKNOWN;
    private int maxNumTask;
    private int numTaskSuccess = 0;
    private int numToSuceed;
    private int waitingTime = 0;
    private int waitingTimeout;

    /* access modifiers changed from: private */
    public enum ProcessingState {
        NO_FACE,
        WAITING,
        MATCHED
    }

    public Liveness(int i, int i2, int i3) {
        this.maxNumTask = i;
        this.numToSuceed = i2;
        this.waitingTimeout = i3;
        this.currentMaxNumTasks = i;
    }

    public void init() {
        this.currentState = ProcessingState.NO_FACE;
        this.currentTask = LivenessTask.UNKNOWN;
        this.numTaskSuccess = 0;
        this.currentMaxNumTasks = this.maxNumTask;
    }

    public LivenessTask updateFrame(FirebaseVisionFace firebaseVisionFace) {
        int i = AnonymousClass1.$SwitchMap$com$face$liveness$utils$Liveness$ProcessingState[this.currentState.ordinal()];
        if (i == 1) {
            this.currentTask = LivenessTask.FACE_FORWARD;
            this.currentState = ProcessingState.WAITING;
        } else if (i != 2) {
            if (i != 3) {
                init();
            } else {
                suceedTask();
            }
        } else if (checkTask(firebaseVisionFace).booleanValue()) {
            suceedTask();
        } else {
            int i2 = this.waitingTime + 1;
            this.waitingTime = i2;
            if (i2 >= this.waitingTimeout) {
                if (this.currentTask == LivenessTask.FACE_FORWARD) {
                    this.currentMaxNumTasks = 1;
                }
                failTask();
                this.waitingTime = 0;
            }
        }
        return this.currentTask;
    }

    private LivenessTask generateRandomTask() {
        if (this.currentTask != LivenessTask.FACE_FORWARD) {
            return LivenessTask.FACE_FORWARD;
        }
        int length = LivenessTask.values().length;
        while (true) {
            LivenessTask livenessTask = LivenessTask.values()[ThreadLocalRandom.current().nextInt(0, length)];
            if (livenessTask != LivenessTask.UNKNOWN && livenessTask != LivenessTask.LIVENESS_FAILED && livenessTask != LivenessTask.LIVENESS_PASSED) {
                return livenessTask;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.face.liveness.utils.Liveness$1  reason: invalid class name */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$face$liveness$utils$Liveness$ProcessingState;
        static final /* synthetic */ int[] $SwitchMap$com$face$liveness$utils$LivenessTask;

        /* JADX WARNING: Can't wrap try/catch for region: R(17:0|(2:1|2)|3|(2:5|6)|7|9|10|11|12|13|14|15|17|18|19|20|(3:21|22|24)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|(2:1|2)|3|5|6|7|9|10|11|12|13|14|15|17|18|19|20|(3:21|22|24)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(21:0|1|2|3|5|6|7|9|10|11|12|13|14|15|17|18|19|20|21|22|24) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0033 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0059 */
        static {
            int[] iArr = new int[LivenessTask.values().length];
            $SwitchMap$com$face$liveness$utils$LivenessTask = iArr;
            try {
                iArr[LivenessTask.FACE_FORWARD.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$face$liveness$utils$LivenessTask[LivenessTask.ROTATE_LEFT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            $SwitchMap$com$face$liveness$utils$LivenessTask[LivenessTask.ROTATE_RIGHT.ordinal()] = 3;
            $SwitchMap$com$face$liveness$utils$LivenessTask[LivenessTask.ROTATE_CLOCLWISE.ordinal()] = 4;
            $SwitchMap$com$face$liveness$utils$LivenessTask[LivenessTask.ROTATE_COUNTERCLOCLWISE.ordinal()] = 5;
            int[] iArr2 = new int[ProcessingState.values().length];
            $SwitchMap$com$face$liveness$utils$Liveness$ProcessingState = iArr2;
            iArr2[ProcessingState.NO_FACE.ordinal()] = 1;
            $SwitchMap$com$face$liveness$utils$Liveness$ProcessingState[ProcessingState.WAITING.ordinal()] = 2;
            try {
                $SwitchMap$com$face$liveness$utils$Liveness$ProcessingState[ProcessingState.MATCHED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private Boolean checkTask(FirebaseVisionFace firebaseVisionFace) {
        Boolean bool = Boolean.FALSE;
        int i = AnonymousClass1.$SwitchMap$com$face$liveness$utils$LivenessTask[this.currentTask.ordinal()];
        boolean z = false;
        if (i == 1) {
            if (((double) Math.abs(firebaseVisionFace.getHeadEulerAngleY())) < 15.0d && ((double) Math.abs(firebaseVisionFace.getHeadEulerAngleZ())) < 5.0d) {
                z = true;
            }
            return Boolean.valueOf(z);
        } else if (i == 2) {
            if (((double) Math.abs(firebaseVisionFace.getHeadEulerAngleZ())) < 10.0d && ((double) firebaseVisionFace.getHeadEulerAngleY()) < -21.0d) {
                z = true;
            }
            return Boolean.valueOf(z);
        } else if (i == 3) {
            if (((double) Math.abs(firebaseVisionFace.getHeadEulerAngleZ())) < 10.0d && ((double) firebaseVisionFace.getHeadEulerAngleY()) > 21.0d) {
                z = true;
            }
            return Boolean.valueOf(z);
        } else if (i == 4) {
            if (((double) Math.abs(firebaseVisionFace.getHeadEulerAngleY())) < 15.0d && ((double) firebaseVisionFace.getHeadEulerAngleZ()) < -13.0d) {
                z = true;
            }
            return Boolean.valueOf(z);
        } else if (i != 5) {
            return bool;
        } else {
            if (((double) Math.abs(firebaseVisionFace.getHeadEulerAngleY())) < 15.0d && ((double) firebaseVisionFace.getHeadEulerAngleZ()) > 13.0d) {
                z = true;
            }
            return Boolean.valueOf(z);
        }
    }

    private void suceedTask() {
        int i = this.numTaskSuccess + 1;
        this.numTaskSuccess = i;
        if (i >= this.numToSuceed) {
            this.currentTask = LivenessTask.LIVENESS_PASSED;
            this.currentState = ProcessingState.NO_FACE;
            return;
        }
        this.currentTask = generateRandomTask();
        this.currentState = ProcessingState.WAITING;
    }

    private void failTask() {
        int i = this.currentMaxNumTasks - 1;
        this.currentMaxNumTasks = i;
        if (i <= this.numTaskSuccess) {
            this.currentTask = LivenessTask.LIVENESS_FAILED;
            this.currentState = ProcessingState.NO_FACE;
            return;
        }
        this.currentTask = generateRandomTask();
        this.currentState = ProcessingState.WAITING;
    }
}
