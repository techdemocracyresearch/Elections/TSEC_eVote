package com.face.liveness.utils;

import com.google.firebase.ml.vision.face.FirebaseVisionFace;

public class BlinkTracker {
    private static int state;

    public static boolean isBlink(FirebaseVisionFace firebaseVisionFace) {
        float leftEyeOpenProbability = firebaseVisionFace.getLeftEyeOpenProbability();
        float rightEyeOpenProbability = firebaseVisionFace.getRightEyeOpenProbability();
        if (!(leftEyeOpenProbability == -1.0f || rightEyeOpenProbability == -1.0f)) {
            int i = state;
            if (i == 0) {
                if (leftEyeOpenProbability > 0.85f && rightEyeOpenProbability > 0.85f) {
                    state = 1;
                }
            } else if (i == 1) {
                if (leftEyeOpenProbability < 0.15f && rightEyeOpenProbability < 0.15f) {
                    state = 2;
                }
            } else if (i == 2 && leftEyeOpenProbability > 0.85f && rightEyeOpenProbability > 0.85f) {
                Utils.showLogs("BlinkTracker", "blink occurred!");
                state = 0;
                return true;
            }
        }
        return false;
    }
}
