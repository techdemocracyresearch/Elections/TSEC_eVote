package com.face.liveness.exception;

import com.face.liveness.utils.Logger;

public class ExceptionUtility {
    public static void logError(String str, String str2, Exception exc) {
        exc.printStackTrace();
        Logger.logException(str, exc);
    }
}
