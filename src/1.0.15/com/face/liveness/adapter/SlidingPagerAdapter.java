package com.face.liveness.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewpager.widget.PagerAdapter;
import com.face.liveness.R;

public class SlidingPagerAdapter extends PagerAdapter {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private int[] IMAGES;
    private Context context;
    private LayoutInflater inflater;

    @Override // androidx.viewpager.widget.PagerAdapter
    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
    }

    @Override // androidx.viewpager.widget.PagerAdapter
    public Parcelable saveState() {
        return null;
    }

    public SlidingPagerAdapter(Context context2, int[] iArr) {
        this.context = context2;
        this.IMAGES = iArr;
        this.inflater = LayoutInflater.from(context2);
    }

    @Override // androidx.viewpager.widget.PagerAdapter
    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) obj);
    }

    @Override // androidx.viewpager.widget.PagerAdapter
    public int getCount() {
        return this.IMAGES.length;
    }

    @Override // androidx.viewpager.widget.PagerAdapter
    public Object instantiateItem(ViewGroup viewGroup, int i) {
        View inflate = this.inflater.inflate(R.layout.pager_image, viewGroup, false);
        ((ImageView) inflate.findViewById(R.id.image)).setImageResource(this.IMAGES[i]);
        viewGroup.addView(inflate, 0);
        return inflate;
    }

    @Override // androidx.viewpager.widget.PagerAdapter
    public boolean isViewFromObject(View view, Object obj) {
        return view.equals(obj);
    }
}
