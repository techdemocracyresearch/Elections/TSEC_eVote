package com.face.liveness.directApi;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseArray;
import com.face.liveness.T5OnFacesResultListener;
import com.face.liveness.T5OnScanResultListener;
import com.face.liveness.utils.T5FaceAngle;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.Landmark;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import java.util.ArrayList;
import java.util.List;

public class DirectApiUtils {
    private static final int FB_EXCEPTION = 5007;
    private FirebaseVisionFaceDetector detector;
    private FirebaseVisionImageMetadata metadata;

    public DirectApiUtils(boolean z) {
        FirebaseVisionFaceDetectorOptions firebaseVisionFaceDetectorOptions;
        if (z) {
            firebaseVisionFaceDetectorOptions = new FirebaseVisionFaceDetectorOptions.Builder().setPerformanceMode(2).setLandmarkMode(1).setMinFaceSize(0.15f).build();
        } else {
            firebaseVisionFaceDetectorOptions = new FirebaseVisionFaceDetectorOptions.Builder().setPerformanceMode(1).setLandmarkMode(1).setClassificationMode(2).setContourMode(2).setMinFaceSize(0.15f).build();
        }
        this.detector = FirebaseVision.getInstance().getVisionFaceDetector(firebaseVisionFaceDetectorOptions);
    }

    public void getAllFaces(final T5Image t5Image, final T5OnFacesResultListener t5OnFacesResultListener) {
        this.detector.detectInImage(FirebaseVisionImage.fromBitmap(t5Image.getBitmap())).addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionFace>>() {
            /* class com.face.liveness.directApi.DirectApiUtils.AnonymousClass2 */

            public void onSuccess(List<FirebaseVisionFace> list) {
                if (list == null || list.size() <= 0) {
                    t5OnFacesResultListener.onFacesResult(null);
                    return;
                }
                ArrayList<Bitmap> arrayList = new ArrayList<>();
                for (FirebaseVisionFace firebaseVisionFace : list) {
                    try {
                        if (firebaseVisionFace.getBoundingBox().left > 0 && firebaseVisionFace.getBoundingBox().top > 0) {
                            Bitmap createBitmap = Bitmap.createBitmap(t5Image.getBitmap(), firebaseVisionFace.getBoundingBox().left, firebaseVisionFace.getBoundingBox().top, firebaseVisionFace.getBoundingBox().width(), firebaseVisionFace.getBoundingBox().height());
                            Log.v("face ", "found");
                            arrayList.add(createBitmap);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                t5OnFacesResultListener.onFacesResult(arrayList);
            }
        }).addOnFailureListener(new OnFailureListener() {
            /* class com.face.liveness.directApi.DirectApiUtils.AnonymousClass1 */

            @Override // com.google.android.gms.tasks.OnFailureListener
            public void onFailure(Exception exc) {
                t5OnFacesResultListener.onFacesError(DirectApiUtils.FB_EXCEPTION, exc.getMessage());
            }
        });
    }

    public void scanFace(T5Image t5Image, boolean z, final T5OnScanResultListener t5OnScanResultListener) {
        FirebaseVisionImage firebaseVisionImage;
        if (this.metadata == null) {
            this.metadata = new FirebaseVisionImageMetadata.Builder().setWidth(t5Image.getWidth()).setHeight(t5Image.getHeight()).setFormat(17).setRotation(t5Image.getOrientation().ordinal()).build();
        }
        if (z) {
            firebaseVisionImage = FirebaseVisionImage.fromBitmap(t5Image.getBitmap());
        } else {
            firebaseVisionImage = FirebaseVisionImage.fromByteArray(t5Image.getImageData(), this.metadata);
        }
        this.detector.detectInImage(firebaseVisionImage).addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionFace>>() {
            /* class com.face.liveness.directApi.DirectApiUtils.AnonymousClass4 */

            public void onSuccess(List<FirebaseVisionFace> list) {
                if (list == null || list.size() <= 0) {
                    t5OnScanResultListener.onScanResult(T5FaceAngle.NO_FACE_FOUND);
                    return;
                }
                FirebaseVisionFace firebaseVisionFace = list.get(0);
                if (firebaseVisionFace.getHeadEulerAngleZ() > -12.0f && firebaseVisionFace.getHeadEulerAngleZ() < 12.0f && firebaseVisionFace.getHeadEulerAngleY() > -12.0f && firebaseVisionFace.getHeadEulerAngleY() < 12.0f) {
                    t5OnScanResultListener.onScanResult(T5FaceAngle.STRAIGHT);
                } else if (firebaseVisionFace.getHeadEulerAngleY() > 36.0f) {
                    t5OnScanResultListener.onScanResult(T5FaceAngle.LEFT);
                } else if (firebaseVisionFace.getHeadEulerAngleY() < -36.0f) {
                    t5OnScanResultListener.onScanResult(T5FaceAngle.RIGHT);
                } else if (firebaseVisionFace.getHeadEulerAngleZ() > 25.0f) {
                    t5OnScanResultListener.onScanResult(T5FaceAngle.CLOCKWISE);
                } else if (firebaseVisionFace.getHeadEulerAngleZ() < -25.0f) {
                    t5OnScanResultListener.onScanResult(T5FaceAngle.ANTI_CLOCKWISE);
                } else {
                    t5OnScanResultListener.onScanResult(T5FaceAngle.FACE_FOUND);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            /* class com.face.liveness.directApi.DirectApiUtils.AnonymousClass3 */

            @Override // com.google.android.gms.tasks.OnFailureListener
            public void onFailure(Exception exc) {
                t5OnScanResultListener.onScanError(DirectApiUtils.FB_EXCEPTION, exc.getMessage());
            }
        });
    }

    public void release() {
        this.metadata = null;
        try {
            this.detector.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap resizeBasedOnEyeDistance(Context context, Bitmap bitmap) {
        FaceDetector.Builder builder = new FaceDetector.Builder(context);
        FaceDetector build = builder.setTrackingEnabled(false).setLandmarkType(1).build();
        SparseArray<Face> detect = build.detect(new Frame.Builder().setBitmap(bitmap).build());
        if (detect.size() == 0) {
            return null;
        }
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < detect.size(); i3++) {
            for (Landmark landmark : detect.valueAt(i3).getLandmarks()) {
                int type = landmark.getType();
                if (type == 4) {
                    i2 = (int) landmark.getPosition().x;
                } else if (type == 10) {
                    i = (int) landmark.getPosition().x;
                }
            }
        }
        int i4 = i - i2;
        Log.v("eyesPosX", "== " + i2 + "," + i);
        float f = ((float) i4) / 70.0f;
        Log.v("selfie Size", "== w:" + bitmap.getWidth() + ", h:" + bitmap.getHeight());
        StringBuilder sb = new StringBuilder();
        sb.append("== ");
        sb.append(f);
        Log.v("requiredPercent", sb.toString());
        int width = (int) (((float) bitmap.getWidth()) / f);
        int height = (int) (((float) bitmap.getHeight()) / f);
        Log.v("distanceBtwEyesX", "== " + i4);
        Log.v("req selfie Size", "== width:" + width + ", height:" + height);
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        build.release();
        return createScaledBitmap;
    }
}
