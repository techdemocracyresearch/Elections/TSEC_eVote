package com.gantix.JailMonkey.Rooted;

import android.content.Context;
import android.os.Build;
import com.scottyab.rootbeer.RootBeer;

public class RootedCheck {
    public static boolean isJailBroken(Context context) {
        CheckApiVersion checkApiVersion;
        if (Build.VERSION.SDK_INT >= 23) {
            checkApiVersion = new GreaterThan23();
        } else {
            checkApiVersion = new LessThan23();
        }
        return checkApiVersion.checkRooted() || rootBeerCheck(context);
    }

    private static boolean rootBeerCheck(Context context) {
        return new RootBeer(context).isRootedWithoutBusyBoxCheck();
    }
}
