package com.mvote.helloworld;

public class SingleTone {
    private static final SingleTone instance = new SingleTone();
    String ANDROID_ID;
    String ANDROID_IMEI;
    String KEY_HASH;

    public static SingleTone getInstance() {
        return instance;
    }

    public void setAndroidId(String str) {
        this.ANDROID_ID = str;
    }

    public String getAndroidId() {
        return this.ANDROID_ID;
    }

    public String getDeviceIMEI() {
        return this.ANDROID_IMEI;
    }

    public void setKEY_HASH(String str) {
        this.KEY_HASH = str;
    }

    public String getKEY_HASH() {
        return this.KEY_HASH;
    }

    public void setDeviceIMEI(String str) {
        this.ANDROID_IMEI = str;
    }

    private SingleTone() {
    }
}
