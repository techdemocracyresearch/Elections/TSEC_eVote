package javax.mail.event;

public abstract class ConnectionAdapter implements ConnectionListener {
    @Override // javax.mail.event.ConnectionListener
    public void closed(ConnectionEvent connectionEvent) {
    }

    @Override // javax.mail.event.ConnectionListener
    public void disconnected(ConnectionEvent connectionEvent) {
    }

    @Override // javax.mail.event.ConnectionListener
    public void opened(ConnectionEvent connectionEvent) {
    }
}
