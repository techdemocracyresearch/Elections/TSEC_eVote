package javax.mail;

import java.util.Vector;
import javax.mail.event.MailEvent;

/* access modifiers changed from: package-private */
public class EventQueue implements Runnable {
    private QueueElement head = null;
    private Thread qThread;
    private QueueElement tail = null;

    /* access modifiers changed from: package-private */
    public static class QueueElement {
        MailEvent event = null;
        QueueElement next = null;
        QueueElement prev = null;
        Vector vector = null;

        QueueElement(MailEvent mailEvent, Vector vector2) {
            this.event = mailEvent;
            this.vector = vector2;
        }
    }

    public EventQueue() {
        Thread thread = new Thread(this, "JavaMail-EventQueue");
        this.qThread = thread;
        thread.setDaemon(true);
        this.qThread.start();
    }

    public synchronized void enqueue(MailEvent mailEvent, Vector vector) {
        QueueElement queueElement = new QueueElement(mailEvent, vector);
        QueueElement queueElement2 = this.head;
        if (queueElement2 == null) {
            this.head = queueElement;
            this.tail = queueElement;
        } else {
            queueElement.next = queueElement2;
            this.head.prev = queueElement;
            this.head = queueElement;
        }
        notifyAll();
    }

    private synchronized QueueElement dequeue() throws InterruptedException {
        QueueElement queueElement;
        while (true) {
            queueElement = this.tail;
            if (queueElement != null) {
                QueueElement queueElement2 = queueElement.prev;
                this.tail = queueElement2;
                if (queueElement2 == null) {
                    this.head = null;
                } else {
                    queueElement2.next = null;
                }
                queueElement.next = null;
                queueElement.prev = null;
            } else {
                wait();
            }
        }
        return queueElement;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0006 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x0007 A[Catch:{ InterruptedException -> 0x0024 }] */
    public void run() {
        while (true) {
            try {
                QueueElement dequeue = dequeue();
                if (dequeue == null) {
                    MailEvent mailEvent = dequeue.event;
                    Vector vector = dequeue.vector;
                    int i = 0;
                    while (true) {
                        if (i < vector.size()) {
                            try {
                                mailEvent.dispatch(vector.elementAt(i));
                            } catch (Throwable th) {
                                if (th instanceof InterruptedException) {
                                    return;
                                }
                            }
                            i++;
                        }
                    }
                    QueueElement dequeue2 = dequeue();
                    if (dequeue2 == null) {
                    }
                } else {
                    return;
                }
            } catch (InterruptedException unused) {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void stop() {
        Thread thread = this.qThread;
        if (thread != null) {
            thread.interrupt();
            this.qThread = null;
        }
    }
}
